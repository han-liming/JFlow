export enum PageEnum {
  // basic login path
  BASE_LOGIN = '/login',
  // basic home path
  BASE_HOME = '/wf/todo',
  // Middleware home path
  BASE_MIDDLE_HOME = '/Middle',
  BASE_CCMobile_HOME = '/CCMobilePortal/Home',
  // error page path
  ERROR_PAGE = '/exception',
  // error log page path
  ERROR_LOG_PAGE = '/error-log/list',
  //DBInstall page
  DB_INSTALL = '/DBInstall',
  //SelectOrg page
  SELECT_ORG = '/SelectOrg',
  //SAAS AdminLogin page
  SAAS_ADMIN_LOGIN = '/SaasAdminLogin',
  //SAAS Login page
  SAAS_LOGIN = '/SaasLogin',
  //SAAS Admin Home
  SAAS_ADMIN_HOME = '/SaasHome',
  //Group Login page
  GROUP_LOGIN = '/GroupLogin',
  //SAASOperation Login page
  SAAS_OP_Login = '/OpLogin',
  //SAASOperation ChooseRegister page
  SAAS_OP_ChooseRegister = '/ChooseRegister',
  //SAASOperation Register page
  SAAS_OP_Register = '/Register',
}
