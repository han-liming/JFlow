import type { User } from '@/stores/user'
import request from '@/utils/request'

const { VITE_GLOB_PrivateKey } = import.meta.env
//表单目录
export function CCForm_List(domain: any) {
  return request.get<any, null>('/WF/API/CCForm_List', {
    params: {
      domain
    }
  })
}

