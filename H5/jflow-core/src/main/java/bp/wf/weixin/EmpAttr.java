package bp.wf.weixin;

/** 
 工作人员属性
*/
public class EmpAttr extends bp.en.EntityNoNameAttr
{

		///#region 基本属性
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 公众号，OpenID
	*/
	public static final String OpenID = "OpenID";
	/**
	 企业微信，UserID
	 */
	public static final String UserID = "UserID";
	/** 
	 手机号码
	*/
	public static final String Tel = "Tel";

		///#endregion
}
