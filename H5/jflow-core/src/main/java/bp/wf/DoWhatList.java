package bp.wf;


/** 
 执行内容列表
*/
public class DoWhatList
{
	public static final String DoNode = "DoNode";
	public static final String Start = "Start";
	public static final String Start5 = "Start5";
	public static final String StartSimple = "StartSimple";
	public static final String Amaze = "Amaze";
	public static final String StartLigerUI = "StartLigerUI";
	public static final String MyWork = "MyWork";
	public static final String Login = "Login";
	public static final String FlowSearch = "FlowSearch";
	public static final String FlowSearchSmall = "FlowSearchSmall";
	public static final String FlowSearchSmallSingle = "FlowSearchSmallSingle";
	public static final String Emps = "Emps";
	public static final String EmpWorks = "EmpWorks";
	public static final String MyFlow = "MyFlow";
	public static final String MyCC = "MyCC";
	public static final String CCList = "CCList";
	public static final String Complete = "Complete";
	public static final String StartEarlyer = "StartEarlyer";
	public static final String Watchdog = "Watchdog";
	public static final String DistributedOfMy = "DistributedOfMy";
	public static final String SearchDataGrid = "SearchDataGrid";
	public static final String SearchDelays = "SearchDelays";
	public static final String HuiQianList = "HuiQianList";
	public static final String TeamupList = "TeamupList";
	public static final String AuthorList = "AuthorList";
	public static final String AuthorTodolist2019 = "AuthorTodolist2019";
	public static final String TaskPoolSharing = "TaskPoolSharing";
	public static final String TaskPoolApply = "TaskPoolApply";
	public static final String FutureTodolist = "FutureTodolist";
	public static final String MySetting = "MySetting";
	public static final String MyFocus = "MyFocus";
	public static final String HungupList = "HungupList";
	public static final String Task = "Task";
	public static final String Knowledge = "Knowledge";
	public static final String FlowFX = "FlowFX";
	public static final String DealWork = "DealWork";
	public static final String Bill = "Bill";
	public static final String Home = "Home";
	public static final String MyStartFlows = "MyStartFlows";
	public static final String MyJoinFlows = "MyJoinFlows";
	public static final String GenerWorkFlowViews = "GenerWorkFlowViews";
	/**
	 处理消息连接
	*/
	public static final String DealMsg = "DealMsg";
	public static final String Tools = "Tools";
	public static final String ToolsSmall = "ToolsSmall";
	public static final String Runing = "Runing";
	public static final String Draft = "Draft";
	/** 
	 打开iu成
	*/
	public static final String Flows = "Flows";
	public static final String Frms = "Frms";
	/** 
	 工作处理器
	*/
	public static final String OneWork = "OneWork";
	public static final String CCMobileDealWork = "CCMobileDealWork";
}
