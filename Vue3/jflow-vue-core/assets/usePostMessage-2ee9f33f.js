import{P as n,a3 as s}from"./index-f4658ae7.js";function o(e){n(()=>{window.addEventListener("message",e,!0)}),s(()=>{window.removeEventListener("message",e,!0)})}export{o as usePostMessage};
