var S=Object.defineProperty;var c=(C,o,F)=>o in C?S(C,o,{enumerable:!0,configurable:!0,writable:!0,value:F}):C[o]=F;var t=(C,o,F)=>(c(C,typeof o!="symbol"?o+"":o,F),F);var p=(C,o,F)=>new Promise((d,e)=>{var r=B=>{try{D(F.next(B))}catch(u){e(u)}},i=B=>{try{D(F.throw(B))}catch(u){e(u)}},D=B=>B.done?d(B.value):Promise.resolve(B.value).then(r,i);D((F=F.apply(C,o)).next())});import{Y as s}from"./index-f4658ae7.js";import{MapAttr as T}from"./MapAttr-cb594d82.js";import{G as l}from"./DataType-33901a1c.js";import{Cond as h}from"./Cond-7bb97535.js";import{Direction as N}from"./Direction-43f7cd43.js";import{PageBaseGroupNew as W,GPNReturnObj as A,GPNReturnType as a}from"./PageBaseGroupNew-ee20c033.js";import{GloComm as f}from"./GloComm-7cfbdfd9.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Events-141c34ea.js";import"./EntityNoName-d08126ae.js";import"./DBAccess-d3bef90d.js";import"./Node-6b42ba5e.js";import"./EntityNodeID-d5ae71b1.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./FrmTrack-10f0746d.js";class E{}t(E,"CondByFrm","0"),t(E,"StandAloneFrm","1"),t(E,"CondStation","2"),t(E,"CondDept","3"),t(E,"CondBySQL","4"),t(E,"CondBySQLTemplate","5"),t(E,"CondByPara","6"),t(E,"CondByUrl","7"),t(E,"CondByWebApi","8"),t(E,"CondByWorkCheck","9"),t(E,"Operator","100");class ou extends W{constructor(){super("GPN_Cond");t(this,"CondByWorkCheck",`
  #### \u5E2E\u52A9
  - \u5BA1\u6838\u7EC4\u4EF6\u7ACB\u573A.

  `);t(this,"ByFrm",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u8868\u5355\u7684\u5B57\u6BB5\u503C\u6765\u8BA1\u7B97,\u662F\u5E38\u7528\u7684\u4E00\u4E2A\u65B9\u5411\u6761\u4EF6,\u524D\u63D0\u662F\u60A8\u4E0D\u662F\u91C7\u7528sdk\u6A21\u5F0F\u5F00\u53D1\u7684,\u662F\u4F7F\u7528ccbpm\u5185\u7F6E\u7684\u8868\u5355.
  - \u573A\u666F: \u8BF7\u5047\u6D41\u7A0B\u7684\u8BF7\u5047\u5929\u6570\u8F6C\u5411\u6761\u4EF6, \u5408\u540C\u5BA1\u6279\u6D41\u7A0B\u7684\u5408\u540C\u91D1\u989D,\u4F5C\u4E3A\u8F6C\u5411\u6761\u4EF6.
  - \u8BF7\u9009\u62E9\u4E00\u4E2A\u8868\u5355,\u70B9\u51FB\u521B\u5EFA\u6309\u94AE.
  #### \u914D\u7F6E\u5B9E\u4F8B\u56FE
  - \u5F53\u6709\u591A\u4E2A\u6761\u4EF6\u7684\u65F6\u8FD9\u4E9B\u624D\u6709\u7528.
  `);t(this,"ByCond100",`
  #### \u5E2E\u52A9
  - \u6761\u4EF6\u8868\u8FBE\u5F0F\u5305\u62EC ( \u3001) \u3001AND\u3001 OR \u56DB\u4E2A\u7C7B\u578B.
  - \u7528\u4E8E\u94FE\u63A5\u6761\u4EF6, \u53EA\u6709\u6B63\u786E\u7684\u914D\u7F6E\u597D\u6761\u4EF6\u8868\u8FBE\u5F0F\u6761\u4EF6\u624D\u53EF\u4EE5\u5DE5\u4F5C.
  - \u6211\u4EEC\u63D0\u4F9B\u68C0\u67E5\u529F\u80FD\uFF0C\u6765\u5E2E\u52A9\u60A8\u68C0\u67E5\u6761\u4EF6\u8868\u8FBE\u5F0F\u662F\u5426\u6B63\u786E.
  - \u60A8\u53EF\u4EE5\u4F7F\u7528\u62D6\u52A8\u7684\uFF0C\u6765\u8C03\u6574\u4F4D\u7F6E.
  #### \u5176\u5B83
  - \u5F53\u6709\u591A\u4E2A\u6761\u4EF6\u7684\u65F6\u8FD9\u4E9B\u624D\u6709\u7528.
  `);t(this,"BySQL",`
  #### \u5E2E\u52A9
  - \u6309SQL\u8868\u8FBE\u5F0F\u8BA1\u7B97.
  - \u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2SQL, \u8FD4\u56DE\u4E00\u884C\u4E00\u5217\uFF0C\u83B7\u53D6\u4E00\u4E2A\u6570\u503C\uFF0C\u5982\u679C\u5927\u4E8E 0\u6761\u4EF6=true, \u5426\u5219 =false.
  - \u6BD4\u5982: SELECT count(*) from port_emp WHERE FK_Dept='@WebUser.DeptNo' or xxxx=@MyFieldName
  - \u652F\u6301ccbpm\u8868\u8FBE\u5F0F, \u53EF\u4EE5\u83B7\u53D6\u5F53\u524D\u767B\u5F55\u4EBA\u5458\u7684\u4FE1\u606F\u53D8\u91CF,\u4E5F\u53EF\u4EE5\u83B7\u53D6\u8868\u5355\u5B57\u6BB5\u53D8\u91CF.
  `);t(this,"sqlTemplate",`
  #### \u5E2E\u52A9
  - \u6309\u7167SQL\u6A21\u677F\u8BA1\u7B97\uFF0C\u4E0E\u6309SQL\u8BA1\u7B97\u7C7B\u4F3C. \u6211\u4EEC\u628A\u5E38\u7528\u7684SQL\u653E\u5165sql\u6A21\u677F\u5E93\u5B58\u50A8\u8D77\u6765.
  - \u8FD9\u91CC\u53EA\u662F\u4E00\u4E2A\u5F15\u7528\uFF0C\u4E0D\u662Fcopy. \u5C31\u662F\u8BF4\u5F53sql\u6A21\u677F\u7684\u914D\u7F6E\u4FE1\u606F\u53D8\u5316\u540E\uFF0C\u8FD9\u91CC\u8DDF\u7740\u53D8\u5316.
  - \u652F\u6301ccbpm\u8868\u8FBE\u5F0F, \u53EF\u4EE5\u83B7\u53D6\u5F53\u524D\u767B\u5F55\u4EBA\u5458\u7684\u4FE1\u606F\u53D8\u91CF,\u4E5F\u53EF\u4EE5\u83B7\u53D6\u8868\u5355\u5B57\u6BB5\u53D8\u91CF.
  #### \u5176\u5B83
  - \u8BF7\u5728\u7CFB\u7EDF\u7BA1\u7406\u91CC\u7EF4\u62A4SQL\u6A21\u677F.
  `);t(this,"CondByPara",`
  #### \u5E2E\u52A9
  - \u6240\u8C13\u7684\u5F00\u53D1\u8005\u53C2\u6570\uFF0C\u5C31\u662F\u5F00\u53D1\u4EBA\u5458\u5728\u6267\u884C\u6D41\u7A0B\u8FC7\u7A0B\u4E2D(\u53D1\u9001\u3001\u9000\u56DE),\u5411\u63A5\u53E3\u4F20\u5165\u7684\u53C2\u6570\u4F5C\u4E3A\u6761\u4EF6.
  - \u6BD4\u5982: \u53D1\u9001\u65B9\u6CD5, 
 #### DEMO
  //\u7EC4\u7EC7\u53C2\u6570.
  Hashtable ht = new Hashtable();
  ht.Add("PrjNo", "\u9879\u76EE\u7F16\u53F7"); 
  ht.Add("PrjName", "\u9879\u76EE\u540D\u79F0"); 
  ht.Add("JinE", 500000.00); //\u9879\u76EE\u91D1\u989D, \u6839\u636E\u9879\u76EE\u91D1\u989D\u5927\u5C0F\u81EA\u52A8\u8F6C\u5411.
  //\u8C03\u7528\u53D1\u9001\u63A5\u53E3.
  BP.WF.Dev2Interface.Node_SendWork("001", 1002, ht, null);
  
  `);t(this,"CondByUrl",`
  #### \u5E2E\u52A9
  - URL\u5C31\u662F\u901A\u8FC7http\u534F\u8BAE,\u8FD0\u884C\u4E00\u4E2Aurl,\u8FD4\u56DE\u6570\u636E\uFF0C\u6839\u636E\u6570\u636E\u5185\u5BB9\u4F5C\u4E3A\u6761\u4EF6\u7684\u4E00\u79CD\u65B9\u5F0F.
  - \u8FD4\u56DE\u503C\u662F err@\u5F00\u5934\u7684\u5B57\u7B26\u4E32\uFF0C\u8BF4\u660E\u7CFB\u7EDF\u662F\u6709\u5F02\u5E38\u3002
  - \u8FD4\u56DE\u503C >0 \u6761\u4EF6 =true,  \u5426\u5219\u4E3Afalse.
  #### \u914D\u7F6E\u53C2\u6570
  - \u914D\u7F6E\u683C\u5F0F: http://ccflow.org/xxx.jsp
  - \u7CFB\u7EDF\u89E3\u6790\u683C\u5F0F: http://ccflow.org/xxx.do?WorkID=xxxx&FK_Flow=001&FK_Node=101&UserNo=zhangsan&Token=xxx-xx-xxx
  - \u7CFB\u7EDF\u4F1A\u81EA\u52A8\u628A\u5F53\u524D\u73AF\u5883\u5DF2\u77E5\u7684\u53C2\u6570\u52A0\u91CC\u9762\u53BB, \u5F00\u53D1\u4EBA\u5458\u53EF\u4EE5\u901A\u8FC7 WorkID\u83B7\u53D6\u6D41\u7A0B\u7684\u5B9E\u4F8B\u7684\u5176\u5B83\u6570\u636E,\u53EF\u4EE5\u901A\u8FC7UserNo, Token\u6765\u6821\u9A8C\u5408\u6CD5\u6027.
  `);t(this,"WebApi",`
  #### \u5E2E\u52A9
   - \u8FD4\u56DE\u503C\u8BF4\u660E,"false"\u662F\u4E0D\u901A\u8FC7\uFF0C"true"\u662F\u901A\u8FC7\u3002
   - WebAPI\u7684\u8F93\u5165\u683C\u5F0F\uFF1Ahttp://demo.ccflow.org/DataUser/GetEmps?id=51184
   - \u63A5\u53E3\u5730\u5740\u652F\u6301\u56FA\u5B9A\u53C2\u6570\uFF0C\u6216\u8005ccbpm\u5185\u7F6E\u53C2\u6570\uFF0C\u6216\u8005\u6D41\u7A0B\u8868\u5355\u53C2\u6570\uFF0C\u6BD4\u5982:http://demo.ccflow.org/DataUser/GetEmps?id=@FK_Node
   #### \u5176\u5B83
   - \u8FD4\u56DE\u503C\u662F err@\u5F00\u5934\u7684\u5B57\u7B26\u4E32\uFF0C\u8BF4\u660E\u7CFB\u7EDF\u662F\u6709\u5F02\u5E38\u3002
   - \u8FD4\u56DE\u503C >0 \u6761\u4EF6 =true,  \u5426\u5219\u4E3Afalse.
  `);t(this,"ByStation",`
  #### \u5E2E\u52A9
   - \u6309\u89D2\u8272\u8BA1\u7B97: \u5C31\u662F\u6307\u5B9A\u8EAB\u4EFD\u7684\u4EBA\u5458\u62E5\u6709\u7684\u89D2\u8272\u662F\u5426\u4E0E\u6761\u4EF6\u8BBE\u7F6E\u7684\u89D2\u8272\u96C6\u5408\u662F\u5426\u6709\u4EA4\u96C6\u6765\u5224\u65AD\u6761\u4EF6\u7684\u4E00\u79CD\u65B9\u5F0F. 
   - \u6709\u4EA4\u96C6=true,\u65E0\u4EA4\u96C6=false.
   - \u8BE5\u65B9\u5F0F\u4F7F\u7528\u6BD4\u8F83\u5E7F\u6CDB, \u6BD4\u5982,\u8BF7\u5047\u4EBA\u662F\u4E2D\u5C42\u89D2\u8272\uFF0C\u8D70\u90A3\u4E2A\u8DEF\u7EBF\uFF0C\u9AD8\u5C42\u89D2\u8272\u8D70\u90A3\u4E2A\u8DEF\u7EBF.
   - \u914D\u7F6E\u65B9\u5F0F:\u9009\u62E9\u89D2\u8272\u96C6\u5408,\u70B9\u521B\u5EFA\u6309\u94AE.
  ##### \u4EBA\u5458\u8EAB\u4EFD
   - \u5C31\u662F\u6309\u90A3\u4E2A\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97,\u786E\u5B9A\u4EBA\u5458\u8EAB\u4EFD\u6C42\u4ED6\u7684\u89D2\u8272\u96C6\u5408.
   - \u9ED8\u8BA4\u4E3A\u5F53\u524D\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97.
  `);t(this,"ByDept",`
  #### \u5E2E\u52A9
  - \u6309\u90E8\u95E8\u8BA1\u7B97: \u5C31\u662F\u6307\u5B9A\u8EAB\u4EFD\u7684\u4EBA\u5458\u62E5\u6709\u7684\u90E8\u95E8\u662F\u5426\u4E0E\u6761\u4EF6\u8BBE\u7F6E\u7684\u90E8\u95E8\u96C6\u5408\u662F\u5426\u6709\u4EA4\u96C6\u6765\u5224\u65AD\u6761\u4EF6\u7684\u4E00\u79CD\u65B9\u5F0F. 
  - \u6709\u4EA4\u96C6=true,\u65E0\u4EA4\u96C6=false.
  - \u8BE5\u65B9\u5F0F\u4F7F\u7528\u6BD4\u8F83\u5E7F\u6CDB, \u6BD4\u5982\uFF1A\u4EC0\u4E48\u89D2\u8272\u7684\u4EBA\u8D70\u90A3\u4E2A\u8DEF\u7EBF.
  - \u914D\u7F6E\u65B9\u5F0F:\u9009\u62E9\u90E8\u95E8\u96C6\u5408,\u70B9\u521B\u5EFA\u6309\u94AE.
 ##### \u4EBA\u5458\u8EAB\u4EFD
  - \u5C31\u662F\u6309\u90A3\u4E2A\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97,\u786E\u5B9A\u4EBA\u5458\u8EAB\u4EFD\u6C42\u4ED6\u7684\u90E8\u95E8\u96C6\u5408.
  - \u9ED8\u8BA4\u4E3A\u5F53\u524D\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97.
  `);this.PageTitle="\u65B0\u5EFA\u6761\u4EF6/\u8868\u8FBE\u5F0F",this.ForEntityClassID="TS.WF.Cond"}Init(){return p(this,null,function*(){this.AddGroup("A","\u6761\u4EF6\u8868\u8FBE\u5F0F"),this.AddBlank("Left","\u5DE6\u62EC\u53F7",this.ByCond100),this.AddBlank("Right","\u53F3\u62EC\u53F7",this.ByCond100),this.AddBlank("AND","AND",this.ByCond100),this.AddBlank("OR","OR",this.ByCond100),this.AddGroup("B","\u5185\u7F6E\u8868\u5355\u6761\u4EF6");const F=new N;F.MyPK=this.RefPKVal,yield F.Retrieve();const d=F.Node,e="ND"+Number(F.FK_Flow)+"Rpt",r=`
    SELECT A.FK_Frm as No,  B.Name as Name FROM WF_FrmNode A,Sys_MapData B WHERE A.FK_Node=${d} AND A.FK_Frm=B.No
     UNION
    SELECT '${e}' as No, '${e}\u6D41\u7A0B\u4E1A\u52A1\u8868' as Name FROM Port_Emp where No='admin' 
    `;this.SelectItemsByList("CondByFrm","\u8868\u5355\u5B57\u6BB5\u6761\u4EF6",this.ByFrm,!1,r),this.SelectItemsByList("CondByFrm.SelectField","\u9009\u62E9\u5B57\u6BB5",this.ByFrm,!1,()=>`SELECT MyPK as No, Name FROM Sys_MapAttr WHERE FK_MapData='${this.RequestVal("tb1","CondByFrm")}' 
      AND KeyOfEn NOT IN ('OID','FID','AtPara')`),this.TextBox1_Name(E.CondByWorkCheck,"\u5BA1\u6838\u7EC4\u4EF6\u7684\u7ACB\u573A",this.CondByWorkCheck,"\u8F93\u5165\u7ACB\u573A\u4E2D\u6587\u540D\u79F0","\u540C\u610F","\u5728\u5BA1\u6838\u7EC4\u4EF6\u4E2D\u7684\u7ACB\u573A\u914D\u7F6E."),this.AddGroup("C","\u7EC4\u7EC7\u7ED3\u6784\u6761\u4EF6"),this.SelectItemsByGroupList(E.CondStation,"\u6309\u89D2\u8272\u8BA1\u7B97",this.ByCond100,!0,l.srcStationTypes,l.srcStations),this.SelectItemsByTree(E.CondDept,"\u6309\u90E8\u95E8\u8BA1\u7B97",this.ByDept,!0,l.srcDepts,l.srcDeptRoot),this.AddGroup("D","\u5F00\u53D1\u63A5\u53E3\u6761\u4EF6");const i=" SELECT count(*) AS NUM FROM MyTable WHERE MyField='@WebUser.No' ";this.TextSQL(E.CondBySQL,"\u6309SQL\u8868\u8FBE\u5F0F\u8BA1\u7B97",this.BySQL,"SQL\u8868\u8FBE\u5F0F",i,"\u8BF7\u8BBE\u7F6E\u4E00\u4E2ASQL\u8BED\u53E5,\u8FD4\u56DE\u662Fnumber");const D="SELECT No as No,Name as Name FROM WF_SQLTemplate WHERE SQLType=0 ";this.SelectItemsByList(E.CondBySQLTemplate,"\u6309SQL\u6A21\u677F\u6761\u4EF6\u8BA1\u7B97",this.sqlTemplate,!1,D),this.TextBox1_Name(E.CondByPara,"\u6309\u5F00\u53D1\u8005\u53C2\u6570\u8BA1\u7B97",this.CondByPara,"\u53C2\u6570","","\u8BF7\u8F93\u5165\u8868\u8FBE\u5F0F,\u6BD4\u5982:Jine > 1000 "),this.TextBox1_Name(E.CondByUrl,"\u6309Url\u6761\u4EF6\u8BA1\u7B97",this.CondByUrl,"URL","http://","\u8BF7\u8F93\u5165url\u5730\u5740."),this.TextBox3_NameNoNote(E.CondByWebApi,"\u6309WebApi\u8FD4\u56DE\u503C\u8BA1\u7B97",this.WebApi,"","\u8BF7\u8F93\u5165webapi\u63A5\u53E3\u5730\u5740 ","\u5224\u65AD\u503C","\u5907\u6CE8(\u4E0D\u4E3A\u7A7A)","")})}Save_TextBox_X(F,d,e,r,i){return p(this,null,function*(){if(F=="CondByFrm")return;const D=this.RequestVal("RefPKVal"),B=new N(D);yield B.Init(),yield B.Retrieve();const u=new h;if(yield u.Init(),u.FK_Flow=B.FK_Flow,u.FK_Node=B.Node,u.ToNodeID=B.ToNode,u.CondType=2,u.DataFrom=F,u.DataFromText=this.GetPageName(F),u.Idx=100,u.RefPKVal=B.MyPK,F==="Left"||F==="Right"||F==="AND"||F==="OR")return u.DataFrom=100,u.DataFromText="\u8FD0\u7B97\u7B26",u.Note=F,F==="Left"&&(u.Note="("),F==="Right"&&(u.Note=")"),u.FK_Operator=u.Note,u.OperatorValue=u.Note,u.SetPara("EnName","TS.WF.Cond100"),yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F!!!"),new A(a.CloseAndReload);if(F==="CondByFrm.SelectField"){const n=new T(e);n.MyPK=e,yield n.Retrieve();let m="TS.WF.CondFrmString";n.LGType===1&&(m="TS.WF.CondFrmEnum",u.Tag1=n.UIBindKey),n.LGType>=2&&(m="TS.WF.CondFrmString"),n.LGType===0&&n.IsNum&&(m="TS.WF.CondFrmNum"),u.Note="",u.DataFrom=E.CondByFrm,u.FK_Attr=e,u.AttrKey=n.KeyOfEn,u.AttrName=r,u.FK_Operator="=",u.FK_OperatorT="\u7B49\u4E8E",u.OperatorValue="0",u.OperatorValueT="\u672A\u8BBE\u7F6E",u.Idx=100,u.SetPara("EnName",m),u.FrmID=this.RequestVal("tb1","CondByFrm"),u.FrmName=this.RequestVal("tb2","CondByFrm"),yield u.Insert();const y=f.UrlEn(m,u.MyPK);return new A(a.GoToUrl,y)}if(F===E.CondByWorkCheck)return u.Note="\u5F53\u7ACB\u573A=["+e+"]\u65F6.",u.FK_Operator=F,u.OperatorValue=e,u.Idx=100,u.SetPara("EnName","TS.WF.CondWorkCheckSelected"),u.AttrKey="",yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F"),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondWorkCheckSelected&PKVal="+u.MyPK);if(F===E.CondBySQL)return u.Note=i,u.FK_Operator=F,u.OperatorValue=r,u.Idx=100,u.SetPara("EnName","TS.WF.CondSQL"),u.FK_DBSrc=e,yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F"),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondSQL&PKVal="+u.MyPK);if(F===E.CondByPara)return u.Note=e,u.FK_Operator=F,u.OperatorValue=e,u.Idx=100,u.SetPara("EnName","TS.WF.CondParas"),yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F"),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondParas&PKVal="+u.MyPK);if(F===E.CondByUrl)return u.Note=e,u.FK_Operator=F,u.OperatorValue=e,u.Idx=100,u.SetPara("EnName","TS.WF.CondWebApi"),yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F"),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondWebApi&PKVal="+u.MyPK);if(F===E.CondByWebApi)return u.Note=i,u.OperatorValue=e,u.Idx=100,u.SetPara("EnName","TS.WF.CondWebApi"),yield u.Insert(),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondWebApi&PKVal="+u.MyPK);if(F===E.CondStation)return u.OperatorValue=e,u.OperatorValueT=r,u.Note=r,u.Idx=100,u.SetPara("EnName","TS.WF.CondStation"),yield u.Insert(),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondStation&PKVal="+u.MyPK);if(F===E.CondDept)return u.OperatorValue=e,u.OperatorValueT=r,u.Note=r,u.Idx=100,u.SetPara("EnName","TS.WF.CondDept"),yield u.Insert(),s.info("\u4FDD\u5B58\u6210\u529F"),new A(a.GoToUrl,"/src/WF/Comm/En.vue?EnName=TS.WF.CondDept&PKVal="+u.MyPK);alert("\u6CA1\u6709\u5224\u65AD\u7684\u7C7B\u578B:"+u.DataFromText)})}GenerSorts(){return Promise.resolve([])}}export{E as DataFrom,ou as GPN_Cond};
