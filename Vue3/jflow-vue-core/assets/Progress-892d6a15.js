import{k as M,c as P,e as l,f as b,b as D}from"./light-0dfdc1ad.js";import{f as S}from"./format-length-c9d165c6.js";import{N as q}from"./Icon-e3cbad7d.js";import{S as L,E as O,I as T}from"./Success-7a2433de.js";import{W as A}from"./Warning-d0098cab.js";import{d as z,f as x,a8 as r}from"./index-f4658ae7.js";import{u as X}from"./use-config-816d55a6.js";import{c as W}from"./create-key-bf4384d6.js";import{u as j}from"./use-css-vars-class-3ae3b4b3.js";const G=e=>{const{infoColor:c,successColor:d,warningColor:n,errorColor:i,textColor2:o,progressRailColor:f,fontSize:s,fontWeight:g}=e;return{fontSize:s,fontSizeCircle:"28px",fontWeightCircle:g,railColor:f,railHeight:"8px",iconSizeCircle:"36px",iconSizeLine:"18px",iconColor:c,iconColorInfo:c,iconColorSuccess:d,iconColorWarning:n,iconColorError:i,textColorCircle:o,textColorLineInner:"rgb(255, 255, 255)",textColorLineOuter:o,fillColor:c,fillColorInfo:c,fillColorSuccess:d,fillColorWarning:n,fillColorError:i,lineBgProcessing:"linear-gradient(90deg, rgba(255, 255, 255, .3) 0%, rgba(255, 255, 255, .5) 100%)"}},Y={name:"Progress",common:M,self:G},E=Y,H=P([l("progress",{display:"inline-block"},[l("progress-icon",`
 color: var(--n-icon-color);
 transition: color .3s var(--n-bezier);
 `),b("line",`
 width: 100%;
 display: block;
 `,[l("progress-content",`
 display: flex;
 align-items: center;
 `,[l("progress-graph",{flex:1})]),l("progress-custom-content",{marginLeft:"14px"}),l("progress-icon",`
 width: 30px;
 padding-left: 14px;
 height: var(--n-icon-size-line);
 line-height: var(--n-icon-size-line);
 font-size: var(--n-icon-size-line);
 `,[b("as-text",`
 color: var(--n-text-color-line-outer);
 text-align: center;
 width: 40px;
 font-size: var(--n-font-size);
 padding-left: 4px;
 transition: color .3s var(--n-bezier);
 `)])]),b("circle, dashboard",{width:"120px"},[l("progress-custom-content",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 justify-content: center;
 `),l("progress-text",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 color: inherit;
 font-size: var(--n-font-size-circle);
 color: var(--n-text-color-circle);
 font-weight: var(--n-font-weight-circle);
 transition: color .3s var(--n-bezier);
 white-space: nowrap;
 `),l("progress-icon",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 color: var(--n-icon-color);
 font-size: var(--n-icon-size-circle);
 `)]),b("multiple-circle",`
 width: 200px;
 color: inherit;
 `,[l("progress-text",`
 font-weight: var(--n-font-weight-circle);
 color: var(--n-text-color-circle);
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 justify-content: center;
 transition: color .3s var(--n-bezier);
 `)]),l("progress-content",{position:"relative"}),l("progress-graph",{position:"relative"},[l("progress-graph-circle",[P("svg",{verticalAlign:"bottom"}),l("progress-graph-circle-fill",`
 stroke: var(--n-fill-color);
 transition:
 opacity .3s var(--n-bezier),
 stroke .3s var(--n-bezier),
 stroke-dasharray .3s var(--n-bezier);
 `,[b("empty",{opacity:0})]),l("progress-graph-circle-rail",`
 transition: stroke .3s var(--n-bezier);
 overflow: hidden;
 stroke: var(--n-rail-color);
 `)]),l("progress-graph-line",[b("indicator-inside",[l("progress-graph-line-rail",`
 height: 16px;
 line-height: 16px;
 border-radius: 10px;
 `,[l("progress-graph-line-fill",`
 height: inherit;
 border-radius: 10px;
 `),l("progress-graph-line-indicator",`
 background: #0000;
 white-space: nowrap;
 text-align: right;
 margin-left: 14px;
 margin-right: 14px;
 height: inherit;
 font-size: 12px;
 color: var(--n-text-color-line-inner);
 transition: color .3s var(--n-bezier);
 `)])]),b("indicator-inside-label",`
 height: 16px;
 display: flex;
 align-items: center;
 `,[l("progress-graph-line-rail",`
 flex: 1;
 transition: background-color .3s var(--n-bezier);
 `),l("progress-graph-line-indicator",`
 background: var(--n-fill-color);
 font-size: 12px;
 transform: translateZ(0);
 display: flex;
 vertical-align: middle;
 height: 16px;
 line-height: 16px;
 padding: 0 10px;
 border-radius: 10px;
 position: absolute;
 white-space: nowrap;
 color: var(--n-text-color-line-inner);
 transition:
 right .2s var(--n-bezier),
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier);
 `)]),l("progress-graph-line-rail",`
 position: relative;
 overflow: hidden;
 height: var(--n-rail-height);
 border-radius: 5px;
 background-color: var(--n-rail-color);
 transition: background-color .3s var(--n-bezier);
 `,[l("progress-graph-line-fill",`
 background: var(--n-fill-color);
 position: relative;
 border-radius: 5px;
 height: inherit;
 width: 100%;
 max-width: 0%;
 transition:
 background-color .3s var(--n-bezier),
 max-width .2s var(--n-bezier);
 `,[b("processing",[P("&::after",`
 content: "";
 background-image: var(--n-line-bg-processing);
 animation: progress-processing-animation 2s var(--n-bezier) infinite;
 `)])])])])])]),P("@keyframes progress-processing-animation",`
 0% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 100%;
 opacity: 1;
 }
 66% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 0;
 opacity: 0;
 }
 100% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 0;
 opacity: 0;
 }
 `)]),_={success:r(L,null),error:r(O,null),warning:r(A,null),info:r(T,null)},V=z({name:"ProgressLine",props:{clsPrefix:{type:String,required:!0},percentage:{type:Number,default:0},railColor:String,railStyle:[String,Object],fillColor:String,status:{type:String,required:!0},indicatorPlacement:{type:String,required:!0},indicatorTextColor:String,unit:{type:String,default:"%"},processing:{type:Boolean,required:!0},showIndicator:{type:Boolean,required:!0},height:[String,Number],railBorderRadius:[String,Number],fillBorderRadius:[String,Number]},setup(e,{slots:c}){const d=x(()=>S(e.height)),n=x(()=>e.railBorderRadius!==void 0?S(e.railBorderRadius):e.height!==void 0?S(e.height,{c:.5}):""),i=x(()=>e.fillBorderRadius!==void 0?S(e.fillBorderRadius):e.railBorderRadius!==void 0?S(e.railBorderRadius):e.height!==void 0?S(e.height,{c:.5}):"");return()=>{const{indicatorPlacement:o,railColor:f,railStyle:s,percentage:g,unit:u,indicatorTextColor:h,status:p,showIndicator:m,fillColor:t,processing:v,clsPrefix:a}=e;return r("div",{class:`${a}-progress-content`,role:"none"},r("div",{class:`${a}-progress-graph`,"aria-hidden":!0},r("div",{class:[`${a}-progress-graph-line`,{[`${a}-progress-graph-line--indicator-${o}`]:!0}]},r("div",{class:`${a}-progress-graph-line-rail`,style:[{backgroundColor:f,height:d.value,borderRadius:n.value},s]},r("div",{class:[`${a}-progress-graph-line-fill`,v&&`${a}-progress-graph-line-fill--processing`],style:{maxWidth:`${e.percentage}%`,backgroundColor:t,height:d.value,lineHeight:d.value,borderRadius:i.value}},o==="inside"?r("div",{class:`${a}-progress-graph-line-indicator`,style:{color:h}},c.default?c.default():`${g}${u}`):null)))),m&&o==="outside"?r("div",null,c.default?r("div",{class:`${a}-progress-custom-content`,style:{color:h},role:"none"},c.default()):p==="default"?r("div",{role:"none",class:`${a}-progress-icon ${a}-progress-icon--as-text`,style:{color:h}},g,u):r("div",{class:`${a}-progress-icon`,"aria-hidden":!0},r(q,{clsPrefix:a},{default:()=>_[p]}))):null)}}}),F={success:r(L,null),error:r(O,null),warning:r(A,null),info:r(T,null)},K=z({name:"ProgressCircle",props:{clsPrefix:{type:String,required:!0},status:{type:String,required:!0},strokeWidth:{type:Number,required:!0},fillColor:String,railColor:String,railStyle:[String,Object],percentage:{type:Number,default:0},offsetDegree:{type:Number,default:0},showIndicator:{type:Boolean,required:!0},indicatorTextColor:String,unit:String,viewBoxWidth:{type:Number,required:!0},gapDegree:{type:Number,required:!0},gapOffsetDegree:{type:Number,default:0}},setup(e,{slots:c}){function d(n,i,o){const{gapDegree:f,viewBoxWidth:s,strokeWidth:g}=e,u=50,h=0,p=u,m=0,t=2*u,v=50+g/2,a=`M ${v},${v} m ${h},${p}
      a ${u},${u} 0 1 1 ${m},${-t}
      a ${u},${u} 0 1 1 ${-m},${t}`,C=Math.PI*2*u,$={stroke:o,strokeDasharray:`${n/100*(C-f)}px ${s*8}px`,strokeDashoffset:`-${f/2}px`,transformOrigin:i?"center":void 0,transform:i?`rotate(${i}deg)`:void 0};return{pathString:a,pathStyle:$}}return()=>{const{fillColor:n,railColor:i,strokeWidth:o,offsetDegree:f,status:s,percentage:g,showIndicator:u,indicatorTextColor:h,unit:p,gapOffsetDegree:m,clsPrefix:t}=e,{pathString:v,pathStyle:a}=d(100,0,i),{pathString:C,pathStyle:$}=d(g,f,n),y=100+o;return r("div",{class:`${t}-progress-content`,role:"none"},r("div",{class:`${t}-progress-graph`,"aria-hidden":!0},r("div",{class:`${t}-progress-graph-circle`,style:{transform:m?`rotate(${m}deg)`:void 0}},r("svg",{viewBox:`0 0 ${y} ${y}`},r("g",null,r("path",{class:`${t}-progress-graph-circle-rail`,d:v,"stroke-width":o,"stroke-linecap":"round",fill:"none",style:a})),r("g",null,r("path",{class:[`${t}-progress-graph-circle-fill`,g===0&&`${t}-progress-graph-circle-fill--empty`],d:C,"stroke-width":o,"stroke-linecap":"round",fill:"none",style:$}))))),u?r("div",null,c.default?r("div",{class:`${t}-progress-custom-content`,role:"none"},c.default()):s!=="default"?r("div",{class:`${t}-progress-icon`,"aria-hidden":!0},r(q,{clsPrefix:t},{default:()=>F[s]})):r("div",{class:`${t}-progress-text`,style:{color:h},role:"none"},r("span",{class:`${t}-progress-text__percentage`},g),r("span",{class:`${t}-progress-text__unit`},p))):null)}}});function N(e,c,d=100){return`m ${d/2} ${d/2-e} a ${e} ${e} 0 1 1 0 ${2*e} a ${e} ${e} 0 1 1 0 -${2*e}`}const Z=z({name:"ProgressMultipleCircle",props:{clsPrefix:{type:String,required:!0},viewBoxWidth:{type:Number,required:!0},percentage:{type:Array,default:[0]},strokeWidth:{type:Number,required:!0},circleGap:{type:Number,required:!0},showIndicator:{type:Boolean,required:!0},fillColor:{type:Array,default:()=>[]},railColor:{type:Array,default:()=>[]},railStyle:{type:Array,default:()=>[]}},setup(e,{slots:c}){const d=x(()=>e.percentage.map((i,o)=>`${Math.PI*i/100*(e.viewBoxWidth/2-e.strokeWidth/2*(1+2*o)-e.circleGap*o)*2}, ${e.viewBoxWidth*8}`));return()=>{const{viewBoxWidth:n,strokeWidth:i,circleGap:o,showIndicator:f,fillColor:s,railColor:g,railStyle:u,percentage:h,clsPrefix:p}=e;return r("div",{class:`${p}-progress-content`,role:"none"},r("div",{class:`${p}-progress-graph`,"aria-hidden":!0},r("div",{class:`${p}-progress-graph-circle`},r("svg",{viewBox:`0 0 ${n} ${n}`},h.map((m,t)=>r("g",{key:t},r("path",{class:`${p}-progress-graph-circle-rail`,d:N(n/2-i/2*(1+2*t)-o*t,i,n),"stroke-width":i,"stroke-linecap":"round",fill:"none",style:[{strokeDashoffset:0,stroke:g[t]},u[t]]}),r("path",{class:[`${p}-progress-graph-circle-fill`,m===0&&`${p}-progress-graph-circle-fill--empty`],d:N(n/2-i/2*(1+2*t)-o*t,i,n),"stroke-width":i,"stroke-linecap":"round",fill:"none",style:{strokeDasharray:d.value[t],strokeDashoffset:0,stroke:s[t]}})))))),f&&c.default?r("div",null,r("div",{class:`${p}-progress-text`},c.default())):null)}}}),J=Object.assign(Object.assign({},D.props),{processing:Boolean,type:{type:String,default:"line"},gapDegree:Number,gapOffsetDegree:Number,status:{type:String,default:"default"},railColor:[String,Array],railStyle:[String,Array],color:[String,Array],viewBoxWidth:{type:Number,default:100},strokeWidth:{type:Number,default:7},percentage:[Number,Array],unit:{type:String,default:"%"},showIndicator:{type:Boolean,default:!0},indicatorPosition:{type:String,default:"outside"},indicatorPlacement:{type:String,default:"outside"},indicatorTextColor:String,circleGap:{type:Number,default:1},height:Number,borderRadius:[String,Number],fillBorderRadius:[String,Number],offsetDegree:Number}),se=z({name:"Progress",props:J,setup(e){const c=x(()=>e.indicatorPlacement||e.indicatorPosition),d=x(()=>{if(e.gapDegree||e.gapDegree===0)return e.gapDegree;if(e.type==="dashboard")return 75}),{mergedClsPrefixRef:n,inlineThemeDisabled:i}=X(e),o=D("Progress","-progress",H,E,e,n),f=x(()=>{const{status:g}=e,{common:{cubicBezierEaseInOut:u},self:{fontSize:h,fontSizeCircle:p,railColor:m,railHeight:t,iconSizeCircle:v,iconSizeLine:a,textColorCircle:C,textColorLineInner:$,textColorLineOuter:y,lineBgProcessing:k,fontWeightCircle:B,[W("iconColor",g)]:R,[W("fillColor",g)]:w}}=o.value;return{"--n-bezier":u,"--n-fill-color":w,"--n-font-size":h,"--n-font-size-circle":p,"--n-font-weight-circle":B,"--n-icon-color":R,"--n-icon-size-circle":v,"--n-icon-size-line":a,"--n-line-bg-processing":k,"--n-rail-color":m,"--n-rail-height":t,"--n-text-color-circle":C,"--n-text-color-line-inner":$,"--n-text-color-line-outer":y}}),s=i?j("progress",x(()=>e.status[0]),f,e):void 0;return{mergedClsPrefix:n,mergedIndicatorPlacement:c,gapDeg:d,cssVars:i?void 0:f,themeClass:s==null?void 0:s.themeClass,onRender:s==null?void 0:s.onRender}},render(){const{type:e,cssVars:c,indicatorTextColor:d,showIndicator:n,status:i,railColor:o,railStyle:f,color:s,percentage:g,viewBoxWidth:u,strokeWidth:h,mergedIndicatorPlacement:p,unit:m,borderRadius:t,fillBorderRadius:v,height:a,processing:C,circleGap:$,mergedClsPrefix:y,gapDeg:k,gapOffsetDegree:B,themeClass:R,$slots:w,onRender:I}=this;return I==null||I(),r("div",{class:[R,`${y}-progress`,`${y}-progress--${e}`,`${y}-progress--${i}`],style:c,"aria-valuemax":100,"aria-valuemin":0,"aria-valuenow":g,role:e==="circle"||e==="line"||e==="dashboard"?"progressbar":"none"},e==="circle"||e==="dashboard"?r(K,{clsPrefix:y,status:i,showIndicator:n,indicatorTextColor:d,railColor:o,fillColor:s,railStyle:f,offsetDegree:this.offsetDegree,percentage:g,viewBoxWidth:u,strokeWidth:h,gapDegree:k===void 0?e==="dashboard"?75:0:k,gapOffsetDegree:B,unit:m},w):e==="line"?r(V,{clsPrefix:y,status:i,showIndicator:n,indicatorTextColor:d,railColor:o,fillColor:s,railStyle:f,percentage:g,processing:C,indicatorPlacement:p,unit:m,fillBorderRadius:v,railBorderRadius:t,height:a},w):e==="multiple-circle"?r(Z,{clsPrefix:y,strokeWidth:h,railColor:o,fillColor:s,railStyle:f,viewBoxWidth:u,percentage:g,showIndicator:n,circleGap:$},w):null)}});export{se as N,E as p};
