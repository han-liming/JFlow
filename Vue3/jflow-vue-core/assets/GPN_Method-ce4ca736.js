var p=Object.defineProperty;var R=(C,D,t)=>D in C?p(C,D,{enumerable:!0,configurable:!0,writable:!0,value:t}):C[D]=t;var E=(C,D,t)=>(R(C,typeof D!="symbol"?D+"":D,t),t);var h=(C,D,t)=>new Promise((l,r)=>{var m=i=>{try{u(t.next(i))}catch(e){r(e)}},P=i=>{try{u(t.throw(i))}catch(e){r(e)}},u=i=>i.done?l(i.value):Promise.resolve(i.value).then(m,P);u((t=t.apply(C,D)).next())});import{PageBaseGroupNew as T,GPNReturnObj as a,GPNReturnType as c}from"./PageBaseGroupNew-ee20c033.js";import{$ as N}from"./index-f4658ae7.js";import{GroupMethods as I,GroupMethodAttr as G}from"./GroupMethod-abd9efe3.js";import{Method as x}from"./Method-27bff018.js";import S from"./DBAccess-d3bef90d.js";import f from"./BSEntity-840a884b.js";import{GloComm as A}from"./GloComm-7cfbdfd9.js";import{G as w}from"./DataType-33901a1c.js";import{MapData as _}from"./MapData-4fa397be.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./ParamsUtils-3cbc5822.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./PCenter-7f795966.js";import"./EntityMyPK-e742fec8.js";import"./PowerCenter-f8ebe7c0.js";import"./FrmTrack-10f0746d.js";import"./EnumLab-4f91f91c.js";class F{}E(F,"Link","Link"),E(F,"Func","Func"),E(F,"Bill","Bill"),E(F,"FrmBBS","FrmBBS"),E(F,"DataVer","DataVer"),E(F,"DictLog","DictLog"),E(F,"QRCode","QRCode"),E(F,"DBList","DBList"),E(F,"Toolbar","Toolbar"),E(F,"ImpFromFile","ImpFromFile"),E(F,"PrintRTF","PrintRTF"),E(F,"PrintHtml","PrintHtml"),E(F,"PrintPDF","PrintPDF"),E(F,"PrintZip","PrintZip"),E(F,"FlowBaseData","FlowBaseData"),E(F,"FlowEtc","FlowEtc"),E(F,"FlowNewEntity","FlowNewEntity"),E(F,"SingleDictGenerWorkFlows","SingleDictGenerWorkFlows");class nu extends T{constructor(){super("GPN_Method");E(this,"SingleDictGenerWorkFlows",`
  #### \u5E2E\u52A9
  - \u4E00\u4E2A\u5B9E\u4F53\u53D1\u8D77\u7684\u6240\u6709\u6D41\u7A0B
  - \u6BD4\u5982\uFF1A\u5728\u4E00\u4E2A\u5B66\u751F\u8EAB\u4E0A\u53D1\u8D77\u7684\uFF0C\u8BF7\u5047\u6D41\u7A0B\u3001\u5165\u515A\u7533\u8BF7\u6D41\u7A0B\u3001\u57FA\u672C\u8D44\u6599\u53D8\u66F4\u6D41\u7A0B.
  - \u6BD4\u5982\uFF1A\u5728\u4E00\u4E2A\u56FA\u5B9A\u8D44\u4EA7\u8EAB\u53D1\u8D77\u7684\uFF1A\u9886\u7528\u6D41\u7A0B\u3001\u7EF4\u4FEE\u6D41\u7A0B\u3001\u6298\u65E7\u6D41\u7A0B\u3001\u79FB\u4EA4\u6D41\u7A0B.
  - \u6240\u6709\u7684\u6D41\u7A0B\u90FD\u7EC4\u5408\u4E00\u4E2A\u8868\u663E\u793A\u51FA\u6765
  `);E(this,"Desc100","\u6682\u672A\u5F00\u653E");E(this,"DocSelfUrl",`
 
 \u81EA\u5B9A\u4E49URL\u83DC\u5355\uFF0C \u60A8\u53EF\u4EE5\u4F7F\u7528\u53F3\u4E0A\u89D2\u7684\u4E0B\u62C9\u6846\u9009\u62E9\u81EA\u5DF1\u8981\u5B9A\u4E49\u7684\u83DC\u5355\u7C7B\u578B.
  #### \u5E2E\u52A9
   - \u83DC\u5355\u8FDE\u63A5\uFF1A http://ccbpm.cn/MyUrl.htm  
   - \u83DC\u5355\u8FDE\u63A5\uFF1A http://ccbpm.cn/MyUrl.htm  
   - \u94FE\u63A5\uFF1A /WF/Comm/Search.htm?EnsName=TS.ZS.Projcets \u67E5\u8BE2
   - \u94FE\u63A5\uFF1A /WF/Comm/Group.htm?EnsName=TS.ZS.Projcets  \u5206\u6790
   - \u94FE\u63A5\uFF1A /WF/MyFlow.htm?FK_Flow=001 \u53D1\u8D77\u6307\u5B9A\u7684\u6D41\u7A0B. 
  #### \u5E2E\u52A9
   -  \u53EF\u4EE5\u4F7F\u7528\u76F8\u5BF9\u8DEF\u5F84\uFF0C\u4E5F\u53EF\u4EE5\u4F7F\u7528\u7EDD\u5BF9\u8DEF\u5F84\u3002
   -  \u7528\u6237\u8F93\u5165\u7684Url:  http://ccbpm.cn/MyUrl.htm
   -  \u6253\u5F00\u7684Url : http://ccbpm.cn/MyUrl.htm?UserNo=xxxx&Token=xxxx\u3002
   -  SID\u5C31\u7C7B\u4F3C\u4E8Etoken, UserNo\u5C31\u662F\u5F53\u524D\u767B\u5F55\u7528\u6237\u7684\u7F16\u53F7\u3002
   -  <img src="SelfUrl.png" class="HelpImg" />
</fieldset>

  `);E(this,"Docs0",`
  
  #### \u5E2E\u52A9
   - \u7528\u4E8E\u89E3\u51B3\u4E0D\u80FD\u5B9E\u73B0\u7684\u5BF9\u5B9E\u4F53\u7684\u64CD\u4F5C\u4E2A\u6027\u5316\u8F83\u5F3A\u7684\u529F\u80FD\u3002
   - \u6BD4\u5982\uFF1A\u60A8\u8F93\u5165\u7684url\u4E3A\u5916\u90E8\u94FE\u63A5: http://ccbpm.cn/MyUrl.htm
   - \u6BD4\u5982\uFF1A\u60A8\u9700\u8981\u6253\u5F00\u7CFB\u7EDF\u5185\u90E8\u7684\u4E00\u4E2Avue\u6587\u4EF6: /src/WF/Comm/En.vue \u8FD9\u4E2A\u6587\u4EF6\u5FC5\u987B\u4F4D\u4E8E\u9879\u76EE\u5185\u90E8
   - \u7CFB\u7EDF\u5C06\u89E3\u6790\u4E3A: http://ccbpm.cn/MyUrl.htm?WorkID=xxxx&FrmID=xxxx&UserNo=xxxx&Token=xxxx
   - \u8BE5\u94FE\u63A5\u663E\u793A\u5728\u67E5\u8BE2\u5DE5\u5177\u680F\u4E0A\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/Link.png "\u5C4F\u5E55\u622A\u56FE.png")      
        `);E(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u5BF9\u4E00\u4E2A\u5B9E\u4F53\u8BB0\u5F55\uFF0C\u6267\u884C\u76F8\u5173\u7684\u64CD\u4F5C\u3002
  - \u6267\u884C\u4E00\u6BB5SQL, Javascript, Url, \u7C7B\u3002
  #### \u65E0\u53C2\u6570\u7684\u65B9\u6CD5\u6548\u679C
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/Func.png "\u5C4F\u5E55\u622A\u56FE.png")    
  #### \u6709\u53C2\u6570\u7684\u65B9\u6CD5\u6548\u679C
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/Func2.png "\u5C4F\u5E55\u622A\u56FE.png")    
        `);E(this,"Docs2",`
  
  #### \u5E2E\u52A9
   - \u5355\u636E\u5C31\u662F\u4F9D\u8D56\u4E0E\u5B9E\u4F53\u5B58\u5728\u6D41\u6C34\u6027\u8D28\u7684\u8BB0\u8D26\u51ED\u8BC1\u3002
   - \u6BD4\u5982\uFF1A\u51FA\u95E8\u8BC1\u3001\u4ECB\u7ECD\u4FE1\u3001\u8BC1\u660E\u51FD\u3002
   - \u6BD4\u5982\uFF1A\u51FA\u5E93\u5355\u3001\u5165\u5E93\u5355\u3002
   - \u521B\u5EFA\u5355\u636E\u540E\uFF0C\u4E0D\u9700\u8981\u5BA1\u6279\uFF0C\u6216\u8005\u7B80\u5355\u7684\u5BA1\u6279\u5C31\u53EF\u4EE5\u8BBE\u7F6E\u4E3A\u5165\u5E93\u72B6\u6001\u7684\u6570\u636E\u3002
    
  `);E(this,"Docs3",`
   
  #### \u5E2E\u52A9
  - \u4E00\u4E2A\u5B9E\u4F53\u91CC\u53EA\u6709\u4E00\u4E2A\u8BE5\u7EC4\u4EF6\u83DC\u5355
  - \u5E94\u7528\u573A\u666F\uFF1A\u586B\u5199\u5BA2\u6237\u8DDF\u8E2A\u4FE1\u606F\u3001\u5B9E\u4F53\u8DDF\u8E2A\u8BB0\u5F55\u3001\u5B9E\u4F53\u7559\u8A00\u8BB0\u5F55\u3001\u591A\u4E2A\u4EBA\u5BF9\u4E00\u4E2A\u5B9E\u4F53\u7684\u64CD\u4F5C\u8BB0\u5F55\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/FrmBBS.png "\u5C4F\u5E55\u622A\u56FE.png")     
  `);E(this,"Docs4",`
   
  #### \u5E2E\u52A9
  - \u7C7B\u4F3C\u4E0E\u6570\u636E\u5E93\u7684\u5907\u4EFD
  - \u53EF\u4EE5\u5728\u4E00\u5B9A\u7684\u4E8B\u4EF6\u5BF9\u5F53\u524D\u7684\u5B9E\u4F53\u8FDB\u884C\u6570\u636E\u5907\u4EFD\uFF0C\u53EF\u4EE5\u6062\u590D\u5230\u6570\u636E\u5230\u6307\u5B9A\u7684\u6570\u636E\u5907\u4EFD\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/DataVer.png "\u5C4F\u5E55\u622A\u56FE.png") 
    
  `);E(this,"Docs5",`
   
  #### \u5E2E\u52A9
  - \u64CD\u4F5C\u65E5\u5FD7\uFF0C\u7559\u5B58\u64CD\u4F5C\u75D5\u8FF9\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/DictLog.png "\u5C4F\u5E55\u622A\u56FE.png") 
    
  `);E(this,"Docs6",`
   
  #### \u5E2E\u52A9
  - \u8BE5\u4E8C\u7EF4\u7801\u662F\u4E00\u4E2A\u7528\u4E8E\u6570\u636E\u626B\u63CF\u67E5\u770B\u7684\u4E8C\u7EF4\u7801\u3002
  -  \u7528\u6237\u626B\u4E00\u626B\u5C31\u53EF\u4EE5\u5728\u624B\u673A\u4E0A\u67E5\u770B\u8BE5\u8868\u5355\u7684\u4FE1\u606F\u3002
  -  \u5982\u679C\u60A8\u9700\u8981\u586B\u62A5\u4E8C\u7EF4\u7801\uFF0C\u8BF7\u5728\u83DC\u5355\u65B0\u5EFA\u3010\u8868\u5355\u586B\u62A5\u4E8C\u7EF4\u7801\u3011\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/QRCode.png "\u5C4F\u5E55\u622A\u56FE.png") 
    
  `);E(this,"Docs7",`
   
  #### \u5E2E\u52A9
  - \u7B2C\u5DE5\u5177\u680F\u6309\u94AE\u6743\u9650\u5C5E\u4E8E\u5B9E\u4F53\u7EC4\u4EF6\u7684\u4E00\u90E8\u5206\u3002
  - \u6309\u7167\u8282\u70B9\u5C5E\u6027\u7684\u8BBE\u7F6E\u4E60\u60EF\uFF0C\u6211\u4EEC\u628A\u5176\u653E\u5165\u4E86\u5B9E\u4F53\u5C5E\u6027\u91CC\u8BBE\u7F6E\u3002
  #### \u8FD0\u884C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/ToolbarRuning.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/ToolbarSetting.png "\u5C4F\u5E55\u622A\u56FE.png")    
  `);E(this,"Docs8",`
   
  #### \u5E2E\u52A9
  -
  `);E(this,"Docs9",`
   
  #### \u5E2E\u52A9
  - 
    
  `);E(this,"Docs10",`
   
  #### \u5E2E\u52A9
  - 
    
  `);E(this,"Docs11",`
   
  #### \u5E2E\u52A9
  - \u6BD4\u5982:\u57FA\u7840\u8D44\u6599\u53D8\u66F4\u3001\u6CD5\u4EBA\u53D8\u66F4\u3001\u4F01\u4E1A\u53D8\u66F4\u3001\u72B6\u6001\u53D8\u66F4\u3002
  - \u5C31\u662F\u5BF9\u5F53\u524D\u6309\u9009\u62E9\u4E00\u884C\uFF08\u4E00\u4E2A\u5B9E\u4F53\u7684\u57FA\u7840\u6570\u636E\u53D8\u66F4)\u3002
  - \u6D41\u7A0B\u7ED3\u675F\u540E\uFF0C\u7CFB\u7EDF\u5C31\u4F1A\u628A\u8FD9\u4E9B\u5B57\u6BB5\u540C\u6B65\u5230\u5B9E\u4F53\u4E2D\u53BB\u3002
  - <a href=https://www.bilibili.com/video/BV12P4y1p74h/>\u6D41\u7A0B\u4E0E\u5B9E\u4F53\u7684\u5173\u7CFB</a>
  #### \u5F00\u53D1\u8BF4\u660E
  - \u70B9\u51FB\u786E\u5B9A\u540E\uFF0C\u7CFB\u7EDF\u81EA\u52A8\u521B\u5EFA\u4E00\u4E2A\u6D41\u7A0B\uFF0C\u5E76\u4E14\u5F00\u6D41\u7A0B\u4E3A\u6781\u7B80\u6A21\u5F0F\u3002
  - \u6B64\u6D41\u7A0B\u7684\u8868\u5355\uFF0C\u662F\u4ECE\u5F53\u524D\u5B9E\u4F53\u8868\u5355\u4E2D\u590D\u5236\u800C\u6765\u7684\u3002
  - \u60A8\u53EF\u4EE5\u6839\u636E\u81EA\u5DF1\u7684\u9700\u8981\u65B0\u589E\u4E0E\u5220\u9664\u5B57\u6BB5\u3002
  - \u53EF\u4EE5\u5728\u65B9\u6CD5\u5C5E\u6027\u91CC\uFF0C\u8BBE\u7F6E\u6570\u636E\u540C\u6B65\u65B9\u5F0F\u4E0E\u540C\u6B65\u5185\u5BB9\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/CCBill/Method/Img/FlowBaseData.png "\u5C4F\u5E55\u622A\u56FE.png")    
  `);E(this,"Docs12",`
   
  #### \u5E2E\u52A9
  - \u6BD4\u5982\uFF1A\u7269\u4E1A\u8D39\u7F34\u7EB3\u3001\u7EF4\u4FEE\u6D41\u7A0B\u3001\u6D3E\u8F66\u6D41\u7A0B\u3001\u5904\u7F5A\u6D41\u7A0B\u3001\u5609\u5956\u6D41\u7A0B\u3001\u4E09\u597D\u5B66\u751F\u8BC4\u5B9A\u3002
  - \u6D41\u7A0B\u8FD0\u884C\u5B8C\u6BD5\u540E\uFF0C\u5C31\u4F5C\u4E3A\u4E1A\u52A1\u67E5\u8BE2\u6570\u636E\u3002
  #### \u5F00\u53D1\u8BF4\u660E
  - \u70B9\u51FB\u786E\u5B9A\u540E\uFF0C\u7CFB\u7EDF\u81EA\u52A8\u521B\u5EFA\u4E00\u4E2A\u6D41\u7A0B\uFF0C\u5E76\u4E14\u5F00\u6D41\u7A0B\u4E3A\u6781\u7B80\u6A21\u5F0F\u3002
  - \u6B64\u6D41\u7A0B\u7684\u8868\u5355\uFF0C\u662F\u4ECE\u5F53\u524D\u5B9E\u4F53\u8868\u5355\u4E2D\u590D\u5236\u800C\u6765\u7684\u3002
  - \u60A8\u53EF\u4EE5\u6839\u636E\u81EA\u5DF1\u7684\u9700\u8981\u65B0\u589E\u4E0E\u5220\u9664\u5B57\u6BB5\u3002
  - <a href=https://www.bilibili.com/video/BV12P4y1p74h/>\u6D41\u7A0B\u4E0E\u5B9E\u4F53\u7684\u5173\u7CFB</a>
    
  `);this.PageTitle="\u65B0\u5EFA\u5B9E\u4F53\u65B9\u6CD5",this.ForEntityClassID="TS.CCBill.Method"}Init(){this.AddGroup("A","\u5E38\u89C4\u7EC4\u4EF6","icon-doc"),this.TextBox2_NameNo(F.Link,"\u81EA\u5B9A\u4E49\u94FE\u63A5",this.Docs0,"","URL\u94FE\u63A5","\u94FE\u63A5\u540D\u79F0","\u6211\u7684\u94FE\u63A5"),this.TextBox2_NameNo(F.Func,"\u65B9\u6CD5",this.Docs1,"Func_","\u65B9\u6CD5ID","\u65B9\u6CD5\u540D\u79F0","\u7F34\u7EB3\u73ED\u8D39"),this.TextBox1_Name(F.FrmBBS,"BBS/\u8BC4\u8BBA/\u65E5\u5FD7\u7EC4\u4EF6",this.Docs3,"\u540D\u79F0","\u8BC4\u8BBA"),this.TextBox1_Name(F.DataVer,"\u6570\u636E\u5FEB\u7167",this.Docs4,"\u540D\u79F0","\u6570\u636E\u5FEB\u7167"),this.TextBox1_Name(F.DictLog,"\u64CD\u4F5C\u65E5\u5FD7",this.Docs5,"\u540D\u79F0","\u64CD\u4F5C\u65E5\u5FD7"),this.TextBox1_Name(F.QRCode,"\u4E8C\u7EF4\u7801",this.Docs6,"\u540D\u79F0","\u4E8C\u7EF4\u7801"),this.TextBox1_Name(F.DBList,"\u6570\u636E\u5217\u8868",this.Docs6,"\u540D\u79F0","\u6570\u636E\u5217\u8868"),this.TextBox1_Name(F.PrintRTF,"RTF\u6A21\u677F\u6253\u5370",this.Docs7,"\u540D\u79F0","RTF\u6A21\u677F\u6253\u5370"),this.AddGroup("D","\u5173\u8054\u5355\u636E","icon-doc"),this.TextBox2_NameNo("NewBill","\u65B0\u5EFA\u5355\u636E",this.HelpTodo,"Bill_","\u7F16\u53F7","\u540D\u79F0","\u7EF4\u4FEE\u5355");const t=this.RequestVal("FrmID"),l=`SELECT OID as No, Lab as Name FROM Sys_GroupField WHERE FrmID='${t}' AND CtrlID='' `,r=` SELECT MyPK AS No, Name, GroupID FROM Sys_MapAttr WHERE FK_MapData='${t}' 
    AND UIContralType <=4 AND KeyOfEn NOT IN ('OID','Rec','RDT','FID','Title','BillNo','BillState','FlowStarter',
    'FlowEmps',
    'FlowStartRDT','WFState','Emps')
    AND UIVisible=1 ORDER BY GroupID,Idx
    `;this.SelectItemsByGroupList("NewBill.SelectAttrs","\u9009\u62E9\u5B57\u6BB5",this.HelpUn,!0,l,r),this.SelectItemsByList("NewBill.SelectAttrs.Group","\u9009\u62E9\u76EE\u5F55",this.HelpUn,!1,w.srcFrmTree),this.SelectItemsByGroupList("RefBill","\u5173\u8054\u5355\u636E",this.Desc100,!1,w.srcFrmTree,w.srcFrmListBill),this.SelectItemsByList("RefBill.DictID","\u9009\u62E9\u5173\u8054\u5B57\u6BB5ID",this.HelpUn,!1,this.RefBillAttrs),this.SelectItemsByList("RefBill.DictID.DictName","\u9009\u62E9\u5173\u8054\u5B57\u6BB5Name",this.HelpUn,!1,this.RefBillAttrs),this.AddGroup("C","\u6D41\u7A0B\u7C7B","icon-doc"),this.TextBox1_Name(F.FlowBaseData,"\u57FA\u7840\u6570\u636E\u53D8\u66F4\u6D41\u7A0B",this.Docs11,"\u6D41\u7A0B\u540D\u79F0","\u57FA\u7840\u6570\u636E\u53D8\u66F4\u6D41\u7A0B"),this.TextBox1_Name(F.FlowEtc,"\u4E1A\u52A1\u6D41\u7A0B",this.Docs12,"\u6D41\u7A0B\u540D\u79F0",""),this.TextBox1_Name(F.SingleDictGenerWorkFlows,"\u5B9E\u4F53\u6D41\u7A0B\u6C47\u603B\u5217\u8868(\u7EFC\u5408\u6D41\u7A0B\u5217\u8868)",this.SingleDictGenerWorkFlows,"\u6D41\u7A0B\u540D\u79F0","\u6D41\u7A0B\u5217\u8868"),this.AddIcon("icon-link","Link"),this.AddIcon("icon-film","Func"),this.AddIcon("icon-bubbles","FrmBBS"),this.AddIcon("icon-docs","DataVer"),this.AddIcon("icon-film","DictLog"),this.AddIcon("icon-frame","QRCode"),this.AddIcon("icon-list","DBList"),this.AddIcon("icon-layers","PrintRTF"),this.AddIcon("icon-doc","NewBill"),this.AddIcon("icon-doc","RefBill"),this.AddIcon("icon-grid","FlowBaseData"),this.AddIcon("icon-grid","FlowEtc"),this.AddIcon("icon-grid","SingleDictGenerWorkFlows")}RefBillAttrs(){return h(this,null,function*(){const t=this.RequestVal("tb1","RefBill"),l=new N("BP.CCBill.WF_CCBill_Admin_Method");return l.AddPara("FrmID",t),yield l.DoMethodReturnString("GPN_Menthd_RefBill_BillAttrs")})}GenerSorts(){return h(this,null,function*(){const t=new I;return yield t.Retrieve(G.FrmID,this.PKVal,"Idx"),t})}Save_TextBox_X(t,l,r,m,P){return h(this,null,function*(){const u=new x;if(u.GroupID=l,u.GroupIDT=this.GetSortName(l),u.FrmID=this.PKVal,u.Icon=this.GetPageIcon(t),u.IsEnable=!0,u.Idx=100,u.Name=r,u.MethodModel=t,t===F.Link){u.Name=r,u.Docs=m,u.No=S.GenerGUID(),u.SetPara("EnName","TS.CCBill.MethodLink"),yield u.Insert();const i=A.UrlEn("TS.CCBill.MethodLink",u.No);return new a(c.GoToUrl,i)}if(t===F.Func){u.Name=r,u.Docs=m,u.No=S.GenerGUID(),u.SetPara("EnName","TS.CCBill.MethodFunc"),u.MethodID=m,yield u.Insert();const i=A.UrlEn("TS.CCBill.MethodFunc",u.No);return new a(c.GoToUrl,i)}if(t==="NewBill"){const i=new _;if(i.No=m,(yield i.RetrieveFromDBSources())!=0)return new a(c.Error,"\u8868\u5355ID["+r+"]\u5DF2\u7ECF\u5B58\u5728.")}if(t=="RefBill.DictID.DictName"){const i=this.RequestVal("FrmID"),e=this.RequestVal("tb2","RefBill"),n=this.RequestVal("tb1","RefBill"),s=this.RequestVal("tb1","RefBill.DictID"),o=this.RequestVal("tb1","RefBill.DictID.DictName");u.Name=n,u.MethodModel="DictRefBill",u.No=i+"_"+e,u.Tag1=e,u.Tag2=n,u.SetPara("EnName","TS.CCBill.MethodDictRefBill"),u.SetPara("RefDictNo",s),u.SetPara("RefDictName",o),u.MethodID="DictRefBill",yield u.Insert();const B=A.UrlEn("TS.CCBill.MethodDictRefBill",u.No);return new a(c.GoToUrl,B)}if(t=="NewBill.SelectAttrs.Group"){const i=this.RequestVal("FrmID"),e=this.RequestVal("tb2","NewBill"),n=this.RequestVal("tb1","NewBill"),s=this.RequestVal("tb3","NewBill"),o=this.RequestVal("tb1","NewBill.SelectAttrs.Group"),B=new N("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");B.AddUrlData(),B.AddPara("FK_FrmSort",o),B.AddPara("TB_No",e),B.AddPara("TB_Name",n),B.AddPara("TB_PTable",s),B.AddPara("DDL_PTableModel",0),B.AddPara("EntityType",1),B.AddPara("SelectAttrs",this.RequestVal("tb1","NewBill.SelectAttrs")),B.AddPara("DictFrmID",i),yield B.DoMethodReturnString("NewFrmGuide_Create"),u.Name=n,u.MethodModel="DictRefBill",u.No=i+"_"+e,u.Tag1=e,u.Tag2=n,u.SetPara("EnName","TS.CCBill.MethodDictRefBill"),u.SetPara("RefDictNo",i+"No"),u.SetPara("RefDictName",i+"Name"),u.MethodID="DictRefBill",yield u.Insert();const d=A.UrlEn("TS.CCBill.MethodDictRefBill",u.No);return new a(c.GoToUrl,d)}if(t===F.FrmBBS||t===F.DBList||t===F.DictLog||t===F.QRCode||t==F.DataVer){if(u.No=this.PKVal+"_"+t,(yield u.IsExits())==!0){if(t!=F.DBList){alert("\u8BE5\u7EC4\u4EF6\u5DF2\u7ECF\u5B58\u5728,\u4E0D\u53EF\u91CD\u590D\u6DFB\u52A0.");return}u.No=S.GenerGUID(),u.MethodID=t}u.Name=r,t===F.FrmBBS&&(u.Icon="icon-film"),t===F.DictLog&&(u.Icon="icon-eye"),t===F.QRCode&&(u.Icon="icon-frame"),t===F.DataVer&&(u.Icon="icon-camera"),t===F.DBList&&(u.Icon="icon-drop"),u.SetPara("EnName","TS.CCBill.Method"+t),yield u.Insert();const i=A.UrlEn(u.GetParaString("EnName",""),u.No);return new a(c.GoToUrl,i)}if(t===F.PrintHtml||t===F.PrintPDF||t===F.PrintRTF||t==F.PrintZip){if(u.No=this.PKVal+"_"+t,t===F.PrintRTF&&(yield u.IsExits())==!0)u.No=S.GenerGUID();else if((yield u.IsExits())==!0){alert("\u8BE5\u7EC4\u4EF6\u5DF2\u7ECF\u5B58\u5728,\u4E0D\u53EF\u91CD\u590D\u6DFB\u52A0.");return}u.Name=r,t===F.PrintHtml&&(u.Icon="icon-printer"),t===F.PrintPDF&&(u.Icon="icon-printer"),t===F.PrintRTF&&(u.Icon="icon-printer"),t===F.PrintZip&&(u.Icon="icon-cloud-download"),u.Tag1=t,t=="PrintRTF"?u.SetPara("EnName","TS.CCBill.MethodPrintRTF"):u.SetPara("EnName","TS.CCBill.MethodPrint"),yield u.Insert();const i="/src/WF/Comm/En.vue?EnName=TS.CCBill.MethodFlowBaseData&PKVal="+u.No;return new a(c.GoToUrl,i)}if(t===F.FlowBaseData){const i=r;let e=this.RefPKVal;(e==null||e==null)&&(e=this.RequestVal("FrmID"));const n=l;(e==null||e=="")&&(e=this.PKVal);const s=new f("BP.Sys.MapData",e);s.No=e,yield s.Retrieve();const o=new N("BP.CCBill.WF_CCBill_Admin_Method");o.AddPara("SortNo",s.data.FK_FormTree),o.AddPara("FlowName",i),o.AddPara("Name",i),o.AddPara("FrmID",e),o.AddPara("FlowDevModel",1),o.AddPara("GroupID",n),o.AddPara("ModuleNo","");const B=yield o.DoMethodReturnString("FlowBaseData_Save"),d=A.UrlEn("TS.CCBill.MethodFlowBaseData",B);return new a(c.GoToUrl,d)}if(t===F.FlowEtc){const i=r;let e=this.RefPKVal;(e==null||e==null)&&(e=this.RequestVal("FrmID"));const n=l;(e==null||e=="")&&(e=this.PKVal);const s=new f("BP.Sys.MapData",e);s.No=e,yield s.Retrieve();const o=new N("BP.CCBill.WF_CCBill_Admin_Method");o.AddPara("SortNo",s.FK_FormTree),o.AddPara("FlowName",i),o.AddPara("Name",i),o.AddPara("FrmID",e),o.AddPara("FlowDevModel",1),o.AddPara("GroupID",n),o.AddPara("ModuleNo","");const B=yield o.DoMethodReturnString("FlowEtc_Save"),d=A.UrlEn("TS.CCBill.MethodFlowEtc",B);return new a(c.GoToUrl,d)}if(t===F.SingleDictGenerWorkFlows){if(u.No=this.RefPKVal+"_"+t,(yield u.IsExits())==!0){alert("\u8BE5\u7EC4\u4EF6\u5DF2\u7ECF\u5B58\u5728,\u4E0D\u53EF\u91CD\u590D\u6DFB\u52A0.");return}u.Name=r,u.MethodID=F.SingleDictGenerWorkFlows,u.MethodModel=F.SingleDictGenerWorkFlows,u.RefMethodType=1,u.Icon="icon-drop",u.SetPara("EnName","TS.CCBill.MethodSingleDictGenerWorkFlow"),u.Insert();const i="/src/WF/Comm/En.vue?EnName=TS.CCBill.MethodSingleDictGenerWorkFlow&PKVal="+u.No;return new a(c.GoToUrl,i)}})}}export{nu as GPN_Method,F as MethodModel};
