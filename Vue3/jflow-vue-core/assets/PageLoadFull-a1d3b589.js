var r=Object.defineProperty;var D=(F,E,u)=>E in F?r(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var t=(F,E,u)=>(D(F,typeof E!="symbol"?E+"":E,u),u);import{UAC as e}from"./UAC-8e255d47.js";import{Map as o}from"./Map-73575e6b.js";import{EntityMyPK as C}from"./EntityMyPK-e742fec8.js";import{a as B}from"./MapExt-db8cd7f3.js";import{SFDBSrc as i}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class w extends C{constructor(u){super("TS.MapExt.PageLoadFull");t(this,"Desc0",`
  #### \u6267\u884C\u5185\u5BB9
   - \u9996\u5148\u8981\u9009\u62E9\u6570\u636E\u6E90,\u6839\u636E\u6570\u636E\u6E90\u586B\u5199\u6267\u884C\u5185\u5BB9.
   - \u6267\u884C\u5185\u5BB9\u540E\u8FD4\u56DE\u7684\u662F\u6570\u636E\u6E90\uFF0C\u5BF9\u6570\u636E\u6E90\u8981\u8981\u6C42\u662F\u4E00\u884C\u4E00\u5217\u6570\u636E.
  #### SQL\u683C\u5F0F
   - SQL\u586B\u5199\u5E2E\u52A9.
   - \u5FC5\u987B\u8FD4\u56DE\u4E00\u884C\u6570\u636E\u7684 SQL\u6216\u8005\u6570\u636E\u6E90.
   - \u8FD4\u56DE\u7684\u5217\u540D\u8981\u4E0E\u5B57\u6BB5\u540D\u8FDB\u884C\u5BF9\u5E94\uFF0C\u5982\u679C\u5339\u914D\u7684\u4E0D\u7BA1\u63A7\u4EF6\u7C7B\u578B\u7CFB\u7EDF\u5C31\u4F1A\u81EA\u52A8\u8D4B\u503C\u3002
   - \u5B9E\u4F8B\uFF1A SELECT Name as MingCheng, Tel as DianHua, Email FROM WF_EMP WHERE No='@WebUser.No'
   - @WebUser.No \u7CFB\u7EDF\u7EA6\u5B9A\u7684\u6807\u8BB0\u3002
   - @WorkID \u66FF\u6362\u6D41\u7A0B\u4E2D\u7684\u6570\u636E
  #### URL\u683C\u5F0F
   - \u5FC5\u987B\u8FD4\u56DE\u4E00\u884C\u6570\u636E\u7684\u7684json\u683C\u5F0F\u7684\u6570\u636E\u6E90\u3002
   - \u8FD4\u56DE\u7684\u5217\u540D\u8981\u4E0E\u5B57\u6BB5\u540D\u8FDB\u884C\u5BF9\u5E94\uFF0C\u5982\u679C\u5339\u914D\u7684\u4E0D\u7BA1\u63A7\u4EF6\u7C7B\u578B\u7CFB\u7EDF\u5C31\u4F1A\u81EA\u52A8\u8D4B\u503C\u3002
   - \u5B9E\u4F8B\uFF1A /App/Handler.ashx?DoType=EmpFull&Key=@Key
   - @Key \u5C31\u662F\u6307\u9009\u62E9\u7684\u4E3B\u952E. \u662F\u7CFB\u7EDF\u7EA6\u5B9A\u7684\u6807\u8BB0\u3002
  `);t(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5F39\u51FA\u6846\u786E\u5B9A\u540E\u6267\u884C\u7684JS\uFF0C\u53EF\u4EE5\u76F4\u63A5\u5199\u65B9\u6CD5\u540D\u6216\u8005\u65B9\u6CD5()\u3002
   - \u7528\u6237\u6570\u636E\u8FD4\u56DE\u586B\u5145\u540E\uFF0C\u5BF9\u6570\u636E\u7684\u5904\u7406\u3002
   `);u&&(this.MyPK=u)}get HisUAC(){const u=new e;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new o("Sys_MapExt","\u88C5\u8F7D\u586B\u5145\u4E3B\u8868");return u.AddGroupAttr("\u586B\u5145\u4E3B\u8868"),u.AddMyPK(),u.AddDDLEntities(B.FK_DBSrc,"local","\u6570\u636E\u6E90",new i,!0,null,!1),u.AddTBStringDoc(B.Doc,null,"\u6267\u884C\u5185\u5BB9",!0,!1,!0,this.Desc0),u.AddTBStringDoc(B.Tag2,null,"\u786E\u5B9A\u540E\u6267\u884C\u7684JS",!0,!1,!0,this.Desc1),this._enMap=u,this._enMap}}export{w as PageLoadFull};
