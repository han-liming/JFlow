var p=Object.defineProperty;var o=(e,t,u)=>t in e?p(e,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):e[t]=u;var r=(e,t,u)=>(o(e,typeof t!="symbol"?t+"":t,u),u);import{UAC as E}from"./UAC-8e255d47.js";import{Map as a}from"./Map-73575e6b.js";import{EntityMyPK as m}from"./EntityMyPK-e742fec8.js";import{a as i}from"./MapExt-db8cd7f3.js";import{SFDBSrc as s}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class H extends m{constructor(u){super("TS.MapExt.MultipleChoiceSearchEn1");r(this,"DescSearchTip",`
  #### \u8BF4\u660E
  - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57.
  - \u8F93\u5165\u57CE\u5E02\u540D\u79F0,\u6BD4\u5982:beijing,bj,\u8FDB\u884C\u641C\u7D22.
  - \u4EBA\u5458\u7684\u7F16\u53F7,\u540D\u79F0,\u62FC\u97F3,\u8FDB\u884C\u6A21\u7CCA\u641C\u7D22.
  
  `);r(this,"DescTag2",` 
  #### \u8BF4\u660E
  - SQL\u683C\u5F0F\u4E3A:
  - SELECT No,Name FROM Port_Emp WHERE PinYin LIKE '@Key%' OR No LIKE '%@Key%' OR Name LIKE '%@Key%' 
  - SELECT No,Name FROM CN_City WHERE PinYin LIKE '%@Key%' OR Name LIKE '%@Key%'
  - URL\u683C\u5F0F\u4E3A:
  - /DataUser/Handler.ashx?xxx=sss 
  - \u65B9\u6CD5\u7684\u683C\u5F0F\u4E3A:
  - MyFunName
  `);u&&(this.MyPK=u)}get HisUAC(){const u=new E;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new a("Sys_MapExt","\u641C\u7D22\u591A\u9009");return u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK(),u.AddDDLEntities(i.FK_DBSrc,"local","\u6570\u636E\u6E90",new s,!0,null,!1),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.DescSearchTip),u.AddDDLSysEnum(i.DBType,0,"\u6570\u636E\u6E90\u7C7B\u578B",!0,!0,"DBType","@0=\u6267\u884CSQL@1=\u6267\u884Curl\u8FD4\u56DEJSON@2=\u6267\u884CCCFromRef.js\u8FD4\u56DEJSON",null,!1),u.AddRadioBtn("MultipleSelectType",1,"\u9009\u62E9\u7C7B\u578B",!0,!0,"MultipleSelectType","@0=\u5355\u9009@1=\u591A\u9009",null,!0),u.AddTBString(i.Doc,null,"\u6570\u636E\u6E90\u8868\u8FBE\u5F0F",!0,!1,0,50,200,!0,this.DescTag2),u.ParaFields=",SearchTip,MultipleSelectType,",u.AddTBAtParas(4e3),this._enMap=u,this._enMap}}export{H as MultipleChoiceSearchEn1};
