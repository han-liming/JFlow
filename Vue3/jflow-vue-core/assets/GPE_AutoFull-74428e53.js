var l=Object.defineProperty;var C=(o,t,u)=>t in o?l(o,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):o[t]=u;var p=(o,t,u)=>(C(o,typeof t!="symbol"?t+"":t,u),u);var m=(o,t,u)=>new Promise((E,F)=>{var i=B=>{try{r(u.next(B))}catch(A){F(A)}},a=B=>{try{r(u.throw(B))}catch(A){F(A)}},r=B=>B.done?E(B.value):Promise.resolve(B.value).then(i,a);r((u=u.apply(o,t)).next())});import{PageBaseGroupEdit as s}from"./PageBaseGroupEdit-202e8e85.js";import{D as g}from"./DataType-33901a1c.js";import{b as e,a as n}from"./MapExt-db8cd7f3.js";import{MapAttr as c}from"./MapAttr-cb594d82.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./Help-be517e8f.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Events-141c34ea.js";class X extends s{constructor(){super("GPE_AutoFull");p(this,"Desc1",` 
  #### \u5E2E\u52A9
  \u8BF4\u660E\uFF1A\u81EA\u52A8\u8BA1\u7B97\u5C31\u662F\u5BF9\u5B57\u6BB5\u4E4B\u524D\u8FDB\u884C\u6570\u5B66\u57FA\u672C\u8BA1\u7B97\u3002\u53EF\u4EE5\u5BF9\u4E3B\u8868\u7684\u5B57\u6BB5\u8FDB\u884C\u8BA1\u7B97\uFF0C\u4E5F\u53EF\u4EE5\u5BF9\u4ECE\u8868\u7684\u5B57\u6BB5\u8FDB\u884C\u8BA1\u7B97\u3002
  #### \u5E94\u7528\u573A\u666F
  \u5B9A\u8D27\u65F6\uFF0C\u6709\u5355\u4F4D\uFF0C\u6709\u6570\u91CF\uFF0C\u81EA\u52A8\u6C42\u5408\u8BA1\u3002
  #### \u6548\u679C\u56FE
   - \u4E3B\u8868\u8BA1\u7B97-\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullzhuyanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u4ECE\u8868\u8BA1\u7B97-\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullcongBiaodan.png "\u5C4F\u5E55\u622A\u56FE.png")  
  `);p(this,"Desc2",` 
  #### \u5E2E\u52A9
  1. \u5982\u679C\u662F\u4E3B\u8868: \u5C31\u662F\u4E3B\u8868\u5B57\u6BB5\u4E4B\u95F4\u7684\u8BA1\u7B97\uFF0C\u6BD4\u5982: @A+@B
  2. \u5982\u679C\u662F\u4ECE\u8868: \u8868\u8FBE\u5F0F\u5C31\u662F\u5217\u4E4B\u95F4\u7684\u8BA1\u7B97,\u6BD4\u5982: @DanJia*@ShuLiang
  3. \u4EC5\u4EC5\u652F\u6301\u6570\u503C\u7C7B\u578B\u7684\u8BA1\u7B97\uFF0C\u6BD4\u5982\uFF1Afloat,int,decimal\u7C7B\u578B\u7684\u6570\u636E\u5B57\u6BB5\u3002
  #### \u4E3B\u8868\u81EA\u52A8\u8BA1\u7B97-\u56FE\u4F8B
  - \u914D\u7F6E\u56FE\u4F8B1
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullzhu.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u914D\u7F6E\u56FE\u4F8B2
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullzhu1.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u8FD0\u884C\u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullzhuyanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u4ECE\u8868\u81EA\u52A8\u8BA1\u7B97-\u56FE\u4F8B

  - \u914D\u7F6E\u56FE\u4F8B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullCong1.png "\u5C4F\u5E55\u622A\u56FE.png") 


  - \u8FD0\u884C\u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFull/Img/AutoFullcongBiaodan.png "\u5C4F\u5E55\u622A\u56FE.png") 
....`);this.PageTitle="\u81EA\u52A8\u8BA1\u7B97"}Init(){return m(this,null,function*(){this.entity=new e,this.KeyOfEn=n.DoWay;const u=this.GetRequestVal("PKVal"),E=new c(u);yield E.Retrieve();const F=new e,i="AutoFull";F.MyPK=E.MyPK+"_"+i,(yield F.RetrieveFromDBSources())==0&&(F.FK_MapData=E.FK_MapData,F.DoWay=0,F.ExtType=i,F.AttrOfOper=E.KeyOfEn,F.Tag1=1,yield F.Insert()),this.entity=F,this.AddGroup("A","\u81EA\u52A8\u8BA1\u7B97"),this.Blank("0","\u7981\u7528",this.Desc1),this.SingleTB("1","\u542F\u7528\u81EA\u52A8\u8BA1\u7B97",n.Tag,this.Desc2,"\u683C\u5F0F:@DanJia*@JinE",g.AppString)})}AfterSave(u,E){if(u==E)throw new Error("Method not implemented.")}BtnClick(u,E,F){if(u==E||u===F)throw new Error("Method not implemented.")}}export{X as GPE_AutoFull};
