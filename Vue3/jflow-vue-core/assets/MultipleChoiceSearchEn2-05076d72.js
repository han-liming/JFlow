var o=Object.defineProperty;var a=(e,t,u)=>t in e?o(e,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):e[t]=u;var E=(e,t,u)=>(a(e,typeof t!="symbol"?t+"":t,u),u);import{UAC as p}from"./UAC-8e255d47.js";import{Map as s}from"./Map-73575e6b.js";import{EntityMyPK as F}from"./EntityMyPK-e742fec8.js";import{a as r}from"./MapExt-db8cd7f3.js";import{SFDBSrc as i}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class w extends F{constructor(u){super("TS.MapExt.MultipleChoiceSearchEn2");E(this,"DescSearchtip",` 
  #### \u8BF4\u660E
  - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57.
  - \u8F93\u5165\u57CE\u5E02\u540D\u79F0,\u6BD4\u5982:beijing,bj,\u8FDB\u884C\u641C\u7D22.
  - \u4EBA\u5458\u7684\u7F16\u53F7,\u540D\u79F0,\u62FC\u97F3,\u8FDB\u884C\u6A21\u7CCA\u641C\u7D22.
   `);E(this,"DescTag1",` 
   #### \u8BF4\u660E
   - \u5217\u8868\u6570\u636E\u6E90\uFF0C\u53EF\u4EE5\u9650\u5B9A\u663E\u793A\u5217\u8868\u7684\u8303\u56F4\uFF0C\u6BD4\u5982\u53EA\u663E\u793A\u524D10\u884C\u3002\u4F7F\u7528\u4E8E\u5927\u91CF\u6570\u636E\u65F6\u3002
    `);E(this,"DescDoc",` 
  #### \u8BF4\u660E
  - SQL\u683C\u5F0F\u4E3A:
  - SELECT No,Name FROM Port_Emp WHERE PinYin LIKE '@Key%' OR No LIKE '%@Key%' OR Name LIKE '%@Key%' 
  - SELECT No,Name FROM CN_City WHERE PinYin LIKE '%@Key%' OR Name LIKE '%@Key%'
  - URL\u683C\u5F0F\u4E3A:
  - /DataUser/Handler.ashx?xxx=sss 
  - \u65B9\u6CD5\u7684\u683C\u5F0F\u4E3A:
  - MyFunName
   `);u&&(this.MyPK=u)}get HisUAC(){const u=new p;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new s("Sys_MapExt","\u641C\u7D22+\u8F93\u5165\u591A\u9009");return u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK(),u.AddDDLEntities(r.FK_DBSrc,"local","\u6570\u636E\u6E90",new i,!0,null,!1),u.AddTBString("Title",null,"\u6807\u9898",!0,!1,0,50,200,!0),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.DescSearchtip),u.AddDDLSysEnum(r.DBType,0,"\u6570\u636E\u6E90\u7C7B\u578B",!0,!0,"DBType","@0=\u6267\u884CSQL@1=\u6267\u884Curl\u8FD4\u56DEJSON@2=\u6267\u884CCCFromRef.js\u8FD4\u56DEJSON",null,!1),u.AddBoolean(r.Tag,!1,"\u662F\u5426\u663E\u793A\u7B7E\u540D",!0,!0),u.AddDDLEntities(r.FK_DBSrc,"local","\u6570\u636E\u5E93",new i,!0,null,!1),u.AddTBString(r.Doc,null,"\u641C\u7D22\u6570\u636E\u6E90",!0,!1,0,50,200,!0,this.DescDoc),u.AddTBString(r.Tag1,null,"\u5217\u8868\u6570\u636E\u6E90",!0,!1,0,50,200,!0,this.DescTag1),u.ParaFields=",Title,SearchTip,",u.AddTBAtParas(),this._enMap=u,this._enMap}}export{w as MultipleChoiceSearchEn2};
