var o=Object.defineProperty;var F=(r,t,u)=>t in r?o(r,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):r[t]=u;var E=(r,t,u)=>(F(r,typeof t!="symbol"?t+"":t,u),u);import{UAC as a}from"./UAC-8e255d47.js";import{Map as B}from"./Map-73575e6b.js";import{EntityMyPK as A}from"./EntityMyPK-e742fec8.js";import{a as e}from"./MapExt-db8cd7f3.js";import{SFDBSrc as l}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class O extends A{constructor(u){super("TS.MapExt.PopTreeEns");E(this,"NoteSearchTip",`
  #### \u5E2E\u52A9
   - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57\uFF0C\u6BD4\u5982:
   - \u8BF7\u8F93\u5165\u4ED8\u6B3E\u4EBA\u540D\u79F0,\u8FDB\u884C\u641C\u7D22.
   - \u8F93\u5165\u4EBA\u5458\u7F16\u53F7,\u540D\u79F0\uFF0C\u540D\u79F0\u5168\u62FC,\u7B80\u62FC\u5173\u952E\u5B57\u641C\u7D22
  `);E(this,"NoteTag1",`
  #### \u5E2E\u52A9
   - \u70B9\u51FB\u5173\u952E\u5B57\u6267\u884C\u641C\u7D22\u8FD4\u56DE\u7684\u6570\u636E\u6E90\uFF0C@Key\u662F\u5173\u952E\u5B57,\u662F\u641C\u7D22\u7684\u5173\u952E\u5B57.
   - For URL:/DataUser/Handler.ashx?DoType=SearchEmps&Keyword=@Key
   - For SQL: SELECT No,Name FROM Port_Emp WHERE No like '%@Key%' OR Name like '%@Key%'
  `);E(this,"NoteTag2",`
  #### \u5E2E\u52A9
   - \u8BBE\u7F6E\u4E00\u4E2A\u53EF\u4EE5\u8FD4\u56DEjson\u7684\u6570\u636E\u6E90\u8BE5\u6570\u636E\u6E90\u6709No,Name,ParentNo\u4E09\u4E2A\u7EA6\u5B9A\u7684\u5217.
   - For URL:/DataUser/Handler.ashx?DoType=ReqDepts
   - For SQL:SELECT No,Name, ParentNo FROM Port_Dept
  `);E(this,"NoteDoc",`
  #### \u5E2E\u52A9
   - \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F,\u6BD4\u5982:@WebUser.DeptNo , @FieldName ,@WebUser.OrgNo 
  `);E(this,"NoteTag3",`
  #### \u5E2E\u52A9
   - \u9009\u62E9\u53F3\u8FB9\u7684\u6811\u8FD4\u56DE\u7684\u8BE6\u7EC6\u4FE1\u606F\u5217\u8868\u6570\u636E\u6E90 \uFF0C @Key\u662F\u5173\u952E\u5B57,\u662F\u9009\u62E9\u7684\u6811\u8282\u70B9\u7F16\u53F7.
   - For URL:/DataUser/Handler.ashx?DoType=ReqEmpsByDeptNo&DeptNo=@Key
   - For SQL:SELECT No,Name FROM Port_Emp WHERE FK_Dept='@Key'
  `);E(this,"NoteTag",`
  #### \u5E2E\u52A9
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5728\u53F3\u4E0A\u89D2\u7684\u5217\u8868\u91CC\u67E5\u8BE2\u6216\u70B9\u6811\u6811\u5E72\u7684\u6570\u636E\u6E90\u51FA\u73B0\u7684\u5217\u8868,\u9700\u8981\u7528\u4E2D\u6587\u663E\u793A\u5217\u5934.
   - \u4F8B\u5982: No=\u7F16\u53F7,Name=\u540D\u79F0,Addr=\u5730\u5740,Tel=\u7535\u8BDD,Email=\u90AE\u4EF6
  `);E(this,"NoteTag5",`
  #### \u5E2E\u52A9
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5F39\u51FA\u6846\u786E\u5B9A\u540E\u6267\u884C\u7684JS\uFF0C\u53EF\u4EE5\u76F4\u63A5\u5199\u65B9\u6CD5\u540D\u6216\u8005\u65B9\u6CD5. 
  `);u&&(this.MyPK=u)}get HisUAC(){const u=new a;return u.IsUpdate=!0,u}get EnMap(){const u=new B("Sys_MapExt","\u6811\u5E72\u53F6\u5B50\u5F39\u7A97");return u.AddGroupAttr("\u6570\u636E\u6765\u6E90"),u.AddMyPK(),u.AddTBString(e.FK_MapData,null,"\u8868\u5355ID",!1,!1,0,50,200),u.AddTBString(e.ExtModel,"Pop","\u6A21\u5F0F(\u5927\u7C7B)",!1,!1,0,50,200),u.AddTBString(e.ExtType,null,"\u7C7B\u578B(\u5C0F\u7C7B)",!1,!1,0,50,200),u.AddDDLEntities(e.FK_DBSrc,"local","\u6570\u636E\u6E90",new l,!0,null,!1),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.NoteSearchTip),u.AddTBStringDoc(e.Tag1,null,"\u641C\u7D22\u6570\u636E\u6E90",!0,!1,!0,this.NoteTag1),u.AddTBStringDoc(e.Tag2,null,"\u5DE6\u4FA7\u6811\u5217\u8868\u6570\u636E\u6E90",!0,!1,!0,this.NoteTag2),u.AddTBString(e.Doc,null,"\u6839\u8282\u70B9\u6811\u7F16\u53F7",!0,!1,0,50,200,!0,this.NotDoc),u.AddTBStringDoc(e.Tag3,null,"\u5B9E\u4F53\u6570\u636E\u6E90",!0,!1,!0,this.NoteTag3),u.AddTBString(e.Tag,null,"\u6570\u636E\u5217\u540D\u4E0E\u4E2D\u6587\u610F\u601D\u5BF9\u7167",!0,!1,0,50,200,!0,this.NoteTag),u.AddTBString(e.Tag5,null,"\u786E\u5B9A\u540E\u6267\u884C\u7684JS",!0,!1,0,50,200,!0,this.NoteTag5),u.AddGroupAttr("\u5916\u89C2"),u.AddRadioBtn("ShowModel",0,"\u5C55\u793A\u65B9\u5F0F",!0,!1,"ShowModel","@0=POP\u5F39\u51FA\u7A97@1=\u4E0B\u62C9\u641C\u7D22\u9009\u62E9",null,!0),u.AddRadioBtn("PopSelectType",1,"\u9009\u62E9\u7C7B\u578B",!0,!0,"PopSelectType","@0=\u5355\u9009@1=\u591A\u9009",null,!0),u.AddTBString("Title",null,"\u6807\u9898",!0,!1,0,50,200,!0),u.AddTBString("BtnLab","\u67E5\u627E","\u67E5\u627E\u6309\u94AE\u6807\u7B7E",!0,!1,0,50,200),u.AddTBInt(e.H,400,"\u5F39\u7A97\u9AD8\u5EA6",!0,!1),u.AddTBInt(e.W,500,"\u5F39\u7A97\u5BBD\u5EA6",!0,!1),u.AddTBAtParas(4e3),u.ParaFields=",Title,BtnLab,SearchTip,ShowModel,PopSelectType,",this._enMap=u,this._enMap}}export{O as PopTreeEns};
