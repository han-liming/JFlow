var cu=Object.defineProperty;var mu=(o,u,e)=>u in o?cu(o,u,{enumerable:!0,configurable:!0,writable:!0,value:e}):o[u]=e;var E=(o,u,e)=>(mu(o,typeof u!="symbol"?u+"":u,e),e);var s=(o,u,e)=>new Promise((t,F)=>{var D=S=>{try{c(e.next(S))}catch(m){F(m)}},p=S=>{try{c(e.throw(S))}catch(m){F(m)}},c=S=>S.done?t(S.value):Promise.resolve(S.value).then(D,p);c((e=e.apply(o,u)).next())});import{UAC as a}from"./UAC-8e255d47.js";import{Map as d}from"./Map-73575e6b.js";import{EntityNodeID as l,EntitiesNodeID as Su}from"./EntityNodeID-d5ae71b1.js";import{Node as f,NodeAttr as B}from"./Node-6b42ba5e.js";import{BtnAttr as O,BtnLab as hu}from"./BtnLab-46145c97.js";import{G as n,D as yu}from"./DataType-33901a1c.js";import{FormSlnType as h}from"./EnumLab-9ed99ea3.js";import{$ as X,Y as V}from"./index-f4658ae7.js";import{FrmNode as fu,FrmNodeAttr as Y}from"./FrmNode-4376932d.js";import{RefMethodType as H,RefMethod as k}from"./RefMethod-33a71db4.js";import{MapData as Nu}from"./MapData-4fa397be.js";import{PageBaseGroupEdit as N}from"./PageBaseGroupEdit-202e8e85.js";import{GPNReturnObj as _,GPNReturnType as I}from"./PageBaseGroupNew-ee20c033.js";import{GloComm as g}from"./GloComm-7cfbdfd9.js";import{DeliveryWay as A}from"./DeliveryWay-8ff1107a.js";import{GPE_ShenFenModel as j}from"./GPE_ShenFenModel-c62f15cb.js";import{NodeStations as P,NodeStation as R}from"./NodeStation-c4e78147.js";import{NodeEmps as uu,NodeEmp as eu}from"./NodeEmp-be38ef1e.js";import{NodeDepts as z,NodeDept as Q}from"./NodeDept-9cc0d31c.js";import{b as _u}from"./MapExt-db8cd7f3.js";import{ARBindWebApi as Iu}from"./ARBindWebApi-a291a7d1.js";import{SystemConfig as gu}from"./SystemConfig-b93c25b3.js";import{GPE_TurnTo as wu}from"./GPE_TurnTo-19ffccf6.js";import{GPE_TodolistModel as Tu}from"./GPE_TodolistModel-aee1caa6.js";import{NodeToolbars as Pu,NodeToolbarAttr as Ru}from"./NodeToolbar-67595d83.js";import{SubFlows as Mu,SubFlowAttr as bu}from"./SubFlow-ddccebaa.js";import{GPE_OvertimeRole as Wu}from"./GPE_OvertimeRole-ced14cc8.js";import{FrmNodeExts as Lu}from"./FrmNodeExt-7793b2aa.js";import{FrmNodeBatchs as Ku}from"./FrmNodeBatch-93b1b7e2.js";import{GPE_FrmTransfer as Ou}from"./GPE_FrmTransfer-895ecb95.js";import{SysEvents as Uu}from"./SysEvent-83a4fc0b.js";import{PushMsgs as Gu,PushMsgAttr as Z}from"./PushMsg-3c61f704.js";import{GPE_FrmTrack as vu}from"./GPE_FrmTrack-ec5abd7c.js";import{GPE_BlockModel as xu}from"./GPE_BlockModel-93e72240.js";import{GPE_CCWriteRole as Hu}from"./GPE_CCWriteRole-5647caf9.js";class Eu extends l{constructor(u){super("TS.AttrNode.Sln11"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u5355\u8868\u5355(\u7ED1\u5B9A\u72EC\u7ACB\u8868\u5355)");u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u8282\u70B9\u540D\u79F0",!0,!0,0,50,200),u.AddTBString(r.FK_Flow,null,"FK_Flow",!1,!1,0,50,200),u.AddTBString(r.NodeFrmID,null,"\u8868\u5355ID",!0,!1,0,50,200),u.SetPopGroupList(r.NodeFrmID,n.srcFrmTree,n.srcFrmList,!1),u.AddGroupMethod("\u7ED1\u5B9A\u5355\u8868\u5355");const e="/#/WF/Comm/En?EnName=TS.AttrNode.FrmNodeExt&PKVal=@NodeFrmID_@FrmID_@FK_Flow";u.AddRM_UrlTabOpen("\u8282\u70B9\u8868\u5355\u5C5E\u6027",e,"icon-drop");const t=new k;return t.Title="\u8BBE\u7F6E\u6240\u6709\u8282\u70B9\u90FD\u91C7\u7528\u6B64\u65B9\u6848",t.ClassMethod="DoSetIt",t.RefMethodType=H.Func,u.AddRefMethod(t),this._enMap=u,this._enMap}beforeUpdateInsertAction(){return s(this,null,function*(){try{const u=new fu;u.MyPK=this.NodeFrmID+"_"+this.NodeID+"__"+this.FK_Flow;const e=yield u.RetrieveFromDBSources();if(u.FK_Node=this.NodeID,u.FK_Frm=this.NodeFrmID,u.FK_Flow=this.FK_Flow,u.SetPara("EnName","TS.AttrNode.FrmNodeExt"),e==0){const t=new Nu(this.NodeFrmID);return yield t.RetrieveFromDBSources(),u.FrmNameShow=t.Name,yield u.Insert(),!0}return yield u.Update(),!0}catch(u){return alert(u),!1}})}DoFrmAttr(){return this.NodeFrmID==""||this.NodeFrmID==null?"err@\u9519\u8BEF,\u8BF7\u5148\u8BBE\u7F6E\u8868\u5355ID.":"url@/src/WF/Comm/En.vue?EnName=TS.AttrNode.FrmNodeExt&PKVal="+this.NodeFrmID+"_"+this.NodeFrmID+"_"+this.FK_Flow}DoSetIt(){return s(this,null,function*(){if(this.NodeFrmID==""||this.NodeFrmID==null)throw new Error("err@\u9519\u8BEF,\u8BF7\u5148\u8BBE\u7F6E\u8868\u5355ID.");const u=new X("BP.WF.HttpHandler.WF_Admin_AttrNode_FrmSln");return u.AddPara("FK_Node",this.NodeID),yield u.DoMethodReturnString("RefOneFrmTree_SetAllNodeFrmUseThisSln")})}}const I5=Object.freeze(Object.defineProperty({__proto__:null,Sln11:Eu},Symbol.toStringTag,{value:"Module"}));class U extends l{constructor(u){super("TS.WF.SelfFormEn","BP.WF.Template.NodeExt"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","URL\u53C2\u6570");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u540D\u79F0",!1,!1,0,50,200),u.AddTBString(r.FormUrl,null,"\u6587\u4EF6URL",!0,!1,0,50,200,!0),u.AddDDLSysEnum("SelfFormEnRoot",0,"\u6587\u4EF6\u6839\u8DEF\u5F84",!0,!0,"SelfFormEnRoot","@0=\u672C\u673A\u6587\u4EF6@1=\u5168\u8DEF\u5F84@2=\u5B50\u7CFB\u7EDF\u8BBE\u7F6E@3=\u5168\u5C40\u914D\u7F6E\u6587\u4EF6(.env)@4=\u7CFB\u7EDF\u53C2\u6570FrmUrl"),u.ParaFields=",SelfFormEnRoot,",u.AddTBAtParas(500),this._enMap=u,this._enMap}}const g5=Object.freeze(Object.defineProperty({__proto__:null,SelfFormEn:U},Symbol.toStringTag,{value:"Module"}));class Fu extends N{constructor(){super("GPE_FrmSln");E(this,"FoolForm",`

  #### \u5E2E\u52A9
   - \u89C6\u9891\u6559\u7A0B: https://drive.weixin.qq.com/s?k=AOsAZQczAAY9NtLjEw
   - \u8BBE\u8BA1\u65B9\u4FBF\uFF0C\u754C\u9762\u7B80\u6D01\u6E05\u6670\u3002
   - \u5B57\u6BB5\u7684\u987A\u5E8F\u53EF\u4EE5\u901A\u8FC7\u62D6\u62FD\u5B9E\u73B0\u79FB\u52A8,\u901A\u8FC7\u6805\u680F\u683C\u6765\u5E03\u5C40\u754C\u9762\u5143\u7D20\u3002
   - \u53EF\u4EE5\u901A\u8FC7\u5B9A\u4E49\u6587\u672C\u5C5E\u6027\u6765\u4F53\u73B0\u4E0D\u540C\u63A7\u4EF6\u7684\u5C55\u793A\u8981\u6C42\uFF08\u6587\u672C\uFF0C\u5355\u9009\uFF0C\u591A\u9009\uFF0C\u5B9A\u4F4D\uFF0C\u8BC4\u5206\uFF0C\u591A\u9644\u4EF6\uFF0C\u5730\u56FE\uFF0C\u8EAB\u4EFD\u8BC1\u8BC6\u522B\u7B49\uFF09\u6EE1\u8DB3\u8868\u5355\u8981\u6C42\u3002
 
  #### \u56FE\u4F8B1
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FoolFrmD.png "\u5C4F\u5E55\u622A\u56FE.png")
   
  #### \u56FE\u4F8B2
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FoolFrmD2.png "\u5C4F\u5E55\u622A\u56FE.png")
 
  #### \u8868\u5355\u6A21\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/shagua1.png "\u5C4F\u5E55\u622A\u56FE.png")
   
  `);E(this,"Developer",`
  #### \u5E2E\u52A9
   - \u652F\u6301\u7F16\u5199js,Html\u6A21\u5F0F\uFF0C\u4F7F\u7528\u5BCC\u6587\u672C\u7F16\u8F91\u5668\u5F00\u53D1.
  #### \u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/kaifaze.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);E(this,"RefNodeFrm",`
  #### \u5E2E\u52A9
   - \u652F\u6301\u7F16\u5199js,Html\u6A21\u5F0F\uFF0C\u4F7F\u7528\u5BCC\u6587\u672C\u7F16\u8F91\u5668\u5F00\u53D1.
  #### \u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/kaifaze.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);E(this,"FoolTruck",`
  #### \u5E2E\u52A9
   - \u8BE5\u8868\u5355\u662F\u4EE5\u7ECF\u5178\u8868\u5355\u5C55\u793A\uFF0C\u4E5F\u53EB\u7D2F\u52A0\u8868\u5355\uFF0C\u4E5F\u53EB\u8F68\u8FF9\u8868\u5355\u3002
   - \u6BCF\u4E2A\u8282\u70B9\u4E0A\u90FD\u6709\u81EA\u5DF1\u7684\u4E00\u4E2A\u5B57\u6BB5\u96C6\u5408, \u5F53\u524D\u8282\u70B9\u7684\u8868\u5355\u662F\u4EE5\u524D\u8868\u5355\u6309\u7167\u8D70\u8FC7\u7684\u987A\u5E8F\u7D2F\u52A0\uFF08\u9ED8\u8BA4\u5B57\u6BB5\u53EA\u8BFB\uFF09\u52A0\u4E0A\u5F53\u524D\u8282\u70B9\u8868\u5355\u62FC\u63A5\u800C\u6210\u7684\u3002
   - \u8BE5\u8868\u5355\u8BBE\u8BA1\u7B80\u5355\uFF0C\u601D\u8DEF\u6E05\u6670\uFF0C\u9002\u7528\u4E8E\u5728\u5BA1\u6838\u7EC4\u4EF6\u6A21\u5F0F\u4E0B\u4E0D\u80FD\u89E3\u51B3\u7684\u5BA1\u6838\u8282\u70B9\u7279\u6B8A\u5B57\u6BB5\u6570\u636E\u7684\u91C7\u96C6\u3002
   - \u8BE5\u8868\u5355\u662F\u5BA1\u6838\u7EC4\u4EF6\u6A21\u5F0F\u8868\u5355\u7684\u6709\u76CA\u8865\u5145\u3002
  `);E(this,"SelfForm",`
  #### \u5B9A\u4E49
 - \u5D4C\u5165\u6A21\u5F0F\u7684\u8868\u5355\uFF0C\u5C31\u662F\u5F00\u53D1\u8005\u81EA\u5B9A\u4E49\u4E2A\u4E00\u4E2A\u8868\u5355\uFF0C\u7136\u540E\u628A\u8868\u5355\u7684\u5730\u5740\u7ED1\u5B9A\u5230\u8282\u70B9\u5C5E\u6027\u7684\u4E00\u79CD\u505A\u6CD5\u3002
 - \u8BE5\u6A21\u5F0F\u4E0B\uFF0C\u7CFB\u7EDF\u5728\u8FD0\u884C\u7684\u8FC7\u7A0B\u4E2D\uFF0C\u5934\u90E8\u4F7F\u7528\u5DE5\u5177\u680F\uFF0C\u5C3E\u90E8\u4F7F\u7528\u5BA1\u6838\u7EC4\u4EF6\u3002
 - \u5F00\u53D1\u8005\u81EA\u5DF1\u5B9A\u4E49\u7684\u9875\u9762\u6709\u4E00\u4E2ASave() \u7684 function \uFF0C\u5F53\u7528\u6237\u70B9\u51FB\u6846\u67B6\u5916\u9762\u7684\u5DE5\u5177\u680F\u4E0A\u7684\u3010\u4FDD\u5B58\u3011\u6309\u94AE\u6216\u8005\u3010\u53D1\u9001\u3011\u6309\u94AE\uFF0C\u5C31\u4F1A\u89E6\u53D1\u8FD9\u4E2A\u51FD\u6570\u3002
 - \u60A8\u9700\u8981\u5728Save()\u7684function\u91CC\u5B8C\u6210\u6570\u636E\u5B8C\u6574\u6027\u6548\u9A8C\u4E0E\u6570\u636E\u4FDD\u5B58\uFF0C\u6216\u8005\u8C03\u7528ccbpm\u7684\u63A5\u53E3\u628A\u53C2\u6570\u4F20\u5165\u5230\u6D41\u7A0B\u5F15\u64CE\u91CC\u9762\u53BB.
 - \u8BE5\u65B9\u6CD5: \u5982\u679C\u4FDD\u5B58\u6210\u529F\u5C31return true, \u4FDD\u5B58\u5931\u8D25\u5C31return false. \u7528\u4E8E\u6821\u9A8C\u6570\u636E\u586B\u5199\u7684\u5B8C\u6574\u6027. \u6BD4\u5982:\u5F53\u7528\u6237\u6267\u884C\u53D1\u9001\u7684\u65F6\u5019\uFF0C\u9996\u5148\u6267\u884C\u4FDD\u5B58\uFF0C\u4FDD\u5B58\u6210\u529F\u540E\u5728\u6267\u884C\u53D1\u9001\uFF0C\u4FDD\u5B58\u5931\u8D25\u540E\uFF0C\u5C31\u963B\u6B62\u53D1\u9001\u3002
 - \u60A8\u8F93\u5165\u7684Url\u53EF\u4EE5\u6709\u53C2\u6570\uFF0C\u4F46\u662F\u7CFB\u7EDF\u4F1A\u628A\u6240\u6709\u7684\u53C2\u6570\u9644\u4EF6\u5230\u8BE5url\u540E\u9762\u3002
 - \u4F8B\u5982:/SDKFlowDemo/QingJia/SDKQianRuFangShiForm.htm(\u4E5F\u53EF\u80FD\u662Fvue\u7684\u8DEF\u7531\u9875\u9762.\u4F8B\u5982\uFF1Ahttp://localhost:3000/#/QingJia)
 - \u6BD4\u5982:\u60A8\u914D\u7F6E\u7684url\u4E3A http://xxxx:222:/abc.htm \u7CFB\u7EDF\u5B9E\u9645\u7684Url\u4E3A http://xxxx:222:/abc.htm?FK_Flow=xxx&FK_Node=xxx&WorkID=xxx&UserNo=xxx&Token=xxx
 - \u7CFB\u7EDF\u4F1A\u628A\u5F53\u524D\u6D41\u7A0B\u73AF\u5883\u4E2D\u7684\u53D8\u91CF\u4E0E\u53C2\u6570\u90FD\u4F20\u9012\u5230\u60A8\u7684\u81EA\u5B9A\u4E49\u9875\u9762\u4E0A\u6765\uFF0C\u60A8\u53EF\u4EE5\u6839\u636E\u8FD9\u4E9B\u53C2\u6570\u6765\u5C55\u793A\uFF0C\u4FDD\u5B58\u6570\u636E\uFF0C\u63A7\u5236\u6570\u636E\u53EA\u8BFB\uFF0C\u53EF\u7F16\u8F91\u3002
 #### \u5DE5\u4F5C\u793A\u610F\u56FE
 - \u5934\u90E8\uFF1A\u5DE5\u5177\u680F, \u5C3E\u90E8:\u5BA1\u6838\u7EC4\u4EF6, \u4E2D\u95F4\u90E8\u95E8\u81EA\u5B9A\u4E49url.
 - \u56FE\uFF1A
 ![\u5D4C\u5165\u6A21\u5F0F\u8868\u5355](/resource/WF/Admin/AttrNode/FrmSln/SelfFrm/SelfFrm.png "\u5D4C\u5165\u6A21\u5F0F\u8868\u5355.")
 - \u8BE5\u6A21\u5F0F\u5145\u5206\u5229\u7528ccbpm\u63D0\u4F9B\u7684toolbar\u529F\u80FD, \u542F\u7528\u7981\u7528\u6D41\u7A0B\u529F\u80FD\u6309\u94AE\uFF0C\u5728\u8282\u70B9\u5C5E\u6027\u91CC\u8BBE\u7F6E. 
 - \u5BA1\u6838\u7EC4\u4EF6\u7684\u7981\u7528\u542F\u7528\uFF0C\u5728\u8868\u5355\u5C5E\u6027=\u300B\u8868\u5355=\u300B\u8BBE\u7F6E\uFF0C\u6216\u8005\u5728\u6D41\u7A0B\u5C5E\u6027\u91CC\u6279\u91CF\u8BBE\u7F6E\u3002

 #### Url\u8DDF\u76EE\u5F55\u5B9A\u4E49
 - \u672C\u673A\u6587\u4EF6: \u5728ccbpm\u524D\u7AEF\u76EE\u5F55\u91CC\u5F00\u53D1\u7684\u8868\u5355\u6587\u4EF6.
 - \u5168\u8DEF\u5F84: \u6BD4\u5982\uFF1Ahttp://xxx.ccbpm.cn/xx.htm
 - \u5B50\u7CFB\u7EDF\u8BBE\u7F6E: \u5728\u6D41\u7A0B\u6A21\u677F\u76EE\u5F55\u4E0A\u8BBE\u7F6E\u7684\u8DEF\u5F84,\u5728\u6D41\u7A0B\u76EE\u5F55\u4E0A\u70B9\u5C5E\u6027\u8BBE\u7F6E\u8DEF\u5F84.
 - \u56FE\uFF1A
 ![\u5D4C\u5165\u6A21\u5F0F\u8868\u5355](/resource/WF/Admin/AttrNode/FrmSln/SelfFrm/systemSetting.png "\u8DEF\u5F84\u8BBE\u7F6E.")
 - ccbpm \u4F1A\u8BFB\u53D6\u7B2C\u4E00\u4E2A\u8BBE\u7F6E\u4F5C\u4E3A\u8868\u5355\u7684\u6839\u76EE\u5F55.
 - \u5168\u5C40\u914D\u7F6E\u6587\u4EF6(.env):  \u914D\u7F6E\u5728.env \u7684http\u8DEF\u5F84\u53D8\u91CF.  VITE_GLOB_API_URL
 - \u7CFB\u7EDF\u53C2\u6570(PC\u7AEFFrmUrl,\u79FB\u52A8\u7AEF:MobileFrmUrl) \u542F\u52A8\u6216\u8005\u8FD0\u884C\u6D41\u7A0B\u7684\u8FC7\u7A0B\u4E2D\u901A\u8FC7\u8C03\u7528api Flow_SaveParas() \u65B9\u6CD5\u5199\u5165\u7684\u53C2\u6570\u7684Url, ccbpm\u5C31\u83B7\u53D6\u8FD9\u4E2A\u53C2\u6570\u4F5C\u4E3Aurl\u7684\u8DEF\u5F84.

 #### \u5982\u4F55\u5411\u6D41\u7A0B\u5F15\u64CE\u5199\u5165\u53C2\u6570?
 - \u5728\u6D41\u7A0B\u8FD0\u884C\u7684\u8FC7\u7A0B\u4E2D,\u9700\u8981\u5411\u6D41\u7A0B\u5F15\u64CE\u5199\u5165\u53C2\u6570,\u7528\u4E8E\u63A7\u5236:\u6D41\u7A0B\u65B9\u5411\u8F6C\u5411\u3001\u4EE5\u53CA\u63A5\u53D7\u4EBA\u7B49.
 - ccbpm\u63D0\u4F9B\u4E24\u79CD\u65B9\u6CD5:
 1. \u5728\u4FDD\u5B58\u65B9\u6CD5\u91CC\u8C03\u7528API\u63A5\u53E3.
 2. \u5728\u4FDD\u5B58\u65B9\u6CD5\u91CC\uFF0C\u8FD4\u56DE\u6307\u5B9A\u7684\u683C\u5F0F\u7684json: 
  
  `);E(this,"SDKFormSmart",`
  #### \u5E2E\u52A9
  1. \u5982\u679C\u8981\u5728\u60A8\u7684\u4E1A\u52A1\u8868\u5355\u4E0A\u8DD1\u6D41\u7A0B\uFF0C\u4EC5\u4EC5\u8981\u505A\u7684\u662F\u628A SmartSDKFrm.js \u653E\u5165\u5230\u60A8\u7684\u9875\u9762\u91CC\u9762\u3002
  1. \u7CFB\u7EDF\u5C31\u4F1A\u81EA\u52A8\u751F\u6210\u6D41\u7A0B\u5F15\u64CE\u7684\u63A7\u5236toolbar\uFF0C toolbar \u7684\u6309\u94AE\u6743\u9650\u5728\u8282\u70B9\u5C5E\u6027\u91CC\u76F4\u63A5\u63A7\u5236\u3002
  1. \u60A8\u53EF\u4EE5\u5145\u5206\u5229\u7528ccbpm\u7684\u5F88\u591A\u7EC4\u4EF6\u529F\u80FD\uFF0C\u6BD4\u5982\uFF1A\u5B9A\u4F4D\u3001\u5730\u56FE\u3001\u62CD\u7167\u3001\u9644\u4EF6\u3001\u8BC4\u8BBA\u3001\u5199\u5B57\u677F\u3001\u8D85\u94FE\u63A5\u7EC4\u4EF6\u7B49\u7B49\u3002
  `);E(this,"SDKForm",`

  #### \u5E2E\u52A9
  1. SDK\u8868\u5355\u5C31\u662Fccbpm\u628A\u754C\u9762\u7684\u5C55\u73B0\u5B8C\u5168\u4EA4\u7ED9\u4E86\u5F00\u53D1\u4EBA\u5458\u5904\u7406,\u5F00\u53D1\u4EBA\u5458\u53EA\u8981\u8BBE\u8BA1\u4E00\u4E2A\u8868\u5355,\u589E\u52A0\u4E00\u4E2A\u53D1\u9001\u6309\u94AE,\u8C03\u7528ccbpm\u7684\u53D1\u9001API\u5C31\u53EF\u4EE5\u5B8C\u6210\u3002
  1. \u5982\u679C\u4F7F\u7528\u7EDD\u5BF9\u8DEF\u5F84\u53EF\u4EE5\u4F7F\u7528ccbpm\u7684\u5168\u5C40\u53D8\u91CF@SDKFromServHost \uFF0C\u6BD4\u5982: @SDKFromServHost/MyFile.htm
  1. \u4F8B\u5982:/SDKFlowDemo/QingJia/S1_TianxieShenqingDan.jsp , /SDKFlowDemo/QingJia/S1_TianxieShenqingDan.htm
  1. ccbpm\u56E2\u961F\u4E3A\u60A8\u63D0\u4F9B\u4E86\u4E00\u4E2Ademo\u6D41\u7A0B \\\u6D41\u7A0B\u6811\\SDK\u6D41\u7A0B\\ \u8BE5\u76EE\u5F55\u4E0B\u6709\u5F88\u591ASDK\u6A21\u5F0F\u7684\u6D41\u7A0B\u4F9B\u60A8\u53C2\u8003\u3002
  #### \u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/SDKFrm.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);E(this,"RefOneFrmTree",`
  #### \u5E2E\u52A9
   - \u8868\u5355\u5E93\u91CC\u9009\u62E9\u4E00\u4E2A\u8868\u5355\u7ED1\u5B9A\u5230\u5F53\u524D\u8282\u70B9\u4E0A,\u8BE5\u6A21\u5F0F\u6211\u4EEC\u4E5F\u79F0\u4E3A\u7ED1\u5B9A\u72EC\u7ACB\u8868\u5355.
   - \u4E00\u4E2A\u8868\u5355\u53EF\u4EE5\u88AB\u591A\u4E2A\u6D41\u7A0B\u6A21\u677F\u7ED1\u5B9A\uFF0C\u8FD9\u4E2A\u8868\u5355\u6A21\u677F\u5C31\u53EF\u4EE5\u91CD\u7528. 
   - \u6BD4\u5982: \u96C6\u56E2IT\u90E8\u95E8\u53D1\u5E03\u4E00\u4E2A\u8BF7\u5047\u8868\u5355\uFF0C\u4E00\u4E2A\u516C\u6587\u683C\u5F0F\u8868\u5355\uFF0C\u5404\u4E2A\u5B50\u516C\u53F8\u90FD\u53EF\u4EE5\u4F7F\u7528\u3002
   - \u6211\u4EEC\u53EF\u4EE5\u5728\u6BCF\u4E2A\u8282\u70B9\u4E0A\u901A\u8FC7\u8282\u70B9\u4E0E\u8868\u5355\u7684\u5173\u7CFB\u8BBE\u7F6E\uFF0C\u5B57\u6BB5\u7684\u53EF\u89C1\uFF0C\u53EF\u7528\uFF0C\u53EA\u8BFB\u7B49\u64CD\u4F5C\uFF0C\u6EE1\u8DB3\u4E0D\u540C\u7684\u573A\u666F\u4F7F\u7528\u3002
   - \u8BE5\u8868\u5355\u7684\u5DE5\u4F5C\u65B9\u5F0F\u4E0E\u5185\u7F6E\u8868\u5355\u5DE5\u4F5C\u6A21\u5F0F\u4E00\u81F4, \u4E0A\u9762\u662F\u5DE5\u5177\u680F\u4E0B\u9762\u662F\u8868\u5355.

  #### \u8BBE\u7F6E\u6240\u6709\u7684\u8282\u70B9\u90FD\u91C7\u7528\u6B64\u8868\u5355
   - \u9A70\u9A8B\u5DE5\u4F5C\u6D41\u4ECE\u7406\u8BBA\u4E0A\u6765\u8BF4\uFF0C\u6CA1\u4E2A\u8282\u70B9\u90FD\u53EF\u4EE5\u7ED1\u5B9A\u4E0D\u540C\u7684\u5355\u4E2A\u8868\u5355\uFF0C\u4F46\u662F\u5B9E\u8DF5\u7684\u573A\u666F\u662F\u4E00\u4E2A\u6D41\u7A0B\u6A21\u677F\uFF0C\u901A\u5E38\u7ED1\u5B9A\u4E00\u4E2A\u8868\u5355\u3002
   - \u7CFB\u7EDF\u63D0\u4F9B\u8FD9\u4E2A\u529F\u80FD\uFF0C\u65B9\u4FBF\u8BBE\u8BA1\u4EBA\u5458\uFF0C\u4E00\u6B21\u6027\u5C31\u53EF\u4EE5\u7ED1\u5B9A\u8868\u5355.
   - \u6211\u4EEC\u901A\u8FC7\u8282\u70B9\u4E0E\u8868\u5355\u7684\u8BBE\u8BA1\u5B9E\u73B0\u8BE5\u8868\u5355\u5DE5\u4F5C\u573A\u666F\uFF0C\u5982\u4E0B\u8BF4\u660E.
  #### \u8282\u70B9\u4E0E\u8868\u5355\u7684\u5173\u7CFB
   - 

  #### \u5DE5\u4F5C\u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/ziyou.png")
 
  `);E(this,"SheetTree",`

  #### \u5E2E\u52A9
   - \u5F53\u4E00\u4E2A\u8282\u70B9\u9700\u8981\u7ED1\u5B9A\u591A\u4E2A\u8868\u5355\u7684\u65F6\u5019\uFF0C\u6211\u4EEC\u628A\u8FD9\u6837\u7684\u8282\u70B9\u79F0\u4E3A\u591A\u8868\u5355\u8282\u70B9\uFF0C\u6216\u8005\u8868\u5355\u6811\u8282\u70B9\u3002
   - \u6BD4\u5982: \u9879\u76EE\u7533\u62A5\u6D41\u7A0B\uFF0C\u63D0\u62A5\u9879\u76EE\u7684\u65F6\u5019\uFF08\u5F00\u59CB\u8282\u70B9\uFF09\u9700\u8981\u63D0\u4EA4\uFF0C\u9879\u76EE\u57FA\u672C\u4FE1\u606F\u3001\u9879\u76EE\u73AF\u8BC4\u4FE1\u606F\u3001\u9879\u76EE\u98CE\u9669\u8BC4\u4F30\u3001\u9879\u76EE\u5B9E\u65BD\u8BA1\u5212\u4E66.
   #### \u9875\u9762\u5C55\u793A
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/duobiaodanzhanshi.png "\u5C4F\u5E55\u622A\u56FE.png")

   #### \u529F\u80FD\u8BF4\u660E
   - \u5982\u679C\u5728\u6BCF\u4E2A\u8282\u70B9\u4E0A\uFF0C\u7ED1\u5B9A\u4E0D\u540C\u7684\u8868\u5355\uFF0C\u5C31\u8981\u5728\u6BCF\u4E2A\u8282\u70B9\u4E0A\u5355\u72EC\u7ED1\u5B9A. \u6BD4\u5982: \u5F00\u59CB\u8282\u70B9\u7ED1\u5B9A3\u4E2A\u8868\u5355\uFF0C\u7B2C2\u4E2A\u8282\u70B9\u7ED1\u5B9A10\u4E2A\u8868\u5355.
   #### \u8868\u5355\u8282\u70B9\u5173\u7CFB
   - \u4E00\u4E2A\u8868\u5355\u53EF\u4EE5\u7ED1\u5B9A\u4E00\u4E2A\u6D41\u7A0B\u7684\u4E0D\u540C\u8282\u70B9\u4E0A\uFF0C\u6BCF\u4E2A\u8282\u70B9\u4E0E\u8FD9\u4E2A\u8868\u5355\uFF0C\u90FD\u6709\u4E00\u4E2A\u8BBE\u7F6E\u5173\u7CFB,\u6211\u4EEC\u79F0\u4E3A\u8868\u5355\u8282\u70B9\u5173\u7CFB.
   - \u5982\u4E0B\u56FE\uFF0C\u8282\u70B9\u7ED1\u5B9A\u8868\u5355
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/duobiaodanliebiao.png "\u5C4F\u5E55\u622A\u56FE.png")

   #### \u8868\u5355\u5C55\u73B0\u65B9\u5F0F
   - \u6211\u4EEC\u628A\u4E00\u4E2A\u8282\u70B9\u9700\u8981\u7ED1\u5B9A\u591A\u4E2A\u8868\u5355\u7684\u8282\u70B9\u79F0\u4E3A\u591A\u8868\u5355\u8282\u70B9\uFF0C\u5B83\u6709\u4E24\u79CD\u5C55\u73B0\u65B9\u5F0F\uFF0C\u6807\u7B7E\u9875\u4E0E\u8868\u5355\u6811\u3002
   - \u5BF9\u5E94\u7684\u6D41\u7A0Bdemo:\\\u6D41\u7A0B\u6811\\\u8868\u5355\u89E3\u51B3\u65B9\u6848\\\u6811\u5F62\u8868\u5355\u4E0E\u591A\u8868\u5355\u3002
  `);this.PageTitle="\u8868\u5355\u65B9\u6848",this.Btns=[{pageNo:"5",list:["\u8BBE\u7F6E"]}]}Init(){this.entity=new f,this.KeyOfEn=B.FormType,this.AddGroup("A","\u5185\u7F6E\u8868\u5355"),this.Blank(h.FoolForm,"\u7ECF\u5178\u8868\u5355(\u9ED8\u8BA4)",this.FoolForm),this.Blank(h.Developer,"\u5F00\u53D1\u8005\u8868\u5355",this.Developer),this.SelectItemsByList(h.RefNodeFrm,"\u5F15\u7528\u5176\u5B83\u8282\u70B9\u8868\u5355",this.RefNodeFrm,!1,n.sqlNodeFrmList,B.NodeFrmID),this.AddGroup("B","\u81EA\u5B9A\u4E49\u8868\u5355"),this.AddEntity(h.SelfForm,"\u5D4C\u5165\u5F0F\u8868\u5355",new U,this.SelfForm),this.AddEntity(h.SDKForm,"SDK\u8868\u5355",new U,this.SDKForm),this.AddGroup("C","\u7ED1\u5B9A\u8868\u5355\u5E93\u91CC\u7684\u8868\u5355"),this.AddEntity(h.RefOneFrmTree,"\u5355\u8868\u5355(\u7ED1\u5B9A\u72EC\u7ACB\u8868\u5355)",new Eu,this.RefOneFrmTree),this.Blank(h.SheetTree,"\u591A\u8868\u5355",this.SheetTree)}AfterSave(e,t){return s(this,null,function*(){if(e==h.FoolForm.toString()||t==h.Developer.toString()){const F=new f(this.RefPKVal);yield F.Retrieve(),F.NodeFrmID="",yield F.Update()}})}BtnClick(e,t,F){if(e=="5"){const D=g.UrlEn("TS.AttrNode.Sln5",this.entity.PKVal);return new _(I.OpenUrlByDrawer75,D,"\u8BBE\u7F6E")}throw new Error("\u65B9\u6CD5\u6682\u672A\u5B9E\u73B0")}}const w5=Object.freeze(Object.defineProperty({__proto__:null,GPE_FrmSln:Fu},Symbol.toStringTag,{value:"Module"}));class M extends l{constructor(e){super("TS.WF.AccepterRoleBindStation");E(this,"Help1",`
  1. \u5C97\u4F4D\u5207\u7247-\u4E25\u8C28\u6A21\u5F0F\uFF1A\u5982\u679C\u914D\u7F6E\u4E86\u4E25\u8C28\u6A21\u5F0F\uFF0C\u6548\u679C\u662F\u6709\u90E8\u5206\u4EBA\u5458\u7ED1\u5B9A\u4E86\u8FD9\u51E0\u4E2A\u5C97\u4F4D\uFF0C\u8FD9\u51E0\u4E2A\u5C97\u4F4D\u5FC5\u987B\u6BCF\u4E2A\u5C97\u4F4D\u4E0B\u90FD\u6709\u4EBA\u88AB\u7ED1\u5B9A\uFF0C\u5982\u679C\u6709\u4E00\u4E2A\u5C97\u4F4D\u4E0B\u6CA1\u67E5\u5230\u5BF9\u5E94\u5C97\u4F4D\u7684\u4EBA\u5458\uFF0C\u5C31\u7B97\u6240\u6709\u5C97\u4F4D\u6CA1\u67E5\u5230\u4EBA\u5458\u6570\u636E\u3002
  2. \u5C97\u4F4D\u5207\u7247-\u5BBD\u6CDB\u6A21\u5F0F\uFF1A\u5982\u679C\u914D\u7F6E\u4E86\u5BBD\u6CDB\u6A21\u5F0F\uFF0C\u6548\u679C\u662F\u6709\u90E8\u5206\u4EBA\u5458\u7ED1\u5B9A\u4E86\u8FD9\u51E0\u4E2A\u5C97\u4F4D\uFF0C\u4E0D\u6DF1\u7A76\u6BCF\u4E2A\u5C97\u4F4D\u662F\u5426\u6709\u4EBA\u5458\u88AB\u7ED1\u5B9A\uFF0C\u67E5\u627E\u8FD9\u51E0\u4E2A\u5C97\u4F4D\u4E0B\u4EBA\u5458\u6570\u636E\u7684\u5E76\u96C6\uFF0C\u6700\u7EC8\u67E5\u5230\u7684\u4EBA\u5458\u6570\u636E\u6700\u5C11\u4E00\u6761\u3002\u5982\u679C\u5728\u8FD9\u51E0\u4E2A\u5C97\u4F4D\u4E0B\u90FD\u6CA1\u6709\u4EBA\u5458\u88AB\u7ED1\u5B9A\uFF0C\u4E5F\u5C31\u662F\u6CA1\u6709\u67E5\u5230\u4EBA\u5458\uFF0C\u4F1A\u7ED9\u524D\u53F0\u4E00\u4E2A\u63D0\u793A\u3002

\u8FD9\u91CC\u7684\u5C97\u4F4D\u7B49\u540C\u4E8E\u89D2\u8272\uFF0C\u4E0D\u540C\u7684\u4E1A\u52A1\u573A\u666F\u53EB\u6CD5\u4E0D\u540C\uFF0C\u610F\u4E49\u4E00\u6837\u3002
  `);E(this,"Help2",`
  #### \u5E2E\u52A9 -\u5C97\u4F4D\u627E\u4EBA\u89C4\u5219.
  - \u5BFB\u627E\u4EBA\u7684\u8DEF\u5F84, \u5728\u6811\u7ED3\u6784\u91CC\u9762.
  #### \u5C97\u4F4D\u627E\u4EBA\u89C4\u5219 
  - \u9012\u5F52\u7236\u7EA7,\u7236\u7EA7\u5E73\u7EA7\u5BFB\u627E.
  - \u9012\u5F52\u7236\u7EA7\u5BFB\u627E.
 `);e&&(this.NodeID=e)}get HisUAC(){const e=new a;return e.IsDelete=!1,e.IsUpdate=!0,e.IsInsert=!1,e}get EnMap(){const e=new d("WF_Node","\u7ED1\u5B9A\u89D2\u8272");return e.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),e.AddTBInt("ShenFenModel",0,"\u8EAB\u4EFD\u89C4\u5219",!1,!1),e.AddTBString("NodeStations",null,"\u89D2\u8272",!0,!1,0,100,100,!0),e.SetPopGroupList("NodeStations",n.srcStationTypes,n.srcStations,!0,"300px","500px","\u9009\u62E9\u89D2\u8272","icon-people","1"),e.AddDDLSysEnum("StationReqEmpsWay",0,"\u5C97\u4F4D\u8BA1\u7B97\u65B9\u5F0F",!0,!0,"StationReqEmpsWay","@0=\u89D2\u8272\u96C6\u5408\u6A21\u5F0F@1=\u5C97\u4F4D\u5207\u7247-\u4E25\u8C28\u6A21\u5F0F@2=\u5C97\u4F4D\u5207\u7247-\u5BBD\u6CDB\u6A21\u5F0F",this.Help1,!1),e.AddDDLSysEnum("StationFindWay",0,"\u5C97\u4F4D\u627E\u4EBA\u89C4\u5219",!0,!0,"StationFindWay","@0=\u9012\u5F52\u7236\u7EA7,\u7236\u7EA7\u5E73\u7EA7\u5BFB\u627E@1=\u9012\u5F52\u7236\u7EA7\u5BFB\u627E",this.Help2,!1),e.AddTBAtParas(4e3),e.ParaFields=",ShenFenModel,StationReqEmpsWay,StationFindWay,",e.AddRM_GPE(new j,"icon-drop"),this._enMap=e,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new P().Delete("FK_Node",this.NodeID),typeof this.NodeStations!="string"||this.NodeStations.split(",").forEach(F=>s(this,null,function*(){const D=new R;D.FK_Node=this.NodeID,D.FK_Station=F,D.MyPK=this.NodeID+"_"+F,yield D.Insert()})),Promise.resolve(!0)})}}const T5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleBindStation:M},Symbol.toStringTag,{value:"Module"}));class b extends l{constructor(u){super("TS.WF.AccepterRoleBindEmp"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u7ED1\u5B9A\u4EBA\u5458");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("NodeEmps",null,"\u4EBA\u5458",!0,!1,0,1e3,100,!0),u.SetPopTreeEns("NodeEmps",n.srcDeptLazily,"@WebUser.DeptNo",n.srcEmpLazily,n.srcEmpSearchKey,!0,"300px","500px","\u9009\u62E9\u63A5\u6536\u4EBA","icon-people","0","1"),this._enMap=u,this._enMap}beforeUpdateInsertAction(){return s(this,null,function*(){return this.NodeEmps==null||this.NodeEmps===""?(V.config({top:"100px"}),V.error("\u8BF7\u9009\u62E9\u63A5\u53D7\u4EBA"),Promise.resolve(!1)):Promise.resolve(!0)})}afterUpdate(){return s(this,null,function*(){return yield new uu().Delete("FK_Node",this.NodeID),this.NodeEmps.split(",").forEach(t=>s(this,null,function*(){if(t){const F=new eu;F.FK_Node=this.NodeID,F.FK_Emp=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()}})),Promise.resolve(!0)})}}const P5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleBindEmp:b},Symbol.toStringTag,{value:"Module"}));class W extends l{constructor(u){super("TS.WF.AccepterRoleBindDeptStation"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u7ED1\u5B9A\u90E8\u95E8\u89D2\u8272");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("NodeEmps",null,"\u4EBA\u5458",!0,!1,0,1e3,100,!0),u.SetPopTreeEns("NodeEmps",n.srcDeptLazily,"@WebUser.DeptNo",n.srcEmpLazily,n.srcEmpSearchKey,!0,"300px","500px","\u9009\u62E9\u63A5\u6536\u4EBA","icon-people","0","1"),u.AddTBString("NodeDepts",null,"\u90E8\u95E8",!0,!1,0,1e3,100,!0),u.SetPopTree("NodeDepts",n.srcDepts,n.srcDeptRoot,!0,"300px","500px","\u9009\u62E9\u90E8\u95E8","icon-people","1"),u.AddTBString("NodeStations",null,"\u89D2\u8272",!0,!1,0,100,100,!0),u.SetPopGroupList("NodeStations",n.srcStationTypes,n.srcStations,!0,"300px","500px","\u9009\u62E9\u89D2\u8272","icon-people","1"),this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){let u=new uu;yield u.Delete("FK_Node",this.NodeID);let e=[];return typeof this.NodeEmps=="string"&&(this.NodeEmps.split(","),e.forEach(t=>s(this,null,function*(){const F=new eu;F.FK_Node=this.NodeID,F.FK_Emp=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()}))),u=new P,yield u.Delete("FK_Node",this.NodeID),typeof this.NodeStations=="string"&&(e=this.NodeStations.split(","),e.forEach(t=>s(this,null,function*(){const F=new R;F.FK_Node=this.NodeID,F.FK_Station=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()}))),u=new z,yield u.Delete("FK_Node",this.NodeID),typeof this.NodeDepts=="string"&&(e=this.NodeDepts.split(","),e.forEach(t=>s(this,null,function*(){const F=new Q;F.FK_Node=this.NodeID,F.FK_Dept=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()}))),Promise.resolve(!0)})}}const R5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleBindDeptStation:W},Symbol.toStringTag,{value:"Module"}));class w extends l{constructor(u){super("TS.WF.AccepterRoleBindDept"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u7ED1\u5B9A\u90E8\u95E8");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("NodeDepts",null,"\u90E8\u95E8",!0,!1,0,500,100,!0),u.SetPopTree("NodeDepts",n.srcDepts,n.srcDeptRoot,!0,"300px","500px","\u9009\u62E9\u90E8\u95E8","icon-people"),this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new z().Delete("FK_Node",this.NodeID),this.NodeDepts.split(",").forEach(t=>s(this,null,function*(){const F=new Q;F.FK_Node=this.NodeID,F.FK_Dept=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()})),Promise.resolve(!0)})}}const M5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleBindDept:w},Symbol.toStringTag,{value:"Module"}));class i extends B{}E(i,"NodeID","NodeID"),E(i,"SelectorModel","SelectorModel"),E(i,"SelectorP1","SelectorP1"),E(i,"SelectorP2","SelectorP2"),E(i,"SelectorP3","SelectorP3"),E(i,"SelectorP4","SelectorP4"),E(i,"FK_SQLTemplate","FK_SQLTemplate"),E(i,"IsAutoLoadEmps","IsAutoLoadEmps"),E(i,"IsSimpleSelector","IsSimpleSelector"),E(i,"IsEnableDeptRange","IsEnableDeptRange"),E(i,"IsEnableStaRange","IsEnableStaRange"),E(i,"Name","Name");class G extends l{constructor(u){super("TS.WF.SelecterFree"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u81EA\u7531\u9009\u62E9(\u6D77\u9009)");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID",!1),u.AddBoolean(i.IsAutoLoadEmps,!0,"\u662F\u5426\u81EA\u52A8\u52A0\u8F7D\u4E0A\u4E00\u6B21\u9009\u62E9\u7684\u4EBA\u5458\uFF1F",!0,!0,!0),u.AddBoolean(i.IsSimpleSelector,!1,"\u662F\u5426\u5355\u9879\u9009\u62E9(\u53EA\u80FD\u9009\u62E9\u4E00\u4E2A\u4EBA)\uFF1F",!0,!0,!0),u.AddBoolean(i.IsEnableDeptRange,!1,"\u662F\u5426\u542F\u7528\u90E8\u95E8\u641C\u7D22\u8303\u56F4\u9650\u5B9A(\u5BF9\u4F7F\u7528\u901A\u7528\u4EBA\u5458\u9009\u62E9\u5668\u6709\u6548)\uFF1F",!0,!0,!0),u.AddBoolean(i.IsEnableStaRange,!1,"\u662F\u5426\u542F\u7528\u89D2\u8272\u641C\u7D22\u8303\u56F4\u9650\u5B9A(\u5BF9\u4F7F\u7528\u901A\u7528\u4EBA\u5458\u9009\u62E9\u5668\u6709\u6548)\uFF1F",!0,!0,!0),this._enMap=u,this._enMap}}const b5=Object.freeze(Object.defineProperty({__proto__:null,SelecterAttr:i,SelecterFree:G},Symbol.toStringTag,{value:"Module"}));class C{}E(C,"Station",0),E(C,"Dept",1),E(C,"Emp",2),E(C,"SQL",3),E(C,"SQLTemplate",4),E(C,"GenerUserSelecter",5),E(C,"DeptAndStation",6),E(C,"Url",7),E(C,"AccepterOfDeptStationEmp",8),E(C,"AccepterOfDeptStationOfCurrentOper",9),E(C,"TeamOrgOnly",10),E(C,"TeamOnly",11),E(C,"TeamDeptOnly",12),E(C,"ByStationAI",13),E(C,"ByWebAPI",14),E(C,"ByMyDeptEmps",15);class J extends N{constructor(){super("GPE_SelectorModel");E(this,"SQLTemplate",`
  #### \u5E2E\u52A9
  - \u9009\u62E9\u5DF2\u7ECF\u9884\u5236\u597D\u7684SQL\u8BED\u53E5,\u67E5\u8BE2\u51FA\u6765\u53EF\u4EE5\u9009\u62E9\u7684\u4EBA\u5458\u8303\u56F4.
  - \u8FD9\u4E9B\u6A21\u677F\u6587\u4EF6\u90FD\u5B58\u50A8\u5728 WF_SQLTemplate ,\u60A8\u53EF\u4EE5\u4F7F\u7528 SELECT No,Name FROM WF_SQLTemplate WHERE SQLType=5 \u67E5\u8BE2\u51FA\u6765\u8FD9\u4E9B\u6A21\u677F.
  - \u7EF4\u62A4\u8FD9\u4E9B\u6A21\u677F\u5728\u8868\u5355\u8BBE\u8BA1\u5668\u4E2D\u7EF4\u62A4\uFF0C\u4E5F\u53EF\u4EE5\u624B\u5DE5\u7EF4\u62A4.
  `);E(this,"DeptAndStation",`
  #### \u5E2E\u52A9
   - \u6309\u9009\u62E9\u7684\u90E8\u95E8\u4E0E\u89D2\u8272\u7684\u4EA4\u96C6\uFF0C\u663E\u793A\u4EBA\u5458\u96C6\u5408\u3002
   - \u89D2\u8272\u96C6\u5408\u4E0B\u6709\u4E00\u4E9B\u4EBA\u5458\uFF0C\u90E8\u95E8\u4E0B\u6709\u4E00\u4E9B\u4EBA\u5458\uFF0C\u4ED6\u4EEC\u7684\u4EA4\u96C6\uFF0C\u5C31\u662F\u8981\u9009\u62E9\u7684\u8303\u56F4.
 
   `);E(this,"Emp",`
  #### \u5E2E\u52A9
   -  \u7ED1\u5B9A\u591A\u5C11\u4EBA\uFF0C\u5C31\u663E\u793A\u591A\u5C11\u4EBA\u3002
    `);E(this,"ByMyDeptEmps",`
  #### \u5E2E\u52A9
   - \u5F39\u51FA\u7684\u4EBA\u5458\u9009\u62E9\u5668\u4E2D\uFF0C\u4EC5\u4EC5\u5217\u51FA\u6765\u672C\u90E8\u95E8\u7684\u4EBA\u5458\uFF0C\u5305\u62EC\u517C\u804C\u90E8\u95E8\u3002
   - \u5173\u4E8E\u7EC4\u7EC7\u7ED3\u6784\u8868\uFF0C\u8BF7\u53C2\u8003doc.ccbpm.cn\u624B\u518C.
    `);E(this,"ByStation",`
  #### \u5E2E\u52A9
   - \u6839\u636E\u9009\u62E9\u7684\u89D2\u8272\u96C6\u5408\uFF0C\u6C42\u51FA\u6765\u8BE5\u96C6\u5408\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u3002
   - \u628A\u4EBA\u5458\u96C6\u5408\u5217\u51FA\u6765\uFF0C\u8BA9\u7528\u6237\u9009\u62E9\u3002
  `);E(this,"ByStationAI",`
  #### \u5E2E\u52A9

   - \u6839\u636E\u5F53\u524D\u4EBA\u5458\u6240\u5728\u90E8\u95E8\u7684\u96C6\u5408\uFF08\u4E00\u4EBA\u53EF\u80FD\u6240\u5728\u591A\u4E2A\u90E8\u95E8\uFF09\u5F97\u5230\u7684\u4EBA\u5458\u96C6\u5408,\u4E0E\u9009\u62E9\u7684\u89D2\u8272\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u7684\u4EA4\u96C6,\u5C31\u662F\u53EF\u9009\u62E9\u7684\u4EBA\u5458\u96C6\u5408.

   `);E(this,"ByBindEmp",`
  #### \u5E2E\u52A9
   - \u6309\u7ED1\u5B9A\u7684\u4EBA\u5458\u96C6\u5408\uFF0C\u4F5C\u4E3A\u9009\u62E9\u7684\u5BF9\u8C61\u3002     
`);E(this,"ByAPIUrl",`
#### \u5E2E\u52A9
 - \u63A5\u53E3\u8FD4\u56DE\u503C\u7C7B\u578B\u4E3AString\u7C7B\u578B, \u591A\u4E2A\u4EBA\u5458\u7528\u9017\u53F7\u5206\u5F00.
 - \u683C\u5F0F: zhangsan,lisi,wangwu
`);E(this,"BySQL",`
 
  #### \u5E2E\u52A9
   -  \u8BE5SQL\u662F\u9700\u8981\u8FD4\u56DENo,Name\u4E24\u4E2A\u5217\uFF0C\u5206\u522B\u662F\u4EBA\u5458\u7F16\u53F7,\u4EBA\u5458\u540D\u79F0\uFF0C\u8FD4\u56DE\u7684\u6570\u636E\u5FC5\u987B\u6309\u7167\u987A\u5E8F\u6765\u3002
   -  SQL\u8BED\u53E5\u652F\u6301ccbpm\u8868\u8FBE\u5F0F, \u6BD4\u5982\uFF1ASELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'
   -  \u6BD4\u5982\uFF1ASELECT No,Name FROM Port_Emp WHERE FK_Dept='@MyFieldName' MyFieldName \u5FC5\u987B\u662F\u8282\u70B9\u8868\u5355\u5B57\u6BB5.
   -  \u4EC0\u4E48\u662Fccbpm\u8868\u8FBE\u5F0F\uFF0C\u8BF7\u767E\u5EA6\uFF1Accbpm \u8868\u8FBE\u5F0F\u3002
   -  \u6CE8\u610F\uFF1A1. \u533A\u5206\u5927\u5C0F\u5199\u30022. \u987A\u5E8F\u4E0D\u80FD\u53D8\u5316, No,Name 
             
`);this.PageTitle="\u53EF\u9009\u4EBA\u5458\u8303\u56F4"}Init(){this.entity=new L,this.KeyOfEn=i.SelectorModel,this.AddGroup("B","\u6309\u7EC4\u7EC7\u7ED3\u6784\u9650\u5B9A\u8303\u56F4"),this.AddEntity(C.Station,"\u6309\u7167\u89D2\u8272",new M,this.ByStation),this.AddEntity(C.ByStationAI,"\u6309\u89D2\u8272\u667A\u80FD\u8BA1\u7B97",new M,this.ByStationAI),this.AddEntity(C.DeptAndStation,"\u6309\u90E8\u95E8\u4E0E\u89D2\u8272\u7684\u4EA4\u96C6",new W,this.DeptAndStation),this.AddEntity(C.Dept,"\u6309\u7ED1\u5B9A\u7684\u90E8\u95E8\u8BA1\u7B97",new w,this.ByBindEmp),this.Blank(C.ByMyDeptEmps,"\u53EA\u80FD\u9009\u62E9\u672C\u90E8\u95E8\u4EBA\u5458",this.ByMyDeptEmps),this.AddEntity(C.Emp,"\u53EA\u80FD\u9009\u62E9\u6307\u5B9A\u7684\u4EBA\u5458",new b,this.Emp),this.AddGroup("C","\u6309\u81EA\u5B9A\u4E49SQL\u9650\u5B9A\u8303\u56F4"),this.SingleTBSQL(C.SQL,"\u6309SQL\u8BA1\u7B97",B.DeliveryParas,this.BySQL);const e="SELECT No,Name FROM WF_SQLTemplate WHERE SQLType=5";this.SingleDDLSQL(C.SQLTemplate,"\u6309SQL\u6A21\u677F\u8BA1\u7B97",B.DeliveryParas,this.SQLTemplate,e,!1)}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const W5=Object.freeze(Object.defineProperty({__proto__:null,GPE_SelectorModel:J,SelectorModelEnum:C},Symbol.toStringTag,{value:"Module"}));class L extends l{constructor(u){super("TS.WF.SelecterFix"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u4EBA\u5458\u9009\u62E9\u5668");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID",!0),u.AddTBInt(i.SelectorModel,0,"\u663E\u793A\u65B9\u5F0F",!1,!1),u.AddBoolean(i.IsAutoLoadEmps,!0,"\u662F\u5426\u81EA\u52A8\u52A0\u8F7D\u4E0A\u4E00\u6B21\u9009\u62E9\u7684\u4EBA\u5458\uFF1F",!0,!0,!0),u.AddBoolean(i.IsSimpleSelector,!1,"\u662F\u5426\u5355\u9879\u9009\u62E9(\u53EA\u80FD\u9009\u62E9\u4E00\u4E2A\u4EBA)\uFF1F",!0,!0,!0),u.AddTBString(i.DeliveryParas,null,"\u53C2\u6570",!1,!1,0,100,100,!1,null),u.AddRM_GPE(new J,"icon-drop"),this._enMap=u,this._enMap}}const L5=Object.freeze(Object.defineProperty({__proto__:null,SelecterFix:L},Symbol.toStringTag,{value:"Module"}));class tu extends l{constructor(u){super("TS.WF.AccepterRoleBindSFTable"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u7ED1\u5B9A\u5B57\u5178");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),_u.AddAttrSFTable(u,"DeliveryParas","\u7ED1\u5B9A\u5B57\u5178(\u591A\u53C2)",1,0),this._enMap=u,this._enMap}}const K5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleBindSFTable:tu},Symbol.toStringTag,{value:"Module"}));class q extends N{constructor(){super("GPE_ARDeptModel");E(this,"Help0",`
  #### \u8BF4\u660E
   - \u63D0\u4EA4\u4EBA\u6240\u6709\u7684\u5C97\u4F4D\u96C6\u5408,\u4E0E\u5F53\u524D\u7684\u5C97\u4F4D\u96C6\u5408\u5339\u914D.
    `);E(this,"Help1",`
    #### \u8BF4\u660E
     - \u63D0\u4EA4\u4EBA\u767B\u5F55\u90E8\u95E8\u4E0B\u7684\u5C97\u4F4D\u96C6\u5408.
  `);E(this,"Desc1",`
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
   - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow1.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi1.png "\u5C4F\u5E55\u622A\u56FE")
      `);E(this,"Desc2",`
  #### \u8BF4\u660E
  - \u9009\u62E9\u7684\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD(\u8BE5\u5B57\u6BB5\u91CC\u5B58\u50A8\u7684\u662F\u8D26\u53F7)
  - \u6307\u5B9A\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
  - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u8868\u5355\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingBiaodan2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi2.png "\u5C4F\u5E55\u622A\u56FE")
      `);this.PageTitle="\u90E8\u95E8\u96C6\u5408\u8303\u56F4"}Init(){return s(this,null,function*(){this.entity=new K,this.KeyOfEn="ARDeptModel";const e=new f(this.PKVal);yield e.Retrieve();const t=e.FK_Flow;this.AddGroup("A","\u6309\u53D1\u9001\u8282\u70B9\u53D1\u9001\u4EBA\u8BA1\u7B97"),this.Blank("0","(\u4E0A\u4E00\u8282\u70B9)\u53D1\u9001\u4EBA\u6240\u6709\u7684\u90E8\u95E8",this.Help0),this.Blank("1","(\u4E0A\u4E00\u8282\u70B9)\u53D1\u9001\u4EBA\u767B\u5F55\u90E8\u95E8",this.Help1),this.Blank("2","(\u4E0A\u4E00\u8282\u70B9)\u53D1\u9001\u4EBA\u4F7F\u7528\u90E8\u95E8",this.HelpUn),this.Blank("3","(\u5F00\u59CB\u8282\u70B9)\u53D1\u8D77\u4EBA\u4F7F\u7528\u90E8\u95E8",this.HelpUn),this.AddGroup("B","\u6309\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97");const F=`SELECT NodeID No,CONCAT(NodeID,'_',Name) as Name FROM WF_Node WHERE FK_Flow='${t}' `;this.SelectItemsByList("10","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4F7F\u7528\u90E8\u95E8",this.HelpUn,!1,F,"ARDeptPara",""),this.SelectItemsByList("11","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u6240\u6709\u90E8\u95E8",this.HelpUn,!1,F,"ARDeptPara",""),this.SelectItemsByList("12","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4E3B\u90E8\u95E8",this.HelpUn,!1,F,"ARDeptPara",""),this.AddGroup("C","\u6309\u8868\u5355\u5B57\u6BB5\u8BA1\u7B97");const D="ND"+parseInt(t)+"Rpt",p=n.SQLOfMapAttrsGener(D);this.SelectItemsByList("20","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u4EBA\u5458\u7F16\u53F7-\u4E3B\u90E8\u95E8",this.HelpUn,!1,p,"ARDeptPara",""),this.SelectItemsByList("21","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u4EBA\u5458\u7F16\u53F7-\u6240\u6709\u90E8\u95E8",this.HelpUn,!1,p,"ARDeptPara",""),this.SelectItemsByList("22","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u90E8\u95E8\u7F16\u53F7",this.HelpUn,!1,p,"ARDeptPara",""),this.SingleTB("23","\u7CFB\u7EDF\u53C2\u6570\u503C\u662F\u90E8\u95E8\u7F16\u53F7","ARDeptPara",this.HelpUn,"\u8BF7\u8F93\u5165\u7CFB\u7EDF\u53C2\u6570")})}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const O5=Object.freeze(Object.defineProperty({__proto__:null,GPE_ARDeptModel:q},Symbol.toStringTag,{value:"Module"}));class K extends l{constructor(u){super("TS.WF.ARBindStationSpecDept"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u6309\u6307\u5B9A\u7684\u90E8\u95E8\u96C6\u5408\u4E0E\u8BBE\u7F6E\u7684\u89D2\u8272\u4EA4\u96C6\u8BA1\u7B97");u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("ARDeptModel",null,"\u90E8\u95E8\u89C4\u5219",!1,!1,0,100,100,!0),u.AddTBString("ARDeptPara",null,"\u53C2\u6570",!1,!1,0,100,100,!0),u.AddTBString("NodeStations",null,"\u89D2\u8272",!0,!1,0,100,100,!0),u.SetPopGroupList("NodeStations",n.srcStationTypes,n.srcStations,!0,"300px","500px","\u9009\u62E9\u89D2\u8272","icon-people","1");const e=`
    #### \u5E2E\u52A9
    - \u6307\u5B9A\u7684\u90E8\u95E8\u96C6\u5408\u9012\u5F52\u6A21\u5F0F.
    - 0=\u9012\u5F52\u5E76\u7D2F\u52A0,\u9012\u5F52\u5230\u6839\u8282\u70B9,\u5E76\u628A\u627E\u5230\u7684\u4EBA\u7D2F\u52A0\u8D77\u6765.
    - 1=\u9012\u5F52\u4E0D\u7D2F\u52A0,\u5411\u6839\u8282\u70B9\u9012\u5F52,\u5982\u679C\u627E\u5230\u4EBA,\u5C31\u4E0D\u5728\u9012\u5F52\u4E86.
    - 2=\u4E0D\u9012\u5F52, \u4EC5\u4EC5\u6309\u7167\u6307\u5B9A\u7684\u90E8\u95E8\u5BFB\u627E.
    `;return u.AddDDLSysEnum("DGModel56",0,"\u9012\u5F52\u6A21\u5F0F",!0,!0,"DGModel56","@0=\u9012\u5F52\u5E76\u7D2F\u52A0@1=\u9012\u5F52\u4E0D\u7D2F\u52A0@2=\u4E0D\u9012\u5F52",e,!0),u.AddTBString("ARDeptPara",null,"\u53C2\u6570",!1,!1,0,100,100,!0),u.AddTBAtParas(4e3),u.ParaFields=",ARDeptModel,ARDeptPara,DGModel56,",u.AddRM_GPE(new q,"icon-drop"),this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new P().Delete("FK_Node",this.NodeID),typeof this.NodeStations!="string"||this.NodeStations.split(",").forEach(t=>s(this,null,function*(){const F=new R;F.FK_Node=this.NodeID,F.FK_Station=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()})),Promise.resolve(!0)})}}const U5=Object.freeze(Object.defineProperty({__proto__:null,ARBindStationSpecDept:K},Symbol.toStringTag,{value:"Module"}));class Bu extends l{constructor(u){super("TS.WF.ARBindStationSpecSta"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u6309\u6307\u5B9A\u7684\u5C97\u4F4D\u96C6\u5408\u4E0E\u8BBE\u7F6E\u7684\u90E8\u95E8\u4EA4\u96C6\u8BA1\u7B97");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("ARStaModel",null,"\u90E8\u95E8\u89C4\u5219",!1,!1,0,100,100,!0),u.AddTBString("ARStaPara",null,"\u53C2\u6570",!1,!1,0,100,100,!0),u.AddTBString("NodeDepts",null,"\u90E8\u95E8",!0,!1,0,500,100,!0),u.SetPopTree("NodeDepts",n.srcDepts,n.srcDeptRoot,!0,"300px","500px","\u9009\u62E9\u90E8\u95E8","icon-people"),u.AddTBAtParas(4e3),u.ParaFields=",ARStaModel,ARStaPara,",u.AddRM_GPE(new $,"icon-drop"),this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new z().Delete("FK_Node",this.NodeID),this.NodeDepts.split(",").forEach(t=>s(this,null,function*(){const F=new Q;F.FK_Node=this.NodeID,F.FK_Dept=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()})),Promise.resolve(!0)})}}const G5=Object.freeze(Object.defineProperty({__proto__:null,ARBindStationSpecSta:Bu},Symbol.toStringTag,{value:"Module"}));class $ extends N{constructor(){super("GPE_ARStaModel");E(this,"Help0",`
  #### \u8BF4\u660E
   - \u63D0\u4EA4\u4EBA\u6240\u6709\u7684\u5C97\u4F4D\u96C6\u5408,\u4E0E\u5F53\u524D\u7684\u5C97\u4F4D\u96C6\u5408\u5339\u914D.
    `);E(this,"Help1",`
    #### \u8BF4\u660E
     - \u63D0\u4EA4\u4EBA\u767B\u5F55\u90E8\u95E8\u4E0B\u7684\u5C97\u4F4D\u96C6\u5408.
  `);E(this,"Desc1",`
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
   - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow1.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi1.png "\u5C4F\u5E55\u622A\u56FE")
      `);E(this,"Desc2",`
  #### \u8BF4\u660E
  - \u9009\u62E9\u7684\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD(\u8BE5\u5B57\u6BB5\u91CC\u5B58\u50A8\u7684\u662F\u8D26\u53F7)
  - \u6307\u5B9A\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
  - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u8868\u5355\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingBiaodan2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi2.png "\u5C4F\u5E55\u622A\u56FE")
      `);this.PageTitle="\u89D2\u8272\u96C6\u5408\u8303\u56F4"}Init(){return s(this,null,function*(){this.entity=new Bu,this.KeyOfEn="ARStaModel";const e=new f(this.PKVal);yield e.Retrieve();const t=e.FK_Flow;this.AddGroup("A","\u6309\u53D1\u9001\u8282\u70B9\u53D1\u9001\u4EBA\u8BA1\u7B97"),this.Blank("0","\u53D1\u9001\u4EBA\u6240\u6709\u7684\u89D2\u8272",this.Help0),this.Blank("1","\u53D1\u9001\u4EBA\u4F7F\u7528\u7684\u89D2\u8272",this.Help1),this.AddGroup("B","\u6309\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97");const F=`SELECT NodeID No,Name FROM WF_Node WHERE FK_Flow='${t}' `;this.SelectItemsByList("10","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4F7F\u7528\u89D2\u8272",this.HelpUn,!1,F,"ARStaPara",""),this.SelectItemsByList("11","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u6240\u6709\u89D2\u8272",this.HelpUn,!1,F,"ARStaPara",""),this.AddGroup("C","\u6309\u8868\u5355\u5B57\u6BB5\u8BA1\u7B97");const D="ND"+parseInt(t)+"Rpt",p=n.SQLOfMapAttrsGener(D);this.SelectItemsByList("20","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u4EBA\u5458\u7F16\u53F7-\u6240\u6709\u89D2\u8272",this.HelpUn,!1,p,"ARStaPara",""),this.SelectItemsByList("21","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u89D2\u8272\u7F16\u53F7",this.HelpUn,!1,p,"ARStaPara","")})}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const v5=Object.freeze(Object.defineProperty({__proto__:null,GPE_ARStaModel:$},Symbol.toStringTag,{value:"Module"}));class v extends l{constructor(u){super("TS.WF.ARStation"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u7ED1\u5B9A\u89D2\u8272");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("NodeStations",null,"\u89D2\u8272",!0,!1,0,100,100,!0),u.SetPopGroupList("NodeStations",n.srcStationTypes,n.srcStations,!0,"300px","500px","\u9009\u62E9\u89D2\u8272","icon-people","1"),u.AddRM_GPE(new j,"icon-drop"),this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new P().Delete("FK_Node",this.NodeID),typeof this.NodeStations!="string"||this.NodeStations.split(",").forEach(t=>s(this,null,function*(){const F=new R;F.FK_Node=this.NodeID,F.FK_Station=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()})),Promise.resolve(!0)})}}const x5=Object.freeze(Object.defineProperty({__proto__:null,ARStation:v},Symbol.toStringTag,{value:"Module"}));class Au extends l{constructor(u){super("TS.WF.AccepterRoleMLeader"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u8FDE\u7EED\u591A\u7EA7\u4E3B\u7BA1");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("NodeStations",null,"\u89D2\u8272",!0,!1,0,100,100,!0),u.SetPopGroupList("NodeStations",n.srcStationTypes,n.srcStations,!0,"300px","500px","\u9009\u62E9\u89D2\u8272","icon-people","1"),u.AddBoolean("JiBieModel",!1,"\u540C\u65F6\u4E0D\u8D85\u8FC7\u53D1\u8D77\u4EBA\u5411\u4E0A\u7684",!0,!0),u.AddTBInt("JiBieLevel",0,"\u7EA7\u522B",!0,!1),u.AddTBAtParas(4e3),u.ParaFields=",JiBieModel,JiBieLevel,",this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return yield new P().Delete("FK_Node",this.NodeID),typeof this.NodeStations!="string"||this.NodeStations.split(",").forEach(t=>s(this,null,function*(){const F=new R;F.FK_Node=this.NodeID,F.FK_Station=t,F.MyPK=this.NodeID+"_"+t,yield F.Insert()})),Promise.resolve(!0)})}}const H5=Object.freeze(Object.defineProperty({__proto__:null,AccepterRoleMLeader:Au},Symbol.toStringTag,{value:"Module"}));class ou extends N{constructor(){super("GPE_AccepterRole");E(this,"Starter_Station",`
  #### \u5E2E\u52A9
  - \u89D2\u8272\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u53EF\u4EE5\u53D1\u8D77.
  `);E(this,"Starter_Dept",`
  #### \u5E2E\u52A9
  - \u90E8\u95E8\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u7684\u4EA4\u96C6\u53EF\u4EE5\u53D1\u8D77.
  `);E(this,"Starter_DeptAndStation",`
  #### \u5E2E\u52A9
  - \u89D2\u8272\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u4E0E\u90E8\u95E8\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u7684\u4EA4\u96C6\u53EF\u4EE5\u53D1\u8D77.
  `);E(this,"Starter_Orgs",`
  #### \u5E2E\u52A9
  - \u5BF9\u4E8E\u96C6\u56E2\u7248\u4E00\u4E2A\u7EC4\u7EC7\u8BBE\u8BA1\u4E00\u4E2A\u6D41\u7A0B\u5982\u679C\u8981\u5171\u4EAB\u7ED9\u5176\u4ED6\u7EC4\u7EC7\u4F7F\u7528\uFF0C\u5C31\u53EF\u4EE5\u542F\u7528\u8BE5\u529F\u80FD.
  - \u8FD9\u5C31\u53EB\u6D41\u7A0B\u6A21\u677F\u5171\u4EAB, \u5171\u4EAB\u7684\u6D41\u7A0B\u90FD\u5FC5\u987B\u662F\u4EFB\u4F55\u4EBA\u90FD\u53EF\u4EE5\u53D1\u8D77\u7684\u6D41\u7A0B.
  ##### \u5176\u4ED6
  - \u5173\u4E8Eccbpm\u7684\u5DE5\u4F5C\u6A21\u5F0F\uFF0C\u8BF7\u53C2\u8003doc.ccbpm.cn \u624B\u518C. 
  `);E(this,"Starter_Guests",`
  #### \u5E2E\u52A9
  - \u6211\u4EEC\u628A\u7528\u6237\u5206\u4E3A\u5916\u90E8\u7528\u6237\u4E0E\u5185\u90E8\u7528\u6237.
  - \u5185\u90E8\u7528\u6237\u5C31\u662F\u7EC4\u7EC7\u5185\u7684\u7528\u6237\uFF0C\u5B58\u50A8\u5728Port_Emp\u8868\u91CC, \u5C31\u662F\u7EC4\u7EC7\u5185\u7684\u5DE5\u4F5C\u4EBA\u5458.
  - \u5916\u90E8\u7528\u6237\u6709\u591A\u79CD, \u6BD4\u5982:\u4F9B\u5E94\u5546\u3001\u9500\u552E\u5546\u3001\u5408\u4F5C\u4F19\u4F34\u8FD9\u4E9B\u4EBA\u5458\u53EF\u4EE5\u5B58\u50A8\u81EA\u5DF1\u6307\u5B9A\u7684\u8868\u91CC.
  - \u66F4\u591A\u4FE1\u606F\u8BF7\u53C2\u8003\u64CD\u4F5C\u624B\u518C doc.ccbpm.cn
  #### \u5B66\u751F\u8BF7\u5047\u7CFB\u7EDF
  - \u6D41\u7A0B\u56FE: 
  - \u6559\u804C\u5DE5\u5C31\u662F\u5185\u90E8\u7528\u6237\uFF0C\u5BB6\u957F\u4E3A\u5B69\u5B50\u8BF7\u5047\uFF0C\u5BB6\u957F\u767B\u5F55\u5C31\u662F\u5916\u90E8\u7528\u6237.
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/StartGuest.png "\u5C4F\u5E55\u622A\u56FE")
  - \u5916\u90E8\u7528\u6237\u4E5F\u6709\u5F85\u529E\u3001\u5728\u9014\u3001\u53D1\u8D77\u3001\u6D41\u7A0B\u64CD\u4F5C\uFF0C\u63A5\u53E3\u4E0E\u5185\u90E8\u7528\u6237\u4E0D\u540C.
  #### \u5916\u90E8\u7528\u6237\u53D1\u8D77\u6D41\u7A0B\u7684API\u63A5\u53E3.
  - BP.WF.Dev2InterfaceGuest.*.* 
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/StarterDev2.png "\u5C4F\u5E55\u622A\u56FE.png")

  `);E(this,"Starter_AnyOne",`
  #### \u5E2E\u52A9
  - \u6D41\u7A0B\u53D1\u8D77\u4EBA\u4E5F\u79F0\u4E3A\u6D41\u7A0B\u7684\u542F\u52A8\u6743\u9650.
  - \u9ED8\u8BA4\u4E3A\uFF1A\u6240\u6709\u7684\u4EBA\u90FD\u53EF\u53D1\u8D77,\u542F\u52A8\u6539\u6D41\u7A0B\u7684\u6743\u9650\u4E0D\u9650\u5236.
  #### \u5176\u4ED6
  - \u6709\u4E00\u4E9B\u6D41\u7A0B\u4E0D\u60F3\u88AB\u51FA\u73B0\u5728\u53D1\u8D77\u6D41\u7A0B\u7684\u5217\u8868\u91CC\uFF0C\u6BD4\u5982\uFF1A\u5B50\u6D41\u7A0B\u4E0D\u80FD\u88AB\u5355\u72EC\u8C03\u7528\uFF0C\u5C31\u5728\u6D41\u7A0B\u5C5E\u6027\u91CC\u662F\u5426\u53EF\u4EE5\u72EC\u7ACB\u542F\u52A8.
  - \u4E0D\u53EF\u4EE5\u72EC\u7ACB\u542F\u52A8\uFF0C\u5C31\u7B97\u6709\u53D1\u8D77\u6743\u9650\u4E5F\u4E0D\u80FD\u663E\u793A\u5728\u53D1\u8D77\u6D41\u7A0B\u7684\u5217\u8868\u91CC.
  #### \u53D1\u8D77\u6D41\u7A0B\u5217\u8868
  - \u4E00\u4E2A\u4EBA\u8FDB\u5165\u7CFB\u7EDF\u540E\uFF0C\u53EF\u4EE5\u53D1\u8D77\u7684\u6D41\u7A0B\u5217\u8868\u662F\u53EF\u4EE5\u901A\u8FC7\u63A5\u53E3\u8C03\u7528\u7684.
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/StarterAnyOne.png "\u5C4F\u5E55\u622A\u56FE.png")

  `);E(this,"Starter_Emps",`
  #### \u5E2E\u52A9
  - \u6309\u7ED1\u5B9A\u7684\u4EBA\u5458\u8BA1\u7B97, \u7ED1\u5B9A\u7684\u4EBA\u624D\u80FD\u53D1\u8D77\u6D41\u7A0B.
  `);E(this,"ByStation",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u63A5\u53D7\u4EBA\u89C4\u5219,\u5C31\u662FA\u8282\u70B9\u53D1\u5230B\u8282\u70B9, B\u8282\u70B9\u4E0A\u90A3\u4E9B\u4EBA\u53EF\u4EE5\u5904\u7406B\u8282\u70B9\u4E0A\u7684\u5DE5\u4F5C,\u6839\u636E\u4E0D\u540C\u7684\u60C5\u51B5\u4E0B\uFF0C\u6211\u4EEC\u505A\u4E0D\u540C\u7684\u5904\u7406\uFF0C\u8FD9\u5C31\u53EB\u63A5\u53D7\u4EBA\u89C4\u5219.
  - \u63A5\u6536\u4EBA\u89C4\u5219,\u5206\u4E3A\u4E24\u5927\u7C7B:\u4E3B\u89C2\u9009\u62E9\u4E0E\u81EA\u52A8\u8BA1\u7B97.
  - \u529F\u80FD\u62D3\u6251\u56FE

  #### \u627E\u4EBA\u7B97\u6CD5
  
   -  \u7B2C0\u6B65\uFF1A\u9996\u5148\u4ECE\u672C\u7EA7\u90E8\u95E8\u5185\u7684\u4EBA\u5458\u5BFB\u627E\uFF0C\u8BE5\u8282\u70B9\u7ED1\u5B9A\u7684\u89D2\u8272\uFF0C\u5982\u679C\u627E\u4E0D\u5230\u4EBA\u5C31\u8FDB\u5165\u4E0B\u4E00\u6B65\u5BFB\u627E\u3002
   -  \u7B2C1\u6B65\uFF1A\u76F4\u7EBF\u4E0A\u7EA7\u5BFB\u627E\uFF0C\u4E00\u76F4\u67E5\u8BE2\u5230\u6839\u8282\u70B9\uFF0C\u5982\u679C\u627E\u4E0D\u5230\u4EBA\u5C31\u8FDB\u5165\u4E0B\u4E00\u6B65\u5BFB\u627E\u3002
   -  \u7B2C2\u6B65\uFF1A\u76F4\u7EBF\u4E0A\u7EA7\u7684\u5E73\u7EA7\u90E8\u95E8\u5BFB\u627E\uFF0C\u4E00\u76F4\u67E5\u8BE2\u5230\u6839\u8282\u70B9\uFF0C\u5982\u679C\u627E\u4E0D\u5230\u4EBA\u5C31\u8FDB\u5165\u4E0B\u4E00\u6B65\u5BFB\u627E\u3002
   -  \u7B2C3\u6B65\uFF1A\u5F53\u524D\u90E8\u95E8\u7684\u4E0B\u4E00\u7EA7\u6240\u6709\u7684\u90E8\u95E8\u5BFB\u627E\u3002
   -  \u7B2C4\u6B65\uFF1A\u67E5\u627E\u5168\u5C40\u89D2\u8272\u4EBA\u5458\u3002
   
  #### \u4EC0\u4E48\u662F\u4EBA\u5458\u8EAB\u4EFD? 
   -  \u5C31\u662F\u6309\u7167\u90A3\u4E2A\u4EBA\u5458\u6765\u8BA1\u7B97\u5F53\u524D\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\uFF0C\u9ED8\u8BA4\u662F\u8C01\u64CD\u4F5C\u7684\u6309\u7167\u8C01\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002
   -  \u6BD4\u5982\uFF1A\u6211\u8981\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u5C31\u6309\u7167\u5F53\u524D\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002
   -  \u6709\u7684\u73AF\u5883\u4E0B\u5E76\u4E0D\u662F\u6309\u7167\u5F53\u524D\u4EBA\u5458\u8BA1\u7B97\u3002 
   -  \u6BD4\u5982\uFF1A\u6211\u8981\u4E3A\u522B\u4EBA\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u8981\u6309\u7167\u6307\u5B9A\u7684\u5B57\u6BB5\uFF08\u8BE5\u5B57\u6BB5\u5C31\u662F\u88AB\u8BF7\u5047\u4EBA\u5458\u7684\u8D26\u53F7\uFF09\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD\u3002
   -  \u8BE6\u89C1Entity\u5185\u8BF4\u660E\u3002
   #### \u5176\u4ED6\u8BF4\u660E
   - \u8BE5\u9009\u9879\u4E0E\u4EC5\u6309\u7167\u89D2\u8272\u8BA1\u7B97\u6709\u533A\u522B.
   - \u4EC5\u6309\u7167\u89D2\u8272\u8BA1\u7B97\uFF0C\u5C31\u662F\u53D1\u9001\u7ED9\u9009\u62E9\u7684\u89D2\u8272\u4E0B\u7684\u6240\u6709\u7684\u4EBA\u3002
  `);E(this,"ByStationSpecDepts",`
  #### \u5E2E\u52A9
   - \u5F53\u524D\u8282\u70B9\u8BBE\u7F6E\u5C97\u4F4D\u96C6\u5408\uFF0C\u8BBE\u7F6E\u90E8\u95E8\u96C6\u5408\u89C4\u5219.
   - \u7CFB\u7EDF\u5728\u8BA1\u7B97\u7684\u65F6\u5019\uFF0C\u5C31\u53EF\u4EE5\u6839\u636E\u90E8\u95E8\u89C4\u5219\u6C42\u51FA\u90E8\u95E8\u96C6\u5408\uFF0C\u52A0\u4E0A\u5C97\u4F4D\u96C6\u5408\u6C42\u51FA\u4ED6\u4EEC\u7684\u4EA4\u96C6\u3002
  #### \u5176\u4ED6
  - \u4E3A\u5929\u5B87\u96C6\u56E2\u5F00\u53D1,
  - \u5F00\u53D1\u65E5\u671F: 2023.07.18
  #### \u7D2F\u52A0\u573A\u666F
  - \u4E00\u4E2A\u5355\u5B50\uFF0C\u9700\u8981\u5B50\u516C\u53F8\u7684\u4E3B\u7BA1\u5BA1\u6279,\u4E5F\u9700\u8981\u603B\u516C\u53F8\u7684\u4E3B\u7BA1\u5BA1\u6279,\u6839\u636E\u6307\u5B9A\u7684\u90E8\u95E8\u4E00\u6B65\u6B65\u7684\u5BFB\u627E,\u7136\u540E\u6309\u7167\u4E3B\u7EBF\u76F4\u7EBF\u5BFB\u627E.
  - \u5728\u6BCF\u4E2A\u90E8\u95E8\u5BFB\u627E\u51FA\u6765\u7684\u4EBA\u5458\u5C31\u7D2F\u52A0\u8D77\u6765.
    `);E(this,"ByStationOnly",`
  #### \u5E2E\u52A9

   - \u5F53\u524D\u8282\u70B9\u7ED1\u5B9A\u89D2\u8272\u7684\u96C6\u5408\u4E0B\u9762\u7684\u4EBA\u5458\u96C6\u5408\u4F5C\u4E3A\u63A5\u53D7\u4EBA\u3002
   - \u6BD4\u5982\uFF1A\u4E00\u4E2A\u7701\u7EA7\u7684\u516C\u5B89\u7CFB\u7EDF\u5E94\u7528\u91CC\uFF0C\u5F53\u524D\u8282\u70B9\u7ED1\u5B9A\u6D3E\u51FA\u6240\u6240\u957F\u89D2\u8272, \u5982\u679C\u4EC5\u6309\u7167\u89D2\u8272\u8BA1\u7B97\uFF0C\u5C31\u4F1A\u6295\u9012\u5230\u5168\u7701\u7684\u6240\u6709\u6D3E\u51FA\u6240\u6240\u957F\u3002
   - \u5982\u679C\u6309\u7167\u89D2\u8272\u667A\u80FD\u8BA1\u7B97\uFF0C\u5C31\u4F1A\u6295\u9012\u5230\u8BE5\u8B66\u5BDF\u6240\u5728\u90E8\u95E8\u7684\u6D3E\u51FA\u6240\u6240\u957F\u3002
   #### \u5176\u4ED6
   - \u5168\u5C40\u7684\u6309\u7167\u89D2\u8272\u5BFB\u627E\u63A5\u6536\u4EBA\uFF0C\u4E0D\u8003\u8651\u90E8\u95E8\u7684\u7EF4\u5EA6\u3002

    `);E(this,"ByBindEmp",`
  #### \u5E2E\u52A9
   - \u7ED1\u5B9A\u7684\u6240\u6709\u7684\u4EBA\u5458\uFF0C\u90FD\u53EF\u4EE5\u5904\u7406\u8BE5\u8282\u70B9\u7684\u5DE5\u4F5C\u3002
   - \u7ED1\u5B9A\u591A\u5C11\u4E2A\u4EBA\uFF0C\u5F53\u524D\u8282\u70B9\u5C31\u6709\u591A\u5C11\u4E2A\u4EBA\u5904\u7406\uFF0C\u8FD9\u4E00\u79CD\u662F\u6700\u7B80\u6D01\u6700\u76F4\u63A5\u7684\u65B9\u5F0F\u3002
   - \u9002\u7528\u4E8E\u5F53\u524D\u8282\u70B9\u4EBA\u5458\u6BD4\u8F83\u7A33\u5B9A\uFF0C\u4E00\u822C\u4E0D\u4F1A\u53D8\u5316\u7684\u60C5\u51B5\u3002
   - \u5982\u679C\u4EBA\u5458\u53D8\u5316\u6BD4\u8F83\u9891\u7E41\uFF0C\u5C31\u9700\u8981\u8BBE\u7F6E\u89D2\u8272\uFF0C\u8BA9\u89D2\u8272\u8BBE\u7F6E\u4EBA\u5458\u3002     
`);E(this,"ByDeptAndStation",`
  #### \u5E2E\u52A9
   - \u53D6\u65E2\u5177\u5907\u6B64\u89D2\u8272\u96C6\u5408\u7684\u53C8\u5177\u5907\u6B64\u90E8\u95E8\u96C6\u5408\u7684\u4EBA\u5458\uFF0C\u505A\u4E3A\u672C\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u5458\u3002
   - \u90E8\u95E8\u4EBA\u5458\u662F\u4E00\u4E2A\u96C6\u5408\uFF0C\u89D2\u8272\u4EBA\u5458\u662F\u4E00\u4E2A\u96C6\u5408\uFF0C\u4E24\u4E2A\u96C6\u5408\u76F8\u4EA4\u7684\u4EBA\u5458\u96C6\u5408\u5C31\u662F\u5F53\u524D\u8282\u70B9\u8981\u6295\u9012\u7684\u5BF9\u8C61\u3002
`);E(this,"ByStationAndEmpDept",`

  #### \u5E2E\u52A9

   - \u8BE5\u64CD\u4F5C\u9700\u8981\u8BBE\u7F6E\u90E8\u95E8\u4E0E\u8BBE\u7F6E\u89D2\u8272\uFF0C\u4E24\u4E2A\u8BBE\u7F6E\u3002
   - \u5F53\u524D\u8282\u70B9\u7684\u5904\u7406\u4EBA\u5458\u9700\u8981\u6C42\u4E24\u4E2A\u96C6\u5408\u7684\u4EA4\u96C6\u3002
   - \u6BD4\u5982:\u5728\u89D2\u8272\u91CC\u8BBE\u7F6E\u90E8\u95E8\u7ECF\u7406\u89D2\u8272\uFF0C\u5728\u90E8\u95E8\u91CC\u8BBE\u7F6E\u8D22\u52A1\u4E0E\u4EBA\u529B\u8D44\u6E90\u4E24\u4E2A\u90E8\u95E8\u3002
   - \u7CFB\u7EDF\u5C31\u4F1A\u5F97\u5230\u4E24\u4E2A\u4EBA\u5458\u96C6\u5408\uFF0C\u7B2C\u4E00\u4E2A\u96C6\u5408\u662F\u6240\u6709\u5177\u6709\u90E8\u95E8\u7ECF\u7406\u89D2\u8272\u7684\u4EBA\u5458\uFF0C\u7B2C2\u4E2A\u96C6\u5408\u5C31\u662F\u8D22\u52A1\u90E8\uFF0C\u4EBA\u529B\u8D44\u6E90\u90E8\u6240\u6709\u7684\u4EBA\u5458\u3002
   - \u4E24\u4E2A\u7684\u4EA4\u96C6\u5C31\u662F\u4E00\u4E2A\u8D22\u52A1\u90E8\u7ECF\u7406\u4E8E\u4E00\u4E2A\u4EBA\u529B\u8D44\u6E90\u90E8\u7ECF\u7406\u3002

`);E(this,"BySpecNodeEmpStation",`
  #### \u5E2E\u52A9
   - \u6307\u5B9A\u8282\u70B9\u5904\u7406\u4EBA\u5458\u7684\u8EAB\u4EFD\u7684\u89D2\u8272\u505A\u4E3A\u8BA1\u7B97\u89C4\u5219\u3002
   - \u4E0E\u5F53\u524D\u64CD\u4F5C\u5458\u89D2\u8272\u8EAB\u4EFD\u4E0D\u540C\u7684\u662F\uFF0C\u4EE5\u4EE5\u524D\u7684\u8282\u70B9\u5904\u7406\u4EBA\u7684\u8EAB\u4EFD\u4FE1\u606F\uFF0C\u90E8\u95E8\u4FE1\u606F\uFF0C\u89D2\u8272\u4FE1\u606F\u6765\u8BA1\u7B97\u3002
`);E(this,"BySetDeptAsSubthread",`

  #### \u5E2E\u52A9

  - \u4EC5\u9002\u7528\u4E8E\u5B50\u7EBF\u7A0B\u8282\u70B9\uFF0C\u6309\u7167\u90E8\u95E8\u5206\u7EC4\u5B50\u7EBF\u7A0B\u4E0A\u7684\u5904\u7406\u4EBA\u5458\u3002
  - \u6BCF\u4E2A\u90E8\u95E8\u4E00\u4E2A\u4EFB\u52A1\uFF0C\u5982\u679C\u8BE5\u90E8\u95E8\u7684\u5176\u4E2D\u6709\u4E00\u4E2A\u4EBA\u5904\u7406\u4E86\uFF0C\u5C31\u6807\u8BC6\u8BE5\u90E8\u95E8\u7684\u5DE5\u4F5C\u5B8C\u6210\uFF0C\u53EF\u4EE5\u6D41\u8F6C\u5230\u4E0B\u4E00\u6B65\u3002
`);E(this,"FindSpecDeptEmps",`

  #### \u5E2E\u52A9
   

    
  - \u7ED1\u5B9A\u7684\u6240\u6709\u7684\u4EBA\u5458\uFF0C\u90FD\u53EF\u4EE5\u5904\u7406\u8BE5\u8282\u70B9\u7684\u5DE5\u4F5C\u3002
  - \u7ED1\u5B9A\u591A\u5C11\u4E2A\u4EBA\uFF0C\u5F53\u524D\u8282\u70B9\u5C31\u6709\u591A\u5C11\u4E2A\u4EBA\u5904\u7406\uFF0C\u8FD9\u4E00\u79CD\u662F\u6700\u7B80\u6D01\u6700\u76F4\u63A5\u7684\u65B9\u5F0F\u3002
  - \u9002\u7528\u4E8E\u5F53\u524D\u8282\u70B9\u4EBA\u5458\u6BD4\u8F83\u7A33\u5B9A\uFF0C\u4E00\u822C\u4E0D\u4F1A\u53D8\u5316\u7684\u60C5\u51B5\u3002
  - \u5982\u679C\u4EBA\u5458\u53D8\u5316\u6BD4\u8F83\u9891\u7E41\uFF0C\u5C31\u9700\u8981\u8BBE\u7F6E\u89D2\u8272\uFF0C\u8BA9\u89D2\u8272\u8BBE\u7F6E\u4EBA\u5458\u3002

   
  

`);E(this,"ByDeptLeader",`
        
  #### \u5E2E\u52A9
          
   - \u5C31\u662F\u6309\u7167\u90A3\u4E2A\u4EBA\u5458\u6765\u8BA1\u7B97\u5F53\u524D\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\uFF0C\u9ED8\u8BA4\u662F\u8C01\u64CD\u4F5C\u7684\u6309\u7167\u8C01\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002 
   - \u6BD4\u5982\uFF1A\u6211\u8981\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u5C31\u6309\u7167\u5F53\u524D\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002 
   - \u6709\u7684\u73AF\u5883\u4E0B\u5E76\u4E0D\u662F\u6309\u7167\u5F53\u524D\u4EBA\u5458\u8BA1\u7B97\u3002 
   - \u6BD4\u5982\uFF1A\u6211\u8981\u4E3A\u522B\u4EBA\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u8981\u6309\u7167\u6307\u5B9A\u7684\u5B57\u6BB5\uFF08\u8BE5\u5B57\u6BB5\u5C31\u662F\u88AB\u8BF7\u5047\u4EBA\u5458\u7684\u8D26\u53F7\uFF09\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD\u3002 

        
  #### \u8BF4\u660E
  
   - \u4E0A\u4E00\u4E2A\u8282\u70B9\u53D1\u9001\u4EBA\u7684\u76F4\u5C5E\u9886\u5BFC\uFF0C\u5904\u7406\u8BE5\u5DE5\u4F5C\u3002 
   - \u90E8\u95E8\u8D1F\u8D23\u4EBA\u4FE1\u606F\uFF0C\u5B58\u50A8\u5230\u8868 Port_Dept \u5B57\u6BB5\uFF1ALeader \u4E2D.  
   - \u6CE8\u610F:Leader \u5B57\u6BB5\u662F\u767B\u5F55\u4EBA\u5458\u7684\u5E10\u53F7\uFF0C\u4E0D\u662F\u4EBA\u5458\u540D\u79F0\uFF0C\u5982\u679C\u6CA1\u6709\u6B64\u5217\u7CFB\u7EDF\u5C31\u62A5\u9519\u3002   
   - \u8BF4\u660E\uFF1A\u5982\u679C\u5F53\u524D\u90E8\u95E8\u6CA1\u6709\u8D1F\u8D23\u4EBA\uFF0C\u5C31\u5411\u4E0A\u4E00\u7EA7\u90E8\u95E8\u53BB\u627E\u8D1F\u8D23\u4EBA\uFF0C\u5982\u679C\u5728\u6CA1\u6709\uFF0C\u5C31\u63D0\u793A\u9519\u8BEF\u3002  
        
    `);E(this,"ByDept",`
  #### \u8BF4\u660E  
   - \u8282\u70B9\u7ED1\u5B9A\u90E8\u95E8\u5C31\u662F\u8BE5\u8282\u70B9\u4E0B\u7ED1\u5B9A\u90E8\u95E8\u91CC\u9762\u7684\u6240\u6709\u4EBA\u5458\u90FD\u53EF\u4EE5\u63A5\u53D7\u8BE5\u5DE5\u4F5C.
          
  `);E(this,"ByEmpLeader",`
 
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u53D1\u9001\u4EBA\u7684\u76F4\u5C5E\u9886\u5BFC\uFF0C\u5904\u7406\u8BE5\u5DE5\u4F5C\u3002 
   - \u4FE1\u606F\uFF0C\u5B58\u50A8\u5230\u8868 Port_Emp \u5B57\u6BB5\uFF1ALeader \u4E2D.  
   - \u8BF4\u660E\uFF1A\u4F7F\u7528\u672C\u89C4\u5219\u524D\uFF0C\u8BF7\u914D\u7F6E\u76F8\u5E94\u4EBA\u5458\u7684\u76F4\u5C5E\u90E8\u95E8\u7684Leader\uFF01 
   #### \u6280\u672F\u8BF4\u660E
  - Port_Emp \u662F\u4EBA\u5458\u8868, Leader \u5B57\u6BB5\u5C31\u662F\u672C\u90E8\u95E8\u7684\u8D1F\u8D23\u4EBA.
  - Port_Emp.Leader \u662F\u5B58\u50A8\u7684\u90E8\u95E8\u9886\u5BFC\u767B\u5F55\u8D26\u53F7.
  - \u5982\u679C\u627E\u4E0D\u5230(\u6CA1\u6709\u8BBE\u7F6E)\uFF0C\u90E8\u95E8\u7684\u9886\u5BFC\uFF0C\u7CFB\u7EDF\u9ED8\u8BA4\u4F1A\u5411\u90E8\u95E8\u9886\u5BFC\u53BB\u627E.
  `);E(this,"BySenderParentDeptLeader",`
  #### \u4EC0\u4E48\u662F\u4EBA\u5458\u8EAB\u4EFD
   - \u5C31\u662F\u6309\u7167\u90A3\u4E2A\u4EBA\u5458\u6765\u8BA1\u7B97\u5F53\u524D\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\uFF0C\u9ED8\u8BA4\u662F\u8C01\u64CD\u4F5C\u7684\u6309\u7167\u8C01\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002
   - \u6BD4\u5982\uFF1A\u6211\u8981\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u5C31\u6309\u7167\u5F53\u524D\u64CD\u4F5C\u5458\u7684\u8EAB\u4EFD\u8BA1\u7B97\u3002
   - \u6709\u7684\u73AF\u5883\u4E0B\u5E76\u4E0D\u662F\u6309\u7167\u5F53\u524D\u4EBA\u5458\u8BA1\u7B97\u3002
   - \u6BD4\u5982\uFF1A\u6211\u8981\u4E3A\u522B\u4EBA\u8BF7\u5047\uFF0C\u627E\u90E8\u95E8\u8D1F\u8D23\u4EBA\uFF0C\u8981\u6309\u7167\u6307\u5B9A\u7684\u5B57\u6BB5\uFF08\u8BE5\u5B57\u6BB5\u5C31\u662F\u88AB\u8BF7\u5047\u4EBA\u5458\u7684\u8D26\u53F7\uFF09\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD\u3002 
  #### \u8BF4\u660E
   - \u4E0A\u4E00\u4E2A\u8282\u70B9\u53D1\u9001\u4EBA\u7684\u76F4\u5C5E\u9886\u5BFC\uFF0C\u5904\u7406\u8BE5\u5DE5\u4F5C\u3002
   - \u90E8\u95E8\u8D1F\u8D23\u4EBA\u4FE1\u606F\uFF0C\u5B58\u50A8\u5230\u8868 Port_Dept \u5B57\u6BB5\uFF1ALeader \u4E2D.
   - \u6CE8\u610F:Leader \u5B57\u6BB5\u662F\u767B\u5F55\u4EBA\u5458\u7684\u5E10\u53F7\uFF0C\u4E0D\u662F\u4EBA\u5458\u540D\u79F0\uFF0C\u5982\u679C\u6CA1\u6709\u6B64\u5217\u7CFB\u7EDF\u5C31\u62A5\u9519\u3002
   - \u5982\u679C\u5F53\u524D\u90E8\u95E8\u6CA1\u6709\u8D1F\u8D23\u4EBA\uFF0C\u5C31\u5411\u4E0A\u4E00\u7EA7\u90E8\u95E8\u53BB\u627E\u8D1F\u8D23\u4EBA\uFF0C\u5982\u679C\u8FD8\u6CA1\u6709\uFF0C\u5C31\u63D0\u793A\u9519\u8BEF\u3002 
`);E(this,"BySenderParentDeptStations",`
  #### \u5E2E\u52A9   
   - \u6307\u5B9A\u8282\u70B9\u53D1\u9001\u4EBA\u7684\u4E0A\u7EA7\u90E8\u95E8\u6240\u5728\u89D2\u8272\u4E0B\u7684\u6240\u6709\u4EBA\u5458\u3002 
   - \u6CE8\u610F\uFF1A\u4E0A\u7EA7\u90E8\u95E8\u5FC5\u987B\u7ED1\u5B9A\u89D2\u8272 
`);E(this,"ByStarter",`
  #### \u5E2E\u52A9       
   - \u5F53\u524D\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4E0E\u5F00\u59CB\u8282\u70B9\u4E00\u81F4\uFF0C\u53D1\u8D77\u4EBA\u662F zhangsan,\u73B0\u5728\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4E5F\u662Fzhangsan\u3002   
   - \u591A\u7528\u4E8E\u53CD\u9988\u7ED9\u7533\u8BF7\u4EBA\u8282\u70B9\uFF0C\u901A\u77E5\u7533\u8BF7\u4EBA\u5BA1\u6279\u5BA1\u6838\u7ED3\u679C\uFF0C\u6B64\u5DE5\u4F5C\u5DF2\u7ECF\u5BA1\u6838\u5BA1\u6279\u5B8C\u6BD5\u3002
   #### \u56FE\u4F8B
   
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow3.png "\u5C4F\u5E55\u622A\u56FE")

`);E(this,"ByPreviousNodeEmp",`
  #### \u5E2E\u52A9      
   - \u8282\u70B9A\u662Fzhangsan\u5904\u7406\uFF0C\u53D1\u9001\u5230\u8282\u70B9B,\u4E5F\u662F\u9700\u8981zhangsan\u5904\u7406\u3002 
   - \u5C31\u662F\u81EA\u5DF1\u53D1\u9001\u7ED9\u81EA\u5DF1\u7684\u6A21\u5F0F\u3002 
`);E(this,"BySpecNodeEmp",`
  #### \u5E2E\u52A9  
   - \u5F53\u524D\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4E0E\u6307\u5B9A\u7684\u8282\u70B9\u5904\u7406\u4EBA\u76F8\u540C\u3002
   - \u6240\u6307\u5B9A\u7684\u8282\u70B9\u4E00\u822C\u90FD\u662F\u5F53\u524D\u8282\u70B9\u4EE5\u524D\u7684\u8282\u70B9\uFF0C\u7531\u4E8E\u5206\u652F\u539F\u56E0\u4F1A\u5BFC\u81F4\u5386\u53F2\u7684\u8282\u70B9\u6709\u591A\u4E2A\u3002
   - \u5982\u679C\u51FA\u73B0\u591A\u4E2A\uFF0C\u7CFB\u7EDF\u5C31\u4F1A\u6309\u7167\u8282\u70B9\u7684\u53D1\u751F\u65F6\u95F4\u6392\u5E8F\u7684\u7B2C\u4E00\u4E2A\u8282\u70B9\u8BA1\u7B97\u3002  
      
`);E(this,"BySFTable",`
#### \u5E2E\u52A9
 -  \u7ED1\u5B9A\u7684\u5B57\u5178\u8BA1\u7B97.
 - \u8BF7\u53C2\u8003\u5B57\u5178\u7684\u6982\u5FF5\uFF0C\u5E76\u5B9A\u4E49\u5B57\u5178\u8868.
 -  \u5B57\u5178\u7EF4\u62A4: \u7CFB\u7EDF\u8BBE\u7F6E=>\u5B57\u5178\u7EF4\u62A4.
`);E(this,"BySQL",`
  #### \u5E2E\u52A9
   -  \u8BE5SQL\u662F\u9700\u8981\u8FD4\u56DENo,Name\u4E24\u4E2A\u5217\uFF0C\u5206\u522B\u662F\u4EBA\u5458\u7F16\u53F7,\u4EBA\u5458\u540D\u79F0\uFF0C\u8FD4\u56DE\u7684\u6570\u636E\u5FC5\u987B\u6309\u7167\u987A\u5E8F\u6765\u3002
   -  SQL\u8BED\u53E5\u652F\u6301ccbpm\u8868\u8FBE\u5F0F, \u6BD4\u5982\uFF1ASELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'
   -  \u6BD4\u5982\uFF1ASELECT No,Name FROM Port_Emp WHERE FK_Dept='@MyFieldName' MyFieldName \u5FC5\u987B\u662F\u8282\u70B9\u8868\u5355\u5B57\u6BB5.
   -  \u4EC0\u4E48\u662Fccbpm\u8868\u8FBE\u5F0F\uFF0C\u8BF7\u767E\u5EA6\uFF1Accbpm \u8868\u8FBE\u5F0F\u3002
   -  \u6CE8\u610F\uFF1A1. \u533A\u5206\u5927\u5C0F\u5199\u30022. \u987A\u5E8F\u4E0D\u80FD\u53D8\u5316, No,Name 
   #### \u5176\u4ED6
   - \u590D\u6742\u7684\uFF0C\u5E38\u7528\u7684sql,\u53EF\u4EE5\u4F7F\u7528 \u2018\u6309\u8BBE\u7F6E\u7684SQLTempate\u83B7\u53D6\u63A5\u53D7\u4EBA\u8BA1\u7B97\u2019 .
      
`);E(this,"BySQLTemplate",`
  #### \u5E2E\u52A9
   - \u5BF9\u4E8E\u7ECF\u5E38\u4F7F\u7528\u7684\u3001\u590D\u6742\u7684\u7528sql\u8868\u8FBE\u7684\u63A5\u6536\u4EBA\u573A\u666F\uFF0C\u4FDD\u5B58\u8D77\u6765\uFF0C\u5728\u5176\u4ED6\u8282\u70B9\u4E0A\u53EF\u4EE5\u8C03\u7528.
   - SQL\u6A21\u677F\u53D8\u5316\u540E\uFF0C\u5176\u4ED6\u8282\u70B9\u8DDF\u7740\u53D8\u5316. \u662F\u5F15\u7528\u5173\u7CFB\uFF0C\u800C\u4E0D\u662F\u590D\u5236\u5173\u7CFB.
   - \u573A\u666F: \u5F00\u53D1\u4EBA\u5458\u628A\u89C4\u5219\u914D\u7F6E\u597D\uFF0C\u4E1A\u52A1\u4EBA\u5458\u53EF\u4EE5\u8C03\u7528.
   #### \u6280\u672F\u4FE1\u606F
   - \u6570\u636E\u5B58\u50A8\u5728 wf_sqltemplate \u91CC\u9762. 
   - \u53EF\u4EE5\u624B\u5DE5\u7684\u7EF4\u62A4\u4E0A.
`);E(this,"BySQLAsSubThreadEmpsAndData",`
 
  #### \u5E2E\u52A9
   - \u6B64\u65B9\u6CD5\u4E0E\u5206\u5408\u6D41\u76F8\u5173\uFF0C\u53EA\u6709\u5F53\u524D\u8282\u70B9\u662F\u5B50\u7EBF\u7A0B\u624D\u6709\u610F\u4E49\u3002
              
`);E(this,"BySelected",`
  #### \u5E2E\u52A9

   - A\u8282\u70B9\u53D1\u9001\u5230B\u8282\u70B9\uFF0C\u5982\u679CB\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\u662F\u7531A\u6765\u9009\u62E9\u7684,\u8FD9\u6837\u7684\u884C\u4E3A\u7C7B\u4F3C\u4E8E\u53D1\u9001\u90AE\u4EF6\u6A21\u5F0F\uFF0C\u5C31\u662F\u6211\u9009\u62E9\u8C01\u5C31\u53D1\u9001\u7ED9\u8C01.
   - \u8FD9\u79CD\u6A21\u5F0F\u662F\u6709\u4E0A\u4E00\u4E2A\u8282\u70B9\u7684\u53D1\u9001\u4EBA\u4E3B\u89C2\u5224\u65AD\u7684\uFF0C\u800C\u975E\u81EA\u52A8\u8BA1\u7B97\u3002
   - \u6D77\u9009:\u5C31\u662F\u53EF\u4EE5\u9009\u62E9\u4EFB\u4F55\u4EBA,\u5982\u4E0B\u56FE\uFF0C\u53EF\u4EE5\u5728\u6587\u672C\u6846\u91CC\u641C\u7D22\u3002
  #### \u901A\u7528\u7684\u4EBA\u5458\u9009\u62E9\u5668\u6548\u679C\u56FE
 - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/BySelected.png "\u5C4F\u5E55\u622A\u56FE.png")
      
`);E(this,"BySelectedFix",`
  #### \u5E2E\u52A9
   - A\u8282\u70B9\u53D1\u9001\u5230B\u8282\u70B9\uFF0C\u5982\u679CB\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\u662F\u7531A\u6765\u9009\u62E9\u7684,\u8FD9\u6837\u7684\u884C\u4E3A\u7C7B\u4F3C\u4E8E\u53D1\u9001\u90AE\u4EF6\u6A21\u5F0F\uFF0C\u5C31\u662F\u6211\u9009\u62E9\u8C01\u5C31\u53D1\u9001\u7ED9\u8C01.
   - \u8FD9\u79CD\u6A21\u5F0F\u662F\u6709\u4E0A\u4E00\u4E2A\u8282\u70B9\u7684\u53D1\u9001\u4EBA\u4E3B\u89C2\u5224\u65AD\u7684\uFF0C\u800C\u975E\u81EA\u52A8\u8BA1\u7B97\u3002
   - \u56FA\u5B9A\u8303\u56F4\u7684\u9009\u62E9:\u5C31\u662F\u9700\u8981\u8BBE\u7F6E\u9009\u62E9\u4EBA\u7684\u8303\u56F4\uFF0C\u6BD4\u5982\uFF1A\u6309\u7167SQL,\u90E8\u95E8\uFF0C\u4EBA\u5458\u8BBE\u5B9A\u4EC5\u4EC5\u9009\u62E9\u526F\u5C40\u957F\uFF0C\u526F\u90E8\u957F.
  #### \u56FA\u5B9A\u8303\u56F4\u4EBA\u5458\u9009\u62E9\u5668\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/BySelectedFix.png "\u5C4F\u5E55\u622A\u56FE.png")
      
`);E(this,"BySelfUrl",`

  #### \u5E2E\u52A9
   -  \u8BE5URL\u662F\u70B9\u51FB\u53D1\u9001\u76F4\u63A5\u5F39\u51FA\u81EA\u5B9A\u4E49\u7684\u4EBA\u5458\u9009\u62E9\u7684\u9875\u9762\uFF0C\u9700\u8981\u5F00\u53D1\u4EBA\u5458\u5C06\u63A5\u6536\u4EBA\u7684\u4FE1\u606F\u4FDD\u5B58\u5230 WF_SelectAccepter\u8868\u91CC.
   -  \u7CFB\u7EDF\u5C06\u4F1A\u628A\u5F53\u524D\u8282\u70B9\u7684\u4FE1\u606F\u4F20\u5165\u5230\u60A8\u7684url\u91CC\u9762\u53BB\uFF0C\u6BD4\u5982\uFF1AFK_Node,WorkID,FK_Flow
   -  \u5728\u9009\u62E9\u5B8C\u6BD5\u540E,\u60A8\u9700\u8981\u5C06\u9009\u62E9\u7684\u7528\u6237ID,\u5B58\u50A8\u5230\u5230\u63A5\u53E3\u91CC\u9762\u53BB\u3002

  #### DEMO 
   - /DataUser/PopSelf.htm
      
`);E(this,"ByAPIUrl",`
 
  #### \u5E2E\u52A9
   - \u8BF7\u8BBE\u7F6Ewebpai\uFF0C \u70B9\u51FBwebApi\u8BBE\u7F6E\u6309\u94AE.
   - \u8FD4\u56DE\u7684\u6570\u636E\u683C\u5F0F\u4E3A: zhangsan,lisi,wangwu
   - \u591A\u4E2A\u4EBA\u5458\u7528\u9017\u53F7\u5206\u5F00.
      
`);E(this,"ByPreviousNodeFormEmpsField",`
  #### \u5E2E\u52A9
   - \u5728\u8BBE\u8BA1\u8282\u70B9\u8868\u5355\u7684\u65F6\uFF0C\u589E\u52A0\u4E00\u4E2A\u4EBA\u5458\u4FE1\u606F\u5B57\u6BB5\u3002\u8BE5\u5B57\u6BB5\u7684\u5185\u5BB9\u505A\u4E3A\u672C\u8282\u70B9\u7684\u63A5\u6536\u4EBA.
  #### \u8FD0\u884C\u56FE
  -  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/EmpsField.png "\u5C4F\u5E55\u622A\u56FE.png")
      
`);E(this,"ByDtlAsSubThreadEmps",`
 
  #### \u5E2E\u52A9
   - \u9002\u7528\u4E8E\u5206\u5408\u6D41\u573A\u666F\uFF0C\u5F53\u524D\u8282\u70B9\u7684\u4E0A\u4E00\u4E2A\u8282\u70B9\u662F\u5206\u6D41\u8282\u70B9, \u5F53\u524D\u8282\u70B9\u662F\u5B50\u7EBF\u7A0B\u6709\u6548.
   - \u4E0A\u4E00\u4E2A\u8282\u70B9\u5728\u4ECE\u8868\u91CC\u91C7\u96C6\u5230\u63A5\u6536\u4EBA\u4FE1\u606F\uFF0C\u505A\u4E3A\u5F53\u524D\u8282\u70B9\u7684\u63A5\u6536\u4EBA.
   - \u6B64\u65B9\u6CD5\u4E0E\u5206\u5408\u6D41\u76F8\u5173\uFF0C\u53EA\u6709\u5F53\u524D\u8282\u70B9\u662F\u5B50\u7EBF\u7A0B\u624D\u6709\u610F\u4E49\u3002
   - \u5F53\u524D\u53C2\u6570\u4E3A\u660E\u7EC6\u8868\u7684\u5B57\u6BB5\u5217\uFF0C\u5982\u679C\u4E0D\u586B\u5199\uFF0C\u5C31\u9ED8\u8BA4\u4E3A UserNo \u3002
      
`);E(this,"ByFEE",`
 
  #### \u5E2E\u52A9

  - \u7528\u6D41\u7A0B\u4E8B\u4EF6,\u901A\u8FC7\u8C03\u7528\u8BBE\u7F6E\u63A5\u53D7\u7684\u63A5\u53E3,\u6765\u8BBE\u7F6E\u5F53\u524D\u8282\u70B9\u7684\u63A5\u6536\u4EBA,\u5B9E\u73B0\u7684\u628A\u63A5\u53D7\u4EBA\u4FE1\u606F\u5199\u5165\u63A5\u6536\u4EBA\u5217\u8868\u91CC\u3002
  - \u8FD9\u91CC\u9700\u8981\u542F\u52A8\u6D41\u7A0B\u4E8B\u4EF6,\u5728\u4E8B\u4EF6\u91CC\u52A8\u6001\u7684,\u7528\u7A0B\u5E8F\u6765\u8BA1\u7B97\u63A5\u53D7\u4EBA.
`);E(this,"ByFromEmpToEmp",`
 
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u6309\u7167\u683C\u5F0F\u914D\u7F6E\u4EBA\u5458\u5230\u4EBA\u5458\u7684\u63A5\u6536\u4EBA\u7684\u8DEF\u5F84,\u6BCF\u4E2A\u4EBA\u5458\u7684\u4E0B\u4E00\u4E2A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u662F\u56FA\u5B9A\u7684.
  - \u683C\u5F0F\u4E3A @zhangsan,lisi@wangwu,zhaoliu \u8BF4\u660E\uFF1A\u5982\u679C\u662F\u5F20\u4E09\u53D1\u9001\u7684\u5C31\u53D1\u9001\u5230\u674E\u56DB\u8EAB\u4E0A. \u591A\u4E2A\u4EBA\u5458\u5BF9\u7528@\u5206\u5F00\u3002
  - \u9ED8\u8BA4\u63A5\u53D7\u4EBA\u5217\u8868\uFF0C\u5C31\u6309\u7167\u9ED8\u8BA4\u503C\u5BFB\u627E: @Defualt,zhangsan \u7740\u4E00\u6837\u914D\u7F6E\u8868\u793A\uFF0C\u6CA1\u6709\u627E\u5230\u4EBA\u5C31\u6309\u7167\u9ED8\u8BA4\u503C\u6295\u9012\u3002
  - \u5E94\u7528\u573A\u666F: 1. \u4EBA\u5458\u89C4\u5219\u6BD4\u8F83\u56FA\u5B9A. 2.\u6570\u636E\u91CF\u8F83\u5C0F.  3. \u4E34\u65F6\u6027\u7684\u9879\u76EE\u7EC4\u5DE5\u4F5C.
  #### \u914D\u7F6E\u6548\u679C\u56FE
  -  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ByFromEmpToEmp.png "\u5C4F\u5E55\u622A\u56FE.png")

      
`);this.PageTitle="\u63A5\u6536\u4EBA\u89C4\u5219"}Init(){return s(this,null,function*(){this.entity=new f,this.KeyOfEn=B.DeliveryWay,this.Icon="icon-user-following",this.entity.setPKVal(this.PKVal),yield this.entity.Retrieve();const e=this.PKVal;if(!e||typeof e!="string")throw new Error("\u521D\u59CB\u5316\u5931\u8D25\uFF0C\u672A\u83B7\u53D6\u5230\u4E3B\u952E");if(e.endsWith("01")){this.PageTitle="\u53D1\u8D77\u4EBA\u89C4\u5219",this.AddGroup("A","\u6D41\u7A0B\u53D1\u8D77\u4EBA\u8BBE\u7F6E"),this.Blank(A.BySelected,"\u6240\u6709\u4EBA\u90FD\u53EF\u4EE5\u53D1\u8D77(\u9ED8\u8BA4)",this.Starter_AnyOne),this.AddEntity(A.ByStation,"\u7ED1\u5B9A\u89D2\u8272\u8BA1\u7B97",new v,this.Starter_Station),this.AddEntity(A.ByBindEmp,"\u6309\u7ED1\u5B9A\u4EBA\u5458\u8BA1\u7B97",new b,this.Starter_Emps),this.AddEntity(A.ByDept,"\u6309\u7ED1\u5B9A\u90E8\u95E8\u8BA1\u7B97",new w,this.Starter_Dept),this.AddEntity(A.ByDeptAndStation,"\u6309\u7ED1\u5B9A\u7684\u89D2\u8272\u4E0E\u90E8\u95E8\u4EA4\u96C6\u8BA1\u7B97",new W,this.Starter_DeptAndStation),this.Blank(A.ByGuest,"\u4EC5\u5916\u90E8\u7528\u6237\u53EF\u53D1\u8D77",this.Starter_Guests);return}this.Btns=[{pageNo:A.ByAPIUrl.toString(),list:["\u8BBE\u7F6EWebApi"]},{pageNo:A.ByStation.toString(),list:["\u8EAB\u4EFD\u89C4\u5219"]},{pageNo:A.ByStationSpecDepts.toString(),list:["\u90E8\u95E8\u8303\u56F4\u89C4\u5219"]},{pageNo:A.ByStationSpecStas.toString(),list:["\u89D2\u8272\u8303\u56F4\u89C4\u5219"]},{pageNo:A.BySelected_2.toString(),list:["\u8BBE\u7F6E\u8303\u56F4"]}],this.AddGroup("A","\u6309\u7EC4\u7EC7\u7ED3\u6784\u7ED1\u5B9A"),this.AddEntity(A.ByStation,"\u6309\u89D2\u8272\u667A\u80FD\u8BA1\u7B97",new M,this.ByStation),this.AddEntity(A.ByStationOnly,"\u4EC5\u6309\u7ED1\u5B9A\u7684\u89D2\u8272\u8BA1\u7B97",new v,this.ByStationOnly),this.AddEntity(A.ByStationSpecDepts,"\u6309\u6307\u5B9A\u7684\u90E8\u95E8\u96C6\u5408\u4E0E\u8BBE\u7F6E\u7684\u89D2\u8272\u4EA4\u96C6\u8BA1\u7B97",new K,this.ByStationSpecDepts),this.AddEntity(A.ByStationSpecStas,"\u6309\u6307\u5B9A\u7684\u89D2\u8272\u96C6\u5408\u4E0E\u8BBE\u7F6E\u7684\u90E8\u95E8\u4EA4\u96C6\u8BA1\u7B97",new K,this.ByStationSpecDepts),this.AddEntity(A.ByBindEmp,"\u6309\u7ED1\u5B9A\u7684\u4EBA\u5458\u8BA1\u7B97",new b,this.ByBindEmp),this.AddEntity(A.ByDeptAndStation,"\u6309\u7ED1\u5B9A\u7684\u89D2\u8272\u4E0E\u90E8\u95E8\u4EA4\u96C6\u8BA1\u7B97",new W,this.ByDeptAndStation),this.SingleTB(A.BySpecNodeEmpStation,"\u6309\u6307\u5B9A\u8282\u70B9\u7684\u4EBA\u5458\u89D2\u8272\u8BA1\u7B97",B.DeliveryParas,this.BySpecNodeEmp,"\u8BF7\u8F93\u5165\u8282\u70B9ID"),this.AddEntity(A.BySetDeptAsSubthread,"\u6309\u7ED1\u5B9A\u90E8\u95E8\u8BA1\u7B97\uFF0C\u8BE5\u90E8\u95E8\u4E00\u4EBA\u5904\u7406\u6807\u8BC6\u8BE5\u5DE5\u4F5C\u7ED3\u675F(\u5B50\u7EBF\u7A0B)",new w,this.BySetDeptAsSubthread),this.AddEntity(A.FindSpecDeptEmps,"\u627E\u672C\u90E8\u95E8\u8303\u56F4\u5185\u7684\u89D2\u8272\u96C6\u5408\u91CC\u9762\u7684\u4EBA\u5458",new w,this.FindSpecDeptEmps),this.Blank(A.ByDeptLeader,"\u627E\u672C\u90E8\u95E8\u7684\u9886\u5BFC(\u4E3B\u7BA1,\u8D1F\u8D23\u4EBA,Port_Dept.Leader)",this.ByDeptLeader);const t="SELECT NodeID as No, Name FROM WF_Node WHERE FK_Flow='@FK_Flow'";this.SingleDDLSQL(A.ByEmpLeader,"\u627E\u6307\u5B9A\u8282\u70B9\u7684\u4EBA\u5458\u76F4\u5C5E\u9886\u5BFC(\u4E3B\u7BA1,\u8D1F\u8D23\u4EBA,Port_Emp.Leader)","DeliveryParas",this.ByEmpLeader,t,!0),this.AddEntity(A.ByDept,"\u6309\u7ED1\u5B9A\u7684\u90E8\u95E8\u8BA1\u7B97",new w,this.ByDept),this.AddGroup("Y","\u8FDE\u7EED\u591A\u7EA7\u4E3B\u7BA1"),this.AddEntity(A.ByMLeader0,"\u6307\u5B9A\u89D2\u8272\u6A21\u5F0F",new Au,""),this.SingleTB(A.ByMLeader1,"\u6307\u5B9A\u901A\u8BAF\u5F55\u6A21\u5F0F","DeliveryParas",this.HelpUn,"\u8BF7\u8F93\u5165\u7EA7\u522B",yu.AppInt),this.AddGroup("B","\u6309\u4E0A\u4E00\u4E2A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u8EAB\u4EFD"),this.Blank(A.BySenderParentDeptLeader,"\u53D1\u9001\u4EBA\u4E0A\u7EA7\u90E8\u95E8\u7684\u8D1F\u8D23\u4EBA",this.ByDeptLeader),this.Blank(A.BySenderParentDeptStations,"\u53D1\u9001\u4EBA\u4E0A\u7EA7\u90E8\u95E8\u89D2\u8272\u4E0B\u7684\u4EBA\u5458(\u9700\u7ED1\u5B9A\u89D2\u8272)",this.ByDeptLeader),this.AddGroup("C","\u6309\u6307\u5B9A\u8282\u70B9\u5904\u7406\u4EBA"),this.Blank(A.ByStarter,"\u4E0E\u5F00\u59CB\u8282\u70B9\u5904\u7406\u4EBA\u76F8\u540C",this.ByStarter),this.Blank(A.ByPreviousNodeEmp,"\u4E0E\u4E0A\u4E00\u8282\u70B9\u5904\u7406\u4EBA\u76F8\u540C",this.ByPreviousNodeEmp),this.SingleTB(A.BySpecNodeEmp,"\u4E0E\u6307\u5B9A\u8282\u70B9\u5904\u7406\u4EBA\u76F8\u540C",B.DeliveryParas,this.BySpecNodeEmp,"\u8BF7\u8F93\u5165\u8282\u70B9ID"),this.AddGroup("D","\u6309\u81EA\u5B9A\u4E49SQL\u67E5\u8BE2"),this.SingleTBSQL(A.BySQL,"\u6309\u8BBE\u7F6E\u7684SQL\u83B7\u53D6\u63A5\u53D7\u4EBA\u8BA1\u7B97",B.DeliveryParas,this.BySQL),this.SingleTBSQL(A.BySQLAsSubThreadEmpsAndData,"\u6309SQL\u786E\u5B9A\u5B50\u7EBF\u7A0B\u63A5\u53D7\u4EBA\u4E0E\u6570\u636E\u6E90",B.DeliveryParas,this.BySQLAsSubThreadEmpsAndData),this.AddEntity(A.BySFTable,"\u7ED1\u5B9A\u5B57\u5178(\u591A\u53C2\u6570)",new tu,this.BySQL),this.Blank(A.ByAPIUrl,"\u6309\u7167WebAPI\u8BA1\u7B97",this.ByAPIUrl),gu.CustomNo=="CCFlow"&&(this.AddEntity(A.ZhieJieShangJi501,"\u76F4\u63A5\u4E0A\u7EA7",new x,this.ByStationOnly),this.AddEntity(A.ZhieJieShangJi502,"\u76F4\u63A5\u4E0A2\u7EA7",new x,this.ByStationOnly)),this.AddGroup("E","\u4F7F\u7528\u4EBA\u5458\u9009\u62E9\u5668-\u4E3B\u89C2\u9009\u62E9"),this.AddEntity(A.BySelected,"\u81EA\u7531\u9009\u62E9(\u6D77\u9009)",new G,this.BySelected),this.AddEntity(A.BySelected_2,"\u56FA\u5B9A\u8303\u56F4\u9009\u62E9",new L,this.BySelectedFix),this.SingleTB(A.BySelfUrl,"\u81EA\u5B9A\u4E49\u4EBA\u5458\u9009\u62E9\u5668",B.DeliveryParas,this.BySelfUrl,"\u8BF7\u8F93\u5165\u81EA\u5B9A\u4E49\u7684url"),this.AddGroup("F","\u4F7F\u7528\u4EBA\u5458\u9009\u62E9\u5668-\u9884\u5148\u9009\u62E9"),this.AddEntity(A.PreplaceWokerFree,"\u81EA\u7531\u9009\u62E9(\u6D77\u9009)",new G,this.BySelected),this.AddEntity(A.PreplaceWokerFix,"\u56FA\u5B9A\u8303\u56F4\u9009\u62E9",new L,this.BySelectedFix),this.AddGroup("G","\u8282\u70B9\u8868\u5355\u5B57\u6BB5");let F="ND"+this.GetRequestVal("FlowNo")+"Rpt";F=F.replace("ND000",""),F=F.replace("ND00",""),F=F.replace("ND0",""),F="ND"+F;const D=`SELECT KeyOfEn as No, Name FROM Sys_MapAttr WHERE FK_MapData='${F}'
     AND KeyOfEn Not IN ('AtPara','OID','RDT','GUID','Title','Rec','CDT') 
    AND  MyDataType=1 `;this.SingleDDLSQL(A.ByPreviousNodeFormEmpsField,"\u4E3B\u8868\u4EBA\u5458\u7F16\u53F7\u5B57\u6BB5","DeliveryParas",this.ByPreviousNodeFormEmpsField,D,!0),this.SingleDDLSQL(A.ByPreviousNodeFormEmpsFrmDtl,"\u4ECE\u8868\u91CC\u7684\u4EBA\u5458\u7F16\u53F7\u5B57\u6BB5","DeliveryParas",this.ByPreviousNodeFormEmpsField,D,!0),this.SingleDDLSQL(A.ByPreviousNodeFormDepts,"\u5B57\u6BB5\u662F\u90E8\u95E8\u7F16\u53F7(\u6309\u90E8\u95E8\u7684\u9886\u5BFC\u8BA1\u7B97)","DeliveryParas",this.ByPreviousNodeFormEmpsField,D,!0),this.SingleTB(A.ByPreviousNodeFormStationsAI,"\u5B57\u6BB5\u662F\u5C97\u4F4D\u7F16\u53F7(\u6309\u5C97\u4F4D\u667A\u80FD\u8BA1\u7B97)",B.DeliveryParas,this.ByPreviousNodeFormEmpsField,"\u8BF7\u8F93\u5165\u5B57\u6BB5\u540D\u79F0"),this.SingleTB(A.ByPreviousNodeFormStationsOnly,"\u5B57\u6BB5\u662F\u5C97\u4F4D\u7F16\u53F7(\u4EC5\u6309\u5C97\u4F4D\u8BA1\u7B97)",B.DeliveryParas,this.ByPreviousNodeFormEmpsField,"\u8BF7\u8F93\u5165\u5B57\u6BB5\u540D\u79F0"),this.SingleTB(A.ByPreviousNodeFormEmpsTeam,"\u5B57\u6BB5\u662F\u6743\u9650\u7EC4",B.DeliveryParas,this.ByPreviousNodeFormEmpsField,"\u8BF7\u8F93\u5165\u5B57\u6BB5\u540D\u79F0"),this.Blank(A.ByDtlAsSubThreadEmps,"\u7531\u4E0A\u4E00\u8282\u70B9\u7684\u660E\u7EC6\u8868\u6765\u51B3\u5B9A\u5B50\u7EBF\u7A0B\u7684\u63A5\u53D7\u4EBA",this.ByDtlAsSubThreadEmps),this.AddGroup("Z","\u5176\u5B83\u65B9\u5F0F"),this.SingleTextArea(A.ByFromEmpToEmp,"\u6309\u7167\u914D\u7F6E\u7684\u4EBA\u5458\u8DEF\u7531\u5217\u8868\u8BA1\u7B97",B.DeliveryParas,"\u8BF7\u6309\u7167\u89C4\u5219\u8F93\u5165\u53C2\u6570",this.ByFromEmpToEmp),this.Blank(A.ByFEE,"\u7531FEE\u6765\u51B3\u5B9A",this.ByFEE)})}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){return s(this,null,function*(){if(F==="\u8EAB\u4EFD\u89C4\u5219"){const D=g.UrlGPE(new j,this.PKVal);return new _(I.OpenUrlByDrawer75,D)}if(F==="\u90E8\u95E8\u8303\u56F4\u89C4\u5219"){const D=g.UrlGPE(new q,this.PKVal);return new _(I.OpenUrlByDrawer75,D)}if(F==="\u89D2\u8272\u8303\u56F4\u89C4\u5219"){const D=g.UrlGPE(new $,this.PKVal);return new _(I.OpenUrlByDrawer75,D)}if(F==="\u8BBE\u7F6E\u8303\u56F4"){const D=g.UrlGPE(new J,this.PKVal);return new _(I.OpenUrlByDrawer75,D)}if(e==A.ByAPIUrl.toString()&&F=="\u8BBE\u7F6EWebApi"){const D=this.PKVal,p="AR"+D,c=new Iu;if(yield c.Init(),c.setPKVal(p),c.MyPK=p,(yield c.RetrieveFromDBSources())==0){const T=new f;T.NodeID=D,yield T.RetrieveFromDBSources(),c.NodeID=D,c.FlowNo=T.FK_Flow,c.MyPK=p,yield c.Insert()}const m=g.UrlEn(c.classID,p);return new _(I.OpenUrlByDrawer75,m)}if(e==t||e===F)throw new Error("Method not implemented.")})}}const k5=Object.freeze(Object.defineProperty({__proto__:null,GPE_AccepterRole:ou},Symbol.toStringTag,{value:"Module"}));class y{}E(y,"BatchCheckNoteModel","BatchCheckNoteModel"),E(y,"BatchCheckListCount","BatchCheckListCount"),E(y,"BatchFields","BatchFields"),E(y,"EditFields","EditFields");class ru extends l{constructor(e){super("TS.WF.GPENodeBatchRole1");E(this,"ShowRows",`
  #### \u5E2E\u52A9
   - \u663E\u793A\u7684\u884C\u6570,\u5C31\u662F\u6BCF\u9875\u663E\u793A\u591A\u5C11\u6761\u8BB0\u5F55.
   - \u8BBE\u7F6E\u592A\u591A\u6279\u91CF\u5BA1\u6838\u5C31\u4F1A\u5BFC\u81F4\u7CFB\u7EDF\u592A\u6162.
   `);E(this,"Note1",`
  #### \u5E2E\u52A9
   - \u8BBE\u7F6E\u663E\u793A\u7684\u5217\u8868\u5B57\u6BB5\uFF0C\u591A\u4E2A\u5B57\u6BB5\u7528\u9017\u53F7\u5206\u5F00.
   - \u6BD4\u5982\uFF1A Tel,Addr,Email
   - \u8FD9\u91CC\u7684\u5B57\u6BB5\u662F\u53EF\u4EE5\u7F16\u8F91\u7684\u5B57\u6BB5.
   `);E(this,"Note2",`
   #### \u5E2E\u52A9
    - \u8BBE\u7F6E\u663E\u793A\u7684\u5217\u8868\u5B57\u6BB5\uFF0C\u591A\u4E2A\u5B57\u6BB5\u7528\u9017\u53F7\u5206\u5F00.
    - \u6BD4\u5982\uFF1A Tel,Addr,Email
    `);e&&(this.NodeID=e)}get HisUAC(){const e=new a;return e.IsDelete=!1,e.IsUpdate=!0,e.IsInsert=!1,e}get EnMap(){const e=new d("WF_Node","\u5BA1\u6838\u7EC4\u4EF6\u6A21\u5F0F");e.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),e.AddTBString(r.Name,null,"\u540D\u79F0",!0,!1,0,100,10),e.AddTBString(r.FK_Flow,null,"FK_Flow",!1,!1,0,100,10);const t="@0=\u9009\u62E9\u7684\u591A\u6761\u8BB0\u5F55\u4E00\u4E2A\u610F\u89C1\u6846@1=\u6BCF\u4E2A\u8BB0\u5F55\u540E\u9762\u90FD\u6709\u4E00\u4E2A\u610F\u89C1\u6846@2=\u65E0\u610F\u89C1";return e.AddDDLSysEnum(y.BatchCheckNoteModel,0,"\u586B\u5199\u610F\u89C1\u683C\u5F0F",!0,!0,"BatchCheckNoteModel",t,null,!0),e.AddTBInt(y.BatchCheckListCount,12,"\u663E\u793A\u884C\u6570",!0,!1,!1,this.ShowRows),e.AddTBString(y.BatchFields,null,"\u663E\u793A\u7684\u5B57\u6BB5",!0,!1,0,300,10,!0,this.Note1),e.AddTBString(y.EditFields,null,"\u53EF\u7F16\u8F91\u7684\u5B57\u6BB5",!0,!1,0,300,10,!0,this.Note2),e.AddTBAtParas(),e.ParaFields=",BatchCheckNoteModel,BatchCheckListCount,BatchFields,EditFields,",this._enMap=e,this._enMap}}const j5=Object.freeze(Object.defineProperty({__proto__:null,GPENodeBatchRole1:ru,GPENodeBatchRoleAttr:y},Symbol.toStringTag,{value:"Module"}));class su extends l{constructor(u){super("TS.WF.GPENodeBatchRole2"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u5BA1\u6838\u5B57\u6BB5\u5206\u7EC4\u6A21\u5F0F");u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u8282\u70B9\u540D",!0,!0,0,300,10,!0);const e="@0=\u9009\u62E9\u7684\u591A\u6761\u8BB0\u5F55\u4E00\u4E2A\u610F\u89C1\u6846@1=\u6BCF\u4E2A\u8BB0\u5F55\u540E\u9762\u90FD\u6709\u4E00\u4E2A\u610F\u89C1\u6846@2=\u65E0\u610F\u89C1";return u.AddDDLSysEnum("BatchCheckNoteModel",0,"\u586B\u5199\u610F\u89C1\u683C\u5F0F",!0,!0,"BatchCheckNoteModel",e),u.AddTBInt("BatchCheckListCount",0,"\u663E\u793A\u884C\u6570",!0,!1),u.AddTBString("BatchCheckNoteField",null,"\u8BBE\u7F6E\u5206\u7EC4\u5B57\u6BB5",!0,!1,0,300,10,!0),u.AddTBString("BatchFields",null,"\u663E\u793A\u7684\u5B57\u6BB5",!0,!1,0,300,10,!0),u.AddTBString("EditFields",null,"\u53EF\u7F16\u8F91\u7684\u5B57\u6BB5",!0,!1,0,300,10,!0),u.AddTBAtParas(),u.ParaFields=",BatchCheckNoteModel,BatchCheckListCount,BatchFields,EditFields,BatchCheckNoteField,",this._enMap=u,this._enMap}}const z5=Object.freeze(Object.defineProperty({__proto__:null,GPENodeBatchRole2:su},Symbol.toStringTag,{value:"Module"}));class Du extends N{constructor(){super("GPE_BatchRole");E(this,"Desc0",`
  #### \u5E2E\u52A9
  
   - \u9ED8\u8BA4\u4E3A\u4E0D\u5904\u7406\u3002
   - \u6279\u5904\u7406\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1 \u5BA1\u6838\u7EC4\u4EF6\u7684\u6279\u5904\u7406\uFF0C2.\u5BA1\u6838\u5206\u7EC4\u7684\u6279\u5904\u7406\u3002
   - \u5BA1\u6838\u7EC4\u4EF6\u7684\u6279\u5904\u7406\uFF1A\u662F\u5F53\u524D\u8282\u70B9\u542F\u7528\u4E86\u5BA1\u6838\u7EC4\u4EF6\uFF0C\u5BA1\u6838\u610F\u89C1\u7684\u65F6\u5019\u3002
   - \u5BA1\u6838\u5206\u7EC4\u7684\u6279\u5904\u7406\uFF1A\u662F\u91C7\u7528\u7ECF\u5178\u8868\u5355\u8BBE\u8BA1\u6A21\u5F0F\uFF0C\u8BBE\u8BA1\u7684\u5BA1\u6838\u5206\u7EC4\uFF0C\u7528\u6237\u586B\u5199\u610F\u89C1\u662F\u586B\u5199\u7684\u5BA1\u6838\u5206\u7EC4\u3002

 `);E(this,"Desc1",`

  #### \u8BF4\u660E

   - \u4EC5\u4EC5\u5BF9\u5F53\u524D\u8282\u70B9\u542F\u7528\u4E86\u5BA1\u6279\u7EC4\u4EF6(\u6216\u8005\u7B7E\u6279\u7EC4\u4EF6)\u6709\u6548.
   - \u5BA1\u6838\u7EC4\u4EF6\u7684\u4FE1\u606F\u4F1A\u8BB0\u5F55\u5230\u5BA1\u6838\u4FE1\u606F\u8868\u91CC\u9762.
   - \u901A\u8FC7\u8BBE\u7F6E\u6279\u91CF\u5BA1\u6279\u5C5E\u6027\u53EF\u4EE5\u7075\u6D3B\u7684\u6EE1\u8DB3\u4E0D\u540C\u7684\u5BA2\u6237\u9700\u6C42.
   
  #### \u8FD0\u884C\u6548\u679C\u56FE
  
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRole.png "\u5C4F\u5E55\u622A\u56FE.png")

  #### \u6D41\u7A0B\u6848\u4F8B\u56FE
  - \u51CF\u5211\u5047\u91CA\u6D41\u7A0B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow.png "\u5C4F\u5E55\u622A\u56FE.png")
  - \u6279\u6B21\u51CF\u5211\u6D41\u7A0B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow1.png "\u5C4F\u5E55\u622A\u56FE.png")

  
  #### \u914D\u7F6E\u8BF4\u660E

  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow2.png "\u5C4F\u5E55\u622A\u56FE.png")

 `);E(this,"Desc2",`
  #### \u5E2E\u52A9
  - \u5BF9\u4E8E\u8282\u70B9\u8868\u5355\u6709\u6548.
  - \u5EFA\u8BAE\u4F7F\u7528\u5BA1\u6838\u7EC4\u4EF6.
  #### \u5176\u5B83 
  - \u8BE5\u529F\u80FD\u57282022.10\u4EE5\u540E\u7684\u7248\u672C\u53D6\u6D88\u4E86.
  `);this.PageTitle="\u6279\u91CF\u5BA1\u6838"}Init(){this.entity=new f,this.KeyOfEn=B.BatchRole,this.AddGroup("A","\u8282\u70B9\u8868\u5355\u6279\u5904\u7406"),this.Blank("0","\u4E0D\u4F7F\u7528\u6279\u5904\u7406",this.Desc0),this.AddEntity("1","\u5BA1\u6838\u7EC4\u4EF6\u6A21\u5F0F\u6279\u5904\u7406",new ru,this.Desc1),this.AddEntity("2","\u5B57\u6BB5\u5206\u7EC4\u6A21\u5F0F\u6279\u5904\u7406",new su,this.Desc2)}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const Q5=Object.freeze(Object.defineProperty({__proto__:null,GPE_BatchRole:Du},Symbol.toStringTag,{value:"Module"}));class nu extends l{get NodeFrmID(){return this.GetValStringByKey("NodeFrmID")}constructor(u){super("TS.WF.EvaluationRole1"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u56FA\u5B9A\u65F6\u6548\u8003\u6838");return u.GroupBarShowModel=1,u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u540D\u79F0",!0,!1,0,50,200),u.AddDDLSysEnum("CHWayOfTimeRole",0,"\u65F6\u95F4\u8BA1\u7B97\u65B9\u5F0F",!0,!1,"CHWayOfTimeRole","@0=\u6309\u8BBE\u7F6E\u8BA1\u7B97@1=\u6309\u8868\u5355\u7684\u5B57\u6BB5\u8BA1\u7B97"),u.AddGroupAttr("\u6309\u8BBE\u7F6E\u8BA1\u7B97"),u.AddTBInt("TimeLimit",1,"\u5929\u6570",!0,!1),u.AddTBInt("TimeLimitHH",0,"\u5C0F\u65F6",!0,!1),u.AddTBInt("TimeLimitMM",0,"\u5206\u949F",!0,!1),u.AddTBAtParas(),u.ParaFields=",CHWayOfTimeRole,TimeLimit,TimeLimitHH,TimeLimitMM,",this._enMap=u,this._enMap}beforeUpdateInsertAction(){return s(this,null,function*(){return this.CHWayOfTimeRole=0,!0})}}const J5=Object.freeze(Object.defineProperty({__proto__:null,EvaluationRole1:nu},Symbol.toStringTag,{value:"Module"}));class Cu extends l{constructor(u){super("TS.WF.EvaluationRole4"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u6307\u5B9A\u5B57\u6BB5\u7684\u65F6\u95F4\u8003\u6838");u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u540D\u79F0",!0,!1,0,50,200),u.AddDDLSysEnum("CHWayOfTimeRole",1,"\u65F6\u95F4\u8BA1\u7B97\u65B9\u5F0F",!0,!1,"CHWayOfTimeRole","@0=\u6309\u8BBE\u7F6E\u8BA1\u7B97@1=\u6309\u8868\u5355\u7684\u5B57\u6BB5\u8BA1\u7B97"),u.AddGroupAttr("\u6309\u8868\u5355\u7684\u5B57\u6BB5\u8BA1\u7B97");const e="SELECT KeyOfEn as No, Name FROM Sys_MapAttr WHERE FK_MapData ='ND@NodeID' AND MyDataType IN (7,8) ";return u.AddDDLSQL("CHWayOfTimeRoleField",null,"\u9009\u5B57\u6BB5(\u5BF9\u8868\u5355\u5B57\u6BB5\u6709\u6548)",e,!0),u.AddTBAtParas(),u.ParaFields=",CHWayOfTimeRole,CHWayOfTimeRoleField,",u.AddRM_UrlRightFrameOpen("\u8BBE\u7F6E\u5DE5\u4F5C\u65E5","/src/","\u9AD8\u7EA7\u8BBE\u7F6E"),this._enMap=u,this._enMap}beforeUpdateInsertAction(){return s(this,null,function*(){return this.CHWayOfTimeRole=1,!0})}}const q5=Object.freeze(Object.defineProperty({__proto__:null,EvaluationRole4:Cu},Symbol.toStringTag,{value:"Module"}));class iu extends N{constructor(){super("GPE_EvaluationRole");E(this,"Desc0",`
    
  #### \u5E2E\u52A9
   - \u9ED8\u8BA4\u4E3A\u4E0D\u8003\u6838\uFF0C\u5F53\u524D\u8282\u70B9\u4E0D\u8BBE\u7F6E\u4EFB\u4F55\u5F62\u5F0F\u7684\u8003\u6838\u3002
   #### \u5176\u5B83
   - ccbpm\u628A\u8003\u6838\u5206\u4E3A:\u65F6\u6548\u8003\u6838\u3001\u5DE5\u4F5C\u91CF\u8003\u6838\u3001\u8D28\u91CF\u8003\u6838\u4E09\u79CD\u7C7B\u578B.
   - \u65F6\u6548\u8003\u6838,\u5C31\u662F\u6309\u7167\u6307\u5B9A\u65F6\u95F4\u8303\u56F4\u5185\u8003\u6838,\u6BD4\u5982:\u4E00\u4EF6\u5DE5\u4F5C\u9700\u89813\u5929\u5B8C\u6210.
   - \u5DE5\u4F5C\u91CF\u8003\u6838:\u5C31\u7C7B\u4F3C\u4E8E\u8BA1\u4EF6\u5DE5\u8D44.
   - \u8D28\u91CF\u8003\u6838:\u5C31\u662F\u5DE5\u4F5C\u5904\u7406\u7684\u5185\u5BB9\u5B8C\u6210\u7684\u7ED3\u679C\u7531\u4E0B\u4E00\u6B65\u7684\u5DE5\u4F5C\u4EBA\u5458(\u9886\u5BFC)\u8FDB\u884C\u6253\u5206\u8003\u6838.
   ##### \u8D28\u91CF\u8003\u6838\u5B58\u50A8\u8868
   - \u8D28\u91CF\u8003\u6838\u7684\u6570\u636E\u5B58\u50A8\u5728 WF_CHEval \u8868\u91CC.  
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/EvaluationRole/Img/WF_CH.png "\u5C4F\u5E55\u622A\u56FE")
   #### \u65F6\u6548\u8003\u6838\u5B58\u50A8\u8868.
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/EvaluationRole/Img/WF_CH.png "\u5C4F\u5E55\u622A\u56FE")

    `);E(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u6309\u65F6\u95F4\u70B9\u8BA1\u7B97\uFF0C\u6216\u8005\u8BF4\u6309\u7167\u8BBE\u7F6E\u7684\u65F6\u95F4\u533A\u95F4\u8BA1\u7B97\u3002
   - \u8FD9\u4E2A\u65B9\u5F0F\u6709\uFF1A
   - 1. \u8BBE\u7F6E\u5929\u6570\uFF0C\u6BD4\u5982\u8BBE\u7F6E\u5E94\u8BE5\u5728\u51E0\u5929\u51E0\u5C0F\u65F6\u5B8C\u6210\u3002
   - 2. \u6309\u8868\u5355\u7684\u8868\u5355\u5B57\u6BB5\uFF0C\u9009\u62E9\u65F6\u95F4\u5B57\u6BB5\uFF0C\u6309\u5176\u8BBE\u7F6E\u7684\u65F6\u95F4\u8BA1\u7B97\u3002
   - 3. \u6D41\u8F6C\u81EA\u5B9A\u4E49\u3002 
  
 
      `);E(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u6309\u7167\u5904\u7406\u5DE5\u4F5C\u7684\u591A\u5C11\u8FDB\u884C\u8003\u6838\u3002 
   - \u8FD9\u6837\u7684\u8282\u70B9\uFF0C\u4E00\u822C\u90FD\u662F\u591A\u4EBA\u5904\u7406\u7684\u8282\u70B9\u3002
   `);E(this,"Desc3",`
  #### \u5E2E\u52A9
   - \u8D28\u91CF\u8003\u6838\uFF0C\u662F\u5F53\u524D\u8282\u70B9\u5BF9\u4E0A\u4E00\u6B65\u7684\u5DE5\u4F5C\u8FDB\u884C\u4E00\u4E2A\u5DE5\u4F5C\u597D\u574F\u7684\u4E00\u4E2A\u8003\u6838\u3002
   - \u8003\u6838\u7684\u65B9\u5F0F\u662F\u5BF9\u4E0A\u4E00\u4E2A\u8282\u70B9\u8FDB\u884C\u6253\u5206\uFF0C\u8BE5\u5206\u503C\u8BB0\u5F55\u5230WF_CHEval\u7684\u8868\u91CC\uFF0C\u5F00\u53D1\u4EBA\u5458\u5BF9WF_CHEval\u7684\u6570\u636E\u6839\u636E\u7528\u6237\u7684\u9700\u6C42\u8FDB\u884C\u4E8C\u6B21\u5904\u7406\u3002
   `);this.PageTitle="\u8003\u6838\u89C4\u5219"}Init(){this.entity=new f,this.KeyOfEn="CHWay",this.AddGroup("A","\u8003\u6838\u89C4\u5219"),this.Blank("0","\u4E0D\u8003\u6838",this.Desc0),this.AddEntity("1","\u6309\u7167\u56FA\u5B9A\u65F6\u6548\u8003\u6838",new nu,this.Desc1),this.AddEntity("4","\u6309\u7167\u6307\u5B9A\u5B57\u6BB5\u7684\u65F6\u6548\u8003\u6838",new Cu,this.Desc1),this.Blank("2","\u6309\u5DE5\u4F5C\u91CF\u8003\u6838",this.Desc2),this.Blank("3","\u6309\u5DE5\u4F5C\u8D28\u91CF\u8003\u6838",this.Desc3)}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const $5=Object.freeze(Object.defineProperty({__proto__:null,GPE_EvaluationRole:iu},Symbol.toStringTag,{value:"Module"}));class au extends l{constructor(u){super("TS.AttrNode.Sln5"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u591A\u8868\u5355");u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString(r.Name,null,"\u540D\u79F0",!0,!0,0,50,200),u.AddTBInt("FrmSummaryFieldRole",0,"\u6458\u8981\u5B57\u6BB5\u89C4\u5219",!1,!1),u.AddTBString("FrmSummaryFields",null,"\u6458\u8981\u5B57\u6BB5s",!1,!1,0,50,200),u.AddTBString("FrmSummaryNames",null,"\u5B57\u6BB5\u540D\u79F0",!1,!1,0,50,200);const e="@0=\u8868\u5355\u6811@1=\u4E3A1\u4E2A\u8868\u5355\u7684\u65F6\u5019,\u6309\u7ED1\u5B9A\u8868\u5355\u5E93\u7684\u8868\u5355\u8BA1\u7B97@2=Tab\u6807\u7B7E\u9875";u.AddDDLSysEnum("SheetTreeModel",0,"\u5C55\u793A\u65B9\u5F0F\u8BBE\u7F6E",!0,!0,"SheetTreeModel",e,"\u5DE5\u4F5C\u5904\u7406\u5668\u7684\u5C55\u73B0\u65B9\u5F0F.",!1),u.ParaFields=",FrmSummaryFieldRole,FrmSummaryFields,SheetTreeModel,",u.AddTBAtParas(),u.AddGroupMethod("\u7ED1\u5B9A\u591A\u8868\u5355");const t="FK_Frm,FrmSln,WhoIsPK,FrmNameShow,FrmEnableRole";u.AddRM_DtlSearch("\u7ED1\u5B9A\u8868\u5355",new Lu,Y.FK_Node,"","",t,"icon-drop",!0,""),u.AddRM_DtlBatch("\u6279\u91CF\u4FEE\u6539",new Ku,Y.FK_Node,"","","icon-drop","");const F=new k;return F.Title="\u8BBE\u7F6E\u6240\u6709\u8282\u70B9\u90FD\u91C7\u7528\u6B64\u65B9\u6848",F.Warning="\u60A8\u786E\u5B9A\u8981\u6267\u884C,\u8BBE\u7F6E\u8BE5\u6D41\u7A0B\u6240\u6709\u8282\u70B9\u90FD\u91C7\u7528\u6B64\u65B9\u6848\u5417?",F.ClassMethod="DoSetIt",F.RefMethodType=H.FuncToolbar,u.AddRefMethod(F),this._enMap=u,this._enMap}DoFrmAttr(){return this.NodeFrmID==""||this.NodeFrmID==null?"err@\u9519\u8BEF,\u8BF7\u5148\u8BBE\u7F6E\u8868\u5355ID.":new _(I.GoToUrl,g.UrlEn("TS.AttrNode.FrmNodeExt",this.NodeID+"_"+this.NodeFrmID))}DoSetIt(){return s(this,null,function*(){const u=new X("BP.WF.HttpHandler.WF_Admin_AttrNode_FrmSln");return u.AddPara("FK_Node",this.NodeID),yield u.DoMethodReturnString("RefOneFrmTree_SetAllNodeFrmUseThisSln")})}}const V5=Object.freeze(Object.defineProperty({__proto__:null,Sln5:au},Symbol.toStringTag,{value:"Module"}));class du extends N{constructor(){super("GPE_FrmSummaryField");E(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u6458\u8981\u5B57\u6BB5\u5B9A\u4E49: \u628A\u6311\u9009\u7684\u5B57\u6BB5\u5B58\u50A8\u5230\u6D41\u7A0B\u5F15\u64CE\u6CE8\u518C\u8868\u7684AtPara\u91CC.
   - \u8BF4\u660E:  \u5728\u6D41\u7A0B\u529F\u80FD\u9875\u9762(\u5F85\u529E\u3001\u5728\u9014\u3001\u8349\u7A3F)\u4E2D\u53EF\u4EE5\u67E5\u770B\u7684\u90FD\u662F\u6D41\u7A0B\u5B57\u6BB5,\u4E1A\u52A1\u8868\u5355\u4FE1\u606F\u65E0\u6CD5\u770B\u5230,\u8BE5\u529F\u80FD\u89E3\u51B3\u4E86\u6B64\u95EE\u9898.
  #### \u5E94\u7528\u573A\u666F
   -  \u5728\u5BA1\u6279\u8BF7\u5047\u4FE1\u606F\u4E2D,\u6CA1\u6709\u6253\u5F00\u4E4B\u524D\uFF0C\u5C31\u60F3\u770B\u5230\u8BF7\u5047\u5929\u6570\uFF0C\u8BF7\u5047\u539F\u56E0.
   -  \u5BA1\u6279\u5408\u540C\u7684\u65F6\u95F4\uFF0C\u5728\u5F85\u529E\u91CC\u53EF\u4EE5\u770B\u5230\uFF0C\u5408\u540C\u91D1\u989D\u3002
 `);E(this,"Desc1",`
 #### \u5E2E\u52A9
  - \u8BF7\u9009\u62E9\u6458\u8981\u5B57\u6BB5,\u663E\u793A\u987A\u5E8F\u662F\u6309\u7167\u8868\u5355\u7684\u5B57\u6BB5\u987A\u5E8F\u8FDB\u884C\u6392\u5E8F\u7684.
  #### \u8BBE\u7F6E\u6548\u679C\u56FE
  - \u6682\u65E0
  #### \u5C55\u793A\u6548\u679C\u56FE
   - \u6682\u65E0
`);this.PageTitle="\u6458\u8981\u5B57\u6BB5"}Init(){this.entity=new au,this.KeyOfEn="FrmSummaryFieldRole",this.AddGroup("A","+\u6458\u8981\u5B57\u6BB5"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0);const e="ND101",t=" SELECT KeyOfEn as No, Name, GroupID as GroupNo FROM Sys_MapAttr WHERE FK_MapData='"+e+"' and UIVisible=1 Order BY  GroupID, Idx",F="SELECT OID AS No, Lab as  Name FROM Sys_GroupField WHERE FrmID='"+e+"'";this.SelectItemsByGroupList("1","\u9009\u62E9\u6458\u8981\u5B57\u6BB5",this.Desc1,!0,F,t,"FrmSummaryFields","FrmSummaryNames")}AfterSave(e,t){if(e==t)throw new Error("Method not implemented.")}BtnClick(e,t,F){if(e==t||e===F)throw new Error("Method not implemented.")}}const Y5=Object.freeze(Object.defineProperty({__proto__:null,GPE_FrmSummaryField:du},Symbol.toStringTag,{value:"Module"}));class r extends B{}class lu extends l{constructor(u){super("TS.WF.Template.NodeExt","BP.WF.Template.NodeExt"),u&&this.setPKVal(u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u8282\u70B9\u5C5E\u6027");u.EnClassID=this.classID,u.GroupBarShowModel=0,u.AddGroupAttr("\u57FA\u672C\u914D\u7F6E"),u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.SetHelperUrl(B.NodeID,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3576080&doc_id=31094"),u.AddTBString(B.FK_Flow,null,"\u6D41\u7A0B\u7F16\u53F7",!1,!1,0,5,10),u.AddTBString(B.FlowName,null,"\u6D41\u7A0B\u540D",!1,!0,0,200,10),u.AddTBString(B.Name,null,"\u540D\u79F0",!0,!0,0,100,10,!1),u.SetHelperAlert(B.Name,"\u4FEE\u6539\u8282\u70B9\u540D\u79F0\u65F6\u5982\u679C\u8282\u70B9\u8868\u5355\u540D\u79F0\u4E3A\u7A7A\u7740\u8282\u70B9\u8868\u5355\u540D\u79F0\u548C\u8282\u70B9\u540D\u79F0\u76F8\u540C\uFF0C\u5426\u5219\u8282\u70B9\u540D\u79F0\u548C\u8282\u70B9\u8868\u5355\u540D\u79F0\u53EF\u4EE5\u4E0D\u76F8\u540C"),u.AddDDLSysEnum(B.WhoExeIt,0,"\u8C01\u6267\u884C\u5B83",!0,!0,B.WhoExeIt,"@0=\u64CD\u4F5C\u5458\u6267\u884C@1=\u673A\u5668\u6267\u884C@2=\u6DF7\u5408\u6267\u884C"),u.SetHelperUrl(B.WhoExeIt,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3576195&doc_id=31094"),u.AddDDLSysEnum(B.ReadReceipts,0,"\u5DF2\u8BFB\u56DE\u6267",!0,!0,B.ReadReceipts,"@0=\u4E0D\u56DE\u6267@1=\u81EA\u52A8\u56DE\u6267@2=\u7531\u4E0A\u4E00\u8282\u70B9\u8868\u5355\u5B57\u6BB5\u51B3\u5B9A@3=\u7531SDK\u5F00\u53D1\u8005\u53C2\u6570\u51B3\u5B9A"),u.SetHelperUrl(B.ReadReceipts,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3882411&doc_id=31094");const e="@0=\u4E0A\u4E00\u6B65\u53EF\u4EE5\u64A4\u9500@1=\u4E0D\u80FD\u64A4\u9500@2=\u4E0A\u4E00\u6B65\u4E0E\u5F00\u59CB\u8282\u70B9\u53EF\u4EE5\u64A4\u9500@3=\u6307\u5B9A\u7684\u8282\u70B9\u53EF\u4EE5\u64A4\u9500";u.AddDDLSysEnum(B.CancelRole,0,"\u64A4\u9500\u89C4\u5219",!0,!0,B.CancelRole,e),u.SetHelperUrl(B.CancelRole,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3576276&doc_id=31094"),u.AddTBString(B.CancelNodes,null,"\u53EF\u64A4\u9500\u7684\u8282\u70B9",!0,!1,0,200,50,!0),u.AddBoolean(B.CancelDisWhenRead,!1,"\u5BF9\u65B9\u5DF2\u7ECF\u6253\u5F00\u5C31\u4E0D\u80FD\u64A4\u9500",!0,!0),u.AddBoolean(B.IsOpenOver,!1,"\u5DF2\u9605\u5373\u5B8C\u6210?",!0,!0,!1),u.SetHelperUrl(B.IsOpenOver,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3653663&doc_id=31094");const t=`
    #### \u5B9A\u4E49
    - \u6240\u8C13\u7684\u53EF\u9006\u8282\u70B9,Reversible Node, \u5C31\u662F\u53CC\u5411\u7BAD\u5934\u8282\u70B9,\u53EF\u4EE5\u91CD\u590D\u6267\u884C\u7684\u8282\u70B9. 
    - \u5F53\u4E00\u4E2A\u8282\u70B9\uFF0C\u88AB\u8FD0\u52A8\u4E861\u6B21+\uFF0C\u5B83\u5C31\u662F\u53EF\u9006\u8282\u70B9, \u56E0\u4E3A\u5B83\u88AB\u91CD\u590D\u53D1\u9001\u4E86.
    - \u7B2C\u4E00\u6B21\u6309\u7167\u63A5\u6536\u4EBA\u89C4\u5219\u63A5\u6536\u4EBA\u6709a,b,c\u4E09\u4E2A\u4EBA. \u5982\u679C\u5728\u53D1\u9001\u56DE\u6765, \u9700\u8981\u91CD\u65B0\u8BA1\u7B97\u63A5\u6536\u4EBA\u5C31true,
    \u4E0D\u9700\u8981\u91CD\u65B0\u8BA1\u7B97\u63A5\u53D7\u4EBA,\u628A\u5F53\u4E8B\u4EBA\u505A\u4E3A\u63A5\u6536\u4EBA\u5C31\u662F false.
    #### \u5E94\u7528\u573A\u666F
    - \u6D41\u7A0B\u56FE
    - \u8282\u70B9A,\u8282\u70B9B\u662F\u53CC\u7EBF\u7BAD\u5934.
    `;u.AddBoolean(B.IsResetAccepter,!1,"\u53EF\u9006\u8282\u70B9\u65F6\u91CD\u65B0\u8BA1\u7B97\u63A5\u6536\u4EBA?",!0,!0,!0,t);const F="\u5982\u679C\u6709\u542F\u52A8\u7684\u8349\u7A3F\u5B50\u6D41\u7A0B\uFF0C\u662F\u5426\u53D1\u9001\u5B83\u4EEC\uFF1F";u.AddBoolean(B.IsSendDraftSubFlow,!1,"\u662F\u5426\u53D1\u9001\u8349\u7A3F\u5B50\u6D41\u7A0B?",!0,!0,!0,F),u.AddBoolean(B.IsGuestNode,!1,"\u662F\u5426\u662F\u5916\u90E8\u7528\u6237\u6267\u884C\u7684\u8282\u70B9(\u975E\u7EC4\u7EC7\u7ED3\u6784\u4EBA\u5458\u53C2\u4E0E\u5904\u7406\u5DE5\u4F5C\u7684\u8282\u70B9)?",!0,!0,!0,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3661834&doc_id=31094"),u.AddBoolean(B.IsYouLiTai,!1,"\u8BE5\u8282\u70B9\u662F\u5426\u662F\u6E38\u79BB\u6001",!0,!0),u.SetHelperUrl(B.IsYouLiTai,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3653664&doc_id=31094"),u.AddTBString(B.FocusField,null,"\u7126\u70B9\u5B57\u6BB5",!0,!1,0,50,10,!0),u.SetHelperUrl(B.FocusField,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3653665&doc_id=31094"),u.AddTBInt("FWCSta",0,"\u8282\u70B9\u72B6\u6001",!1,!1),u.AddTBInt("FWCAth",0,"\u5BA1\u6838\u9644\u4EF6\u662F\u5426\u542F\u7528",!1,!1),u.AddTBInt(B.DeliveryWay,0,"\u63A5\u53D7\u4EBA\u89C4\u5219",!1,!1),u.AddTBString(B.SelfParas,null,"\u81EA\u5B9A\u4E49\u5C5E\u6027",!0,!1,0,500,10,!0),u.SetHelperUrl(B.SelfParas,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3653666&doc_id=31094"),u.AddTBInt(B.Step,0,"\u6B65\u9AA4(\u65E0\u8BA1\u7B97\u610F\u4E49)",!0,!1),u.SetHelperUrl(B.Step,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3576085&doc_id=31094"),u.AddTBString(B.Tip,null,"\u64CD\u4F5C\u63D0\u793A",!0,!1,0,100,10,!1,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3653667&doc_id=31094");const D=`
    #### \u5E2E\u52A9
    - \u8BE5\u5C5E\u6027\u662F\u5BF9\u4E8E\u8BE5\u8282\u70B9\u4E0A\u6709\u591A\u4E2A\u4EBA\u5904\u7406\u6709\u6548\u3002
    - \u6BD4\u5982:A,\u53D1\u9001\u5230B,B\u8282\u70B9\u4E0A\u6709\u5F20\u4E09\uFF0C\u674E\u56DB\uFF0C\u738B\u4E94\u53EF\u4EE5\u5904\u7406\uFF0C\u60A8\u53EF\u4EE5\u6307\u5B9A1\u4E2A\u6216\u8005\u591A\u4E2A\u4EBA\u5904\u7406B\u8282\u70B9\u4E0A\u7684\u5DE5\u4F5C\u3002
    `;u.AddBoolean(B.IsTask,!1,"\u662F\u5426\u5141\u8BB8\u5206\u914D\u4EBA\u5458?",!0,!0,!0,D);const p=`
    #### \u5E2E\u52A9
    - \u8BE5\u5C5E\u6027\u662F\u5BF9\u4E8E\u8BE5\u8282\u70B9\u4E0A\u6709\u591A\u4E2A\u4EBA\u5904\u7406\u6709\u6548\u3002
    - \u6BD4\u5982:A,\u53D1\u9001\u5230B,B\u8282\u70B9\u4E0A\u6709\u5F20\u4E09\uFF0C\u674E\u56DB\uFF0C\u738B\u4E94\u53EF\u4EE5\u5904\u7406\uFF0C\u8FD9\u6B21\u4F60\u628A\u5DE5\u4F5C\u5206\u914D\u7ED9\u674E\u56DB\uFF0C
    - \u5982\u679C\u8BBE\u7F6E\u4E86\u8BB0\u5FC6\uFF0C\u90A3\u4E48ccbpm\u5C31\u5728\u4E0B\u6B21\u53D1\u9001\u7684\u65F6\u5019\uFF0C\u81EA\u52A8\u6295\u9012\u7ED9\u674E\u56DB\uFF0C\u5F53\u7136\u60A8\u4E5F\u53EF\u4EE5\u91CD\u65B0\u5206\u914D\u3002
    `;u.AddBoolean(B.IsRM,!0,"\u662F\u5426\u542F\u7528\u6295\u9012\u8DEF\u5F84\u81EA\u5B9A\u8BB0\u5FC6?",!0,!0,!0,p);const c=`
    #### \u5E2E\u52A9
    - \u8BE5\u5C5E\u6027\u662F\u5BF9\u4E8E\u8BE5\u8282\u70B9\u4E0A\u6709\u591A\u4E2A\u4EBA\u5904\u7406\u6709\u6548\u3002
    - \u6BD4\u5982:A\u53D1\u9001\u5230B,B\u8282\u70B9\u4E0A\u6709\u5F20\u4E09\uFF0C\u674E\u56DB\uFF0C\u738B\u4E94\u53EF\u4EE5\u5904\u7406\uFF0C\u5982\u679C\u662F\u674E\u56DB\u53D1\u9001\u7684\uFF0C\u8BE5\u8BBE\u7F6E\u662F\u5426\u9700\u8981\u628A\u674E\u56DB\u6392\u9664\u6389\u3002
    `;u.AddBoolean(B.IsExpSender,!0,"\u63A5\u6536\u4EBA\u8303\u56F4\u662F\u5426\u6392\u9664\u53D1\u9001\u4EBA?",!0,!0,!0,c),u.AddGroupAttr("\u8FD0\u884C\u6A21\u5F0F"),u.AddDDLSysEnum(B.RunModel,0,"\u8282\u70B9\u7C7B\u578B",!0,!1,B.RunModel,"@0=\u7EBF\u5F62@1=\u5408\u6D41@2=\u5206\u6D41@3=\u5206\u5408\u6D41@4=\u540C\u8868\u5355@5=\u5F02\u8868\u5355"),u.SetHelperUrl(B.RunModel,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3661853&doc_id=31094"),u.AddTBInt(B.PassRate,100,"\u5B8C\u6210\u901A\u8FC7\u7387",!0,!1),u.SetHelperUrl(B.PassRate,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3661856&doc_id=31094"),u.AddBoolean(O.ThreadIsCanDel,!0,"\u662F\u5426\u53EF\u4EE5\u5220\u9664\u5B50\u7EBF\u7A0B(\u5F53\u524D\u8282\u70B9\u5DF2\u7ECF\u53D1\u9001\u51FA\u53BB\u7684\u7EBF\u7A0B\uFF0C\u5E76\u4E14\u5F53\u524D\u8282\u70B9\u662F\u5206\u6D41\uFF0C\u6216\u8005\u5206\u5408\u6D41\u6709\u6548\uFF0C\u5728\u5B50\u7EBF\u7A0B\u9000\u56DE\u540E\u7684\u64CD\u4F5C)\uFF1F",!0,!0,!0),u.AddBoolean(O.ThreadIsCanAdd,!0,"\u662F\u5426\u53EF\u4EE5\u589E\u52A0\u5B50\u7EBF\u7A0B(\u5F53\u524D\u8282\u70B9\u5DF2\u7ECF\u53D1\u9001\u51FA\u53BB\u7684\u7EBF\u7A0B\uFF0C\u5E76\u4E14\u5F53\u524D\u8282\u70B9\u662F\u5206\u6D41\uFF0C\u6216\u8005\u5206\u5408\u6D41\u6709\u6548)\uFF1F",!0,!0,!0),u.AddBoolean(O.ThreadIsCanShift,!1,"\u662F\u5426\u53EF\u4EE5\u79FB\u4EA4\u5B50\u7EBF\u7A0B(\u5F53\u524D\u8282\u70B9\u5DF2\u7ECF\u53D1\u9001\u51FA\u53BB\u7684\u7EBF\u7A0B\uFF0C\u5E76\u4E14\u5F53\u524D\u8282\u70B9\u662F\u5206\u6D41\uFF0C\u6216\u8005\u5206\u5408\u6D41\u6709\u6548\uFF0C\u5728\u5B50\u7EBF\u7A0B\u9000\u56DE\u540E\u7684\u64CD\u4F5C)\uFF1F",!0,!0,!0),u.AddDDLSysEnum(B.USSWorkIDRole,0,"\u5F02\u8868\u5355\u5B50\u7EBF\u7A0BWorkID\u751F\u6210\u89C4\u5219",!0,!0,B.USSWorkIDRole,"@0=\u4EC5\u751F\u6210\u4E00\u4E2AWorkID@1=\u6309\u63A5\u53D7\u4EBA\u751F\u6210WorkID"),u.SetHelperAlert(B.USSWorkIDRole,"\u5BF9\u4E0A\u4E00\u4E2A\u8282\u70B9\u662F\u5408\u6D41\u8282\u70B9\uFF0C\u5F53\u524D\u8282\u70B9\u662F\u5F02\u8868\u5355\u5B50\u7EBF\u7A0B\u6709\u6548."),u.AddBoolean(B.IsSendBackNode,!1,"\u662F\u5426\u662F\u53D1\u9001\u8FD4\u56DE\u8282\u70B9(\u53D1\u9001\u5F53\u524D\u8282\u70B9,\u81EA\u52A8\u53D1\u9001\u7ED9\u8BE5\u8282\u70B9\u7684\u53D1\u9001\u4EBA,\u53D1\u9001\u8282\u70B9.)?",!0,!0,!0),u.SetHelperUrl(B.IsSendBackNode,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=6396936&doc_id=31094"),u.AddGroupAttr("\u8DF3\u8F6C\u89C4\u5219"),u.AddBoolean(B.AutoJumpRole0,!1,"\u5904\u7406\u4EBA\u5C31\u662F\u53D1\u8D77\u4EBA",!0,!0,!0),u.SetHelperUrl(B.AutoJumpRole0,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3980077&doc_id=31094"),u.AddBoolean(B.AutoJumpRole1,!1,"\u5904\u7406\u4EBA\u5DF2\u7ECF\u51FA\u73B0\u8FC7",!0,!0,!0),u.AddBoolean(B.AutoJumpRole3,!1,"\u672A\u6765\u8282\u70B9\u5904\u7406\u4EBA\u5DF2\u7ECF\u51FA\u73B0\u8FC7(\u53EA\u9488\u5BF9\u8BA1\u7B97\u672A\u6765\u5904\u7406\u4EBA\u7684\u8282\u70B9\u4F7F\u7528)",!0,!0,!0),u.SetHelperUrl(B.AutoJumpRole3,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=8393139&doc_id=31094"),u.AddBoolean(B.AutoJumpRole2,!1,"\u5904\u7406\u4EBA\u4E0E\u4E0A\u4E00\u6B65\u76F8\u540C",!0,!0,!0),u.AddBoolean(B.WhenNoWorker,!1,"(\u662F)\u627E\u4E0D\u5230\u4EBA\u5C31\u8DF3\u8F6C,(\u5426)\u63D0\u793A\u9519\u8BEF.",!0,!0,!0),u.AddTBString(B.AutoJumpExp,null,"\u8868\u8FBE\u5F0F",!0,!1,0,200,10,!0),u.SetHelperAlert(B.AutoJumpExp,"\u53EF\u4EE5\u8F93\u5165Url\u6216SQL\u8BED\u53E5,\u8BF7\u53C2\u8003\u5E2E\u52A9\u6587\u6863."),u.AddDDLSysEnum(B.SkipTime,0,"\u6267\u884C\u8DF3\u8F6C\u4E8B\u4EF6",!0,!0,B.SkipTime,"@0=\u4E0A\u4E00\u4E2A\u8282\u70B9\u53D1\u9001\u65F6@1=\u5F53\u524D\u8282\u70B9\u5DE5\u4F5C\u6253\u5F00\u65F6"),u.SetHelperUrl(B.SkipTime,"https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3980077&doc_id=31094"),u.AddGroupAttr("\u6309\u94AE\u6743\u9650");const S=new hu;u.AddAttrs(S._enMap.attrs),u.SetPopList(B.ReturnNodes,n.srcNodes,!0,"300px","500px","\u9009\u62E9\u9000\u56DE\u5230\u7684\u8282\u70B9","icon-people"),u.SetPopList(B.CancelNodes,n.srcNodes,!0,"300px","500px","\u9009\u62E9\u64A4\u9500\u7684\u8282\u70B9","icon-people"),u.SetPopList(B.JumpToNodes,n.srcNodes,!0,"300px","500px","\u53EF\u8DF3\u8F6C\u5230\u7684\u8282\u70B9","icon-people"),u.ParaFields=",IsTask,IsRM,IsExpSender,IsYouLiTai,CancelNodes,ReturnNodes,",u.AddTBAtParas(4e3),u.AddGroupMethod("\u57FA\u672C\u914D\u7F6E","icon-drop"),u.AddRM_GPE(new ou,"icon-user-following"),u.AddRM_GPE(new Tu,"icon-people"),u.AddRM_GPE(new Hu,"icon-book-open"),u.AddRM_DtlSearch("\u8282\u70B9\u4E8B\u4EF6",new Uu,"RefPKVal",null,"","","icon-energy",!0),u.AddRM_DtlSearch("\u8282\u70B9\u6D88\u606F",new Gu,Z.RefPKVal,"","",Z.showAttrsMsg,"icon-speech",!1,"&MsgModel=NodeMsg"),u.AddRM_GPE(new wu,"icon-directions"),u.AddRM_GPE(new xu,"icon-close");const m=new k,T=new Pu;m.Title="\u81EA\u5B9A\u4E49\u5DE5\u5177\u680F",m.Icon="icon-settings",m.RefMethodType=H.Dtl,m.ClassMethod="/src/WF/Comm/Dtl/DtlSearch.vue?key=custom_toolbar",m.params={EnName:T.GetNewEntity.classID,RefPK:Ru.FK_Node,RefMainEnName:this.EnClassID,ButsTableTop:"",ButsItem:"",ShowAttrs:"",isMove:"0",Title:"\u8282\u70B9\u4E8B\u4EF6",Icon:"icon-settings"},u.AddRefMethod(m),u.AddGroupMethod("\u8868\u5355\u65B9\u6848","icon-grid"),u.AddRM_GPE(new Fu,"icon-grid"),u.AddRM_GPE(new Du,"iconfont icon-ptkj-lianxuqianpimoshi"),u.AddRM_EnOnly("\u5BA1\u6838\u7EC4\u4EF6","TS.WF.Template.NodeWorkCheck","@NodeID","icon-note"),u.AddRM_EnOnly("\u516C\u6587\u7EC4\u4EF6","TS.WF.Template.NodeGovDoc","@NodeID","icon-note"),u.AddRM_GPE(new vu,"icon-list"),u.AddRM_GPE(new Ou,"icon-puzzle"),u.AddRM_GPE(new du,"iconfont icon-fuwenbenkuang"),u.AddGroupMethod("\u5B50\u6D41\u7A0B","icon-paper-plane"),u.AddRM_EnOnly("\u5B50\u6D41\u7A0B\u7EC4\u4EF6","TS.WF.Template.FrmSubFlow","@NodeID","icon-organization");const pu="SubFlowNo,SubFlowName,SubFlowSta,SubFlowType,SubFlowModel";return u.AddRM_DtlSearch("\u5B50\u6D41\u7A0B",new Mu,bu.FK_Node,"","",pu,"icon-organization",!0,""),u.AddGroupMethod("\u8003\u6838\u89C4\u5219"),u.AddRM_GPE(new iu,"icon-badge"),u.AddRM_GPE(new Wu,"icon-bell"),this._enMap=u,this._enMap}DoSetCheckModel(){return""}DoNodeToolbars(){return"/WF/Comm/Dtl.vue?EnsName=TS.WF.Template.NodeToolbars&RefPK=FK_Node&RefPKVal="+this.NodeID}}class ku extends Su{get GetNewEntity(){return new lu}constructor(){super()}}const Z5=Object.freeze(Object.defineProperty({__proto__:null,NodeExt:lu,NodeExtAttr:r,NodeExts:ku},Symbol.toStringTag,{value:"Module"}));class x extends l{constructor(u){super("TS.WF.AR501"),u&&(this.NodeID=u)}get HisUAC(){const u=new a;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new d("WF_Node","\u76F4\u63A5\u4E0A\u7EA7");return u.AddTBIntPK(r.NodeID,"\u8282\u70B9ID"),u.AddTBString("AR501DeptNo",null,"\u90E8\u95E8\u7F16\u7801",!0,!1,0,100,100,!0),u.AddTBInt("AR501StationType",0,"\u5C97\u4F4D\u7C7B\u578B",!0,!1,!0),u.AddTBAtParas(4e3),u.ParaFields=",AR501DeptNo,AR501StationType,",this._enMap=u,this._enMap}afterUpdate(){return s(this,null,function*(){return Promise.resolve(!0)})}}const X5=Object.freeze(Object.defineProperty({__proto__:null,AR501:x},Symbol.toStringTag,{value:"Module"}));export{T5 as A,J5 as E,ou as G,r as N,I5 as S,lu as a,g5 as b,w5 as c,P5 as d,R5 as e,M5 as f,b5 as g,W5 as h,L5 as i,K5 as j,O5 as k,U5 as l,G5 as m,v5 as n,x5 as o,H5 as p,k5 as q,j5 as r,z5 as s,Q5 as t,q5 as u,$5 as v,V5 as w,Y5 as x,Z5 as y,X5 as z};
