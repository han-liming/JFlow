var p=Object.defineProperty;var C=(F,E,u)=>E in F?p(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var o=(F,E,u)=>(C(F,typeof E!="symbol"?E+"":E,u),u);var n=(F,E,u)=>new Promise((B,t)=>{var A=e=>{try{i(u.next(e))}catch(D){t(D)}},m=e=>{try{i(u.throw(e))}catch(D){t(D)}},i=e=>e.done?B(e.value):Promise.resolve(e.value).then(A,m);i((u=u.apply(F,E)).next())});import{G as c}from"./DataType-33901a1c.js";import{PageBaseGroupEdit as l}from"./PageBaseGroupEdit-202e8e85.js";import{Node as s,NodeAttr as r}from"./Node-6b42ba5e.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class T extends l{constructor(){super("GPE_ShenFenModel");o(this,"Desc3",`
  #### \u8BF4\u660E
   - \u83B7\u53D6\u5F53\u524D\u4EBA\u5458\u4FE1\u606F\u7684\u662F\u6309\u7167\u6307\u5B9A\u7684\u90E8\u95E8\u4E0E\u6307\u5B9A\u7684\u89D2\u8272\u7684\u4EA4\u96C6\u8BA1\u7B97.
   - \u6307\u5B9A\u7684\u90E8\u95E8\u662F\u4ECE\u6D41\u7A0B\u8868\u5355\u7684\u5B57\u6BB5\u83B7\u53D6\u7684.
   - \u6D41\u7A0B\u7684\u7CFB\u7EDF\u53C2\u6570\u5B58\u50A8\u5728 \u8868:WF_GenerWorkFlow \u5B57\u6BB5: AtPara \u4E2D.
   #### \u5176\u4ED6
   - \u8868\u5355ID:  NDxxxRpt, \u5B57\u6BB5\u5B58\u50A8\u5728 Sys_MapAttr \u8868.
    `);o(this,"Desc4",`
  #### \u8BF4\u660E
   - \u83B7\u53D6\u5F53\u524D\u4EBA\u5458\u4FE1\u606F\u7684\u662F\u6309\u7167\u6307\u5B9A\u7684\u90E8\u95E8\u4E0E\u6307\u5B9A\u7684\u89D2\u8272\u7684\u4EA4\u96C6\u8BA1\u7B97.
   - \u6307\u5B9A\u7684\u90E8\u95E8\u662F\u4ECEccbpm\u7684\u7CFB\u7EDF\u53C2\u6570\u83B7\u53D6\u7684.
   - \u5728\u6D41\u7A0B\u8FD0\u884C\u7684\u8FC7\u7A0B\u4E2D\u7CFB\u7EDF\u53C2\u6570\uFF0C\u662F\u901A\u8FC7 Flow_SavePara() \u7684\u65B9\u6CD5\u4FDD\u5B58\u5230ccbpm\u4E2D\u7684.
   - \u6D41\u7A0B\u7684\u7CFB\u7EDF\u53C2\u6570\u5B58\u50A8\u5728 \u8868:WF_GenerWorkFlow \u5B57\u6BB5:AtPara \u4E2D.
   #### \u5176\u4ED6
   - \u8BF7\u9605\u8BFBccbpm\u7684\u63A5\u53E3\u65B9\u6CD5\uFF0C\u4FDD\u5B58\u53C2\u6570.

    `);o(this,"Desc0",`
  #### \u8BF4\u660E
   - \u4E0A\u4E00\u6B65\u7684\u53D1\u9001\u4EBA\u4F5C\u4E3A\u63A5\u6536\u4EBA\u3002
   - \u9ED8\u8BA4\u4E3A\u8BE5\u6A21\u5F0F\u3002
    `);o(this,"Desc1",`
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
   - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow1.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi1.png "\u5C4F\u5E55\u622A\u56FE")
      `);o(this,"Desc2",`
  #### \u8BF4\u660E
  - \u9009\u62E9\u7684\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD(\u8BE5\u5B57\u6BB5\u91CC\u5B58\u50A8\u7684\u662F\u8D26\u53F7)
  - \u6307\u5B9A\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD.
  - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u8868\u5355\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingBiaodan2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi2.png "\u5C4F\u5E55\u622A\u56FE")
      `);this.PageTitle="\u4EBA\u5458\u8EAB\u4EFD\u89C4\u5219"}Init(){return n(this,null,function*(){this.entity=new s,this.KeyOfEn=r.ShenFenModel,this.AddGroup("A","\u6309\u7EC4\u7EC7\u7ED3\u6784\u7ED1\u5B9A"),this.Blank("0"," \u5F53\u524D\u4EBA\u5458\u7684\u8EAB\u4EFD\uFF08\u9ED8\u8BA4\uFF09",this.Desc0);const u=new s(this.PKVal);yield u.Retrieve();const B=u.FK_Flow;this.SelectItemsByList("1","\u6307\u5B9A\u8282\u70B9\u7684\u4EBA\u5458\u8EAB\u4EFD",this.Desc1,!1,`SELECT NodeID No,Name FROM WF_Node WHERE FK_Flow='${B}' `,r.ShenFenVal);const t="ND"+parseInt(u.FK_Flow)+"Rpt";let A=c.SQLOfMapAttrsGener(t);A=A.replace("MyPK","KeyOfEn"),this.SelectItemsByList("2","\u6309\u8868\u5355\u5B57\u6BB5\u503C(\u4EBA\u5458\u7F16\u53F7)\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD",this.Desc2,!1,A,r.ShenFenVal),this.SelectItemsByList("3","\u6309\u8868\u5355\u5B57\u6BB5\u503C(\u90E8\u95E8\u7F16\u53F7)\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD",this.Desc3,!1,A,r.ShenFenVal),this.SingleTB("4","\u6309\u7CFB\u7EDF\u53C2\u6570(\u90E8\u95E8\u7F16\u53F7)\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD",r.ShenFenVal,this.Desc4,"\u8BF7\u8F93\u5165\u7CFB\u7EDF\u53C2\u6570")})}AfterSave(u,B){if(u==B)throw new Error("Method not implemented.")}BtnClick(u,B,t){if(u==B||u===t)throw new Error("Method not implemented.")}}export{T as GPE_ShenFenModel};
