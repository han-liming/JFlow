var o=Object.defineProperty;var C=(A,u,E)=>u in A?o(A,u,{enumerable:!0,configurable:!0,writable:!0,value:E}):A[u]=E;var F=(A,u,E)=>(C(A,typeof u!="symbol"?u+"":u,E),E);import{PageBaseGroupEdit as i}from"./PageBaseGroupEdit-202e8e85.js";import{MapDtl as e,MapDtlAttr as r}from"./MapDtl-dc3f1bee.js";import{ListShoModel2D as p}from"./ListShoModel2D-a43270d9.js";import{ListShoModel3D as B}from"./ListShoModel3D-f85679e1.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./BSEntity-840a884b.js";class v extends i{constructor(){super("GPE_ListShowModel");F(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u4ECE\u8868\u5C55\u793A\u65B9\u5F0F\u4E3A\u8868\u683C\u7684\u5F62\u5F0F\u3002
  #### \u8868\u683C\u6A21\u5F0F\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/ListShowModelBiaoge.png "\u4ECE\u8868\u6A21\u5F0F")  
  `);F(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u4ECE\u8868\u5C55\u793A\u65B9\u5F0F\u4E3A\u5361\u7247\u7684\u5F62\u5F0F\u3002
  - \u573A\u666F\uFF1A\u6570\u636E\u91CF\u6BD4\u8F83\u591A, \u6709\u4ECE\u8868.
  #### \u5361\u7247\u6A21\u5F0F\u6548\u679C\u56FE
  - \u5361\u7247
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/ListShowModelkapian.png "\u5361\u7247\u6A21\u5F0F")  

  #### \u5C40\u90E8\u653E\u5927
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/ListShowModelkapian1.png "\u5361\u7247\u6A21\u5F0F")  

  `);F(this,"Desc2",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u4ECE\u8868\u5C55\u793A\u65B9\u5F0F\u4E3A\u81EA\u5B9A\u4E49URL\u3002
  - \u8BF4\u660E\uFF1A\u8BE5\u6A21\u5F0F\u4E0B\uFF0C\u662F\u5F53\u73B0\u5728\u7684\u4ECE\u8868\u4E0D\u80FD\u6EE1\u8DB3\u5BA2\u6237\u5BF9\u6570\u636E\u5C55\u73B0\u91C7\u96C6\u7684\u8981\u6C42\uFF0C\u9700\u8981\u5199\u4E00\u4E2A\u81EA\u5B9A\u4E49\u7684url\u5B9E\u73B0\uFF0C\u4F46\u662F\u6570\u636E\u8FD8\u662F\u8981\u5B58\u50A8\u5728\u5F53\u524D\u4ECE\u8868\u91CC\u9762\u6765\u3002
  - \u6BD4\u5982\uFF1A\u5BF9\u8F93\u5165\u7684\u590D\u6742\u7684\u8BA1\u7B97,\u76EE\u524D\u7684\u4ECE\u8868\u4E0D\u80FD\u63A7\u5236\u5230\u4F4D.
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/SelfUrl.png "\u81EA\u5B9A\u4E49rul")  
  `);F(this,"DescD2",`
  #### \u5E2E\u52A9
  - \u5982\u4E0B\u56FE\u6240\u793A.
  - \u4ECE\u8868\u91CC\u6709\u4E09\u4E2A\u5B57\u6BB5\uFF0C\u4E24\u4E2A\u5916\u952E\u6216\u8005\u679A\u4E3E\u7C7B\u578B\u7684\u5B57\u6BB5\uFF0C\u4E00\u4E2A\u6570\u503C\u7C7B\u578B\u7684\u5B57\u6BB5. 
  - \u9700\u8981\u4EA4\u53C9\u6570\u636E\u5C55\u73B0\u6A21\u5F0F\uFF0C\u7528\u4E8E\u6570\u636E\u91C7\u96C6\u6216\u8005\u5C55\u73B0\u3002
  - \u4ECE\u5DE6\u4FA7\u5F00\u59CB,\u7B2C1\u4E2A\u5B57\u6BB5\u662F\u7B2C1\u7EF4\u5EA6,\u5934\u90E8\u662F\u7B2C2\u7EF4\u5EA6. 
  #### \u8868\u683C\u6A21\u5F0F\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/D2.png "\u81EA\u5B9A\u4E49rul")  
  - \u7EF4\u5EA61\uFF1A\u5E74\u7EA7.
  - \u7EF4\u5EA62\uFF1A\u653F\u6CBB\u9762\u8C8C.
  `);F(this,"DescD3Top",`
  #### \u5E2E\u52A9
  - \u5982\u4E0B\u56FE\u6240\u793A.
  - \u4ECE\u8868\u91CC\u6709\u56DB\u4E2A\u5B57\u6BB5\uFF0C\u4E09\u4E2A\u5916\u952E\u6216\u8005\u679A\u4E3E\u7C7B\u578B\uFF0C\u4E00\u4E2A\u6570\u503C\u7C7B\u578B\u7684\u5B57\u6BB5. 
  - \u9700\u8981\u4EA4\u53C9\u6570\u636E\u5C55\u73B0\u6A21\u5F0F\uFF0C\u7528\u4E8E\u6570\u636E\u91C7\u96C6\u6216\u8005\u5C55\u73B0\u3002
  #### \u8868\u683C\u6A21\u5F0F\u6548\u679C\u56FE
  - \u5C55\u73B0\u6548\u679C
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/D3Top.png "3\u7EF4\u5EA6\u5DE6\u4FA7")  
  - \u5B58\u50A8\u6548\u679C
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/YSTable.png "3\u7EF4\u5EA6\u5DE6\u4FA7")  
  `);F(this,"DescD3Left",`
  #### \u5E2E\u52A9
  - \u5982\u4E0B\u56FE\u6240\u793A.
  - \u4ECE\u8868\u91CC\u6709\u56DB\u4E2A\u5B57\u6BB5\uFF0C\u4E09\u4E2A\u5916\u952E\u6216\u8005\u679A\u4E3E\u7C7B\u578B\uFF0C\u4E00\u4E2A\u6570\u503C\u7C7B\u578B\u7684\u5B57\u6BB5. 
  - \u9700\u8981\u4EA4\u53C9\u6570\u636E\u5C55\u73B0\u6A21\u5F0F\uFF0C\u7528\u4E8E\u6570\u636E\u91C7\u96C6\u6216\u8005\u5C55\u73B0\u3002
  #### \u8868\u683C\u6A21\u5F0F\u6548\u679C\u56FE
  - \u5C55\u73B0\u6548\u679C
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/D3Left.png "3\u7EF4\u5EA6\u5DE6\u4FA7")  
  - \u5B58\u50A8\u6548\u679C
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/ListShowModel/Img/YSTable.png "3\u7EF4\u5EA6\u5DE6\u4FA7")  
  `);this.PageTitle="\u5C55\u793A\u6A21\u5F0F"}Init(){this.entity=new e,this.KeyOfEn="ListShowModel",this.AddGroup("A","\u5E38\u89C4\u6A21\u5F0F"),this.Blank("0","\u8868\u683C(\u9ED8\u8BA4)",this.Desc0),this.Blank("1","\u5361\u7247\u6A21\u5F0F",this.Desc1),this.SingleTB("2","\u81EA\u5B9A\u4E49URL",r.UrlDtl,this.Desc2,"\u8BF7\u8F93\u5165\u81EA\u5B9A\u4E49\u7684url"),this.AddGroup("B","\u62A5\u8868\u6A21\u5F0F"),this.AddEntity("3","2\u7EF4\u8868",new p,this.DescD2),this.AddEntity("4","3\u7EF4\u8868(\u5DE6)",new B,this.DescD3Left),this.AddEntity("5","3\u7EF4\u8868(\u4E0A)",new B,this.DescD3Top)}BtnClick(E,t,s){}AfterSave(E,t){}}export{v as GPE_ListShowModel};
