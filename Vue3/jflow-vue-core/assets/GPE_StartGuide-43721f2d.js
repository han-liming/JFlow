var t=Object.defineProperty;var i=(B,E,F)=>E in B?t(B,E,{enumerable:!0,configurable:!0,writable:!0,value:F}):B[E]=F;var u=(B,E,F)=>(i(B,typeof E!="symbol"?E+"":E,F),F);import{StarGuideBySQLMulti as D}from"./StarGuideBySQLMulti-f4a2122e.js";import{StartGuideBySQLOne as e}from"./StartGuideBySQLOne-672d2bf4.js";import{PageBaseGroupEdit as r}from"./PageBaseGroupEdit-202e8e85.js";import{Flow as o,FlowAttr as s}from"./Flow-6121039a.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class N extends r{constructor(){super("GPE_StartGuide");u(this,"Desc10",`
  #### \u5E2E\u52A9
   
   - \u53D1\u8D77\u4E4B\u524D\u9700\u8981\u63D0\u793A\u7684Html\u6587\u672C\u4FE1\u606F.
   - \u6BD4\u5982:\u8BB8\u53EF\u534F\u8BAE\u3001\u6CE8\u610F\u4E8B\u9879\u3001\u529E\u7406\u6D41\u7A0B\u3001\u529E\u7406\u7A0B\u5E8F\u8BF4\u660E\u7B49\u7B49.
   - \u7528\u6237\u70B9\u51FB\u540E,\u5C31\u8FDB\u5165\u53D1\u8D77\u9875\u9762.
   
    `);u(this,"Desc0",`
  #### \u5E2E\u52A9
   
   - \u53D1\u8D77\u524D\u7F6E\u5BFC\u822A\uFF0C\u5C31\u662F\u6D41\u7A0B\u5728\u542F\u52A8\u4E4B\u524D\u9700\u8981\u6709\u4E00\u4E2A\u524D\u7F6E\u9875\u9762\u3002
   - \u5728\u8FD9\u4E2A\u9875\u9762\u91CC\uFF0C\u53EF\u4EE5\u6839\u636E\u4E0D\u540C\u7684\u5E94\u7528\u573A\u666F\u8BBE\u7F6E\u4E0D\u540C\u7684\u754C\u9762\u3002
   - \u4E0D\u4F7F\u7528 \uFF0C\u4E0D\u542F\u52A8\u524D\u7F6E\u9875\u9762\u3002
   
    `);u(this,"Desc1",`
  #### \u5E2E\u52A9
   
   - \u53D1\u8D77\u524D\u7F6E\u5BFC\u822A\uFF0C\u5C31\u662F\u6D41\u7A0B\u5728\u542F\u52A8\u4E4B\u524D\u9700\u8981\u6709\u4E00\u4E2A\u524D\u7F6E\u9875\u9762\u3002
   - \u5728\u8FD9\u4E2A\u9875\u9762\u91CC\uFF0C\u53EF\u4EE5\u6839\u636E\u4E0D\u540C\u7684\u5E94\u7528\u573A\u666F\u8BBE\u7F6E\u4E0D\u540C\u7684\u754C\u9762\u3002

   #### \u5E94\u7528\u573A\u666F
   - \u542F\u52A8\u4E00\u4E2A\u8BA2\u5355\u8981\u9009\u62E9\u4E00\u4E2A\u5BA2\u6237.
   - \u5BF9\u5BA2\u6237\u7684\u9009\u62E9\u662F\u4E00\u4E2A\u5355\u9009,\u53EA\u80FD\u9009\u62E9\u4E00\u4E2A\u8BB0\u5F55.
   - \u8FD9\u91CC\u8981\u914D\u7F6E\u67E5\u8BE2\u6761\u4EF6,\u5217\u8868\u6570\u636E\u6E90,\u9009\u62E9\u4E00\u7B14\u6570\u636E\u6839\u636E\u4E3B\u952E\u67E5\u8BE2\u8BB0\u5F55.
   #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrFlow/StartGuide/Img/StartGuidSetting.png "\u5C4F\u5E55\u622A\u56FE.png")
   #### \u6548\u679C\u56FE
   - \u8FD0\u884C\u56FE 
   \u70B9\u51FB\u4E00\u6761\u8BB0\u5F55\u53D1\u8D77\u5BFC\u822A
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrFlow/StartGuide/Img/StartGuide.png "\u5C4F\u5E55\u622A\u56FE.png")
   #### \u6548\u679C\u56FE
   - \u8FD0\u884C\u6D41\u7A0B\uFF0C\u628A\u5BFC\u822A\u6570\u636E\u5BFC\u5165\u5230\u8868\u5355\u4E2D\uFF0C\u70B9\u51FB\u53D1\u9001\u53EF\u4EE5\u53D1\u8D77\u6D41\u7A0B
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrFlow/StartGuide/Img/StartGuide2.png "\u5C4F\u5E55\u622A\u56FE.png")
    `);u(this,"Desc2",`
  #### \u5E2E\u52A9

   - \u5E94\u7528\u524D\u7F6E\u5BFC\u822A\u53EF\u4EE5\u5BF9\u6570\u636E\u5217\u8868\u91CC\u6279\u91CF\u53D1\u8D77\u5B50\u6D41\u7A0B\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u4E09\u597D\u5B66\u751F\u8BC4\u5B9A\u3001\u5206\u5305\u5546\u5BA1\u6838\u7B49\u3002\u4E0B\u9762\u4EE5\u4E09\u597D\u5B66\u751F\u8BC4\u5B9A\u4EE5\u4F8B\u8FDB\u884C\u8BF4\u660E\u3002
    1.\u6253\u5F00\u5B66\u751F\u7684\u5B9E\u4F53\u7C7B\u5217\u8868\u3002
    2.\u5728\u5B9E\u4F53\u7C7B\u5217\u8868\u91CC\uFF0C\u9009\u62E9\u591A\u540D\u5B66\u751F\uFF08\u9009\u62E9\u591A\u884C\uFF09\u3002
    3.\u53D1\u8D77\u4E09\u597D\u5B66\u751F\u8BC4\u5B9A\u6D41\u7A0B\u3002
  
    `);u(this,"Desc3",`
  #### \u5E2E\u52A9
   
   - \u7528\u6237\u5E0C\u671B\u51FA\u73B0\u4E00\u4E2A\u5386\u53F2\u53D1\u8D77\u7684\u6D41\u7A0B\u5217\u8868\uFF0C\u9009\u62E9\u4E00\u6761\u6D41\u7A0B\u5E76\u628A\u8BE5\u6D41\u7A0B\u7684\u6570\u636Ecopy\u5230\u65B0\u5EFA\u7684\u6D41\u7A0B\u4E0A\u3002
   - \u60A8\u9700\u8981\u5728\u8FD9\u91CC\u914D\u7F6E\u4E00\u4E2ASQL, \u5E76\u4E14\u8BE5SQL\u5FC5\u987B\u6709\u4E00\u4E2AOID\u5217\u3002
   - \u6BD4\u5982\uFF1ASELECT Title ,OID FROM WF_GenerWorkFlow WHERE Title LIKE '%@Key%' AND FK_Flow='001' AND WFState=3
  

    `);u(this,"Desc4",`
  #### \u5E2E\u52A9
   - \u53D1\u8D77\u8BE5\u6D41\u7A0B\u4E4B\u524D\uFF0C\u9700\u8981\u9009\u62E9\u4E00\u4E2A\u5DF2\u7ECF\u53D1\u8D77\u7684\u6D41\u7A0B\u5B9E\u4F8B\u4F5C\u4E3A\u7236\u6D41\u7A0B\u3002
   - \u542F\u52A8\u8BE5\u6D41\u7A0B\u7684\u65F6\u5019\uFF0C\u9700\u8981\u542F\u52A8 /WF/WorkOpt/StartGuideParentFlowModel.htm \u7684\u9875\u9762\uFF0C\u8BA9\u7528\u6237\u9009\u62E9\u4E00\u4E2A\u7236\u6D41\u7A0B\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u5728\u6267\u884C\u8D22\u52A1\u62A5\u9500\u7684\u65F6\u5019\uFF0C\u8981\u9009\u62E9\u4E00\u4E2A\u51FA\u5DEE\u7533\u8BF7\u7236\u6D41\u7A0B\uFF0C\u5E76\u628A\u7533\u8BF7\u4FE1\u606F\u5E26\u5165\u5230\u62A5\u9500\u5355\u91CC\u9762\u3002
   - \u5728\u6267\u884C\u91C7\u8D2D\u5165\u5E93\u7684\u65F6\u5019\uFF0C\u8981\u9009\u62E9\u4E00\u4E2A\u91C7\u8D2D\u7533\u8BF7\u5355\u4F5C\u4E3A\u7236\u6D41\u7A0B\u3002
  
  
    `);u(this,"Desc5",`
  #### \u5E2E\u52A9
   - \u5728\u6D41\u7A0B\u8FD0\u884C\u8FC7\u7A0B\u4E2D\uFF0C\u9009\u62E9\u591A\u4E2A\u5DF2\u5B8C\u6210\u7684\u6D41\u7A0B\u505A\u4E3A\u8BE5\u6D41\u7A0B\u7684\u5B50\u6D41\u7A0B\uFF0C\u6279\u91CF\u7684\u53D1\u8D77\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u591A\u6807\u6BB5\u7684\u8DEF\u6865\u5EFA\u8BBE\u4E2D\uFF0C\u6BCF\u4E00\u6BB5\u7684\u5EFA\u8BBE\u8FDB\u5EA6\u90FD\u4E3A\u4E00\u4E2A\u6D41\u7A0B\u3002
   - \u5728\u62A5\u603B\u90E8\u5BA1\u6279\u65F6\uFF0C\u53EF\u4EE5\u628A\u6BCF\u4E00\u6BB5\u6D41\u7A0B\u505A\u4E3A\u4E00\u4E2A\u5B50\u6D41\u7A0B\uFF0C\u591A\u4E2A\u5B50\u6D41\u7A0B\u4E00\u8D77\u5BA1\u6279\u3002
   - \u6CE8\u610F\uFF0C\u5404\u5B50\u6D41\u7A0B\u5FC5\u987B\u4E3A\u540C\u4E00\u7C7B\u522B\u3002\u5177\u6709\u76F8\u540C\u7684\u5DE5\u4F5C\u3002
   
    
      `);u(this,"Desc6",`
  #### \u5E2E\u52A9
     
   - \u6B64\u6A21\u5F0F\u4EC5\u4EC5\u9650\u4E8E\u5F00\u59CB\u8282\u70B9\u662F\u8868\u5355\u6811\u7684\u6A21\u5F0F\u3002
   - \u5F53\u5F00\u59CB\u8282\u70B9\u7ED1\u5B9A\u591A\u4E2A\u8868\u5355\u7684\u65F6\u5019\uFF0C\u6D41\u7A0B\u53D1\u8D77\u9700\u8981\u9009\u62E9\u4E00\u4E2A\u6D3B\u7684\u591A\u4E2A\u8868\u5355\u542F\u52A8\u6D41\u7A0B\u3002

    
      `);u(this,"Desc7",`
  #### \u5E2E\u52A9
      
   - \u8BF7\u8BBE\u7F6EURL\u5728\u6587\u672C\u6846\u91CC\u3002
   - \u8BE5URL\u662F\u4E00\u4E2A\u5217\u8868\uFF0C\u5728\u6BCF\u4E00\u884C\u7684\u6570\u636E\u91CC\u6709\u4E00\u4E2A\u8FDE\u63A5\u94FE\u63A5\u5230\u5DE5\u4F5C\u5904\u7406\u5668\u4E0A\uFF08/WF/MyFlow.htm\uFF09
   - \u8FDE\u63A5\u5230\u5DE5\u4F5C\u5904\u7406\u5668\uFF08 WF/MyFlow.htm\uFF09\u5FC5\u987B\u67092\u4E2A\u53C2\u6570FK_Flow=xxx&IsCheckGuide=1
   - \u60A8\u53EF\u4EE5\u6253\u5F00Demo: /SDKFlowDemo/TestCase/StartGuideSelfUrl.htm \u8BE6\u7EC6\u7684\u8BF4\u660E\u4E86\u8BE5\u529F\u80FD\u5982\u4F55\u5F00\u53D1\u3002
    
    `);u(this,"Desc8",`
  #### \u5E2E\u52A9
   - \u4E8C\u7EF4\u7801\uFF1A\u9002\u7528\u4E8E\u79FB\u52A8\u7AEF\uFF0C\u626B\u4E8C\u7EF4\u7801\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C\u3002
   - \u6761\u7801\uFF1A\u9002\u7528\u4E8E\u79FB\u52A8\u7AEF\uFF0C\u626B\u6761\u7801\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C\u3002
  #### \u573A\u666F
   - \u5229\u7528\u626B\u63CF\u67AA\u6216\u624B\u673A\u626B\u63CF\u529F\u80FD\uFF0C\u628A\u5E26\u6709\u8BC6\u522B\u7801\u7684\u7269\u54C1\u4FE1\u606F\u5F55\u5165\u5230\u7CFB\u7EDF\u4E2D\u3002
   - \u6BD4\u5982\uFF1A\u7EF4\u4FEE\u5DE5\u5177\u5F52\u8FD8\u6D41\u7A0B\u4E2D\uFF0C\u53EF\u4EE5\u5229\u7528\u5916\u7F6E\u626B\u63CF\u67AA\uFF0C\u626B\u63CF\u5DE5\u5177\u4E0A\u7684\u8BC6\u522B\u7801\uFF08\u4E8C\u7EF4\u7801\u6216\u6761\u7801\uFF09\u3002
   - \u628A\u5DE5\u5177\u7684\u7F16\u53F7\uFF0C\u540D\u79F0\u7B49\u4FE1\u606F\u81EA\u52A8\u5F55\u5165\u5230\u8868\u683C\u4E2D\uFF0C\u5229\u7528\u53D1\u8D77\u524D\u7F6E\u5BFC\u822A\u529F\u80FD\uFF0C\u9009\u4E2D\u8FD9\u4E9B\u5DE5\u5177\uFF0C\u53D1\u8D77\u5F52\u8FD8\u6D41\u7A0B\u3002
    
  
    `);this.PageTitle="\u53D1\u8D77\u524D\u7F6E\u5BFC\u822A"}Init(){this.entity=new o,this.KeyOfEn=s.StartGuideWay,this.AddGroup("A","\u6570\u636E\u6E90\u914D\u7F6E\u6A21\u5F0F"),this.Blank("0","\u4E0D\u4F7F\u7528",this.Desc0),this.AddEntity("1","\u6309\u8BBE\u7F6E\u7684SQL-\u5355\u6761\u6A21\u5F0F",new e,this.Desc1),this.AddEntity("2","\u6309\u8BBE\u7F6E\u7684SQL-\u591A\u6761\u6A21\u5F0F(\u7528\u4E8E\u6279\u91CF\u53D1\u8D77)",new D,this.Desc2),this.AddGroup("B","\u6D41\u7A0B\u6570\u636E\u83B7\u53D6\u6A21\u5F0F"),this.SingleTBSQL("3","\u4ECE\u5386\u53F2\u53D1\u8D77\u7684\u6D41\u7A0BCopy\u6570\u636E(\u67E5\u8BE2\u5386\u53F2\u8BB0\u5F55)","StartGuidePara1",this.Desc3),this.SingleTBSQL("4","\u7236\u5B50\u6D41\u7A0B\u6A21","StartGuidePara1",this.Desc4),this.AddEntity("5","\u5B50\u6D41\u7A0B\u5B9E\u4F8B\u5217\u8868\u6A21\u5F0F-\u591A\u6761)",new D,this.Desc5),this.AddGroup("C","\u626B\u7801\u5F55\u5165"),this.Blank("8","\u4E8C\u7EF4\u7801",this.Desc8),this.Blank("9","\u6761\u7801",this.Desc8),this.AddGroup("D","\u5176\u5B83\u6A21\u5F0F"),this.SingleEnumDDL("6","\u5F00\u59CB\u8282\u70B9\u7ED1\u5B9A\u7684\u72EC\u7ACB\u8868\u5355\u5217\u8868","StartGuidePara1",this.Desc6,"@0=\u5355\u9009\u6A21\u5F0F@1=\u591A\u9009\u6A21\u5F0F"),this.SingleTB("7","\u6309\u81EA\u5B9A\u4E49\u7684Url ","StartGuidePara1",this.Desc7,"\u8BF7\u8F93\u5165url."),this.SingleRichTxt("10","\u53D1\u8D77\u8BF4\u660EHTML\u6587\u672C","StartGuidePara1","\u8BF7\u8F93\u5165\u5185\u5BB9",this.Desc10)}AfterSave(F,A){if(F==A)throw new Error("Method not implemented.")}BtnClick(F,A,C){if(F==A||F===C)throw new Error("Method not implemented.")}}export{N as GPE_StartGuide};
