import{u as l,d as t,i as d,e as u,c}from"./light-0dfdc1ad.js";import{c as f}from"./use-config-816d55a6.js";import{k as h,bg as p,d as m,dO as b,a8 as g}from"./index-f4658ae7.js";function M(e,a,r){if(!a)return;const n=l(),o=h(f,null),s=()=>{const i=r.value;a.mount({id:i===void 0?e:i+e,head:!0,anchorMetaName:t,props:{bPrefix:i?`.${i}-`:void 0},ssr:n}),o!=null&&o.preflightStyleDisabled||d.mount({id:"n-global",head:!0,anchorMetaName:t,ssr:n})};n?s():p(s)}const y=u("base-icon",`
 height: 1em;
 width: 1em;
 line-height: 1em;
 text-align: center;
 display: inline-block;
 position: relative;
 fill: currentColor;
 transform: translateZ(0);
`,[c("svg",`
 height: 1em;
 width: 1em;
 `)]),P=m({name:"BaseIcon",props:{role:String,ariaLabel:String,ariaDisabled:{type:Boolean,default:void 0},ariaHidden:{type:Boolean,default:void 0},clsPrefix:{type:String,required:!0},onClick:Function,onMousedown:Function,onMouseup:Function},setup(e){M("-base-icon",y,b(e,"clsPrefix"))},render(){return g("i",{class:`${this.clsPrefix}-base-icon`,onClick:this.onClick,onMousedown:this.onMousedown,onMouseup:this.onMouseup,role:this.role,"aria-label":this.ariaLabel,"aria-hidden":this.ariaHidden,"aria-disabled":this.ariaDisabled},this.$slots)}});export{P as N,M as u};
