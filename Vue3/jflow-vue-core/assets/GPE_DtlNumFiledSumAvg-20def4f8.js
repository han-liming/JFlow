var s=Object.defineProperty;var B=(F,t,u)=>t in F?s(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var p=(F,t,u)=>(B(F,typeof t!="symbol"?t+"":t,u),u);var e=(F,t,u)=>new Promise((E,m)=>{var A=i=>{try{r(u.next(i))}catch(o){m(o)}},l=i=>{try{r(u.throw(i))}catch(o){m(o)}},r=i=>i.done?E(i.value):Promise.resolve(i.value).then(A,l);r((u=u.apply(F,t)).next())});import{b as C,a as n}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as a}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class J extends a{constructor(){super("GPE_DtlNumFiledSumAvg");p(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u5BF9\u4ECE\u8868\u7684\u5217\uFF0C\u8FDB\u884C\u6C42\u548C\uFF0C\u6C42\u5E73\u5747\u3001\u6C42\u6700\u5927\u3001\u6C42\u6700\u5C0F\u8BA1\u7B97\u663E\u793A\u3002
   - \u6C42\u51FA\u6765\u7684\u6570\u636E\u5448\u73B0\u5728\u4ECE\u8868\u7684\u5E95\u90E8\u3002
   - \u5BF9\u5F53\u524D\u5B57\u6BB5\u662F\u4ECE\u8868\u6709\u6548\u3002
   - \u914D\u7F6E\u56FE\u4F8B1
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/DtlNumFiledSumAvg/Img/AutoFullDtl1.png "\u5C4F\u5E55\u622A\u56FE.png") 

   - \u914D\u7F6E\u56FE\u4F8B2
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/DtlNumFiledSumAvg/Img/AutoFullDtl.png "\u5C4F\u5E55\u622A\u56FE.png") 
 
   - \u914D\u7F6E\u56FE\u4F8B3
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/DtlNumFiledSumAvg/Img/AutoFullDtl2.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
   - \u8FD0\u884C\u56FE\u4F8B
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/DtlNumFiledSumAvg/Img/AutoFullDtlXianshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
   
  .`);p(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u7981\u7528\uFF1A\u4E0D\u5BF9\u4ECE\u8868\u5217\u8FDB\u884C\u8BA1\u7B97\u3002
  - \u5BF9\u5F53\u524D\u5B57\u6BB5\u662F\u4ECE\u8868\u6709\u6548\u3002
  - \u5BF9\u4ECE\u8868\u7684\u5217\uFF0C\u8FDB\u884C\u6C42\u548C\uFF0C\u6C42\u5E73\u5747\u3001\u6C42\u6700\u5927\u3001\u6C42\u6700\u5C0F\u8BA1\u7B97\u663E\u793A\u3002
  - \u6C42\u51FA\u6765\u7684\u6570\u636E\u5448\u73B0\u5728\u4ECE\u8868\u7684\u5E95\u90E8\u3002
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/DtlNumFiledSumAvg/Img/AutoFullDtlXianshi.png "\u5C4F\u5E55\u622A\u56FE.png") 


  `);this.PageTitle="\u6C42\u5408,\u5E73\u5747\u663E\u793A"}Init(){return e(this,null,function*(){this.entity=new C,this.KeyOfEn=n.DoWay,yield this.entity.InitDataForMapAttr("NumFiledSumAvg",this.GetRequestVal("PKVal")),this.AddGroup("A","\u626B\u7801\u5F55\u5165"),this.Blank("0","\u7981\u7528",this.Desc0),this.Blank("1","\u663E\u793A\u5408\u8BA1",this.Desc1),this.Blank("2","\u663E\u793A\u5E73\u5747\u6570",this.Desc1),this.Blank("3","\u663E\u793A\u6700\u5927",this.Desc1),this.Blank("4","\u663E\u793A\u6700\u5C0F",this.Desc1)})}BtnClick(u,E,m){}AfterSave(u,E){return e(this,null,function*(){if(u==E)throw new Error("Method not implemented.")})}}export{J as GPE_DtlNumFiledSumAvg};
