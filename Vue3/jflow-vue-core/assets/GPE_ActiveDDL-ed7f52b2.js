var a=Object.defineProperty;var c=(B,E,u)=>E in B?a(B,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):B[E]=u;var i=(B,E,u)=>(c(B,typeof E!="symbol"?E+"":E,u),u);var o=(B,E,u)=>new Promise((A,C)=>{var r=F=>{try{t(u.next(F))}catch(D){C(D)}},e=F=>{try{t(u.throw(F))}catch(D){C(D)}},t=F=>F.done?A(F.value):Promise.resolve(F.value).then(r,e);t((u=u.apply(B,E)).next())});import{PageBaseGroupEdit as s}from"./PageBaseGroupEdit-202e8e85.js";import{b as l,a as g}from"./MapExt-db8cd7f3.js";import{GPEActiveDDLSFTable as L}from"./GPEActiveDDLSFTable-93814b78.js";import{GPEActiveDDLSelfSetting as S}from"./GPEActiveDDLSelfSetting-e8f4a6d8.js";import{GPNReturnObj as p,GPNReturnType as m}from"./PageBaseGroupNew-ee20c033.js";import{GloComm as n}from"./GloComm-7cfbdfd9.js";import{SFTable as d}from"./SFTable-d63f9fb4.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFDBSrc-e641ea16.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";class eu extends s{constructor(){super("GPE_ActiveDDL");i(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u5F53\u4E00\u4E2A\u4E0B\u62C9\u6846\u7684\u6570\u636E\u6E90\u53D8\u5316\u540E\uFF0C\u53E6\u5916\u4E00\u4E2A\u4E0B\u62C9\u6846\u7684\u6570\u636E\u540C\u65F6\u53D1\u751F\u53D8\u5316\uFF0C\u6211\u4EEC\u628A\u8FD9\u6837\u7684\u884C\u4E3A\u79F0\u4E3A\u7EA7\u8054\u4E0B\u62C9\u6846.
  - \u6BD4\u5982\uFF1A \u5927\u7C7B\u3001\u5C0F\u7C7B\uFF0C\u5B9E\u73B0\u7EA7\u8054\u3002
  - \u6BD4\u5982: \u7247\u533A\u3001\u7701\u4EFD\u3001\u57CE\u5E02\u3001\u5E02\u53BF\u3002
  - \u5982\u679C\u5B9E\u73B0\u4E24\u4E24\u7EA7\u8054\uFF0C\u5C31\u53EF\u4EE5\u5B9E\u73B0\u65E0\u9650\u5236\u7684\u7EA7\u8054\u3002
  - \u7EA7\u8054\u5173\u7CFB\u4F53\u73B0\u4E3B\u8868\u4E0A\uFF0C\u4E5F\u53EF\u4F53\u73B0\u4ECE\u8868\u4E0A\u3002
  #### \u4E3B\u8868\u65E0\u9650\u7EA7\u8054\u6548\u679C\u56FE
  - \u7701\u4EFD\u57CE\u5E02
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ActiveDDL/Img/MainTable.png "\u7701\u4EFD\u57CE\u5E02.png")  

  #### \u4ECE\u8868\u7684\u7EA7\u8054\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ActiveDDL/Img/Dtl.png "\u7701\u4EFD\u57CE\u5E02\u4ECE\u8868.png")  

  `);i(this,"GPEActiveDDLSelfSetting",`
  #### \u5E2E\u52A9
- \u4E3A\u4E86\u517C\u5BB9\u65E7\u7248\u672C\uFF0C\u4FDD\u7559\u6B64\u6A21\u5F0F,\u65B0\u6A21\u5F0F\u4E0B\uFF0C\u5EFA\u8BAE\u4F7F\u7528,\u67E5\u8BE2.

 - \u5728\u4E0B\u9762\u6587\u672C\u6846\u4E2D\u8F93\u5165\u4E00\u4E2ASQL,\u5177\u6709\u7F16\u53F7\uFF0C\u6807\u7B7E\u5217\uFF0C\u7528\u6765\u7ED1\u5B9A\u4E0B\u4ECE\u52A8\u4E0B\u62C9\u6846\u3002
 - \u6211\u4EEC\u5EFA\u8BAE\u4F7F\u7528\u67E5\u8BE2\u7684\u6A21\u5F0F\u8BBE\u7F6E.

 #### \u5173\u7CFB\u6570\u636E\u5E93\u914D\u7F6E
 - \u6BD4\u5982: SELECT No, Name FROM CN_SF WHERE FK_PQ = '@Key'
 - SELECT No, Name FROM CN_City WHERE FK_SF = '@Key'
 - \u8BF4\u660E:@Key\u662Fccflow\u7EA6\u5B9A\u7684\u5173\u952E\u5B57\uFF0C\u662F\u4E3B\u4E0B\u62C9\u6846\u4F20\u9012\u8FC7\u6765\u7684\u503C\u3002
 - \u4E3B\u83DC\u5355\u662F\u7F16\u53F7\u7684\u662F\u4ECE\u52A8\u83DC\u5355\u7F16\u53F7\u7684\u524D\u51E0\u4F4D\uFF0C\u4E0D\u5FC5\u8054\u52A8\u5185\u5BB9\u3002
 - \u6BD4\u5982: \u4E3B\u4E0B\u62C9\u6846\u662F\u7701\u4EFD\uFF0C\u8054\u52A8\u83DC\u5355\u662F\u57CE\u5E02\u3002
 #### WebApi\u6A21\u5F0F\u914D\u7F6E.
 - \u8BBE\u7F6Eurl: /xxxx/@WebUser.No/@MyFormName/@WorkID
 - \u683C\u5F0F\u8BF4\u660E: 
 - 1. \u8BBE\u7F6E\u9009\u62E9\u7684web\u670D\u52A1\u5730\u5740\u7684\u540E\u90E8\u5206.
 - 2. \u8BBE\u7F6E\u7684\u5185\u5BB9\u5982\u679C\u9700\u8981\u53D8\u91CF\u5C31\u4F7F\u7528@+\u5B57\u6BB5\u540D.
 - 3. \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F.
 #### \u914D\u7F6E\u56FE
 -  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ActiveDDL/Img/MainTableSetting.png "\u7701\u4EFD\u57CE\u5E02.png")  

 #### \u4E3B\u8868\u65E0\u9650\u7EA7\u8054\u6548\u679C\u56FE
  - \u7701\u4EFD\u57CE\u5E02
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ActiveDDL/Img/MainTable.png "\u7701\u4EFD\u57CE\u5E02.png")  

 #### \u4ECE\u8868\u7684\u7EA7\u8054\u6548\u679C\u56FE.
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ActiveDDL/Img/Dtl.png "\u7701\u4EFD\u57CE\u5E02\u4ECE\u8868.png")  
  ...
  `);i(this,"GPEActiveDDLSFTable",`
  #### \u67E5\u8BE2\u5B9A\u4E49
  - \u628A\u8868\u5355\u4E2D\u7528\u5230\u5BF9\u5916\u90E8\u6570\u636E\u7684\u83B7\u53D6\u884C\u4E3A\uFF0C\u6211\u4EEC\u79F0\u4E3A\u67E5\u8BE2.
  - \u67E5\u8BE2\u6709\u4E00\u4E9B\u53C2\u6570\u8F93\u5165\uFF0C\u901A\u8FC7\u67E5\u8BE2\u7C7B\u7684\u8BA1\u7B97\u83B7\u5F97\u6570\u636E\u6E90.
  #### \u5982\u4F55\u8BBE\u7F6E\u67E5\u8BE2?
  - \u5728\u8868\u5355\u8BBE\u8BA1\u5668\u5DE5\u5177\u680F\u4E2D\uFF0C\u627E\u5230\u7CFB\u7EDF\u7BA1\u7406\uFF0C\u67E5\u8BE2\u7BA1\u7406.
  - \u8FDB\u5165\u67E5\u8BE2\u5217\u8868\uFF0C\u7136\u540E\u65B0\u5EFA\u67E5\u8BE2.
  #### \u5E2E\u52A9
 - \u67E5\u8BE2\u5B9A\u4E49: \u8F93\u5165\u8981\u6C42\u7684\u53C2\u6570\uFF0C\u7ECF\u8FC7\u7CFB\u7EDF\u5904\u7406\uFF0C\u8FD4\u56DE\u8981\u6307\u5B9A\u7684\u6570\u636E.
 - \u4E00\u4E2A\u67E5\u8BE2\u901A\u5E38\u6709\u5173\u7CFB\u6570\u636E\u5E93\u4E0Eweb\u670D\u52A1\u4E24\u4E2A\u7C7B\u578B.
 - \u67E5\u8BE2\uFF1A\u5C31\u662F\u4ECE\u5DF2\u7ECF\u8BBE\u7F6E\u7684\u67E5\u8BE2\u5E93\u4E2D\u9009\u62E9\u4E00\u4E2A\u67E5\u8BE2.
 - \u7CFB\u7EDF\u8BBE\u7F6E\u7684\u67E5\u8BE2\u5E93\uFF0C\u90FD\u5DF2\u7ECF\u7EA6\u5B9A\u597D\u4E86\u8981\u8F93\u5165\u7684\u53C2\u6570\u540D\u5B57.
 #### \u53C2\u6570\u683C\u5F0F
 - \u683C\u5F0F: {Url\u53C2\u6570\u540D1}=@\u5B57\u6BB5\u540D\u6216\u8005ccbpm\u8868\u8FBE\u5F0F1;{Url\u53C2\u6570\u540D2}=@\u5B57\u6BB5\u540D\u6216\u8005ccbpm\u8868\u8FBE\u5F0F2;
 - \u591A\u4E2A\u53C2\u6570\u4F7F\u7528\u5206\u53F7\u5206\u9694.
 - \u5B9E\u4F8B: Url\u683C\u5F0F: /BU_PDT/{appcode}/{accesstoken}/{BU}
 - \u5B9E\u4F8B\u914D\u7F6E: {BU_PDT}=@BU;{appcode}=@WebUser.No;{accesstoken}=@Token
 #### \u914D\u7F6E\u56FE
 - \u5F85\u63D0\u4F9B
  ...
  `);this.PageTitle="\u7EA7\u8054\u4E0B\u62C9\u6846"}Init(){return o(this,null,function*(){this.entity=new l,this.KeyOfEn=g.DoWay,this.Btns=[{pageNo:"2",list:["\u5B57\u5178\u7EF4\u62A4"]}],yield this.entity.InitDataForMapAttr("ActiveDDL",this.GetRequestVal("PKVal")),this.AddGroup("A","\u7EA7\u8054\u4E0B\u62C9\u6846"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.AddEntity("1","\u914D\u7F6E\u6570\u636E\u6E90",new S,this.GPEActiveDDLSelfSetting),this.AddEntity("2","\u7ED1\u5B9A\u5B57\u5178\u8868",new L,this.GPEActiveDDLSFTable)})}BtnClick(u,A,C){return o(this,null,function*(){var r;if(C=="\u5B57\u5178\u5C5E\u6027"){const e=(r=this.entity)==null?void 0:r.Doc;if(!e){alert("\u8BF7\u9009\u62E9\u7ED1\u5B9A\u7684\u5B57\u5178\uFF0C\u7136\u540E\u6267\u884C\u4FDD\u5B58\u6309\u94AE.");return}const t=new d(e);yield t.Retrieve();const F=t.GetParaString("EnName",""),D=n.UrlEn(F,t.No);return new p(m.OpenUrlByDrawer75,D)}if(C=="\u5B57\u5178\u7EF4\u62A4"){const e=n.UrlSearch("TS.FrmUI.SFTable");return new p(m.OpenUrlByDrawer75,e)}throw new Error(`\u6CA1\u6709\u5B9A\u4E49\u6B64\u6309\u94AE\u3010${C}\u3011\u7684\u4E8B\u4EF6\uFF0C\u8BF7\u68C0\u67E5\uFF01`)})}AfterSave(u,A){}}export{eu as GPE_ActiveDDL};
