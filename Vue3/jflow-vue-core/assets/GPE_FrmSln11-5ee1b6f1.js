var E=Object.defineProperty;var m=(t,u,F)=>u in t?E(t,u,{enumerable:!0,configurable:!0,writable:!0,value:F}):t[u]=F;var r=(t,u,F)=>(m(t,typeof u!="symbol"?u+"":u,F),F);import{PageBaseGroupEdit as p}from"./PageBaseGroupEdit-202e8e85.js";import{Node as i,NodeAttr as B}from"./Node-6b42ba5e.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class w extends p{constructor(){super("GPE_Sln11");r(this,"FoolForm",`
  #### \u8BF4\u660E 
   - \u8BBE\u8BA1\u65B9\u4FBF\uFF0C\u754C\u9762\u7B80\u6D01\u6E05\u6670\u3002
   - \u5B57\u6BB5\u7684\u987A\u5E8F\u53EF\u4EE5\u901A\u8FC7\u62D6\u62FD\u5B9E\u73B0\u79FB\u52A8,\u901A\u8FC7\u6805\u680F\u683C\u6765\u5E03\u5C40\u754C\u9762\u5143\u7D20\u3002
   - \u53EF\u4EE5\u901A\u8FC7\u5B9A\u4E49\u6587\u672C\u5C5E\u6027\u6765\u4F53\u73B0\u4E0D\u540C\u63A7\u4EF6\u7684\u5C55\u793A\u8981\u6C42\uFF08\u6587\u672C\uFF0C\u5355\u9009\uFF0C\u591A\u9009\uFF0C\u5B9A\u4F4D\uFF0C\u8BC4\u5206\uFF0C\u591A\u9644\u4EF6\uFF0C\u5730\u56FE\uFF0C\u8EAB\u4EFD\u8BC1\u8BC6\u522B\u7B49\uFF09\u6EE1\u8DB3\u8868\u5355\u8981\u6C42\u3002
 
  #### \u56FE\u4F8B
 -  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FoolFrmD.png "\u5C4F\u5E55\u622A\u56FE.png")
   
  #### \u6837\u5F0F1
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/shagua.png "\u5C4F\u5E55\u622A\u56FE.png")

 
  #### \u6837\u5F0F2
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/shagua2.png "\u5C4F\u5E55\u622A\u56FE.png")
   
  `);this.PageTitle="\u7ED1\u5B9A\u5355\u8868\u5355"}Init(){this.Btns="\u5E2E\u52A9",this.entity=new i,this.KeyOfEn=B.NodeFrmID,this.AddGroup("A","\u7ED1\u5B9A\u5355\u8868\u5355")}AfterSave(F,o){}BtnClick(F,o,e){}}export{w as GPE_Sln11};
