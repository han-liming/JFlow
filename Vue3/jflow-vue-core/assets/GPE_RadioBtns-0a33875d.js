var m=Object.defineProperty;var A=(F,t,u)=>t in F?m(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var s=(F,t,u)=>(A(F,typeof t!="symbol"?t+"":t,u),u);var p=(F,t,u)=>new Promise((o,a)=>{var i=E=>{try{r(u.next(E))}catch(B){a(B)}},e=E=>{try{r(u.throw(E))}catch(B){a(B)}},r=E=>E.done?o(E.value):Promise.resolve(E.value).then(i,e);r((u=u.apply(F,t)).next())});import{b as C,a as n,M as g}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as c}from"./PageBaseGroupEdit-202e8e85.js";import{MapAttr as d,MapAttrs as D}from"./MapAttr-cb594d82.js";import{bp as R}from"./index-f4658ae7.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./Events-141c34ea.js";class Z extends c{constructor(){super("GPE_RadioBtns");s(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u4E0D\u542F\u7528\uFF1A\u4E0D\u5BF9\u5176\u5B83\u63A7\u4EF6\u8FDB\u884C\u63A7\u5236\u3002
   - \u9009\u9879\u8054\u52A8\u63A7\u4EF6\uFF0C\u5C31\u662F\u9009\u62E9\u4E00\u4E2AItem,\u5BF9\u5176\u4ED6\u63A7\u4EF6\u5B9E\u73B0\u9690\u85CF\uFF0C\u663E\u793A\uFF0C\u8BBE\u7F6E\u7279\u5B9A\u7684\u503C\u7684\u529F\u80FD\u3002
   - \u6BD4\u5982: \u8BF7\u5047\u8868\u5355\u7684\u8BF7\u5047\u7C7B\u578B\uFF0C\u9009\u62E9\u75C5\u5047\uFF0C\u8BA9\u5176\u4E0A\u4F20\u75C5\u4F8B\uFF0C\u586B\u5199\u6240\u5728\u533B\u9662\uFF0C\u8BF7\u4E8B\u5047\u5C31\u4E0D\u9700\u8981\u663E\u793A\u9644\u4EF6\u4E0A\u4F20\u4E0E\u6240\u5728\u533B\u9662\u63A7\u4EF6\u3002
  #### \u9009\u62E9\u75C5\u5047-\u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtns.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u9009\u62E9\u4E8B\u5047-\u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtns1.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);s(this,"Desc1",`

  #### \u5E2E\u52A9
  - \u5C31\u662F\u9009\u62E9\u4E00\u4E2AItem,\u5BF9\u5176\u4ED6\u63A7\u4EF6\u5B9E\u73B0\u9690\u85CF\uFF0C\u663E\u793A\uFF0C\u8BBE\u7F6E\u7279\u5B9A\u7684\u503C\u7684\u529F\u80FD\u3002
  - \u70B9\u51FB\u8BBE\u7F6E\u8FDB\u5165\u8BE6\u7EC6\u8BBE\u7F6E.
  - \u914D\u7F6E\u56FE\u4F8B1
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtnsSetting.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u914D\u7F6E\u56FE\u4F8B2
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtnsSetting1.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u8FD0\u884C\u56FE\u4F8B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtns.png "\u5C4F\u5E55\u622A\u56FE.png") 
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RadioBtns/Img/RadioBtns1.png "\u5C4F\u5E55\u622A\u56FE.png") 

  `);this.PageTitle="\u9009\u9879\u8054\u52A8\u63A7\u4EF6"}Init(){return p(this,null,function*(){const{loadComponent:u}=R();this.entity=new C,this.KeyOfEn=n.DoWay,yield this.entity.InitDataForMapAttr("RBAction",this.GetRequestVal("PKVal")),this.AddGroup("A","\u9009\u9879\u8054\u52A8\u63A7\u4EF6"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.SelfComponent("1","\u542F\u7528\u8BBE\u7F6E",u("/@/WF/Admin/FrmLogic/Views/RadioBtns.vue"),{EnClassID:this.EnClassID,PKVal:this.PKVal,enable:!1})})}BtnClick(u,o,a){return p(this,null,function*(){if(u=="0"||o=="0"||a=="0")return;const i=new d(this.PKVal);yield i.Retrieve(),new D().Retrieve("FK_MapData",i.FK_MapData),new g().Retrieve(n.FK_MapData,i.FK_MapData,n.ExtModel,"RBAction",n.ExtType,"RBActionAttr")})}AfterSave(u,o){return p(this,null,function*(){})}}export{Z as GPE_RadioBtns};
