var d=Object.defineProperty;var c=(F,t,u)=>t in F?d(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var e=(F,t,u)=>(c(F,typeof t!="symbol"?t+"":t,u),u);var p=(F,t,u)=>new Promise((o,r)=>{var E=i=>{try{m(u.next(i))}catch(s){r(s)}},a=i=>{try{m(u.throw(i))}catch(s){r(s)}},m=i=>i.done?o(i.value):Promise.resolve(i.value).then(E,a);m((u=u.apply(F,t)).next())});import{G as l}from"./DataType-33901a1c.js";import{PageBaseGroupEdit as A}from"./PageBaseGroupEdit-202e8e85.js";import{Node as n,NodeAttr as B}from"./Node-6b42ba5e.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class M extends A{constructor(){super("GPE_FrmPos");e(this,"DescCurrentFrm",`
  #### \u8BF4\u660E
   - \u662F\u4F7F\u7528\u5F53\u524D\u8282\u70B9\u7684\u8868\u5355.
  `);e(this,"DescEtc",`
  #### \u5E2E\u52A9
   - \u652F\u6301\u7F16\u5199js,Html\u6A21\u5F0F\uFF0C\u4F7F\u7528\u5BCC\u6587\u672C\u7F16\u8F91\u5668\u5F00\u53D1.
  #### \u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/kaifaze.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);e(this,"DescPri",`
  #### \u5E2E\u52A9
   - \u652F\u6301\u7F16\u5199js,Html\u6A21\u5F0F\uFF0C\u4F7F\u7528\u5BCC\u6587\u672C\u7F16\u8F91\u5668\u5F00\u53D1.
  #### \u6837\u5F0F
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/kaifaze.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);this.PageTitle="\u8868\u5355\u5F15\u7528",this.Btns="\u5E2E\u52A9"}Init(){this.entity=new n,this.KeyOfEn="NodeFrmRef",this.AddGroup("A","\u5F15\u7528\u4F4D\u7F6E"),this.Blank("0","\u5F53\u524D\u8282\u70B9\u8868\u5355",this.DescCurrentFrm),this.SelectItemsByList("1","\u5F15\u7528\u5176\u4ED6\u8282\u70B9\u8868\u5355",this.DescEtc,!1,l.sqlNodeFrmList,B.NodeFrmID)}AfterSave(u,o){return p(this,null,function*(){if(u==="0"){const r=new n;r.NodeID=this.PKVal,yield r.Retrieve(),r.NodeFrmID="",yield r.Update()}})}BtnClick(u,o,r){if(u==o||u===r)throw new Error("Method not implemented.")}}export{M as GPE_FrmPos};
