var a=Object.defineProperty;var p=(E,t,u)=>t in E?a(E,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[t]=u;var r=(E,t,u)=>(p(E,typeof t!="symbol"?t+"":t,u),u);import{UAC as i}from"./UAC-8e255d47.js";import{Map as m}from"./Map-73575e6b.js";import{EntityMyPK as n}from"./EntityMyPK-e742fec8.js";import{a as e}from"./MapExt-db8cd7f3.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class h extends n{constructor(u){super("TS.MapExt.GPEDateFieldInputRole");r(this,"JSDesc",` 
  #### \u8BF4\u660E
  \u8BA1\u7B97\u540E\u8981\u89E6\u53D1\u7684\u811A\u672C\u51FD\u6570(\u6BD4\u5982:\u6C42\u548C\u4EE5\u540E\u8981\u6FC0\u6D3B\u7684function)\uFF0C\u8BE5\u811A\u672C\u8981\u6C42\u5199\u5165\u5230:DataUserJSLabMyFromID_Self.js
   `);r(this,"DescTag1",` 
   #### \u8BF4\u660E
   - zhoupeng \u8865\u5145
    `);r(this,"DescDoc",` 
  #### \u8BF4\u660E
  - SQL\u683C\u5F0F\u4E3A:
  - SELECT No,Name FROM Port_Emp WHERE PinYin LIKE '@Key%' OR No LIKE '%@Key%' OR Name LIKE '%@Key%' 
  - SELECT No,Name FROM CN_City WHERE PinYin LIKE '%@Key%' OR Name LIKE '%@Key%'
  - URL\u683C\u5F0F\u4E3A:
  - /DataUser/Handler.ashx?xxx=sss 
  - \u65B9\u6CD5\u7684\u683C\u5F0F\u4E3A:
  - MyFunName
   `);u&&(this.MyPK=u)}get HisUAC(){const u=new i;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new m("Sys_MapExt","\u65E5\u671F\u8F93\u5165\u89C4\u5219");u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK(),u.AddTBString(e.FK_MapData,null,"\u8868\u5355ID",!0,!0,0,100,100),u.AddDDLStringEnum(e.Tag,"GTE","\u8FD0\u7B97\u7B26","@GT=\u5927\u4E8E@GTE=\u5927\u4E8E\u7B49\u4E8E@IT=\u5C0F\u4E8E@ITE=\u5C0F\u4E8E\u7B49\u4E8E@EQ=\u7B49\u4E8E@NEQ=\u4E0D\u7B49\u4E8E",!0,null,!1);const o=`
     SELECT KeyOfEn as No, Name FROM Sys_MapAttr 
     WHERE MyDataType IN(6,7) AND UIVisible=1 AND UIIsEnable=1 AND FK_MapData='@FK_MapData'`;return u.AddDDLSQL(e.Tag1,null,"\u65E5\u671F\u5B57\u6BB5",o,!0,null,!0),this._enMap=u,this._enMap}}export{h as GPEDateFieldInputRole};
