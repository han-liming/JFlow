var i=Object.defineProperty;var e=(F,E,u)=>E in F?i(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var A=(F,E,u)=>(e(F,typeof E!="symbol"?E+"":E,u),u);import{GloComm as C}from"./GloComm-7cfbdfd9.js";import{Node as m,NodeAttr as t}from"./Node-6b42ba5e.js";import{PageBaseGroupEdit as p}from"./PageBaseGroupEdit-202e8e85.js";import{GPNReturnObj as n,GPNReturnType as D}from"./PageBaseGroupNew-ee20c033.js";import"./FrmTrack-10f0746d.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./EntityNodeID-d5ae71b1.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class Q extends p{constructor(){super("GPE_CondModel");A(this,"Desc0",` 
  #### \u5E2E\u52A9
   - \u8BE5\u6A21\u5F0F\u9700\u8981\u4E3A\u6BCF\u4E00\u8DDF\u8FDE\u63A5\u7EBF\u8BBE\u7F6E\u65B9\u5411\u6761\u4EF6\u3002
   - ccbpm\u5728\u53D1\u9001\u7684\u65F6\u5019\u4F1A\u68C0\u67E5\u8FD9\u4E9B\u6761\u4EF6,\u5982\u679C\u6761\u4EF6\u6210\u7ACB\u5C31\u8F6C\u5411\u8FD9\u4E2A\u8282\u70B9\u3002
   - \u8BE5\u6A21\u5F0F\u662F\u8BA9ccbpm\u81EA\u52A8\u4E3A\u60A8\u8BA1\u7B97\u8981\u53D1\u9001\u5230\u7684\u8282\u70B9\u3002
  #### \u6548\u679C\u56FE
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Img/CondModel0.png "\u5C4F\u5E55\u622A\u56FE.png")
   - \u8BF7\u5728\u53F3\u8FB9\u8981\u5230\u8FBE\u7684\u8282\u70B9\u8BBE\u7F6E\u65B9\u5411\u6761\u4EF6\u3002
   `);A(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u89C6\u9891\u6559\u7A0B: https://drive.weixin.qq.com/s?k=AOsAZQczAAYgmfqbFS
   - \u7528\u6237\u53D1\u9001\u7684\u65F6\u5019\uFF0C\u5728\u53D1\u9001\u6309\u94AE\u65C1\u8FB9\u6709\u4E00\u4E2A\u4E0B\u62C9\u6846\uFF0C\u8BE5\u4E0B\u62C9\u6846\u662Fccbpm\u4E3A\u60A8\u8BA1\u7B97\u51FA\u6765\u7684\u53EF\u4EE5\u53D1\u9001\u5230\u7684\u8282\u70B9\u3002
   - \u7531\u64CD\u4F5C\u8005\u6765\u51B3\u5B9A\u8981\u53D1\u9001\u5230\u90A3\u4E2A\u8282\u70B9\u4E0A\u53BB\u3002
   - \u5982\u679C\u60A8\u9009\u62E9\u7684\u8282\u70B9\u7684\u63A5\u6536\u4EBA\u89C4\u5219\u662F\u7531\u4E0A\u4E00\u6B65\u53D1\u9001\u4EBA\u5458\u6765\u9009\u62E9\u7684\uFF0C\u7CFB\u7EDF\u5C31\u4F1A\u5F39\u51FA\u63A5\u53D7\u4EBA\u6309\u94AE\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Img/CondModel1.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
  `);A(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u8BE5\u6A21\u5F0F\u591A\u7528\u4E8E\u5206\u5408\u6D41\u8282\u70B9\u3002
   - \u5728\u5F02\u8868\u5355\u7684\u5408\u6D41\u8282\u70B9\u4E0A\u914D\u7F6E\u8BE5\u6A21\u5F0F\uFF0C\u5408\u6D41\u8282\u70B9\u7684\u64CD\u4F5C\u5458\u53D1\u9001\u540E\uFF0C\u5C31\u8F6C\u5230\u8BE5\u9875\u9762\u4E0A\uFF0C\u9009\u62E9\u5230\u8FBE\u7684\u8282\u70B9\u3002

   #### \u56FE\u4F8B
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Img/CondModel2.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u4E5F\u53EF\u4EE5\u7528\u4E8E\u534F\u4F5C\u6A21\u5F0F\u4E0B\u7684\u8282\u70B9,\u5230\u8FBE\u7684\u8282\u70B9\u662F\u9700\u8981\u6700\u540E\u4E00\u4E2A\u4EBA\u5BA1\u6838,\u7531\u6700\u540E\u4E00\u4E2A\u4EBA\u9009\u62E9\u5230\u8FBE\u7684\u8282\u70B9\u4E0E\u63A5\u53D7\u4EBA\u3002
  `);A(this,"Desc3",`
  #### \u5E2E\u52A9
   - \u7528\u6237\u53D1\u9001\u7684\u65F6\u5019\uFF0C\u5728\u53D1\u9001\u6309\u94AE\u6709\u9009\u62E9\u6309\u94AE\uFF0C\u662Fccbpm\u4E3A\u60A8\u8BA1\u7B97\u51FA\u6765\u7684\u53EF\u4EE5\u53D1\u9001\u5230\u7684\u8282\u70B9\u3002
   - \u662F\u7531\u64CD\u4F5C\u8005\u6765\u51B3\u5B9A\u8981\u53D1\u9001\u5230\u90A3\u4E2A\u8282\u70B9\u4E0A\u53BB\u3002
   - \u5982\u679C\u60A8\u9009\u62E9\u7684\u8282\u70B9\u7684\u63A5\u6536\u4EBA\u89C4\u5219\u662F\u7531\u4E0A\u4E00\u6B65\u53D1\u9001\u4EBA\u5458\u6765\u9009\u62E9\u7684\uFF0C\u7CFB\u7EDF\u5C31\u4F1A\u5F39\u51FA\u63A5\u53D7\u4EBA\u6309\u94AE\u3002 
  
  `);this.PageTitle="\u8F6C\u5411\u89C4\u5219"}Init(){this.entity=new m,this.KeyOfEn="CondModel",this.Icon="icon-directions",this.Btns=[{pageNo:"0",list:["\u4F18\u5148\u7EA7"]}],this.AddGroup("A","\u81EA\u52A8\u8BA1\u7B97"),this.Blank("0","\u7531\u8FDE\u63A5\u7EBF\u6761\u4EF6\u63A7\u5236",this.Desc0),this.AddGroup("B","\u4E3B\u89C2\u9009\u62E9"),this.SingleCheckBox("2","\u4E0B\u62C9\u6846\u6A21\u5F0F","\u9000\u56DE\u8282\u70B9\u662F\u5426\u73B0\u5728\u5DE5\u5177\u680F?",t.IsShowReturnNodeInToolbar,this.Desc1),this.SingleCheckBox("3","\u6309\u94AE\u6A21\u5F0F","\u9000\u56DE\u8282\u70B9\u662F\u5426\u73B0\u5728\u5DE5\u5177\u680F?",t.IsShowReturnNodeInToolbar,this.Desc3),this.Blank("1"," \u53D1\u9001\u540E\u624B\u5DE5\u9009\u62E9\u5230\u8FBE\u8282\u70B9\u4E0E\u63A5\u53D7\u4EBA",this.Desc2)}AfterSave(u,B){if(u==B)throw new Error("Method not implemented.")}BtnClick(u,B,o){if(o=="\u4F18\u5148\u7EA7"){this.RefPKVal;const r=C.UrlEn("TS.WF.Template.NodeDir",this.entity.NodeID);return new n(D.OpenUrlByDrawer75,r)}if(u==B||u===o)throw new Error("Method not implemented.")}}export{Q as GPE_CondModel};
