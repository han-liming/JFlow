import{S as T}from"./SelectHelper-5d9fefe7.js";import"./Tabs-ec1c48f8.js";import"./RadioGroup-a80e69a5.js";import k from"./InputTheme-b25bb000.js";import"./FormTheme-70eb47bf.js";import{a8 as h,d as N,dO as O,r as M,f as y,bt as E,aT as P,aU as z,o as j,a as A,w as R,b as V,j as H}from"./index-f4658ae7.js";import{k as D,e as K,c as g,g as b,f as w,h as W,b as U}from"./light-0dfdc1ad.js";import{u as X}from"./use-config-816d55a6.js";import{u as q}from"./use-form-item-34ce685d.js";import{u as G,c as $}from"./use-merged-state-66be05d7.js";import{c as J}from"./create-key-bf4384d6.js";import{u as Q}from"./use-css-vars-class-3ae3b4b3.js";import{c as Y}from"./color-to-class-b0332f36.js";import{N as Z}from"./Icon-e3cbad7d.js";import{N as ee}from"./FormItem-104f9f94.js";const te=e=>{const{railColor:r}=e;return{itemColor:r,itemColorActive:"#FFCC33",sizeSmall:"16px",sizeMedium:"20px",sizeLarge:"24px"}},oe={name:"Rate",common:D,self:te},ae=oe,ne=h("svg",{viewBox:"0 0 512 512"},h("path",{d:"M394 480a16 16 0 01-9.39-3L256 383.76 127.39 477a16 16 0 01-24.55-18.08L153 310.35 23 221.2a16 16 0 019-29.2h160.38l48.4-148.95a16 16 0 0130.44 0l48.4 149H480a16 16 0 019.05 29.2L359 310.35l50.13 148.53A16 16 0 01394 480z"})),re=K("rate",{display:"inline-flex",flexWrap:"nowrap"},[g("&:hover",[b("item",`
 transition:
 transform .1s var(--n-bezier),
 color .3s var(--n-bezier);
 `)]),b("item",`
 position: relative;
 display: flex;
 transition:
 transform .1s var(--n-bezier),
 color .3s var(--n-bezier);
 transform: scale(1);
 font-size: var(--n-item-size);
 color: var(--n-item-color);
 `,[g("&:not(:first-child)",`
 margin-left: 6px;
 `),w("active",`
 color: var(--n-item-color-active);
 `)]),W("readonly",`
 cursor: pointer;
 `,[b("item",[g("&:hover",`
 transform: scale(1.05);
 `),g("&:active",`
 transform: scale(0.96);
 `)])]),b("half",`
 display: flex;
 transition: inherit;
 position: absolute;
 top: 0;
 left: 0;
 bottom: 0;
 width: 50%;
 overflow: hidden;
 color: rgba(255, 255, 255, 0);
 `,[w("active",`
 color: var(--n-item-color-active);
 `)])]),le=Object.assign(Object.assign({},U.props),{allowHalf:Boolean,count:{type:Number,default:5},value:Number,defaultValue:{type:Number,default:null},readonly:Boolean,size:{type:[String,Number],default:"medium"},clearable:Boolean,color:String,onClear:Function,"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array]}),se=N({name:"Rate",props:le,setup(e){const{mergedClsPrefixRef:r,inlineThemeDisabled:s}=X(e),l=U("Rate","-rate",re,ae,e,r),u=O(e,"value"),m=M(e.defaultValue),i=M(null),a=q(e),c=G(u,m);function _(t){const{"onUpdate:value":o,onUpdateValue:n}=e,{nTriggerFormChange:p,nTriggerFormInput:v}=a;o&&$(o,t),n&&$(n,t),m.value=t,p(),v()}function f(t,o){return e.allowHalf?o.offsetX>=Math.floor(o.currentTarget.offsetWidth/2)?t+1:t+.5:t+1}let C=!1;function x(t,o){C||(i.value=f(t,o))}function B(){i.value=null}function F(t,o){var n;const{clearable:p}=e,v=f(t,o);p&&v===c.value?(C=!0,(n=e.onClear)===null||n===void 0||n.call(e),i.value=null,_(null)):_(v)}function L(){C=!1}const S=y(()=>{const{size:t}=e,{self:o}=l.value;return typeof t=="number"?`${t}px`:o[J("size",t)]}),I=y(()=>{const{common:{cubicBezierEaseInOut:t},self:o}=l.value,{itemColor:n,itemColorActive:p}=o,{color:v}=e;return{"--n-bezier":t,"--n-item-color":n,"--n-item-color-active":v||p,"--n-item-size":S.value}}),d=s?Q("rate",y(()=>{const t=S.value,{color:o}=e;let n="";return t&&(n+=t[0]),o&&(n+=Y(o)),n}),I,e):void 0;return{mergedClsPrefix:r,mergedValue:c,hoverIndex:i,handleMouseMove:x,handleClick:F,handleMouseLeave:B,handleMouseEnterSomeStar:L,cssVars:s?void 0:I,themeClass:d==null?void 0:d.themeClass,onRender:d==null?void 0:d.onRender}},render(){const{readonly:e,hoverIndex:r,mergedValue:s,mergedClsPrefix:l,onRender:u,$slots:{default:m}}=this;return u==null||u(),h("div",{class:[`${l}-rate`,{[`${l}-rate--readonly`]:e},this.themeClass],style:this.cssVars,onMouseleave:this.handleMouseLeave},E(this.count,(i,a)=>{const c=m?m({index:a}):h(Z,{clsPrefix:l},{default:()=>ne}),_=r!==null?a+1<=r:a+1<=(s||0);return h("div",{key:a,class:[`${l}-rate__item`,_&&`${l}-rate__item--active`],onClick:e?void 0:f=>{this.handleClick(a,f)},onMouseenter:this.handleMouseEnterSomeStar,onMousemove:e?void 0:f=>{this.handleMouseMove(a,f)}},c,this.allowHalf?h("div",{class:[`${l}-rate__half`,{[`${l}-rate__half--active`]:!_&&r!==null?a+.5<=r:a+.5<=(s||0)}]},c):null)}))}}),ie=N({name:"UniversalRate",components:{NFormItem:ee,SelectHelper:T,NRate:se},props:{widgetInfo:{type:Object,default:()=>{}}},setup(e){var s;return{label:M(e.widgetInfo.title),InputTheme:k,settingUrl:`../../Comm/EnOnly.htm?EnName=TS.FrmUI.SelfCommonent.ExtScore&PKVal=${(s=e.widgetInfo)==null?void 0:s.id}&s=${Math.random()}`}}});const ce={class:"item"};function ue(e,r,s,l,u,m){const i=z("n-rate"),a=z("n-form-item"),c=z("select-helper");return j(),A(c,{widget:e.widgetInfo,"setting-url":e.settingUrl},{default:R(()=>[V(a,{label:e.widgetInfo.title,"show-feedback":!1},{default:R(()=>[H("div",ce,[V(i,{color:"#1890ff"})])]),_:1},8,["label"])]),_:1},8,["widget","setting-url"])}const me=P(ie,[["render",ue],["__scopeId","data-v-724d5b84"]]),Ve=Object.freeze(Object.defineProperty({__proto__:null,default:me},Symbol.toStringTag,{value:"Module"}));export{Ve as _};
