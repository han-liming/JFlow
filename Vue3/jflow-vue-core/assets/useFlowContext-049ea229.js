import{p as e,k as r}from"./index-f4658ae7.js";const t=Symbol("flow-chart");function a(o){e(t,o)}function c(){return r(t)}export{a as createFlowChartContext,c as useFlowChartContext};
