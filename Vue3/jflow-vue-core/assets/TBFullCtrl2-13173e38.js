var i=Object.defineProperty;var o=(t,E,u)=>E in t?i(t,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[E]=u;var r=(t,E,u)=>(o(t,typeof E!="symbol"?E+"":E,u),u);import{UAC as p}from"./UAC-8e255d47.js";import{Map as m}from"./Map-73575e6b.js";import{EntityMyPK as B}from"./EntityMyPK-e742fec8.js";import{a as e}from"./MapExt-db8cd7f3.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class U extends B{constructor(u){super("TS.MapExt.TBFullCtrl2");r(this,"Desc2",`
  #### \u8BF4\u660E
   
  1. \u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2\u7684SQL\u8BED\u53E5\uFF0C\u8BE5SQL\u5FC5\u987B\u5305\u542B No, Name \u5217, \u7528\u4E0E\u5C55\u793A\u5FEB\u901F\u8865\u5168\u7684\u90E8\u5206\u3002
  1. \u8BE5SQL\u5FC5\u987B\u5305\u542B @Key \u5173\u952E\u5B57\uFF0C@Key \u8F93\u5165\u6587\u672C\u6846\u7684\u503C.
  1. SQL\u8FD4\u56DE\u7684\u5217\u4E0E\u5176\u4ED6\u5B57\u6BB5\u540D\u79F0\u4FDD\u6301\u4E00\u81F4\uFF0C\u5C31\u53EF\u4EE5\u5B8C\u6210\u63A7\u4EF6\u6570\u636E\u7684\u81EA\u52A8\u586B\u5145\u3002
  1. \u6BD4\u5982:SELECT No,Name,Name as CaoZuoYuanMingCheng,Tel as DianHua,Email,FK_Dept FROM WF_Emp WHERE No LIKE '@Key%'
  1. \u4E3A\u9632\u6B62URL\u7F16\u7801\u89C4\u5B9Alike\u7684\u7B2C\u4E00\u4E2A%\u5199\u6210[%],\u5982\u679Clike '%@Key%' \u5199\u6210'[%]@Key%'
  `);r(this,"Desc1",`
  #### \u8BF4\u660E
   
  1. \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5728\u53F3\u4E0A\u89D2\u7684\u5217\u8868\u91CC\u67E5\u8BE2\u6216\u70B9\u6811\u6811\u5E72\u7684\u6570\u636E\u6E90\u51FA\u73B0\u7684\u5217\u8868,\u9700\u8981\u7528\u4E2D\u6587\u663E\u793A\u5217\u5934.
  1. \u4F8B\u5982: No=\u7F16\u53F7,Name=\u540D\u79F0,Addr=\u5730\u5740,Tel=\u7535\u8BDD,Email=\u90AE\u4EF6
  
  `);this.RefEnName="TS.Sys.MapExt",u&&(this.MyPK=u)}get HisUAC(){const u=new p;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new m("Sys_MapExt","\u6587\u672C\u6846\u81EA\u52A8\u5B8C\u62102");return u.AddMyPK(),u.AddMyPK(),u.AddDDLSysEnum(e.DBType,0,"\u6570\u636E\u6E90\u7C7B\u578B",!0,!0,"DBType","@0=\u6267\u884CSQL@1=\u6267\u884Curl\u8FD4\u56DEJSON@2=\u6267\u884CCCFromRef.js\u8FD4\u56DEJSON",null,!1),u.AddTBString(e.Tag4,null,"\u6570\u636E\u6E90:",!0,!1,0,50,200,!0,this.Desc2),u.AddTBString(e.Tag3,null,"\u6570\u636E\u5217\u540D\u4E0E\u4E2D\u6587\u610F\u601D\u5BF9\u7167 ",!0,!1,0,50,200,!0,this.Desc1),this._enMap=u,this._enMap}}export{U as TBFullCtrl2};
