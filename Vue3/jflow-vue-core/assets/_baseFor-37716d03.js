function b(s){return function(r,i,o){for(var l=-1,e=Object(r),a=o(r),n=a.length;n--;){var t=a[s?n:++l];if(i(e[t],t,e)===!1)break}return r}}var f=b();const u=f;export{u as b};
