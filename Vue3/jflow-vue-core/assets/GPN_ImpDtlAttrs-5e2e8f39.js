var d=Object.defineProperty;var n=(i,u,t)=>u in i?d(i,u,{enumerable:!0,configurable:!0,writable:!0,value:t}):i[u]=t;var o=(i,u,t)=>(n(i,typeof u!="symbol"?u+"":u,t),t);var E=(i,u,t)=>new Promise((B,p)=>{var F=e=>{try{m(t.next(e))}catch(r){p(r)}},c=e=>{try{m(t.throw(e))}catch(r){p(r)}},m=e=>e.done?B(e.value):Promise.resolve(e.value).then(F,c);m((t=t.apply(i,u)).next())});import{MapAttr as S}from"./MapAttr-cb594d82.js";import{PageBaseGroupNew as D,GPNReturnObj as I,GPNReturnType as f}from"./PageBaseGroupNew-ee20c033.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Events-141c34ea.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class X extends D{constructor(){super("GPN_ImpDtlAttrs");o(this,"Imp",`
  #### \u5E2E\u52A9
  - \u9009\u62E9\u4E00\u4E2A\u4ECE\u8868\uFF0C\u7136\u540E\u5BFC\u5165\u5B57\u6BB5.
  - \u5DF2\u7ECF\u6709\u7684\u5C31\u81EA\u52A8\u521B\u5EFA.
  - 
  #### \u914D\u7F6E\u56FE
  - sdfsdfsd
`);o(this,"Imp_SelectDtl",`
  #### \u5E2E\u52A9
  - csdfsadfsad
  #### \u914D\u7F6E\u56FE
  - sdfsdfsd
`);o(this,"Imp_SelectDtl_SelectFields",`
  #### \u5E2E\u52A9
  - csdfsadfsad
  #### \u914D\u7F6E\u56FE
  - sdfsdfsd
`);this.PageTitle="\u5BFC\u5165\u5B9E\u4F53\u5B57\u6BB5"}Init(){this.AddGroup("A","\u5BFC\u5165\u5B9E\u4F53\u5B57\u6BB5"),this.AddBlank("Imp","\u51C6\u5907",this.Imp),this.SelectItemsByList("Imp.SelectDtl","\u9009\u62E9\u4ECE\u8868",this.Imp_SelectDtl,!1,"SELECT No,Name FROM Sys_MapDtl ");const t="SELECT MyPK as No, Name FROM Sys_MapAttr WHERE FK_MapData='@Imp.SelectDtl_tb1' ";this.SelectItemsByList("Imp.SelectDtl.SelectFields","\u9009\u62E9\u5B57\u6BB5",this.Imp_SelectDtl_SelectFields,!0,t)}GenerSorts(){return E(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(t,B,p,F,c){return E(this,null,function*(){if(t==="Imp.SelectDtl.SelectFields"){const m=this.PKVal,e=p.split(",");let r="";const s=new S;for(let l=0;l<e.length;l++){const a=e[l];if(s.MyPK=a,yield s.Retrieve(),s.MyPK=m+"_"+s.Key,(yield s.IsExits())==!0){r+=`@\u5B57\u6BB5: ${a} - ${s.Name} \u5DF2\u7ECF\u5B58\u5728.`;continue}s.FK_MapData=m,yield s.Insert(),r+=`@\u5B57\u6BB5: ${a} - ${s.Name} \u6210\u529F\u5BFC\u5165...`}return new I(f.Message,r)}})}}export{X as GPN_ImpDtlAttrs};
