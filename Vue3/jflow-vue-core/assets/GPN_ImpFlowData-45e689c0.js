var r=Object.defineProperty;var a=(t,E,u)=>E in t?r(t,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[E]=u;var C=(t,E,u)=>(a(t,typeof E!="symbol"?E+"":E,u),u);var i=(t,E,u)=>new Promise((A,D)=>{var n=F=>{try{e(u.next(F))}catch(B){D(B)}},o=F=>{try{e(u.throw(F))}catch(B){D(B)}},e=F=>F.done?A(F.value):Promise.resolve(F.value).then(n,o);e((u=u.apply(t,E)).next())});import{$ as l}from"./index-f4658ae7.js";import{PageBaseGroupNew as s,GPNReturnObj as p,GPNReturnType as c}from"./PageBaseGroupNew-ee20c033.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class P extends s{constructor(){super("GPN_ImpFlowData");C(this,"DingDing",`
  #### \u5E2E\u52A9
  - \u9009\u62E9\u4ECE\u9489\u9489\u7CFB\u7EDF\u4E2D\u5BFC\u51FA\u7684\u6D41\u7A0B\u5B9E\u4F8BExcel\u683C\u5F0F\u7684\u6D41\u7A0B\u6570\u636E.
  - \u4E0A\u4F20\u6267\u884C\u5BFC\u5165.
  #### \u8BF4\u660E
  - \u7CFB\u7EDF\u8BFB\u53D6excel\u4EE5\u540E\uFF0C\u6839\u636E\u5F53\u524D\u7684\u6D41\u7A0B\u8282\u70B9\u8BBE\u7F6E\uFF0C\u5BFC\u5165\u6570\u636E\u3002
  - \u80FD\u591F\u89E3\u6790\u51FA\u6765\u5BA1\u6279\u610F\u89C1\uFF0C\u5BA1\u6838\u8DEF\u5F84\uFF0C\u6D41\u7A0B\u8F68\u8FF9\u56FE\u3002
  - \u80FD\u591F\u8BBE\u7F6E\u5F53\u521D\u7684\u6267\u884C\u65F6\u95F4\u3002
  - \u9002\u7528\u4E8E\u9489\u9489\u7CFB\u7EDF\u8F6Cccbpm\u7CFB\u7EDF\uFF0C\u6D41\u7A0B\u5B9E\u4F8B\u6570\u636E\u7684\u6EAF\u6E90\u3002
  - ![\u5BFC\u5165\u9489\u9489\u683C\u5F0F\u6A21\u677F](/resource/WF/Admin/AttrFlow/ImpFlowData_DingDing.png "\u5BFC\u5165\u9489\u9489\u683C\u5F0F\u6A21\u677F")  

  `);C(this,"WeiXin",`
  #### \u5E2E\u52A9
  - \u9009\u62E9\u4ECE\u5FAE\u4FE1\u7CFB\u7EDF\u4E2D\u5BFC\u51FA\u7684\u6D41\u7A0B\u5B9E\u4F8BExcel\u683C\u5F0F\u7684\u6D41\u7A0B\u6570\u636E.
  - \u4E0A\u4F20\u6267\u884C\u5BFC\u5165.
  #### \u8BF4\u660E
  - \u7CFB\u7EDF\u8BFB\u53D6excel\u4EE5\u540E\uFF0C\u6839\u636E\u5F53\u524D\u7684\u6D41\u7A0B\u8282\u70B9\u8BBE\u7F6E\uFF0C\u5BFC\u5165\u6570\u636E\u3002
  - \u80FD\u591F\u89E3\u6790\u51FA\u6765\u5BA1\u6279\u610F\u89C1\uFF0C\u5BA1\u6838\u8DEF\u5F84\uFF0C\u6D41\u7A0B\u8F68\u8FF9\u56FE\u3002
  - \u80FD\u591F\u8BBE\u7F6E\u5F53\u521D\u7684\u6267\u884C\u65F6\u95F4\u3002
  - \u9002\u7528\u4E8E\u5FAE\u4FE1\u7CFB\u7EDF\u8F6Cccbpm\u7CFB\u7EDF\uFF0C\u6D41\u7A0B\u5B9E\u4F8B\u6570\u636E\u7684\u6EAF\u6E90\u3002
  - ![\u5BFC\u5165\u5FAE\u4FE1\u683C\u5F0F\u6A21\u677F](/resource/WF/Admin/AttrFlow/ImpFlowData_WeiXin.png "\u5BFC\u5165\u5FAE\u4FE1\u683C\u5F0F\u6A21\u677F")  

  `);this.PageTitle="\u6D41\u7A0B\u6570\u636E\u5BFC\u5165"}Init(){return i(this,null,function*(){this.AddGroup("A","\u7CFB\u7EDF\u652F\u6301"),this.FileUpload("DingDing","\u9489\u9489\u6570\u636E\u5BFC\u5165","\u8BF7\u4E0A\u4F20\u7B26\u5408\u683C\u5F0F\u7684Excel\u6587\u4EF6.",this.DingDing),this.FileUpload("WeiXin","\u5FAE\u4FE1\u6570\u636E\u5BFC\u5165","\u8BF7\u4E0A\u4F20\u7B26\u5408\u683C\u5F0F\u7684Excel\u6587\u4EF6.",this.WeiXin)})}GenerSorts(){return i(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,A,D,n,o){return i(this,null,function*(){const e=this.PKVal;if(u=="DingDing"){const F=new l("BP.WF.HttpHandler.WF_Admin_AttrFlow");F.AddFile(this.UploadFile),F.AddPara("FlowNo",e);let B=yield F.DoMethodReturnString("DingDing_ImpDataFile");return typeof B=="string"&&B.includes("@")&&(B=B.split("@").join(`
`)),new p(c.Message,B)}})}}export{P as GPN_ImpFlowData};
