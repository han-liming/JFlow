var F=Object.defineProperty;var l=(r,B,t)=>B in r?F(r,B,{enumerable:!0,configurable:!0,writable:!0,value:t}):r[B]=t;var s=(r,B,t)=>(l(r,typeof B!="symbol"?B+"":B,t),t);var m=(r,B,t)=>new Promise((C,o)=>{var E=u=>{try{i(t.next(u))}catch(e){o(e)}},D=u=>{try{i(t.throw(u))}catch(e){o(e)}},i=u=>u.done?C(u.value):Promise.resolve(u.value).then(E,D);i((t=t.apply(r,B)).next())});import{G as A}from"./DataType-33901a1c.js";import{CCRole as S}from"./CCRole-ba08e115.js";import{PageBaseGroupNew as y,GPNReturnObj as a,GPNReturnType as p}from"./PageBaseGroupNew-ee20c033.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./DBAccess-d3bef90d.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class g extends y{constructor(){super("GPN_CCRole");s(this,"ByField",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u6284\u9001\u4EBA.
  - \u901A\u5E38\u662F\u5728\u8282\u70B9\u8868\u5355\u4E0A\u52A0\u4E00\u4E2A\u5B57\u6BB5,\u8FD9\u4E2A\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4EBA\u5458\u8D26\u53F7\uFF0C\u591A\u4E2A\u4EBA\u5458\u4F7F\u7528\u9017\u53F7\u5206\u5F00.
  #### \u5B9E\u4F8B
  - @liang
`);s(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u81EA\u52A8\u6284\u9001\u7ED9\u8981\u7ED1\u5B9A\u7684\u4EBA\u5458.
`);s(this,"Desc5",`
  #### \u5E2E\u52A9
  - \u7ED1\u5B9A\u8282\u70B9\u7684\u63A5\u6536\u4EBA\u89C4\u5219.
  - \u8BF7\u70B9\u51FB\u8BBE\u7F6E\u63A5\u53D7\u4EBA\u89C4\u5219.
`);s(this,"BySQL",`
  #### \u5E2E\u52A9
  - \u6309SQL\u8BA1\u7B97\u6284\u9001\u4EBA\u5458.
  - \u6709\u4E00\u4E2A\u89C4\u5219
  #### DEMO
  - \u6284\u9001\u672C\u90E8\u95E8\u7684\u4EBA\u5458.
  - SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo';
  
`);this.ForEntityClassID="TS.AttrNode.CCRole",this.PageTitle="\u65B0\u5EFA\u6284\u9001\u89C4\u5219"}Init(){this.AddGroup("A","\u8BF7\u9009\u62E9\u89C4\u5219"),this.SelectItemsByTreeEns("1","\u6309\u4EBA\u5458\u8BA1\u7B97",this.Docs1,!0,A.srcDeptLazily,"0",A.srcEmpLazily,"@No=\u8D26\u53F7@Name=\u540D\u79F0@Tel=\u7535\u8BDD"),this.SelectItemsByGroupList("2","\u6309\u89D2\u8272\u8BA1\u7B97",this.Docs1,!0,A.srcStationTypes,A.srcStations),this.SelectItemsByTree("3","\u6309\u90E8\u95E8\u8BA1\u7B97",this.Docs1,!0,A.srcDepts,"0"),this.TextSQL("4","\u6309SQL\u8BA1\u7B97",this.BySQL,"\u67E5\u8BE2SQL","SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo' ","\u8F93\u5165\u7684SQL\u8FD4\u56DE\u4EBA\u5458\u96C6\u5408\u5177\u6709No,Name\u4E24\u4E2A\u5217."),this.TextBox1_Name("0","\u6309\u8868\u5355\u5B57\u6BB5\u8BA1\u7B97",this.ByField,"\u8868\u5355\u5B57\u6BB5","","\u8BF7\u8F93\u5165\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u540D."),this.AddBlank("5","\u6309\u63A5\u53D7\u4EBA\u89C4\u5219\u8BA1\u7B97",this.Desc5)}GenerSorts(){return m(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(t,C,o,E,D){return m(this,null,function*(){const i=this.RefPKVal,u=new S;u.NodeID=i,u.CCRoleExcType=t,u.EnIDs=o,u.Tag2=E,u.EnIDsT=E;let e="";if(t==="0"&&(e="TS.AttrNode.CCRoleByField"),t==="1"&&(e="TS.AttrNode.CCRoleByEmp"),t==="2"&&(e="TS.AttrNode.CCRoleByStation"),t==="3"&&(e="TS.AttrNode.CCRoleByDept"),t==="4"&&(e="TS.AttrNode.CCRoleBySQL",u.DBSrc=o,u.Tag1=E),t==="5"){if(e="TS.AttrNode.CCRoleByDeliveryWay",u.MyPK=this.RefPKVal,u.Tag1="\u8BBE\u7F6E\u63A5\u53D7\u4EBA\u89C4\u5219.",(yield u.IsExits())==!0)return new a(p.Message,"err@\u8BE5\u89C4\u5219\u5DF2\u7ECF\u5B58\u5728,\u53EA\u5141\u8BB8\u6709\u4E00\u4E2A\u89C4\u5219.");u.SetPara("EnName",e),yield u.Insert();return}u.SetPara("EnName",e),yield u.Insert();const n="/@/WF/Comm/En.vue?EnName="+e+"&PKVal="+u.MyPK;return new a(p.GoToUrl,n)})}}export{g as GPN_CCRole};
