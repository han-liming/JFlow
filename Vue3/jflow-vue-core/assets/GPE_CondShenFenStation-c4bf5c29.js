var s=Object.defineProperty;var m=(F,E,u)=>E in F?s(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var r=(F,E,u)=>(m(F,typeof E!="symbol"?E+"":E,u),u);var p=(F,E,u)=>new Promise((B,t)=>{var o=e=>{try{i(u.next(e))}catch(n){t(n)}},D=e=>{try{i(u.throw(e))}catch(n){t(n)}},i=e=>e.done?B(e.value):Promise.resolve(e.value).then(o,D);i((u=u.apply(F,E)).next())});import{G as l}from"./DataType-33901a1c.js";import{Cond as C,CondAttr as A}from"./Cond-7bb97535.js";import{PageBaseGroupEdit as d}from"./PageBaseGroupEdit-202e8e85.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./EntityNoName-d08126ae.js";import"./DBAccess-d3bef90d.js";import"./Node-6b42ba5e.js";import"./EntityNodeID-d5ae71b1.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class $ extends d{constructor(){super("GPE_CondShenFenStation");r(this,"Desc0",`
  #### \u8BF4\u660E
   - \u9ED8\u8BA4\u4E3A\u8BE5\u6A21\u5F0F\u3002
   - \u63D0\u4EA4\u4EBA\u5458\u767B\u5F55\u90E8\u95E8\u5C31\u662F\uFF0C\u6761\u4EF6\u7684\u5224\u65AD\u53C2\u6570.
    `);r(this,"Desc1",`
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD\u3002
   - \u5982\u4E0B\u56FE\uFF0C\u8BBE\u5907\u7EF4\u4FEE\u7533\u8BF7\u4EBA\uFF0C\u4E3A\u516C\u53F8\u4E0D\u540C\u90E8\u95E8\uFF0C\u5F53\u8BBE\u5907\u90E8\u4EBA\u5458\u68C0\u67E5\u540E\uFF0C\u8BA4\u5B9A\u8BE5\u8BBE\u5907\u53EF\u4EE5\u7EF4\u62A4\u5E76\u7ED9\u51FA\u62A5\u4EF7\uFF0C\u8FD9\u65F6\u5BA1\u6279\u6743\u5C31\u4EA4\u8FD8\u7ED9\u7533\u8BF7\u90E8\u95E8\u7684\u9886\u5BFC\u3002
   - \u90A3\u4E48\u5728\u8F6C\u5411\u6761\u4EF6\u5C31\u8BBE\u7F6E\u4E3A\u6309\u89D2\u8272\u9009\u62E9\uFF0C\u4E5F\u5C31\u662F\u628A\u5404\u90E8\u95E8\u9886\u5BFC\u89D2\u8272\u7684\u4EBA\u9009\u62E9\u51FA\u6765\uFF0C\u518D\u786E\u8BA4\u7533\u8BF7\u4EBA\u7684\u90E8\u95E8\uFF0C\u8FD9\u6837\uFF0C\u5177\u6709\u5BA1\u6279\u6743\u7684\u63A5\u6536\u4EBA\u5C31\u662F\u7533\u8BF7\u4EBA\u7684\u90E8\u95E8\u9886\u5BFC\u3002
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModel.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  - \u9009\u62E9\u65B0\u589E\u65B9\u5411\u6761\u4EF6
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModelSetting.png "\u5C4F\u5E55\u622A\u56FE")
  - \u9009\u62E9\u4EBA\u5458\u8EAB\u4EFD
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModelSetting2.png "\u5C4F\u5E55\u622A\u56FE")
  
  `);r(this,"Desc2",`
  #### \u8BF4\u660E
  - \u9009\u62E9\u7684\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD(\u8BE5\u5B57\u6BB5\u91CC\u5B58\u50A8\u7684\u662F\u8D26\u53F7)
  - \u6307\u5B9A\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD\u3002
  - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u8868\u5355\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingBiaodan2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi2.png "\u5C4F\u5E55\u622A\u56FE")
      `);this.PageTitle="\u5C97\u4F4D\u6761\u4EF6\u4EBA\u5458\u8EAB\u4EFD"}Init(){return p(this,null,function*(){this.entity=new C,this.KeyOfEn=A.SpecOperWay;const u=new C(this.PKVal);yield u.Retrieve(),this.AddGroup("A","\u6309\u53D1\u9001\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97"),this.Blank("0","\u53D1\u9001\u4EBA\u767B\u5F55\u90E8\u95E8\u4E0B\u6240\u6709\u5C97\u4F4D",this.HelpUn),this.Blank("1","\u53D1\u9001\u4EBA\u7684\u6240\u6709\u90E8\u95E8\u7684\u5C97\u4F4D",this.HelpUn),this.Blank("2","\u53D1\u9001\u4EBA\u9009\u62E9\u90E8\u95E8+\u5C97\u4F4D",this.HelpUn),this.AddGroup("B","\u6309\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97");const B=`SELECT NodeID No,Name FROM WF_Node WHERE FK_Flow='${u.FK_Flow}'`;this.SingleDDLSQL("10","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4F7F\u7528\u90E8\u95E8\u4E0B\u6240\u6709\u5C97\u4F4D",A.SpecOperPara,this.HelpUn,B,!1),this.SingleDDLSQL("11","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u6240\u6709\u90E8\u95E8\u7684\u5C97\u4F4D",A.SpecOperPara,this.HelpUn,B,!1);const t="ND"+parseInt(u.FK_Flow)+"Rpt",o=l.SQLOfMapAttrsGener(t);this.AddGroup("C","\u6309\u8868\u5355\u5B57\u6BB5\u4EBA\u5458\u8BA1\u7B97"),this.SingleDDLSQL("20","\u5B57\u6BB5(\u53C2\u6570)\u4EBA\u5458\u7684\u4E3B\u90E8\u95E8\u4E0B\u6240\u6709\u7684\u5C97\u4F4D",A.SpecOperPara,this.HelpUn,o,!1),this.SingleDDLSQL("21","\u5B57\u6BB5(\u53C2\u6570)\u4EBA\u5458\u7684\u6240\u6709\u90E8\u95E8\u7684\u5C97\u4F4D",A.SpecOperPara,this.HelpUn,o,!1),this.SingleDDLSQL("22","\u5B57\u6BB5(\u53C2\u6570)\u5C31\u662F\u5C97\u4F4D\u7F16\u53F7",A.SpecOperPara,this.HelpUn,o,!1)})}AfterSave(u,B){if(u==B)throw new Error("Method not implemented.")}BtnClick(u,B,t){if(u==B||u===t)throw new Error("Method not implemented.")}}export{$ as GPE_CondShenFenStation};
