var x=Object.defineProperty;var S=(o,i,u)=>i in o?x(o,i,{enumerable:!0,configurable:!0,writable:!0,value:u}):o[i]=u;var r=(o,i,u)=>(S(o,typeof i!="symbol"?i+"":i,u),u);var h=(o,i,u)=>new Promise((n,B)=>{var m=t=>{try{E(u.next(t))}catch(F){B(F)}},I=t=>{try{E(u.throw(t))}catch(F){B(F)}},E=t=>t.done?n(t.value):Promise.resolve(t.value).then(m,I);E((u=u.apply(o,i)).next())});import{GloComm as l}from"./GloComm-7cfbdfd9.js";import{MapAttr as d}from"./MapAttr-cb594d82.js";import{GroupFields as T}from"./GroupField-d6637832.js";import{PageBaseGroupNew as w,GPNReturnObj as s,GPNReturnType as c}from"./PageBaseGroupNew-ee20c033.js";import{UIContralType as A}from"./EnumLab-3cbd0812.js";import{b as k}from"./MapExt-db8cd7f3.js";import"./FrmTrack-10f0746d.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Events-141c34ea.js";import"./BSEntity-840a884b.js";import"./EntityOID-553df0d1.js";import"./MapData-4fa397be.js";import"./EntityNoName-d08126ae.js";import"./EnumLab-4f91f91c.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./SFTable-d63f9fb4.js";import"./SFDBSrc-e641ea16.js";class su extends w{constructor(){super("GPN_ComponentField");r(this,"ScoreDesc",`
  #### \u5E2E\u52A9
  - \u8BC4\u5206\u63A7\u4EF6: \u5BF9\u5F53\u524D\u8BB0\u5F55\u7684\u6570\u636E\u8FDB\u884C\u6253\u5206\u7684\u63A7\u4EF6.
  - \u4F7F\u7528\u5C5E\u6027\u53EF\u4EE5\u63A7\u5236\u6253\u5206\u7684\u957F\u5EA6\uFF0C\u6BD4\u5982\uFF1A5\u5206\uFF0C10\u5206.
  - \u8BBE\u7F6E\u56FE\u7247:
  - \u8FD0\u884C\u56FE\u7247\uFF1A
  #### \u6570\u636E\u5B58\u50A8.
  - \u8BBE\u7F6E\u4FE1\u606F\u5B58\u50A8\u5728\uFF1ASys_MapAttr\u8868\u91CC.
  - \u6570\u636E\u5B58\u50A8\u5728,\u63A7\u4EF6ID\u5BF9\u5E94\u7684\u5B57\u6BB5\u91CC.
    `);r(this,"FrmBtnDesc",`
  #### \u5E2E\u52A9
  - \u6309\u94AE\u63A7\u4EF6: \u53EF\u4EE5\u70B9\u51FB\u540E\u6267\u884CJs\u811A\u672C\u7684\u63A7\u4EF6, \u5229\u7528onclick\u6765\u627F\u8F7D\u7F16\u5199\u7684js.
  - \u5E94\u7528\u573A\u666F: \u4F7F\u7528\u5C5E\u6027\u53EF\u4EE5\u63A7\u5236\u6253\u5206\u7684\u957F\u5EA6\uFF0C\u6BD4\u5982\uFF1A5\u5206\uFF0C10\u5206.
  - \u5982\u4E0B\u56FE:
  - 
  #### \u6570\u636E\u5B58\u50A8.
  - \u8BBE\u7F6E\u4FE1\u606F\u5B58\u50A8\u5728\uFF1ASys_MapAttr\u8868\u91CC.
    `);r(this,"FrmLinkDesc",`
  #### \u5E2E\u52A9
  - \u6309\u94AE\u63A7\u4EF6: \u53EF\u4EE5\u70B9\u51FB\u540E\u6267\u884CJs\u811A\u672C\u7684\u63A7\u4EF6, \u5229\u7528onclick\u6765\u627F\u8F7D\u7F16\u5199\u7684js.
  - \u5982\u4E0B\u56FE:
  - 
  #### \u6570\u636E\u5B58\u50A8.
  - \u8BBE\u7F6E\u4FE1\u606F\u5B58\u50A8\u5728\uFF1ASys_MapAttr\u8868\u91CC.
  - \u6570\u636E\u4FE1\u606F\u5B58\u50A8\uFF1A\u65E0
    `);r(this,"LocationDesc",`
    #### \u5E2E\u52A9
    - \u6309\u94AE\u63A7\u4EF6: \u5B9A\u4F4D\u4FE1\u606F
    - \u5982\u4E0B\u56FE:
    - 
    #### \u6570\u636E\u5B58\u50A8.
    - \u8BBE\u7F6E\u4FE1\u606F\u5B58\u50A8\u5728\uFF1ASys_MapAttr\u8868\u91CC.
    - \u6570\u636E\u4FE1\u606F\u5B58\u50A8\uFF1A\u63A7\u4EF6\u5BF9\u5E94\u7684\u5B57\u6BB5.
      `);r(this,"FieldAth",`
  #### \u5E2E\u52A9
  - \u5B57\u6BB5\u9644\u4EF6; 
  #### \u6570\u636E\u5B58\u50A8.
    `);r(this,"NewIntEnum",`
  #### \u5E2E\u52A9
  - \u586B\u5199\u683C\u5F0F1: \u56E2\u5458,\u515A\u5458,\u7FA4\u4F17
  - \u7CFB\u7EDF\u89E3\u6790\u4E3A: 0\u662F\u56E2\u5458\uFF0C 1\u662F\u515A\u5458\uFF0C2\u662F\u7FA4\u4F17.
  - \u586B\u5199\u683C\u5F0F2: @0=\u56E2\u5458@1=\u515A\u5458@2=\u7FA4\u4F17
  - \u7CFB\u7EDF\u89E3\u6790\u4E3A: 10\u662F\u56E2\u5458\uFF0C 20\u662F\u515A\u5458\uFF0C30\u662F\u7FA4\u4F17\uFF0C\u8FD9\u6837\u5C31\u53EF\u4EE5\u81EA\u5DF1\u5B9A\u4E49\u679A\u4E3E\u503C.
  #### \u6570\u636E\u5B58\u50A8
  - int\u7C7B\u578B\u7684\u679A\u4E3E\u503C\u662F\u5E38\u7528\u7684\u6570\u636E\u7C7B\u578B\uFF0Cccfrom\u662F\u683C\u5F0F\u5316\u7684\u5B58\u50A8\u5230\u6570\u636E\u8868\u91CC.
  - \u521B\u5EFA\u4E00\u4E2Aint\u7C7B\u578B\u7684\u5B57\u6BB5\uFF0C\u7528\u4E8E\u5B58\u50A8\u679A\u4E3E\u7684\u6570\u636E.
    `);r(this,"Docs1",`
  #### \u5E2E\u52A9 
   - \u8BE5\u8868\u5355\u662F\u56FA\u5B9A\u683C\u5F0F\u7684\u8868\u5355,\u53EF\u4EE5\u5C55\u73B04\u52176\u5217\u5C55\u73B0.
   - \u4F18\u70B9:\u5F00\u53D1\u6548\u7387\u9AD8,\u5C55\u73B0\u7B80\u6D01,\u5B66\u4E60\u6210\u672C\u4F4E,\u4E1A\u52A1\u4EBA\u5458\u53EF\u4EE5\u5165\u624B.
   - \u7F3A\u70B9:\u5C55\u793A\u6837\u5F0F\u56FA\u5B9A.
  `);r(this,"Docs2",`

  #### \u5E2E\u52A9
   - \u4F9D\u6258\u5BCC\u6587\u672C\u7F16\u8F91\u5668,\u5B9E\u73B0\u5BF9\u8868\u5355\u7684\u7F16\u8F91.
   - \u4F18\u70B9:\u683C\u5F0F\u7075\u6D3B,\u5C55\u73B0\u6548\u679C\u968F\u5FC3\u6240\u6B32.
   - \u7F3A\u70B9:\u4E1A\u52A1\u4EBA\u5458\u5165\u624B\u9700\u8981\u4E00\u5B9A\u7684\u5B66\u4E60\u6210\u672C.
   - \u9002\u7528\u4E8E:\u6548\u679C
  `);this.PageTitle="\u65B0\u5EFA\u5B57\u6BB5\u81EA\u5B9A\u4E49\u7EC4\u4EF6"}Init(){this.AddGroup("A","\u901A\u7528\u7EC4\u4EF6"),this.TextBox2_NameNo("ExtScore","\u8BC4\u5206",this.ScoreDesc,"Score","\u5B57\u6BB5ID","\u540D\u79F0","\u8BC4\u5206"),this.AddIcon("icon-like","ExtScore"),this.TextBox2_NameNo("FrmBtn","\u6309\u94AE",this.FrmBtnDesc,"FrmBtn","\u7EC4\u4EF6ID","\u7EC4\u4EF6\u540D\u79F0","\u6309\u94AE1"),this.AddIcon("icon-drop","FrmBtn"),this.TextBox2_NameNo("FrmLink","\u8D85\u94FE\u63A5",this.FrmLinkDesc,"FrmLink","\u5B57\u6BB5ID","\u8FDE\u63A5\u6807\u7B7E","\u6211\u7684\u8FDE\u63A5"),this.AddIcon("icon-link","FrmLink"),this.TextBox2_NameNo("Location","\u5B9A\u4F4D",this.LocationDesc,"Location","\u5B57\u6BB5ID","\u540D\u79F0","\u5B9A\u4F4D\u7EC4\u4EF6"),this.AddIcon("icon-location-pin","Location"),this.TextBox1_Name("FrmHtml","\u5927\u5757\u8BF4\u660E",this.HelpUn,"FrmHtml","\u5927\u5757\u8BF4\u660E"),this.AddIcon("icon-doc","FrmHtml"),this.AddGroup("B","\u6D41\u7A0B\u7EC4\u4EF6"),this.TextBox2_NameNo("FlowRefLink","\u5173\u8054\u6D41\u7A0B",this.HelpUn,"FlowRefLink","\u5B57\u6BB5ID","\u540D\u79F0","\u5173\u8054\u6D41\u7A0B"),this.AddIcon("icon-share","FlowRefLink"),this.TextBox2_NameNo("BillRefLink","\u5173\u8054\u5355\u636E",this.HelpUn,"BillRefLink","\u5B57\u6BB5ID","\u540D\u79F0","\u5173\u8054\u5355\u636E"),this.AddIcon("icon-share","BillRefLink"),this.AddBlank("GovAth","\u516C\u6587\u6B63\u6587",this.HelpUn),this.AddIcon("icon-doc","GovAth"),this.AddBlank("WordNum","\u516C\u6587\u5B57\u53F7",this.HelpUn),this.AddIcon("icon-star","WordNum"),this.AddBlank("FlowBBS","\u6D41\u7A0B\u8BC4\u8BBA",this.HelpUn),this.AddIcon("icon-bubble","FlowBBS"),this.TextBox2_NameNo("SignCheck","\u7B7E\u6279\u7EC4\u4EF6",this.HelpUn,"SC","\u5B57\u6BB5ID","\u540D\u79F0","\u7B7E\u6279\u7EC4\u4EF6"),this.AddIcon("icon-check","SignCheck"),this.AddGroup("C","\u5B9E\u9A8C\u4E2D"),this.TextBox3_NameNoNote("img","\u56FE\u7247",this.HelpUn,"Img","\u7EC4\u4EF6ID","\u56FE\u7247\u540D\u79F0","\u56FE\u7247URL","\u901A\u7528\u56FE\u7247"),this.AddIcon("icon-picture","img"),this.TextBox2_NameNo("Progress","\u6D41\u7A0B\u8FDB\u5EA6\u56FE",this.HelpUn,"Progress","\u5B57\u6BB5ID","\u540D\u79F0","\u6D41\u7A0B\u8FDB\u5EA6\u56FE"),this.AddIcon("icon-check","Progress"),this.TextBox2_NameNo("id_card_upload","\u8EAB\u4EFD\u8BC1",this.HelpUn,"Card","\u7EC4\u4EF6ID","\u7EC4\u4EF6\u540D\u79F0","\u8EAB\u4EFD\u8BC11"),this.AddIcon("icon-user","id_card_upload"),this.TextBox2_NameNo("iframe","\u6846\u67B6",this.FieldAth,"Iframe","\u5B57\u6BB5ID","\u540D\u79F0","\u6211\u7684\u6846\u67B6"),this.AddIcon("icon-loop","iframe")}GenerSorts(){return h(this,null,function*(){const u=this.RequestVal("FrmID"),n=new T;return yield n.Retrieve("FrmID",u,"Idx"),n.filter(B=>B.CtrlType===""||B.CtrlType==="Attr").map(B=>({No:B.PKVal,Name:B.Lab}))})}Save_TextBox_X(u,n,B,m,I){return h(this,null,function*(){if(u==="ExtScore"||u=="FlowRefLink"||u=="BillRefLink"||u=="FrmBtn"||u=="FrmLink"||u=="SignCheck"){const E=this.RequestVal("FrmID"),t=new d;if(t.GroupID=n,t.FK_MapData=E,t.MyPK=E+"_"+m,yield t.IsExits())return new s(c.Message,"\u5B57\u6BB5ID=["+m+"]\u5DF2\u7ECF\u5B58\u5728");t.KeyOfEn=m,t.Name=B;let F=0;u=="ExtScore"&&(F=101),u=="FlowRefLink"&&(F=200),u=="BillRefLink"&&(F=201),u=="SignCheck"&&(F=A.SignCheck),u=="FrmBtn"&&(F=A.Btn),u=="FrmLink"&&(F=A.HyperLink);const C="TS.FrmUI.SelfCommonent."+u;t.UIContralType=F,t.SetPara("CtrlType",u),t.SetPara("EnName",C),yield t.Insert();const a=l.UrlEn(C,t.MyPK);return new s(c.GoToUrl,a)}if(u==="FlowBBS"||u=="FrmHtml"||u=="Location"||u=="WordNum"||u=="GovAth"){const E=this.RequestVal("FrmID"),t=new d;if(t.GroupID=n,t.Name=this.GetPageName(u),t.FK_MapData=E,t.MyPK=E+"_"+u,yield t.IsExits())return new s(c.Message,"\u5B57\u6BB5ID=["+u+"]\u5DF2\u7ECF\u5B58\u5728");t.KeyOfEn=u,t.Name=B;let F=0;if(u=="FlowBBS"&&(F=A.FlowBBS),u=="FrmHtml"&&(F=A.FrmHtml),u=="Location"&&(F=A.Location),u=="WordNum"&&(F=A.WordNum),u=="GovAth"&&(F=A.GovDocFile,t.Name="\u516C\u6587\u6B63\u6587"),u=="FrmHtml"){const D="HtmlText_"+t.MyPK,e=new k("BP.Sys.MapExt");e.setPKVal(D),(yield e.RetrieveFromDBSources())==0&&(e.MyPK=D,e.FK_MapData=E,e.ExtType="HtmlText",e.ExtModel="HtmlText",e.AttrOfOper=u,yield e.Insert());const p="TS.FrmUI.SelfCommonent."+u;t.UIContralType=F,t.SetPara("CtrlType",u),t.SetPara("EnName",p),yield t.Insert();const f=l.UrlEn(p,D);return new s(c.GoToUrl,f)}const C="TS.FrmUI.SelfCommonent."+u;t.UIContralType=F,t.SetPara("CtrlType",u),t.SetPara("EnName",C),yield t.Insert();const a=l.UrlEn(C,t.MyPK);return new s(c.GoToUrl,a)}if(u==="AthField"){const E=this.RequestVal("FrmID"),t=new d;if(t.GroupID=n,t.FK_MapData=E,t.MyPK=E+"_"+m,yield t.IsExits())return new s(c.Message,"\u9644\u4EF6ID\u5DF2\u7ECF\u5B58\u5728");t.KeyOfEn=m,t.Name=B,yield t.Insert();const F=l.UrlEn("TS.FrmUI.MapAttrEnum",t.MyPK);return new s(c.GoToUrl,F)}})}}export{su as GPN_ComponentField};
