var p=Object.defineProperty;var a=(e,F,t)=>F in e?p(e,F,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[F]=t;var r=(e,F,t)=>(a(e,typeof F!="symbol"?F+"":F,t),t);var A=(e,F,t)=>new Promise((m,i)=>{var D=u=>{try{o(t.next(u))}catch(n){i(n)}},C=u=>{try{o(t.throw(u))}catch(n){i(n)}},o=u=>u.done?m(u.value):Promise.resolve(u.value).then(D,C);o((t=t.apply(e,F)).next())});import{Y as B}from"./index-f4658ae7.js";import{PCenter as c}from"./PCenter-7f795966.js";import{PageBaseGroupNew as P,GPNReturnObj as l,GPNReturnType as S}from"./PageBaseGroupNew-ee20c033.js";import G from"./DBAccess-d3bef90d.js";import{G as s}from"./DataType-33901a1c.js";import{AuthType as E}from"./AuthType-64e5d103.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./PowerCenter-f8ebe7c0.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class X extends P{constructor(){super("GPN_PCenter");r(this,"Docs0",`
  #### \u5E2E\u52A9
  - \u6240\u6709\u4EBA\u90FD\u53EF\u4EE5\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Anyone.png "\u5C4F\u5E55\u622A\u56FE.png") 

`);r(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u53EA\u6709\u7BA1\u7406\u5458\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Admin.png "\u5C4F\u5E55\u622A\u56FE.png") 

  `);r(this,"Docs2",`
  #### \u5E2E\u52A9
  - \u7BA1\u7406\u5458\u548C\u4E8C\u7EA7\u7BA1\u7406\u5458\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/AdminerAndAmin2.png "\u5C4F\u5E55\u622A\u56FE.png") 

  
  `);r(this,"Docs3",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Emp.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs4",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u89D2\u8272\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Stations.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs5",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u90E8\u95E8\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Dept.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs6",`
  #### \u5E2E\u52A9
  - \u81EA\u52A8\u6284\u9001\u7ED9\u8981\u7ED1\u5B9A\u7684\u4EBA\u5458.1. \u8F93\u5165\u7684SQL\u662F\u4E00\u4E2A\u67E5\u8BE2\u8BED\u53E5\uFF0C\u8FD4\u56DE\u7684\u4E00\u884C\u7684\u7B2C\u4E00\u5217\u6570\u636E\u3002
  - \u8BE5\u6570\u636E\u5927\u4E8E0 \uFF0C\u5C31\u662F\u771F(\u53EF\u4EE5\u62E5\u6709\u6B64\u6743\u9650)\uFF0C\u5426\u5219\u5C31\u662F\u5047\uFF08\u4E0D\u80FD\u64CD\u4F5C\u6B64\u6743\u9650\uFF09\u3002
  - SQL\u8BED\u53E5\u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F\uFF0C\u6BD4\u5982\uFF1ASELECT count(*) FROM Port_Dept WHERE No='@WebUser.DeptNo'\u3002
  #### \u8BF4\u660E
  - @WebUser.No \u5F53\u524D\u767B\u5F55\u7684\u4EBA\u5458\u7F16\u53F7
  - @WebUser.DeptNo \u5F53\u524D\u767B\u5F55\u7684\u90E8\u95E8\u7F16\u53F7
  - @RDT \u662F\u5F53\u524D\u65E5\u671F\uFF0C \u6BD4\u5982\uFF1A2020-01-01
  - @DateTime \u662F\u5F53\u524D\u65F6\u95F4\uFF0C \u6BD4\u5982\uFF1A2020-01-01 10:09
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Sql.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);this.ForEntityClassID="TS.GPM.PCenter",this.PageTitle="\u65B0\u5EFA\u6743\u9650"}Init(){this.AddGroup("A","\u65B0\u5EFA\u6743\u9650"),this.AddBlank(E.Anyone,"\u6240\u6709\u4EBA",this.Docs0),this.AddBlank(E.Adminer,"\u7BA1\u7406\u5458",this.Docs1),this.AddBlank(E.AdminerAndAmin2,"\u7BA1\u7406\u5458\u3001\u4E8C\u7EA7\u7BA1\u7406\u5458",this.Docs2),this.SelectItemsByTreeEns(E.Emps,"\u6309\u4EBA\u5458\u8BA1\u7B97",this.Docs3,!0,s.srcDeptLazily,"0",s.srcEmpLazily,"@No=\u8D26\u53F7@Name=\u540D\u79F0@Tel=\u7535\u8BDD"),this.SelectItemsByGroupList(E.Stations,"\u6309\u89D2\u8272\u8BA1\u7B97",this.Docs4,!0,s.srcStationTypes,s.srcStations),this.SelectItemsByTree(E.Depts,"\u6309\u90E8\u95E8\u8BA1\u7B97",this.Docs5,!0,s.srcDepts,s.srcDeptRoot),this.TextBox1_Name(E.SQL,"\u6309SQL\u8BA1\u7B97",this.Docs6,"\u67E5\u8BE2SQL","SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo' ","\u8F93\u5165\u7684SQL\u8FD4\u56DE")}GenerSorts(){return A(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(t,m,i,D,C){return A(this,null,function*(){const o=this.RefPKVal,u=new c;if(u.CtrlObj=this.RequestVal("CtrlObj"),u.CtrlPKVal=o,u.CtrlModel=t,u.CtrlModelT=this.GetPageName(t),u.IDs=i,u.IDNames=D,u.MyPK=G.GenerGUID(),(t===E.Anyone||t===E.Adminer||t===E.AdminerAndAmin2)&&(u.IDs="\u65E0",u.IDNames="\u65E0",u.MyPK=u.CtrlPKVal+"_"+t,(yield u.IsExits())==!0)){B.info("\u5DF2\u7ECF\u5B58\u5728\u8FD9\u4E2A\u6A21\u5F0F");return}const n=new Map([[E.Emps,"TS.GPM.PCenterEmp"],[E.Depts,"TS.GPM.PCenterDept"],[E.Stations,"TS.GPM.PCenterStation"],[E.SQL,"TS.GPM.PCenterSQL"]]);return u.SetPara("EnName",n.get(t)||"None"),yield u.Insert(),B.info("\u521B\u5EFA\u6210\u529F"),new l(S.CloseAndReload)})}}export{X as GPN_PCenter};
