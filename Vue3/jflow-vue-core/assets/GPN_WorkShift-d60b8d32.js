var p=Object.defineProperty;var c=(E,t,u)=>t in E?p(E,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[t]=u;var s=(E,t,u)=>(c(E,typeof t!="symbol"?t+"":t,u),u);var A=(E,t,u)=>new Promise((i,e)=>{var D=o=>{try{r(u.next(o))}catch(B){e(B)}},m=o=>{try{r(u.throw(o))}catch(B){e(B)}},r=o=>o.done?i(o.value):Promise.resolve(o.value).then(D,m);r((u=u.apply(E,t)).next())});import"./index-f4658ae7.js";import{G as F}from"./DataType-33901a1c.js";import"./bignumber-cf158d26.js";import{PageBaseGroupNew as a}from"./PageBaseGroupNew-ee20c033.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./ParamsUtils-3cbc5822.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class T extends a{constructor(){super("GPN_WorkShift");s(this,"Docs0",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u6284\u9001\u4EBA.
  - \u901A\u5E38\u662F\u5728\u8282\u70B9\u8868\u5355\u4E0A\u52A0\u4E00\u4E2A\u5B57\u6BB5,\u8FD9\u4E2A\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4EBA\u5458\u8D26\u53F7\uFF0C\u591A\u4E2A\u4EBA\u5458\u4F7F\u7528\u9017\u53F7\u5206\u5F00.
  #### \u8FD0\u884C\u56FE\u4F8B
  - @liang.

`);s(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u81EA\u52A8\u6284\u9001\u7ED9\u8981\u7ED1\u5B9A\u7684\u4EBA\u5458.
`);s(this,"Docs2",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u7ED1\u5B9A\u7684\u90E8\u89D2\u8272\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u4F5C\u4E3A\u6284\u9001\u4EBA.
  - \u6709\u4E00\u4E2A\u89C4\u5219
  
`);this.PageTitle="\u5DE5\u4F5C\u79FB\u4EA4"}Init(){this.AddGroup("A","\u8BF7\u9009\u62E9\u89C4\u5219");const u="SELECT No,Name FROM WF_Flow";this.SelectItemsByList("Flows","\u9009\u62E9\u5F85\u529E\u6D41\u7A0B",this.Docs0,!0,u),this.SelectItemsByTreeEns("Flows.Emps","\u9009\u62E9\u88AB\u79FB\u4EA4\u4EBA",this.Docs1,!1,F.srcDeptLazily,F.srcDeptRoot,F.srcEmpLazily,"@No=\u8D26\u53F7@Name=\u540D\u79F0@Tel=\u7535\u8BDD")}GenerSorts(){return A(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,i,e,D,m){return A(this,null,function*(){})}}export{T as GPN_WorkShift};
