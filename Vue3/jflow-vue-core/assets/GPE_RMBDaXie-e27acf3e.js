var A=Object.defineProperty;var n=(E,F,u)=>F in E?A(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var o=(E,F,u)=>(n(E,typeof F!="symbol"?F+"":F,u),u);var p=(E,F,u)=>new Promise((t,r)=>{var m=i=>{try{B(u.next(i))}catch(e){r(e)}},D=i=>{try{B(u.throw(i))}catch(e){r(e)}},B=i=>i.done?t(i.value):Promise.resolve(i.value).then(m,D);B((u=u.apply(E,F)).next())});import{b as s,a}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as C}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class j extends C{constructor(){super("GPE_RMBDaXie");o(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u5B9A\u4E49\uFF1A\u5F53\u524D\u8F93\u5165\u91D1\u989D\u7C7B\u578B\u7684\u6570\u636E\uFF0C\u8981\u5B9E\u73B0\u5BF9\u5176\u4ED6\u53EA\u8BFB\u7684\u6587\u672C\u6846\u8FDB\u884C\u5927\u5199\u8F93\u51FA\u3002
   - \u6BD4\u5982: \u5728\u4E00\u4E2A\u91D1\u989D\u7C7B\u578B\u6216\u8005\u6570\u503C\u7C7B\u578B\u7684\u5B57\u6BB5\u8F93\u5165\u8F93\u5165\u65F6\u81EA\u52A8\uFF0C\u5728\u53E6\u5916\u4E00\u4E2A\u6587\u672C\u6846\u4E0A\u663E\u793A\u8BE5\u8F93\u5165\u503C\u7684\u4EBA\u6C11\u5E01\u5927\u5199.
   - \u7ECF\u5E38\u7528\u4E8E\u6253\u5370\uFF0C\u8981\u6C42\u8F93\u5165\u4EBA\u6C11\u5E01\u5927\u5199\u7684\u573A\u666F.
   #### \u6548\u679C\u56FE
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXieyanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 

   ### \u914D\u7F6E\u56FE
   - \u914D\u7F6E\u56FE\u4F8B1
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXie1.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u914D\u7F6E\u56FE\u4F8B2
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXie2.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u914D\u7F6E\u56FE\u4F8B3
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXie.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u8FD0\u884C\u56FE\u4F8B
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXieyanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
 
   `);o(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u4E0D\u542F\u7528\uFF1A\u4E0D\u5BF9\u5176\u4ED6\u6587\u672C\u6846\u5B9E\u73B0\u5927\u5199\u8F6C\u6362.
   - \u4EBA\u6C11\u5E01\u5927\u5199\uFF1A\u5F53\u524D\u8F93\u5165\u91D1\u989D\u7C7B\u578B\u7684\u6570\u636E\uFF0C\u8981\u5B9E\u73B0\u5BF9\u5176\u4ED6\u53EA\u8BFB\u7684\u6587\u672C\u6846\u8FDB\u884C\u5927\u5199\u8F93\u51FA\u3002
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/RMBDaXie/Img/RMBDaXieyanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);this.PageTitle="\u4EBA\u6C11\u5E01\u5927\u5199"}Init(){return p(this,null,function*(){this.entity=new s,this.KeyOfEn=a.DoWay,yield this.entity.InitDataForMapAttr("RMBDaXie",this.GetRequestVal("PKVal")),this.AddGroup("A","\u4EBA\u6C11\u5E01\u5927\u5199"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0);const u=`SELECT KeyOfEn as No, Name FROM Sys_MapAttr WHERE FK_MapData='@FK_MapData' AND MyDataType=1
    AND UIVisible=1`;this.SingleDDLSQL("1","\u9009\u62E9\u53EA\u8BFB\u7684String\u5B57\u6BB5",a.Tag,this.Desc0,u,!1)})}AfterSave(u,t){if(u==t)throw new Error("Method not implemented.")}BtnClick(u,t,r){if(u==t||u===r)throw new Error("Method not implemented.")}}export{j as GPE_RMBDaXie};
