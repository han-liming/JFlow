import{a as ee,b as oe,c as ne,N as se,d as re,u as te,e as ie,f as le,g as ae}from"./Modal-45bd3c03.js";import{k as ce}from"./Popover-ab55c8ff.js";import{d as I,r as y,a8 as d,p as x,F as P,q as S,k as de,f as w,P as ue,ep as fe}from"./index-f4658ae7.js";import{o as j}from"./omit-b0e7e098.js";import{c as L}from"./index-cad90cf4.js";import{m as A,a as ge}from"./use-message-f27d95bf.js";import{k as ve,c as C,e as k,g as O,f as b,b as E}from"./light-0dfdc1ad.js";import{a as he,b as me,N as pe}from"./Loading-fead3a83.js";import{f as be}from"./fade-in-height-expand.cssr-390ab856.js";import{u as M}from"./use-config-816d55a6.js";import{u as Ce}from"./use-rtl-889b67fe.js";import{c as p}from"./create-key-bf4384d6.js";import{u as xe}from"./use-css-vars-class-3ae3b4b3.js";import{r as ye}from"./render-ee8eb435.js";import{N as Ie}from"./Close-c51bd8a8.js";import{N as ke}from"./Icon-e3cbad7d.js";import{I as Oe,S as we,E as Pe}from"./Success-7a2433de.js";import{W as Se}from"./Warning-d0098cab.js";import{N as je}from"./FadeInExpandTransition-fc975915.js";const Le=Object.assign(Object.assign({},ee),{onAfterEnter:Function,onAfterLeave:Function,transformOrigin:String,blockScroll:{type:Boolean,default:!0},closeOnEsc:{type:Boolean,default:!0},onEsc:Function,autoFocus:{type:Boolean,default:!0},internalStyle:[String,Object],maskClosable:{type:Boolean,default:!0},onPositiveClick:Function,onNegativeClick:Function,onClose:Function,onMaskClick:Function}),Ae=I({name:"DialogEnvironment",props:Object.assign(Object.assign({},Le),{internalKey:{type:String,required:!0},to:[String,Object],onInternalAfterLeave:{type:Function,required:!0}}),setup(r){const s=y(!0);function i(){const{onInternalAfterLeave:n,internalKey:a,onAfterLeave:g}=r;n&&n(a),g&&g()}function t(n){const{onPositiveClick:a}=r;a?Promise.resolve(a(n)).then(g=>{g!==!1&&e()}):e()}function l(n){const{onNegativeClick:a}=r;a?Promise.resolve(a(n)).then(g=>{g!==!1&&e()}):e()}function u(){const{onClose:n}=r;n?Promise.resolve(n()).then(a=>{a!==!1&&e()}):e()}function f(n){const{onMaskClick:a,maskClosable:g}=r;a&&(a(n),g&&e())}function c(){const{onEsc:n}=r;n&&n()}function e(){s.value=!1}function o(n){s.value=n}return{show:s,hide:e,handleUpdateShow:o,handleAfterLeave:i,handleCloseClick:u,handleNegativeClick:l,handlePositiveClick:t,handleMaskClick:f,handleEsc:c}},render(){const{handlePositiveClick:r,handleUpdateShow:s,handleNegativeClick:i,handleCloseClick:t,handleAfterLeave:l,handleMaskClick:u,handleEsc:f,to:c,maskClosable:e,show:o}=this;return d(se,{show:o,onUpdateShow:s,onMaskClick:u,onEsc:f,to:c,maskClosable:e,onAfterEnter:this.onAfterEnter,onAfterLeave:l,closeOnEsc:this.closeOnEsc,blockScroll:this.blockScroll,autoFocus:this.autoFocus,transformOrigin:this.transformOrigin,internalAppear:!0,internalDialog:!0},{default:()=>d(oe,Object.assign({},ce(this.$props,ne),{style:this.internalStyle,onClose:t,onNegativeClick:i,onPositiveClick:r}))})}}),Ee={injectionKey:String,to:[String,Object]},lo=I({name:"DialogProvider",props:Ee,setup(){const r=y([]),s={};function i(c={}){const e=L(),o=S(Object.assign(Object.assign({},c),{key:e,destroy:()=>{s[`n-dialog-${e}`].hide()}}));return r.value.push(o),o}const t=["info","success","warning","error"].map(c=>e=>i(Object.assign(Object.assign({},e),{type:c})));function l(c){const{value:e}=r;e.splice(e.findIndex(o=>o.key===c),1)}function u(){Object.values(s).forEach(c=>{c.hide()})}const f={create:i,destroyAll:u,info:t[0],success:t[1],warning:t[2],error:t[3]};return x(re,f),x(le,{clickedRef:te(64),clickPositionRef:ie()}),x(ae,r),Object.assign(Object.assign({},f),{dialogList:r,dialogInstRefs:s,handleAfterLeave:l})},render(){var r,s;return d(P,null,[this.dialogList.map(i=>d(Ae,j(i,["destroy","style"],{internalStyle:i.style,to:this.to,ref:t=>{t===null?delete this.dialogInstRefs[`n-dialog-${i.key}`]:this.dialogInstRefs[`n-dialog-${i.key}`]=t},internalKey:i.key,onInternalAfterLeave:this.handleAfterLeave}))),(s=(r=this.$slots).default)===null||s===void 0?void 0:s.call(r)])}}),Me={margin:"0 0 8px 0",padding:"10px 20px",maxWidth:"720px",minWidth:"420px",iconMargin:"0 10px 0 0",closeMargin:"0 0 0 10px",closeSize:"20px",closeIconSize:"16px",iconSize:"20px",fontSize:"14px"},ze=r=>{const{textColor2:s,closeIconColor:i,closeIconColorHover:t,closeIconColorPressed:l,infoColor:u,successColor:f,errorColor:c,warningColor:e,popoverColor:o,boxShadow2:n,primaryColor:a,lineHeight:g,borderRadius:v,closeColorHover:h,closeColorPressed:m}=r;return Object.assign(Object.assign({},Me),{closeBorderRadius:v,textColor:s,textColorInfo:s,textColorSuccess:s,textColorError:s,textColorWarning:s,textColorLoading:s,color:o,colorInfo:o,colorSuccess:o,colorError:o,colorWarning:o,colorLoading:o,boxShadow:n,boxShadowInfo:n,boxShadowSuccess:n,boxShadowError:n,boxShadowWarning:n,boxShadowLoading:n,iconColor:s,iconColorInfo:u,iconColorSuccess:f,iconColorWarning:e,iconColorError:c,iconColorLoading:a,closeColorHover:h,closeColorPressed:m,closeIconColor:i,closeIconColorHover:t,closeIconColorPressed:l,closeColorHoverInfo:h,closeColorPressedInfo:m,closeIconColorInfo:i,closeIconColorHoverInfo:t,closeIconColorPressedInfo:l,closeColorHoverSuccess:h,closeColorPressedSuccess:m,closeIconColorSuccess:i,closeIconColorHoverSuccess:t,closeIconColorPressedSuccess:l,closeColorHoverError:h,closeColorPressedError:m,closeIconColorError:i,closeIconColorHoverError:t,closeIconColorPressedError:l,closeColorHoverWarning:h,closeColorPressedWarning:m,closeIconColorWarning:i,closeIconColorHoverWarning:t,closeIconColorPressedWarning:l,closeColorHoverLoading:h,closeColorPressedLoading:m,closeIconColorLoading:i,closeIconColorHoverLoading:t,closeIconColorPressedLoading:l,loadingColor:a,lineHeight:g,borderRadius:v})},He={name:"Message",common:ve,self:ze},Re=He,z={icon:Function,type:{type:String,default:"info"},content:[String,Number,Function],showIcon:{type:Boolean,default:!0},closable:Boolean,keepAliveOnHover:Boolean,onClose:Function,onMouseenter:Function,onMouseleave:Function},Ne=C([k("message-wrapper",`
 margin: var(--n-margin);
 z-index: 0;
 transform-origin: top center;
 display: flex;
 `,[be({overflow:"visible",originalTransition:"transform .3s var(--n-bezier)",enterToProps:{transform:"scale(1)"},leaveToProps:{transform:"scale(0.85)"}})]),k("message",`
 box-sizing: border-box;
 display: flex;
 align-items: center;
 transition:
 color .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 opacity .3s var(--n-bezier),
 transform .3s var(--n-bezier),
 margin-bottom .3s var(--n-bezier);
 padding: var(--n-padding);
 border-radius: var(--n-border-radius);
 flex-wrap: nowrap;
 overflow: hidden;
 max-width: var(--n-max-width);
 color: var(--n-text-color);
 background-color: var(--n-color);
 box-shadow: var(--n-box-shadow);
 `,[O("content",`
 display: inline-block;
 line-height: var(--n-line-height);
 font-size: var(--n-font-size);
 `),O("icon",`
 position: relative;
 margin: var(--n-icon-margin);
 height: var(--n-icon-size);
 width: var(--n-icon-size);
 font-size: var(--n-icon-size);
 flex-shrink: 0;
 `,[["default","info","success","warning","error","loading"].map(r=>b(`${r}-type`,[C("> *",`
 color: var(--n-icon-color-${r});
 transition: color .3s var(--n-bezier);
 `)])),C("> *",`
 position: absolute;
 left: 0;
 top: 0;
 right: 0;
 bottom: 0;
 `,[he()])]),O("close",`
 margin: var(--n-close-margin);
 transition:
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 flex-shrink: 0;
 `,[C("&:hover",`
 color: var(--n-close-icon-color-hover);
 `),C("&:active",`
 color: var(--n-close-icon-color-pressed);
 `)])]),k("message-container",`
 z-index: 6000;
 position: fixed;
 height: 0;
 overflow: visible;
 display: flex;
 flex-direction: column;
 align-items: center;
 `,[b("top",`
 top: 12px;
 left: 0;
 right: 0;
 `),b("top-left",`
 top: 12px;
 left: 12px;
 right: 0;
 align-items: flex-start;
 `),b("top-right",`
 top: 12px;
 left: 0;
 right: 12px;
 align-items: flex-end;
 `),b("bottom",`
 bottom: 4px;
 left: 0;
 right: 0;
 justify-content: flex-end;
 `),b("bottom-left",`
 bottom: 4px;
 left: 12px;
 right: 0;
 justify-content: flex-end;
 align-items: flex-start;
 `),b("bottom-right",`
 bottom: 4px;
 left: 0;
 right: 12px;
 justify-content: flex-end;
 align-items: flex-end;
 `)])]),Fe={info:()=>d(Oe,null),success:()=>d(we,null),warning:()=>d(Se,null),error:()=>d(Pe,null),default:()=>null},$e=I({name:"Message",props:Object.assign(Object.assign({},z),{render:Function}),setup(r){const{inlineThemeDisabled:s,mergedRtlRef:i}=M(r),{props:t,mergedClsPrefixRef:l}=de(A),u=Ce("Message",i,l),f=E("Message","-message",Ne,Re,t,l),c=w(()=>{const{type:o}=r,{common:{cubicBezierEaseInOut:n},self:{padding:a,margin:g,maxWidth:v,iconMargin:h,closeMargin:m,closeSize:H,iconSize:R,fontSize:N,lineHeight:F,borderRadius:$,iconColorInfo:T,iconColorSuccess:W,iconColorWarning:B,iconColorError:_,iconColorLoading:K,closeIconSize:D,closeBorderRadius:V,[p("textColor",o)]:q,[p("boxShadow",o)]:U,[p("color",o)]:G,[p("closeColorHover",o)]:J,[p("closeColorPressed",o)]:Q,[p("closeIconColor",o)]:X,[p("closeIconColorPressed",o)]:Y,[p("closeIconColorHover",o)]:Z}}=f.value;return{"--n-bezier":n,"--n-margin":g,"--n-padding":a,"--n-max-width":v,"--n-font-size":N,"--n-icon-margin":h,"--n-icon-size":R,"--n-close-icon-size":D,"--n-close-border-radius":V,"--n-close-size":H,"--n-close-margin":m,"--n-text-color":q,"--n-color":G,"--n-box-shadow":U,"--n-icon-color-info":T,"--n-icon-color-success":W,"--n-icon-color-warning":B,"--n-icon-color-error":_,"--n-icon-color-loading":K,"--n-close-color-hover":J,"--n-close-color-pressed":Q,"--n-close-icon-color":X,"--n-close-icon-color-pressed":Y,"--n-close-icon-color-hover":Z,"--n-line-height":F,"--n-border-radius":$}}),e=s?xe("message",w(()=>r.type[0]),c,{}):void 0;return{mergedClsPrefix:l,rtlEnabled:u,messageProviderProps:t,handleClose(){var o;(o=r.onClose)===null||o===void 0||o.call(r)},cssVars:s?void 0:c,themeClass:e==null?void 0:e.themeClass,onRender:e==null?void 0:e.onRender,placement:t.placement}},render(){const{render:r,type:s,closable:i,content:t,mergedClsPrefix:l,cssVars:u,themeClass:f,onRender:c,icon:e,handleClose:o,showIcon:n}=this;c==null||c();let a;return d("div",{class:[`${l}-message-wrapper`,f],onMouseenter:this.onMouseenter,onMouseleave:this.onMouseleave,style:[{alignItems:this.placement.startsWith("top")?"flex-start":"flex-end"},u]},r?r(this.$props):d("div",{class:[`${l}-message ${l}-message--${s}-type`,this.rtlEnabled&&`${l}-message--rtl`]},(a=Te(e,s,l))&&n?d("div",{class:`${l}-message__icon ${l}-message__icon--${s}-type`},d(me,null,{default:()=>a})):null,d("div",{class:`${l}-message__content`},ye(t)),i?d(Ie,{clsPrefix:l,class:`${l}-message__close`,onClick:o,absolute:!0}):null))}});function Te(r,s,i){if(typeof r=="function")return r();{const t=s==="loading"?d(pe,{clsPrefix:i,strokeWidth:24,scale:.85}):Fe[s]();return t?d(ke,{clsPrefix:i,key:s},{default:()=>t}):null}}const We=I({name:"MessageEnvironment",props:Object.assign(Object.assign({},z),{duration:{type:Number,default:3e3},onAfterLeave:Function,onLeave:Function,internalKey:{type:String,required:!0},onInternalAfterLeave:Function,onHide:Function,onAfterHide:Function}),setup(r){let s=null;const i=y(!0);ue(()=>{t()});function t(){const{duration:n}=r;n&&(s=window.setTimeout(f,n))}function l(n){n.currentTarget===n.target&&s!==null&&(window.clearTimeout(s),s=null)}function u(n){n.currentTarget===n.target&&t()}function f(){const{onHide:n}=r;i.value=!1,s&&(window.clearTimeout(s),s=null),n&&n()}function c(){const{onClose:n}=r;n&&n(),f()}function e(){const{onAfterLeave:n,onInternalAfterLeave:a,onAfterHide:g,internalKey:v}=r;n&&n(),a&&a(v),g&&g()}function o(){f()}return{show:i,hide:f,handleClose:c,handleAfterLeave:e,handleMouseleave:u,handleMouseenter:l,deactivate:o}},render(){return d(je,{appear:!0,onAfterLeave:this.handleAfterLeave,onLeave:this.onLeave},{default:()=>[this.show?d($e,{content:this.content,type:this.type,icon:this.icon,showIcon:this.showIcon,closable:this.closable,onClose:this.handleClose,onMouseenter:this.keepAliveOnHover?this.handleMouseenter:void 0,onMouseleave:this.keepAliveOnHover?this.handleMouseleave:void 0}):null]})}}),Be=Object.assign(Object.assign({},E.props),{to:[String,Object],duration:{type:Number,default:3e3},keepAliveOnHover:Boolean,max:Number,placement:{type:String,default:"top"},closable:Boolean,containerStyle:[String,Object]}),ao=I({name:"MessageProvider",props:Be,setup(r){const{mergedClsPrefixRef:s}=M(r),i=y([]),t=y({}),l={create(e,o){return u(e,Object.assign({type:"default"},o))},info(e,o){return u(e,Object.assign(Object.assign({},o),{type:"info"}))},success(e,o){return u(e,Object.assign(Object.assign({},o),{type:"success"}))},warning(e,o){return u(e,Object.assign(Object.assign({},o),{type:"warning"}))},error(e,o){return u(e,Object.assign(Object.assign({},o),{type:"error"}))},loading(e,o){return u(e,Object.assign(Object.assign({},o),{type:"loading"}))},destroyAll:c};x(A,{props:r,mergedClsPrefixRef:s}),x(ge,l);function u(e,o){const n=L(),a=S(Object.assign(Object.assign({},o),{content:e,key:n,destroy:()=>{var v;(v=t.value[n])===null||v===void 0||v.hide()}})),{max:g}=r;return g&&i.value.length>=g&&i.value.shift(),i.value.push(a),a}function f(e){i.value.splice(i.value.findIndex(o=>o.key===e),1),delete t.value[e]}function c(){Object.values(t.value).forEach(e=>{e.hide()})}return Object.assign({mergedClsPrefix:s,messageRefs:t,messageList:i,handleAfterLeave:f},l)},render(){var r,s,i;return d(P,null,(s=(r=this.$slots).default)===null||s===void 0?void 0:s.call(r),this.messageList.length?d(fe,{to:(i=this.to)!==null&&i!==void 0?i:"body"},d("div",{class:[`${this.mergedClsPrefix}-message-container`,`${this.mergedClsPrefix}-message-container--${this.placement}`],key:"message-container",style:this.containerStyle},this.messageList.map(t=>d(We,Object.assign({ref:l=>{l&&(this.messageRefs[t.key]=l)},internalKey:t.key,onInternalAfterLeave:this.handleAfterLeave},j(t,["destroy"],void 0),{duration:t.duration===void 0?this.duration:t.duration,keepAliveOnHover:t.keepAliveOnHover===void 0?this.keepAliveOnHover:t.keepAliveOnHover,closable:t.closable===void 0?this.closable:t.closable}))))):null)}});export{ao as N,lo as a};
