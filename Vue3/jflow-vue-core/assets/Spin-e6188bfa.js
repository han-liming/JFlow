import{d as S,f as u,r as C,G as x,a8 as a,bi as k}from"./index-f4658ae7.js";import{k as T,c as f,e as m,f as h,b as g}from"./light-0dfdc1ad.js";import{f as $}from"./Scrollbar-35d51129.js";import{u as w}from"./use-config-816d55a6.js";import{p as N}from"./index-22809599.js";import{c as R}from"./create-key-bf4384d6.js";import{u as B}from"./use-css-vars-class-3ae3b4b3.js";import{u as L}from"./use-compitable-1a225331.js";import{N as P}from"./Loading-fead3a83.js";const V=e=>{const{opacityDisabled:r,heightTiny:i,heightSmall:t,heightMedium:l,heightLarge:s,heightHuge:c,primaryColor:o,fontSize:n}=e;return{fontSize:n,textColor:o,sizeTiny:i,sizeSmall:t,sizeMedium:l,sizeLarge:s,sizeHuge:c,color:o,opacitySpinning:r}},W={name:"Spin",common:T,self:V},H=W,I=f([f("@keyframes spin-rotate",`
 from {
 transform: rotate(0);
 }
 to {
 transform: rotate(360deg);
 }
 `),m("spin-container",{position:"relative"},[m("spin-body",`
 position: absolute;
 top: 50%;
 left: 50%;
 transform: translateX(-50%) translateY(-50%);
 `,[$()])]),m("spin-body",`
 display: inline-flex;
 align-items: center;
 justify-content: center;
 flex-direction: column;
 `),m("spin",`
 display: inline-flex;
 height: var(--n-size);
 width: var(--n-size);
 font-size: var(--n-size);
 color: var(--n-color);
 `,[h("rotate",`
 animation: spin-rotate 2s linear infinite;
 `)]),m("spin-description",`
 display: inline-block;
 font-size: var(--n-font-size);
 color: var(--n-text-color);
 transition: color .3s var(--n-bezier);
 margin-top: 8px;
 `),m("spin-content",`
 opacity: 1;
 transition: opacity .3s var(--n-bezier);
 pointer-events: all;
 `,[h("spinning",`
 user-select: none;
 -webkit-user-select: none;
 pointer-events: none;
 opacity: var(--n-opacity-spinning);
 `)])]),O={small:20,medium:18,large:16},j=Object.assign(Object.assign({},g.props),{description:String,stroke:String,size:{type:[String,Number],default:"medium"},show:{type:Boolean,default:!0},strokeWidth:Number,rotate:{type:Boolean,default:!0},spinning:{type:Boolean,validator:()=>!0,default:void 0},delay:Number}),A=S({name:"Spin",props:j,setup(e){const{mergedClsPrefixRef:r,inlineThemeDisabled:i}=w(e),t=g("Spin","-spin",I,H,e,r),l=u(()=>{const{size:n}=e,{common:{cubicBezierEaseInOut:d},self:p}=t.value,{opacitySpinning:v,color:y,textColor:b}=p,z=typeof n=="number"?N(n):p[R("size",n)];return{"--n-bezier":d,"--n-opacity-spinning":v,"--n-size":z,"--n-color":y,"--n-text-color":b}}),s=i?B("spin",u(()=>{const{size:n}=e;return typeof n=="number"?String(n):n[0]}),l,e):void 0,c=L(e,["spinning","show"]),o=C(!1);return x(n=>{let d;if(c.value){const{delay:p}=e;if(p){d=window.setTimeout(()=>{o.value=!0},p),n(()=>{clearTimeout(d)});return}}o.value=c.value}),{mergedClsPrefix:r,active:o,mergedStrokeWidth:u(()=>{const{strokeWidth:n}=e;if(n!==void 0)return n;const{size:d}=e;return O[typeof d=="number"?"medium":d]}),cssVars:i?void 0:l,themeClass:s==null?void 0:s.themeClass,onRender:s==null?void 0:s.onRender}},render(){var e,r;const{$slots:i,mergedClsPrefix:t,description:l}=this,s=i.icon&&this.rotate,c=(l||i.description)&&a("div",{class:`${t}-spin-description`},l||((e=i.description)===null||e===void 0?void 0:e.call(i))),o=i.icon?a("div",{class:[`${t}-spin-body`,this.themeClass]},a("div",{class:[`${t}-spin`,s&&`${t}-spin--rotate`],style:i.default?"":this.cssVars},i.icon()),c):a("div",{class:[`${t}-spin-body`,this.themeClass]},a(P,{clsPrefix:t,style:i.default?"":this.cssVars,stroke:this.stroke,"stroke-width":this.mergedStrokeWidth,class:`${t}-spin`}),c);return(r=this.onRender)===null||r===void 0||r.call(this),i.default?a("div",{class:[`${t}-spin-container`,this.themeClass],style:this.cssVars},a("div",{class:[`${t}-spin-content`,this.active&&`${t}-spin-content--spinning`]},i),a(k,{name:"fade-in-transition"},{default:()=>this.active?o:null})):o}});export{A as N};
