var C=Object.defineProperty;var a=(F,i,u)=>i in F?C(F,i,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[i]=u;var E=(F,i,u)=>(a(F,typeof i!="symbol"?i+"":i,u),u);var l=(F,i,u)=>new Promise((t,o)=>{var n=e=>{try{m(u.next(e))}catch(r){o(r)}},p=e=>{try{m(u.throw(e))}catch(r){o(r)}},m=e=>e.done?t(e.value):Promise.resolve(e.value).then(n,p);m((u=u.apply(F,i)).next())});import{b as B,a as g}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as s}from"./PageBaseGroupEdit-202e8e85.js";import{SysEnumMains as c}from"./SysEnumMain-7dd95ff9.js";import{SFTables as S}from"./SFTable-d63f9fb4.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./EntityNoName-d08126ae.js";import"./SysEnum-989b6639.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class Z extends s{constructor(){super("GPE_SingleChoiceSmall");E(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u5C31\u662F\u7528\u6237\u5F55\u5165\u76F8\u5BF9\u56FA\u5B9A\u7684\u6587\u672C\u65F6\uFF0C\u5728\u6587\u672C\u6846\u91CC\u63D0\u524D\u8F93\u5165\u76F8\u5173\u7684\u9009\u9879\uFF0C\u53EF\u4EE5\u76F4\u63A5\u8FDB\u884C\u5355\u9879\u9009\u62E9\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u6BD4\u5982\uFF0C\u8BF7\u5047\u7C7B\u578B\uFF0C\u51FA\u884C\u65B9\u5F0F\u7B49
  #### \u6548\u679C\u56FE
  - \u6309\u6587\u672C\u8F93\u5165\u7684\u503C
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmall1.png "\u5C4F\u5E55\u622A\u56FE.png")
  - \u6309\u7CFB\u7EDF\u5916\u952E\u8868\u8BA1\u7B97
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmall.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);E(this,"Desc1",`
  #### \u8BF4\u660E
   - \u503C\u7528\u9017\u53F7\u5206\u5F00,\u6BD4\u5982: \u98DE\u673A,\u706B\u8F66,\u8F6E\u8239,\u706B\u7BAD,\u5176\u4ED6
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmallPeizhi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmall1.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);E(this,"Desc2",`
  #### \u8BF4\u660E
   - \u7CFB\u7EDF\u7528\u679A\u4E3E\u503C\u4F5C\u4E3A\u8BE5\u5B57\u6BB5\u7684\u5355\u9009
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmall.png "\u5C4F\u5E55\u622A\u56FE.png")

  `);E(this,"Desc3",`
  #### \u8BF4\u660E
   - \u7CFB\u7EDF\u7528\u7CFB\u7EDF\u5916\u952E\u4F5C\u4E3A\u8BE5\u5B57\u6BB5\u7684\u5355\u9009
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/SingleChoiceSmall/Img/SingleChouceSmall.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);this.PageTitle="\u5C0F\u8303\u56F4\u5355\u9009"}Init(){return l(this,null,function*(){this.entity=new B,this.KeyOfEn=g.DoWay,yield this.entity.InitDataForMapAttr("SingleChoiceSmall",this.GetRequestVal("PKVal")),this.AddGroup("A","\u5C0F\u8303\u56F4\u5355\u9009"),this.Blank("0","\u4E0D\u8BBE\u7F6E",this.Desc0),this.SingleTB("1","\u6309\u6587\u672C\u8F93\u5165\u7684\u503C","Tag1",this.Desc1,""),this.SingleDDLEntities("2","\u6309\u7167\u679A\u4E3E\u503C","Tag1",this.Desc2,new c,!1),this.SingleDDLEntities("3","\u6309\u7167\u7CFB\u7EDF\u5916\u952E\u8868\u8BA1\u7B97","Tag1",this.Desc3,new S,!1),this.SingleTBSQL("4","\u6309\u7167SQL\u8BA1\u7B97","Tag4","\u67E5\u8BE2sql\u8FD4\u56DENo,Name\u4E24\u5217")})}AfterSave(u,t){if(u==t)throw new Error("Method not implemented.")}BtnClick(u,t,o){if(u==t||u===o)throw new Error("Method not implemented.")}}export{Z as GPE_SingleChoiceSmall};
