var T=Object.defineProperty;var x=(i,B,r)=>B in i?T(i,B,{enumerable:!0,configurable:!0,writable:!0,value:r}):i[B]=r;var A=(i,B,r)=>(x(i,typeof B!="symbol"?B+"":B,r),r);var d=(i,B,r)=>new Promise((s,l)=>{var c=u=>{try{E(r.next(u))}catch(a){l(a)}},p=u=>{try{E(r.throw(u))}catch(a){l(a)}},E=u=>u.done?s(u.value):Promise.resolve(u.value).then(c,p);E((r=r.apply(i,B)).next())});import{FrmSorts as b}from"./FrmSort-0f444555.js";import w from"./GloFrm-2131e899.js";import{GroupFields as y,GroupField as h}from"./GroupField-d6637832.js";import{PageBaseGroupNew as f,GPNReturnObj as m,GPNReturnType as D}from"./PageBaseGroupNew-ee20c033.js";import S from"./BSEntity-840a884b.js";import{$ as N}from"./index-f4658ae7.js";import{windowOpen as g}from"./windowOpen-b8703c39.js";import"./EntityNoName-d08126ae.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Entities-6a72b013.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityTree-333c163f.js";import"./assign-481cba08.js";import"./_createAssigner-77c8874c.js";import"./MapAttr-cb594d82.js";import"./EntityMyPK-e742fec8.js";import"./Events-141c34ea.js";import"./EntityOID-553df0d1.js";import"./MapData-4fa397be.js";import"./EnumLab-4f91f91c.js";import"./GloComm-7cfbdfd9.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class _u extends f{constructor(){super("GPN_NewFrm");A(this,"FrmDictDesc",`
  
  #### \u5E2E\u52A9 
   - \u5B9E\u4F53\u662F\u7BA1\u7406\u5BF9\u8C61. \u6BD4\u5982\u56FA\u5B9A\u8D44\u4EA7\u7BA1\u7406\u3001\u5408\u540C\u7BA1\u7406\u3001\u4EBA\u529B\u5B66\u751F\u3001\u9879\u76EE\u7B49.
   - \u5B9E\u4F53\u7684\u57FA\u7840\u7BA1\u7406\u5C31\u662F\u5BF9\u5B83\u7684\u589E\u3001\u5220\u3001\u6539\u3001\u67E5.
   - \u5BF9\u5B9E\u4F53\u7684\u7BA1\u7406\u5305\u62EC\u5B9E\u4F53\u7684\u6D41\u7A0B\u7BA1\u7406\u3001\u76F8\u5173\u529F\u80FD\u7BA1\u7406\u3001\u65B9\u6CD5\u7BA1\u7406\u4E09\u90E8\u5206.
   - \u901A\u8FC7\u5BF9\u5916\u63D0\u4F9Burl\u6A21\u5F0F\u7684api\u63A5\u53E3\uFF0C\u7ED1\u5B9A\u5230\u83DC\u5355\u91CC.
   - \u5B9E\u4F53\u4E0E\u6D41\u7A0B\u7684\u5173\u7CFB\u8BF7\u53C2\u8003: http://doc.ccbpm.cn
   #### \u793A\u4F8B
    - \u56FE1
    - \u56FE2
  `);A(this,"FrmBillDesc",`
  
  #### \u5E2E\u52A9
   - \u5B9A\u4E49: \u5355\u636E\u5177\u6709\u6D41\u6C34\u6027\u8D28\u7684\u6570\u636E\u589E\u5220\u6539\u67E5,\u6BD4\u5982:\u62A5\u9500\u5355\u3001\u8BF7\u5047\u5355\u3001\u51FA\u5DEE\u7533\u8BF7\u5355.
   - \u5355\u636E\u4E0E\u6D41\u7A0B: \u5355\u636E\u53EF\u4EE5\u88AB\u6D41\u7A0B\u8282\u70B9\u7ED1\u5B9A,\u4E5F\u53EF\u4E5F\u4ECE\u5B9E\u4F53\u4E0A\u53D1\u8D77.
   - \u57FA\u672C\u5B57\u6BB5: \u5236\u5355\u4EBAStarter\u3001\u5236\u5355\u65E5\u671FRDT\u3001\u5355\u53F7BillNo\u3001\u6807\u9898Title\u3001\u72B6\u6001BillSta.
   - \u5355\u636E\u7F16\u53F7:\u53EF\u4EE5\u81EA\u52A8\u5B9A\u4E49,\u5B58\u50A8\u5728BillNo\u5B57\u6BB5\u4E2D.
   - \u5355\u636E\u6807\u9898: \u53EF\u4EE5\u81EA\u5B9A\u4E49\u89C4\u5219\uFF0C\u7C7B\u4F3C\u4E8E\u6D41\u7A0B\u6807\u9898.
   - \u5355\u636E\u72B6\u6001: BillState 0=\u8349\u7A3F,1=\u7F16\u8F91\u4E2D,2=\u9000\u56DE,3=\u5F52\u6863.
   - \u5355\u636E\u4E3B\u952E: OID \u662F\u4E2A\u81EA\u52A8\u751F\u7684\u5B57\u6BB5,\u7C7B\u4F3C\u4E8EWorkID.
   - \u53D1\u8D77\u4EBA: Starter, StarterName.
   - \u5197\u4F59\u5B57\u6BB5: PWorkID, PFrmID\u7236\u8868\u5355.
   #### \u64CD\u4F5C\u754C\u9762.
   1. \u521B\u5EFA\u4E00\u4E2A\u5355\u636E\u6570\u636E\u5B58\u50A8\u5230 Frm_GenerBill \u4E00\u4EFD.
   2. \u5F85\u529E:\u5355\u636E\u7BA1\u7406
   3. \u6211\u7684\u5355\u636E: \u6211\u53D1\u8D77\u7684\u5355\u636E,\u7B49\u5F85\u6211\u5BA1\u6279\u7684\u5355\u636E,\u5DF2\u7ECF\u521B\u5EFA\u7684\u5355\u636E.
   4. \u53D1\u8D77\u5355\u636E: \u6211\u80FD\u521B\u5EFA\u7684\u5355\u636E\u5217\u8868.
   5. \u5355\u636E\u8349\u7A3F: \u542F\u52A8\u7684\u8349\u7A3F.
   6. \u8FD1\u671F\u5355\u636E: \u8FD1\u671F\u53D1\u8D77\u7684\u5355\u636E.
  `);A(this,"FoolFrm",`
  #### \u5E2E\u52A9 
   - \u8BE5\u8868\u5355\u662F\u56FA\u5B9A\u683C\u5F0F\u7684\u8868\u5355,\u53EF\u4EE5\u5C55\u73B04\u52176\u5217\u5C55\u73B0.
   - \u4F18\u70B9:\u5F00\u53D1\u6548\u7387\u9AD8,\u5C55\u73B0\u7B80\u6D01,\u5B66\u4E60\u6210\u672C\u4F4E,\u4E1A\u52A1\u4EBA\u5458\u53EF\u4EE5\u5165\u624B.
   - \u7F3A\u70B9:\u5C55\u793A\u6837\u5F0F\u56FA\u5B9A.
  `);A(this,"DevFrm",`
  #### \u5E2E\u52A9
   - \u4F9D\u6258\u5BCC\u6587\u672C\u7F16\u8F91\u5668,\u5B9E\u73B0\u5BF9\u8868\u5355\u7684\u7F16\u8F91.
   - \u4F18\u70B9:\u683C\u5F0F\u7075\u6D3B,\u5C55\u73B0\u6548\u679C\u968F\u5FC3\u6240\u6B32.
   - \u7F3A\u70B9:\u4E1A\u52A1\u4EBA\u5458\u5165\u624B\u9700\u8981\u4E00\u5B9A\u7684\u5B66\u4E60\u6210\u672C.
   - \u9002\u7528\u4E8E:\u6548\u679C
    
  `);A(this,"ChartFrm",`
  
  #### \u5E2E\u52A9
   - \u4F9D\u6258\u4E8E\u7ECF\u5178\u8868\u5355\u8BBE\u8BA1\u5668\u8FDB\u884C\u8BBE\u8BA1.
   - \u4E00\u4E2A\u5206\u7EC4\u5C31\u662F\u7AE0.
   - \u6BCF\u4E2A\u5B57\u6BB5\u90FD\u662F\u5927\u5757\u6587\u672C,\u5C31\u662F\u8282.
    
  `);this.PageTitle="\u65B0\u5EFA\u8868\u5355"}Init(){return d(this,null,function*(){this.AddGroup("D","\u5B9E\u4F53\u8868\u5355"),this.TextBox2_NameNo("FrmDict","\u65B0\u5EFA\u5B9E\u4F53",this.FrmDictDesc,"Dict_","\u5B9E\u4F53ID","\u5B9E\u4F53\u540D\u79F0",""),this.AddIcon("icon-layers","FrmDict"),this.TextBox2_NameNo("FrmBill","\u65B0\u5EFA\u5355\u636E",this.FrmBillDesc,"Bill_","\u5355\u636EID","\u5355\u636E\u540D\u79F0",""),this.AddIcon("icon-notebook","FrmBill"),this.AddGroup("A","\u6D41\u7A0B\u8868\u5355"),this.TextBox3_NameNoNote("0","\u7ECF\u5178\u8868\u5355",this.FoolFrm,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","\u5B58\u50A8\u8868",""),this.AddIcon("icon-notebook","0"),this.TextBox3_NameNoNote("8","\u5F00\u53D1\u8005\u8868\u5355",this.DevFrm,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","\u5B58\u50A8\u8868",""),this.AddIcon("icon-calendar","8"),this.TextBox3_NameNoNote("10","\u7AE0\u8282\u8868\u5355",this.ChartFrm,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","\u5B58\u50A8\u8868",""),this.TextBox3_NameNoNote("6","Vsto\u6A21\u6A21\u5F0FExcel\u8868\u5355",this.HelpUn,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","\u5B58\u50A8\u8868",""),this.AddIcon("icon-grid","6"),this.TextBox3_NameNoNote("9","Wps\u8868\u5355",this.HelpUn,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","\u5B58\u50A8\u8868",""),this.TextBox2_NameNo("3","\u5D4C\u5165\u5F0F\u8868\u5355",this.HelpUn,"Frm_","\u8868\u5355ID","\u8868\u5355\u540D\u79F0","")})}GenerSorts(){return d(this,null,function*(){const r=new b;return yield r.Init(),yield r.RetrieveAll(),r})}Save_TextBox_X(r,s,l,c,p){return d(this,null,function*(){const E=l,u=c,a=p;if(r=="FrmDict"){const t=new N("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");t.AddPara("TB_No",u),t.AddPara("TB_Name",E),t.AddPara("TB_PTable",a),t.AddPara("DDL_DBSrc","local"),t.AddPara("FK_FrmSort",s),t.AddPara("EntityType",2);const F=yield t.DoMethodReturnString("NewFrmGuide_Create");if(typeof F=="string"&&F.includes("err@")==!0)return new m(D.Error,F);yield w.CheckForm(u);const e="/#/WF/Designer/Form?FrmID="+u;return new m(D.OpenUrlByNewWindow,e)}if(r=="FrmBill"){const t=new N("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");t.AddPara("TB_No",u),t.AddPara("TB_Name",E),t.AddPara("TB_PTable",a),t.AddPara("DDL_DBSrc","local"),t.AddPara("FK_FrmSort",s),t.AddPara("EntityType",1);const F=yield t.DoMethodReturnString("NewFrmGuide_Create");if(typeof F=="string"&&F.includes("err@")==!0)return new m(D.Error,F);yield w.CheckForm(u);const e=new S("BP.CCBill.FrmBill",u);yield e.RetrieveFromDBSources(),yield e.DoMethodReturnJSON("CheckEnityTypeAttrsFor_Bill");const P="/#/WF/Designer/Form?FrmID="+u;return new m(D.OpenUrlByNewWindow,P)}const n=new N("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");n.AddPara("TB_No",u),n.AddPara("TB_Name",E),n.AddPara("TB_PTable",a),n.AddPara("DDL_FrmType",r),n.AddPara("FK_FrmSort",s);const C=yield n.DoMethodReturnString("NewFrmGuide_Create");if(typeof C=="string"){g(C,E);return}if(r=="10"){const t=new y;if(yield t.Retrieve("FrmID",u),t.length==0){const o=new h;o.Lab="\u8282\u70B91",o.FrmID=u,o.CtrlType="Dir",o.Idx=0,yield o.Insert();const F=new h;F.Lab="\u8282\u70B92",F.FrmID=u,F.CtrlType="Dir",F.ParentOID=o.OID,F.Idx=0,yield F.Insert();const e=new h;e.Lab="\u8282\u70B93",e.FrmID=u,e.CtrlType="Dir",e.ParentOID=o.OID,e.Idx=1,yield e.Insert()}}const _=C;if(typeof _=="string"&&_.includes("err@")==!0)return new m(D.Error,_);yield w.CheckForm(u);const I="/#/WF/Designer/Form?FrmID="+u;return new m(D.OpenUrlByNewWindow,I)})}}export{_u as GPN_NewFrm};
