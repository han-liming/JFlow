import{i as ar}from"./browser-1654e206.js";import{u as ir}from"./use-rtl-889b67fe.js";import{a as lr,u as sr}from"./use-config-816d55a6.js";import{d as be,a8 as a,r as y,O as ge,k as ur,f as _,dO as Ce,P as dr,D as cr,G as ze,p as fr,F as hr,J as Se}from"./index-f4658ae7.js";import{b as pr,u as vr,r as oe,a as he,c as w}from"./use-merged-state-66be05d7.js";import{k as gr,o as pe,e as x,g as d,c as A,f as $,h as X,b as Te}from"./light-0dfdc1ad.js";import{u as br,N as Fe}from"./Icon-e3cbad7d.js";import{u as mr}from"./Loading-fead3a83.js";import{u as xr}from"./use-form-item-34ce685d.js";import{u as Ae}from"./use-memo-f04d43e5.js";import{c as ve}from"./create-key-bf4384d6.js";import{a as yr}from"./index-22809599.js";import{u as wr}from"./use-css-vars-class-3ae3b4b3.js";import{N as Cr,a as $e,o as Re}from"./Scrollbar-35d51129.js";import{V as zr}from"./VResizeObserver-e3ad0bab.js";import{N as _e,a as Sr}from"./Suffix-56e79b3b.js";import{E as Fr}from"./Eye-9d82dd1a.js";const Ar=be({name:"EyeOff",render(){return a("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512"},a("path",{d:"M432 448a15.92 15.92 0 0 1-11.31-4.69l-352-352a16 16 0 0 1 22.62-22.62l352 352A16 16 0 0 1 432 448z",fill:"currentColor"}),a("path",{d:"M255.66 384c-41.49 0-81.5-12.28-118.92-36.5c-34.07-22-64.74-53.51-88.7-91v-.08c19.94-28.57 41.78-52.73 65.24-72.21a2 2 0 0 0 .14-2.94L93.5 161.38a2 2 0 0 0-2.71-.12c-24.92 21-48.05 46.76-69.08 76.92a31.92 31.92 0 0 0-.64 35.54c26.41 41.33 60.4 76.14 98.28 100.65C162 402 207.9 416 255.66 416a239.13 239.13 0 0 0 75.8-12.58a2 2 0 0 0 .77-3.31l-21.58-21.58a4 4 0 0 0-3.83-1a204.8 204.8 0 0 1-51.16 6.47z",fill:"currentColor"}),a("path",{d:"M490.84 238.6c-26.46-40.92-60.79-75.68-99.27-100.53C349 110.55 302 96 255.66 96a227.34 227.34 0 0 0-74.89 12.83a2 2 0 0 0-.75 3.31l21.55 21.55a4 4 0 0 0 3.88 1a192.82 192.82 0 0 1 50.21-6.69c40.69 0 80.58 12.43 118.55 37c34.71 22.4 65.74 53.88 89.76 91a.13.13 0 0 1 0 .16a310.72 310.72 0 0 1-64.12 72.73a2 2 0 0 0-.15 2.95l19.9 19.89a2 2 0 0 0 2.7.13a343.49 343.49 0 0 0 68.64-78.48a32.2 32.2 0 0 0-.1-34.78z",fill:"currentColor"}),a("path",{d:"M256 160a95.88 95.88 0 0 0-21.37 2.4a2 2 0 0 0-1 3.38l112.59 112.56a2 2 0 0 0 3.38-1A96 96 0 0 0 256 160z",fill:"currentColor"}),a("path",{d:"M165.78 233.66a2 2 0 0 0-3.38 1a96 96 0 0 0 115 115a2 2 0 0 0 1-3.38z",fill:"currentColor"}))}}),$r={paddingTiny:"0 8px",paddingSmall:"0 10px",paddingMedium:"0 12px",paddingLarge:"0 14px",clearSize:"16px"},Rr=r=>{const{textColor2:v,textColor3:t,textColorDisabled:S,primaryColor:C,primaryColorHover:g,inputColor:c,inputColorDisabled:s,borderColor:u,warningColor:i,warningColorHover:l,errorColor:f,errorColorHover:b,borderRadius:E,lineHeight:z,fontSizeTiny:ne,fontSizeSmall:V,fontSizeMedium:te,fontSizeLarge:F,heightTiny:P,heightSmall:O,heightMedium:T,heightLarge:ae,actionColor:B,clearColor:I,clearColorHover:R,clearColorPressed:M,placeholderColor:N,placeholderColorDisabled:L,iconColor:ie,iconColorDisabled:le,iconColorHover:K,iconColorPressed:se}=r;return Object.assign(Object.assign({},$r),{countTextColorDisabled:S,countTextColor:t,heightTiny:P,heightSmall:O,heightMedium:T,heightLarge:ae,fontSizeTiny:ne,fontSizeSmall:V,fontSizeMedium:te,fontSizeLarge:F,lineHeight:z,lineHeightTextarea:z,borderRadius:E,iconSize:"16px",groupLabelColor:B,groupLabelTextColor:v,textColor:v,textColorDisabled:S,textDecorationColor:v,caretColor:C,placeholderColor:N,placeholderColorDisabled:L,color:c,colorDisabled:s,colorFocus:c,groupLabelBorder:`1px solid ${u}`,border:`1px solid ${u}`,borderHover:`1px solid ${g}`,borderDisabled:`1px solid ${u}`,borderFocus:`1px solid ${g}`,boxShadowFocus:`0 0 0 2px ${pe(C,{alpha:.2})}`,loadingColor:C,loadingColorWarning:i,borderWarning:`1px solid ${i}`,borderHoverWarning:`1px solid ${l}`,colorFocusWarning:c,borderFocusWarning:`1px solid ${l}`,boxShadowFocusWarning:`0 0 0 2px ${pe(i,{alpha:.2})}`,caretColorWarning:i,loadingColorError:f,borderError:`1px solid ${f}`,borderHoverError:`1px solid ${b}`,colorFocusError:c,borderFocusError:`1px solid ${b}`,boxShadowFocusError:`0 0 0 2px ${pe(f,{alpha:.2})}`,caretColorError:f,clearColor:I,clearColorHover:R,clearColorPressed:M,iconColor:ie,iconColorDisabled:le,iconColorHover:K,iconColorPressed:se,suffixTextColor:v})},_r={name:"Input",common:gr,self:Rr},Er=_r,Be=lr("n-input");function Tr(r){let v=0;for(const t of r)v++;return v}function re(r){return r===""||r==null}function Br(r){const v=y(null);function t(){const{value:g}=r;if(!(g!=null&&g.focus)){C();return}const{selectionStart:c,selectionEnd:s,value:u}=g;if(c==null||s==null){C();return}v.value={start:c,end:s,beforeText:u.slice(0,c),afterText:u.slice(s)}}function S(){var g;const{value:c}=v,{value:s}=r;if(!c||!s)return;const{value:u}=s,{start:i,beforeText:l,afterText:f}=c;let b=u.length;if(u.endsWith(f))b=u.length-f.length;else if(u.startsWith(l))b=l.length;else{const E=l[i-1],z=u.indexOf(E,i-1);z!==-1&&(b=z+1)}(g=s.setSelectionRange)===null||g===void 0||g.call(s,b,b)}function C(){v.value=null}return ge(r,C),{recordCursor:t,restoreCursor:S}}const Ee=be({name:"InputWordCount",setup(r,{slots:v}){const{mergedValueRef:t,maxlengthRef:S,mergedClsPrefixRef:C,countGraphemesRef:g}=ur(Be),c=_(()=>{const{value:s}=t;return s===null||Array.isArray(s)?0:(g.value||Tr)(s)});return()=>{const{value:s}=S,{value:u}=t;return a("span",{class:`${C.value}-input-word-count`},pr(v.default,{value:u===null||Array.isArray(u)?"":u},()=>[s===void 0?c.value:`${c.value} / ${s}`]))}}}),Pr=x("input",`
 max-width: 100%;
 cursor: text;
 line-height: 1.5;
 z-index: auto;
 outline: none;
 box-sizing: border-box;
 position: relative;
 display: inline-flex;
 border-radius: var(--n-border-radius);
 background-color: var(--n-color);
 transition: background-color .3s var(--n-bezier);
 font-size: var(--n-font-size);
 --n-padding-vertical: calc((var(--n-height) - 1.5 * var(--n-font-size)) / 2);
`,[d("input, textarea",`
 overflow: hidden;
 flex-grow: 1;
 position: relative;
 `),d("input-el, textarea-el, input-mirror, textarea-mirror, separator, placeholder",`
 box-sizing: border-box;
 font-size: inherit;
 line-height: 1.5;
 font-family: inherit;
 border: none;
 outline: none;
 background-color: #0000;
 text-align: inherit;
 transition:
 -webkit-text-fill-color .3s var(--n-bezier),
 caret-color .3s var(--n-bezier),
 color .3s var(--n-bezier),
 text-decoration-color .3s var(--n-bezier);
 `),d("input-el, textarea-el",`
 -webkit-appearance: none;
 scrollbar-width: none;
 width: 100%;
 min-width: 0;
 text-decoration-color: var(--n-text-decoration-color);
 color: var(--n-text-color);
 caret-color: var(--n-caret-color);
 background-color: transparent;
 `,[A("&::-webkit-scrollbar, &::-webkit-scrollbar-track-piece, &::-webkit-scrollbar-thumb",`
 width: 0;
 height: 0;
 display: none;
 `),A("&::placeholder",`
 color: #0000;
 -webkit-text-fill-color: transparent !important;
 `),A("&:-webkit-autofill ~",[d("placeholder","display: none;")])]),$("round",[X("textarea","border-radius: calc(var(--n-height) / 2);")]),d("placeholder",`
 pointer-events: none;
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 overflow: hidden;
 color: var(--n-placeholder-color);
 `,[A("span",`
 width: 100%;
 display: inline-block;
 `)]),$("textarea",[d("placeholder","overflow: visible;")]),X("autosize","width: 100%;"),$("autosize",[d("textarea-el, input-el",`
 position: absolute;
 top: 0;
 left: 0;
 height: 100%;
 `)]),x("input-wrapper",`
 overflow: hidden;
 display: inline-flex;
 flex-grow: 1;
 position: relative;
 padding-left: var(--n-padding-left);
 padding-right: var(--n-padding-right);
 `),d("input-mirror",`
 padding: 0;
 height: var(--n-height);
 line-height: var(--n-height);
 overflow: hidden;
 visibility: hidden;
 position: static;
 white-space: pre;
 pointer-events: none;
 `),d("input-el",`
 padding: 0;
 height: var(--n-height);
 line-height: var(--n-height);
 `,[A("+",[d("placeholder",`
 display: flex;
 align-items: center; 
 `)])]),X("textarea",[d("placeholder","white-space: nowrap;")]),d("eye",`
 display: flex;
 align-items: center;
 justify-content: center;
 transition: color .3s var(--n-bezier);
 `),$("textarea","width: 100%;",[x("input-word-count",`
 position: absolute;
 right: var(--n-padding-right);
 bottom: var(--n-padding-vertical);
 `),$("resizable",[x("input-wrapper",`
 resize: vertical;
 min-height: var(--n-height);
 `)]),d("textarea-el, textarea-mirror, placeholder",`
 height: 100%;
 padding-left: 0;
 padding-right: 0;
 padding-top: var(--n-padding-vertical);
 padding-bottom: var(--n-padding-vertical);
 word-break: break-word;
 display: inline-block;
 vertical-align: bottom;
 box-sizing: border-box;
 line-height: var(--n-line-height-textarea);
 margin: 0;
 resize: none;
 white-space: pre-wrap;
 scroll-padding-block-end: var(--n-padding-vertical);
 `),d("textarea-mirror",`
 width: 100%;
 pointer-events: none;
 overflow: hidden;
 visibility: hidden;
 position: static;
 white-space: pre-wrap;
 overflow-wrap: break-word;
 `)]),$("pair",[d("input-el, placeholder","text-align: center;"),d("separator",`
 display: flex;
 align-items: center;
 transition: color .3s var(--n-bezier);
 color: var(--n-text-color);
 white-space: nowrap;
 `,[x("icon",`
 color: var(--n-icon-color);
 `),x("base-icon",`
 color: var(--n-icon-color);
 `)])]),$("disabled",`
 cursor: not-allowed;
 background-color: var(--n-color-disabled);
 `,[d("border","border: var(--n-border-disabled);"),d("input-el, textarea-el",`
 cursor: not-allowed;
 color: var(--n-text-color-disabled);
 text-decoration-color: var(--n-text-color-disabled);
 `),d("placeholder","color: var(--n-placeholder-color-disabled);"),d("separator","color: var(--n-text-color-disabled);",[x("icon",`
 color: var(--n-icon-color-disabled);
 `),x("base-icon",`
 color: var(--n-icon-color-disabled);
 `)]),x("input-word-count",`
 color: var(--n-count-text-color-disabled);
 `),d("suffix, prefix","color: var(--n-text-color-disabled);",[x("icon",`
 color: var(--n-icon-color-disabled);
 `),x("internal-icon",`
 color: var(--n-icon-color-disabled);
 `)])]),X("disabled",[d("eye",`
 color: var(--n-icon-color);
 cursor: pointer;
 `,[A("&:hover",`
 color: var(--n-icon-color-hover);
 `),A("&:active",`
 color: var(--n-icon-color-pressed);
 `)]),A("&:hover",[d("state-border","border: var(--n-border-hover);")]),$("focus","background-color: var(--n-color-focus);",[d("state-border",`
 border: var(--n-border-focus);
 box-shadow: var(--n-box-shadow-focus);
 `)])]),d("border, state-border",`
 box-sizing: border-box;
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 pointer-events: none;
 border-radius: inherit;
 border: var(--n-border);
 transition:
 box-shadow .3s var(--n-bezier),
 border-color .3s var(--n-bezier);
 `),d("state-border",`
 border-color: #0000;
 z-index: 1;
 `),d("prefix","margin-right: 4px;"),d("suffix",`
 margin-left: 4px;
 `),d("suffix, prefix",`
 transition: color .3s var(--n-bezier);
 flex-wrap: nowrap;
 flex-shrink: 0;
 line-height: var(--n-height);
 white-space: nowrap;
 display: inline-flex;
 align-items: center;
 justify-content: center;
 color: var(--n-suffix-text-color);
 `,[x("base-loading",`
 font-size: var(--n-icon-size);
 margin: 0 2px;
 color: var(--n-loading-color);
 `),x("base-clear",`
 font-size: var(--n-icon-size);
 `,[d("placeholder",[x("base-icon",`
 transition: color .3s var(--n-bezier);
 color: var(--n-icon-color);
 font-size: var(--n-icon-size);
 `)])]),A(">",[x("icon",`
 transition: color .3s var(--n-bezier);
 color: var(--n-icon-color);
 font-size: var(--n-icon-size);
 `)]),x("base-icon",`
 font-size: var(--n-icon-size);
 `)]),x("input-word-count",`
 pointer-events: none;
 line-height: 1.5;
 font-size: .85em;
 color: var(--n-count-text-color);
 transition: color .3s var(--n-bezier);
 margin-left: 4px;
 font-variant: tabular-nums;
 `),["warning","error"].map(r=>$(`${r}-status`,[X("disabled",[x("base-loading",`
 color: var(--n-loading-color-${r})
 `),d("input-el, textarea-el",`
 caret-color: var(--n-caret-color-${r});
 `),d("state-border",`
 border: var(--n-border-${r});
 `),A("&:hover",[d("state-border",`
 border: var(--n-border-hover-${r});
 `)]),A("&:focus",`
 background-color: var(--n-color-focus-${r});
 `,[d("state-border",`
 box-shadow: var(--n-box-shadow-focus-${r});
 border: var(--n-border-focus-${r});
 `)]),$("focus",`
 background-color: var(--n-color-focus-${r});
 `,[d("state-border",`
 box-shadow: var(--n-box-shadow-focus-${r});
 border: var(--n-border-focus-${r});
 `)])])]))]),Ir=x("input",[$("disabled",[d("input-el, textarea-el",`
 -webkit-text-fill-color: var(--n-text-color-disabled);
 `)])]),Mr=Object.assign(Object.assign({},Te.props),{bordered:{type:Boolean,default:void 0},type:{type:String,default:"text"},placeholder:[Array,String],defaultValue:{type:[String,Array],default:null},value:[String,Array],disabled:{type:Boolean,default:void 0},size:String,rows:{type:[Number,String],default:3},round:Boolean,minlength:[String,Number],maxlength:[String,Number],clearable:Boolean,autosize:{type:[Boolean,Object],default:!1},pair:Boolean,separator:String,readonly:{type:[String,Boolean],default:!1},passivelyActivated:Boolean,showPasswordOn:String,stateful:{type:Boolean,default:!0},autofocus:Boolean,inputProps:Object,resizable:{type:Boolean,default:!0},showCount:Boolean,loading:{type:Boolean,default:void 0},allowInput:Function,renderCount:Function,onMousedown:Function,onKeydown:Function,onKeyup:[Function,Array],onInput:[Function,Array],onFocus:[Function,Array],onBlur:[Function,Array],onClick:[Function,Array],onChange:[Function,Array],onClear:[Function,Array],countGraphemes:Function,status:String,"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array],textDecoration:[String,Array],attrSize:{type:Number,default:20},onInputBlur:[Function,Array],onInputFocus:[Function,Array],onDeactivate:[Function,Array],onActivate:[Function,Array],onWrapperFocus:[Function,Array],onWrapperBlur:[Function,Array],internalDeactivateOnEnter:Boolean,internalForceFocus:Boolean,internalLoadingBeforeSuffix:{type:Boolean,default:!0},showPasswordToggle:Boolean}),Zr=be({name:"Input",props:Mr,setup(r){const{mergedClsPrefixRef:v,mergedBorderedRef:t,inlineThemeDisabled:S,mergedRtlRef:C}=sr(r),g=Te("Input","-input",Pr,Er,r,v);ar&&br("-input-safari",Ir,v);const c=y(null),s=y(null),u=y(null),i=y(null),l=y(null),f=y(null),b=y(null),E=Br(b),z=y(null),{localeRef:ne}=mr("Input"),V=y(r.defaultValue),te=Ce(r,"value"),F=vr(te,V),P=xr(r),{mergedSizeRef:O,mergedDisabledRef:T,mergedStatusRef:ae}=P,B=y(!1),I=y(!1),R=y(!1),M=y(!1);let N=null;const L=_(()=>{const{placeholder:e,pair:o}=r;return o?Array.isArray(e)?e:e===void 0?["",""]:[e,e]:e===void 0?[ne.value.placeholder]:[e]}),ie=_(()=>{const{value:e}=R,{value:o}=F,{value:n}=L;return!e&&(re(o)||Array.isArray(o)&&re(o[0]))&&n[0]}),le=_(()=>{const{value:e}=R,{value:o}=F,{value:n}=L;return!e&&n[1]&&(re(o)||Array.isArray(o)&&re(o[1]))}),K=Ae(()=>r.internalForceFocus||B.value),se=Ae(()=>{if(T.value||r.readonly||!r.clearable||!K.value&&!I.value)return!1;const{value:e}=F,{value:o}=K;return r.pair?!!(Array.isArray(e)&&(e[0]||e[1]))&&(I.value||o):!!e&&(I.value||o)}),ue=_(()=>{const{showPasswordOn:e}=r;if(e)return e;if(r.showPasswordToggle)return"click"}),j=y(!1),Pe=_(()=>{const{textDecoration:e}=r;return e?Array.isArray(e)?e.map(o=>({textDecoration:o})):[{textDecoration:e}]:["",""]}),me=y(void 0),Ie=()=>{var e,o;if(r.type==="textarea"){const{autosize:n}=r;if(n&&(me.value=(o=(e=z.value)===null||e===void 0?void 0:e.$el)===null||o===void 0?void 0:o.offsetWidth),!s.value||typeof n=="boolean")return;const{paddingTop:p,paddingBottom:m,lineHeight:h}=window.getComputedStyle(s.value),W=Number(p.slice(0,-2)),D=Number(m.slice(0,-2)),k=Number(h.slice(0,-2)),{value:U}=u;if(!U)return;if(n.minRows){const G=Math.max(n.minRows,1),fe=`${W+D+k*G}px`;U.style.minHeight=fe}if(n.maxRows){const G=`${W+D+k*n.maxRows}px`;U.style.maxHeight=G}}},Me=_(()=>{const{maxlength:e}=r;return e===void 0?void 0:Number(e)});dr(()=>{const{value:e}=F;Array.isArray(e)||ce(e)});const We=cr().proxy;function Y(e){const{onUpdateValue:o,"onUpdate:value":n,onInput:p}=r,{nTriggerFormInput:m}=P;o&&w(o,e),n&&w(n,e),p&&w(p,e),V.value=e,m()}function J(e){const{onChange:o}=r,{nTriggerFormChange:n}=P;o&&w(o,e),V.value=e,n()}function De(e){const{onBlur:o}=r,{nTriggerFormBlur:n}=P;o&&w(o,e),n()}function ke(e){const{onFocus:o}=r,{nTriggerFormFocus:n}=P;o&&w(o,e),n()}function Ve(e){const{onClear:o}=r;o&&w(o,e)}function He(e){const{onInputBlur:o}=r;o&&w(o,e)}function Oe(e){const{onInputFocus:o}=r;o&&w(o,e)}function Ne(){const{onDeactivate:e}=r;e&&w(e)}function Le(){const{onActivate:e}=r;e&&w(e)}function Ke(e){const{onClick:o}=r;o&&w(o,e)}function je(e){const{onWrapperFocus:o}=r;o&&w(o,e)}function Ue(e){const{onWrapperBlur:o}=r;o&&w(o,e)}function Ge(){R.value=!0}function Xe(e){R.value=!1,e.target===f.value?q(e,1):q(e,0)}function q(e,o=0,n="input"){const p=e.target.value;if(ce(p),e instanceof InputEvent&&!e.isComposing&&(R.value=!1),r.type==="textarea"){const{value:h}=z;h&&h.syncUnifiedContainer()}if(N=p,R.value)return;E.recordCursor();const m=Ye(p);if(m)if(!r.pair)n==="input"?Y(p):J(p);else{let{value:h}=F;Array.isArray(h)?h=[h[0],h[1]]:h=["",""],h[o]=p,n==="input"?Y(h):J(h)}We.$forceUpdate(),m||Se(E.restoreCursor)}function Ye(e){const{countGraphemes:o,maxlength:n,minlength:p}=r;if(o){let h;if(n!==void 0&&(h===void 0&&(h=o(e)),h>Number(n))||p!==void 0&&(h===void 0&&(h=o(e)),h<Number(n)))return!1}const{allowInput:m}=r;return typeof m=="function"?m(e):!0}function Je(e){He(e),e.relatedTarget===c.value&&Ne(),e.relatedTarget!==null&&(e.relatedTarget===l.value||e.relatedTarget===f.value||e.relatedTarget===s.value)||(M.value=!1),Q(e,"blur"),b.value=null}function qe(e,o){Oe(e),B.value=!0,M.value=!0,Le(),Q(e,"focus"),o===0?b.value=l.value:o===1?b.value=f.value:o===2&&(b.value=s.value)}function Qe(e){r.passivelyActivated&&(Ue(e),Q(e,"blur"))}function Ze(e){r.passivelyActivated&&(B.value=!0,je(e),Q(e,"focus"))}function Q(e,o){e.relatedTarget!==null&&(e.relatedTarget===l.value||e.relatedTarget===f.value||e.relatedTarget===s.value||e.relatedTarget===c.value)||(o==="focus"?(ke(e),B.value=!0):o==="blur"&&(De(e),B.value=!1))}function eo(e,o){q(e,o,"change")}function oo(e){Ke(e)}function ro(e){Ve(e),r.pair?(Y(["",""]),J(["",""])):(Y(""),J(""))}function no(e){const{onMousedown:o}=r;o&&o(e);const{tagName:n}=e.target;if(n!=="INPUT"&&n!=="TEXTAREA"){if(r.resizable){const{value:p}=c;if(p){const{left:m,top:h,width:W,height:D}=p.getBoundingClientRect(),k=14;if(m+W-k<e.clientX&&e.clientX<m+W&&h+D-k<e.clientY&&e.clientY<h+D)return}}e.preventDefault(),B.value||xe()}}function to(){var e;I.value=!0,r.type==="textarea"&&((e=z.value)===null||e===void 0||e.handleMouseEnterWrapper())}function ao(){var e;I.value=!1,r.type==="textarea"&&((e=z.value)===null||e===void 0||e.handleMouseLeaveWrapper())}function io(){T.value||ue.value==="click"&&(j.value=!j.value)}function lo(e){if(T.value)return;e.preventDefault();const o=p=>{p.preventDefault(),Re("mouseup",document,o)};if($e("mouseup",document,o),ue.value!=="mousedown")return;j.value=!0;const n=()=>{j.value=!1,Re("mouseup",document,n)};$e("mouseup",document,n)}function so(e){r.onKeyup&&w(r.onKeyup,e)}function uo(e){switch(r.onKeydown&&w(r.onKeydown,e),e.key){case"Escape":de();break;case"Enter":co(e);break}}function co(e){var o,n;if(r.passivelyActivated){const{value:p}=M;if(p){r.internalDeactivateOnEnter&&de();return}e.preventDefault(),r.type==="textarea"?(o=s.value)===null||o===void 0||o.focus():(n=l.value)===null||n===void 0||n.focus()}}function de(){r.passivelyActivated&&(M.value=!1,Se(()=>{var e;(e=c.value)===null||e===void 0||e.focus()}))}function xe(){var e,o,n;T.value||(r.passivelyActivated?(e=c.value)===null||e===void 0||e.focus():((o=s.value)===null||o===void 0||o.focus(),(n=l.value)===null||n===void 0||n.focus()))}function fo(){var e;!((e=c.value)===null||e===void 0)&&e.contains(document.activeElement)&&document.activeElement.blur()}function ho(){var e,o;(e=s.value)===null||e===void 0||e.select(),(o=l.value)===null||o===void 0||o.select()}function po(){T.value||(s.value?s.value.focus():l.value&&l.value.focus())}function vo(){const{value:e}=c;e!=null&&e.contains(document.activeElement)&&e!==document.activeElement&&de()}function go(e){if(r.type==="textarea"){const{value:o}=s;o==null||o.scrollTo(e)}else{const{value:o}=l;o==null||o.scrollTo(e)}}function ce(e){const{type:o,pair:n,autosize:p}=r;if(!n&&p)if(o==="textarea"){const{value:m}=u;m&&(m.textContent=(e!=null?e:"")+`\r
`)}else{const{value:m}=i;m&&(e?m.textContent=e:m.innerHTML="&nbsp;")}}function bo(){Ie()}const ye=y({top:"0"});function mo(e){var o;const{scrollTop:n}=e.target;ye.value.top=`${-n}px`,(o=z.value)===null||o===void 0||o.syncUnifiedContainer()}let Z=null;ze(()=>{const{autosize:e,type:o}=r;e&&o==="textarea"?Z=ge(F,n=>{!Array.isArray(n)&&n!==N&&ce(n)}):Z==null||Z()});let ee=null;ze(()=>{r.type==="textarea"?ee=ge(F,e=>{var o;!Array.isArray(e)&&e!==N&&((o=z.value)===null||o===void 0||o.syncUnifiedContainer())}):ee==null||ee()}),fr(Be,{mergedValueRef:F,maxlengthRef:Me,mergedClsPrefixRef:v,countGraphemesRef:Ce(r,"countGraphemes")});const xo={wrapperElRef:c,inputElRef:l,textareaElRef:s,isCompositing:R,focus:xe,blur:fo,select:ho,deactivate:vo,activate:po,scrollTo:go},yo=ir("Input",C,v),we=_(()=>{const{value:e}=O,{common:{cubicBezierEaseInOut:o},self:{color:n,borderRadius:p,textColor:m,caretColor:h,caretColorError:W,caretColorWarning:D,textDecorationColor:k,border:U,borderDisabled:G,borderHover:fe,borderFocus:wo,placeholderColor:Co,placeholderColorDisabled:zo,lineHeightTextarea:So,colorDisabled:Fo,colorFocus:Ao,textColorDisabled:$o,boxShadowFocus:Ro,iconSize:_o,colorFocusWarning:Eo,boxShadowFocusWarning:To,borderWarning:Bo,borderFocusWarning:Po,borderHoverWarning:Io,colorFocusError:Mo,boxShadowFocusError:Wo,borderError:Do,borderFocusError:ko,borderHoverError:Vo,clearSize:Ho,clearColor:Oo,clearColorHover:No,clearColorPressed:Lo,iconColor:Ko,iconColorDisabled:jo,suffixTextColor:Uo,countTextColor:Go,countTextColorDisabled:Xo,iconColorHover:Yo,iconColorPressed:Jo,loadingColor:qo,loadingColorError:Qo,loadingColorWarning:Zo,[ve("padding",e)]:er,[ve("fontSize",e)]:or,[ve("height",e)]:rr}}=g.value,{left:nr,right:tr}=yr(er);return{"--n-bezier":o,"--n-count-text-color":Go,"--n-count-text-color-disabled":Xo,"--n-color":n,"--n-font-size":or,"--n-border-radius":p,"--n-height":rr,"--n-padding-left":nr,"--n-padding-right":tr,"--n-text-color":m,"--n-caret-color":h,"--n-text-decoration-color":k,"--n-border":U,"--n-border-disabled":G,"--n-border-hover":fe,"--n-border-focus":wo,"--n-placeholder-color":Co,"--n-placeholder-color-disabled":zo,"--n-icon-size":_o,"--n-line-height-textarea":So,"--n-color-disabled":Fo,"--n-color-focus":Ao,"--n-text-color-disabled":$o,"--n-box-shadow-focus":Ro,"--n-loading-color":qo,"--n-caret-color-warning":D,"--n-color-focus-warning":Eo,"--n-box-shadow-focus-warning":To,"--n-border-warning":Bo,"--n-border-focus-warning":Po,"--n-border-hover-warning":Io,"--n-loading-color-warning":Zo,"--n-caret-color-error":W,"--n-color-focus-error":Mo,"--n-box-shadow-focus-error":Wo,"--n-border-error":Do,"--n-border-focus-error":ko,"--n-border-hover-error":Vo,"--n-loading-color-error":Qo,"--n-clear-color":Oo,"--n-clear-size":Ho,"--n-clear-color-hover":No,"--n-clear-color-pressed":Lo,"--n-icon-color":Ko,"--n-icon-color-hover":Yo,"--n-icon-color-pressed":Jo,"--n-icon-color-disabled":jo,"--n-suffix-text-color":Uo}}),H=S?wr("input",_(()=>{const{value:e}=O;return e[0]}),we,r):void 0;return Object.assign(Object.assign({},xo),{wrapperElRef:c,inputElRef:l,inputMirrorElRef:i,inputEl2Ref:f,textareaElRef:s,textareaMirrorElRef:u,textareaScrollbarInstRef:z,rtlEnabled:yo,uncontrolledValue:V,mergedValue:F,passwordVisible:j,mergedPlaceholder:L,showPlaceholder1:ie,showPlaceholder2:le,mergedFocus:K,isComposing:R,activated:M,showClearButton:se,mergedSize:O,mergedDisabled:T,textDecorationStyle:Pe,mergedClsPrefix:v,mergedBordered:t,mergedShowPasswordOn:ue,placeholderStyle:ye,mergedStatus:ae,textAreaScrollContainerWidth:me,handleTextAreaScroll:mo,handleCompositionStart:Ge,handleCompositionEnd:Xe,handleInput:q,handleInputBlur:Je,handleInputFocus:qe,handleWrapperBlur:Qe,handleWrapperFocus:Ze,handleMouseEnter:to,handleMouseLeave:ao,handleMouseDown:no,handleChange:eo,handleClick:oo,handleClear:ro,handlePasswordToggleClick:io,handlePasswordToggleMousedown:lo,handleWrapperKeydown:uo,handleWrapperKeyup:so,handleTextAreaMirrorResize:bo,getTextareaScrollContainer:()=>s.value,mergedTheme:g,cssVars:S?void 0:we,themeClass:H==null?void 0:H.themeClass,onRender:H==null?void 0:H.onRender})},render(){var r,v;const{mergedClsPrefix:t,mergedStatus:S,themeClass:C,type:g,countGraphemes:c,onRender:s}=this,u=this.$slots;return s==null||s(),a("div",{ref:"wrapperElRef",class:[`${t}-input`,C,S&&`${t}-input--${S}-status`,{[`${t}-input--rtl`]:this.rtlEnabled,[`${t}-input--disabled`]:this.mergedDisabled,[`${t}-input--textarea`]:g==="textarea",[`${t}-input--resizable`]:this.resizable&&!this.autosize,[`${t}-input--autosize`]:this.autosize,[`${t}-input--round`]:this.round&&g!=="textarea",[`${t}-input--pair`]:this.pair,[`${t}-input--focus`]:this.mergedFocus,[`${t}-input--stateful`]:this.stateful}],style:this.cssVars,tabindex:!this.mergedDisabled&&this.passivelyActivated&&!this.activated?0:void 0,onFocus:this.handleWrapperFocus,onBlur:this.handleWrapperBlur,onClick:this.handleClick,onMousedown:this.handleMouseDown,onMouseenter:this.handleMouseEnter,onMouseleave:this.handleMouseLeave,onCompositionstart:this.handleCompositionStart,onCompositionend:this.handleCompositionEnd,onKeyup:this.handleWrapperKeyup,onKeydown:this.handleWrapperKeydown},a("div",{class:`${t}-input-wrapper`},oe(u.prefix,i=>i&&a("div",{class:`${t}-input__prefix`},i)),g==="textarea"?a(Cr,{ref:"textareaScrollbarInstRef",class:`${t}-input__textarea`,container:this.getTextareaScrollContainer,triggerDisplayManually:!0,useUnifiedContainer:!0,internalHoistYRail:!0},{default:()=>{var i,l;const{textAreaScrollContainerWidth:f}=this,b={width:this.autosize&&f&&`${f}px`};return a(hr,null,a("textarea",Object.assign({},this.inputProps,{ref:"textareaElRef",class:[`${t}-input__textarea-el`,(i=this.inputProps)===null||i===void 0?void 0:i.class],autofocus:this.autofocus,rows:Number(this.rows),placeholder:this.placeholder,value:this.mergedValue,disabled:this.mergedDisabled,maxlength:c?void 0:this.maxlength,minlength:c?void 0:this.minlength,readonly:this.readonly,tabindex:this.passivelyActivated&&!this.activated?-1:void 0,style:[this.textDecorationStyle[0],(l=this.inputProps)===null||l===void 0?void 0:l.style,b],onBlur:this.handleInputBlur,onFocus:E=>{this.handleInputFocus(E,2)},onInput:this.handleInput,onChange:this.handleChange,onScroll:this.handleTextAreaScroll})),this.showPlaceholder1?a("div",{class:`${t}-input__placeholder`,style:[this.placeholderStyle,b],key:"placeholder"},this.mergedPlaceholder[0]):null,this.autosize?a(zr,{onResize:this.handleTextAreaMirrorResize},{default:()=>a("div",{ref:"textareaMirrorElRef",class:`${t}-input__textarea-mirror`,key:"mirror"})}):null)}}):a("div",{class:`${t}-input__input`},a("input",Object.assign({type:g==="password"&&this.mergedShowPasswordOn&&this.passwordVisible?"text":g},this.inputProps,{ref:"inputElRef",class:[`${t}-input__input-el`,(r=this.inputProps)===null||r===void 0?void 0:r.class],style:[this.textDecorationStyle[0],(v=this.inputProps)===null||v===void 0?void 0:v.style],tabindex:this.passivelyActivated&&!this.activated?-1:void 0,placeholder:this.mergedPlaceholder[0],disabled:this.mergedDisabled,maxlength:c?void 0:this.maxlength,minlength:c?void 0:this.minlength,value:Array.isArray(this.mergedValue)?this.mergedValue[0]:this.mergedValue,readonly:this.readonly,autofocus:this.autofocus,size:this.attrSize,onBlur:this.handleInputBlur,onFocus:i=>{this.handleInputFocus(i,0)},onInput:i=>{this.handleInput(i,0)},onChange:i=>{this.handleChange(i,0)}})),this.showPlaceholder1?a("div",{class:`${t}-input__placeholder`},a("span",null,this.mergedPlaceholder[0])):null,this.autosize?a("div",{class:`${t}-input__input-mirror`,key:"mirror",ref:"inputMirrorElRef"},"\xA0"):null),!this.pair&&oe(u.suffix,i=>i||this.clearable||this.showCount||this.mergedShowPasswordOn||this.loading!==void 0?a("div",{class:`${t}-input__suffix`},[oe(u["clear-icon-placeholder"],l=>(this.clearable||l)&&a(_e,{clsPrefix:t,show:this.showClearButton,onClear:this.handleClear},{placeholder:()=>l,icon:()=>{var f,b;return(b=(f=this.$slots)["clear-icon"])===null||b===void 0?void 0:b.call(f)}})),this.internalLoadingBeforeSuffix?null:i,this.loading!==void 0?a(Sr,{clsPrefix:t,loading:this.loading,showArrow:!1,showClear:!1,style:this.cssVars}):null,this.internalLoadingBeforeSuffix?i:null,this.showCount&&this.type!=="textarea"?a(Ee,null,{default:l=>{var f;return(f=u.count)===null||f===void 0?void 0:f.call(u,l)}}):null,this.mergedShowPasswordOn&&this.type==="password"?a("div",{class:`${t}-input__eye`,onMousedown:this.handlePasswordToggleMousedown,onClick:this.handlePasswordToggleClick},this.passwordVisible?he(u["password-visible-icon"],()=>[a(Fe,{clsPrefix:t},{default:()=>a(Fr,null)})]):he(u["password-invisible-icon"],()=>[a(Fe,{clsPrefix:t},{default:()=>a(Ar,null)})])):null]):null)),this.pair?a("span",{class:`${t}-input__separator`},he(u.separator,()=>[this.separator])):null,this.pair?a("div",{class:`${t}-input-wrapper`},a("div",{class:`${t}-input__input`},a("input",{ref:"inputEl2Ref",type:this.type,class:`${t}-input__input-el`,tabindex:this.passivelyActivated&&!this.activated?-1:void 0,placeholder:this.mergedPlaceholder[1],disabled:this.mergedDisabled,maxlength:c?void 0:this.maxlength,minlength:c?void 0:this.minlength,value:Array.isArray(this.mergedValue)?this.mergedValue[1]:void 0,readonly:this.readonly,style:this.textDecorationStyle[1],onBlur:this.handleInputBlur,onFocus:i=>{this.handleInputFocus(i,1)},onInput:i=>{this.handleInput(i,1)},onChange:i=>{this.handleChange(i,1)}}),this.showPlaceholder2?a("div",{class:`${t}-input__placeholder`},a("span",null,this.mergedPlaceholder[1])):null),oe(u.suffix,i=>(this.clearable||i)&&a("div",{class:`${t}-input__suffix`},[this.clearable&&a(_e,{clsPrefix:t,show:this.showClearButton,onClear:this.handleClear},{icon:()=>{var l;return(l=u["clear-icon"])===null||l===void 0?void 0:l.call(u)},placeholder:()=>{var l;return(l=u["clear-icon-placeholder"])===null||l===void 0?void 0:l.call(u)}}),i]))):null,this.mergedBordered?a("div",{class:`${t}-input__border`}):null,this.mergedBordered?a("div",{class:`${t}-input__state-border`}):null,this.showCount&&g==="textarea"?a(Ee,null,{default:i=>{var l;const{renderCount:f}=this;return f?f(i):(l=u.count)===null||l===void 0?void 0:l.call(u,i)}}):null)}});export{Zr as N,Er as i};
