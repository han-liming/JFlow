var s=Object.defineProperty;var a=(F,t,u)=>t in F?s(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var n=(F,t,u)=>(a(F,typeof t!="symbol"?t+"":t,u),u);var A=(F,t,u)=>new Promise((e,o)=>{var p=E=>{try{r(u.next(E))}catch(m){o(m)}},i=E=>{try{r(u.throw(E))}catch(m){o(m)}},r=E=>E.done?e(E.value):Promise.resolve(E.value).then(p,i);r((u=u.apply(F,t)).next())});import{b as c,a as C}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as L}from"./PageBaseGroupEdit-202e8e85.js";import{GPEAutoFullDLL as h}from"./GPEAutoFullDLL-b4e4409a.js";import{SFTable as d}from"./SFTable-d63f9fb4.js";import{GloComm as D}from"./GloComm-7cfbdfd9.js";import{GPNReturnObj as B,GPNReturnType as l}from"./PageBaseGroupNew-ee20c033.js";import{GPEAutoFullDDLSFTable as g}from"./GPEAutoFullDDLSFTable-6621de6c.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./SFDBSrc-e641ea16.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";class iu extends L{constructor(){super("GPE_AutoFullDLL");n(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u8BE5SQL\u5FC5\u987B\u8FD4\u56DENo,Name \u4E24\u4E2A\u5217\u3002
  - \u652F\u6301ccbpm\u8868\u8FBE\u5F0F\u3002
  - SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'
  #### \u5E94\u7528\u573A\u666F
   - \u9009\u62E9\u4E00\u4E2A\u4F1A\u8BAE\u4E3B\u6301\u4EBA\uFF0C\u4ECE\u672C\u90E8\u95E8\u4E2D\u9009\u62E9\u3002
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDLL/Img/AutoFullDLLSetting.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDLL/Img/AutoFullDLL.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);n(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u4E0D\u542F\u7528\uFF0C\u4E0D\u542F\u7528\u8FC7\u6EE4\u529F\u80FD\u3002
   - \u542F\u7528\u8FC7\u6EE4\u529F\u80FD\uFF0C\u5E94\u7528SQL\u8BED\u53E5\u7B49\uFF0C\u5F97\u5230\u7CFB\u7EDF\u8FD4\u56DE\u503C\u3002\u5BF9\u6570\u636E\u8FDB\u884C\u8FC7\u6EE4\u5904\u7406\u3002
   - \u6BD4\u5982\uFF0C\u9009\u53D6\u4E3B\u8BB2\u4EBA\uFF0C\u9009\u53D6\u73ED\u4E3B\u4EFB\u3002
   - \u52A0\u8F7D\u7684\u65F6\u5019\u586B\u5145\u7684\u6570\u636E.
  
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDLL/Img/AutoFullDLL.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
  `);n(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u7ED1\u5B9A\u5B57\u5178\u8868\u7684\u663E\u793A\u8FC7\u6EE4.
   - \u60A8\u53EF\u4EE5\u7EF4\u62A4\u5B57\u5178\u8868.
  
  `);this.PageTitle="\u8BBE\u7F6E\u663E\u793A\u8FC7\u6EE4"}Init(){return A(this,null,function*(){this.entity=new c,this.KeyOfEn=C.DoWay,this.Btns=[{pageNo:"2",list:["\u5B57\u5178\u7EF4\u62A4"]}],yield this.entity.InitDataForMapAttr("AutoFullDLL",this.GetRequestVal("PKVal")),this.AddGroup("A","\u8BBE\u7F6E\u663E\u793A\u8FC7\u6EE4"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.AddEntity("1","\u81EA\u5B9A\u4E49\u8BBE\u7F6E",new h,this.Desc1),this.AddEntity("2","\u7ED1\u5B9A\u5B57\u5178\u8868",new g,this.Desc2)})}AfterSave(u,e){if(u==e)throw new Error("Method not implemented.")}BtnClick(u,e,o){return A(this,null,function*(){var p;if(o=="\u5B57\u5178\u5C5E\u6027"){const i=(p=this.entity)==null?void 0:p.Doc;if(!i){alert("\u8BF7\u9009\u62E9\u7ED1\u5B9A\u7684\u5B57\u5178\uFF0C\u7136\u540E\u6267\u884C\u4FDD\u5B58\u6309\u94AE.");return}const r=new d(i);yield r.Retrieve();const E=r.GetParaString("EnName",""),m=D.UrlEn(E,r.No);return new B(l.OpenUrlByDrawer75,m)}if(o=="\u5B57\u5178\u7EF4\u62A4"){const i=D.UrlSearch("TS.FrmUI.SFTable");return new B(l.OpenUrlByDrawer75,i)}if(u==e||u===o)throw new Error("Method not implemented.")})}}export{iu as GPE_AutoFullDLL};
