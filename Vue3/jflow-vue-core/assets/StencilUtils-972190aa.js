import{userNodes as K,routeNodes as tt,ccNodes as et,subFlowNodes as it,labelNodes as ot}from"./x6Shapes-9ea647b1.js";import{V as A,e as J,b as p,G as H,d as N,f as S,g as k,h as st,r as v,j as V,R as nt,k as F,t as rt,l as _,M as O,m as L,n as T,o as G,N as X,p as P}from"./html-a2da55a6.js";import{o as at}from"./position-82cfc837.js";import{bd as lt}from"./index-f4658ae7.js";import"./_createCompounder-ed41a610.js";import"./merge-15067256.js";import"./_baseFor-37716d03.js";import"./_createAssigner-77c8874c.js";import"./difference-bddb49bc.js";import"./_createAggregator-c692fd07.js";import"./_baseEach-1e26df34.js";import"./_baseMap-457e4262.js";import"./isNumber-0bc0d001.js";import"./throttle-35369e52.js";const ct=`.x6-widget-dnd {
  position: absolute;
  top: -10000px;
  left: -10000px;
  z-index: 999999;
  display: none;
  cursor: move;
  opacity: 0.7;
  pointer-events: 'cursor';
}
.x6-widget-dnd.dragging {
  display: inline-block;
}
.x6-widget-dnd.dragging * {
  pointer-events: none !important;
}
.x6-widget-dnd .x6-graph {
  background: transparent;
  box-shadow: none;
}
`;var gt=globalThis&&globalThis.__decorate||function(l,t,e,i){var s=arguments.length,o=s<3?t:i===null?i=Object.getOwnPropertyDescriptor(t,e):i,n;if(typeof Reflect=="object"&&typeof Reflect.decorate=="function")o=Reflect.decorate(l,t,e,i);else for(var r=l.length-1;r>=0;r--)(n=l[r])&&(o=(s<3?n(o):s>3?n(t,e,o):n(t,e))||o);return s>3&&o&&Object.defineProperty(t,e,o),o};class w extends A{get targetScroller(){return this.options.target.getPlugin("scroller")}get targetGraph(){return this.options.target}get targetModel(){return this.targetGraph.model}get snapline(){return this.options.target.getPlugin("snapline")}constructor(t){super(),this.name="dnd",this.options=Object.assign(Object.assign({},w.defaults),t),this.init()}init(){J(this.name,ct),this.container=document.createElement("div"),p(this.container,this.prefixClassName("widget-dnd")),this.draggingGraph=new H(Object.assign(Object.assign({},this.options.delegateGraphOptions),{container:document.createElement("div"),width:1,height:1,async:!1})),N(this.container,this.draggingGraph.container)}start(t,e){const i=e;i.preventDefault(),this.targetModel.startBatch("dnd"),p(this.container,"dragging"),S(this.container,this.options.draggingContainer||document.body),this.sourceNode=t,this.prepareDragging(t,i.clientX,i.clientY);const s=this.updateNodePosition(i.clientX,i.clientY);this.isSnaplineEnabled()&&(this.snapline.captureCursorOffset({e:i,node:t,cell:t,view:this.draggingView,x:s.x,y:s.y}),this.draggingNode.on("change:position",this.snap,this)),this.delegateDocumentEvents(w.documentEvents,i.data)}isSnaplineEnabled(){return this.snapline&&this.snapline.isEnabled()}prepareDragging(t,e,i){const s=this.draggingGraph,o=s.model,n=this.options.getDragNode(t,{sourceNode:t,draggingGraph:s,targetGraph:this.targetGraph});n.position(0,0);let r=5;if(this.isSnaplineEnabled()&&(r+=this.snapline.options.tolerance||0),this.isSnaplineEnabled()||this.options.scaled){const g=this.targetGraph.transform.getScale();s.scale(g.sx,g.sy),r*=Math.max(g.sx,g.sy)}else s.scale(1,1);this.clearDragging(),o.resetCells([n]);const a=s.findViewByCell(n);a.undelegateEvents(),a.cell.off("changed"),s.fitToContent({padding:r,allowNewOrigin:"any",useCellGeometry:!1});const c=a.getBBox();this.geometryBBox=a.getBBox({useCellGeometry:!0}),this.delta=this.geometryBBox.getTopLeft().diff(c.getTopLeft()),this.draggingNode=n,this.draggingView=a,this.draggingBBox=n.getBBox(),this.padding=r,this.originOffset=this.updateGraphPosition(e,i)}updateGraphPosition(t,e){const i=document.body.scrollTop||document.documentElement.scrollTop,s=document.body.scrollLeft||document.documentElement.scrollLeft,o=this.delta,n=this.geometryBBox,r=this.padding||5,a={left:t-o.x-n.width/2-r+s,top:e-o.y-n.height/2-r+i};return this.draggingGraph&&k(this.container,{left:`${a.left}px`,top:`${a.top}px`}),a}updateNodePosition(t,e){const i=this.targetGraph.clientToLocal(t,e),s=this.draggingBBox;return i.x-=s.width/2,i.y-=s.height/2,this.draggingNode.position(i.x,i.y),i}snap({cell:t,current:e,options:i}){const s=t;if(i.snapped){const o=this.draggingBBox;s.position(o.x+i.tx,o.y+i.ty,{silent:!0}),this.draggingView.translate(),s.position(e.x,e.y,{silent:!0}),this.snapOffset={x:i.tx,y:i.ty}}else this.snapOffset=null}onDragging(t){const e=this.draggingView;if(e){t.preventDefault();const i=this.normalizeEvent(t),s=i.clientX,o=i.clientY;this.updateGraphPosition(s,o);const n=this.updateNodePosition(s,o),r=this.targetGraph.options.embedding.enabled,a=(r||this.isSnaplineEnabled())&&this.isInsideValidArea({x:s,y:o});if(r){e.setEventData(i,{graph:this.targetGraph,candidateEmbedView:this.candidateEmbedView});const c=e.getEventData(i);a?e.processEmbedding(i,c):e.clearEmbedding(c),this.candidateEmbedView=c.candidateEmbedView}this.isSnaplineEnabled()&&(a?this.snapline.snapOnMoving({e:i,view:e,x:n.x,y:n.y}):this.snapline.hide())}}onDragEnd(t){const e=this.draggingNode;if(e){const i=this.normalizeEvent(t),s=this.draggingView,o=this.draggingBBox,n=this.snapOffset;let r=o.x,a=o.y;n&&(r+=n.x,a+=n.y),e.position(r,a,{silent:!0});const c=this.drop(e,{x:i.clientX,y:i.clientY}),g=u=>{u?(this.onDropped(e),this.targetGraph.options.embedding.enabled&&s&&(s.setEventData(i,{cell:u,graph:this.targetGraph,candidateEmbedView:this.candidateEmbedView}),s.finalizeEmbedding(i,s.getEventData(i)))):this.onDropInvalid(),this.candidateEmbedView=null,this.targetModel.stopBatch("dnd")};st(c)?(this.undelegateDocumentEvents(),c.then(g)):g(c)}}clearDragging(){this.draggingNode&&(this.sourceNode=null,this.draggingNode.remove(),this.draggingNode=null,this.draggingView=null,this.delta=null,this.padding=null,this.snapOffset=null,this.originOffset=null,this.undelegateDocumentEvents())}onDropped(t){this.draggingNode===t&&(this.clearDragging(),v(this.container,"dragging"),V(this.container))}onDropInvalid(){const t=this.draggingNode;t&&this.onDropped(t)}isInsideValidArea(t){let e,i=null;const s=this.targetGraph,o=this.targetScroller;this.options.dndContainer&&(i=this.getDropArea(this.options.dndContainer));const n=i&&i.containsPoint(t);if(o)if(o.options.autoResize)e=this.getDropArea(o.container);else{const r=this.getDropArea(o.container);e=this.getDropArea(s.container).intersectsWithRect(r)}else e=this.getDropArea(s.container);return!n&&e&&e.containsPoint(t)}getDropArea(t){const e=at(t),i=document.body.scrollTop||document.documentElement.scrollTop,s=document.body.scrollLeft||document.documentElement.scrollLeft;return nt.create({x:e.left+parseInt(k(t,"border-left-width"),10)-s,y:e.top+parseInt(k(t,"border-top-width"),10)-i,width:t.clientWidth,height:t.clientHeight})}drop(t,e){if(this.isInsideValidArea(e)){const i=this.targetGraph,s=i.model,o=i.clientToLocal(e),n=this.sourceNode,r=this.options.getDropNode(t,{sourceNode:n,draggingNode:t,targetGraph:this.targetGraph,draggingGraph:this.draggingGraph}),a=r.getBBox();o.x+=a.x-a.width/2,o.y+=a.y-a.height/2;const c=this.snapOffset?1:i.getGridSize();r.position(F.snapToGrid(o.x,c),F.snapToGrid(o.y,c)),r.removeZIndex();const g=this.options.validateNode,u=g?g(r,{sourceNode:n,draggingNode:t,droppingNode:r,targetGraph:i,draggingGraph:this.draggingGraph}):!0;return typeof u=="boolean"?u?(s.addCell(r,{stencil:this.cid}),r):null:rt(u).then(x=>x?(s.addCell(r,{stencil:this.cid}),r):null)}return null}onRemove(){this.draggingGraph&&(this.draggingGraph.view.remove(),this.draggingGraph.dispose())}dispose(){this.remove(),_(this.name)}}gt([A.dispose()],w.prototype,"dispose",null);(function(l){l.defaults={getDragNode:t=>t.clone(),getDropNode:t=>t.clone()},l.documentEvents={mousemove:"onDragging",touchmove:"onDragging",mouseup:"onDragEnd",touchend:"onDragEnd",touchcancel:"onDragEnd"}})(w||(w={}));function ht(l,t={}){const e=O.isModel(l)?l:new O().resetCells(l,{sort:!1,dryrun:!0}),i=e.getNodes(),s=t.columns||1,o=Math.ceil(i.length/s),n=t.dx||0,r=t.dy||0,a=t.center!==!1,c=t.resizeToFit===!0,g=t.marginX||0,u=t.marginY||0,x=[];let f=t.columnWidth;if(f==="compact")for(let h=0;h<s;h+=1){const y=b.getNodesInColumn(i,h,s);x.push(b.getMaxDim(y,"width")+n)}else{(f==null||f==="auto")&&(f=b.getMaxDim(i,"width")+n);for(let h=0;h<s;h+=1)x.push(f)}const j=b.accumulate(x,g),C=[];let D=t.rowHeight;if(D==="compact")for(let h=0;h<o;h+=1){const y=b.getNodesInRow(i,h,s);C.push(b.getMaxDim(y,"height")+r)}else{(D==null||D==="auto")&&(D=b.getMaxDim(i,"height")+r);for(let h=0;h<o;h+=1)C.push(D)}const Q=b.accumulate(C,u);e.startBatch("layout"),i.forEach((h,y)=>{const W=y%s,U=Math.floor(y/s),R=x[W],I=C[U];let Z=0,$=0,m=h.getSize();if(c){let z=R-2*n,B=I-2*r;const Y=m.height*(m.width?z/m.width:1),q=m.width*(m.height?B/m.height:1);I<Y?z=q:B=Y,m={width:z,height:B},h.setSize(m,t)}a&&(Z=(R-m.width)/2,$=(I-m.height)/2),h.position(j[W]+n+Z,Q[U]+r+$,t)}),e.stopBatch("layout")}var b;(function(l){function t(o,n){return o.reduce((r,a)=>Math.max(a==null?void 0:a.getSize()[n],r),0)}l.getMaxDim=t;function e(o,n,r){const a=[];for(let c=r*n,g=c+r;c<g;c+=1)o[c]&&a.push(o[c]);return a}l.getNodesInRow=e;function i(o,n,r){const a=[];for(let c=n,g=o.length;c<g;c+=r)o[c]&&a.push(o[c]);return a}l.getNodesInColumn=i;function s(o,n){return o.reduce((r,a,c)=>(r.push(r[c]+a),r),[n||0])}l.accumulate=s})(b||(b={}));const pt=`.x6-widget-dnd {
  position: absolute;
  top: -10000px;
  left: -10000px;
  z-index: 999999;
  display: none;
  cursor: move;
  opacity: 0.7;
  pointer-events: 'cursor';
}
.x6-widget-dnd.dragging {
  display: inline-block;
}
.x6-widget-dnd.dragging * {
  pointer-events: none !important;
}
.x6-widget-dnd .x6-graph {
  background: transparent;
  box-shadow: none;
}
.x6-widget-stencil {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
.x6-widget-stencil::after {
  position: absolute;
  top: 0;
  display: block;
  width: 100%;
  height: 20px;
  padding: 8px 0;
  line-height: 20px;
  text-align: center;
  opacity: 0;
  transition: top 0.1s linear, opacity 0.1s linear;
  content: ' ';
  pointer-events: none;
}
.x6-widget-stencil-content {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: auto;
  overflow-x: hidden;
  overflow-y: auto;
}
.x6-widget-stencil .x6-node [magnet]:not([magnet='passive']) {
  pointer-events: none;
}
.x6-widget-stencil-group {
  padding: 0;
  padding-bottom: 8px;
  overflow: hidden;
  user-select: none;
}
.x6-widget-stencil-group.collapsed {
  height: auto;
  padding-bottom: 0;
}
.x6-widget-stencil-group-title {
  position: relative;
  margin-top: 0;
  margin-bottom: 0;
  padding: 4px;
  cursor: pointer;
}
.x6-widget-stencil-title,
.x6-widget-stencil-group > .x6-widget-stencil-group-title {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  user-select: none;
}
.x6-widget-stencil .unmatched {
  opacity: 0.3;
}
.x6-widget-stencil .x6-node.unmatched {
  display: none;
}
.x6-widget-stencil-group.unmatched {
  display: none;
}
.x6-widget-stencil-search-text {
  position: relative;
  z-index: 1;
  box-sizing: border-box;
  width: 100%;
  height: 30px;
  max-height: 30px;
  line-height: 30px;
  outline: 0;
}
.x6-widget-stencil.not-found::after {
  opacity: 1;
  content: attr(data-not-found-text);
}
.x6-widget-stencil.not-found.searchable::after {
  top: 30px;
}
.x6-widget-stencil.not-found.searchable.collapsable::after {
  top: 50px;
}
.x6-widget-stencil {
  color: #333;
  background: #f5f5f5;
}
.x6-widget-stencil-content {
  position: absolute;
}
.x6-widget-stencil.collapsable > .x6-widget-stencil-content {
  top: 32px;
}
.x6-widget-stencil.searchable > .x6-widget-stencil-content {
  top: 80px;
}
.x6-widget-stencil.not-found::after {
  position: absolute;
}
.x6-widget-stencil.not-found.searchable.collapsable::after {
  top: 80px;
}
.x6-widget-stencil.not-found.searchable::after {
  top: 60px;
}
.x6-widget-stencil-group {
  height: auto;
  margin-bottom: 1px;
  padding: 0;
  transition: none;
}
.x6-widget-stencil-group .x6-graph {
  background: transparent;
  box-shadow: none;
}
.x6-widget-stencil-group.collapsed {
  height: auto;
  max-height: 31px;
}
.x6-widget-stencil-title,
.x6-widget-stencil-group > .x6-widget-stencil-group-title {
  position: relative;
  left: 0;
  box-sizing: border-box;
  width: 100%;
  height: 32px;
  padding: 0 5px 0 8px;
  color: #666;
  font-weight: 700;
  font-size: 12px;
  line-height: 32px;
  cursor: default;
  transition: all 0.3;
}
.x6-widget-stencil-title:hover,
.x6-widget-stencil-group > .x6-widget-stencil-group-title:hover {
  color: #444;
}
.x6-widget-stencil-title {
  background: #e9e9e9;
}
.x6-widget-stencil-group > .x6-widget-stencil-group-title {
  background: #ededed;
}
.x6-widget-stencil.collapsable > .x6-widget-stencil-title,
.x6-widget-stencil-group.collapsable > .x6-widget-stencil-group-title {
  padding-left: 32px;
  cursor: pointer;
}
.x6-widget-stencil.collapsable > .x6-widget-stencil-title::before,
.x6-widget-stencil-group.collapsable > .x6-widget-stencil-group-title::before {
  position: absolute;
  top: 6px;
  left: 8px;
  display: block;
  width: 18px;
  height: 18px;
  margin: 0;
  padding: 0;
  background-color: transparent;
  background-repeat: no-repeat;
  background-position: 0 0;
  border: none;
  content: ' ';
}
.x6-widget-stencil.collapsable > .x6-widget-stencil-title::before,
.x6-widget-stencil-group.collapsable > .x6-widget-stencil-group-title::before {
  background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJub256ZXJvIj48cGF0aCBkPSJNOS4zNzUuNUM0LjY4Ny41Ljg3NSA0LjMxMy44NzUgOWMwIDQuNjg4IDMuODEyIDguNSA4LjUgOC41IDQuNjg3IDAgOC41LTMuODEyIDguNS04LjUgMC00LjY4Ny0zLjgxMy04LjUtOC41LTguNXptMCAxNS44ODZDNS4zMDMgMTYuMzg2IDEuOTkgMTMuMDcyIDEuOTkgOXMzLjMxMi03LjM4NSA3LjM4NS03LjM4NVMxNi43NiA0LjkyOCAxNi43NiA5YzAgNC4wNzItMy4zMTMgNy4zODYtNy4zODUgNy4zODZ6Ii8+PHBhdGggZD0iTTEyLjc1MyA4LjQ0M0g1Ljk5N2EuNTU4LjU1OCAwIDAwMCAxLjExNmg2Ljc1NmEuNTU4LjU1OCAwIDAwMC0xLjExNnoiLz48L2c+PC9zdmc+');
  opacity: 0.4;
  transition: all 0.3s;
}
.x6-widget-stencil.collapsable > .x6-widget-stencil-title:hover::before,
.x6-widget-stencil-group.collapsable > .x6-widget-stencil-group-title:hover::before {
  opacity: 0.6;
}
.x6-widget-stencil.collapsable.collapsed > .x6-widget-stencil-title::before,
.x6-widget-stencil-group.collapsable.collapsed > .x6-widget-stencil-group-title::before {
  background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJub256ZXJvIj48cGF0aCBkPSJNOS4zNzUuNUM0LjY4Ny41Ljg3NSA0LjMxMy44NzUgOWMwIDQuNjg4IDMuODEyIDguNSA4LjUgOC41IDQuNjg3IDAgOC41LTMuODEyIDguNS04LjUgMC00LjY4Ny0zLjgxMy04LjUtOC41LTguNXptMCAxNS44ODZDNS4zMDMgMTYuMzg2IDEuOTkgMTMuMDcyIDEuOTkgOXMzLjMxMi03LjM4NSA3LjM4NS03LjM4NVMxNi43NiA0LjkyOCAxNi43NiA5YzAgNC4wNzItMy4zMTMgNy4zODYtNy4zODUgNy4zODZ6Ii8+PHBhdGggZD0iTTEyLjc1MyA4LjQ0M0g1Ljk5N2EuNTU4LjU1OCAwIDAwMCAxLjExNmg2Ljc1NmEuNTU4LjU1OCAwIDAwMC0xLjExNnoiLz48cGF0aCBkPSJNOC44MTcgNS42MjN2Ni43NTZhLjU1OC41NTggMCAwMDEuMTE2IDBWNS42MjNhLjU1OC41NTggMCAxMC0xLjExNiAweiIvPjwvZz48L3N2Zz4=');
  opacity: 0.4;
}
.x6-widget-stencil.collapsable.collapsed > .x6-widget-stencil-title:hover::before,
.x6-widget-stencil-group.collapsable.collapsed > .x6-widget-stencil-group-title:hover::before {
  opacity: 0.6;
}
.x6-widget-stencil input[type='search'] {
  appearance: textfield;
}
.x6-widget-stencil input[type='search']::-webkit-search-cancel-button,
.x6-widget-stencil input[type='search']::-webkit-search-decoration {
  appearance: none;
}
.x6-widget-stencil-search-text {
  display: block;
  width: 90%;
  margin: 8px 5%;
  padding-left: 8px;
  color: #333;
  background: #fff;
  border: 1px solid #e9e9e9;
  border-radius: 12px;
  outline: 0;
}
.x6-widget-stencil-search-text:focus {
  outline: 0;
}
.x6-widget-stencil::after {
  color: #808080;
  font-weight: 600;
  font-size: 12px;
  background: 0 0;
}
`;var dt=globalThis&&globalThis.__decorate||function(l,t,e,i){var s=arguments.length,o=s<3?t:i===null?i=Object.getOwnPropertyDescriptor(t,e):i,n;if(typeof Reflect=="object"&&typeof Reflect.decorate=="function")o=Reflect.decorate(l,t,e,i);else for(var r=l.length-1;r>=0;r--)(n=l[r])&&(o=(s<3?n(o):s>3?n(t,e,o):n(t,e))||o);return s>3&&o&&Object.defineProperty(t,e,o),o};class M extends A{get targetScroller(){return this.options.target.getPlugin("scroller")}get targetGraph(){return this.options.target}get targetModel(){return this.targetGraph.model}constructor(t={}){super(),this.name="stencil",J(this.name,pt),this.graphs={},this.groups={},this.options=Object.assign(Object.assign({},M.defaultOptions),t),this.init()}init(){this.dnd=new w(this.options),this.onSearch=lt(this.onSearch,200),this.initContainer(),this.initSearch(),this.initContent(),this.initGroups(),this.setTitle(),this.startListening()}load(t,e){return Array.isArray(t)?this.loadGroup(t,e):this.options.groups&&Object.keys(this.options.groups).forEach(i=>{t[i]&&this.loadGroup(t[i],i)}),this}unload(t,e){return Array.isArray(t)?this.loadGroup(t,e,!0):this.options.groups&&Object.keys(this.options.groups).forEach(i=>{t[i]&&this.loadGroup(t[i],i,!0)}),this}toggleGroup(t){return this.isGroupCollapsed(t)?this.expandGroup(t):this.collapseGroup(t),this}collapseGroup(t){if(this.isGroupCollapsable(t)){const e=this.groups[t];e&&!this.isGroupCollapsed(t)&&(this.trigger("group:collapse",{name:t}),p(e,"collapsed"))}return this}expandGroup(t){if(this.isGroupCollapsable(t)){const e=this.groups[t];e&&this.isGroupCollapsed(t)&&(this.trigger("group:expand",{name:t}),v(e,"collapsed"))}return this}isGroupCollapsable(t){const e=this.groups[t];return L(e,"collapsable")}isGroupCollapsed(t){const e=this.groups[t];return e&&L(e,"collapsed")}collapseGroups(){return Object.keys(this.groups).forEach(t=>this.collapseGroup(t)),this}expandGroups(){return Object.keys(this.groups).forEach(t=>this.expandGroup(t)),this}resizeGroup(t,e){const i=this.graphs[t];return i&&i.resize(e.width,e.height),this}addGroup(t){const e=Array.isArray(t)?t:[t];this.options.groups?this.options.groups.push(...e):this.options.groups=e,e.forEach(i=>this.initGroup(i))}removeGroup(t){const e=Array.isArray(t)?t:[t];this.options.groups&&(this.options.groups=this.options.groups.filter(i=>!e.includes(i.name)),e.forEach(i=>{this.graphs[i].dispose(),delete this.graphs[i];const o=this.groups[i];V(o),delete this.groups[i]}))}initContainer(){this.container=document.createElement("div"),p(this.container,this.prefixClassName(d.base)),T(this.container,"data-not-found-text",this.options.notFoundText||"No matches found")}initContent(){this.content=document.createElement("div"),p(this.content,this.prefixClassName(d.content)),S(this.content,this.container)}initSearch(){this.options.search&&(p(this.container,"searchable"),N(this.container,this.renderSearch()))}initGroup(t){const e=this.options.stencilGraphOptions||{},i=document.createElement("div");p(i,this.prefixClassName(d.group)),T(i,"data-name",t.name),(t.collapsable==null&&this.options.collapsable||t.collapsable!==!1)&&p(i,"collapsable"),G(i,"collapsed",t.collapsed===!0);const s=document.createElement("h3");p(s,this.prefixClassName(d.groupTitle)),s.innerHTML=t.title||t.name;const o=document.createElement("div");p(o,this.prefixClassName(d.groupContent));const n=t.graphOptions,r=new H(Object.assign(Object.assign(Object.assign({},e),n),{container:document.createElement("div"),model:e.model||new O,width:t.graphWidth||this.options.stencilGraphWidth,height:t.graphHeight||this.options.stencilGraphHeight,interacting:!1,preventDefaultBlankAction:!1}));N(o,r.container),N(i,[s,o]),S(i,this.content),this.groups[t.name]=i,this.graphs[t.name]=r}initGroups(){if(this.clearGroups(),this.setCollapsableState(),this.options.groups&&this.options.groups.length)this.options.groups.forEach(t=>{this.initGroup(t)});else{const t=this.options.stencilGraphOptions||{},e=new H(Object.assign(Object.assign({},t),{container:document.createElement("div"),model:t.model||new O,width:this.options.stencilGraphWidth,height:this.options.stencilGraphHeight,interacting:!1,preventDefaultBlankAction:!1}));N(this.content,e.container),this.graphs[E.defaultGroupName]=e}}setCollapsableState(){this.options.collapsable=this.options.collapsable&&this.options.groups&&this.options.groups.some(t=>t.collapsable!==!1),this.options.collapsable?(p(this.container,"collapsable"),this.options.groups&&this.options.groups.every(e=>e.collapsed||e.collapsable===!1)?p(this.container,"collapsed"):v(this.container,"collapsed")):v(this.container,"collapsable")}setTitle(){const t=document.createElement("div");p(t,this.prefixClassName(d.title)),t.innerHTML=this.options.title,S(t,this.container)}renderSearch(){const t=document.createElement("div");p(t,this.prefixClassName(d.search));const e=document.createElement("input");return T(e,{type:"search",placeholder:this.options.placeholder||"Search"}),p(e,this.prefixClassName(d.searchText)),N(t,e),t}startListening(){const t=this.prefixClassName(d.title),e=this.prefixClassName(d.searchText),i=this.prefixClassName(d.groupTitle);this.delegateEvents({[`click .${t}`]:"onTitleClick",[`touchstart .${t}`]:"onTitleClick",[`click .${i}`]:"onGroupTitleClick",[`touchstart .${i}`]:"onGroupTitleClick",[`input .${e}`]:"onSearch",[`focusin .${e}`]:"onSearchFocusIn",[`focusout .${e}`]:"onSearchFocusOut"}),Object.keys(this.graphs).forEach(s=>{this.graphs[s].on("cell:mousedown",this.onDragStart,this)})}stopListening(){this.undelegateEvents(),Object.keys(this.graphs).forEach(t=>{this.graphs[t].off("cell:mousedown",this.onDragStart,this)})}loadGroup(t,e,i){const s=this.getModel(e);if(s){const a=t.map(c=>X.isNode(c)?c:X.create(c));i===!0?s.removeCells(a):s.resetCells(a)}const o=this.getGroup(e);let n=this.options.stencilGraphHeight;o&&o.graphHeight!=null&&(n=o.graphHeight);const r=o&&o.layout||this.options.layout;if(r&&s&&P(r,this,s,o),!n){const a=this.getGraph(e);a.fitToContent({minWidth:a.options.width,gridHeight:1,padding:o&&o.graphPadding||this.options.stencilGraphPadding||10})}return this}onDragStart(t){const{e,node:i}=t,s=this.getGroupByNode(i);s&&s.nodeMovable===!1||this.dnd.start(i,e)}filter(t,e){const i=Object.keys(this.graphs).reduce((s,o)=>{const n=this.graphs[o],r=o===E.defaultGroupName?null:o,a=n.model.getNodes().filter(x=>{let f=!1;typeof e=="function"?f=P(e,this,x,t,r,this):typeof e=="boolean"?f=e:f=this.isCellMatched(x,t,e,t.toLowerCase()!==t);const j=n.renderer.findViewByCell(x);return j&&G(j.container,"unmatched",!f),f}),c=a.length>0,g=this.options,u=new O;return u.resetCells(a),g.layout&&P(g.layout,this,u,this.getGroup(o)),this.groups[o]&&G(this.groups[o],"unmatched",!c),n.fitToContent({gridWidth:1,gridHeight:1,padding:g.stencilGraphPadding||10}),s||c},!1);G(this.container,"not-found",!i)}isCellMatched(t,e,i,s){return e&&i?Object.keys(i).some(o=>{if(o==="*"||t.shape===o){const n=i[o];return typeof n=="boolean"?n:(Array.isArray(n)?n:[n]).some(a=>{let c=t.getPropByPath(a);return c!=null?(c=`${c}`,s||(c=c.toLowerCase()),c.indexOf(e)>=0):!1})}return!1}):!0}onSearch(t){this.filter(t.target.value,this.options.search)}onSearchFocusIn(){p(this.container,"is-focused")}onSearchFocusOut(){v(this.container,"is-focused")}onTitleClick(){this.options.collapsable&&(G(this.container,"collapsed"),L(this.container,"collapsed")?this.collapseGroups():this.expandGroups())}onGroupTitleClick(t){const e=t.target.closest(`.${this.prefixClassName(d.group)}`);e&&this.toggleGroup(T(e,"data-name")||"");const i=Object.keys(this.groups).every(s=>{const o=this.getGroup(s),n=this.groups[s];return o&&o.collapsable===!1||L(n,"collapsed")});G(this.container,"collapsed",i)}getModel(t){const e=this.getGraph(t);return e?e.model:null}getGraph(t){return this.graphs[t||E.defaultGroupName]}getGroup(t){const e=this.options.groups;return t!=null&&e&&e.length?e.find(i=>i.name===t):null}getGroupByNode(t){const e=this.options.groups;return e?e.find(i=>{const s=this.getModel(i.name);return s?s.has(t.id):!1}):null}clearGroups(){Object.keys(this.graphs).forEach(t=>{this.graphs[t].dispose()}),Object.keys(this.groups).forEach(t=>{const e=this.groups[t];V(e)}),this.graphs={},this.groups={}}onRemove(){this.clearGroups(),this.dnd.remove(),this.stopListening(),this.undelegateDocumentEvents()}dispose(){this.remove(),_(this.name)}}dt([A.dispose()],M.prototype,"dispose",null);(function(l){l.defaultOptions=Object.assign({stencilGraphWidth:200,stencilGraphHeight:800,title:"Stencil",collapsable:!1,placeholder:"Search",notFoundText:"No matches found",layout(t,e){const i={columnWidth:this.options.stencilGraphWidth/2-10,columns:2,rowHeight:80,resizeToFit:!1,dx:10,dy:10};ht(t,Object.assign(Object.assign(Object.assign({},i),this.options.layoutOptions),e?e.layoutOptions:{}))}},w.defaults)})(M||(M={}));var d;(function(l){l.base="widget-stencil",l.title=`${l.base}-title`,l.search=`${l.base}-search`,l.searchText=`${l.search}-text`,l.content=`${l.base}-content`,l.group=`${l.base}-group`,l.groupTitle=`${l.group}-title`,l.groupContent=`${l.group}-content`})(d||(d={}));var E;(function(l){l.defaultGroupName="__default__"})(E||(E={}));function ut(l,t,e,i){const s=[];for(const o of e){const n=l.createNode(o);s.push(n)}t.load(s,i)}function Ct(l,t){const e=[{title:"\u7528\u6237\u8282\u70B9",name:"userNode",list:K},{title:"\u8DEF\u7531\u8282\u70B9",name:"routeNode",list:tt},{title:"\u6284\u9001\u8282\u70B9",name:"ccNode",list:et},{title:"\u5B50\u6D41\u7A0B\u8282\u70B9",name:"subFlowNode",list:it},{title:"\u6807\u7B7E",name:"labelNode",list:ot}],i=new M({title:"\u6D41\u7A0B\u8BBE\u8BA1\u5668",target:l,stencilGraphWidth:180,stencilGraphHeight:0,collapsable:!1,groups:e.map(({title:s,name:o})=>({title:s,name:o,collapsable:!1})),layoutOptions:{columns:1,columnWidth:180,rowHeight:55}});t==null||t.appendChild(i.container);for(const s of e)ut(l,i,s.list,s.name)}export{Ct as initStencil};
