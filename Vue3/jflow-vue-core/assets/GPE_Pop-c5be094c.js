var n=Object.defineProperty;var a=(p,o,F)=>o in p?n(p,o,{enumerable:!0,configurable:!0,writable:!0,value:F}):p[o]=F;var r=(p,o,F)=>(a(p,typeof o!="symbol"?o+"":o,F),F);var A=(p,o,F)=>new Promise((B,E)=>{var u=e=>{try{i(F.next(e))}catch(C){E(C)}},t=e=>{try{i(F.throw(e))}catch(C){E(C)}},i=e=>e.done?B(e.value):Promise.resolve(e.value).then(u,t);i((F=F.apply(p,o)).next())});import{b as s,P as m}from"./MapExt-db8cd7f3.js";import{PopGroupList as D}from"./PopGroupList-dc2033b2.js";import{PopSelfUrl as c}from"./PopSelfUrl-6acfd4e6.js";import{PopTable as g}from"./PopTable-98c10c32.js";import{PopTree as P}from"./PopTree-be84886a.js";import{PopTreeEns as l}from"./PopTreeEns-85cec969.js";import{PopTreeEnsSFTable as h}from"./PopTreeEnsSFTable-445abaa2.js";import{GloComm as d}from"./GloComm-7cfbdfd9.js";import{PageBaseGroupEdit as L}from"./PageBaseGroupEdit-202e8e85.js";import{GPNReturnObj as f,GPNReturnType as T}from"./PageBaseGroupNew-ee20c033.js";import{MapAttr as M}from"./MapAttr-cb594d82.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./Events-141c34ea.js";class su extends L{constructor(){super("GPE_Pop");r(this,"PopTableSearch",` 

  #### \u5E2E\u52A9
   - \u6570\u636E\u662F\u4EE5\u8868\u683C\u7684\u6A21\u5F0F\u5C55\u73B0\uFF0C\u53EF\u4EE5\u8BBE\u7F6E\u67E5\u8BE2\u6761\u4EF6, \u6BD4\u5982\u9009\u62E9\u5355\u636E\u3001\u4EA7\u54C1\u3001\u6240\u5728\u73ED\u7EA7\u3002
   - \u9002\u5E94\u6570\u636E\u91CF\u8F83\u5927\uFF0C\u9700\u8981\u641C\u7D22\u5B8C\u6210\u3002
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableSearch2.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u8FD0\u884C\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableSearch.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);r(this,"PopSelfUrl",` 
  #### \u5E2E\u52A9
   - \u5F53ccflow\u63D0\u4F9B\u7684\u6A21\u5F0F\u4E0D\u80FD\u6EE1\u8DB3\u60A8\u7684\u8981\u6C42\u7684\u65F6\u5019\uFF0C\u8FD9\u4E2A\u65B9\u6848\u5C31\u662F\u7EC8\u6781\u89E3\u51B3\u529E\u6CD5\u3002
   - \u60A8\u81EA\u5DF1\u5B9A\u4E49\u4E00\u4E2A\u9875\u9762\uFF0C\u914D\u7F6E\u5230\u7CFB\u7EDF\u4E2D\u53BB. 
   - \u8FD4\u56DE\u7684\u6570\u636E\uFF0C\u9700\u8981\u6EE1\u8DB3ccflow\u7684\u89C4\u8303,\u8BF7\u53C2\u8003\u793A\u4F8B. /DataUser/

  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/Url2.png "\u5C4F\u5E55\u622A\u56FE.png")


  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/Url.png "\u5C4F\u5E55\u622A\u56FE.png")

  
  `);r(this,"Desc1",` 

  #### \u5E2E\u52A9
   - \u5F39\u7A97\uFF08Pop\uFF09\u8FD4\u56DE\u503C\uFF0C\u5C31\u662F\u53CC\u51FB\u4E00\u4E2A\u6587\u672C\u6846\u5F39\u51FA\u4E00\u4E2A\u7A97\u4F53\uFF0C\u9009\u62E9\u7A97\u4F53\u7684\u5185\u5BB9\u70B9\u51FB"\u786E\u5B9A"\u6309\u94AE\u628A\u9009\u62E9\u7684\u503C\u653E\u5165\u5230\u8BE5\u6587\u672C\u6846\u91CC\u3002
   - \u5F53\u653E\u5165\u6587\u672C\u6846\u7684\u503C\u4EE5\u540E\uFF0C\u6FC0\u6D3B\u4E00\u4E2A\u65B9\u6CD5\uFF0C\u628A\u5176\u4ED6\u7684\u503C\u586B\u5145\u5230\u5176\u4ED6\u63A7\u4EF6\u91CC\uFF0C\u6211\u4EEC\u628A\u8FD9\u79CD\u884C\u4E3A\u79F0\u4E3A\u586B\u5145\u8BBE\u7F6E\u3002
   - \u4E3A\u4E86\u6EE1\u8DB3\u4E0D\u540C\u6A21\u5F0F\u4E0B\u7684\u586B\u5145\u7A97\u4F53\u5185\u5BB9\u7684\u663E\u793A\uFF0C\u6211\u4EEC\u5206\u4E3A\u5F88\u591A\u79CD\u5F39\u7A97\u8FD4\u56DE\u503C\uFF0C\u60A8\u53EF\u4EE5\u6839\u636E\u4E0D\u540C\u7684\u573A\u666F\u8BBE\u7F6E\u4E0D\u540C\u7684\u6A21\u5F0F\u3002
 #### \u5E94\u7528\u573A\u666F
  - \u5BF9\u4E00\u4E2A\u6587\u672C\u6846\u8F93\u5165\u7684\u6570\u636E\u9700\u8981\u83B7\u53D6\u5916\u90E8\u6570\u636E\u7684\u65F6\u5019.
  - \u6BD4\u5982\uFF1A\u9009\u62E9\u53C2\u4E0E\u4EBA\u3001\u9009\u62E9\u4EA7\u54C1\u3001\u9009\u62E9\u5BA2\u6237\u3001\u9009\u62E9...
  - \u8F93\u5165\u7684\u6570\u636E\u9700\u8981\u9009\u62E9\u7684\u65F6\u5019.
  - \u6570\u636E\u6E90\u83B7\u53D6\u65B9\u5F0F\uFF1A\u6267\u884CSQL\u3001\u6267\u884CURL\u3001\u6267\u884C\u81EA\u5B9A\u4E49\u7684function.
  - \u5F39\u7A97\u7C7B\u578B\uFF1A\u6811\u5E72\u53F6\u5B50\u6A21\u5F0F \u3001\u6811\u5E72\u6A21\u5F0F\u3001 \u5206\u7EC4\u6A21\u5F0F\u3001 \u5217\u8868\u6A21\u5F0F\u3001\u8868\u683C\u6A21\u5F0F
  #### \u6548\u679C\u56FE
   - \u6811\u5E72\u53F6\u5B50\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/BranchesAndLeaf.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u6811\u5E72\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/Branches.png "\u6811\u7ED3\u6784\u6548\u679C\u56FE.png")
   - \u5206\u7EC4\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/GroupList.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - \u5217\u8868\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableList.png "\u5C4F\u5E55\u622A\u56FE.png")
   - \u8868\u683C\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableSearch.png "\u5C4F\u5E55\u622A\u56FE.png")

   `);r(this,"PopBranchesAndLeaf",` 
  #### \u8BF4\u660E
   - \u8BE5\u6A21\u5F0F\u4E0B\u6700\u7ECF\u5178\u7684\u5C31\u662F\u90E8\u95E8\u6811\u4E0E\u4EBA\u5458\u7684\u7ED3\u6784, \u90E8\u95E8\u5C31\u662F\u6811\u5E72\uFF0C\u4EBA\u5458\u5C31\u662F\u53F6\u5B50\uFF0C\u6211\u4EEC\u628A\u8FD9\u6837\u7684\u6A21\u5F0F\u6210\u4E3A\u6811\u5E72\u53F6\u5B50\u6A21\u5F0F\u3002
   - \u4E0E\u6B64\u76F8\u7C7B\u4F3C\u7684\u6709\uFF1A \u6D41\u7A0B\u6811\u4E0E\u6D41\u7A0B\u7684\u5173\u7CFB\uFF0C \u8868\u5355\u5E93\u4E0E\u8868\u5355\u5173\u7CFB.
   - \u901A\u8FC7\u5B9A\u4E49\u7684\u6570\u636E\u6E90\uFF0C\u5C31\u53EF\u4EE5\u8F7B\u677E\u5B9E\u73B0\u8FD9\u6837\u7684Pop\u7ED3\u6784\u3002
  
   #### \u914D\u7F6E\u56FE

   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/BranchesAndLeaf2.png "\u5C4F\u5E55\u622A\u56FE.png")    
  
   #### \u6548\u679C\u56FE

   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/BranchesAndLeaf.png "\u5C4F\u5E55\u622A\u56FE.png")
 
 
   `);r(this,"PopBranches",` 

  #### \u8BF4\u660E
   - \u5F39\u7A97\u7684\u6570\u636E\u5C55\u73B0\u4E3A\u6811\u7ED3\u6784\uFF0C\u6BD4\u5982\uFF1A\u90E8\u95E8\u3001\u7C7B\u522B\u7B49\u7B49\u3002
   - \u6570\u636E\u7ED3\u6784\u4E3A\u5E38\u89C1\u901A\u7528\u7684\u7F16\u53F7\u3001\u540D\u79F0\u3001\u7236\u8282\u70B9\u7F16\u53F7\u89C4\u5219\u3002
   - \u70B9\u51FB\u4E0A\u65B9\u6309\u94AE\u53EF\u4EE5\u8BBE\u7F6E\u5C5E\u6027\u3002
   
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/Branches2.png "\u5C4F\u5E55\u622A\u56FE.png") 
   #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/Branches.png "\u6811\u7ED3\u6784\u6548\u679C\u56FE.png")


   `);r(this,"PopTableList",` 

  #### \u5E2E\u52A9
   - \u5355\u5B9E\u4F53\u5E73\u94FA\uFF0C\u5C31\u662F\u5BF9\u6570\u636E\u6E90\u8FDB\u884C\u7B80\u5355\u7684\u5BAB\u683C\u5217\u8868\u5C55\u793A\uFF0C\u65B9\u4FBF\u7528\u6237\u9009\u62E9\u3002
   - \u662F\u6700\u7B80\u5355\u7684\u4E00\u79CD\u5F39\u7A97\u6570\u636E\u5C55\u73B0\u6A21\u5F0F\uFF0C\u9002\u7528\u4E8E\u6570\u636E\u91CF\u8F83\u5C0F\uFF0C\u6CA1\u6709\u6570\u636E\u5C55\u793A\u5206\u7EC4\u7684\u9700\u8981\u3002
  
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableList2.png "\u5C4F\u5E55\u622A\u56FE.png")  

  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/TableList.png "\u5C4F\u5E55\u622A\u56FE.png")


   `);r(this,"PopGroupList",` 

  #### \u5E2E\u52A9
   - \u5206\u7EC4\u5217\u8868\u5E73\u94FA,\u5C31\u662F\u5BF9\u5B9E\u4F53\u8FDB\u884C\u5206\u7EC4\u5C55\u793A. \u4F8B\u5982: \u4EA7\u54C1\u7C7B\u522B\u4E0E\u4EA7\u54C1\u3002 \u89D2\u8272\u7C7B\u578B\u4E0E\u89D2\u8272\u3002
   - \u4EA7\u54C1\u7C7B\u522B\u89D2\u8272\u7C7B\u578B\u5C31\u662F\u5206\u7EC4\u6570\u636E\u6E90\uFF0C\u4EA7\u54C1\u4E0E\u89D2\u8272\u5C31\u662F\u5B9E\u4F53\u6570\u636E\u6E90\u3002
   - \u5B9E\u4F53\u6570\u636E\u6E90\u8981\u6C42\u8FD4\u56DE\u4E09\u4E2A\u5217\uFF0C\u6700\u540E\u4E00\u5217\u5C31\u662F\u4E0E\u5206\u7EC4\u6570\u636E\u6E90\u5BF9\u5E94\u7684\u5916\u952E\u5217\u3002
   - \u8BF7\u53C2\u8003\u914D\u7F6E\u56FE\u3002
  
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/GroupList2.png "\u5C4F\u5E55\u622A\u56FE.png") 

  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/Pop/Img/GroupList.png "\u5C4F\u5E55\u622A\u56FE.png")
  
    `);this.PageTitle="\u5F39\u7A97\u8FD4\u56DE\u503C"}AfterSave(F,B){return A(this,null,function*(){if(F!="None"){let E=this.GetRequestVal("PKVal");E.endsWith("_Pop")&&(E=E.replace("_Pop",""));const u=new M,t=E;u.setPKVal(E+"T"),(yield u.RetrieveFromDBSources())==0&&(u.setPKVal(t),yield u.RetrieveFromDBSources(),u.MyPK=u.MyPK+"T",u.KeyOfEn=u.KeyOfEn+"T",u.Name=u.Name+"T",u.UIVisible=!1,u.UIIsEnable=!1,yield u.Insert())}if(F==B)throw new Error("Method not implemented.")})}BtnClick(F,B,E){return A(this,null,function*(){var u;if(E==="\u843D\u503C\u586B\u5145"||E==="\u586B\u5145"){const t=d.UrlEn("TS.MapExt.FullData",(u=this.entity)==null?void 0:u.MyPK);return new f(T.OpenUrlByDrawer75,t)}})}Init(){return A(this,null,function*(){this.entity=new s,this.KeyOfEn="DoWay",this.Btns=[{pageNo:"PopBranchesAndLeaf",list:["\u586B\u5145"]},{pageNo:"PopBranches",list:["\u586B\u5145"]},{pageNo:"PopGroupList",list:["\u586B\u5145"]},{pageNo:"PopTableList",list:["\u586B\u5145"]},{pageNo:"PopTableSearch",list:["\u586B\u5145"]},{pageNo:"PopSelfUrl",list:["\u586B\u5145"]},{pageNo:"PopBranches",list:["\u586B\u5145"]}],yield this.entity.InitDataForMapAttr("Pop",this.GetRequestVal("PKVal")),this.AddGroup("A","\u6811\u5F62\u7ED3\u6784"),this.Blank("None","\u65E0,\u4E0D\u8BBE\u7F6E(\u9ED8\u8BA4).",this.Desc1),this.AddEntity("PopBranchesAndLeaf","\u6811\u5E72\u53F6\u5B50\u6A21\u5F0F",new l,this.PopBranchesAndLeaf),this.AddEntity("PopBranchesAndLeafSFTable","\u6811\u5E72\u53F6\u5B50\u6A21\u5F0F(\u7ED1\u5B9A\u5B57\u5178\u8868)",new h,this.PopBranchesAndLeaf),this.AddEntity("PopBranches","\u6811\u5E72\u6A21\u5F0F",new P,this.PopBranches),this.AddGroup("B","\u5206\u7EC4\u6A21\u5F0F"),this.AddEntity("PopGroupList","\u5206\u7EC4\u5217\u8868\u5E73\u94FA",new D,this.PopGroupList),this.AddEntity("PopTableList","\u5355\u5B9E\u4F53\u5E73\u94FA",new m,this.PopTableList),this.AddGroup("C","\u5176\u4ED6\u6A21\u5F0F"),this.AddEntity("PopTableSearch","\u8868\u683C\u6761\u4EF6\u67E5\u8BE2",new g,this.PopTableSearch),this.AddEntity("PopSelfUrl","\u81EA\u5B9A\u4E49URL",new c,this.PopSelfUrl)})}}export{su as GPE_Pop};
