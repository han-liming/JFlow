var m=Object.defineProperty;var e=(i,t,u)=>t in i?m(i,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):i[t]=u;var o=(i,t,u)=>(e(i,typeof t!="symbol"?t+"":t,u),u);import{D}from"./DataType-33901a1c.js";import{PageBaseGroupEdit as s}from"./PageBaseGroupEdit-202e8e85.js";import{Flow as n}from"./Flow-6121039a.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./BSEntity-840a884b.js";class b extends s{constructor(){super("GPE_BatchStart");o(this,"Desc0",` 
  #### \u5E2E\u52A9
   - \u4E0D\u542F\u52A8\uFF1A\u4E0D\u542F\u52A8\u6D41\u7A0B\u7684\u6279\u91CF\u53D1\u8D77\u3002
   - \u542F\u7528\u6279\u91CF\u53D1\u8D77:\u4E00\u6B21\u6279\u91CF\u53D1\u8D77\u591A\u4E2A\u6D41\u7A0B\u3002
 
     
    `);o(this,"Desc1",` 
  #### \u5E2E\u52A9
   
    
    `);this.PageTitle="\u6279\u91CF\u53D1\u8D77"}Init(){this.entity=new n,this.KeyOfEn="BatchListCount",this.AddGroup("A","\u6279\u91CF\u53D1\u8D77"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.SingleTB("1","\u542F\u7528\u6279\u91CF\u53D1\u8D77","BatchListCount",this.Desc1,"\u4E00\u6B21\u53D1\u8D77\u591A\u5C11\u6761\u8BB0\u5F55",D.AppString)}AfterSave(u,r){if(u==r)throw new Error("Method not implemented.")}BtnClick(u,r,p){if(u==r||u===p)throw new Error("Method not implemented.")}}export{b as GPE_BatchStart};
