var C=Object.defineProperty;var i=(E,F,u)=>F in E?C(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var B=(E,F,u)=>(i(E,typeof F!="symbol"?F+"":F,u),u);import{FrmNode as m,FrmNodeAttr as n}from"./FrmNode-4376932d.js";import{GloComm as e}from"./GloComm-7cfbdfd9.js";import{PageBaseGroupEdit as o}from"./PageBaseGroupEdit-202e8e85.js";import{GPNReturnObj as p,GPNReturnType as D}from"./PageBaseGroupNew-ee20c033.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class Y extends o{constructor(){super("GPE_FrmCtrlSln");B(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u89C6\u9891\u6559\u7A0B: https://drive.weixin.qq.com/s?k=AOsAZQczAAYgmfqbFS
   - \u5B9A\u4E49: \u8868\u5355\u6743\u9650\u65B9\u6848\u5C31\u662F\uFF0C\u4E00\u4E2A\u8868\u5355\u5728\u4E0D\u540C\u7684\u8282\u70B9\u4E0A\uFF0C\u9700\u8981\u63A7\u5236\u4E0D\u540C\u7684\u6743\u9650\uFF0C\u6211\u4EEC\u79F0\u4E3A\u8868\u5355\u6743\u9650\u65B9\u6848.
   - \u6BD4\u5982: \u5F00\u59CB\u8282\u70B9\u53EF\u7F16\u8F91\uFF0C\u7B2C2\u4E2A\u8282\u70B9\u53EA\u8BFB\uFF0C\u7B2C3\u4E2A\u8282\u70B9\u90E8\u95E8\u5B57\u6BB5\u53EF\u7F16\u8F91. \u8FD9\u6837\u7684\u573A\u666F\uFF0C\u53EF\u4EE5\u4F7F\u7528\u6743\u9650\u65B9\u6848\u6765\u5B9A\u4E49.
   #### \u9ED8\u8BA4\u65B9\u6848
   - \u6309\u7167\u5728\u8BBE\u8BA1\u8868\u5355\u65F6\u7684\u8868\u5355\u6743\u9650, \u5448\u73B0\u7ED9\u64CD\u4F5C\u8005.

  #### \u53C2\u8003\u6D41\u7A0B
   - \u6F14\u793A\u6D41\u7A0B: 014.\u7ACB\u9879\u5BA1\u6279\u6D41\u7A0B.
   - \u8BE5\u6D41\u7A0B\u7ED1\u5B9A\u591A\u4E2A\u8868\u5355,\u5728\u6BCF\u4E2A\u8282\u70B9\uFF0C\u7ED1\u5B9A\u7684\u8868\u5355\u6743\u9650\u4E0D\u540C\u3002
     1401\u8282\u70B9\uFF0C\u7ED1\u5B9A\u7684\u8868\u5355\u4E3A\u9ED8\u8BA4\u65B9\u6848\uFF0C\u53EF\u4EE5\u7F16\u8F91\u3002
     1402\u8282\u70B9\uFF0C\u7ED1\u5B9A\u7684\u8868\u5355\u4E3A\u53EA\u8BFB\u65B9\u6848\uFF0C\u63A5\u6536\u4EBA\u53EA\u53EF\u4EE5\u67E5\u770B\u8868\u5355\uFF0C\u4E0D\u53EF\u4EE5\u5BF9\u8868\u5355\u5B57\u6BB5\u5185\u5BB9\u8FDB\u884C\u7F16\u8F91\u3002
     1403\u8282\u70B9\uFF0C\u7ED1\u5B9A\u7684\u8868\u5355\u4E3A\u81EA\u5B9A\u4E49\u8868\u5355\u6743\u9650\uFF0C\u63A5\u6536\u4EBA\u53EF\u4EE5\u6839\u636E\u8BBE\u5B9A\u7684\u8868\u5355\u5143\u7D20\u5BF9\u8868\u5355\u8FDB\u884C\u7F16\u8F91\uFF0C\u5305\u62EC\u8868\u5355\u5185\u7684\u5B57\u6BB5\uFF0C\u4ECE\u8868\uFF0C\u9644\u4EF6\u7B49\u3002
     1404\u8282\u70B9\uFF0C\u7ED1\u5B9A\u7684\u8868\u5355\u4F4D\u4E3A\u53EA\u8BFB\u65B9\u6848\u3002
  #### \u914D\u7F6E\u56FE
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Sln5/Img/FrmCtrlSlnSetting1.png "\u5C4F\u5E55\u622A\u56FE.png") 
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Sln5/Img/FrmCtrlSlnSetting4.png "\u5C4F\u5E55\u622A\u56FE.png")     
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Sln5/Img/FrmCtrlSlnSetting2.png "\u5C4F\u5E55\u622A\u56FE.png")   
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Sln5/Img/FrmCtrlSlnSetting3.png "\u5C4F\u5E55\u622A\u56FE.png")   
    `);B(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u89C6\u9891\u6559\u7A0B: https://drive.weixin.qq.com/s?k=AOsAZQczAAYgmfqbFS
   - \u53EA\u8BFB\uFF1A\u8868\u5355\u5143\u7D20\u53EA\u6709\u53EA\u8BFB\u529F\u80FD\uFF0C\u4E0D\u80FD\u4FEE\u6539\u6570\u636E\u3002
   - \u573A\u666F: \u9879\u76EE\u7533\u62A5\u6D41\u7A0B\u4E2D\uFF0C\u7B2C1\u4E2A\u8282\u70B9\u586B\u5199\u7533\u62A5\u5355\u3001\u7B2C2\u4E2A\u8282\u70B9\u8BC4\u5BA1\u59D4\u5458\u8BC4\u5BA1\uFF0C\u5C31\u4E0D\u53EF\u4EE5\u8868\u5355,\u6211\u4EEC\u5C31\u8BBE\u7F6E\u53EA\u8BFB\u65B9\u6848.

      `);B(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u81EA\u5B9A\u4E49\uFF1A\u5BF9\u4E8E\u8868\u5355\u91CC\u7684\u6BCF\u4E2A\u5143\u7D20\uFF0C\u53EF\u4EE5\u8FDB\u884C\u4E2A\u6027\u5316\u7684\u8BBE\u7F6E\u3002
   - \u53EF\u4EE5\u81EA\u5B9A\u4E49\u7684\u5143\u7D20: \u57FA\u672C\u5B57\u6BB5\u3001\u7EC4\u4EF6\u3001\u4ECE\u8868\u3001\u4E00\u822C\u9644\u4EF6\u3001\u56FE\u7247\u9644\u4EF6.
   - 
  #### \u914D\u7F6E\u56FE
   -  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Sln5/Img/FrmCtrlSlnSetting.png "\u5C4F\u5E55\u622A\u56FE.png") 
      `);this.PageTitle="\u8868\u5355\u6743\u9650\u65B9\u6848"}Init(){this.entity=new m,this.KeyOfEn=n.FrmSln,this.AddGroup("A","\u8868\u5355\u6743\u9650\u65B9\u6848"),this.Blank("0","\u9ED8\u8BA4\u65B9\u6848",this.Desc0),this.Blank("1","\u53EA\u8BFB\u65B9\u6848",this.Desc1),this.Blank("2","\u81EA\u5B9A\u4E49\u8868\u5355\u5143\u7D20\u6743\u9650",this.Desc1),this.Btns=[{pageNo:"2",list:["\u8BBE\u7F6E\u6743\u9650"]}]}AfterSave(u,A){if(u==A)throw new Error("Method not implemented.")}BtnClick(u,A,t){if(u=="2"&&t==="\u8BBE\u7F6E\u6743\u9650"){const r=e.UrlEn("TS.AttrNode.FrmNodeCtrlSln",this.entity.PKVal);return new p(D.OpenUrlByDrawer75,r,"\u8BBE\u7F6E\u6743\u9650")}}}export{Y as GPE_FrmCtrlSln};
