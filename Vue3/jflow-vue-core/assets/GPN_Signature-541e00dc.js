var p=Object.defineProperty;var l=(r,e,u)=>e in r?p(r,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):r[e]=u;var s=(r,e,u)=>(l(r,typeof e!="symbol"?e+"":e,u),u);var n=(r,e,u)=>new Promise((a,B)=>{var E=F=>{try{t(u.next(F))}catch(o){B(o)}},i=F=>{try{t(u.throw(F))}catch(o){B(o)}},t=F=>F.done?a(F.value):Promise.resolve(F.value).then(E,i);t((u=u.apply(r,e)).next())});import{PageBaseGroupNew as C,GPNReturnObj as A,GPNReturnType as d}from"./PageBaseGroupNew-ee20c033.js";import{$ as m}from"./index-f4658ae7.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class x extends C{constructor(){super("GPN_Signature");s(this,"ImpLocal",`
  #### \u5E2E\u52A9 
   ##### \u4E0A\u4F20\u6A21\u677F
   - \u8BF7\u4E0A\u4F20\u60A8\u7684\u8868\u5355\u6A21\u677F.
   ##### \u9009\u62E9\u5BFC\u5165\u6A21\u5F0F
   - \u8BF7\u9009\u62E9\u5BFC\u5165\u6A21\u5F0F.
   - \u6309\u7167\u6A21\u7248\u7684\u8868\u5355\u7F16\u53F7\u5BFC\u51651\uFF1A\u5982\u679C\u8BE5\u7F16\u53F7\u5DF2\u7ECF\u5B58\u5728\u5C31\u63D0\u793A\u9519\u8BEF.
   - \u6309\u7167\u6A21\u7248\u7684\u8868\u5355\u7F16\u53F7\u5BFC\u51652\uFF1A\u5982\u679C\u8BE5\u7F16\u53F7\u5DF2\u7ECF\u5B58\u5728\u5C31\u76F4\u63A5\u8986\u76D6.
   - \u6309\u7167\u6A21\u7248\u7684\u8868\u5355\u7F16\u53F7\u5BFC\u51653\uFF1A\u5982\u679C\u8BE5\u7F16\u53F7\u5DF2\u7ECF\u5B58\u5728\u5C31\u589E\u52A0@WebUser.OrgNo(\u7EC4\u7EC7\u7F16\u53F7)\u5BFC\u5165.
        `);s(this,"ImpFrmID",`
  #### \u5E2E\u52A9
   - \u4ECE\u8868\u5355\u5E93\u5BFC\u5165
  `);this.PageTitle="\u5B57\u6BB5\u7B7E\u540D"}Init(){return n(this,null,function*(){this.AddGroup("A","\u5B57\u6BB5\u7B7E\u540D","icon-xxx"),this.AddBlank("0","\u56FE\u7247\u7B7E\u540D",this.HelpUn),this.AddBlank("1","\u624B\u5199\u7B7E\u540D",this.HelpUn),this.AddBlank("2","\u7535\u5B50\u7B7E\u7AE0",this.HelpUn)})}GenerSorts(){return n(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,a,B,E,i){return n(this,null,function*(){if(u==="ImpLocal.Way"){const t=new m("BP.WF.HttpHandler.WF_Admin_Template");t.AddFile(this.UploadFile),t.AddPara("RB_ImpType",B),t.AddPara("FrmSort",a);const F=yield t.DoMethodReturnString("ImpFrmLocal_Done");return F.includes("err@")?new A(d.Error,F):new A(d.Message,F)}})}}export{x as GPN_Signature};
