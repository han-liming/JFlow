var n=Object.defineProperty;var B=(E,F,u)=>F in E?n(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var e=(E,F,u)=>(B(E,typeof F!="symbol"?F+"":F,u),u);var m=(E,F,u)=>new Promise((t,i)=>{var a=r=>{try{p(u.next(r))}catch(o){i(o)}},s=r=>{try{p(u.throw(r))}catch(o){i(o)}},p=r=>r.done?t(r.value):Promise.resolve(r.value).then(a,s);p((u=u.apply(E,F)).next())});import{b as A,a as g}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as D}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class J extends D{constructor(){super("GPN_RegularExpression");e(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u5E94\u7528\u573A\u666F\uFF1A\u5728\u8BF7\u5047\u5355\u4E2D\uFF0C\u6839\u636E\u8BF7\u5047\u65E5\u671F\u4ECE\uFF0C\u8BF7\u5047\u65E5\u671F\u5230\u7684\u5DEE\u503C\uFF0C\u81EA\u52A8\u8BA1\u7B97\u8BF7\u5047\u5929\u6570\u3002

  #### \u914D\u7F6E\u56FE\u4F8B1
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysBiaodan.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u914D\u7F6E\u56FE\u4F8B2
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysBiaodan2.png "\u5C4F\u5E55\u622A\u56FE.png") 

  #### \u8FD0\u884C\u56FE\u4F8B
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysYanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);e(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u5BF9\u4E24\u4E2A\u65E5\u671F\u6C42\u5DEE\uFF0C\u65E5\u671F1-\u65E5\u671F2= \u5929\u6570.
   - \u5F97\u51FA\u7684\u503C\u662F\u4E00\u4E2A\u6574\u5F62\u7684\u6570\u503C\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u6839\u636E\u8BF7\u5047\u65E5\u671F\u4ECE\uFF0C\u8BF7\u5047\u65E5\u671F\u5230\u81EA\u52A8\u8BA1\u7B97\u8BF7\u5047\u5929\u6570.
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysYanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
  `);this.PageTitle="\u6B63\u5219\u8868\u8FBE\u5F0F"}BtnClick(u,t,i){throw new Error("Method not implemented.{pageID}")}Init(){this.entity=new A,this.KeyOfEn=g.DoWay,this.AddGroup("A","\u6B63\u5219\u8868\u8FBE\u5F0F"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc2)}AfterSave(u,t){return m(this,null,function*(){})}}export{J as GPN_RegularExpression};
