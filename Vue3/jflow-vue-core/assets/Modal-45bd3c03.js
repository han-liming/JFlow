import{E as W,r as z,bg as fe,b3 as V,P as Je,O as re,d as K,f as F,a8 as s,dO as le,k as Le,J as ye,p as A,f1 as Ge,aP as Qe,s as se,v as ke,bi as Ne}from"./index-f4658ae7.js";import{u as ue,a as ge}from"./use-config-816d55a6.js";import{k as me}from"./keysOf-5d5107c5.js";import{u as Ze}from"./use-rtl-889b67fe.js";import{k as ve,c as x,e as P,E as _e,f as k,g as v,m as He,n as eo,b as E,j as Ae}from"./light-0dfdc1ad.js";import{c as D}from"./create-key-bf4384d6.js";import{a as oo}from"./index-22809599.js";import{u as he}from"./use-css-vars-class-3ae3b4b3.js";import{r as T,c as L,a as we}from"./use-merged-state-66be05d7.js";import{N as De}from"./Close-c51bd8a8.js";import{b as to,N as Se}from"./Button-53926a3b.js";import{r as I}from"./render-ee8eb435.js";import{N as no}from"./Icon-e3cbad7d.js";import{I as ze,S as io,E as ro}from"./Success-7a2433de.js";import{W as lo}from"./Warning-d0098cab.js";import{e as We,m as so,d as ao,p as co,c as fo,z as uo,L as go}from"./Follower-3b5f0c65.js";import{g as mo,F as vo,k as ae}from"./Popover-ab55c8ff.js";import{w as ho}from"./warn-77f3ea30.js";import{a as ce,o as de,s as po,N as bo,f as Co,g as xo}from"./Scrollbar-35d51129.js";import{f as yo}from"./fade-in-scale-up.cssr-0b26e361.js";import{i as Ve,h as Ke}from"./on-fonts-ready-d897575d.js";import{i as ko}from"./use-is-mounted-a34b74be.js";import{i as wo}from"./Loading-fead3a83.js";import{e as So}from"./FocusDetector-05234541.js";const N=z(null);function Pe(e){if(e.clientX>0||e.clientY>0)N.value={x:e.clientX,y:e.clientY};else{const{target:t}=e;if(t instanceof Element){const{left:n,top:o,width:a,height:f}=t.getBoundingClientRect();n>0||o>0?N.value={x:n+a/2,y:o+f/2}:N.value={x:0,y:0}}else N.value=null}}let _=0,Re=!0;function zo(){if(!Ve)return W(z(null));_===0&&ce("click",document,Pe,!0);const e=()=>{_+=1};return Re&&(Re=Ke())?(fe(e),V(()=>{_-=1,_===0&&de("click",document,Pe,!0)})):e(),W(N)}const Po=z(void 0);let H=0;function Be(){Po.value=Date.now()}let $e=!0;function Ro(e){if(!Ve)return W(z(!1));const t=z(!1);let n=null;function o(){n!==null&&window.clearTimeout(n)}function a(){o(),t.value=!0,n=window.setTimeout(()=>{t.value=!1},e)}H===0&&ce("click",window,Be,!0);const f=()=>{H+=1,ce("click",window,a,!0)};return $e&&($e=Ke())?(fe(f),V(()=>{H-=1,H===0&&de("click",window,Be,!0),de("click",window,a,!0),o()})):f(),W(t)}let M=0,Oe="",Te="",Fe="",Ie="";const Me=z("0px");function Bo(e){if(typeof document=="undefined")return;const t=document.documentElement;let n,o=!1;const a=()=>{t.style.marginRight=Oe,t.style.overflow=Te,t.style.overflowX=Fe,t.style.overflowY=Ie,Me.value="0px"};Je(()=>{n=re(e,f=>{if(f){if(!M){const u=window.innerWidth-t.offsetWidth;u>0&&(Oe=t.style.marginRight,t.style.marginRight=`${u}px`,Me.value=`${u}px`),Te=t.style.overflow,Fe=t.style.overflowX,Ie=t.style.overflowY,t.style.overflow="hidden",t.style.overflowX="hidden",t.style.overflowY="hidden"}o=!0,M++}else M--,M||a(),o=!1},{immediate:!0})}),V(()=>{n==null||n(),o&&(M--,M||a(),o=!1)})}const pe=z(!1),Ee=()=>{pe.value=!0},je=()=>{pe.value=!1};let j=0;const $o=()=>(wo&&(fe(()=>{j||(window.addEventListener("compositionstart",Ee),window.addEventListener("compositionend",je)),j++}),V(()=>{j<=1?(window.removeEventListener("compositionstart",Ee),window.removeEventListener("compositionend",je),j=0):j--})),pe),Oo={paddingSmall:"12px 16px 12px",paddingMedium:"19px 24px 20px",paddingLarge:"23px 32px 24px",paddingHuge:"27px 40px 28px",titleFontSizeSmall:"16px",titleFontSizeMedium:"18px",titleFontSizeLarge:"18px",titleFontSizeHuge:"18px",closeIconSize:"18px",closeSize:"22px"},To=e=>{const{primaryColor:t,borderRadius:n,lineHeight:o,fontSize:a,cardColor:f,textColor2:u,textColor1:h,dividerColor:c,fontWeightStrong:r,closeIconColor:p,closeIconColorHover:l,closeIconColorPressed:b,closeColorHover:w,closeColorPressed:R,modalColor:y,boxShadow1:g,popoverColor:C,actionColor:m}=e;return Object.assign(Object.assign({},Oo),{lineHeight:o,color:f,colorModal:y,colorPopover:C,colorTarget:t,colorEmbedded:m,colorEmbeddedModal:m,colorEmbeddedPopover:m,textColor:u,titleTextColor:h,borderColor:c,actionColor:m,titleFontWeight:r,closeColorHover:w,closeColorPressed:R,closeBorderRadius:n,closeIconColor:p,closeIconColorHover:l,closeIconColorPressed:b,fontSizeSmall:a,fontSizeMedium:a,fontSizeLarge:a,fontSizeHuge:a,boxShadow:g,borderRadius:n})},Fo={name:"Card",common:ve,self:To},Ue=Fo,Io=x([P("card",`
 font-size: var(--n-font-size);
 line-height: var(--n-line-height);
 display: flex;
 flex-direction: column;
 width: 100%;
 box-sizing: border-box;
 position: relative;
 border-radius: var(--n-border-radius);
 background-color: var(--n-color);
 color: var(--n-text-color);
 word-break: break-word;
 transition: 
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier),
 border-color .3s var(--n-bezier);
 `,[_e({background:"var(--n-color-modal)"}),k("hoverable",[x("&:hover","box-shadow: var(--n-box-shadow);")]),k("content-segmented",[x(">",[v("content",{paddingTop:"var(--n-padding-bottom)"})])]),k("content-soft-segmented",[x(">",[v("content",`
 margin: 0 var(--n-padding-left);
 padding: var(--n-padding-bottom) 0;
 `)])]),k("footer-segmented",[x(">",[v("footer",{paddingTop:"var(--n-padding-bottom)"})])]),k("footer-soft-segmented",[x(">",[v("footer",`
 padding: var(--n-padding-bottom) 0;
 margin: 0 var(--n-padding-left);
 `)])]),x(">",[P("card-header",`
 box-sizing: border-box;
 display: flex;
 align-items: center;
 font-size: var(--n-title-font-size);
 padding:
 var(--n-padding-top)
 var(--n-padding-left)
 var(--n-padding-bottom)
 var(--n-padding-left);
 `,[v("main",`
 font-weight: var(--n-title-font-weight);
 transition: color .3s var(--n-bezier);
 flex: 1;
 min-width: 0;
 color: var(--n-title-text-color);
 `),v("extra",`
 display: flex;
 align-items: center;
 font-size: var(--n-font-size);
 font-weight: 400;
 transition: color .3s var(--n-bezier);
 color: var(--n-text-color);
 `),v("close",`
 margin: 0 0 0 8px;
 transition:
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 `)]),v("action",`
 box-sizing: border-box;
 transition:
 background-color .3s var(--n-bezier),
 border-color .3s var(--n-bezier);
 background-clip: padding-box;
 background-color: var(--n-action-color);
 `),v("content","flex: 1; min-width: 0;"),v("content, footer",`
 box-sizing: border-box;
 padding: 0 var(--n-padding-left) var(--n-padding-bottom) var(--n-padding-left);
 font-size: var(--n-font-size);
 `,[x("&:first-child",{paddingTop:"var(--n-padding-bottom)"})]),v("action",`
 background-color: var(--n-action-color);
 padding: var(--n-padding-bottom) var(--n-padding-left);
 border-bottom-left-radius: var(--n-border-radius);
 border-bottom-right-radius: var(--n-border-radius);
 `)]),P("card-cover",`
 overflow: hidden;
 width: 100%;
 border-radius: var(--n-border-radius) var(--n-border-radius) 0 0;
 `,[x("img",`
 display: block;
 width: 100%;
 `)]),k("bordered",`
 border: 1px solid var(--n-border-color);
 `,[x("&:target","border-color: var(--n-color-target);")]),k("action-segmented",[x(">",[v("action",[x("&:not(:first-child)",{borderTop:"1px solid var(--n-border-color)"})])])]),k("content-segmented, content-soft-segmented",[x(">",[v("content",{transition:"border-color 0.3s var(--n-bezier)"},[x("&:not(:first-child)",{borderTop:"1px solid var(--n-border-color)"})])])]),k("footer-segmented, footer-soft-segmented",[x(">",[v("footer",{transition:"border-color 0.3s var(--n-bezier)"},[x("&:not(:first-child)",{borderTop:"1px solid var(--n-border-color)"})])])]),k("embedded",`
 background-color: var(--n-color-embedded);
 `)]),He(P("card",`
 background: var(--n-color-modal);
 `,[k("embedded",`
 background-color: var(--n-color-embedded-modal);
 `)])),eo(P("card",`
 background: var(--n-color-popover);
 `,[k("embedded",`
 background-color: var(--n-color-embedded-popover);
 `)]))]),be={title:String,contentStyle:[Object,String],headerStyle:[Object,String],headerExtraStyle:[Object,String],footerStyle:[Object,String],embedded:Boolean,segmented:{type:[Boolean,Object],default:!1},size:{type:String,default:"medium"},bordered:{type:Boolean,default:!0},closable:Boolean,hoverable:Boolean,role:String,onClose:[Function,Array],tag:{type:String,default:"div"}},Mo=me(be),Eo=Object.assign(Object.assign({},E.props),be),jo=K({name:"Card",props:Eo,setup(e){const t=()=>{const{onClose:r}=e;r&&L(r)},{inlineThemeDisabled:n,mergedClsPrefixRef:o,mergedRtlRef:a}=ue(e),f=E("Card","-card",Io,Ue,e,o),u=Ze("Card",a,o),h=F(()=>{const{size:r}=e,{self:{color:p,colorModal:l,colorTarget:b,textColor:w,titleTextColor:R,titleFontWeight:y,borderColor:g,actionColor:C,borderRadius:m,lineHeight:B,closeIconColor:S,closeIconColorHover:i,closeIconColorPressed:d,closeColorHover:$,closeColorPressed:O,closeBorderRadius:U,closeIconSize:X,closeSize:Y,boxShadow:q,colorPopover:J,colorEmbedded:G,colorEmbeddedModal:Q,colorEmbeddedPopover:Z,[D("padding",r)]:ee,[D("fontSize",r)]:oe,[D("titleFontSize",r)]:te},common:{cubicBezierEaseInOut:ne}}=f.value,{top:ie,left:Ye,bottom:qe}=oo(ee);return{"--n-bezier":ne,"--n-border-radius":m,"--n-color":p,"--n-color-modal":l,"--n-color-popover":J,"--n-color-embedded":G,"--n-color-embedded-modal":Q,"--n-color-embedded-popover":Z,"--n-color-target":b,"--n-text-color":w,"--n-line-height":B,"--n-action-color":C,"--n-title-text-color":R,"--n-title-font-weight":y,"--n-close-icon-color":S,"--n-close-icon-color-hover":i,"--n-close-icon-color-pressed":d,"--n-close-color-hover":$,"--n-close-color-pressed":O,"--n-border-color":g,"--n-box-shadow":q,"--n-padding-top":ie,"--n-padding-bottom":qe,"--n-padding-left":Ye,"--n-font-size":oe,"--n-title-font-size":te,"--n-close-size":Y,"--n-close-icon-size":X,"--n-close-border-radius":U}}),c=n?he("card",F(()=>e.size[0]),h,e):void 0;return{rtlEnabled:u,mergedClsPrefix:o,mergedTheme:f,handleCloseClick:t,cssVars:n?void 0:h,themeClass:c==null?void 0:c.themeClass,onRender:c==null?void 0:c.onRender}},render(){const{segmented:e,bordered:t,hoverable:n,mergedClsPrefix:o,rtlEnabled:a,onRender:f,embedded:u,tag:h,$slots:c}=this;return f==null||f(),s(h,{class:[`${o}-card`,this.themeClass,u&&`${o}-card--embedded`,{[`${o}-card--rtl`]:a,[`${o}-card--content${typeof e!="boolean"&&e.content==="soft"?"-soft":""}-segmented`]:e===!0||e!==!1&&e.content,[`${o}-card--footer${typeof e!="boolean"&&e.footer==="soft"?"-soft":""}-segmented`]:e===!0||e!==!1&&e.footer,[`${o}-card--action-segmented`]:e===!0||e!==!1&&e.action,[`${o}-card--bordered`]:t,[`${o}-card--hoverable`]:n}],style:this.cssVars,role:this.role},T(c.cover,r=>r&&s("div",{class:`${o}-card-cover`,role:"none"},r)),T(c.header,r=>r||this.title||this.closable?s("div",{class:`${o}-card-header`,style:this.headerStyle},s("div",{class:`${o}-card-header__main`,role:"heading"},r||this.title),T(c["header-extra"],p=>p&&s("div",{class:`${o}-card-header__extra`,style:this.headerExtraStyle},p)),this.closable?s(De,{clsPrefix:o,class:`${o}-card-header__close`,onClick:this.handleCloseClick,absolute:!0}):null):null),T(c.default,r=>r&&s("div",{class:`${o}-card__content`,style:this.contentStyle,role:"none"},r)),T(c.footer,r=>r&&[s("div",{class:`${o}-card__footer`,style:this.footerStyle,role:"none"},r)]),T(c.action,r=>r&&s("div",{class:`${o}-card__action`,role:"none"},r)))}}),Lo={titleFontSize:"18px",padding:"16px 28px 20px 28px",iconSize:"28px",actionSpace:"12px",contentMargin:"8px 0 16px 0",iconMargin:"0 4px 0 0",iconMarginIconTop:"4px 0 8px 0",closeSize:"22px",closeIconSize:"18px",closeMargin:"20px 26px 0 0",closeMarginIconTop:"10px 16px 0 0"},No=e=>{const{textColor1:t,textColor2:n,modalColor:o,closeIconColor:a,closeIconColorHover:f,closeIconColorPressed:u,closeColorHover:h,closeColorPressed:c,infoColor:r,successColor:p,warningColor:l,errorColor:b,primaryColor:w,dividerColor:R,borderRadius:y,fontWeightStrong:g,lineHeight:C,fontSize:m}=e;return Object.assign(Object.assign({},Lo),{fontSize:m,lineHeight:C,border:`1px solid ${R}`,titleTextColor:t,textColor:n,color:o,closeColorHover:h,closeColorPressed:c,closeIconColor:a,closeIconColorHover:f,closeIconColorPressed:u,closeBorderRadius:y,iconColor:w,iconColorInfo:r,iconColorSuccess:p,iconColorWarning:l,iconColorError:b,borderRadius:y,titleFontWeight:g})},_o=Ae({name:"Dialog",common:ve,peers:{Button:to},self:No}),Xe=_o,Ce={icon:Function,type:{type:String,default:"default"},title:[String,Function],closable:{type:Boolean,default:!0},negativeText:String,positiveText:String,positiveButtonProps:Object,negativeButtonProps:Object,content:[String,Function],action:Function,showIcon:{type:Boolean,default:!0},loading:Boolean,bordered:Boolean,iconPlacement:String,onPositiveClick:Function,onNegativeClick:Function,onClose:Function},Ho=me(Ce),Ao=x([P("dialog",`
 word-break: break-word;
 line-height: var(--n-line-height);
 position: relative;
 background: var(--n-color);
 color: var(--n-text-color);
 box-sizing: border-box;
 margin: auto;
 border-radius: var(--n-border-radius);
 padding: var(--n-padding);
 transition: 
 border-color .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 `,[v("icon",{color:"var(--n-icon-color)"}),k("bordered",{border:"var(--n-border)"}),k("icon-top",[v("close",{margin:"var(--n-close-margin)"}),v("icon",{margin:"var(--n-icon-margin)"}),v("content",{textAlign:"center"}),v("title",{justifyContent:"center"}),v("action",{justifyContent:"center"})]),k("icon-left",[v("icon",{margin:"var(--n-icon-margin)"}),k("closable",[v("title",`
 padding-right: calc(var(--n-close-size) + 6px);
 `)])]),v("close",`
 position: absolute;
 right: 0;
 top: 0;
 margin: var(--n-close-margin);
 transition:
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 z-index: 1;
 `),v("content",`
 font-size: var(--n-font-size);
 margin: var(--n-content-margin);
 position: relative;
 word-break: break-word;
 `,[k("last","margin-bottom: 0;")]),v("action",`
 display: flex;
 justify-content: flex-end;
 `,[x("> *:not(:last-child)",{marginRight:"var(--n-action-space)"})]),v("icon",{fontSize:"var(--n-icon-size)",transition:"color .3s var(--n-bezier)"}),v("title",`
 transition: color .3s var(--n-bezier);
 display: flex;
 align-items: center;
 font-size: var(--n-title-font-size);
 font-weight: var(--n-title-font-weight);
 color: var(--n-title-text-color);
 `),P("dialog-icon-container",{display:"flex",justifyContent:"center"})]),He(P("dialog",`
 width: 446px;
 max-width: calc(100vw - 32px);
 `)),P("dialog",[_e(`
 width: 446px;
 max-width: calc(100vw - 32px);
 `)])]),Do={default:()=>s(ze,null),info:()=>s(ze,null),success:()=>s(io,null),warning:()=>s(lo,null),error:()=>s(ro,null)},Wo=K({name:"Dialog",alias:["NimbusConfirmCard","Confirm"],props:Object.assign(Object.assign({},E.props),Ce),setup(e){const{mergedComponentPropsRef:t,mergedClsPrefixRef:n,inlineThemeDisabled:o}=ue(e),a=F(()=>{var l,b;const{iconPlacement:w}=e;return w||((b=(l=t==null?void 0:t.value)===null||l===void 0?void 0:l.Dialog)===null||b===void 0?void 0:b.iconPlacement)||"left"});function f(l){const{onPositiveClick:b}=e;b&&b(l)}function u(l){const{onNegativeClick:b}=e;b&&b(l)}function h(){const{onClose:l}=e;l&&l()}const c=E("Dialog","-dialog",Ao,Xe,e,n),r=F(()=>{const{type:l}=e,b=a.value,{common:{cubicBezierEaseInOut:w},self:{fontSize:R,lineHeight:y,border:g,titleTextColor:C,textColor:m,color:B,closeBorderRadius:S,closeColorHover:i,closeColorPressed:d,closeIconColor:$,closeIconColorHover:O,closeIconColorPressed:U,closeIconSize:X,borderRadius:Y,titleFontWeight:q,titleFontSize:J,padding:G,iconSize:Q,actionSpace:Z,contentMargin:ee,closeSize:oe,[b==="top"?"iconMarginIconTop":"iconMargin"]:te,[b==="top"?"closeMarginIconTop":"closeMargin"]:ne,[D("iconColor",l)]:ie}}=c.value;return{"--n-font-size":R,"--n-icon-color":ie,"--n-bezier":w,"--n-close-margin":ne,"--n-icon-margin":te,"--n-icon-size":Q,"--n-close-size":oe,"--n-close-icon-size":X,"--n-close-border-radius":S,"--n-close-color-hover":i,"--n-close-color-pressed":d,"--n-close-icon-color":$,"--n-close-icon-color-hover":O,"--n-close-icon-color-pressed":U,"--n-color":B,"--n-text-color":m,"--n-border-radius":Y,"--n-padding":G,"--n-line-height":y,"--n-border":g,"--n-content-margin":ee,"--n-title-font-size":J,"--n-title-font-weight":q,"--n-title-text-color":C,"--n-action-space":Z}}),p=o?he("dialog",F(()=>`${e.type[0]}${a.value[0]}`),r,e):void 0;return{mergedClsPrefix:n,mergedIconPlacement:a,mergedTheme:c,handlePositiveClick:f,handleNegativeClick:u,handleCloseClick:h,cssVars:o?void 0:r,themeClass:p==null?void 0:p.themeClass,onRender:p==null?void 0:p.onRender}},render(){var e;const{bordered:t,mergedIconPlacement:n,cssVars:o,closable:a,showIcon:f,title:u,content:h,action:c,negativeText:r,positiveText:p,positiveButtonProps:l,negativeButtonProps:b,handlePositiveClick:w,handleNegativeClick:R,mergedTheme:y,loading:g,type:C,mergedClsPrefix:m}=this;(e=this.onRender)===null||e===void 0||e.call(this);const B=f?s(no,{clsPrefix:m,class:`${m}-dialog__icon`},{default:()=>T(this.$slots.icon,i=>i||(this.icon?I(this.icon):Do[this.type]()))}):null,S=T(this.$slots.action,i=>i||p||r||c?s("div",{class:`${m}-dialog__action`},i||(c?[I(c)]:[this.negativeText&&s(Se,Object.assign({theme:y.peers.Button,themeOverrides:y.peerOverrides.Button,ghost:!0,size:"small",onClick:R},b),{default:()=>I(this.negativeText)}),this.positiveText&&s(Se,Object.assign({theme:y.peers.Button,themeOverrides:y.peerOverrides.Button,size:"small",type:C==="default"?"primary":C,disabled:g,loading:g,onClick:w},l),{default:()=>I(this.positiveText)})])):null);return s("div",{class:[`${m}-dialog`,this.themeClass,this.closable&&`${m}-dialog--closable`,`${m}-dialog--icon-${n}`,t&&`${m}-dialog--bordered`],style:o,role:"dialog"},a?s(De,{clsPrefix:m,class:`${m}-dialog__close`,onClick:this.handleCloseClick}):null,f&&n==="top"?s("div",{class:`${m}-dialog-icon-container`},B):null,s("div",{class:`${m}-dialog__title`},f&&n==="left"?B:null,we(this.$slots.header,()=>[I(u)])),s("div",{class:[`${m}-dialog__content`,S?"":`${m}-dialog__content--last`]},we(this.$slots.default,()=>[I(h)])),S)}}),Vo=ge("n-dialog-provider"),kt=ge("n-dialog-api"),wt=ge("n-dialog-reactive-list"),Ko=e=>{const{modalColor:t,textColor2:n,boxShadow3:o}=e;return{color:t,textColor:n,boxShadow:o}},Uo=Ae({name:"Modal",common:ve,peers:{Scrollbar:po,Dialog:Xe,Card:Ue},self:Ko}),Xo=Uo,xe=Object.assign(Object.assign({},be),Ce),Yo=me(xe),qo=K({name:"ModalBody",inheritAttrs:!1,props:Object.assign(Object.assign({show:{type:Boolean,required:!0},preset:String,displayDirective:{type:String,required:!0},trapFocus:{type:Boolean,default:!0},autoFocus:{type:Boolean,default:!0},blockScroll:Boolean},xe),{renderMask:Function,onClickoutside:Function,onBeforeLeave:{type:Function,required:!0},onAfterLeave:{type:Function,required:!0},onPositiveClick:{type:Function,required:!0},onNegativeClick:{type:Function,required:!0},onClose:{type:Function,required:!0},onAfterEnter:Function,onEsc:Function}),setup(e){const t=z(null),n=z(null),o=z(e.show),a=z(null),f=z(null);re(le(e,"show"),g=>{g&&(o.value=!0)}),Bo(F(()=>e.blockScroll&&o.value));const u=Le(We);function h(){if(u.transformOriginRef.value==="center")return"";const{value:g}=a,{value:C}=f;if(g===null||C===null)return"";if(n.value){const m=n.value.containerScrollTop;return`${g}px ${C+m}px`}return""}function c(g){if(u.transformOriginRef.value==="center")return;const C=u.getMousePosition();if(!C||!n.value)return;const m=n.value.containerScrollTop,{offsetLeft:B,offsetTop:S}=g;if(C){const i=C.y,d=C.x;a.value=-(B-d),f.value=-(S-i-m)}g.style.transformOrigin=h()}function r(g){ye(()=>{c(g)})}function p(g){g.style.transformOrigin=h(),e.onBeforeLeave()}function l(){o.value=!1,a.value=null,f.value=null,e.onAfterLeave()}function b(){const{onClose:g}=e;g&&g()}function w(){e.onNegativeClick()}function R(){e.onPositiveClick()}const y=z(null);return re(y,g=>{g&&ye(()=>{const C=g.el;C&&t.value!==C&&(t.value=C)})}),A(so,t),A(ao,null),A(co,null),{mergedTheme:u.mergedThemeRef,appear:u.appearRef,isMounted:u.isMountedRef,mergedClsPrefix:u.mergedClsPrefixRef,bodyRef:t,scrollbarRef:n,displayed:o,childNodeRef:y,handlePositiveClick:R,handleNegativeClick:w,handleCloseClick:b,handleAfterLeave:l,handleBeforeLeave:p,handleEnter:r}},render(){const{$slots:e,$attrs:t,handleEnter:n,handleAfterLeave:o,handleBeforeLeave:a,preset:f,mergedClsPrefix:u}=this;let h=null;if(!f){if(h=mo(e),!h){ho("modal","default slot is empty");return}h=Ge(h),h.props=Qe({class:`${u}-modal`},t,h.props||{})}return this.displayDirective==="show"||this.displayed||this.show?se(s("div",{role:"none",class:`${u}-modal-body-wrapper`},s(bo,{ref:"scrollbarRef",theme:this.mergedTheme.peers.Scrollbar,themeOverrides:this.mergedTheme.peerOverrides.Scrollbar,contentClass:`${u}-modal-scroll-content`},{default:()=>{var c;return[(c=this.renderMask)===null||c===void 0?void 0:c.call(this),s(vo,{disabled:!this.trapFocus,active:this.show,onEsc:this.onEsc,autoFocus:this.autoFocus},{default:()=>{var r;return s(Ne,{name:"fade-in-scale-up-transition",appear:(r=this.appear)!==null&&r!==void 0?r:this.isMounted,onEnter:n,onAfterEnter:this.onAfterEnter,onAfterLeave:o,onBeforeLeave:a},{default:()=>{const p=[[ke,this.show]],{onClickoutside:l}=this;return l&&p.push([fo,this.onClickoutside,void 0,{capture:!0}]),se(this.preset==="confirm"||this.preset==="dialog"?s(Wo,Object.assign({},this.$attrs,{class:[`${u}-modal`,this.$attrs.class],ref:"bodyRef",theme:this.mergedTheme.peers.Dialog,themeOverrides:this.mergedTheme.peerOverrides.Dialog},ae(this.$props,Ho),{"aria-modal":"true"}),e):this.preset==="card"?s(jo,Object.assign({},this.$attrs,{ref:"bodyRef",class:[`${u}-modal`,this.$attrs.class],theme:this.mergedTheme.peers.Card,themeOverrides:this.mergedTheme.peerOverrides.Card},ae(this.$props,Mo),{"aria-modal":"true",role:"dialog"}),e):this.childNodeRef=h,p)}})}})]}})),[[ke,this.displayDirective==="if"||this.displayed||this.show]]):null}}),Jo=x([P("modal-container",`
 position: fixed;
 left: 0;
 top: 0;
 height: 0;
 width: 0;
 display: flex;
 `),P("modal-mask",`
 position: fixed;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 background-color: rgba(0, 0, 0, .4);
 `,[Co({enterDuration:".25s",leaveDuration:".25s",enterCubicBezier:"var(--n-bezier-ease-out)",leaveCubicBezier:"var(--n-bezier-ease-out)"})]),P("modal-body-wrapper",`
 position: fixed;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 overflow: visible;
 `,[P("modal-scroll-content",`
 min-height: 100%;
 display: flex;
 position: relative;
 `)]),P("modal",`
 position: relative;
 align-self: center;
 color: var(--n-text-color);
 margin: auto;
 box-shadow: var(--n-box-shadow);
 `,[yo({duration:".25s",enterScale:".5"})])]),Go=Object.assign(Object.assign(Object.assign(Object.assign({},E.props),{show:Boolean,unstableShowMask:{type:Boolean,default:!0},maskClosable:{type:Boolean,default:!0},preset:String,to:[String,Object],displayDirective:{type:String,default:"if"},transformOrigin:{type:String,default:"mouse"},zIndex:Number,autoFocus:{type:Boolean,default:!0},trapFocus:{type:Boolean,default:!0},closeOnEsc:{type:Boolean,default:!0},blockScroll:{type:Boolean,default:!0}}),xe),{onEsc:Function,"onUpdate:show":[Function,Array],onUpdateShow:[Function,Array],onAfterEnter:Function,onBeforeLeave:Function,onAfterLeave:Function,onClose:Function,onPositiveClick:Function,onNegativeClick:Function,onMaskClick:Function,internalDialog:Boolean,internalAppear:{type:Boolean,default:void 0},overlayStyle:[String,Object],onBeforeHide:Function,onAfterHide:Function,onHide:Function}),St=K({name:"Modal",inheritAttrs:!1,props:Go,setup(e){const t=z(null),{mergedClsPrefixRef:n,namespaceRef:o,inlineThemeDisabled:a}=ue(e),f=E("Modal","-modal",Jo,Xo,e,n),u=Ro(64),h=zo(),c=ko(),r=e.internalDialog?Le(Vo,null):null,p=$o();function l(i){const{onUpdateShow:d,"onUpdate:show":$,onHide:O}=e;d&&L(d,i),$&&L($,i),O&&!i&&O(i)}function b(){const{onClose:i}=e;i?Promise.resolve(i()).then(d=>{d!==!1&&l(!1)}):l(!1)}function w(){const{onPositiveClick:i}=e;i?Promise.resolve(i()).then(d=>{d!==!1&&l(!1)}):l(!1)}function R(){const{onNegativeClick:i}=e;i?Promise.resolve(i()).then(d=>{d!==!1&&l(!1)}):l(!1)}function y(){const{onBeforeLeave:i,onBeforeHide:d}=e;i&&L(i),d&&d()}function g(){const{onAfterLeave:i,onAfterHide:d}=e;i&&L(i),d&&d()}function C(i){var d;const{onMaskClick:$}=e;$&&$(i),e.maskClosable&&!((d=t.value)===null||d===void 0)&&d.contains(xo(i))&&l(!1)}function m(i){var d;(d=e.onEsc)===null||d===void 0||d.call(e),e.show&&e.closeOnEsc&&So(i)&&!p.value&&l(!1)}A(We,{getMousePosition:()=>{if(r){const{clickedRef:i,clickPositionRef:d}=r;if(i.value&&d.value)return d.value}return u.value?h.value:null},mergedClsPrefixRef:n,mergedThemeRef:f,isMountedRef:c,appearRef:le(e,"internalAppear"),transformOriginRef:le(e,"transformOrigin")});const B=F(()=>{const{common:{cubicBezierEaseOut:i},self:{boxShadow:d,color:$,textColor:O}}=f.value;return{"--n-bezier-ease-out":i,"--n-box-shadow":d,"--n-color":$,"--n-text-color":O}}),S=a?he("theme-class",void 0,B,e):void 0;return{mergedClsPrefix:n,namespace:o,isMounted:c,containerRef:t,presetProps:F(()=>ae(e,Yo)),handleEsc:m,handleAfterLeave:g,handleClickoutside:C,handleBeforeLeave:y,doUpdateShow:l,handleNegativeClick:R,handlePositiveClick:w,handleCloseClick:b,cssVars:a?void 0:B,themeClass:S==null?void 0:S.themeClass,onRender:S==null?void 0:S.onRender}},render(){const{mergedClsPrefix:e}=this;return s(go,{to:this.to,show:this.show},{default:()=>{var t;(t=this.onRender)===null||t===void 0||t.call(this);const{unstableShowMask:n}=this;return se(s("div",{role:"none",ref:"containerRef",class:[`${e}-modal-container`,this.themeClass,this.namespace],style:this.cssVars},s(qo,Object.assign({style:this.overlayStyle},this.$attrs,{ref:"bodyWrapper",displayDirective:this.displayDirective,show:this.show,preset:this.preset,autoFocus:this.autoFocus,trapFocus:this.trapFocus,blockScroll:this.blockScroll},this.presetProps,{onEsc:this.handleEsc,onClose:this.handleCloseClick,onNegativeClick:this.handleNegativeClick,onPositiveClick:this.handlePositiveClick,onBeforeLeave:this.handleBeforeLeave,onAfterEnter:this.onAfterEnter,onAfterLeave:this.handleAfterLeave,onClickoutside:n?void 0:this.handleClickoutside,renderMask:n?()=>{var o;return s(Ne,{name:"fade-in-transition",key:"mask",appear:(o=this.internalAppear)!==null&&o!==void 0?o:this.isMounted},{default:()=>this.show?s("div",{"aria-hidden":!0,ref:"containerRef",class:`${e}-modal-mask`,onClick:this.handleClickoutside}):null})}:void 0}),this.$slots)),[[uo,{zIndex:this.zIndex,enabled:this.show}]])}})}});export{St as N,Ce as a,Wo as b,Ho as c,kt as d,zo as e,Vo as f,wt as g,Ro as u};
