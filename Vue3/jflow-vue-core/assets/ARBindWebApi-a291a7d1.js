var i=Object.defineProperty;var a=(F,t,u)=>t in F?i(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var o=(F,t,u)=>(a(F,typeof t!="symbol"?t+"":t,u),u);var s=(F,t,u)=>new Promise((e,B)=>{var n=r=>{try{E(u.next(r))}catch(A){B(A)}},D=r=>{try{E(u.throw(r))}catch(A){B(A)}},E=r=>r.done?e(r.value):Promise.resolve(r.value).then(n,D);E((u=u.apply(F,t)).next())});import{UAC as C}from"./UAC-8e255d47.js";import{Map as p}from"./Map-73575e6b.js";import{EntityMyPK as d}from"./EntityMyPK-e742fec8.js";import{RefMethodType as l,RefMethod as m}from"./RefMethod-33a71db4.js";import g from"./BSEntity-840a884b.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";class O extends d{constructor(u){super("TS.WF.ARBindWebApi");o(this,"Help",`
  ####  \u5E2E\u52A9
  - \u8BF7\u8F93\u5165\u5B8C\u6574\u7684url \u6BD4\u5982: http:/ccbpm.cn:9090/xxx.do?xx=xx&WorkID=@WorkID&Token=@WwebUser.Token
  - \u5185\u90E8\u53C2\u6570\u683C\u5F0F: @WebUser.* \u662F\u5F53\u524D\u767B\u5F55\u4EBA\u5458\u7684\u4FE1\u606F.
  - \u8868\u5355\u53C2\u6570: @+\u5B57\u6BB5\u540D, \u6BD4\u5982\u5BA1\u6279\u91D1\u989D: @SPJE   
  - \u6D41\u7A0B\u53C2\u6570\uFF1A @WorkID , @NodeID  \u4E24\u4E2A\u56FA\u5B9A\u7684\u53C2\u6570.
  ####  \u5BF9\u4E8Epost\u6A21\u5F0F.
  - \u8BF7\u53C2\u8003 post\u586B\u5199\u5E2E\u52A9.
  ####  \u8FD4\u56DE\u503C\u683C\u5F0F.
  - \u5FC5\u987B\u8FD4\u56DE: zhangsan,lisi,wangwu \u8FD9\u6837\u7684\u683C\u5F0F.
  `);o(this,"HelpParaModel",`
  #### \u81EA\u5B9A\u4E49\u6A21\u5F0F
  - \u5728\u3010\u81EA\u5B9A\u4E49\u6570\u636E\u5185\u5BB9\u3011\u8F93\u5165json\u5B57\u7B26\u4E32\uFF0C\u5B57\u7B26\u4E32\u5185\u53EF\u4EE5\u4F7F\u7528@+\u5B57\u6BB5\u540D.
  - \u6BD4\u5982\uFF1A
  {
    QingJiaTianShu:@QingJiaTianShu
    WorkID:@WorkID
    NodeID:@FK_Node
    User:@WebUser.No
  }
  - \u53C2\u6570\u91CC\u5305\u62EC\u4E09\u7C7B:
  - 1. \u5185\u7F6E\u53C2\u6570@WebUser.* ,   \u89E3\u91CA: \u5F53\u524D\u767B\u5F55\u4EBA\u5458\u7684\u4FE1\u606F, @WebUer.No,  @WebUer.Nme,  @WebUer.FK_Dept,  @WebUer.DeptName,  @WebUer.OrgNo
  - 2. \u8868\u5355\u53C2\u6570:@\u8868\u5355\u5B57\u6BB5     \u89E3\u91CA: \u8868\u5355\u7684\u82F1\u6587\u5B57\u6BB5\u540D.
  - 3. \u6D41\u7A0B\u53C2\u6570:@WorkID   @NodeID    \u89E3\u91CA:\u5F53\u524D\u7684\u5DE5\u4F5CID\uFF0C\u8282\u70B9ID.
  #### \u5168\u91CF\u6A21\u5F0F
  - \u628A\u8868\u5355\u5B57\u6BB5, \u4F5C\u4E3A\u4E00\u4E2A\u5927\u7684json\u4F20\u5165\u63A5\u53E3\u91CC\u9762\u53BB.
  - \u63A5\u53E3\u5F00\u53D1\u8005\uFF0C\u83B7\u5F97\u8868\u5355\u6570\u636E\u8FDB\u884C\u6709\u6548\u7684\u5229\u7528\uFF0C\u751F\u6210\u4E0B\u4E00\u6B65\u5DE5\u4F5C\u4EBA\u5458\uFF0C\u8FD4\u56DE\u8FC7\u6765\u3002
  `);o(this,"helpJsonNode",`
  #### \u5E2E\u52A9
  - \u8FD4\u56DE\u7684JSON\u4ECE\u90A3\u4E2A\u8282\u70B9\u4E0B\u5F00\u59CB\u83B7\u53D6\u6570\u636E\uFF0C\u5982\u679C\u6CA1\u6709\u8282\u70B9\u5C31\u4E0D\u9700\u8981\u8BBE\u7F6E.
  #### \u6BD4\u59821\uFF1A\u8FD4\u56DE\u6570\u636E
   {
        "contents": [
          {
             "UserNo": "string",
             "UserName": "string",
          },
          {
              "UserNo": "string",
               "UserName": "string",
          }
        ]
   }

   - \u8FD9\u4E2A\u6A21\u5F0F\u9700\u8981\u914D\u7F6E\u8282\u70B9ID: contents
   #### \u6BD4\u59822\uFF1A\u8FD4\u56DE\u6570\u636E
    [
          {
             "UserNo": "string",
             "UserName": "string",
          },
          {
              "UserNo": "string",
               "UserName": "string",
          }
        ]

   - \u8BE5\u6A21\u5F0F\u4E0D\u9700\u8981\u914D\u7F6E.
  `);u&&this.setPKVal(u)}get HisUAC(){const u=new C;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new p("WF_Part","\u7ED1\u5B9AWebApi");u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK(),u.AddTBString("FlowNo",null,"FlowNo",!1,!1,0,800,100,!1),u.AddTBInt("NodeID",0,"NodeID",!1,!1),u.AddTBString("Tag0",null,"Url",!0,!1,0,800,100,!0,this.Help),u.AddDDLStringEnum("Tag1","Get","\u8BF7\u6C42\u6A21\u5F0F","@Get=Get\u6A21\u5F0F@POST=Post\u6A21\u5F0F",!0,""),u.AddTBStringDoc("Tag9",null,"\u5907\u6CE8",!0,!1,!0,this.Help),u.AddGroupAttr("POST\u8BBE\u7F6E"),u.AddDDLStringEnum("Tag2","0","\u53C2\u6570\u6A21\u5F0F","@0=\u81EA\u5B9A\u4E49\u6A21\u5F0F@1=\u5168\u91CF\u6A21\u5F0F",!0,this.HelpParaModel),u.AddTBStringDoc("Tag3",null,"\u81EA\u5B9A\u4E49\u6570\u636E\u5185\u5BB9",!0,!1,!0,this.Help),u.AddDDLStringEnum("Tag4","0","\u6570\u636E\u683C\u5F0F","@0=From\u683C\u5F0F@1=JSON\u683C\u5F0F",!0,this.HelpParaModel);const e=new m;return e.Title="\u6267\u884C\u6D4B\u8BD5",e.RefMethodType=l.Func,e.HisAttrs.AddTBString("P",null,"\u8F93\u5165\u53C2\u6570",!0,!1,0,2e3,300,!0,"\u683C\u5F0F@BU=1001@PDT=1002"),e.Warning="\u60A8\u786E\u5B9A\u8981\u6267\u884C\u5417\uFF1F",e.ClassMethod="DoTest",u.AddRefMethod(e),this._enMap=u,this._enMap}DoTest(u){return s(this,null,function*(){const e=new g("BP.WF.Template.Part",this.MyPK);return yield e.Init(),yield e.Retrieve(),"tabOpen@\u63A5\u53D7\u4EBA:"+(yield e.DoMethodReturnString("DoTestARWebApi",u))})}}export{O as ARBindWebApi};
