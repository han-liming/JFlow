var D=Object.defineProperty;var F=(o,r,t)=>r in o?D(o,r,{enumerable:!0,configurable:!0,writable:!0,value:t}):o[r]=t;var A=(o,r,t)=>(F(o,typeof r!="symbol"?r+"":r,t),t);var m=(o,r,t)=>new Promise((B,i)=>{var s=u=>{try{e(t.next(u))}catch(E){i(E)}},a=u=>{try{e(t.throw(u))}catch(E){i(E)}},e=u=>u.done?B(u.value):Promise.resolve(u.value).then(s,a);e((t=t.apply(o,r)).next())});import{Auth as l}from"./Auth-a55f8f1f.js";import{G as p,D as n}from"./DataType-33901a1c.js";import{PageBaseGroupNew as h}from"./PageBaseGroupNew-ee20c033.js";import{a1 as c}from"./index-f4658ae7.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";class U extends h{constructor(){super("GPN_Auther");A(this,"Docs0",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u6284\u9001\u4EBA.
  - \u901A\u5E38\u662F\u5728\u8282\u70B9\u8868\u5355\u4E0A\u52A0\u4E00\u4E2A\u5B57\u6BB5,\u8FD9\u4E2A\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4EBA\u5458\u8D26\u53F7\uFF0C\u591A\u4E2A\u4EBA\u5458\u4F7F\u7528\u9017\u53F7\u5206\u5F00.
  #### \u8FD0\u884C\u56FE\u4F8B
  - @liang.

`);A(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u81EA\u52A8\u6284\u9001\u7ED9\u8981\u7ED1\u5B9A\u7684\u4EBA\u5458.
`);A(this,"Docs2",`
  #### \u5E2E\u52A9
  - \u6309\u7167\u7ED1\u5B9A\u7684\u90E8\u89D2\u8272\u4E0B\u7684\u4EBA\u5458\u96C6\u5408\u4F5C\u4E3A\u6284\u9001\u4EBA.
  - \u6709\u4E00\u4E2A\u89C4\u5219
  
`);this.PageTitle="\u65B0\u589E\u6388\u6743\u4EBA",this.ForEntityClassID="TS.Port.Auth"}Init(){this.AddGroup("A","\u8BF7\u9009\u62E9\u89C4\u5219"),this.SelectItemsByTreeEns("1","\u9009\u62E9\u6388\u6743\u4EBA",this.Docs1,!1,p.srcDeptLazily,p.srcDeptRoot,p.srcEmpLazily,"@No=\u8D26\u53F7@Name=\u540D\u79F0@Tel=\u7535\u8BDD")}GenerSorts(){return m(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(t,B,i,s,a){return m(this,null,function*(){let e="";e=this.RequestVal("RefPKVal");const u=new l;if(u.MyPK=c.No+"_"+i,(yield u.IsExits())==!0){alert("\u4EBA\u5458["+s+"]\u5DF2\u7ECF\u88AB\u9009\u62E9.");return}u.Auther=e,u.AuthType=0,u.AutherToEmpNo=i,u.AutherToEmpName=s,u.RDT=n.CurrentDate,yield u.Insert();let E="";return E="/@/WF/Comm/EnOnly.vue?EnName=TS.Port.Auth&PKVal="+u.MyPK,"url@"+E})}}export{U as GPN_Auther};
