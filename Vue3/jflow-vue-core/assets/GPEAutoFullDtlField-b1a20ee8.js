var i=Object.defineProperty;var s=(e,t,u)=>t in e?i(e,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):e[t]=u;var o=(e,t,u)=>(s(e,typeof t!="symbol"?t+"":t,u),u);import{UAC as E}from"./UAC-8e255d47.js";import{Map as p}from"./Map-73575e6b.js";import{EntityMyPK as m}from"./EntityMyPK-e742fec8.js";import{a as r}from"./MapExt-db8cd7f3.js";import{G as F}from"./DataType-33901a1c.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class O extends m{constructor(u){super("TS.MapExt.GPEAutoFullDtlField");o(this,"JSDesc",` 
  #### \u8BF4\u660E
  \u8BA1\u7B97\u540E\u8981\u89E6\u53D1\u7684\u811A\u672C\u51FD\u6570(\u6BD4\u5982:\u6C42\u548C\u4EE5\u540E\u8981\u6FC0\u6D3B\u7684function)\uFF0C\u8BE5\u811A\u672C\u8981\u6C42\u5199\u5165\u5230:DataUserJSLabMyFromID_Self.js
   `);o(this,"DescTag1",` 
   #### \u8BF4\u660E
   - zhoupeng \u8865\u5145
    `);o(this,"DescDoc",` 
  #### \u8BF4\u660E
  - SQL\u683C\u5F0F\u4E3A:
  - SELECT No,Name FROM Port_Emp WHERE PinYin LIKE '@Key%' OR No LIKE '%@Key%' OR Name LIKE '%@Key%' 
  - SELECT No,Name FROM CN_City WHERE PinYin LIKE '%@Key%' OR Name LIKE '%@Key%'
  - URL\u683C\u5F0F\u4E3A:
  - /DataUser/Handler.ashx?xxx=sss 
  - \u65B9\u6CD5\u7684\u683C\u5F0F\u4E3A:
  - MyFunName
   `);u&&(this.MyPK=u)}get HisUAC(){const u=new E;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new p("Sys_MapExt","\u5BF9\u4ECE\u8868\u5217\u6C42\u503C");u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK();const a="SELECT No,Name From Sys_MapDtl WHERE FK_MapData='@FK_MapData'";return u.AddDDLSQL(r.Doc,null,"\u4ECE\u8868",a,!0),u.AddTBString(r.FK_MapData,null,"\u5BF9\u5E94\u7684\u8868\u5355",!1,!1,0,50,200,!0,this.DescDoc),u.AddTBString(r.Tag1,null,"\u8F93\u5165\u5217",!0,!1,0,50,200,!0,this.DescTag1),u.SetPopList("Tag1",F.sqlMapAttrNumberFields,!1,"200px","300px","\u9009\u62E9\u5B57\u6BB5","icon-people"),u.AddBoolean("Tag2",!1,"\u8BA1\u7B97\u540E\u8981\u89E6\u53D1\u7684\u811A\u672C\u51FD\u6570",!0,!0,!0,this.JSDesc),u.AddDDLStringEnum(r.Tag,"Sum","\u8BA1\u7B97\u65B9\u5F0F","@Sum=\u6C42\u548C@Avg=\u6C42\u5E73\u5747@Max=\u6C42\u6700\u5927@Min=\u6C42\u6700\u5C0F",!0,null,!1),u.AddBoolean("Tag3",!1,"\u5BF9\u5176\u5B83\u53EA\u8BFBstring\u5B57\u6BB5\u8FDB\u884C\u5927\u5199\u8F6C\u6362",!0,!0,!0,this.JSDesc),u.AddTBString(r.Tag1,null,"\u53EA\u8BFB\u5B57\u6BB5\u540D",!0,!0,0,50,200,!0,this.DescTag1),this._enMap=u,this._enMap}}export{O as GPEAutoFullDtlField};
