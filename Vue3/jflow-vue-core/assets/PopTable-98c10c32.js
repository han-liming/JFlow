var a=Object.defineProperty;var r=(F,e,u)=>e in F?a(F,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[e]=u;var t=(F,e,u)=>(r(F,typeof e!="symbol"?e+"":e,u),u);import{UAC as B}from"./UAC-8e255d47.js";import{Map as o}from"./Map-73575e6b.js";import{EntityMyPK as A}from"./EntityMyPK-e742fec8.js";import{a as E}from"./MapExt-db8cd7f3.js";import{SFDBSrc as D}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class w extends A{constructor(u){super("TS.MapExt.PopTable");t(this,"NoteSearchTip",` 
  #### \u5E2E\u52A9
  - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57\uFF0C\u6BD4\u5982:
  - \u8BF7\u8F93\u5165\u4ED8\u6B3E\u4EBA\u540D\u79F0,\u8FDB\u884C\u641C\u7D22\u3002
  - \u8F93\u5165\u4EBA\u5458\u7F16\u53F7,\u540D\u79F0\uFF0C\u540D\u79F0\u5168\u62FC,\u7B80\u62FC\u5173\u952E\u5B57\u641C\u7D22\u3002
  `);t(this,"NoteTag1",` 
  #### \u5E2E\u52A9

  \u8BE5\u8BBE\u7F6E\u5BF9table\u67E5\u8BE2\u6709\u6548,(\u53EF\u4EE5\u4E3A\u7A7A)\uFF1A\u65E5\u671F\u7684\u9ED8\u8BA4\u503C\u662FJS\u51FD\u6570\u8868\u8FBE\u5F0F.
  SQL\u683C\u5F0F\u4E3A:
  $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListSQL=Select No,Name FROM Demo_BanJi
  $Para=XB#Label=\u6027\u522B#EnumKey=XB
  $Para=RegDate#Label=\u6CE8\u518C\u65E5\u671F\u4ECE#DefVal=(new Date( (new Date().setDate(-30 + new Date().getDate()))))
  $Para=RegDate#Label=\u5230#DefVal=(new Date())
  URL\u683C\u5F0F\u4E3A:
  $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListURL=/DataUser/Handler.ashx?xxx=sss
  \u6267\u884CCCFromRef.js\u8FD4\u56DEJSON\u683C\u5F0F\u4E3A:
  $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListFuncName=MyFunc
  `);t(this,"NoteTag2",` 
  #### \u5E2E\u52A9
   - \u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2\u7684SQL\u8BED\u53E5\uFF0C\u5FC5\u987B\u8FD4\u56DENo,Name\u4E24\u4E2A\u5217\u3002
   - \u8BE5\u53C2\u6570\u652F\u6301ccbpm\u8868\u8FBE\u5F0F,\u6BD4\u5982:SELECT No, Name FROM WF_Emp WHERE FK_Dept='@WebUser.DeptNo'
   - \u5FC5\u987B\u6709\uFF1A\u5982\u679C\u67E5\u8BE2\u7ED3\u679C\u9700\u8981\u5206\u9875\u65F6\u5FC5\u987B\u5305\u542B@PageIdx @PageSize @Key \u4E09\u4E2A\u53C2\u6570,\u67E5\u8BE2\u7ED3\u679C\u4E0D\u5206\u9875\u65F6\u5FC5\u987B\u5305\u542B@Key,\u5206\u522B\u6807\u8BC6:@PageIdx =\u7B2C\u51E0\u9875, @PageSize=\u6BCF\u9875\u5927\u5C0F. @Key=\u5173\u952E\u5B57
   - \u6BD4\u5982For SQLServer: SELECT TOP @PageSize * FROM ( SELECT row_number() over(order by t.No) as rownumber,No,Name,Tel,Email FROM Demo_Student WHERE Name LIKE '%@Key%' ) A WHERE rownumber > @PageIdx
   - \u6BD4\u5982For Oracle: SELECT No,Name,Email,Tel FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No LIKE '%@Key%')
   - \u6BD4\u5982For MySQL: SELECT No,Name,Email,Tel FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No LIKE '%@Key%')
   - \u67E5\u8BE2\u7ED3\u679C\u5206\u9875\u65F6\u8BBE\u7F6E: \u5982\u679C\u67E5\u8BE2\u7ED3\u679C\u9700\u8981\u5206\u9875\u65F6\uFF0C\u8BBE\u7F6E\u7684\u67E5\u8BE2\u6761\u4EF6\u5FC5\u987B\u5728\u6570\u636E\u6E90\u4E2D\u5305\u542B,\u65F6\u95F4\u7684\u67E5\u8BE2\u6761\u4EF6\u7684\u8BBE\u7F6E\u589E\u52A0RegDate>='@DTFrom_RegDate' AND RegDate<='@DTTo_RegDate'
   - \u67E5\u8BE2\u7ED3\u679C\u4E0D\u5206\u9875\u65F6\u8BBE\u7F6E: \u6DFB\u52A0\u7684\u67E5\u8BE2\u6761\u4EF6\u4E3A\u6267\u884CSQL\u67E5\u8BE2\u65F6SQL\u8BED\u53E5\u540E\u9762\u4E0D\u6DFB\u52A0\uFF0C\u89E3\u6790\u7684\u65F6\u5019\u81EA\u52A8\u6DFB\u52A0\uFF0C\u5176\u4F59\u7684\u67E5\u8BE2\u65B9\u5F0F\u9700\u8981\u5305\u542B\u6DFB\u52A0\u7684\u67E5\u8BE2\u6761\u4EF6
  `);t(this,"NoteTag3",` 
   #### \u5E2E\u52A9
   - \u8BE5\u503C\u4E3A\u7A7A\u65F6\u67E5\u8BE2\u7ED3\u679C\u4E0D\u6267\u884C\u5206\u9875
   - \u603B\u6761\u6570: SELECT COUNT(no) FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No LIKE '%@Key%')
  `);t(this,"NoteTag",` 
   #### \u5E2E\u52A9
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5728\u53F3\u4E0A\u89D2\u7684\u5217\u8868\u91CC\u67E5\u8BE2\u6216\u70B9\u6811\u6811\u5E72\u7684\u6570\u636E\u6E90\u51FA\u73B0\u7684\u5217\u8868,\u9700\u8981\u7528\u4E2D\u6587\u663E\u793A\u5217\u5934.
   - \u4F8B\u5982: No=\u7F16\u53F7,Name=\u540D\u79F0,Addr=\u5730\u5740,Tel=\u7535\u8BDD,Email=\u90AE\u4EF6
  `);t(this,"NoteTag5",` 
 
  #### \u5E2E\u52A9
 
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5F39\u51FA\u6846\u786E\u5B9A\u540E\u6267\u884C\u7684JS\uFF0C\u53EF\u4EE5\u76F4\u63A5\u5199\u65B9\u6CD5\u540D\u6216\u8005\u65B9\u6CD5. 

  `);u&&(this.MyPK=u)}get HisUAC(){const u=new B;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new o("Sys_MapExt","\u8868\u683C\u5F39\u7A97");return u.AddGroupAttr("\u6570\u636E\u6765\u6E90"),u.AddMyPK(),u.AddTBString(E.FK_MapData,null,"\u8868\u5355ID",!1,!1,0,50,200),u.AddTBString(E.ExtModel,"Pop","\u6A21\u5F0F(\u5927\u7C7B)",!1,!1,0,50,200),u.AddTBString(E.ExtType,null,"\u7C7B\u578B(\u5C0F\u7C7B)",!1,!1,0,50,200),u.AddDDLEntities(E.FK_DBSrc,"local","\u6570\u636E\u6E90",new D,!0,null,!1),u.AddGroupAttr("\u6570\u636E\u83B7\u53D6"),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.NoteSearchTip),u.AddTBStringDoc(E.Tag1,null,"\u67E5\u8BE2\u6761\u4EF6\u8BBE\u7F6E",!0,!1,!0,this.NoteTag1),u.AddTBStringDoc(E.Tag2,null,"\u6570\u636E\u6E90",!0,!1,!0,this.NoteTag2),u.AddTBStringDoc(E.Tag3,null,"\u5206\u9875\u603B\u6570\u6570\u636E\u6E90",!0,!1,!0,this.NoteTag3),u.AddTBString(E.Tag,null,"\u6570\u636E\u5217\u540D\u4E0E\u4E2D\u6587\u610F\u601D\u5BF9\u7167",!0,!1,0,50,200,!0,this.NoteTag),u.AddTBString(E.Tag5,null,"\u786E\u5B9A\u540E\u6267\u884C\u7684JS",!0,!1,0,50,200,!0,this.NoteTag5),u.AddGroupAttr("\u5916\u89C2"),u.AddRadioBtn("ShowModel",0,"\u5C55\u793A\u65B9\u5F0F",!0,!1,"ShowModel","@0=POP\u5F39\u51FA\u7A97@1=\u4E0B\u62C9\u641C\u7D22\u9009\u62E9",null,!0),u.AddRadioBtn("PopSelectType",1,"\u9009\u62E9\u7C7B\u578B",!0,!0,"PopSelectType","@0=\u5355\u9009@1=\u591A\u9009",null,!0),u.AddBoolean("IsFirstLoad",!0,"\u6253\u5F00\u662F\u5426\u76F4\u63A5\u52A0\u8F7D\u6570\u636E",!0,!0,!1),u.AddTBString("Title",null,"\u6807\u9898",!0,!1,0,50,200,!0),u.AddTBString("BtnLab","\u67E5\u627E","\u67E5\u627E\u6309\u94AE\u6807\u7B7E",!0,!1,0,50,200),u.AddTBInt(E.H,400,"\u5F39\u7A97\u9AD8\u5EA6",!0,!1),u.AddTBInt(E.W,800,"\u5F39\u7A97\u5BBD\u5EA6",!0,!1),u.AddTBAtParas(4e3),u.ParaFields=",Title,BtnLab,SearchTip,ShowModel,PopSelectType,IsFirstLoad,",this._enMap=u,this._enMap}}export{w as PopTable};
