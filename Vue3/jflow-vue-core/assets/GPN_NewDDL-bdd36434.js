var h=Object.defineProperty;var U=(m,A,u)=>A in m?h(m,A,{enumerable:!0,configurable:!0,writable:!0,value:u}):m[A]=u;var t=(m,A,u)=>(U(m,typeof A!="symbol"?A+"":A,u),u);var l=(m,A,u)=>new Promise((i,e)=>{var n=F=>{try{r(u.next(F))}catch(E){e(E)}},s=F=>{try{r(u.throw(F))}catch(E){e(E)}},r=F=>F.done?i(F.value):Promise.resolve(F.value).then(n,s);r((u=u.apply(m,A)).next())});import{GloComm as c}from"./GloComm-7cfbdfd9.js";import{MapAttr as I}from"./MapAttr-cb594d82.js";import{SFTables as w,SFTable as N}from"./SFTable-d63f9fb4.js";import{SysEnumMain as G}from"./SysEnumMain-7dd95ff9.js";import{G as M}from"./DataType-33901a1c.js";import{PageBaseGroupNew as b,GPNReturnObj as C,GPNReturnType as o}from"./PageBaseGroupNew-ee20c033.js";import{UIContralType as p}from"./EnumLab-3cbd0812.js";import"./FrmTrack-10f0746d.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Events-141c34ea.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./SysEnum-989b6639.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";const d=class extends b{constructor(){super("GPN_NewDDL");t(this,"HelpAdminEnum",`
  #### \u5E2E\u52A9
  - \u679A\u4E3E\u5E93\u7EF4\u62A4, \u8FDB\u5165\u679A\u4E3E\u5E93\u540E\uFF0C\u5728\u5DE5\u5177\u680F\u70B9\u51FB\u65B0\u5EFA\u6309\u94AE.
  `);t(this,"HelpDBSrc",`
  #### \u5E2E\u52A9
  - \u6570\u636E\u6E90\u5206\u4E3A\u5173\u7CFB\u6570\u636E\u5E93\u7C7B\u578B\u7684\u6570\u636E\u6E90\u4E0Eweb\u670D\u52A1\u7684\u6570\u636E\u6E90.
  - \u9996\u5148\u9700\u8981\u7EF4\u62A4\u6570\u636E\u6E90\uFF0C\u7136\u540E\u5728\u6570\u636E\u6E90\u4E0A\u521B\u5EFA\u5B57\u5178.
  `);t(this,"HelpDict",`
  #### \u5E2E\u52A9
  - \u5B57\u5178\u8868\u7BA1\u7406,\u5728\u6570\u636E\u6E90\u4E0A\u521B\u5EFA\u5B57\u5178\u8868.
  - \u5982\u679C\u6CA1\u6709\u6570\u636E\u6E90\uFF0C\u8BF7\u9996\u5148\u521B\u5EFA\u6570\u636E\u6E90.
  `);t(this,"Blank",`
  #### \u5E2E\u52A9
  - \u7A7A\u767D\u7684\u5B57\u6BB5: \u7528\u4E8E\u52A0\u8F7D\u8868\u5355\u7684\u65F6\u5019\uFF0C\u4ED6\u7684\u6570\u636E\u6E90\u662F\u901A\u8FC7\uFF0C\u7531\u53C2\u6570\u7684\u5B57\u5178\u83B7\u5F97\u7684.
  - \u6BD4\u5982\uFF1A\u8868\u5355\u91CC\u7531\uFF0C\u7247\u533A\u3001\u7701\u4EFD\u3001\u5730\u5E02\u3001\u533A\u53BF\u56DB\u4E2A\u4E0B\u62C9\u6846\u5B57\u6BB5. \u5F53\u8868\u5355\u52A0\u8F7D\u7684\u65F6\u5019\uFF0C\u5728\u6CA1\u6709\u786E\u5B9A\u7247\u533A\u5176\u4ED6\u7684\u4E09\u4E2A\u5B57\u6BB5\u90FD\u65E0\u6CD5\u786E\u5B9A\u503C\u3002
  - \u7701\u4EFD\u3001\u5730\u533A\u3001\u533A\u53BF\u5C31\u9700\u8981\u7ED1\u5B9A\u7A7A\u767D\u6570\u636E\u6E90\u5916\u952E.
  - \u4F7F\u7528\u7EA7\u8054\u5173\u7CFB\uFF0C\u628A\u5176\u4ED6\u7684\u5B57\u6BB5\u6570\u636E\u5B9E\u73B0\u6570\u636E\u7EA7\u8054\u67E5\u8BE2.
  `);t(this,"Docs1",`
  #### \u5E2E\u52A9 
  - \u5F85\u5B8C\u5584.
  `);t(this,"SelectedDict_Name",`
  #### \u5E2E\u52A9
  - \u8BF7\u8F93\u5165\u8981\u751F\u6210\u7684\u5B57\u6BB5\u540D, \u683C\u5F0F:\u82F1\u6587\u5B57\u6BCD\u5F00\u5934\u4E0B\u5212\u7EBF\u6570\u5B57.
  - \u8BE5\u540D\u79F0\u5C06\u4F1A\u662F\u6570\u636E\u5E93\u7684\u771F\u5B9E\u5B57\u6BB5.
  `);t(this,"SelectedDict",`
  #### \u5E2E\u52A9
  - \u8BF7\u9009\u62E9\u4E00\u4E2A\u5916\u952E\u5B57\u5178\u8868\uFF0C\u5982\u679C\u6CA1\u6709\u8BF7\u5728\u5DE6\u4FA7\u521B\u5EFA\u4E00\u4E2A\u5B57\u5178\u8868.
  - \u521B\u5EFA\u5916\u952E\u5B57\u6BB5,\u5FC5\u987B\u9009\u5B9A\u5916\u952E\u7684\u5B57\u5178\u8868.
  - \u5217\u51FA\u6765\u7684\u90FD\u662F\u65E0\u53C2\u6570\u7684\u5B57\u5178.

  #### \u5173\u4E8E\u5916\u952E\u5B57\u5178\u8868
  - \u5177\u6709\u7F16\u7801\uFF0C\u540D\u79F0\u6570\u636E\u6211\u4EEC\u79F0\u4E3A\u5B57\u5178\u8868\uFF0C\u6BD4\u5982\uFF1A\u7247\u533A\u3001\u7701\u4EFD\u3001\u7A0E\u79CD\u3001\u7A0E\u76EE\u3001\u90E8\u95E8.
  - \u521B\u5EFA\u5916\u952E\u5B57\u5178\u8868\u5FC5\u987B\u4F9D\u6258\u4E00\u4E2A\u6570\u636E\u6E90.
  - \u4ECE\u5176\u4ED6\u7CFB\u7EDF\u83B7\u5F97\u7684\u6570\u636E, 
  - \u5916\u952E\u5B57\u5178\u8868\u53EF\u4EE5\u94FE\u63A5\u5230

  #### \u5B57\u5178\u6570\u636E\u6765\u6E90\u7C7B\u578B
  - SQL\u8BED\u53E5\uFF1A\u4ECE\u5173\u7CFB\u6570\u636E\u5E93\u4E2D\u83B7\u5F97\u6765\u7684.
  - WebApi: \u901A\u8FC7web\u670D\u52A1\u83B7\u5F97.
  - Javascript: \u901A\u8FC7\u811A\u672C\u7684function\u65B9\u6CD5\u83B7\u5F97.
  - ccform\u5185\u7F6E\u7684\u6570\u636E\u7EF4\u62A4,\u7EF4\u62A4\u5728ccbpm\u7CFB\u7EDF\u4E2D\u7684\u5B57\u5178.

  #### \u4E24\u79CD\u683C\u5F0F\u6570\u636E\u683C\u5F0F
  - \u7F16\u53F7\u540D\u79F0\u683C\u5F0F: \u6BD4\u5982\u7247\u533A\u3001\u7701\u4EFD.
  - \u6811\u7ED3\u6784\u6A21\u5F0F: \u6BD4\u5982\u90E8\u95E8\u3001\u4EA7\u54C1\u76EE\u5F55\u6811.
  #### \u6570\u636E\u6E90\u7684\u7C7B\u578B
  - \u5173\u7CFB\u6570\u636E\u5E93
  - web\u670D\u52A1.

  `);t(this,"NewStrEnum",`
  #### \u5E2E\u52A9
   - \u586B\u5199\u683C\u5F0F: \u679A\u4E3E\u503C=\u679A\u4E3E\u6807\u7B7E; 
   - \u4F8B\u5982: ty=\u56E2\u5458,dy=\u515A\u5458,qz=\u7FA4\u4F17
   - \u7CFB\u7EDF\u89E3\u6790\u4E3A: ty\u662F\u56E2\u5458, dy\u662F\u515A\u5458, qz\u662F\u7FA4\u4F17.

  #### \u6570\u636E\u5B58\u50A8.
   - string\u7C7B\u578B\u7684\u679A\u4E3E\u4E5F\u79F0\u4E3A\u6807\u8BB0\u679A\u4E3E,\u5B57\u6BCD\u5B58\u50A8\u4E00\u4E2A\u5217,\u6807\u7B7E\u5B58\u50A8\u4E00\u4E2A\u5217.
   - \u5728\u8868\u5355\u91CC\u5B57\u6BB5\u662Fabc,\u90A3\u7CFB\u7EDF\u5C31\u4F1A\u81EA\u52A8\u521B\u5EFA\u4E00\u4E2A\u5F71\u5B50\u5B57\u6BB5 abcT.
   - abc\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u6807\u8BB0, abcT\u5B58\u50A8\u7684\u662F\u6807\u7B7E.
   - \u8FD9\u4E00\u70B9\u4E0E\u5916\u90E8\u6570\u636E\u6E90\u5B58\u50A8\u4E00\u81F4.
    `);t(this,"NewIntEnum",`
  #### \u5E2E\u52A9
   - \u586B\u5199\u683C\u5F0F1: \u56E2\u5458,\u515A\u5458,\u7FA4\u4F17
   - \u7CFB\u7EDF\u89E3\u6790\u4E3A: 0\u662F\u56E2\u5458\uFF0C 1\u662F\u515A\u5458\uFF0C2\u662F\u7FA4\u4F17.
   - \u586B\u5199\u683C\u5F0F2: @0=\u56E2\u5458@1=\u515A\u5458@2=\u7FA4\u4F17
   - \u7CFB\u7EDF\u89E3\u6790\u4E3A: 10\u662F\u56E2\u5458\uFF0C 20\u662F\u515A\u5458\uFF0C30\u662F\u7FA4\u4F17\uFF0C\u8FD9\u6837\u5C31\u53EF\u4EE5\u81EA\u5DF1\u5B9A\u4E49\u679A\u4E3E\u503C.
  #### \u6570\u636E\u5B58\u50A8
   - int\u7C7B\u578B\u7684\u679A\u4E3E\u503C\u662F\u5E38\u7528\u7684\u6570\u636E\u7C7B\u578B\uFF0Cccfrom\u662F\u683C\u5F0F\u5316\u7684\u5B58\u50A8\u5230\u6570\u636E\u8868\u91CC.
   - \u521B\u5EFA\u4E00\u4E2Aint\u7C7B\u578B\u7684\u5B57\u6BB5\uFF0C\u7528\u4E8E\u5B58\u50A8\u679A\u4E3E\u7684\u6570\u636E.
    `);t(this,"Docs2",`

  #### \u5E2E\u52A9
   - \u4F9D\u6258\u5BCC\u6587\u672C\u7F16\u8F91\u5668,\u5B9E\u73B0\u5BF9\u8868\u5355\u7684\u7F16\u8F91.
   - \u4F18\u70B9:\u683C\u5F0F\u7075\u6D3B,\u5C55\u73B0\u6548\u679C\u968F\u5FC3\u6240\u6B32.
   - \u7F3A\u70B9:\u4E1A\u52A1\u4EBA\u5458\u5165\u624B\u9700\u8981\u4E00\u5B9A\u7684\u5B66\u4E60\u6210\u672C.
   - \u9002\u7528\u4E8E:\u6548\u679C
    
  `);this.PageTitle="\u65B0\u5EFA\u679A\u4E3E/\u5916\u952E\u5B57\u6BB5"}static SelectedEnum_FieldName(u,i,e,n,s,r){throw new Error("Method not implemented.")}Init(){this.AddGroup("A","\u679A\u4E3E\u5B57\u6BB5");const u=M.SQLEnumMain;this.SelectItemsByList("SelectedEnum","\u65B0\u5EFA\u679A\u4E3E\u5B57\u6BB5",d.SelectedEnum,!1,u),this.TextBox1_Name("SelectedEnum.FieldName","\u8F93\u5165\u5B57\u6BB5ID",this.SelectedDict_Name,"\u5B57\u6BB5ID",()=>this.RequestVal("tb1","SelectedEnum"),"\u82F1\u6587\u5B57\u6BCD\u6216\u8005\u4E0B\u5212\u7EBF\u5F00\u5934."),this.AddGoToUrl("AdminEnum","\u679A\u4E3E\u5E93\u7EF4\u62A4",c.UrlSearch("TS.FrmUI.SysEnumMain")),this.AddGroup("B","\u5916\u952E\u5B57\u6BB5");const i=" SELECT No, Name, DBSrcType as GroupNo FROM Sys_SFTable WHERE IsPara=0 AND No!='Blank' ";this.SelectItemsByList("SelectedDict","\u65B0\u5EFA\u5916\u952E\u5B57\u6BB5",this.SelectedDict,!1,i),this.TextBox1_Name("SelectedDict.Name","\u8F93\u5165\u5B57\u6BB5ID",this.SelectedDict_Name,"\u5B57\u6BB5ID",()=>this.RequestVal("tb1","SelectedDict"),"\u82F1\u6587\u5B57\u6BCD\u6216\u8005\u4E0B\u5212\u7EBF\u5F00\u5934."),this.TextBox2_NameNo("Blank","\u65E0\u6570\u636E\u6E90\u4E0B\u62C9\u6846",this.Blank,"FK_","\u5B57\u6BB5\u82F1\u6587\u540D","\u5B57\u6BB5\u4E2D\u6587\u540D",""),this.AddBlank("AdminDict","\u5B57\u5178\u5E93\u7EF4\u62A4",this.HelpDict),this.AddBlank("AdminDBSrc","\u6570\u636E\u6E90\u7EF4\u62A4",this.HelpDBSrc)}GenerSorts(){return l(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,i,e,n,s){return l(this,null,function*(){if(u!="SelectedEnum"){if(u=="Blank")return yield w.Init_Blank(),this.InitDDL(n,e,"Blank");if(u==="AdminEnum.ToUrl"||u==="AdminEnum")return new C(o.GoToUrl,c.UrlSearch("TS.FrmUI.SysEnumMain"));if(u==="AdminDict.ToUrl"||u==="AdminDict")return new C(o.GoToUrl,c.UrlSearch("TS.FrmUI.SFTable"));if(u==="AdminDBSrc")return new C(o.GoToUrl,c.UrlSearch("TS.Sys.SFDBSrc"));if(u==="SelectedEnum.FieldName"){const r=this.RequestVal("FrmID"),F=this.RequestVal("GroupField"),E=this.RequestVal("CtrlType");if(!e)return;const D=this.RequestVal("tb1","SelectedEnum"),T=this.RequestVal("tb2","SelectedEnum"),a=new G(D);a.No=D,yield a.Retrieve(),a.EnumKey===""&&(a.EnumKey=e);const B=new I;if(B.MyPK=r+"_"+e,yield B.IsExits())return new C(o.Error,"\u5B57\u6BB5\u5728\u8868\u5355\u5DF2\u7ECF\u5B58\u5728"+e);B.Name=T,B.KeyOfEn=e,B.FK_MapData=r,B.UIVisible=1,B.UIIsEnable=1,B.GroupID=F,B.DefVal=0,B.LGType=1,B.MyDataType=a.EnumType===0?2:1,B.SetPara("RBShowModel",3),B.UIContralType=parseInt(E)||p.DDL,B.UIBindKey=a.EnumKey,yield B.Insert();const f="/@/WF/Comm/En.vue?EnName=TS.FrmUI.MapAttrEnum&PKVal="+B.MyPK;return new C(o.GoToUrl,f)}if(u==="SelectedDict.Name"){const r=this.RequestVal("tb1","SelectedDict");return this.InitDDL(e,"",r)}}})}InitDDL(u,i,e){return l(this,null,function*(){const n=this.RequestVal("FrmID"),s=this.RequestVal("GroupField");if(!u)return;const F=new N(e);F.No=e,yield F.Retrieve();const E=new I;if(E.MyPK=n+"_"+u,(yield E.IsExits())==!0)return new C(o.Error,"\u5B57\u6BB5\u5728\u8868\u5355\u5DF2\u7ECF\u5B58\u5728"+u);i?E.Name=i:E.Name=F.Name,E.KeyOfEn=u,E.FK_MapData=n,E.UIVisible=1,E.UIIsEnable=1,E.GroupID=s,E.LGType=0,E.MyDataType=1,E.UIContralType=p.DDL,E.UIBindKey=F.No,E.SetPara("SrcType",F.DBSrcType),yield E.Insert();const y=E.MyPK;E.UIVisible=0,E.MyPK=n+"_"+u+"T",E.KeyOfEn=u+"T",E.Name=E.Name+"T",E.UIContralType=p.TB,yield E.Insert();const D="/@/WF/Comm/En.vue?EnName=TS.FrmUI.MapAttrSFSQL&PKVal="+y;return new C(o.GoToUrl,D)})}};let S=d;t(S,"SelectedEnum",`
  #### \u5E2E\u52A9 
   - \u9009\u62E9\u679A\u4E3E\u5E93\u7684\u679A\u4E3E.
   - \u5982\u679C\u6CA1\u6709\u8BE5\u6570\u636E\uFF0C\u8BF7\u70B9\u51FB\u679A\u4E3E\u5E93\u7BA1\u7406\uFF0C\u65B0\u5EFA\u679A\u4E3E.
  #### \u5B9A\u4E49.
   - \u679A\u4E3E\u5206\u4E3Aint\u7C7B\u578B\u7684\u679A\u4E3E\u4E0Estring\u7C7B\u578B\u7684\u679A\u4E3E.
   - Int\u7C7B\u578B\u7684\u679A\u4E3E:\u653F\u6CBB\u9762\u8C8C, 0=\u7FA4\u4F171=\u56E2\u5458,2=\u515A\u5458.
   - String\u7C7B\u578B\u7684\u679A\u4E3E:\u653F\u6CBB\u9762\u8C8C, qz=\u7FA4\u4F17 ty=\u56E2\u5458, dy=\u515A\u5458 .
   - \u683C\u5F0F\u4E3A(Item\u7528\u9017\u53F7\u5206\u5F00): \u4E8B\u5047,\u75C5\u5047,\u5A5A\u5047,\u5176\u5B83

  #### \u679A\u4E3E\u7684\u5B58\u50A8
   - \u679A\u4E3E\u4E3B\u8868: Sys_EnumMain 
   - \u679A\u4E3E\u4ECE\u8868: Sys_Enum
  `);export{S as GPN_NewDDL};
