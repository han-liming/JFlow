import{k as j,D as he,O as ee,b3 as ve,f as m,r as F,aq as te,d as pe,dO as B,p as ke,P as Re,a8 as p,bi as we}from"./index-f4658ae7.js";import{S as xe}from"./index-2c9d82ce.js";import{f as Se}from"./use-form-item-34ce685d.js";import{f as H,a as ye,b as $e}from"./context-3585cc4b.js";import{f as Y}from"./format-length-c9d165c6.js";import{c as V,a as _e,e as $,g as N,f as S,b as ae}from"./light-0dfdc1ad.js";import{u as Pe}from"./use-config-816d55a6.js";import{c as G}from"./index-cad90cf4.js";import{c as _}from"./create-key-bf4384d6.js";import{u as ze}from"./use-css-vars-class-3ae3b4b3.js";import{r as Ce}from"./use-merged-state-66be05d7.js";import{w as J}from"./warn-77f3ea30.js";function Ae(t,e,d){var a;const c=j(t,null);if(c===null)return;const o=(a=he())===null||a===void 0?void 0:a.proxy;ee(d,s),s(d.value),ve(()=>{s(void 0,d.value)});function s(i,n){const f=c[e];n!==void 0&&b(f,n),i!==void 0&&g(f,i)}function b(i,n){i[n]||(i[n]=[]),i[n].splice(i[n].findIndex(f=>f===o),1)}function g(i,n){i[n]||(i[n]=[]),~i[n].findIndex(f=>f===o)||i[n].push(o)}}function Ie(t){const e=j(H,null);return{mergedSize:m(()=>t.size!==void 0?t.size:(e==null?void 0:e.props.size)!==void 0?e.props.size:"medium")}}function Me(t){const e=j(H,null),d=m(()=>{const{labelPlacement:r}=t;return r!==void 0?r:e!=null&&e.props.labelPlacement?e.props.labelPlacement:"top"}),a=m(()=>d.value==="left"&&(t.labelWidth==="auto"||(e==null?void 0:e.props.labelWidth)==="auto")),c=m(()=>{if(d.value==="top")return;const{labelWidth:r}=t;if(r!==void 0&&r!=="auto")return Y(r);if(a.value){const P=e==null?void 0:e.maxChildLabelWidthRef.value;return P!==void 0?Y(P):void 0}if((e==null?void 0:e.props.labelWidth)!==void 0)return Y(e.props.labelWidth)}),o=m(()=>{const{labelAlign:r}=t;if(r)return r;if(e!=null&&e.props.labelAlign)return e.props.labelAlign}),s=m(()=>{var r;return[(r=t.labelProps)===null||r===void 0?void 0:r.style,t.labelStyle,{width:c.value}]}),b=m(()=>{const{showRequireMark:r}=t;return r!==void 0?r:e==null?void 0:e.props.showRequireMark}),g=m(()=>{const{requireMarkPlacement:r}=t;return r!==void 0?r:(e==null?void 0:e.props.requireMarkPlacement)||"right"}),i=F(!1),n=m(()=>{const{validationStatus:r}=t;if(r!==void 0)return r;if(i.value)return"error"}),f=m(()=>{const{showFeedback:r}=t;return r!==void 0?r:(e==null?void 0:e.props.showFeedback)!==void 0?e.props.showFeedback:!0}),y=m(()=>{const{showLabel:r}=t;return r!==void 0?r:(e==null?void 0:e.props.showLabel)!==void 0?e.props.showLabel:!0});return{validationErrored:i,mergedLabelStyle:s,mergedLabelPlacement:d,mergedLabelAlign:o,mergedShowRequireMark:b,mergedRequireMarkPlacement:g,mergedValidationStatus:n,mergedShowFeedback:f,mergedShowLabel:y,isAutoLabelWidth:a}}function Le(t){const e=j(H,null),d=m(()=>{const{rulePath:s}=t;if(s!==void 0)return s;const{path:b}=t;if(b!==void 0)return b}),a=m(()=>{const s=[],{rule:b}=t;if(b!==void 0&&(Array.isArray(b)?s.push(...b):s.push(b)),e){const{rules:g}=e.props,{value:i}=d;if(g!==void 0&&i!==void 0){const n=te(g,i);n!==void 0&&(Array.isArray(n)?s.push(...n):s.push(n))}}return s}),c=m(()=>a.value.some(s=>s.required)),o=m(()=>c.value||t.required);return{mergedRules:a,mergedRequired:o}}const{cubicBezierEaseInOut:Q}=_e;function qe({name:t="fade-down",fromOffset:e="-4px",enterDuration:d=".3s",leaveDuration:a=".3s",enterCubicBezier:c=Q,leaveCubicBezier:o=Q}={}){return[V(`&.${t}-transition-enter-from, &.${t}-transition-leave-to`,{opacity:0,transform:`translateY(${e})`}),V(`&.${t}-transition-enter-to, &.${t}-transition-leave-from`,{opacity:1,transform:"translateY(0)"}),V(`&.${t}-transition-leave-active`,{transition:`opacity ${a} ${o}, transform ${a} ${o}`}),V(`&.${t}-transition-enter-active`,{transition:`opacity ${d} ${c}, transform ${d} ${c}`})]}const We=$("form-item",`
 display: grid;
 line-height: var(--n-line-height);
`,[$("form-item-label",`
 grid-area: label;
 align-items: center;
 line-height: 1.25;
 text-align: var(--n-label-text-align);
 font-size: var(--n-label-font-size);
 min-height: var(--n-label-height);
 padding: var(--n-label-padding);
 color: var(--n-label-text-color);
 transition: color .3s var(--n-bezier);
 box-sizing: border-box;
 font-weight: var(--n-label-font-weight);
 `,[N("asterisk",`
 white-space: nowrap;
 user-select: none;
 -webkit-user-select: none;
 color: var(--n-asterisk-color);
 transition: color .3s var(--n-bezier);
 `),N("asterisk-placeholder",`
 grid-area: mark;
 user-select: none;
 -webkit-user-select: none;
 visibility: hidden; 
 `)]),$("form-item-blank",`
 grid-area: blank;
 min-height: var(--n-blank-height);
 `),S("auto-label-width",[$("form-item-label","white-space: nowrap;")]),S("left-labelled",`
 grid-template-areas:
 "label blank"
 "label feedback";
 grid-template-columns: auto minmax(0, 1fr);
 grid-template-rows: auto 1fr;
 align-items: start;
 `,[$("form-item-label",`
 display: grid;
 grid-template-columns: 1fr auto;
 min-height: var(--n-blank-height);
 height: auto;
 box-sizing: border-box;
 flex-shrink: 0;
 flex-grow: 0;
 `,[S("reverse-columns-space",`
 grid-template-columns: auto 1fr;
 `),S("left-mark",`
 grid-template-areas:
 "mark text"
 ". text";
 `),S("right-mark",`
 grid-template-areas: 
 "text mark"
 "text .";
 `),S("right-hanging-mark",`
 grid-template-areas: 
 "text mark"
 "text .";
 `),N("text",`
 grid-area: text; 
 `),N("asterisk",`
 grid-area: mark; 
 align-self: end;
 `)])]),S("top-labelled",`
 grid-template-areas:
 "label"
 "blank"
 "feedback";
 grid-template-rows: minmax(var(--n-label-height), auto) 1fr;
 grid-template-columns: minmax(0, 100%);
 `,[S("no-label",`
 grid-template-areas:
 "blank"
 "feedback";
 grid-template-rows: 1fr;
 `),$("form-item-label",`
 display: flex;
 align-items: flex-start;
 justify-content: var(--n-label-text-align);
 `)]),$("form-item-blank",`
 box-sizing: border-box;
 display: flex;
 align-items: center;
 position: relative;
 `),$("form-item-feedback-wrapper",`
 grid-area: feedback;
 box-sizing: border-box;
 min-height: var(--n-feedback-height);
 font-size: var(--n-feedback-font-size);
 line-height: 1.25;
 transform-origin: top left;
 `,[V("&:not(:empty)",`
 padding: var(--n-feedback-padding);
 `),$("form-item-feedback",{transition:"color .3s var(--n-bezier)",color:"var(--n-feedback-text-color)"},[S("warning",{color:"var(--n-feedback-text-color-warning)"}),S("error",{color:"var(--n-feedback-text-color-error)"}),qe({fromOffset:"-3px",enterDuration:".3s",leaveDuration:".2s"})])])]);var X=globalThis&&globalThis.__awaiter||function(t,e,d,a){function c(o){return o instanceof d?o:new d(function(s){s(o)})}return new(d||(d=Promise))(function(o,s){function b(n){try{i(a.next(n))}catch(f){s(f)}}function g(n){try{i(a.throw(n))}catch(f){s(f)}}function i(n){n.done?o(n.value):c(n.value).then(b,g)}i((a=a.apply(t,e||[])).next())})};const Fe=Object.assign(Object.assign({},ae.props),{label:String,labelWidth:[Number,String],labelStyle:[String,Object],labelAlign:String,labelPlacement:String,path:String,first:Boolean,rulePath:String,required:Boolean,showRequireMark:{type:Boolean,default:void 0},requireMarkPlacement:String,showFeedback:{type:Boolean,default:void 0},rule:[Object,Array],size:String,ignorePathChange:Boolean,validationStatus:String,feedback:String,showLabel:{type:Boolean,default:void 0},labelProps:Object});function Z(t,e){return(...d)=>{try{const a=t(...d);return!e&&(typeof a=="boolean"||a instanceof Error||Array.isArray(a))||a!=null&&a.then?a:(a===void 0||J("form-item/validate",`You return a ${typeof a} typed value in the validator method, which is not recommended. Please use `+(e?"`Promise`":"`boolean`, `Error` or `Promise`")+" typed value instead."),!0)}catch(a){J("form-item/validate","An error is catched in the validation, so the validation won't be done. Your callback in `validate` method of `n-form` or `n-form-item` won't be called in this validation.");return}}}const Je=pe({name:"FormItem",props:Fe,setup(t){Ae(ye,"formItems",B(t,"path"));const{mergedClsPrefixRef:e,inlineThemeDisabled:d}=Pe(t),a=j(H,null),c=Ie(t),o=Me(t),{validationErrored:s}=o,{mergedRequired:b,mergedRules:g}=Le(t),{mergedSize:i}=c,{mergedLabelPlacement:n,mergedLabelAlign:f,mergedRequireMarkPlacement:y}=o,r=F([]),P=F(G()),ne=a?B(a.props,"disabled"):F(!1),re=ae("Form","-form-item",We,$e,t,e);ee(B(t,"path"),()=>{t.ignorePathChange||E()});function E(){r.value=[],s.value=!1,t.feedback&&(P.value=G())}function ie(){A("blur")}function le(){A("change")}function oe(){A("focus")}function se(){A("input")}function de(l,R){return X(this,void 0,void 0,function*(){let k,h,z,M;typeof l=="string"?(k=l,h=R):l!==null&&typeof l=="object"&&(k=l.trigger,h=l.callback,z=l.shouldRuleBeApplied,M=l.options),yield new Promise((L,q)=>{A(k,z,M).then(({valid:W,errors:C})=>{W?(h&&h(),L()):(h&&h(C),q(C))})})})}const A=(l=null,R=()=>!0,k={suppressWarning:!0})=>X(this,void 0,void 0,function*(){const{path:h}=t;k?k.first||(k.first=t.first):k={};const{value:z}=g,M=a?te(a.props.model,h||""):void 0,L={},q={},W=(l?z.filter(w=>Array.isArray(w.trigger)?w.trigger.includes(l):w.trigger===l):z).filter(R).map((w,x)=>{const u=Object.assign({},w);if(u.validator&&(u.validator=Z(u.validator,!1)),u.asyncValidator&&(u.asyncValidator=Z(u.asyncValidator,!0)),u.renderMessage){const v=`__renderMessage__${x}`;q[v]=u.message,u.message=v,L[v]=u.renderMessage}return u});if(!W.length)return{valid:!0};const C=h!=null?h:"__n_no_path__",O=new xe({[C]:W}),{validateMessages:T}=(a==null?void 0:a.props)||{};return T&&O.messages(T),yield new Promise(w=>{O.validate({[C]:M},k,x=>{x!=null&&x.length?(r.value=x.map(u=>{const v=(u==null?void 0:u.message)||"";return{key:v,render:()=>v.startsWith("__renderMessage__")?L[v]():v}}),x.forEach(u=>{var v;!((v=u.message)===null||v===void 0)&&v.startsWith("__renderMessage__")&&(u.message=q[u.message])}),s.value=!0,w({valid:!1,errors:x})):(E(),w({valid:!0}))})})});ke(Se,{path:B(t,"path"),disabled:ne,mergedSize:c.mergedSize,mergedValidationStatus:o.mergedValidationStatus,restoreValidation:E,handleContentBlur:ie,handleContentChange:le,handleContentFocus:oe,handleContentInput:se});const fe={validate:de,restoreValidation:E,internalValidate:A},K=F(null);Re(()=>{if(!o.isAutoLabelWidth.value)return;const l=K.value;if(l!==null){const R=l.style.whiteSpace;l.style.whiteSpace="nowrap",l.style.width="",a==null||a.deriveMaxChildLabelWidth(Number(getComputedStyle(l).width.slice(0,-2))),l.style.whiteSpace=R}});const U=m(()=>{var l;const{value:R}=i,{value:k}=n,h=k==="top"?"vertical":"horizontal",{common:{cubicBezierEaseInOut:z},self:{labelTextColor:M,asteriskColor:L,lineHeight:q,feedbackTextColor:W,feedbackTextColorWarning:C,feedbackTextColorError:O,feedbackPadding:T,labelFontWeight:w,[_("labelHeight",R)]:x,[_("blankHeight",R)]:u,[_("feedbackFontSize",R)]:v,[_("feedbackHeight",R)]:ce,[_("labelPadding",h)]:me,[_("labelTextAlign",h)]:be,[_(_("labelFontSize",k),R)]:ge}}=re.value;let D=(l=f.value)!==null&&l!==void 0?l:be;return k==="top"&&(D=D==="right"?"flex-end":"flex-start"),{"--n-bezier":z,"--n-line-height":q,"--n-blank-height":u,"--n-label-font-size":ge,"--n-label-text-align":D,"--n-label-height":x,"--n-label-padding":me,"--n-label-font-weight":w,"--n-asterisk-color":L,"--n-label-text-color":M,"--n-feedback-padding":T,"--n-feedback-font-size":v,"--n-feedback-height":ce,"--n-feedback-text-color":W,"--n-feedback-text-color-warning":C,"--n-feedback-text-color-error":O}}),I=d?ze("form-item",m(()=>{var l;return`${i.value[0]}${n.value[0]}${((l=f.value)===null||l===void 0?void 0:l[0])||""}`}),U,t):void 0,ue=m(()=>n.value==="left"&&y.value==="left"&&f.value==="left");return Object.assign(Object.assign(Object.assign(Object.assign({labelElementRef:K,mergedClsPrefix:e,mergedRequired:b,feedbackId:P,renderExplains:r,reverseColSpace:ue},o),c),fe),{cssVars:d?void 0:U,themeClass:I==null?void 0:I.themeClass,onRender:I==null?void 0:I.onRender})},render(){const{$slots:t,mergedClsPrefix:e,mergedShowLabel:d,mergedShowRequireMark:a,mergedRequireMarkPlacement:c,onRender:o}=this,s=a!==void 0?a:this.mergedRequired;o==null||o();const b=()=>{const g=this.$slots.label?this.$slots.label():this.label;if(!g)return null;const i=p("span",{class:`${e}-form-item-label__text`},g),n=s?p("span",{class:`${e}-form-item-label__asterisk`},c!=="left"?"\xA0*":"*\xA0"):c==="right-hanging"&&p("span",{class:`${e}-form-item-label__asterisk-placeholder`},"\xA0*"),{labelProps:f}=this;return p("label",Object.assign({},f,{class:[f==null?void 0:f.class,`${e}-form-item-label`,`${e}-form-item-label--${c}-mark`,this.reverseColSpace&&`${e}-form-item-label--reverse-columns-space`],style:this.mergedLabelStyle,ref:"labelElementRef"}),c==="left"?[n,i]:[i,n])};return p("div",{class:[`${e}-form-item`,this.themeClass,`${e}-form-item--${this.mergedSize}-size`,`${e}-form-item--${this.mergedLabelPlacement}-labelled`,this.isAutoLabelWidth&&`${e}-form-item--auto-label-width`,!d&&`${e}-form-item--no-label`],style:this.cssVars},d&&b(),p("div",{class:[`${e}-form-item-blank`,this.mergedValidationStatus&&`${e}-form-item-blank--${this.mergedValidationStatus}`]},t),this.mergedShowFeedback?p("div",{key:this.feedbackId,class:`${e}-form-item-feedback-wrapper`},p(we,{name:"fade-down-transition",mode:"out-in"},{default:()=>{const{mergedValidationStatus:g}=this;return Ce(t.feedback,i=>{var n;const{feedback:f}=this,y=i||f?p("div",{key:"__feedback__",class:`${e}-form-item-feedback__line`},i||f):this.renderExplains.length?(n=this.renderExplains)===null||n===void 0?void 0:n.map(({key:r,render:P})=>p("div",{key:r,class:`${e}-form-item-feedback__line`},P())):null;return y?g==="warning"?p("div",{key:"controlled-warning",class:`${e}-form-item-feedback ${e}-form-item-feedback--warning`},y):g==="error"?p("div",{key:"controlled-error",class:`${e}-form-item-feedback ${e}-form-item-feedback--error`},y):g==="success"?p("div",{key:"controlled-success",class:`${e}-form-item-feedback ${e}-form-item-feedback--success`},y):p("div",{key:"controlled-default",class:`${e}-form-item-feedback`},y):null})}})):null)}});export{Je as N};
