import{d as I,r as w,a8 as i,f as $,k as Fe,G as He,p as ot,dO as we,O as nt,bi as at,s as lt,J as it}from"./index-f4658ae7.js";import{j as st,k as ut,t as q,r as D,s as N,v as X,w as P,x as oe,y as Z,z as se,A as ye,B as Se,D as Ce,c as te,e as m,g as U,f as De,b as Be}from"./light-0dfdc1ad.js";import{a as ne,o as ae,g as ct}from"./Scrollbar-35d51129.js";import{a as dt,u as ht}from"./use-config-816d55a6.js";import{i as pt,N as ft}from"./Input-52773133.js";import{N as gt}from"./InputGroup-5bb233c2.js";import{w as vt}from"./warn-77f3ea30.js";import{f as mt}from"./fade-in-scale-up.cssr-0b26e361.js";import{u as bt}from"./use-form-item-34ce685d.js";import{u as xt}from"./Loading-fead3a83.js";import{u as Pe,c as de}from"./use-merged-state-66be05d7.js";import{c as Ie}from"./create-key-bf4384d6.js";import{u as kt}from"./use-css-vars-class-3ae3b4b3.js";import{i as wt}from"./use-is-mounted-a34b74be.js";import{u as Ue,b as yt,a as St,V as Ct,c as Ut}from"./Follower-3b5f0c65.js";import{b as $t,N as he}from"./Button-53926a3b.js";function Te(e,t,o){t/=100,o/=100;const r=t*Math.min(o,1-o)+o;return[e,r?(2-2*o/r)*100:0,r*100]}function ge(e,t,o){t/=100,o/=100;const r=o-o*t/2,n=Math.min(r,1-r);return[e,n?(o-r)/n*100:0,r*100]}function T(e,t,o){t/=100,o/=100;let r=(n,l=(n+e/60)%6)=>o-o*t*Math.max(Math.min(l,4-l,1),0);return[r(5)*255,r(3)*255,r(1)*255]}function $e(e,t,o){e/=255,t/=255,o/=255;let r=Math.max(e,t,o),n=r-Math.min(e,t,o),l=n&&(r==e?(t-o)/n:r==t?2+(o-e)/n:4+(e-t)/n);return[60*(l<0?l+6:l),r&&n/r*100,r*100]}function Re(e,t,o){e/=255,t/=255,o/=255;let r=Math.max(e,t,o),n=r-Math.min(e,t,o),l=1-Math.abs(r+r-n-1),s=n&&(r==e?(t-o)/n:r==t?2+(o-e)/n:4+(e-t)/n);return[60*(s<0?s+6:s),l?n/l*100:0,(r+r-n)*50]}function Ae(e,t,o){t/=100,o/=100;let r=t*Math.min(o,1-o),n=(l,s=(l+e/30)%12)=>o-r*Math.max(Math.min(s-3,9-s,1),-1);return[n(0)*255,n(8)*255,n(4)*255]}const Rt=e=>{const{fontSize:t,boxShadow2:o,popoverColor:r,textColor2:n,borderRadius:l,borderColor:s,heightSmall:u,heightMedium:h,heightLarge:A,fontSizeSmall:E,fontSizeMedium:V,fontSizeLarge:_,dividerColor:G}=e;return{panelFontSize:t,boxShadow:o,color:r,textColor:n,borderRadius:l,border:`1px solid ${s}`,heightSmall:u,heightMedium:h,heightLarge:A,fontSizeSmall:E,fontSizeMedium:V,fontSizeLarge:_,dividerColor:G}},At=st({name:"ColorPicker",common:ut,peers:{Input:pt,Button:$t},self:Rt}),Vt=At;function zt(e,t){switch(e[0]){case"hex":return t?"#000000FF":"#000000";case"rgb":return t?"rgba(0, 0, 0, 1)":"rgb(0, 0, 0)";case"hsl":return t?"hsla(0, 0%, 0%, 1)":"hsl(0, 0%, 0%)";case"hsv":return t?"hsva(0, 0%, 0%, 1)":"hsv(0, 0%, 0%)"}return"#000000"}function ue(e){return e===null?null:/^ *#/.test(e)?"hex":e.includes("rgb")?"rgb":e.includes("hsl")?"hsl":e.includes("hsv")?"hsv":null}function Mt(e){return e=Math.round(e),e>=360?359:e<0?0:e}function Dt(e){return e=Math.round(e*100)/100,e>1?1:e<0?0:e}const Pt={rgb:{hex(e){return q(D(e))},hsl(e){const[t,o,r,n]=D(e);return N([...Re(t,o,r),n])},hsv(e){const[t,o,r,n]=D(e);return X([...$e(t,o,r),n])}},hex:{rgb(e){return P(D(e))},hsl(e){const[t,o,r,n]=D(e);return N([...Re(t,o,r),n])},hsv(e){const[t,o,r,n]=D(e);return X([...$e(t,o,r),n])}},hsl:{hex(e){const[t,o,r,n]=oe(e);return q([...Ae(t,o,r),n])},rgb(e){const[t,o,r,n]=oe(e);return P([...Ae(t,o,r),n])},hsv(e){const[t,o,r,n]=oe(e);return X([...Te(t,o,r),n])}},hsv:{hex(e){const[t,o,r,n]=Z(e);return q([...T(t,o,r),n])},rgb(e){const[t,o,r,n]=Z(e);return P([...T(t,o,r),n])},hsl(e){const[t,o,r,n]=Z(e);return N([...ge(t,o,r),n])}}};function qe(e,t,o){return o=o||ue(e),o?o===t?e:Pt[o][t](e):null}const re="12px",It=12,j="6px",_t=6,Ft="linear-gradient(90deg,red,#ff0 16.66%,#0f0 33.33%,#0ff 50%,#00f 66.66%,#f0f 83.33%,red)",Ht=I({name:"HueSlider",props:{clsPrefix:{type:String,required:!0},hue:{type:Number,required:!0},onUpdateHue:{type:Function,required:!0},onComplete:Function},setup(e){const t=w(null);function o(l){t.value&&(ne("mousemove",document,r),ne("mouseup",document,n),r(l))}function r(l){const{value:s}=t;if(!s)return;const{width:u,left:h}=s.getBoundingClientRect(),A=Mt((l.clientX-h-_t)/(u-It)*360);e.onUpdateHue(A)}function n(){var l;ae("mousemove",document,r),ae("mouseup",document,n),(l=e.onComplete)===null||l===void 0||l.call(e)}return{railRef:t,handleMouseDown:o}},render(){const{clsPrefix:e}=this;return i("div",{class:`${e}-color-picker-slider`,style:{height:re,borderRadius:j}},i("div",{ref:"railRef",style:{boxShadow:"inset 0 0 2px 0 rgba(0, 0, 0, .24)",boxSizing:"border-box",backgroundImage:Ft,height:re,borderRadius:j,position:"relative"},onMousedown:this.handleMouseDown},i("div",{style:{position:"absolute",left:j,right:j,top:0,bottom:0}},i("div",{class:`${e}-color-picker-handle`,style:{left:`calc((${this.hue}%) / 359 * 100 - ${j})`,borderRadius:j,width:re,height:re}},i("div",{class:`${e}-color-picker-handle__fill`,style:{backgroundColor:`hsl(${this.hue}, 100%, 50%)`,borderRadius:j,width:re,height:re}})))))}}),ie="12px",Bt=12,K="6px",Tt=I({name:"AlphaSlider",props:{clsPrefix:{type:String,required:!0},rgba:{type:Array,default:null},alpha:{type:Number,default:0},onUpdateAlpha:{type:Function,required:!0},onComplete:Function},setup(e){const t=w(null);function o(l){!t.value||!e.rgba||(ne("mousemove",document,r),ne("mouseup",document,n),r(l))}function r(l){const{value:s}=t;if(!s)return;const{width:u,left:h}=s.getBoundingClientRect(),A=(l.clientX-h)/(u-Bt);e.onUpdateAlpha(Dt(A))}function n(){var l;ae("mousemove",document,r),ae("mouseup",document,n),(l=e.onComplete)===null||l===void 0||l.call(e)}return{railRef:t,railBackgroundImage:$(()=>{const{rgba:l}=e;return l?`linear-gradient(to right, rgba(${l[0]}, ${l[1]}, ${l[2]}, 0) 0%, rgba(${l[0]}, ${l[1]}, ${l[2]}, 1) 100%)`:""}),handleMouseDown:o}},render(){const{clsPrefix:e}=this;return i("div",{class:`${e}-color-picker-slider`,ref:"railRef",style:{height:ie,borderRadius:K},onMousedown:this.handleMouseDown},i("div",{style:{borderRadius:K,position:"absolute",left:0,right:0,top:0,bottom:0,overflow:"hidden"}},i("div",{class:`${e}-color-picker-checkboard`}),i("div",{class:`${e}-color-picker-slider__image`,style:{backgroundImage:this.railBackgroundImage}})),this.rgba&&i("div",{style:{position:"absolute",left:K,right:K,top:0,bottom:0}},i("div",{class:`${e}-color-picker-handle`,style:{left:`calc(${this.alpha*100}% - ${K})`,borderRadius:K,width:ie,height:ie}},i("div",{class:`${e}-color-picker-handle__fill`,style:{backgroundColor:P(this.rgba),borderRadius:K,width:ie,height:ie}}))))}}),pe="12px",fe="6px",qt=I({name:"Pallete",props:{clsPrefix:{type:String,required:!0},rgba:{type:Array,default:null},displayedHue:{type:Number,required:!0},displayedSv:{type:Array,required:!0},onUpdateSV:{type:Function,required:!0},onComplete:Function},setup(e){const t=w(null);function o(l){t.value&&(ne("mousemove",document,r),ne("mouseup",document,n),r(l))}function r(l){const{value:s}=t;if(!s)return;const{width:u,height:h,left:A,bottom:E}=s.getBoundingClientRect(),V=(E-l.clientY)/h,_=(l.clientX-A)/u,G=100*(_>1?1:_<0?0:_),J=100*(V>1?1:V<0?0:V);e.onUpdateSV(G,J)}function n(){var l;ae("mousemove",document,r),ae("mouseup",document,n),(l=e.onComplete)===null||l===void 0||l.call(e)}return{palleteRef:t,handleColor:$(()=>{const{rgba:l}=e;return l?`rgb(${l[0]}, ${l[1]}, ${l[2]})`:""}),handleMouseDown:o}},render(){const{clsPrefix:e}=this;return i("div",{class:`${e}-color-picker-pallete`,onMousedown:this.handleMouseDown,ref:"palleteRef"},i("div",{class:`${e}-color-picker-pallete__layer`,style:{backgroundImage:`linear-gradient(90deg, white, hsl(${this.displayedHue}, 100%, 50%))`}}),i("div",{class:`${e}-color-picker-pallete__layer ${e}-color-picker-pallete__layer--shadowed`,style:{backgroundImage:"linear-gradient(180deg, rgba(0, 0, 0, 0%), rgba(0, 0, 0, 100%))"}}),this.rgba&&i("div",{class:`${e}-color-picker-handle`,style:{width:pe,height:pe,borderRadius:fe,left:`calc(${this.displayedSv[0]}% - ${fe})`,bottom:`calc(${this.displayedSv[1]}% - ${fe})`}},i("div",{class:`${e}-color-picker-handle__fill`,style:{backgroundColor:this.handleColor,borderRadius:fe,width:pe,height:pe}})))}}),Ve=dt("n-color-picker");function Nt(e){return/^\d{1,3}\.?\d*$/.test(e.trim())?Math.max(0,Math.min(parseInt(e),255)):!1}function Et(e){return/^\d{1,3}\.?\d*$/.test(e.trim())?Math.max(0,Math.min(parseInt(e),360)):!1}function Ot(e){return/^\d{1,3}\.?\d*$/.test(e.trim())?Math.max(0,Math.min(parseInt(e),100)):!1}function Lt(e){const t=e.trim();return/^#[0-9a-fA-F]+$/.test(t)?[4,5,7,9].includes(t.length):!1}function jt(e){return/^\d{1,3}\.?\d*%$/.test(e.trim())?Math.max(0,Math.min(parseInt(e)/100,100)):!1}const Kt={paddingSmall:"0 4px"},_e=I({name:"ColorInputUnit",props:{label:{type:String,required:!0},value:{type:[Number,String],default:null},showAlpha:Boolean,onUpdateValue:{type:Function,required:!0}},setup(e){const t=w(""),{themeRef:o}=Fe(Ve,null);He(()=>{t.value=r()});function r(){const{value:s}=e;if(s===null)return"";const{label:u}=e;return u==="HEX"?s:u==="A"?`${Math.floor(s*100)}%`:String(Math.floor(s))}function n(s){t.value=s}function l(s){let u,h;switch(e.label){case"HEX":h=Lt(s),h&&e.onUpdateValue(s),t.value=r();break;case"H":u=Et(s),u===!1?t.value=r():e.onUpdateValue(u);break;case"S":case"L":case"V":u=Ot(s),u===!1?t.value=r():e.onUpdateValue(u);break;case"A":u=jt(s),u===!1?t.value=r():e.onUpdateValue(u);break;case"R":case"G":case"B":u=Nt(s),u===!1?t.value=r():e.onUpdateValue(u);break}}return{mergedTheme:o,inputValue:t,handleInputChange:l,handleInputUpdateValue:n}},render(){const{mergedTheme:e}=this;return i(ft,{size:"small",placeholder:this.label,theme:e.peers.Input,themeOverrides:e.peerOverrides.Input,builtinThemeOverrides:Kt,value:this.inputValue,onUpdateValue:this.handleInputUpdateValue,onChange:this.handleInputChange,style:this.label==="A"?"flex-grow: 1.25;":""})}}),Xt=I({name:"ColorInput",props:{clsPrefix:{type:String,required:!0},mode:{type:String,required:!0},modes:{type:Array,required:!0},showAlpha:{type:Boolean,required:!0},value:{type:String,default:null},valueArr:{type:Array,default:null},onUpdateValue:{type:Function,required:!0},onUpdateMode:{type:Function,required:!0}},setup(e){return{handleUnitUpdateValue(t,o){const{showAlpha:r}=e;if(e.mode==="hex"){e.onUpdateValue((r?q:se)(o));return}let n;switch(e.valueArr===null?n=[0,0,0,0]:n=Array.from(e.valueArr),e.mode){case"hsv":n[t]=o,e.onUpdateValue((r?X:Ce)(n));break;case"rgb":n[t]=o,e.onUpdateValue((r?P:Se)(n));break;case"hsl":n[t]=o,e.onUpdateValue((r?N:ye)(n));break}}}},render(){const{clsPrefix:e,modes:t}=this;return i("div",{class:`${e}-color-picker-input`},i("div",{class:`${e}-color-picker-input__mode`,onClick:this.onUpdateMode,style:{cursor:t.length===1?"":"pointer"}},this.mode.toUpperCase()+(this.showAlpha?"A":"")),i(gt,null,{default:()=>{const{mode:o,valueArr:r,showAlpha:n}=this;if(o==="hex"){let l=null;try{l=r===null?null:(n?q:se)(r)}catch(s){}return i(_e,{label:"HEX",showAlpha:n,value:l,onUpdateValue:s=>{this.handleUnitUpdateValue(0,s)}})}return(o+(n?"a":"")).split("").map((l,s)=>i(_e,{label:l.toUpperCase(),value:r===null?null:r[s],onUpdateValue:u=>{this.handleUnitUpdateValue(s,u)}}))}}))}}),Zt=I({name:"ColorPickerTrigger",props:{clsPrefix:{type:String,required:!0},value:{type:String,default:null},hsla:{type:Array,default:null},disabled:Boolean,onClick:Function},setup(e){const{colorPickerSlots:t,renderLabelRef:o}=Fe(Ve,null);return()=>{const{hsla:r,value:n,clsPrefix:l,onClick:s,disabled:u}=e,h=t.label||o.value;return i("div",{class:[`${l}-color-picker-trigger`,u&&`${l}-color-picker-trigger--disabled`],onClick:u?void 0:s},i("div",{class:`${l}-color-picker-trigger__fill`},i("div",{class:`${l}-color-picker-checkboard`}),i("div",{style:{position:"absolute",left:0,right:0,top:0,bottom:0,backgroundColor:r?N(r):""}}),n&&r?i("div",{class:`${l}-color-picker-trigger__value`,style:{color:r[2]>50||r[3]<.5?"black":"white"}},h?h(n):n):null))}}});function Gt(e,t){if(t==="hsv"){const[o,r,n,l]=Z(e);return P([...T(o,r,n),l])}return e}function Jt(e){const t=document.createElement("canvas").getContext("2d");return t.fillStyle=e,t.fillStyle}const Yt=I({name:"ColorPickerSwatches",props:{clsPrefix:{type:String,required:!0},mode:{type:String,required:!0},swatches:{type:Array,required:!0},onUpdateColor:{type:Function,required:!0}},setup(e){const t=$(()=>e.swatches.map(l=>{const s=ue(l);return{value:l,mode:s,legalValue:Gt(l,s)}}));function o(l){const{mode:s}=e;let{value:u,mode:h}=l;return h||(h="hex",/^[a-zA-Z]+$/.test(u)?u=Jt(u):(vt("color-picker",`color ${u} in swatches is invalid.`),u="#000000")),h===s?u:qe(u,s,h)}function r(l){e.onUpdateColor(o(l))}function n(l,s){l.key==="Enter"&&r(s)}return{parsedSwatchesRef:t,handleSwatchSelect:r,handleSwatchKeyDown:n}},render(){const{clsPrefix:e}=this;return i("div",{class:`${e}-color-picker-swatches`},this.parsedSwatchesRef.map(t=>i("div",{class:`${e}-color-picker-swatch`,tabindex:0,onClick:()=>{this.handleSwatchSelect(t)},onKeydown:o=>{this.handleSwatchKeyDown(o,t)}},i("div",{class:`${e}-color-picker-swatch__fill`,style:{background:t.legalValue}}))))}}),Qt=I({name:"ColorPreview",props:{clsPrefix:{type:String,required:!0},mode:{type:String,required:!0},color:{type:String,default:null,validator:e=>{const t=ue(e);return!!(!e||t&&t!=="hsv")}},onUpdateColor:{type:Function,required:!0}},setup(e){function t(o){var r;const n=o.target.value;(r=e.onUpdateColor)===null||r===void 0||r.call(e,qe(n.toUpperCase(),e.mode,"hex")),o.stopPropagation()}return{handleChange:t}},render(){const{clsPrefix:e}=this;return i("div",{class:`${e}-color-picker-preview__preview`},i("span",{class:`${e}-color-picker-preview__fill`,style:{background:this.color||"#000000"}}),i("input",{class:`${e}-color-picker-preview__input`,type:"color",value:this.color,onChange:this.handleChange}))}}),Wt=te([m("color-picker",`
 display: inline-block;
 box-sizing: border-box;
 height: var(--n-height);
 font-size: var(--n-font-size);
 width: 100%;
 position: relative;
 `),m("color-picker-panel",`
 margin: 4px 0;
 width: 240px;
 font-size: var(--n-panel-font-size);
 color: var(--n-text-color);
 background-color: var(--n-color);
 transition:
 box-shadow .3s var(--n-bezier),
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 box-shadow: var(--n-box-shadow);
 `,[mt(),m("input",`
 text-align: center;
 `)]),m("color-picker-checkboard",`
 background: white; 
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 `,[te("&::after",`
 background-image: linear-gradient(45deg, #DDD 25%, #0000 25%), linear-gradient(-45deg, #DDD 25%, #0000 25%), linear-gradient(45deg, #0000 75%, #DDD 75%), linear-gradient(-45deg, #0000 75%, #DDD 75%);
 background-size: 12px 12px;
 background-position: 0 0, 0 6px, 6px -6px, -6px 0px;
 background-repeat: repeat;
 content: "";
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 `)]),m("color-picker-slider",`
 margin-bottom: 8px;
 position: relative;
 box-sizing: border-box;
 `,[U("image",`
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 `),te("&::after",`
 content: "";
 position: absolute;
 border-radius: inherit;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 box-shadow: inset 0 0 2px 0 rgba(0, 0, 0, .24);
 pointer-events: none;
 `)]),m("color-picker-handle",`
 z-index: 1;
 box-shadow: 0 0 2px 0 rgba(0, 0, 0, .45);
 position: absolute;
 background-color: white;
 overflow: hidden;
 `,[U("fill",`
 box-sizing: border-box;
 border: 2px solid white;
 `)]),m("color-picker-pallete",`
 height: 180px;
 position: relative;
 margin-bottom: 8px;
 cursor: crosshair;
 `,[U("layer",`
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 `,[De("shadowed",`
 box-shadow: inset 0 0 2px 0 rgba(0, 0, 0, .24);
 `)])]),m("color-picker-preview",`
 display: flex;
 `,[U("sliders",`
 flex: 1 0 auto;
 `),U("preview",`
 position: relative;
 height: 30px;
 width: 30px;
 margin: 0 0 8px 6px;
 border-radius: 50%;
 box-shadow: rgba(0, 0, 0, .15) 0px 0px 0px 1px inset;
 overflow: hidden;
 `),U("fill",`
 display: block;
 width: 30px;
 height: 30px;
 `),U("input",`
 position: absolute;
 top: 0;
 left: 0;
 width: 30px;
 height: 30px;
 opacity: 0;
 z-index: 1;
 `)]),m("color-picker-input",`
 display: flex;
 align-items: center;
 `,[m("input",`
 flex-grow: 1;
 flex-basis: 0;
 `),U("mode",`
 width: 72px;
 text-align: center;
 `)]),m("color-picker-control",`
 padding: 12px;
 `),m("color-picker-action",`
 display: flex;
 margin-top: -4px;
 border-top: 1px solid var(--n-divider-color);
 padding: 8px 12px;
 justify-content: flex-end;
 `,[m("button","margin-left: 8px;")]),m("color-picker-trigger",`
 border: var(--n-border);
 height: 100%;
 box-sizing: border-box;
 border-radius: var(--n-border-radius);
 transition: border-color .3s var(--n-bezier);
 cursor: pointer;
 `,[U("value",`
 white-space: nowrap;
 position: relative;
 `),U("fill",`
 border-radius: var(--n-border-radius);
 position: absolute;
 display: flex;
 align-items: center;
 justify-content: center;
 left: 4px;
 right: 4px;
 top: 4px;
 bottom: 4px;
 `),De("disabled","cursor: not-allowed"),m("color-picker-checkboard",`
 border-radius: var(--n-border-radius);
 `,[te("&::after",`
 --n-block-size: calc((var(--n-height) - 8px) / 3);
 background-size: calc(var(--n-block-size) * 2) calc(var(--n-block-size) * 2);
 background-position: 0 0, 0 var(--n-block-size), var(--n-block-size) calc(-1 * var(--n-block-size)), calc(-1 * var(--n-block-size)) 0px; 
 `)])]),m("color-picker-swatches",`
 display: grid;
 grid-gap: 8px;
 flex-wrap: wrap;
 position: relative;
 grid-template-columns: repeat(auto-fill, 18px);
 margin-top: 10px;
 `,[m("color-picker-swatch",`
 width: 18px;
 height: 18px;
 background-image: linear-gradient(45deg, #DDD 25%, #0000 25%), linear-gradient(-45deg, #DDD 25%, #0000 25%), linear-gradient(45deg, #0000 75%, #DDD 75%), linear-gradient(-45deg, #0000 75%, #DDD 75%);
 background-size: 8px 8px;
 background-position: 0px 0, 0px 4px, 4px -4px, -4px 0px;
 background-repeat: repeat;
 `,[U("fill",`
 position: relative;
 width: 100%;
 height: 100%;
 border-radius: 3px;
 box-shadow: rgba(0, 0, 0, .15) 0px 0px 0px 1px inset;
 cursor: pointer;
 `),te("&:focus",`
 outline: none;
 `,[U("fill",[te("&::after",`
 position: absolute;
 top: 0;
 right: 0;
 bottom: 0;
 left: 0;
 background: inherit;
 filter: blur(2px);
 content: "";
 `)])])])])]),er=Object.assign(Object.assign({},Be.props),{value:String,show:{type:Boolean,default:void 0},defaultShow:Boolean,defaultValue:String,modes:{type:Array,default:()=>["rgb","hex","hsl"]},placement:{type:String,default:"bottom-start"},to:Ue.propTo,showAlpha:{type:Boolean,default:!0},showPreview:Boolean,swatches:Array,disabled:{type:Boolean,default:void 0},actions:{type:Array,default:null},internalActions:Array,size:String,renderLabel:Function,onComplete:Function,onConfirm:Function,"onUpdate:show":[Function,Array],onUpdateShow:[Function,Array],"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array]}),mr=I({name:"ColorPicker",props:er,setup(e,{slots:t}){const o=w(null);let r=null;const n=bt(e),{mergedSizeRef:l,mergedDisabledRef:s}=n,{localeRef:u}=xt("global"),{mergedClsPrefixRef:h,namespaceRef:A,inlineThemeDisabled:E}=ht(e),V=Be("ColorPicker","-color-picker",Wt,Vt,e,h);ot(Ve,{themeRef:V,renderLabelRef:we(e,"renderLabel"),colorPickerSlots:t});const _=w(e.defaultShow),G=Pe(we(e,"show"),_);function J(a){const{onUpdateShow:c,"onUpdate:show":p}=e;c&&de(c,a),p&&de(p,a),_.value=a}const{defaultValue:ze}=e,Me=w(ze===void 0?zt(e.modes,e.showAlpha):ze),x=Pe(we(e,"value"),Me),Y=w([x.value]),z=w(0),ve=$(()=>ue(x.value)),{modes:Ne}=e,R=w(ue(x.value)||Ne[0]||"rgb");function Ee(){const{modes:a}=e,{value:c}=R,p=a.findIndex(f=>f===c);~p?R.value=a[(p+1)%a.length]:R.value="rgb"}let y,S,Q,W,F,H,B,C;const le=$(()=>{const{value:a}=x;if(!a)return null;switch(ve.value){case"hsv":return Z(a);case"hsl":return[y,S,Q,C]=oe(a),[...Te(y,S,Q),C];case"rgb":case"hex":return[F,H,B,C]=D(a),[...$e(F,H,B),C]}}),O=$(()=>{const{value:a}=x;if(!a)return null;switch(ve.value){case"rgb":case"hex":return D(a);case"hsv":return[y,S,W,C]=Z(a),[...T(y,S,W),C];case"hsl":return[y,S,Q,C]=oe(a),[...Ae(y,S,Q),C]}}),me=$(()=>{const{value:a}=x;if(!a)return null;switch(ve.value){case"hsl":return oe(a);case"hsv":return[y,S,W,C]=Z(a),[...ge(y,S,W),C];case"rgb":case"hex":return[F,H,B,C]=D(a),[...Re(F,H,B),C]}}),Oe=$(()=>{switch(R.value){case"rgb":case"hex":return O.value;case"hsv":return le.value;case"hsl":return me.value}}),ce=w(0),be=w(1),xe=w([0,0]);function Le(a,c){const{value:p}=le,f=ce.value,g=p?p[3]:1;xe.value=[a,c];const{showAlpha:d}=e;switch(R.value){case"hsv":v((d?X:Ce)([f,a,c,g]),"cursor");break;case"hsl":v((d?N:ye)([...ge(f,a,c),g]),"cursor");break;case"rgb":v((d?P:Se)([...T(f,a,c),g]),"cursor");break;case"hex":v((d?q:se)([...T(f,a,c),g]),"cursor");break}}function je(a){ce.value=a;const{value:c}=le;if(!c)return;const[,p,f,g]=c,{showAlpha:d}=e;switch(R.value){case"hsv":v((d?X:Ce)([a,p,f,g]),"cursor");break;case"rgb":v((d?P:Se)([...T(a,p,f),g]),"cursor");break;case"hex":v((d?q:se)([...T(a,p,f),g]),"cursor");break;case"hsl":v((d?N:ye)([...ge(a,p,f),g]),"cursor");break}}function Ke(a){switch(R.value){case"hsv":[y,S,W]=le.value,v(X([y,S,W,a]),"cursor");break;case"rgb":[F,H,B]=O.value,v(P([F,H,B,a]),"cursor");break;case"hex":[F,H,B]=O.value,v(q([F,H,B,a]),"cursor");break;case"hsl":[y,S,Q]=me.value,v(N([y,S,Q,a]),"cursor");break}be.value=a}function v(a,c){c==="cursor"?r=a:r=null;const{nTriggerFormChange:p,nTriggerFormInput:f}=n,{onUpdateValue:g,"onUpdate:value":d}=e;g&&de(g,a),d&&de(d,a),p(),f(),Me.value=a}function Xe(a){v(a,"input"),it(ee)}function ee(a=!0){const{value:c}=x;if(c){const{nTriggerFormChange:p,nTriggerFormInput:f}=n,{onComplete:g}=e;g&&g(c);const{value:d}=Y,{value:k}=z;a&&(d.splice(k+1,d.length,c),z.value=k+1),p(),f()}}function Ze(){const{value:a}=z;a-1<0||(v(Y.value[a-1],"input"),ee(!1),z.value=a-1)}function Ge(){const{value:a}=z;a<0||a+1>=Y.value.length||(v(Y.value[a+1],"input"),ee(!1),z.value=a+1)}function Je(){v(null,"input"),J(!1)}function Ye(){const{value:a}=x,{onConfirm:c}=e;c&&c(a),J(!1)}const Qe=$(()=>z.value>=1),We=$(()=>{const{value:a}=Y;return a.length>1&&z.value<a.length-1});nt(G,a=>{a||(Y.value=[x.value],z.value=0)}),He(()=>{if(!(r&&r===x.value)){const{value:a}=le;a&&(ce.value=a[0],be.value=a[3],xe.value=[a[1],a[2]])}r=null});const ke=$(()=>{const{value:a}=l,{common:{cubicBezierEaseInOut:c},self:{textColor:p,color:f,panelFontSize:g,boxShadow:d,border:k,borderRadius:b,dividerColor:L,[Ie("height",a)]:tt,[Ie("fontSize",a)]:rt}}=V.value;return{"--n-bezier":c,"--n-text-color":p,"--n-color":f,"--n-panel-font-size":g,"--n-font-size":rt,"--n-box-shadow":d,"--n-border":k,"--n-border-radius":b,"--n-height":tt,"--n-divider-color":L}}),M=E?kt("color-picker",$(()=>l.value[0]),ke,e):void 0;function et(){var a;const{value:c}=O,{value:p}=ce,{internalActions:f,modes:g,actions:d}=e,{value:k}=V,{value:b}=h;return i("div",{class:[`${b}-color-picker-panel`,M==null?void 0:M.themeClass.value],onDragstart:L=>{L.preventDefault()},style:E?void 0:ke.value},i("div",{class:`${b}-color-picker-control`},i(qt,{clsPrefix:b,rgba:c,displayedHue:p,displayedSv:xe.value,onUpdateSV:Le,onComplete:ee}),i("div",{class:`${b}-color-picker-preview`},i("div",{class:`${b}-color-picker-preview__sliders`},i(Ht,{clsPrefix:b,hue:p,onUpdateHue:je,onComplete:ee}),e.showAlpha?i(Tt,{clsPrefix:b,rgba:c,alpha:be.value,onUpdateAlpha:Ke,onComplete:ee}):null),e.showPreview?i(Qt,{clsPrefix:b,mode:R.value,color:O.value&&se(O.value),onUpdateColor:L=>{v(L,"input")}}):null),i(Xt,{clsPrefix:b,showAlpha:e.showAlpha,mode:R.value,modes:g,onUpdateMode:Ee,value:x.value,valueArr:Oe.value,onUpdateValue:Xe}),((a=e.swatches)===null||a===void 0?void 0:a.length)&&i(Yt,{clsPrefix:b,mode:R.value,swatches:e.swatches,onUpdateColor:L=>{v(L,"input")}})),d!=null&&d.length?i("div",{class:`${b}-color-picker-action`},d.includes("confirm")&&i(he,{size:"small",onClick:Ye,theme:k.peers.Button,themeOverrides:k.peerOverrides.Button},{default:()=>u.value.confirm}),d.includes("clear")&&i(he,{size:"small",onClick:Je,disabled:!x.value,theme:k.peers.Button,themeOverrides:k.peerOverrides.Button},{default:()=>u.value.clear})):null,t.action?i("div",{class:`${b}-color-picker-action`},{default:t.action}):f?i("div",{class:`${b}-color-picker-action`},f.includes("undo")&&i(he,{size:"small",onClick:Ze,disabled:!Qe.value,theme:k.peers.Button,themeOverrides:k.peerOverrides.Button},{default:()=>u.value.undo}),f.includes("redo")&&i(he,{size:"small",onClick:Ge,disabled:!We.value,theme:k.peers.Button,themeOverrides:k.peerOverrides.Button},{default:()=>u.value.redo})):null)}return{mergedClsPrefix:h,namespace:A,selfRef:o,hsla:me,rgba:O,mergedShow:G,mergedDisabled:s,isMounted:wt(),adjustedTo:Ue(e),mergedValue:x,handleTriggerClick(){J(!0)},handleClickOutside(a){var c;!((c=o.value)===null||c===void 0)&&c.contains(ct(a))||J(!1)},renderPanel:et,cssVars:E?void 0:ke,themeClass:M==null?void 0:M.themeClass,onRender:M==null?void 0:M.onRender}},render(){const{$slots:e,mergedClsPrefix:t,onRender:o}=this;return o==null||o(),i("div",{class:[this.themeClass,`${t}-color-picker`],ref:"selfRef",style:this.cssVars},i(yt,null,{default:()=>[i(St,null,{default:()=>i(Zt,{clsPrefix:t,value:this.mergedValue,hsla:this.hsla,disabled:this.mergedDisabled,onClick:this.handleTriggerClick},{label:e.label})}),i(Ct,{placement:this.placement,show:this.mergedShow,containerClass:this.namespace,teleportDisabled:this.adjustedTo===Ue.tdkey,to:this.adjustedTo},{default:()=>i(at,{name:"fade-in-scale-up-transition",appear:this.isMounted},{default:()=>this.mergedShow?lt(this.renderPanel(),[[Ut,this.handleClickOutside,void 0,{capture:!0}]]):null})})]}))}});export{mr as N};
