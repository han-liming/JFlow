var o=Object.defineProperty;var a=(r,e,u)=>e in r?o(r,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):r[e]=u;var E=(r,e,u)=>(a(r,typeof e!="symbol"?e+"":e,u),u);import{UAC as F}from"./UAC-8e255d47.js";import{Map as A}from"./Map-73575e6b.js";import{EntityMyPK as l}from"./EntityMyPK-e742fec8.js";import{a as t}from"./MapExt-db8cd7f3.js";import{SFDBSrc as p}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class b extends l{constructor(u){super("TS.MapExt.PopTree");E(this,"NoteSearchTip",` 
 
  #### \u5E2E\u52A9


   - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57\uFF0C\u6BD4\u5982:
   - \u8BF7\u8F93\u5165\u4ED8\u6B3E\u4EBA\u540D\u79F0,\u8FDB\u884C\u641C\u7D22\u3002
   - \u8F93\u5165\u4EBA\u5458\u7F16\u53F7,\u540D\u79F0\uFF0C\u540D\u79F0\u5168\u62FC,\u7B80\u62FC\u5173\u952E\u5B57\u641C\u7D22\u3002
   
   `);E(this,"NotTag1",` 
 
  #### \u5E2E\u52A9
 
 
   - \u70B9\u51FB\u5173\u952E\u5B57\u6267\u884C\u641C\u7D22\u8FD4\u56DE\u7684\u6570\u636E\u6E90\uFF0C@Key\u662F\u5173\u952E\u5B57,\u662F\u641C\u7D22\u7684\u5173\u952E\u5B57.
   - For URL:/DataUser/Handler.ashx?DoType=SearchEmps&Keyword=@Key
   - For SQL: SELECT No,Name FROM Port_Emp WHERE No like '%@Key%' OR Name like '%@Key%'
    
    `);E(this,"NotTag2",` 
 
  #### \u5E2E\u52A9
 
 
   -\u8BBE\u7F6E\u4E00\u4E2A\u53EF\u4EE5\u8FD4\u56DEjson\u7684\u6570\u636E\u6E90\u8BE5\u6570\u636E\u6E90\u6709No,Name,ParentNo\u4E09\u4E2A\u7EA6\u5B9A\u7684\u5217.
   - For URL:/DataUser/Handler.ashx?DoType=ReqDepts
   - For SQL:SELECT No,Name, ParentNo FROM Port_Dept
     
    `);E(this,"NotDoc",` 
 
  #### \u5E2E\u52A9
 
 
   - \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F,\u6BD4\u5982:@WebUser.DeptNo , @FieldName @WebUser.OrgNo
    
    `);E(this,"NotTag",` 
 
  #### \u5E2E\u52A9
 
 
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5728\u53F3\u4E0A\u89D2\u7684\u5217\u8868\u91CC\u67E5\u8BE2\u6216\u70B9\u6811\u6811\u5E72\u7684\u6570\u636E\u6E90\u51FA\u73B0\u7684\u5217\u8868,\u9700\u8981\u7528\u4E2D\u6587\u663E\u793A\u5217\u5934.
   - \u4F8B\u5982: No=\u7F16\u53F7,Name=\u540D\u79F0,Addr=\u5730\u5740,Tel=\u7535\u8BDD,Email=\u90AE\u4EF6

    `);E(this,"NotTag5",` 
 
  #### \u5E2E\u52A9
 
   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5F39\u51FA\u6846\u786E\u5B9A\u540E\u6267\u884C\u7684JS\uFF0C\u53EF\u4EE5\u76F4\u63A5\u5199\u65B9\u6CD5\u540D\u6216\u8005\u65B9\u6CD5. 

  `);u&&(this.MyPK=u)}get HisUAC(){const u=new F;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new A("Sys_MapExt","\u6811\u5E72\u5F39\u7A97");return u.AddGroupAttr("\u6570\u636E\u6765\u6E90"),u.AddMyPK(),u.AddTBString(t.FK_MapData,null,"\u8868\u5355ID",!1,!1,0,50,200),u.AddTBString(t.ExtModel,"Pop","\u6A21\u5F0F(\u5927\u7C7B)",!1,!1,0,50,200),u.AddTBString(t.ExtType,null,"\u7C7B\u578B(\u5C0F\u7C7B)",!1,!1,0,50,200),u.AddDDLEntities(t.FK_DBSrc,"local","\u6570\u636E\u6E90",new p,!0,null,!1),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.NoteSearchTip),u.AddTBStringDoc(t.Tag2,null,"\u6811\u5E72\u5217\u8868\u6570\u636E\u6E90 ",!0,!1,!0,this.NotTag2),u.AddTBString(t.Doc,null,"\u6839\u76EE\u5F55\u6811\u7F16\u53F7",!0,!1,0,50,200,!0,this.NotDoc),u.AddTBString(t.Tag,null,"\u5217\u540D\u4E2D\u6587\u5BF9\u7167",!0,!1,0,50,200,!0,this.NotTag),u.AddTBString(t.Tag5,null,"\u786E\u5B9A\u540E\u6267\u884C\u7684JS",!0,!1,0,50,200,!0,this.NotTag5),u.AddGroupAttr("\u5916\u89C2"),u.AddRadioBtn("ShowModel",0,"\u5C55\u793A\u65B9\u5F0F",!0,!0,"ShowModel","@0=POP\u5F39\u51FA\u7A97@1=\u4E0B\u62C9\u641C\u7D22\u9009\u62E9",null,!0),u.AddRadioBtn("PopSelectType",1,"\u9009\u62E9\u7C7B\u578B",!0,!0,"PopSelectType","@0=\u5355\u9009@1=\u591A\u9009",null,!0),u.AddTBString("Title",null,"\u6807\u9898",!0,!1,0,50,200,!0),u.AddTBString("BtnLab","\u67E5\u627E","\u67E5\u627E\u6309\u94AE\u6807\u7B7E",!0,!1,0,50,200),u.AddTBInt(t.H,400,"\u5F39\u7A97\u9AD8\u5EA6",!0,!1),u.AddTBInt(t.W,500,"\u5F39\u7A97\u5BBD\u5EA6",!0,!1),u.AddTBAtParas(4e3),u.ParaFields=",PopSelectType,Title,BtnLab,SearchTip,ShowModel,PopSelectType,",this._enMap=u,this._enMap}}export{b as PopTree};
