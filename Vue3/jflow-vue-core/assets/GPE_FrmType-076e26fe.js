var p=Object.defineProperty;var r=(t,u,F)=>u in t?p(t,u,{enumerable:!0,configurable:!0,writable:!0,value:F}):t[u]=F;var E=(t,u,F)=>(r(t,typeof u!="symbol"?u+"":u,F),F);import{MapData as o,MapDataAttr as m}from"./MapData-4fa397be.js";import{PageBaseGroupEdit as e}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./EnumLab-4f91f91c.js";import"./GloComm-7cfbdfd9.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class w extends e{constructor(){super("GPE_FrmType");E(this,"Desc0",`  
  #### \u5E2E\u52A9
  - \u4F7F\u7528\u8868\u683C\u7684\u65B9\u5F0F\u7F16\u8F91\u6570\u636E\uFF0C\u5982\u4E0B\u56FE.
  - \u9002\u7528\u4E8E\u5217\u8F83\u5C11\uFF0C\u6570\u636E\u91CF\u5C0F\uFF0C\u7F16\u8F91\u7B80\u5355\u76F4\u89C2. 
  #### \u56FE\u4F8B
  - \u8868\u683C\u6A21\u5F0F
  -
  `);E(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u4F7F\u7528\u8868\u5355\u7684\u65B9\u5F0F\u7F16\u8F91\u6570\u636E\uFF0C\u5982\u4E0B\u56FE.
  - \u9002\u7528\u4E8E\u5217\u8F83\u591A\uFF0C\u6709\u5B59\u8868\uFF0C\u7F16\u8F91\u65B0\u5EFA\u9700\u8981\u5F39\u7A97.  
  #### \u5217\u8868\u56FE\u4F8B
  - \u70B9\u51FB\u7EA2\u8272\u7684\u533A\u57DF\uFF0C\u65B0\u5EFA\u4E0E\u7F16\u8F91.
  - \u5217\u8868\u7684\u6570\u636E\u90FD\u662F\u53EA\u8BFB\u7684.
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/EditModel/Card1.png "\u8868\u683C\u6A21\u5F0F")  
  #### \u7F16\u8F91\u56FE\u4F8B
  - \u4ECE\u8868\u5C31\u662F\u4E00\u4E2A\u65B0\u7684\u8868\u5355.
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapDtl/EditModel/Card2.png "\u8868\u683C\u6A21\u5F0F")  
  - \u53EF\u4EE5\u4F7F\u7528\u3010\u4FDD\u5B58\u5E76\u65B0\u5EFA\u3011\uFF0C\u3010\u5220\u9664\u3011\u7B49\u64CD\u4F5C.
  `);E(this,"Desc2",`
  #### \u5E2E\u52A9
  - \u540C\u7ECF\u5178\u8868\u5355\uFF0C\u53EA\u662F\u8868\u5355\u7684\u5C55\u793A\u4E0D\u540C.
  `);this.PageTitle="\u8868\u5355\u5DE5\u4F5C\u6A21\u5F0F"}Init(){this.entity=new o,this.KeyOfEn=m.FrmType,this.AddGroup("A","\u8868\u5355\u5DE5\u4F5C\u6A21\u5F0F"),this.Blank("0","\u7ECF\u5178\u8868\u5355",this.HelpUn),this.Blank("9","\u5F00\u53D1\u8005\u8868\u5355",this.HelpUn),this.Blank("10","\u7AE0\u8282\u8868\u5355",this.HelpUn),this.Blank("6","VSTO\u8868\u5355",this.HelpUn)}BtnClick(F,i,C){}AfterSave(F,i){}}export{w as GPE_FrmType};
