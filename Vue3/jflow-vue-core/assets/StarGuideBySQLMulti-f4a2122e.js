var a=Object.defineProperty;var o=(e,t,u)=>t in e?a(e,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):e[t]=u;var E=(e,t,u)=>(o(e,typeof t!="symbol"?t+"":t,u),u);import{UAC as m}from"./UAC-8e255d47.js";import{Map as F}from"./Map-73575e6b.js";import{FlowAttr as r}from"./Flow-6121039a.js";import{EntityNoName as i}from"./EntityNoName-d08126ae.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./BSEntity-840a884b.js";import"./Entities-6a72b013.js";class w extends i{constructor(u){super("TS.AttrFlow.StarGuideBySQLMulti");E(this,"DescTag1",`
  #### \u5E2E\u52A9
   - \u6BD4\u5982:SELECT No, Name, No as EmpNo,Name as EmpName,Email FROM WF_Emp WHERE No LIKE '%@key%'
   - \u521D\u59CB\u5316\u5217\u8868\u53C2\u6570\uFF0C\u8BE5\u67E5\u8BE2\u8BED\u53E5\u5FC5\u987B\u6709No,Name\u4E24\u4E2A\u5217\uFF0C\u6CE8\u610F\u663E\u793A\u6570\u91CF\u9650\u5236\u3002
   - \u5F88\u591A\u573A\u5408\u4E0B\u9700\u8981\u7528\u5230\u7236\u5B50\u6D41\u7A0B\uFF0C\u5728\u542F\u52A8\u5B50\u6D41\u7A0B\u7684\u65F6\u5019\u9700\u8981\u9009\u62E9\u4E00\u4E2A\u7236\u6D41\u7A0B\u3002
   - \u5B9E\u4F8B:SELECT a.WorkID as No, a.Title as Name, a.Starter, a.WorkID As PWorkID, '011' as PFlowNo, a.FK_Node as PNodeID FROM WF_GenerWorkflow a, WF_GenerWorkerlist b WHERE A.WorkID=b.WorkID AND B.FK_Emp='@WebUser.No' AND B.IsPass=0 AND A.FK_Flow='011' AND a.Title Like '%@Key%'


   
  `);E(this,"DescTag2",`
  #### \u5E2E\u52A9
   - \u6BD4\u5982:SELECT top 15 No,Name ,No as EmpNo,Name as EmpName ,Email FROM WF_Emp
   - \u6216\u8005:SELECT No,Name ,No as EmpNo,Name as EmpName ,Email FROM WF_Emp WHERE ROWID < 15
   - \u8BE5\u6570\u636E\u6E90\u5FC5\u987B\u6709No,Name\u4E24\u4E2A\u5217, \u5176\u4ED6\u7684\u5217\u8981\u4E0E\u5F00\u59CB\u8282\u70B9\u8868\u5355\u5B57\u6BB5\u5BF9\u5E94\u3002
   - \u6CE8\u610F\u67E5\u8BE2\u7684\u6570\u91CF\uFF0C\u907F\u514D\u592A\u591A\u5F71\u54CD\u6548\u7387\u3002


  `);u&&this.setPKVal(u)}get HisUAC(){const u=new m;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new F("WF_Flow","\u6309\u8BBE\u7F6E\u7684SQL-\u5355\u6761\u6A21\u5F0F");return u.AddTBStringPK(r.No,null,"\u6D41\u7A0B\u7F16\u53F7",!0,!0,0,10,100,!1),u.AddTBString(r.Name,null,"\u540D\u79F0",!0,!0,0,50,200,!1),u.AddTBString(r.StartGuidePara1,null,"\u67E5\u8BE2\u53C2\u6570",!0,!1,0,50,200,!0,this.DescTag1),u.AddTBString(r.StartGuidePara2,null,"\u521D\u59CB\u5316\u5217\u8868\u53C2\u6570",!0,!1,0,50,200,!0,this.DescTag2),this._enMap=u,this._enMap}}export{w as StarGuideBySQLMulti};
