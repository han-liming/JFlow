import{p as o,k as n}from"./index-f4658ae7.js";const e=Symbol("basic-table");function r(t){o(e,t)}function c(){return n(e)}export{r as createTableContext,c as useTableContext};
