var n=Object.defineProperty;var A=(F,t,u)=>t in F?n(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var p=(F,t,u)=>(A(F,typeof t!="symbol"?t+"":t,u),u);var B=(F,t,u)=>new Promise((o,E)=>{var e=i=>{try{r(u.next(i))}catch(m){E(m)}},l=i=>{try{r(u.throw(i))}catch(m){E(m)}},r=i=>i.done?o(i.value):Promise.resolve(i.value).then(e,l);r((u=u.apply(F,t)).next())});import{b as C,a as s}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as a}from"./PageBaseGroupEdit-202e8e85.js";import{GPEAutoFullDtlField as D}from"./GPEAutoFullDtlField-b1a20ee8.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class X extends a{constructor(){super("GPE_AutoFullDtlField");p(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u4E0D\u542F\u7528\uFF1A\u4E0D\u5BF9\u4E3B\u8868\u5B57\u6BB5\u8FDB\u884C\u4ECE\u8868\u5217\u7684\u6570\u5B66\u8BA1\u7B97\u3002
   - \u5BF9\u4ECE\u8868\u5217\u6C42\u503C\uFF1A\u5F53\u524D\u662F\u4E3B\u8868\u5B57\u6BB5\uFF0C\u5BF9\u4ECE\u8868\u7684\u5217\u8FDB\u884C\u6C42\u548C\u3001\u5E73\u5747\u3001\u6700\u5927\u3001\u6700\u5C0F\u8BA1\u7B97. 
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDtlField/Img/AutoFull.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);p(this,"Desc1",`

  #### \u5E2E\u52A9
  - \u5F53\u524D\u662F\u4E3B\u8868\u5B57\u6BB5\uFF0C\u5BF9\u4ECE\u8868\u7684\u5217\u8FDB\u884C\u6C42\u548C\u3001\u5E73\u5747\u3001\u6700\u5927\u3001\u6700\u5C0F\u8BA1\u7B97.  
  - \u70B9\u51FB\u8BBE\u7F6E\u8FDB\u5165\u8BE6\u7EC6\u8BBE\u7F6E.
  - \u914D\u7F6E\u56FE\u4F8B1
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDtlField/Img/AutoFullBiaodan.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u914D\u7F6E\u56FE\u4F8B2
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDtlField/Img/AutoFullBiaodan2.png "\u5C4F\u5E55\u622A\u56FE.png") 

  - \u8FD0\u884C\u56FE\u4F8B
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/AutoFullDtlField/Img/AutoFull.png "\u5C4F\u5E55\u622A\u56FE.png") 

  `);this.PageTitle="\u5BF9\u4ECE\u8868\u5217\u6C42\u503C"}Init(){return B(this,null,function*(){this.entity=new C,this.KeyOfEn=s.DoWay,yield this.entity.InitDataForMapAttr("NumEnterLimit",this.GetRequestVal("PKVal")),this.AddGroup("A","\u5BF9\u4ECE\u8868\u5217\u6C42\u503C"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.AddEntity("1","\u542F\u7528\u8BBE\u7F6E",new D,this.Desc1)})}AfterSave(u,o){if(u==o)throw new Error("Method not implemented.")}BtnClick(u,o,E){if(u==o||u===E)throw new Error("Method not implemented.")}}export{X as GPE_AutoFullDtlField};
