import ht from"./SimpleLineIcons-dabcdd07.js";import mt from"./GlyphIcons-fb95b955.js";import xt from"./FontAwesomeIcons-88ac90b4.js";import yt from"./Tabs-ec1c48f8.js";import"./RadioGroup-a80e69a5.js";import"./FormTheme-70eb47bf.js";import{a as Ct,u as St}from"./use-config-816d55a6.js";import{t as wt}from"./warn-77f3ea30.js";import{d as N,r as z,a8 as c,k as Be,f as te,aP as Pt,F as ae,O as ne,J as ie,p as _t,dO as M,G as zt,s as Tt,v as Lt,bh as Rt,f1 as $t,o as G,g as E,b as J,w as Q,u as j,j as A,bt as se,x as le,t as de,aT as Bt}from"./index-f4658ae7.js";import{N as Wt}from"./Icon-e3cbad7d.js";import{A as kt}from"./Add-4d1c6932.js";import{r as At}from"./render-ee8eb435.js";import{N as Vt}from"./Close-c51bd8a8.js";import{o as It}from"./omit-b0e7e098.js";import{u as jt,k as Mt,e as r,f as i,c as h,g as R,h as Gt,b as We}from"./light-0dfdc1ad.js";import{u as Pe}from"./use-compitable-1a225331.js";import{f as ce}from"./flatten-2bdfb3d3.js";import{u as Et,r as _e,c as Z}from"./use-merged-state-66be05d7.js";import{t as be}from"./throttle-35369e52.js";import{o as Ht}from"./on-fonts-ready-d897575d.js";import{c as W}from"./create-key-bf4384d6.js";import{a as ee}from"./index-22809599.js";import{u as Ft}from"./use-css-vars-class-3ae3b4b3.js";import{V as ze}from"./VResizeObserver-e3ad0bab.js";import{c as Ot,a as Te}from"./cssr-e43ee704.js";const Dt=Te(".v-x-scroll",{overflow:"auto",scrollbarWidth:"none"},[Te("&::-webkit-scrollbar",{width:0,height:0})]),Nt=N({name:"XScroll",props:{disabled:Boolean,onScroll:Function},setup(){const e=z(null);function n(d){!(d.currentTarget.offsetWidth<d.currentTarget.scrollWidth)||d.deltaY===0||(d.currentTarget.scrollLeft+=d.deltaY+d.deltaX,d.preventDefault())}const b=jt();return Dt.mount({id:"vueuc/x-scroll",head:!0,anchorMetaName:Ot,ssr:b}),Object.assign({selfRef:e,handleWheel:n},{scrollTo(...d){var m;(m=e.value)===null||m===void 0||m.scrollTo(...d)}})},render(){return c("div",{ref:"selfRef",onScroll:this.onScroll,onWheel:this.disabled?void 0:this.handleWheel,class:"v-x-scroll"},this.$slots)}}),Ut={tabFontSizeSmall:"14px",tabFontSizeMedium:"14px",tabFontSizeLarge:"16px",tabGapSmallLine:"36px",tabGapMediumLine:"36px",tabGapLargeLine:"36px",tabGapSmallLineVertical:"8px",tabGapMediumLineVertical:"8px",tabGapLargeLineVertical:"8px",tabPaddingSmallLine:"6px 0",tabPaddingMediumLine:"10px 0",tabPaddingLargeLine:"14px 0",tabPaddingVerticalSmallLine:"6px 12px",tabPaddingVerticalMediumLine:"8px 16px",tabPaddingVerticalLargeLine:"10px 20px",tabGapSmallBar:"36px",tabGapMediumBar:"36px",tabGapLargeBar:"36px",tabGapSmallBarVertical:"8px",tabGapMediumBarVertical:"8px",tabGapLargeBarVertical:"8px",tabPaddingSmallBar:"4px 0",tabPaddingMediumBar:"6px 0",tabPaddingLargeBar:"10px 0",tabPaddingVerticalSmallBar:"6px 12px",tabPaddingVerticalMediumBar:"8px 16px",tabPaddingVerticalLargeBar:"10px 20px",tabGapSmallCard:"4px",tabGapMediumCard:"4px",tabGapLargeCard:"4px",tabGapSmallCardVertical:"4px",tabGapMediumCardVertical:"4px",tabGapLargeCardVertical:"4px",tabPaddingSmallCard:"8px 16px",tabPaddingMediumCard:"10px 20px",tabPaddingLargeCard:"12px 24px",tabPaddingSmallSegment:"4px 0",tabPaddingMediumSegment:"6px 0",tabPaddingLargeSegment:"8px 0",tabPaddingVerticalLargeSegment:"0 8px",tabPaddingVerticalSmallCard:"8px 12px",tabPaddingVerticalMediumCard:"10px 16px",tabPaddingVerticalLargeCard:"12px 20px",tabPaddingVerticalSmallSegment:"0 4px",tabPaddingVerticalMediumSegment:"0 6px",tabGapSmallSegment:"0",tabGapMediumSegment:"0",tabGapLargeSegment:"0",tabGapSmallSegmentVertical:"0",tabGapMediumSegmentVertical:"0",tabGapLargeSegmentVertical:"0",panePaddingSmall:"8px 0 0 0",panePaddingMedium:"12px 0 0 0",panePaddingLarge:"16px 0 0 0",closeSize:"18px",closeIconSize:"14px"},Xt=e=>{const{textColor2:n,primaryColor:b,textColorDisabled:p,closeIconColor:d,closeIconColorHover:m,closeIconColorPressed:l,closeColorHover:v,closeColorPressed:P,tabColor:C,baseColor:x,dividerColor:S,fontWeight:g,textColor1:u,borderRadius:y,fontSize:T,fontWeightStrong:L}=e;return Object.assign(Object.assign({},Ut),{colorSegment:C,tabFontSizeCard:T,tabTextColorLine:u,tabTextColorActiveLine:b,tabTextColorHoverLine:b,tabTextColorDisabledLine:p,tabTextColorSegment:u,tabTextColorActiveSegment:n,tabTextColorHoverSegment:n,tabTextColorDisabledSegment:p,tabTextColorBar:u,tabTextColorActiveBar:b,tabTextColorHoverBar:b,tabTextColorDisabledBar:p,tabTextColorCard:u,tabTextColorHoverCard:u,tabTextColorActiveCard:b,tabTextColorDisabledCard:p,barColor:b,closeIconColor:d,closeIconColorHover:m,closeIconColorPressed:l,closeColorHover:v,closeColorPressed:P,closeBorderRadius:y,tabColor:C,tabColorSegment:x,tabBorderColor:S,tabFontWeightActive:g,tabFontWeight:g,tabBorderRadius:y,paneTextColor:n,fontWeightStrong:L})},Yt={name:"Tabs",common:Mt,self:Xt},Kt=Yt,ve=Ct("n-tabs"),ke={tab:[String,Number,Object,Function],name:{type:[String,Number],required:!0},disabled:Boolean,displayDirective:{type:String,default:"if"},closable:{type:Boolean,default:void 0},tabProps:Object,label:[String,Number,Object,Function]},pe=N({__TAB_PANE__:!0,name:"TabPane",alias:["TabPanel"],props:ke,setup(e){const n=Be(ve,null);return n||wt("tab-pane","`n-tab-pane` must be placed inside `n-tabs`."),{style:n.paneStyleRef,class:n.paneClassRef,mergedClsPrefix:n.mergedClsPrefixRef}},render(){return c("div",{class:[`${this.mergedClsPrefix}-tab-pane`,this.class],style:this.style},this.$slots)}}),qt=Object.assign({internalLeftPadded:Boolean,internalAddable:Boolean,internalCreatedByPane:Boolean},It(ke,["displayDirective"])),ue=N({__TAB__:!0,inheritAttrs:!1,name:"Tab",props:qt,setup(e){const{mergedClsPrefixRef:n,valueRef:b,typeRef:p,closableRef:d,tabStyleRef:m,tabChangeIdRef:l,onBeforeLeaveRef:v,triggerRef:P,handleAdd:C,activateTab:x,handleClose:S}=Be(ve);return{trigger:P,mergedClosable:te(()=>{if(e.internalAddable)return!1;const{closable:g}=e;return g===void 0?d.value:g}),style:m,clsPrefix:n,value:b,type:p,handleClose(g){g.stopPropagation(),!e.disabled&&S(e.name)},activateTab(){if(e.disabled)return;if(e.internalAddable){C();return}const{name:g}=e,u=++l.id;if(g!==b.value){const{value:y}=v;y?Promise.resolve(y(e.name,b.value)).then(T=>{T&&l.id===u&&x(g)}):x(g)}}}},render(){const{internalAddable:e,clsPrefix:n,name:b,disabled:p,label:d,tab:m,value:l,mergedClosable:v,style:P,trigger:C,$slots:{default:x}}=this,S=d!=null?d:m;return c("div",{class:`${n}-tabs-tab-wrapper`},this.internalLeftPadded?c("div",{class:`${n}-tabs-tab-pad`}):null,c("div",Object.assign({key:b,"data-name":b,"data-disabled":p?!0:void 0},Pt({class:[`${n}-tabs-tab`,l===b&&`${n}-tabs-tab--active`,p&&`${n}-tabs-tab--disabled`,v&&`${n}-tabs-tab--closable`,e&&`${n}-tabs-tab--addable`],onClick:C==="click"?this.activateTab:void 0,onMouseenter:C==="hover"?this.activateTab:void 0,style:e?void 0:P},this.internalCreatedByPane?this.tabProps||{}:this.$attrs)),c("span",{class:`${n}-tabs-tab__label`},e?c(ae,null,c("div",{class:`${n}-tabs-tab__height-placeholder`},"\xA0"),c(Wt,{clsPrefix:n},{default:()=>c(kt,null)})):x?x():typeof S=="object"?S:At(S!=null?S:b)),v&&this.type==="card"?c(Vt,{clsPrefix:n,class:`${n}-tabs-tab__close`,onClick:this.handleClose,disabled:p}):null))}}),Jt=r("tabs",`
 box-sizing: border-box;
 width: 100%;
 display: flex;
 flex-direction: column;
 transition:
 background-color .3s var(--n-bezier),
 border-color .3s var(--n-bezier);
`,[i("segment-type",[r("tabs-rail",[h("&.transition-disabled","color: red;",[r("tabs-tab",`
 transition: none;
 `)])])]),i("top",[r("tab-pane",`
 padding: var(--n-pane-padding-top) var(--n-pane-padding-right) var(--n-pane-padding-bottom) var(--n-pane-padding-left);
 `)]),i("left",[r("tab-pane",`
 padding: var(--n-pane-padding-right) var(--n-pane-padding-bottom) var(--n-pane-padding-left) var(--n-pane-padding-top);
 `)]),i("left, right",`
 flex-direction: row;
 `,[r("tabs-bar",`
 width: 2px;
 right: 0;
 transition:
 top .2s var(--n-bezier),
 max-height .2s var(--n-bezier),
 background-color .3s var(--n-bezier);
 `),r("tabs-tab",`
 padding: var(--n-tab-padding-vertical); 
 `)]),i("right",`
 flex-direction: row-reverse;
 `,[r("tab-pane",`
 padding: var(--n-pane-padding-left) var(--n-pane-padding-top) var(--n-pane-padding-right) var(--n-pane-padding-bottom);
 `),r("tabs-bar",`
 left: 0;
 `)]),i("bottom",`
 flex-direction: column-reverse;
 justify-content: flex-end;
 `,[r("tab-pane",`
 padding: var(--n-pane-padding-bottom) var(--n-pane-padding-right) var(--n-pane-padding-top) var(--n-pane-padding-left);
 `),r("tabs-bar",`
 top: 0;
 `)]),r("tabs-rail",`
 padding: 3px;
 border-radius: var(--n-tab-border-radius);
 width: 100%;
 background-color: var(--n-color-segment);
 transition: background-color .3s var(--n-bezier);
 display: flex;
 align-items: center;
 `,[r("tabs-tab-wrapper",`
 flex-basis: 0;
 flex-grow: 1;
 display: flex;
 align-items: center;
 justify-content: center;
 `,[r("tabs-tab",`
 overflow: hidden;
 border-radius: var(--n-tab-border-radius);
 width: 100%;
 display: flex;
 align-items: center;
 justify-content: center;
 `,[i("active",`
 font-weight: var(--n-font-weight-strong);
 color: var(--n-tab-text-color-active);
 background-color: var(--n-tab-color-segment);
 box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .08);
 `),h("&:hover",`
 color: var(--n-tab-text-color-hover);
 `)])])]),i("flex",[r("tabs-nav",{width:"100%"},[r("tabs-wrapper",{width:"100%"},[r("tabs-tab",{marginRight:0})])])]),r("tabs-nav",`
 box-sizing: border-box;
 line-height: 1.5;
 display: flex;
 transition: border-color .3s var(--n-bezier);
 `,[R("prefix, suffix",`
 display: flex;
 align-items: center;
 `),R("prefix","padding-right: 16px;"),R("suffix","padding-left: 16px;")]),i("top, bottom",[r("tabs-nav-scroll-wrapper",[h("&::before",`
 top: 0;
 bottom: 0;
 left: 0;
 width: 20px;
 `),h("&::after",`
 top: 0;
 bottom: 0;
 right: 0;
 width: 20px;
 `),i("shadow-start",[h("&::before",`
 box-shadow: inset 10px 0 8px -8px rgba(0, 0, 0, .12);
 `)]),i("shadow-end",[h("&::after",`
 box-shadow: inset -10px 0 8px -8px rgba(0, 0, 0, .12);
 `)])])]),i("left, right",[r("tabs-nav-scroll-wrapper",[h("&::before",`
 top: 0;
 left: 0;
 right: 0;
 height: 20px;
 `),h("&::after",`
 bottom: 0;
 left: 0;
 right: 0;
 height: 20px;
 `),i("shadow-start",[h("&::before",`
 box-shadow: inset 0 10px 8px -8px rgba(0, 0, 0, .12);
 `)]),i("shadow-end",[h("&::after",`
 box-shadow: inset 0 -10px 8px -8px rgba(0, 0, 0, .12);
 `)])])]),r("tabs-nav-scroll-wrapper",`
 flex: 1;
 position: relative;
 overflow: hidden;
 `,[r("tabs-nav-y-scroll",`
 height: 100%;
 width: 100%;
 overflow-y: auto; 
 scrollbar-width: none;
 `,[h("&::-webkit-scrollbar",`
 width: 0;
 height: 0;
 `)]),h("&::before, &::after",`
 transition: box-shadow .3s var(--n-bezier);
 pointer-events: none;
 content: "";
 position: absolute;
 z-index: 1;
 `)]),r("tabs-nav-scroll-content",`
 display: flex;
 position: relative;
 min-width: 100%;
 width: fit-content;
 box-sizing: border-box;
 `),r("tabs-wrapper",`
 display: inline-flex;
 flex-wrap: nowrap;
 position: relative;
 `),r("tabs-tab-wrapper",`
 display: flex;
 flex-wrap: nowrap;
 flex-shrink: 0;
 flex-grow: 0;
 `),r("tabs-tab",`
 cursor: pointer;
 white-space: nowrap;
 flex-wrap: nowrap;
 display: inline-flex;
 align-items: center;
 color: var(--n-tab-text-color);
 font-size: var(--n-tab-font-size);
 background-clip: padding-box;
 padding: var(--n-tab-padding);
 transition:
 box-shadow .3s var(--n-bezier),
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 border-color .3s var(--n-bezier);
 `,[i("disabled",{cursor:"not-allowed"}),R("close",`
 margin-left: 6px;
 transition:
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 `),R("label",`
 display: flex;
 align-items: center;
 `)]),r("tabs-bar",`
 position: absolute;
 bottom: 0;
 height: 2px;
 border-radius: 1px;
 background-color: var(--n-bar-color);
 transition:
 left .2s var(--n-bezier),
 max-width .2s var(--n-bezier),
 background-color .3s var(--n-bezier);
 `,[h("&.transition-disabled",`
 transition: none;
 `),i("disabled",`
 background-color: var(--n-tab-text-color-disabled)
 `)]),r("tabs-pane-wrapper",`
 position: relative;
 overflow: hidden;
 transition: max-height .2s var(--n-bezier);
 `),r("tab-pane",`
 color: var(--n-pane-text-color);
 width: 100%;
 transition:
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 opacity .2s var(--n-bezier);
 left: 0;
 right: 0;
 top: 0;
 `,[h("&.next-transition-leave-active, &.prev-transition-leave-active, &.next-transition-enter-active, &.prev-transition-enter-active",`
 transition:
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 transform .2s var(--n-bezier),
 opacity .2s var(--n-bezier);
 `),h("&.next-transition-leave-active, &.prev-transition-leave-active",`
 position: absolute;
 `),h("&.next-transition-enter-from, &.prev-transition-leave-to",`
 transform: translateX(32px);
 opacity: 0;
 `),h("&.next-transition-leave-to, &.prev-transition-enter-from",`
 transform: translateX(-32px);
 opacity: 0;
 `),h("&.next-transition-leave-from, &.next-transition-enter-to, &.prev-transition-leave-from, &.prev-transition-enter-to",`
 transform: translateX(0);
 opacity: 1;
 `)]),r("tabs-tab-pad",`
 box-sizing: border-box;
 width: var(--n-tab-gap);
 flex-grow: 0;
 flex-shrink: 0;
 `),i("line-type, bar-type",[r("tabs-tab",`
 font-weight: var(--n-tab-font-weight);
 box-sizing: border-box;
 vertical-align: bottom;
 `,[h("&:hover",{color:"var(--n-tab-text-color-hover)"}),i("active",`
 color: var(--n-tab-text-color-active);
 font-weight: var(--n-tab-font-weight-active);
 `),i("disabled",{color:"var(--n-tab-text-color-disabled)"})])]),r("tabs-nav",[i("line-type",[i("top",[R("prefix, suffix",`
 border-bottom: 1px solid var(--n-tab-border-color);
 `),r("tabs-nav-scroll-content",`
 border-bottom: 1px solid var(--n-tab-border-color);
 `),r("tabs-bar",`
 bottom: -1px;
 `)]),i("left",[R("prefix, suffix",`
 border-right: 1px solid var(--n-tab-border-color);
 `),r("tabs-nav-scroll-content",`
 border-right: 1px solid var(--n-tab-border-color);
 `),r("tabs-bar",`
 right: -1px;
 `)]),i("right",[R("prefix, suffix",`
 border-left: 1px solid var(--n-tab-border-color);
 `),r("tabs-nav-scroll-content",`
 border-left: 1px solid var(--n-tab-border-color);
 `),r("tabs-bar",`
 left: -1px;
 `)]),i("bottom",[R("prefix, suffix",`
 border-top: 1px solid var(--n-tab-border-color);
 `),r("tabs-nav-scroll-content",`
 border-top: 1px solid var(--n-tab-border-color);
 `),r("tabs-bar",`
 top: -1px;
 `)]),R("prefix, suffix",`
 transition: border-color .3s var(--n-bezier);
 `),r("tabs-nav-scroll-content",`
 transition: border-color .3s var(--n-bezier);
 `),r("tabs-bar",`
 border-radius: 0;
 `)]),i("card-type",[R("prefix, suffix",`
 transition: border-color .3s var(--n-bezier);
 border-bottom: 1px solid var(--n-tab-border-color);
 `),r("tabs-pad",`
 flex-grow: 1;
 transition: border-color .3s var(--n-bezier);
 border-bottom: 1px solid var(--n-tab-border-color);
 `),r("tabs-tab-pad",`
 transition: border-color .3s var(--n-bezier);
 `),r("tabs-tab",`
 font-weight: var(--n-tab-font-weight);
 border: 1px solid var(--n-tab-border-color);
 background-color: var(--n-tab-color);
 box-sizing: border-box;
 position: relative;
 vertical-align: bottom;
 display: flex;
 justify-content: space-between;
 font-size: var(--n-tab-font-size);
 color: var(--n-tab-text-color);
 `,[i("addable",`
 padding-left: 8px;
 padding-right: 8px;
 font-size: 16px;
 `,[R("height-placeholder",`
 width: 0;
 font-size: var(--n-tab-font-size);
 `),Gt("disabled",[h("&:hover",`
 color: var(--n-tab-text-color-hover);
 `)])]),i("closable","padding-right: 8px;"),i("active",`
 background-color: #0000;
 font-weight: var(--n-tab-font-weight-active);
 color: var(--n-tab-text-color-active);
 `),i("disabled","color: var(--n-tab-text-color-disabled);")]),r("tabs-scroll-padding","border-bottom: 1px solid var(--n-tab-border-color);")]),i("left, right",[r("tabs-wrapper",`
 flex-direction: column;
 `,[r("tabs-tab-wrapper",`
 flex-direction: column;
 `,[r("tabs-tab-pad",`
 height: var(--n-tab-gap-vertical);
 width: 100%;
 `)])])]),i("top",[i("card-type",[r("tabs-tab",`
 border-top-left-radius: var(--n-tab-border-radius);
 border-top-right-radius: var(--n-tab-border-radius);
 `,[i("active",`
 border-bottom: 1px solid #0000;
 `)]),r("tabs-tab-pad",`
 border-bottom: 1px solid var(--n-tab-border-color);
 `)])]),i("left",[i("card-type",[r("tabs-tab",`
 border-top-left-radius: var(--n-tab-border-radius);
 border-bottom-left-radius: var(--n-tab-border-radius);
 `,[i("active",`
 border-right: 1px solid #0000;
 `)]),r("tabs-tab-pad",`
 border-right: 1px solid var(--n-tab-border-color);
 `)])]),i("right",[i("card-type",[r("tabs-tab",`
 border-top-right-radius: var(--n-tab-border-radius);
 border-bottom-right-radius: var(--n-tab-border-radius);
 `,[i("active",`
 border-left: 1px solid #0000;
 `)]),r("tabs-tab-pad",`
 border-left: 1px solid var(--n-tab-border-color);
 `)])]),i("bottom",[i("card-type",[r("tabs-tab",`
 border-bottom-left-radius: var(--n-tab-border-radius);
 border-bottom-right-radius: var(--n-tab-border-radius);
 `,[i("active",`
 border-top: 1px solid #0000;
 `)]),r("tabs-tab-pad",`
 border-top: 1px solid var(--n-tab-border-color);
 `)])])])]),Qt=Object.assign(Object.assign({},We.props),{value:[String,Number],defaultValue:[String,Number],trigger:{type:String,default:"click"},type:{type:String,default:"bar"},closable:Boolean,justifyContent:String,size:{type:String,default:"medium"},placement:{type:String,default:"top"},tabStyle:[String,Object],barWidth:Number,paneClass:String,paneStyle:[String,Object],paneWrapperClass:String,paneWrapperStyle:[String,Object],addable:[Boolean,Object],tabsPadding:{type:Number,default:0},animated:Boolean,onBeforeLeave:Function,onAdd:Function,"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array],onClose:[Function,Array],labelSize:String,activeName:[String,Number],onActiveNameChange:[Function,Array]}),Zt=N({name:"Tabs",props:Qt,setup(e,{slots:n}){var b,p,d,m;const{mergedClsPrefixRef:l,inlineThemeDisabled:v}=St(e),P=We("Tabs","-tabs",Jt,Kt,e,l),C=z(null),x=z(null),S=z(null),g=z(null),u=z(null),y=z(!0),T=z(!0),L=Pe(e,["labelSize","size"]),H=Pe(e,["activeName","value"]),$=z((p=(b=H.value)!==null&&b!==void 0?b:e.defaultValue)!==null&&p!==void 0?p:n.default?(m=(d=ce(n.default())[0])===null||d===void 0?void 0:d.props)===null||m===void 0?void 0:m.name:null),k=Et(H,$),V={id:0},f=te(()=>{if(!(!e.justifyContent||e.type==="card"))return{display:"flex",justifyContent:e.justifyContent}});ne(k,()=>{V.id=0,U(),he()});function w(){var t;const{value:a}=k;return a===null?null:(t=C.value)===null||t===void 0?void 0:t.querySelector(`[data-name="${a}"]`)}function D(t){if(e.type==="card")return;const{value:a}=x;if(a&&t){const o=`${l.value}-tabs-bar--disabled`,{barWidth:s,placement:_}=e;if(t.dataset.disabled==="true"?a.classList.add(o):a.classList.remove(o),["top","bottom"].includes(_)){if(ge(["top","maxHeight","height"]),typeof s=="number"&&t.offsetWidth>=s){const B=Math.floor((t.offsetWidth-s)/2)+t.offsetLeft;a.style.left=`${B}px`,a.style.maxWidth=`${s}px`}else a.style.left=`${t.offsetLeft}px`,a.style.maxWidth=`${t.offsetWidth}px`;a.style.width="8192px",a.offsetWidth}else{if(ge(["left","maxWidth","width"]),typeof s=="number"&&t.offsetHeight>=s){const B=Math.floor((t.offsetHeight-s)/2)+t.offsetTop;a.style.top=`${B}px`,a.style.maxHeight=`${s}px`}else a.style.top=`${t.offsetTop}px`,a.style.maxHeight=`${t.offsetHeight}px`;a.style.height="8192px",a.offsetHeight}}}function ge(t){const{value:a}=x;if(a)for(const o of t)a.style[o]=""}function U(){if(e.type==="card")return;const t=w();t&&D(t)}function he(t){var a;const o=(a=u.value)===null||a===void 0?void 0:a.$el;if(!o)return;const s=w();if(!s)return;const{scrollLeft:_,offsetWidth:B}=o,{offsetLeft:O,offsetWidth:K}=s;_>O?o.scrollTo({top:0,left:O,behavior:"smooth"}):O+K>_+B&&o.scrollTo({top:0,left:O+K-B,behavior:"smooth"})}const X=z(null);let re=0,I=null;function Ae(t){const a=X.value;if(a){re=t.getBoundingClientRect().height;const o=`${re}px`,s=()=>{a.style.height=o,a.style.maxHeight=o};I?(s(),I(),I=null):I=s}}function Ve(t){const a=X.value;if(a){const o=t.getBoundingClientRect().height,s=()=>{document.body.offsetHeight,a.style.maxHeight=`${o}px`,a.style.height=`${Math.max(re,o)}px`};I?(I(),I=null,s()):I=s}}function Ie(){const t=X.value;if(t){t.style.maxHeight="",t.style.height="";const{paneWrapperStyle:a}=e;if(typeof a=="string")t.style.cssText=a;else if(a){const{maxHeight:o,height:s}=a;o!==void 0&&(t.style.maxHeight=o),s!==void 0&&(t.style.height=s)}}}const me={value:[]},xe=z("next");function je(t){const a=k.value;let o="next";for(const s of me.value){if(s===a)break;if(s===t){o="prev";break}}xe.value=o,Me(t)}function Me(t){const{onActiveNameChange:a,onUpdateValue:o,"onUpdate:value":s}=e;a&&Z(a,t),o&&Z(o,t),s&&Z(s,t),$.value=t}function Ge(t){const{onClose:a}=e;a&&Z(a,t)}function ye(){const{value:t}=x;if(!t)return;const a="transition-disabled";t.classList.add(a),U(),t.classList.remove(a)}let Ce=0;function Ee(t){var a;if(t.contentRect.width===0&&t.contentRect.height===0||Ce===t.contentRect.width)return;Ce=t.contentRect.width;const{type:o}=e;(o==="line"||o==="bar")&&ye(),o!=="segment"&&oe((a=u.value)===null||a===void 0?void 0:a.$el)}const He=be(Ee,64);ne([()=>e.justifyContent,()=>e.size],()=>{ie(()=>{const{type:t}=e;(t==="line"||t==="bar")&&ye()})});const Y=z(!1);function Fe(t){var a;const{target:o,contentRect:{width:s}}=t,_=o.parentElement.offsetWidth;if(!Y.value)_<s&&(Y.value=!0);else{const{value:B}=g;if(!B)return;_-s>B.$el.offsetWidth&&(Y.value=!1)}oe((a=u.value)===null||a===void 0?void 0:a.$el)}const Oe=be(Fe,64);function De(){const{onAdd:t}=e;t&&t(),ie(()=>{const a=w(),{value:o}=u;!a||!o||o.scrollTo({left:a.offsetLeft,top:0,behavior:"smooth"})})}function oe(t){if(!t)return;const{placement:a}=e;if(a==="top"||a==="bottom"){const{scrollLeft:o,scrollWidth:s,offsetWidth:_}=t;y.value=o<=0,T.value=o+_>=s}else{const{scrollTop:o,scrollHeight:s,offsetHeight:_}=t;y.value=o<=0,T.value=o+_>=s}}const Ne=be(t=>{oe(t.target)},64);_t(ve,{triggerRef:M(e,"trigger"),tabStyleRef:M(e,"tabStyle"),paneClassRef:M(e,"paneClass"),paneStyleRef:M(e,"paneStyle"),mergedClsPrefixRef:l,typeRef:M(e,"type"),closableRef:M(e,"closable"),valueRef:k,tabChangeIdRef:V,onBeforeLeaveRef:M(e,"onBeforeLeave"),activateTab:je,handleClose:Ge,handleAdd:De}),Ht(()=>{U(),he()}),zt(()=>{const{value:t}=S;if(!t)return;const{value:a}=l,o=`${a}-tabs-nav-scroll-wrapper--shadow-start`,s=`${a}-tabs-nav-scroll-wrapper--shadow-end`;y.value?t.classList.remove(o):t.classList.add(o),T.value?t.classList.remove(s):t.classList.add(s)});const Se=z(null);ne(k,()=>{if(e.type==="segment"){const t=Se.value;t&&ie(()=>{t.classList.add("transition-disabled"),t.offsetWidth,t.classList.remove("transition-disabled")})}});const Ue={syncBarPosition:()=>{U()}},we=te(()=>{const{value:t}=L,{type:a}=e,o={card:"Card",bar:"Bar",line:"Line",segment:"Segment"}[a],s=`${t}${o}`,{self:{barColor:_,closeIconColor:B,closeIconColorHover:O,closeIconColorPressed:K,tabColor:Xe,tabBorderColor:Ye,paneTextColor:Ke,tabFontWeight:qe,tabBorderRadius:Je,tabFontWeightActive:Qe,colorSegment:Ze,fontWeightStrong:et,tabColorSegment:tt,closeSize:at,closeIconSize:rt,closeColorHover:ot,closeColorPressed:nt,closeBorderRadius:it,[W("panePadding",t)]:q,[W("tabPadding",s)]:st,[W("tabPaddingVertical",s)]:lt,[W("tabGap",s)]:dt,[W("tabGap",`${s}Vertical`)]:ct,[W("tabTextColor",a)]:bt,[W("tabTextColorActive",a)]:pt,[W("tabTextColorHover",a)]:ft,[W("tabTextColorDisabled",a)]:ut,[W("tabFontSize",t)]:vt},common:{cubicBezierEaseInOut:gt}}=P.value;return{"--n-bezier":gt,"--n-color-segment":Ze,"--n-bar-color":_,"--n-tab-font-size":vt,"--n-tab-text-color":bt,"--n-tab-text-color-active":pt,"--n-tab-text-color-disabled":ut,"--n-tab-text-color-hover":ft,"--n-pane-text-color":Ke,"--n-tab-border-color":Ye,"--n-tab-border-radius":Je,"--n-close-size":at,"--n-close-icon-size":rt,"--n-close-color-hover":ot,"--n-close-color-pressed":nt,"--n-close-border-radius":it,"--n-close-icon-color":B,"--n-close-icon-color-hover":O,"--n-close-icon-color-pressed":K,"--n-tab-color":Xe,"--n-tab-font-weight":qe,"--n-tab-font-weight-active":Qe,"--n-tab-padding":st,"--n-tab-padding-vertical":lt,"--n-tab-gap":dt,"--n-tab-gap-vertical":ct,"--n-pane-padding-left":ee(q,"left"),"--n-pane-padding-right":ee(q,"right"),"--n-pane-padding-top":ee(q,"top"),"--n-pane-padding-bottom":ee(q,"bottom"),"--n-font-weight-strong":et,"--n-tab-color-segment":tt}}),F=v?Ft("tabs",te(()=>`${L.value[0]}${e.type[0]}`),we,e):void 0;return Object.assign({mergedClsPrefix:l,mergedValue:k,renderedNames:new Set,tabsRailElRef:Se,tabsPaneWrapperRef:X,tabsElRef:C,barElRef:x,addTabInstRef:g,xScrollInstRef:u,scrollWrapperElRef:S,addTabFixed:Y,tabWrapperStyle:f,handleNavResize:He,mergedSize:L,handleScroll:Ne,handleTabsResize:Oe,cssVars:v?void 0:we,themeClass:F==null?void 0:F.themeClass,animationDirection:xe,renderNameListRef:me,onAnimationBeforeLeave:Ae,onAnimationEnter:Ve,onAnimationAfterEnter:Ie,onRender:F==null?void 0:F.onRender},Ue)},render(){const{mergedClsPrefix:e,type:n,placement:b,addTabFixed:p,addable:d,mergedSize:m,renderNameListRef:l,onRender:v,paneWrapperClass:P,paneWrapperStyle:C,$slots:{default:x,prefix:S,suffix:g}}=this;v==null||v();const u=x?ce(x()).filter(f=>f.type.__TAB_PANE__===!0):[],y=x?ce(x()).filter(f=>f.type.__TAB__===!0):[],T=!y.length,L=n==="card",H=n==="segment",$=!L&&!H&&this.justifyContent;l.value=[];const k=()=>{const f=c("div",{style:this.tabWrapperStyle,class:[`${e}-tabs-wrapper`]},$?null:c("div",{class:`${e}-tabs-scroll-padding`,style:{width:`${this.tabsPadding}px`}}),T?u.map((w,D)=>(l.value.push(w.props.name),fe(c(ue,Object.assign({},w.props,{internalCreatedByPane:!0,internalLeftPadded:D!==0&&(!$||$==="center"||$==="start"||$==="end")}),w.children?{default:w.children.tab}:void 0)))):y.map((w,D)=>(l.value.push(w.props.name),fe(D!==0&&!$?$e(w):w))),!p&&d&&L?Re(d,(T?u.length:y.length)!==0):null,$?null:c("div",{class:`${e}-tabs-scroll-padding`,style:{width:`${this.tabsPadding}px`}}));return c("div",{ref:"tabsElRef",class:`${e}-tabs-nav-scroll-content`},L&&d?c(ze,{onResize:this.handleTabsResize},{default:()=>f}):f,L?c("div",{class:`${e}-tabs-pad`}):null,L?null:c("div",{ref:"barElRef",class:`${e}-tabs-bar`}))},V=H?"top":b;return c("div",{class:[`${e}-tabs`,this.themeClass,`${e}-tabs--${n}-type`,`${e}-tabs--${m}-size`,$&&`${e}-tabs--flex`,`${e}-tabs--${V}`],style:this.cssVars},c("div",{class:[`${e}-tabs-nav--${n}-type`,`${e}-tabs-nav--${V}`,`${e}-tabs-nav`]},_e(S,f=>f&&c("div",{class:`${e}-tabs-nav__prefix`},f)),H?c("div",{class:`${e}-tabs-rail`,ref:"tabsRailElRef"},T?u.map((f,w)=>(l.value.push(f.props.name),c(ue,Object.assign({},f.props,{internalCreatedByPane:!0,internalLeftPadded:w!==0}),f.children?{default:f.children.tab}:void 0))):y.map((f,w)=>(l.value.push(f.props.name),w===0?f:$e(f)))):c(ze,{onResize:this.handleNavResize},{default:()=>c("div",{class:`${e}-tabs-nav-scroll-wrapper`,ref:"scrollWrapperElRef"},["top","bottom"].includes(V)?c(Nt,{ref:"xScrollInstRef",onScroll:this.handleScroll},{default:k}):c("div",{class:`${e}-tabs-nav-y-scroll`,onScroll:this.handleScroll},k()))}),p&&d&&L?Re(d,!0):null,_e(g,f=>f&&c("div",{class:`${e}-tabs-nav__suffix`},f))),T&&(this.animated&&(V==="top"||V==="bottom")?c("div",{ref:"tabsPaneWrapperRef",style:C,class:[`${e}-tabs-pane-wrapper`,P]},Le(u,this.mergedValue,this.renderedNames,this.onAnimationBeforeLeave,this.onAnimationEnter,this.onAnimationAfterEnter,this.animationDirection)):Le(u,this.mergedValue,this.renderedNames)))}});function Le(e,n,b,p,d,m,l){const v=[];return e.forEach(P=>{const{name:C,displayDirective:x,"display-directive":S}=P.props,g=y=>x===y||S===y,u=n===C;if(P.key!==void 0&&(P.key=C),u||g("show")||g("show:lazy")&&b.has(C)){b.has(C)||b.add(C);const y=!g("if");v.push(y?Tt(P,[[Lt,u]]):P)}}),l?c(Rt,{name:`${l}-transition`,onBeforeLeave:p,onEnter:d,onAfterEnter:m},{default:()=>v}):v}function Re(e,n){return c(ue,{ref:"addTabInstRef",key:"__addable",name:"__addable",internalCreatedByPane:!0,internalAddable:!0,internalLeftPadded:n,disabled:typeof e=="object"&&e.disabled})}function $e(e){const n=$t(e);return n.props?n.props.internalLeftPadded=!0:n.props={internalLeftPadded:!0},n}function fe(e){return Array.isArray(e.dynamicProps)?e.dynamicProps.includes("internalLeftPadded")||e.dynamicProps.push("internalLeftPadded"):e.dynamicProps=["internalLeftPadded"],e}const ea={class:"picker"},ta={class:"icons-wrapper"},aa=["onClick"],ra={class:"item-name"},oa={class:"icons-wrapper"},na=["onClick"],ia={class:"item-name"},sa={class:"icons-wrapper"},la=["onClick"],da={class:"item-name"},ca=N({__name:"IconPicker",emits:["pickIcon"],setup(e,{emit:n}){const b=n,p=d=>{b("pickIcon",d)};return(d,m)=>(G(),E("div",ea,[J(j(Zt),{type:"line","theme-overrides":j(yt)},{default:Q(()=>[J(j(pe),{name:"sli",tab:"Simple-Line-Icons"},{default:Q(()=>[A("div",ta,[(G(!0),E(ae,null,se(j(ht),l=>(G(),E("div",{class:"normal icon-item",key:l,onClick:v=>p(l)},[A("i",{class:le(l)},null,2),A("span",ra,de(l),1)],8,aa))),128))])]),_:1}),J(j(pe),{name:"fai",tab:"Font-Awesome-Icons"},{default:Q(()=>[A("div",oa,[(G(!0),E(ae,null,se(j(xt),l=>(G(),E("div",{class:"normal icon-item",key:l,onClick:v=>p(l)},[A("i",{class:le(l)},null,2),A("span",ia,de(l),1)],8,na))),128))])]),_:1}),J(j(pe),{name:"gi",tab:"Glyph-Icons"},{default:Q(()=>[A("div",sa,[(G(!0),E(ae,null,se(j(mt),l=>(G(),E("div",{class:"large icon-item",key:l,onClick:v=>p(l)},[A("i",{class:le(l)},null,2),A("span",da,de(l),1)],8,la))),128))])]),_:1})]),_:1},8,["theme-overrides"])]))}});const ba=Bt(ca,[["__scopeId","data-v-374bc22a"]]),ja=Object.freeze(Object.defineProperty({__proto__:null,default:ba},Symbol.toStringTag,{value:"Module"}));export{ba as I,pe as N,Zt as a,ja as b};
