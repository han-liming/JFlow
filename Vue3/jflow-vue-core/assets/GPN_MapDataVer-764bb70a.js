var D=Object.defineProperty;var n=(E,F,u)=>F in E?D(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var s=(E,F,u)=>(n(E,typeof F!="symbol"?F+"":F,u),u);var r=(E,F,u)=>new Promise((a,e)=>{var o=t=>{try{C(u.next(t))}catch(B){e(B)}},A=t=>{try{C(u.throw(t))}catch(B){e(B)}},C=t=>t.done?a(t.value):Promise.resolve(t.value).then(o,A);C((u=u.apply(E,F)).next())});import{PageBaseGroupNew as i,GPNReturnObj as p,GPNReturnType as m}from"./PageBaseGroupNew-ee20c033.js";import c from"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./index-f4658ae7.js";import"./ParamsUtils-3cbc5822.js";import"./ParamUtils-cdc24dd6.js";class y extends i{constructor(){super("GPN_MapDataVer");s(this,"Docs0",`
  #### \u5E2E\u52A9
  - \u8868\u5355\u7248\u672C.
  - \u8BB0\u5F55\u8868\u5355\u76EE\u524D\u7684\u7248\u672C\u53F7\u3002\u5305\u62EC\u521B\u5EFA\u4EBA\uFF0C\u521B\u65B0\u65F6\u95F4\u7B49\u4FE1\u606F\u3002
  - \u5F53\u8868\u5355\u589E\u52A0\u4E86\u5B57\u6BB5\u540E\uFF0C\u53EF\u4EE5\u521B\u5EFA\u65B0\u7684\u7248\u672C\uFF0C\u800C\u518D\u8FD0\u884C\u6B64\u8868\u5355\u65F6\uFF0C\u6240\u8FD0\u884C\u7684\u7248\u672C\u53F7\u5C31\u4E3A\u65B0\u7684\u7248\u672C\u53F7\u3002
  - \u5728\u4E00\u4E2A\u65F6\u95F4\u5185\u53EA\u6709\u4E00\u4E2A\u8FD0\u884C\u7684\u7248\u672C\uFF0C\u6240\u8FD0\u884C\u7684\u7248\u672C\u4E0D\u4E00\u5B9A\u662F\u6700\u65B0\u7684\u7248\u672C\u3002
  
  #### \u5E94\u7528\u573A\u666F
  - \u6BD4\u5982\u51FA\u5E93\u6D41\u7A0B\uFF0C\u5F53\u8FD0\u884C\u4E00\u6BB5\u65F6\u95F4\u540E\uFF0C\u9700\u8981\u589E\u52A0\u51FA\u5E93\u5546\u54C1\u6279\u53F7\u3002\u5F53\u5728\u8868\u5355\u4E2D\u589E\u52A0\u8FD9\u4E2A\u5B57\u6BB5\u540E\uFF0C\u5C31\u53EF\u4EE5\u5728\u8868\u5355\u5C5E\u6027\u4E2D\u589E\u52A0\u4E00\u4E2A\u65B0\u7684\u7248\u672C\u53F7\u3002
  - \u90A3\u4E48\u518D\u8FDB\u884C\u51FA\u5E93\u64CD\u4F5C\u65F6\uFF0C\u5C31\u8FD0\u884C\u65B0\u7248\u672C\u7684\u8868\u5355\u3002

  #### \u6570\u636E\u7ED3\u6784

  - \u65B0\u589E\u6570\u636E\u7248\u672C\u540E\uFF0C\u6570\u636E\u4FE1\u606F\u5B58\u5165\u5230AtPara\u5B57\u6BB5\u91CC.

`);this.PageTitle="\u65B0\u5EFA\u8868\u5355\u7248\u672C",this.ForEntityClassID="TS.FrmUI.MapDataVer"}Init(){this.AddGroup("A","\u65B0\u5EFA\u8868\u5355\u7248\u672C"),this.AddBlank("0","\u65B0\u5EFA\u8868\u5355\u7248\u672C",this.Docs0)}GenerSorts(){return r(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,a,e,o,A){return r(this,null,function*(){if(window.confirm("\u60A8\u786E\u5B9A\u8981\u521B\u5EFA\u65B0\u7248\u672C\u5417\uFF1F")==!1)return;const C=this.RequestVal("RefPKVal"),B=yield new c("BP.Sys.MapData",C).DoMethodReturnString("CreateMapDataVer");return new p(m.Message,B)})}}export{y as GPN_MapDataVer};
