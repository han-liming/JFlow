var F=Object.defineProperty;var r=(t,E,u)=>E in t?F(t,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[E]=u;var a=(t,E,u)=>(r(t,typeof E!="symbol"?E+"":E,u),u);import{UAC as B}from"./UAC-8e255d47.js";import{Map as o}from"./Map-73575e6b.js";import{EntityMyPK as A}from"./EntityMyPK-e742fec8.js";import{a as e}from"./MapExt-db8cd7f3.js";import{SFDBSrc as D}from"./SFDBSrc-e641ea16.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";class X extends A{constructor(u){super("TS.MapExt.DtlImpEn1");a(this,"NoteTag",` 
 
  #### \u5E2E\u52A9

   - \u8BE5\u9009\u9879\u53EF\u4EE5\u4E3A\u7A7A,\u5728\u53F3\u4E0A\u89D2\u7684\u5217\u8868\u91CC\u67E5\u8BE2\u6216\u70B9\u6811\u6811\u5E72\u7684\u6570\u636E\u6E90\u51FA\u73B0\u7684\u5217\u8868,\u9700\u8981\u7528\u4E2D\u6587\u663E\u793A\u5217\u5934..
   - \u4E0D\u4E3A\u7A7A\u65F6\uFF0C\u8BBE\u7F6E\u51E0\u4E2A\u5B57\u6BB5\u5219\u5217\u8868\u91CC\u9762\u663E\u793A\u51E0\u4E2A\u5B57\u6BB5
   - \u683C\u5F0F\u4E3A:
   - \u4F8B\u5982: No=\u7F16\u53F7,Name=\u540D\u79F0,Addr=\u5730\u5740,Tel=\u7535\u8BDD,Email=\u90AE\u4EF6
   
   `);a(this,"NoteTag1",` 
 
  #### \u5E2E\u52A9
   - \u8BE5\u8BBE\u7F6E\u5BF9table\u67E5\u8BE2\u6709\u6548,(\u53EF\u4EE5\u4E3A\u7A7A)\uFF1A\u65E5\u671F\u7684\u9ED8\u8BA4\u503C\u662FJS\u51FD\u6570\u8868\u8FBE\u5F0F.
   - SQL\u683C\u5F0F\u4E3A:
   - $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListSQL=Select No,Name FROM Demo_BanJi 
   - $Para=XB#Label=\u6027\u522B#EnumKey=XB
   - $Para=DTFrom#Label=\u6CE8\u518C\u65E5\u671F\u4ECE#DefVal=(new Date( (new Date().setDate(-30 + new Date().getDate()))))
   - $Para=DTTo#Label=\u5230#DefVal=(new Date())
   - URL\u683C\u5F0F\u4E3A:
   - $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListURL=/DataUser/Handler.ashx?xxx=sss 
   - \u6267\u884CCCFromRef.js\u8FD4\u56DEJSON\u683C\u5F0F\u4E3A:
   - $Para=Dept#Label=\u6240\u5728\u73ED\u7EA7#ListFuncName=MyFunc 
    
    `);a(this,"NoteTag2",` 
 
  #### \u5E2E\u52A9
 
 
   -\u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2\u7684SQL\u8BED\u53E5\uFF0C\u5FC5\u987B\u8FD4\u56DENo,Name\u4E24\u4E2A\u5217\u3002
   - \u8BE5\u53C2\u6570\u652F\u6301ccbpm\u8868\u8FBE\u5F0F,\u6BD4\u5982:SELECT No, Name FROM WF_Emp WHERE FK_Dept='@WebUser.DeptNo'
   - \u5FC5\u987B\u6709\uFF1A@PageCount @PageSize @Key \u4E09\u4E2A\u53C2\u6570,\u5206\u522B\u6807\u8BC6:@PageCount =\u7B2C\u51E0\u9875, @PageSize=\u6BCF\u9875\u5927\u5C0F. @Key=\u5173\u952E\u5B57
   - \u6BD4\u5982For SQLServer: SELECT TOP @PageSize * FROM ( SELECT row_number() over(order by
      t.No) as rownumber,No,Name,Tel,Email FROM Demo_Student WHERE Name LIKE '%@Key%'
      ) A WHERE rownumber > @PageCount 
   - \u603B\u6761\u6570: SELECT COUNT(no) FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No LIKE '%@Key%')
      AND FK_BanJi=@FK_BanJi ANND XB=@XB 
   - \u6BD4\u5982For Oracle: SELECT No,Name,Email,Tel FROM Demo_Student WHERE (Name LIKE '%@Key%'
       OR No LIKE '%@Key%') AND FK_BanJi=@FK_BanJi ANND XB=@XB 
   - \u6BD4\u5982For MySQL: SELECT No,Name,Email,Tel FROM Demo_Student WHERE (Name LIKE '%@Key%'
       OR No LIKE '%@Key%') AND FK_BanJi=@FK_BanJi ANND XB=@XB 
   - \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F,\u6BD4\u5982:@WebUser.DeptNo , @FieldName @WebUser.OrgNo
    
    `);a(this,"NoteTag3",` 
 
  #### \u5E2E\u52A9
 
 
   - \u6BD4\u5982For SQLServer: SELECT count(No) FROM Demo_Student WHERE (Name LIKE '%@Key%' OR
     No LIKE '%@Key%') AND FK_BanJi=@FK_BanJi ANND XB=@XB 
   - \u6BD4\u5982For Oracle: SELECT count(No) FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No
     LIKE '%@Key%') AND FK_BanJi=@FK_BanJi ANND XB=@XB 
   - \u6BD4\u5982For MySQL: SELECT count(No) FROM Demo_Student WHERE (Name LIKE '%@Key%' OR No
     LIKE '%@Key%') AND FK_BanJi=@FK_BanJi ANND XB=@XB 
    `);a(this,"NoteTag4",` 
 
  #### \u5E2E\u52A9
 
   - \u4E3B\u952E\u4E0D\u4E3A\u7A7A\uFF0CSQL\u67E5\u8BE2\u7684\u5217\u8868\u8BBE\u7F6E\u4E3B\u952E\u65F6\uFF0C\u4E0D\u4F1A\u91CD\u590D\u5BFC\u5165\u8BE5\u6570\u636E
   - \u4E3B\u952E\u4E3A\u7A7A\u65F6\uFF0C\u5219\u4E3A\u9009\u62E9\u7684\u5168\u90E8\u5BFC\u5165\uFF0C\u4E0D\u8FC7\u6EE4\u5DF2\u7ECF\u5BFC\u5165\u7684\u6570\u636E

  `);u&&(this.MyPK=u)}get HisUAC(){const u=new B;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new o("Sys_MapExt","\u4ECE\u8868\u5BFC\u5165");return u.AddGroupAttr("\u6570\u636E\u6765\u6E90"),u.AddMyPK(),u.AddDDLEntities(e.FK_DBSrc,"local","\u6570\u636E\u6E90",new D,!0,null,!1),u.AddTBString(e.Tag1,null,"\u67E5\u8BE2\u6761\u4EF6\u8BBE\u7F6E",!0,!1,0,50,200,!0,this.NoteTag1),u.AddTBString(e.Tag2,null,"\u6570\u636E\u6E90 ",!0,!1,0,50,200,!0,this.NoteTag2),u.AddTBString(e.Tag3,null,"\u603B\u6761\u6570",!0,!1,0,50,200,!0,this.NoteTag3),u.AddTBString(e.Tag,null,"\u6570\u636E\u5217\u540D\u4E0E\u4E2D\u6587\u610F\u601D\u5BF9\u7167",!0,!1,0,50,200,!0,this.NoteTag),u.AddTBString(e.Tag4,null,"\u8BBE\u5B9A\u5BFC\u5165\u6570\u636E\u5217\u8868\u7684\u4E3B\u952E",!0,!1,0,50,200,!0,this.NoteTag4),u.AddGroupAttr("\u57FA\u672C\u4FE1\u606F"),u.AddTBString("Title",null,"\u6807\u9898",!0,!1,0,50,200,!0),u.AddTBString("SearchTip",null,"\u641C\u7D22\u63D0\u793A",!0,!1,0,50,200,!0,this.NoteSearchTip),u.AddTBInt(e.H,500,"\u5F39\u7A97\u9AD8\u5EA6",!0,!1),u.AddTBInt(e.W,800,"\u5F39\u7A97\u5BBD\u5EA6",!0,!1),u.AddTBAtParas(4e3),u.ParaFields=",Title,SearchTip,",this._enMap=u,this._enMap}}export{X as DtlImpEn1};
