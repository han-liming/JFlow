var t=Object.defineProperty;var A=(E,u,F)=>u in E?t(E,u,{enumerable:!0,configurable:!0,writable:!0,value:F}):E[u]=F;var B=(E,u,F)=>(A(E,typeof u!="symbol"?u+"":u,F),F);import{PageBaseGroupEdit as i}from"./PageBaseGroupEdit-202e8e85.js";import{Flow as C}from"./Flow-6121039a.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./BSEntity-840a884b.js";class y extends i{constructor(){super("GPE_Limit");B(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u53D1\u8D77\u9650\u5236\u89C4\u5219\u5C31\u662F\u5982\u4F55\u9650\u5236\u53D1\u8D77\u7684\u6D41\u7A0B\u7684\u89C4\u5219 
  - \u4E0D\u4F7F\u7528\u89C4\u5219,\u4E0D\u8BBE\u7F6E\u53D1\u8D77\u9650\u5236\u6761\u4EF6,\u9ED8\u8BA4\u4E3A\u4E0D\u9650\u5236.

  #### \u5E94\u7528\u573A\u666F.
  - \u6307\u5B9A\u7684\u5B57\u6BB5\u4E0D\u80FD\u91CD\u590D\u573A\u666F: \u5BF9\u7EB3\u7A0E\u4EBA\u8FDB\u884C\u6267\u884C\u6CE8\u9500\u6D41\u7A0B, \u5982\u679C\u4E00\u4E2A\u7EB3\u7A0E\u4EBA\u7F16\u53F7\u5DF2\u7ECF\u542F\u52A8\u4E86\u6CE8\u9500,\u5C31\u4E0D\u80FD\u5728\u542F\u52A8\u6CE8\u9500\u4E86.
  - \u6309\u65F6\u95F4\u89C4\u5219\u8BA1\u7B97\u573A\u666F: \u5468\u4F8B\u4F1A\u6D41\u7A0B, \u6BCF\u4E2A\u5468\u7684\u54681\u53D1\u8D77\u4F8B\u4F1A\u6D41\u7A0B,\u4E0D\u80FD\u91CD\u590D\u53D1\u8D77\u4E24\u6B21.
  - \u5176\u5B83\u7684\u8BF7\u53C2\u8003\u8BF4\u660E.
  `);B(this,"Desc1",`
  #### \u5E2E\u52A9 
   - \u7528\u6765\u9650\u5236\u8BE5\u6D41\u7A0B\u53EF\u4EE5\u5728\u4EC0\u4E48\u65F6\u95F4\u6BB5\u5185\u53D1\u8D77\u3002
   - \u4F8B\u5982:\u6309\u7167\u6BCF\u4EBA\u6BCF\u5929\u4E00\u6B21\u8BBE\u7F6E\u65F6\u95F4\u8303\u56F4\uFF0C\u89C4\u5219\u53C2\u6570\uFF1A@08:30-09:00@18:00-18:30\uFF0C\u89E3\u91CA\uFF1A\u8BE5\u6D41\u7A0B\u53EA\u80FD\u572808:30-09:00\u4E0E18:00-18:30\u4E24\u4E2A\u65F6\u95F4\u6BB5\u53D1\u8D77\u4E14\u53EA\u80FD\u53D1\u8D77\u4E00\u6B21\u3002
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrFlow/Limit/Img/LimitSetting.png "\u5C4F\u5E55\u622A\u56FE.png")
  
    `);B(this,"Desc7",`
   #### \u5E2E\u52A9
   - \u4F8B\u5982\uFF1ASELECT COUNT(*) AS Num FROM TABLE1 WHERE NAME='@MyFieldName'. 
   - \u89E3\u91CA\uFF1A\u7F16\u5199\u4E00\u4E2Asql\u8BED\u53E5\u8FD4\u56DE\u4E00\u884C\u4E00\u5217\uFF0C\u5982\u679C\u4FE1\u606F\u662F0\uFF0C\u5C31\u662F\u53EF\u4EE5\u542F\u52A8\uFF0C\u975E0\u5C31\u4E0D\u53EF\u4EE5\u542F\u52A8\u3002
   - \u8BE5\u53C2\u6570\u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F\u3002\u6BD4\u5982:@WebUser.No,@WebUser.Name,@WebUser.DeptNo,@WebUser.OrgNo
    `);B(this,"Desc6",`
   #### \u5E2E\u52A9 
   
    
   - \u8BBE\u7F6E\u4E00\u4E2A\u5217\u5141\u8BB8\u91CD\u590D\uFF0C\u6BD4\u5982\uFF1ANSRBH
   - \u8BBE\u7F6E\u591A\u4E2A\u5217\u7684\u65F6\u5019\uFF0C\u9700\u8981\u7528\u9017\u53F7\u5206\u5F00\uFF0C\u6BD4\u5982\uFF1Afield1,field2
   - \u6D41\u7A0B\u5728\u53D1\u8D77\u7684\u65F6\u5019\u5982\u679C\u53D1\u73B0\uFF0C\u8BE5\u5217\u662F\u91CD\u590D\u7684\uFF0C\u5C31\u629B\u51FA\u5F02\u5E38\uFF0C\u963B\u6B62\u6D41\u7A0B\u53D1\u8D77\u3002
   - \u6BD4\u5982\uFF1A\u7EB3\u7A0E\u4EBA\u6CE8\u9500\u6D41\u7A0B\uFF0C\u4E00\u4E2A\u7EB3\u7A0E\u4EBA\u53EA\u80FD\u53D1\u8D77\u4E00\u6B21\u6CE8\u9500\uFF0C\u5C31\u8981\u914D\u7F6E\u7EB3\u7A0E\u4EBA\u5B57\u6BB5\uFF0C\u8BA9\u5176\u4E0D\u80FD\u91CD\u590D\u3002
   
   `);B(this,"Desc9",`
   #### \u5E2E\u52A9  
    
   - \u591A\u4E2A\u5B50\u6D41\u7A0B\u7528\u9017\u53F7\u5206\u5F00\u3002
   - \u6BD4\u5982:001,003
   
    `);this.PageTitle="\u53D1\u8D77\u9650\u5236\u89C4\u5219"}Init(){this.entity=new C,this.KeyOfEn="StartLimitRole",this.AddGroup("A","\u9650\u5236\u89C4\u5219"),this.Blank("0","\u4E0D\u9650\u5236",this.Desc0),this.SingleTB("1","\u6309\u65F6\u95F4\u89C4\u5219\u8BA1\u7B97","StartLimitPara",this.Desc1,"\u8BF7\u8F93\u5165\u53C2\u6570.."),this.SingleTB("6","\u6309\u7167\u53D1\u8D77\u5B57\u6BB5\u4E0D\u80FD\u91CD\u590D\u89C4\u5219","StartLimitPara",this.Desc6,"\u8BF7\u8F93\u5165\u53C2\u6570.."),this.SingleTBSQL("7","\u6309SQL","StartLimitPara",this.Desc7),this.SingleTB("9","\u4E3A\u5B50\u6D41\u7A0B\u65F6\u4EC5\u4EC5\u53EA\u80FD\u88AB\u8C03\u75281\u6B21","StartLimitPara",this.Desc9,"\u8BF7\u8F93\u5165\u53C2\u6570..")}AfterSave(F,D){if(F==D)throw new Error("Method not implemented.")}BtnClick(F,D,r){}}export{y as GPE_Limit};
