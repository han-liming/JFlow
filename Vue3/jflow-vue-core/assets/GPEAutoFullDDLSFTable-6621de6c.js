var l=Object.defineProperty;var A=(E,e,u)=>e in E?l(E,e,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[e]=u;var r=(E,e,u)=>(A(E,typeof e!="symbol"?e+"":e,u),u);var s=(E,e,u)=>new Promise((o,i)=>{var C=F=>{try{B(u.next(F))}catch(a){i(a)}},n=F=>{try{B(u.throw(F))}catch(a){i(a)}},B=F=>F.done?o(F.value):Promise.resolve(F.value).then(C,n);B((u=u.apply(E,e)).next())});import{UAC as p}from"./UAC-8e255d47.js";import{Map as m}from"./Map-73575e6b.js";import{EntityMyPK as D}from"./EntityMyPK-e742fec8.js";import{a as t}from"./MapExt-db8cd7f3.js";import{D as T}from"./DataType-33901a1c.js";import{SFTable as c}from"./SFTable-d63f9fb4.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class Y extends D{constructor(u){super("TS.MapExt.GPEAutoFullDDLSFTable");r(this,"HelpTag",` 
  #### resufull\u6570\u636E\u6E90\u8BF4\u660E
  - \u70B9\u51FB\u4FDD\u5B58\u6309\u94AE\u7CFB\u7EDF\u81EA\u52A8\u51FA\u73B0\u3010\u6570\u636E\u6E90\u8868\u8FBE\u5F0F\u3011.
  - \u6BD4\u5982\u6570\u636E\u6E90\u8868\u8FBE\u5F0F:/get_BU_PDT/{accesstoken}/{BU}
  - \u6807\u8BC6\u4ED6\u9700\u8981\u4E8C\u4E2A\u53C2\u6570,{accesstoken} \u4E0E  {BU} \uFF0C \u8FD9\u4E2A{BU}\u5C31\u662F\u6211\u4EEC\u7684\u8054\u52A8\u7684 @Key, \u5C31\u662F\u5F53\u524D\u4E0B\u62C9\u6846\u7684\u9009\u4E2D\u7684\u503C.
  - \u5728\u3010\u53C2\u6570\u683C\u5F0F\u3011\u91CC\u6211\u4EEC\u8F93\u5165:{BU}=@Key;{accesstoken}=@WebUser.Token
  - \u8BF4\u660E: @WebUser.Token \u662F\u7CFB\u7EDF\u53C2\u6570, @Key \u662Fccform\u7EA6\u5B9A\u7684\u53C2\u6570.
  #### SQL\u6570\u636E\u6E90\u8BF4\u660E
  - \u70B9\u51FB\u4FDD\u5B58\u6309\u94AE\u7CFB\u7EDF\u81EA\u52A8\u51FA\u73B0\u3010\u6570\u636E\u6E90\u8868\u8FBE\u5F0F\u3011.
  - \u6BD4\u5982\u6570\u636E\u6E90\u8868\u8FBE\u5F0F:SELECT No,Name FROM Port_Emp WHERE Name='@Key'
  - \u53C2\u6570\u683C\u5F0F:\u5C31\u53EF\u4EE5\u4E3A\u7A7A,\u5FC5\u987B\u8981\u8F93\u5165.
  - \u6BD4\u5982\u6570\u636E\u6E90\u8868\u8FBE\u5F0F:SELECT No,Name FROM Port_Emp WHERE Name='@BU'
  - \u53C2\u6570\u683C\u5F0F: @BU=@Key;
   `);r(this,"HelpTag1",` 
   #### \u8BF4\u660E
   - \u8BE5\u5B57\u6BB5\u53EA\u8BFB
   - \u5F53\u60A8\u9009\u62E9\u4E00\u4E2A\u5B57\u5178\u7684\u65F6\u5019\uFF0C\u70B9\u51FB\u4FDD\u5B58\u8BE5\u5B57\u5178\u8981\u83B7\u5F97\u6570\u636E\u7684\u8868\u8FBE\u5C31\u4F1A\u81EA\u52A8\u586B\u5145\u4E0A\u6765.
    `);r(this,"DescSearchtip",` 
  #### \u8BF4\u660E
  - \u663E\u793A\u5728\u641C\u7D22\u6587\u672C\u6846\u7684\u80CC\u666F\u6587\u5B57.
  - \u8F93\u5165\u57CE\u5E02\u540D\u79F0,\u6BD4\u5982:beijing,bj,\u8FDB\u884C\u641C\u7D22.
  - \u4EBA\u5458\u7684\u7F16\u53F7,\u540D\u79F0,\u62FC\u97F3,\u8FDB\u884C\u6A21\u7CCA\u641C\u7D22.
   `);r(this,"DescTag1",` 
   #### \u8BF4\u660E
   - zhoupeng \u8865\u5145
    `);r(this,"DescDoc",` 
  #### \u8BF4\u660E
  - SQL\u683C\u5F0F\u4E3A:
  - SELECT No,Name FROM Port_Emp WHERE PinYin LIKE '@Key%' OR No LIKE '%@Key%' OR Name LIKE '%@Key%' 
  - SELECT No,Name FROM CN_City WHERE PinYin LIKE '%@Key%' OR Name LIKE '%@Key%'
  - URL\u683C\u5F0F\u4E3A:
  - /DataUser/Handler.ashx?xxx=sss 
  - \u65B9\u6CD5\u7684\u683C\u5F0F\u4E3A:
  - MyFunName
   `);u&&(this.MyPK=u)}get HisUAC(){const u=new p;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new m("Sys_MapExt","\u586B\u5145\u8FC7\u6EE4");u.GroupBarShowModel=1,u.AddGroupAttr("\u57FA\u672C\u8BBE\u7F6E"),u.AddMyPK(),u.AddTBString(t.FK_MapData,null,"\u8868\u5355ID",!0,!0,0,50,200),u.AddTBString(t.ExtModel,null,"AutoFull",!1,!1,0,50,200),u.AddTBString(t.ExtType,null,"AutoFull",!1,!1,0,50,200),u.AddTBString(t.AttrOfOper,null,"\u5F53\u524D\u5B57\u6BB5",!0,!0,0,50,200);const o="SELECT No,Name FROM Sys_SFTable WHERE No!='Blank' AND IsPara=0 ";return u.AddDDLSQL(t.Doc,null,"\u5B57\u5178\u8868",o,!0),u.AddTBString(t.Tag1,null,"\u6570\u636E\u6E90\u8868\u8FBE\u5F0F",!0,!0,0,50,200,!0,this.HelpTag1),u.AddTBString(t.Tag2,null,"WebApi\u4E3B\u673A",!0,!0,0,50,200,!0),u.AddTBStringDoc(t.Tag3,null,"\u8BBE\u7F6E\u4FE1\u606F",!0,!0,!0),this._enMap=u,this._enMap}beforeUpdate(){return s(this,null,function*(){if(T.IsNullOrEmpty(this.Doc)==!0)return Promise.resolve(!0);const u=new c;return u.No=this.Doc,(yield u.RetrieveFromDBSources())==0||(this.Tag1=u.SelectStatement,this.Tag2=u.ConnString),Promise.resolve(!0)})}}export{Y as GPEAutoFullDDLSFTable};
