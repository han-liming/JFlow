var A=Object.defineProperty;var n=(F,o,e)=>o in F?A(F,o,{enumerable:!0,configurable:!0,writable:!0,value:e}):F[o]=e;var r=(F,o,e)=>(n(F,typeof o!="symbol"?o+"":o,e),e);var l=(F,o,e)=>new Promise((D,a)=>{var S=E=>{try{u(e.next(E))}catch(t){a(t)}},c=E=>{try{u(e.throw(E))}catch(t){a(t)}},u=E=>E.done?D(E.value):Promise.resolve(E.value).then(S,c);u((e=e.apply(F,o)).next())});import{SFTable as p}from"./SFTable-d63f9fb4.js";import{D as N}from"./DataType-33901a1c.js";import{PageBaseGroupNew as b,GPNReturnObj as s,GPNReturnType as B}from"./PageBaseGroupNew-ee20c033.js";import C from"./BSEntity-840a884b.js";import{GloComm as m}from"./GloComm-7cfbdfd9.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./SFDBSrc-e641ea16.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";class $ extends b{constructor(){super("GPN_SFTable");r(this,"WebApi_Url",`
  #### \u5E2E\u52A9
   - \u8BF7\u8F93\u5165\u8DEF\u5F84\u53C2\u6570.
   - \u4EC5\u4EC5\u8F93\u5165\u4E3B\u673A\u7AEF\u53E3\u53F7\u540E\u9762\u7684\u90E8\u5206.
   - \u6BD4\u5982: /xxxx.do
  `);r(this,"SrcHelp",`
  #### \u5E2E\u52A9
   - \u8BF7\u9009\u62E9\u6570\u636E\u6E90\uFF0C\u5982\u679C\u6CA1\u6709\uFF0C\u8BF7\u65B0\u5EFA\u6570\u636E\u6E90.
   - 
  `);r(this,"SFTable",`
  #### \u5E2E\u52A9
   - \u5185\u7F6E\u5B57\u5178\u8868,\u6BD4\u5982: \u7701\u4EFD\uFF0C\u7247\u533A\u3001\u57CE\u5E02\u3001\u7A0E\u79CD\uFF0C\u7A0E\u76EE
   - \u5185\u7F6E\u5B57\u5178\u8868\uFF0C\u662F\u81EA\u5DF1\u53EF\u4EE5\u7EF4\u62A4\u7684\u8868.
   - \u5B58\u50A8\u5728 Sys_SFTableDtl \u8868\u91CC. 
   - \u7528\u6237\u53EF\u4EE5\u901A\u8FC7ccfrom\u81EA\u5DF1\u5B9A\u4E49\uFF0C\u81EA\u5DF1\u7EF4\u62A4\u7684\u57FA\u7840\u6570\u636E.
   
  `);r(this,"Handler",`
  #### \u5E2E\u52A9
   - \u4F18\u70B9:\u683C\u5F0F\u7075\u6D3B,\u5C55\u73B0\u6548\u679C\u968F\u5FC3\u6240\u6B32.
   - \u9002\u7528\u4E8E:\u6548\u679C
   #### lisdxcx
   xxxxx
xxx      
  `);r(this,"SQL",`
  #### \u5E2E\u52A9
   - \u8BBE\u7F6E\u4E00\u4E2ASQL\u8BED\u53E5\u4ECE\u6570\u636E\u6E90\u4E2D\u67E5\u8BE2\u51FA\u6765.
   - \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F. @WebUser.No \u5F53\u524D\u7528\u6237\u7F16\u53F7\uFF0C @WebUser.Name \u767B\u5F55\u540D\u79F0\uFF0C @WebUser.DeptNo \u767B\u5F55\u4EBA\u6240\u5728\u90E8\u95E8.
   #### DEMO
   - \u672C\u90E8\u95E8\u7684\u4EBA\u5458.
   - SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'
   - \u6211\u7684\u4E0B\u7EA7\u90E8\u95E8
   - SELECT No,Name FROM Port_Dept WHERE PartentNo='@WebUser.DeptNo'
xxx      
  `);r(this,"SQL_Doc",`
  #### \u5E2E\u52A9
   - \u8BBE\u7F6E\u4E00\u4E2ASQL\u8BED\u53E5\u4ECE\u6570\u636E\u6E90\u4E2D\u67E5\u8BE2\u51FA\u6765.
   - \u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F. @WebUser.No \u5F53\u524D\u7528\u6237\u7F16\u53F7\uFF0C @WebUser.Name \u767B\u5F55\u540D\u79F0\uFF0C @WebUser.DeptNo \u767B\u5F55\u4EBA\u6240\u5728\u90E8\u95E8.
   #### DEMO
   - \u672C\u90E8\u95E8\u7684\u4EBA\u5458.
   - SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'
   - \u6211\u7684\u4E0B\u7EA7\u90E8\u95E8
   - SELECT No,Name FROM Port_Dept WHERE PartentNo='@WebUser.DeptNo'
xxx      
  `);r(this,"JavaScript",`
    #### \u5E2E\u52A9
     - \u6682\u65E0
     #### lisdxcx
     function Xxx()
     {
        
     }
xxx      
    `);r(this,"WebApi",`
  #### \u5E2E\u52A9
   - \u8C03\u7528\u670D\u52A1\u83B7\u5F97\u6570\u636E.
    
  `);r(this,"WebApi_Doc",`
  #### \u5E2E\u52A9
  - \u8C03\u7528\u670D\u52A1\u83B7\u5F97\u6570\u636E.
    
  `);r(this,"Docs1",`
  #### \u5E2E\u52A9 
  - \u6682\u65E0
  `);r(this,"Docs2",`
  #### \u5E2E\u52A9
  - \u6682\u65E0
    
  `);r(this,"Docs4",`
  #### \u5E2E\u52A9
  - \u586B\u5199\u683C\u5F0F: \u679A\u4E3E\u503C,\u679A\u4E3E\u6807\u7B7E; 
  - \u4F8B\u5982: ty,\u56E2\u5458;dy=\u515A\u5458;qz,\u7FA4\u4F17; 
  - \u7CFB\u7EDF\u89E3\u6790\u4E3A: ty\u662F\u56E2\u5458, dy\u662F\u515A\u5458, qz\u662F\u7FA4\u4F17.

  #### \u6570\u636E\u5B58\u50A8.
  - string\u7C7B\u578B\u7684\u679A\u4E3E\u4E5F\u79F0\u4E3A\u6807\u8BB0\u679A\u4E3E,\u5B57\u6BCD\u5B58\u50A8\u4E00\u4E2A\u5217,\u6807\u7B7E\u5B58\u50A8\u4E00\u4E2A\u5217.
  - \u5728\u8868\u5355\u91CC\u5B57\u6BB5\u662Fabc,\u90A3\u7CFB\u7EDF\u5C31\u4F1A\u81EA\u52A8\u521B\u5EFA\u4E00\u4E2A\u5F71\u5B50\u5B57\u6BB5 abcT.
  - abc\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u6807\u8BB0, abcT\u5B58\u50A8\u7684\u662F\u6807\u7B7E.
  `);this.ForEntityClassID="TS.FrmUI.SFTable",this.PageTitle="\u65B0\u5EFA\u5B57\u5178"}Init(){this.AddGroup("A","\u6570\u636E\u6E90\u7C7B\u578B"),this.TextBox2_NameNo("SFTableDict","\u5185\u7F6E\u5B57\u5178\u8868",this.SFTable,"SF_","\u5B57\u5178ID","\u5B57\u5178\u540D\u79F0",""),this.SelectItemsByList("SFTableDict.CodeStruct","\u6570\u636E\u7ED3\u6784",this.SFTable,!1,this.GetCodeStruct()),this.TextBox2_NameNo("SQL","SQL\u67E5\u8BE2\u5B57\u5178\u8868",this.SQL,"SQL_","\u5B57\u5178ID","\u5B57\u5178\u540D\u79F0",""),this.TextSQL("SQL.Doc","\u586B\u5199SQL",this.SQL_Doc,"\u67E5\u8BE2SQL","SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'","\u67E5\u8BE2\u8BED\u53E5"),this.SelectItemsByList("SQL.Doc.CodeStruct","\u6570\u636E\u7ED3\u6784",this.SFTable,!1,this.GetCodeStruct()),this.TextBox2_NameNo("WebApi","WebApi\u63A5\u53E3\u5B57\u5178\u8868",this.WebApi,"WebApi_","\u5B57\u5178ID","\u5B57\u5178\u540D\u79F0",""),this.TextSQL("WebApi.Doc","\u670D\u52A1\u94FE\u63A5",this.WebApi_Url,"\u8DEF\u5F84\u4E0E\u53C2\u6570","/xxx.do","\u8F93\u5165\u4E3B\u673A\u7684\u540E\u90E8\u5206"),this.SelectItemsByList("WebApi.Doc.CodeStruct","\u6570\u636E\u7ED3\u6784",this.SFTable,!1,this.GetCodeStruct()),this.TextBox2_NameNo("Handler","\u5FAE\u670D\u52A1Handler\u5B57\u5178\u8868",this.Handler,"Handler_","\u5B57\u5178ID","\u5B57\u5178\u540D\u79F0",""),this.TextSQL("Handler.Doc","\u586B\u5199\u5185\u5BB9",this.SQL_Doc,"\u67E5\u8BE2SQL","SELECT No,Name FROM Port_Emp WHERE FK_Dept='@WebUser.DeptNo'","\u67E5\u8BE2\u8BED\u53E5"),this.TextBox2_NameNo("JavaScript","JavaScript\u5B57\u5178\u8868",this.JavaScript,"JS_","\u5B57\u5178ID","\u5B57\u5178\u540D\u79F0",""),this.TextArea("JavaScript.Doc","\u586B\u5199\u65B9\u6CD5\u540D",this.SQL_Doc,"\u65B9\u6CD5\u540D"," MyDict() ","Javascript\u7684\u65B9\u6CD5\u540D"),this.AddGroup("B","\u76F8\u5173\u529F\u80FD"),this.AddFunction("AdminDBSrc_ToUrl","\u6570\u636E\u6E90\u7EF4\u62A4",this.GotoUrl)}GetCodeStruct(){return JSON.stringify([{No:"0",Name:"\u7F16\u53F7\u540D\u79F0"},{No:"1",Name:"\u6811\u7ED3\u6784"}])}GotoUrl(){const e="/@/WF/Comm/Search.vue?EnName=TS.Sys.SFDBSrc";return new s(B.GoToUrl,e)}GenerSorts(){return l(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(e,D,a,S,c){return l(this,null,function*(){const u=new p;if(u.Name=a,u.No=S,u.TableDesc=c,u.RDT=N.CurrentDateTime,e.includes(".")==!1&&(yield u.IsExits()))throw new Error("\u7F16\u53F7:"+u.No+"\u5DF2\u5B58\u5728.");if(e==="SFTableDict.CodeStruct"){u.Name=this.RequestVal("tb1","SFTableDict"),u.No=this.RequestVal("tb2","SFTableDict"),u.FK_SFDBSrc="local",u.DBSrcType="SysDict",u.DBType=0,u.FK_Val=u.No,u.CodeStruct=this.RequestVal("tb1","SFTableDict.CodeStruct"),u.CodeStruct==0?u.SetPara("EnName","TS.FrmUI.SFTableDictNoName"):u.SetPara("EnName","TS.FrmUI.SFTableDictTree"),yield u.Insert();const t=new C("BP.Sys.SFTable",u.No);yield t.Retrieve(),yield t.DoMethodReturnString("GenerDataOfJson");let i="";return i="/@/WF/Comm/En.vue?EnName="+u.GetParaString("EnName","")+"&PKVal="+u.No,new s(B.GoToUrl,i)}if(e=="SQL.Doc.CodeStruct"){u.Name=this.RequestVal("tb1","SQL"),u.No=this.RequestVal("tb2","SQL"),u.DBSrcType="SQL",u.CodeStruct=this.RequestVal("tb1","SQL.Doc.CodeStruct");let t="TS.FrmUI.SFTableSQLNoName";u.CodeStruct==1&&(t="TS.FrmUI.SFTableSQLTree"),u.SetPara("EnName",t),u.SelectStatement=this.RequestVal("tb2","SQL.Doc"),u.FK_SFDBSrc=this.RequestVal("tb1","SQL.Doc"),u.FK_Val=u.No,yield u.Insert();const i=m.UrlEn(t,u.No);return new s(B.GoToUrl,i)}if(e=="WebApi.Doc.CodeStruct"){u.Name=this.RequestVal("tb1","WebApi"),u.No=this.RequestVal("tb2","WebApi"),u.DBSrcType="WebApi",u.CodeStruct=this.RequestVal("tb1","WebApi.Doc.CodeStruct");let t="TS.FrmUI.SFTableWebApiNoName";u.CodeStruct==1&&(t="TS.FrmUI.SFTableWebApiTree"),u.SetPara("EnName",t),u.SelectStatement=this.RequestVal("tb2","WebApi.Doc"),u.FK_SFDBSrc=this.RequestVal("tb1","WebApi.Doc"),u.FK_Val=u.No,yield u.Insert();const i=m.UrlEn(t,u.No);return new s(B.GoToUrl,i)}e=="Handler.Doc"&&(u.Name=this.RequestVal("tb1","Handler"),u.No=this.RequestVal("tb2","Handler"),u.DBSrcType="Handler"),e=="JavaScript.Doc"&&(u.Name=this.RequestVal("tb1","JavaScript"),u.No=this.RequestVal("tb2","JavaScript"),u.DBSrcType="JavaScript");let E="";if(e==="Handler.Doc"&&(E="TS.FrmUI.SFTableHandler"),e==="JavaScript.Doc"&&(E="TS.FrmUI.SFTableJS"),u.SelectStatement=S,u.FK_SFDBSrc=a,u.FK_Val=u.No,E!==""){u.SetPara("EnName",E),u.SelectStatement=S,u.FK_SFDBSrc=a,u.FK_Val=u.No,yield u.Insert();const t="/@/WF/Comm/En.vue?EnName="+E+"&PKVal="+u.No;return new s(B.GoToUrl,t)}})}}export{$ as GPN_SFTable};
