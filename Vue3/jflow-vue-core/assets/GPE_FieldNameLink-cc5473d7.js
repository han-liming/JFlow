var a=Object.defineProperty;var h=(i,t,u)=>t in i?a(i,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):i[t]=u;var E=(i,t,u)=>(h(i,typeof t!="symbol"?t+"":t,u),u);var n=(i,t,u)=>new Promise((r,o)=>{var l=F=>{try{B(u.next(F))}catch(p){o(p)}},A=F=>{try{B(u.throw(F))}catch(p){o(p)}},B=F=>F.done?r(F.value):Promise.resolve(F.value).then(l,A);B((u=u.apply(i,t)).next())});import{b as s,a as e}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as C}from"./PageBaseGroupEdit-202e8e85.js";import{LinkAttr as m}from"./LinkAttr-28ef711f.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class X extends C{constructor(){super("GPE_FieldNameLink");E(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u5B9A\u4E49: \u5BF9\u8F93\u5165\u7684\u5B57\u6BB5\u8FDB\u884C\u8BE6\u5C3D\u7684\u63CF\u8FF0\uFF0C\u6587\u5B57\u7279\u522B\u591A\uFF0C\u5C31\u9700\u8981\u6B64\u529F\u80FD.
  - \u6BD4\u5982: \u9879\u76EE\u7533\u62A5\u6D41\u7A0B\u4E2D\uFF0C\u5BF9\u9879\u76EE\u7684\u9884\u671F\u6548\u76CA\u8FDB\u884C\u63CF\u8FF0.
  #### \u6548\u679C\u56FE
  - \u6682\u65E0
  `);E(this,"HelpInfo",`
  #### \u5E2E\u52A9 
  - \u8BF7\u8F93\u5165\u8BE5\u5B57\u6BB5\u7684\u8BE6\u7EC6\u63CF\u8FF0\u4FE1\u606F.
  - \u652F\u6301markdown \u8BED\u6CD5, \u652F\u6301html.
  #### \u56FE\u4F8B
  - \u6682\u65E0
  #### \u8FD0\u884C\u56FE
  - \u6682\u65E0
  `);E(this,"UrlRightOpen",`
  #### \u5E2E\u52A9 
  - \u8BF7\u8F93\u5165\u8BE5\u5B57\u6BB5\u7684\u8BE6\u7EC6\u63CF\u8FF0\u4FE1\u606F.
  - \u683C\u5F0F: http://11.112.11.2/xx.do?DoType=xx&JinE=@JinE&BianHao=@BillNo
  - \u89E3\u6790\u540E\u7684\u683C\u5F0F: http://11.112.11.2/xx.do?DoType=xx&JinE=123.99&BianHao=100-02
  - \u89E3\u6790\u8BF4\u660E, @BillNo,@JinE \u5C31\u662F\u5B57\u6BB5\u540D. \u89E3\u6790\u7684\u65F6\u5019\uFF0C\u4F1A\u628A\u5B57\u6BB5\u540D\u66FF\u6362\u6389.
  #### \u56FE\u4F8B
  - \u6682\u65E0
  #### \u8FD0\u884C\u56FE
  - \u6682\u65E0
  `);this.PageTitle="\u5B57\u6BB5\u540D\u94FE\u63A5"}Init(){return n(this,null,function*(){this.entity=new s,this.KeyOfEn=e.DoWay,yield this.entity.InitDataForMapAttr("FieldNameLink",this.GetRequestVal("PKVal")),this.AddGroup("A","\u5B57\u6BB5\u540D\u94FE\u63A5"),this.Blank("0","\u7981\u7528",this.Desc0),this.SingleTextArea("HelpInfo","\u5F39\u51FA\u5E2E\u52A9\u4FE1\u606F",e.Doc,"\u8BF7\u6309\u7167\u683C\u5F0F\u8F93\u5165\u5185\u5BB9",this.HelpInfo),this.SingleTB("UrlRightOpen","\u4FA7\u6ED1\u5F39\u51FAurl",e.Doc,this.UrlRightOpen,"\u8BF7\u8F93\u5165url"),this.AddEntity("UrlOpen","\u6A21\u6001\u5F39\u7A97",new m,this.UrlRightOpen,""),this.AddEntity("UrlWinOpen","\u65B0\u7A97\u53E3\u5F39\u51FAurl",new m,this.UrlRightOpen,"")})}AfterSave(u,r){if(u==r)throw new Error("Method not implemented.")}BtnClick(u,r,o){if(u==r||u===o)throw new Error("Method not implemented.")}}export{X as GPE_FieldNameLink};
