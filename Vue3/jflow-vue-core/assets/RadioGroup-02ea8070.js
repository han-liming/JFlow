import{a as ee,u as A}from"./use-config-816d55a6.js";import{u as U}from"./use-form-item-34ce685d.js";import{u as E,c as w}from"./use-merged-state-66be05d7.js";import{u as T}from"./use-memo-f04d43e5.js";import{r as k,k as oe,dO as I,d as te,p as re,f as V,a8 as P}from"./index-f4658ae7.js";import{k as ne,o as D,e as F,g as R,f as S,c as $,h as H,b as L}from"./light-0dfdc1ad.js";import{u as ae}from"./use-rtl-889b67fe.js";import{c as _}from"./create-key-bf4384d6.js";import{u as ie}from"./use-css-vars-class-3ae3b4b3.js";import{f as de}from"./flatten-2bdfb3d3.js";function se(e,r="default",o=[]){const a=e.$slots[r];return a===void 0?o:a()}const le={radioSizeSmall:"14px",radioSizeMedium:"16px",radioSizeLarge:"18px",labelPadding:"0 8px",labelFontWeight:"400"},ue=e=>{const{borderColor:r,primaryColor:o,baseColor:i,textColorDisabled:a,inputColorDisabled:f,textColor2:t,opacityDisabled:d,borderRadius:u,fontSizeSmall:g,fontSizeMedium:p,fontSizeLarge:m,heightSmall:c,heightMedium:x,heightLarge:v,lineHeight:C}=e;return Object.assign(Object.assign({},le),{labelLineHeight:C,buttonHeightSmall:c,buttonHeightMedium:x,buttonHeightLarge:v,fontSizeSmall:g,fontSizeMedium:p,fontSizeLarge:m,boxShadow:`inset 0 0 0 1px ${r}`,boxShadowActive:`inset 0 0 0 1px ${o}`,boxShadowFocus:`inset 0 0 0 1px ${o}, 0 0 0 2px ${D(o,{alpha:.2})}`,boxShadowHover:`inset 0 0 0 1px ${o}`,boxShadowDisabled:`inset 0 0 0 1px ${r}`,color:i,colorDisabled:f,colorActive:"#0000",textColor:t,textColorDisabled:a,dotColorActive:o,dotColorDisabled:r,buttonBorderColor:r,buttonBorderColorActive:o,buttonBorderColorHover:r,buttonColor:i,buttonColorActive:i,buttonTextColor:t,buttonTextColorActive:o,buttonTextColorHover:o,opacityDisabled:d,buttonBoxShadowFocus:`inset 0 0 0 1px ${o}, 0 0 0 2px ${D(o,{alpha:.3})}`,buttonBoxShadowHover:"inset 0 0 0 1px #0000",buttonBoxShadow:"inset 0 0 0 1px #0000",buttonBorderRadius:u})},ce={name:"Radio",common:ne,self:ue},be=ce,we={name:String,value:{type:[String,Number,Boolean],default:"on"},checked:{type:Boolean,default:void 0},defaultChecked:Boolean,disabled:{type:Boolean,default:void 0},label:String,size:String,onUpdateChecked:[Function,Array],"onUpdate:checked":[Function,Array],checkedValue:{type:Boolean,default:void 0}},M=ee("n-radio-group");function Be(e){const r=U(e,{mergedSize(n){const{size:s}=e;if(s!==void 0)return s;if(t){const{mergedSizeRef:{value:b}}=t;if(b!==void 0)return b}return n?n.mergedSize.value:"medium"},mergedDisabled(n){return!!(e.disabled||t!=null&&t.disabledRef.value||n!=null&&n.disabled.value)}}),{mergedSizeRef:o,mergedDisabledRef:i}=r,a=k(null),f=k(null),t=oe(M,null),d=k(e.defaultChecked),u=I(e,"checked"),g=E(u,d),p=T(()=>t?t.valueRef.value===e.value:g.value),m=T(()=>{const{name:n}=e;if(n!==void 0)return n;if(t)return t.nameRef.value}),c=k(!1);function x(){if(t){const{doUpdateValue:n}=t,{value:s}=e;w(n,s)}else{const{onUpdateChecked:n,"onUpdate:checked":s}=e,{nTriggerFormInput:b,nTriggerFormChange:l}=r;n&&w(n,!0),s&&w(s,!0),b(),l(),d.value=!0}}function v(){i.value||p.value||x()}function C(){v()}function z(){c.value=!1}function y(){c.value=!0}return{mergedClsPrefix:t?t.mergedClsPrefixRef:A(e).mergedClsPrefixRef,inputRef:a,labelRef:f,mergedName:m,mergedDisabled:i,uncontrolledChecked:d,renderSafeChecked:p,focus:c,mergedSize:o,handleRadioInputChange:C,handleRadioInputBlur:z,handleRadioInputFocus:y}}const fe=F("radio-group",`
 display: inline-block;
 font-size: var(--n-font-size);
`,[R("splitor",`
 display: inline-block;
 vertical-align: bottom;
 width: 1px;
 transition:
 background-color .3s var(--n-bezier),
 opacity .3s var(--n-bezier);
 background: var(--n-button-border-color);
 `,[S("checked",{backgroundColor:"var(--n-button-border-color-active)"}),S("disabled",{opacity:"var(--n-opacity-disabled)"})]),S("button-group",`
 white-space: nowrap;
 height: var(--n-height);
 line-height: var(--n-height);
 `,[F("radio-button",{height:"var(--n-height)",lineHeight:"var(--n-height)"}),R("splitor",{height:"var(--n-height)"})]),F("radio-button",`
 vertical-align: bottom;
 outline: none;
 position: relative;
 user-select: none;
 -webkit-user-select: none;
 display: inline-block;
 box-sizing: border-box;
 padding-left: 14px;
 padding-right: 14px;
 white-space: nowrap;
 transition:
 background-color .3s var(--n-bezier),
 opacity .3s var(--n-bezier),
 border-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 color: var(--n-button-text-color);
 border-top: 1px solid var(--n-button-border-color);
 border-bottom: 1px solid var(--n-button-border-color);
 `,[F("radio-input",`
 pointer-events: none;
 position: absolute;
 border: 0;
 border-radius: inherit;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 opacity: 0;
 z-index: 1;
 `),R("state-border",`
 z-index: 1;
 pointer-events: none;
 position: absolute;
 box-shadow: var(--n-button-box-shadow);
 transition: box-shadow .3s var(--n-bezier);
 left: -1px;
 bottom: -1px;
 right: -1px;
 top: -1px;
 `),$("&:first-child",`
 border-top-left-radius: var(--n-button-border-radius);
 border-bottom-left-radius: var(--n-button-border-radius);
 border-left: 1px solid var(--n-button-border-color);
 `,[R("state-border",`
 border-top-left-radius: var(--n-button-border-radius);
 border-bottom-left-radius: var(--n-button-border-radius);
 `)]),$("&:last-child",`
 border-top-right-radius: var(--n-button-border-radius);
 border-bottom-right-radius: var(--n-button-border-radius);
 border-right: 1px solid var(--n-button-border-color);
 `,[R("state-border",`
 border-top-right-radius: var(--n-button-border-radius);
 border-bottom-right-radius: var(--n-button-border-radius);
 `)]),H("disabled",`
 cursor: pointer;
 `,[$("&:hover",[R("state-border",`
 transition: box-shadow .3s var(--n-bezier);
 box-shadow: var(--n-button-box-shadow-hover);
 `),H("checked",{color:"var(--n-button-text-color-hover)"})]),S("focus",[$("&:not(:active)",[R("state-border",{boxShadow:"var(--n-button-box-shadow-focus)"})])])]),S("checked",`
 background: var(--n-button-color-active);
 color: var(--n-button-text-color-active);
 border-color: var(--n-button-border-color-active);
 `),S("disabled",`
 cursor: not-allowed;
 opacity: var(--n-opacity-disabled);
 `)])]);function he(e,r,o){var i;const a=[];let f=!1;for(let t=0;t<e.length;++t){const d=e[t],u=(i=d.type)===null||i===void 0?void 0:i.name;u==="RadioButton"&&(f=!0);const g=d.props;if(u!=="RadioButton"){a.push(d);continue}if(t===0)a.push(d);else{const p=a[a.length-1].props,m=r===p.value,c=p.disabled,x=r===g.value,v=g.disabled,C=(m?2:0)+(c?0:1),z=(x?2:0)+(v?0:1),y={[`${o}-radio-group__splitor--disabled`]:c,[`${o}-radio-group__splitor--checked`]:m},n={[`${o}-radio-group__splitor--disabled`]:v,[`${o}-radio-group__splitor--checked`]:x},s=C<z?n:y;a.push(P("div",{class:[`${o}-radio-group__splitor`,s]}),d)}}return{children:a,isButtonGroup:f}}const ge=Object.assign(Object.assign({},L.props),{name:String,value:[String,Number,Boolean],defaultValue:{type:[String,Number,Boolean],default:null},size:String,disabled:{type:Boolean,default:void 0},"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array]}),Fe=te({name:"RadioGroup",props:ge,setup(e){const r=k(null),{mergedSizeRef:o,mergedDisabledRef:i,nTriggerFormChange:a,nTriggerFormInput:f,nTriggerFormBlur:t,nTriggerFormFocus:d}=U(e),{mergedClsPrefixRef:u,inlineThemeDisabled:g,mergedRtlRef:p}=A(e),m=L("Radio","-radio-group",fe,be,e,u),c=k(e.defaultValue),x=I(e,"value"),v=E(x,c);function C(l){const{onUpdateValue:h,"onUpdate:value":B}=e;h&&w(h,l),B&&w(B,l),c.value=l,a(),f()}function z(l){const{value:h}=r;h&&(h.contains(l.relatedTarget)||d())}function y(l){const{value:h}=r;h&&(h.contains(l.relatedTarget)||t())}re(M,{mergedClsPrefixRef:u,nameRef:I(e,"name"),valueRef:v,disabledRef:i,mergedSizeRef:o,doUpdateValue:C});const n=ae("Radio",p,u),s=V(()=>{const{value:l}=o,{common:{cubicBezierEaseInOut:h},self:{buttonBorderColor:B,buttonBorderColorActive:j,buttonBorderRadius:G,buttonBoxShadow:N,buttonBoxShadowFocus:O,buttonBoxShadowHover:K,buttonColorActive:W,buttonTextColor:q,buttonTextColorActive:J,buttonTextColorHover:Q,opacityDisabled:X,[_("buttonHeight",l)]:Y,[_("fontSize",l)]:Z}}=m.value;return{"--n-font-size":Z,"--n-bezier":h,"--n-button-border-color":B,"--n-button-border-color-active":j,"--n-button-border-radius":G,"--n-button-box-shadow":N,"--n-button-box-shadow-focus":O,"--n-button-box-shadow-hover":K,"--n-button-color-active":W,"--n-button-text-color":q,"--n-button-text-color-hover":Q,"--n-button-text-color-active":J,"--n-height":Y,"--n-opacity-disabled":X}}),b=g?ie("radio-group",V(()=>o.value[0]),s,e):void 0;return{selfElRef:r,rtlEnabled:n,mergedClsPrefix:u,mergedValue:v,handleFocusout:y,handleFocusin:z,cssVars:g?void 0:s,themeClass:b==null?void 0:b.themeClass,onRender:b==null?void 0:b.onRender}},render(){var e;const{mergedValue:r,mergedClsPrefix:o,handleFocusin:i,handleFocusout:a}=this,{children:f,isButtonGroup:t}=he(de(se(this)),r,o);return(e=this.onRender)===null||e===void 0||e.call(this),P("div",{onFocusin:i,onFocusout:a,ref:"selfElRef",class:[`${o}-radio-group`,this.rtlEnabled&&`${o}-radio-group--rtl`,this.themeClass,t&&`${o}-radio-group--button-group`],style:this.cssVars},f)}});export{Fe as N,be as a,se as g,we as r,Be as s};
