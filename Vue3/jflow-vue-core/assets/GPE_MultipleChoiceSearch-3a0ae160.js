var l=Object.defineProperty;var h=(E,t,u)=>t in E?l(E,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[t]=u;var o=(E,t,u)=>(h(E,typeof t!="symbol"?t+"":t,u),u);var a=(E,t,u)=>new Promise((F,i)=>{var e=r=>{try{c(u.next(r))}catch(m){i(m)}},p=r=>{try{c(u.throw(r))}catch(m){i(m)}},c=r=>r.done?F(r.value):Promise.resolve(r.value).then(e,p);c((u=u.apply(E,t)).next())});import{b as C,a as n}from"./MapExt-db8cd7f3.js";import{MultipleChoiceSearchEn1 as s}from"./MultipleChoiceSearchEn1-caff9578.js";import{MultipleChoiceSearchEn2 as D}from"./MultipleChoiceSearchEn2-05076d72.js";import{PageBaseGroupEdit as M}from"./PageBaseGroupEdit-202e8e85.js";import{MapAttr as B}from"./MapAttr-cb594d82.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./Events-141c34ea.js";class $ extends M{constructor(){super("GPE_MultipleChoiceSearch");o(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u5728\u914D\u7F6E\u9875\u9762\u4E2D\u914D\u7F6E\u597D\u6570\u636E\u6E90\uFF0C\u90A3\u4E48\u5F55\u5165\u7684\u65F6\u5019\u53EF\u4EE5\u6839\u636E\u5F55\u5165\u7684\u5173\u952E\u8BCD\uFF0C\u641C\u7D20\u51FA\u76F8\u5173\u7684\u5185\u5BB9\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u6BD4\u5982\uFF0C\u7533\u8BF7\u4EBA\uFF0C\u7533\u8BF7\u539F\u56E0\u7B49\uFF1B
  #### \u641C\u7D22\u9009\u62E9-\u6548\u679C\u56FE
  - \u641C\u7D22\u9009\u62E9
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleChoiceSearch.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u641C\u7D22+\u8F93\u5165\u591A\u9009-\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleInputSearch.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);o(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u641C\u7D22\u9009\u62E9:\u9009\u62E9\u7684\u8303\u56F4\u5FC5\u987B\u662F\u5728\u6570\u636E\u6E90\u4E2D.
   - \u53EF\u4EE5\u901A\u8FC7\u5173\u952E\u5B57\uFF0C\u8FDB\u884C\u641C\u7D22\u6570\u636E\u6E90\u7684\u5185\u5BB9.
  #### \u914D\u7F6E\u56FE
  
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleChoiceSearchPeizhi.png "\u6570\u636E\u6E90\u914D\u7F6E")
  #### \u6548\u679C\u56FE
  
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleChoiceSearch.png "\u8FD0\u884C\u6548\u679C")
  `);o(this,"Desc2",`
  #### \u5E2E\u52A9
  - \u641C\u7D22\u9009\u62E9+\u8F93\u5165\u591A\u9009:\u9009\u62E9\u7684\u8303\u56F4\u662F\u6570\u636E\u6E90\u4E2D\u7684\uFF0C\u4E5F\u53EF\u4EE5\u662F\u5F55\u5165\u7684.
  - \u53EF\u4EE5\u901A\u8FC7\u5173\u952E\u5B57\uFF0C\u8FDB\u884C\u641C\u7D22\u6570\u636E\u6E90\u7684\u5185\u5BB9\u8FDB\u884C\u9009\u62E9\u4E5F\u53EF\u4EE5\u8F93\u5165\u6570\u636E.
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleChoiceSearchPeizhi2.png "\u6570\u636E\u6E90\u914D\u7F6E")
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/MultipleChoiceSearch/Img/MultipleInputSearch.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);this.PageTitle="\u641C\u7D22\u9009\u62E9"}BtnClick(u,F,i){if(u==F||u===i)throw new Error("Method not implemented.")}AfterSave(u,F){return a(this,null,function*(){if(u!="None"){let i=this.GetRequestVal("PKVal");i.endsWith("_MultipleChoiceSearch")&&(i=i.replace("_MultipleChoiceSearch",""));const e=new B,p=i;e.setPKVal(i+"T"),(yield e.RetrieveFromDBSources())==0&&(e.setPKVal(p),yield e.RetrieveFromDBSources(),e.MyPK=e.MyPK+"T",e.KeyOfEn=e.KeyOfEn+"T",e.Name=e.Name+"T",e.UIVisible=!1,e.UIIsEnable=!1,yield e.Insert())}if(u==F)throw new Error("Method not implemented.")})}Init(){return a(this,null,function*(){this.entity=new C,this.KeyOfEn=n.DoWay,yield this.entity.InitDataForMapAttr("MultipleChoiceSearch",this.GetRequestVal("PKVal")),this.AddGroup("A","\u641C\u7D22\u9009\u62E9"),this.Blank("0","\u4E0D\u8BBE\u7F6E",this.Desc0),this.AddEntity("1","\u641C\u7D22\u9009\u62E9",new s,this.Desc1),this.AddEntity("2","\u641C\u7D22\u9009\u62E9+\u8F93\u5165\u591A\u9009",new D,this.Desc2)})}}export{$ as GPE_MultipleChoiceSearch};
