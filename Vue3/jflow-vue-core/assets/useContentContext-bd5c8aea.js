import{aJ as n,aI as o}from"./index-f4658ae7.js";const t=Symbol();function a(e){return n(e,t,{native:!0})}function u(){return o(t)}export{a as createContentContext,u as useContentContext};
