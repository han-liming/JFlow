var s=Object.defineProperty;var D=(E,F,u)=>F in E?s(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var B=(E,F,u)=>(D(E,typeof F!="symbol"?F+"":F,u),u);var m=(E,F,u)=>new Promise((p,e)=>{var r=t=>{try{l(u.next(t))}catch(o){e(o)}},i=t=>{try{l(u.throw(t))}catch(o){e(o)}},l=t=>t.done?p(t.value):Promise.resolve(t.value).then(r,i);l((u=u.apply(E,F)).next())});import{TBFullCtrl1 as g}from"./TBFullCtrl1-5dff34f0.js";import{PageBaseGroupEdit as S}from"./PageBaseGroupEdit-202e8e85.js";import{TBFullCtrl2 as y}from"./TBFullCtrl2-13173e38.js";import{b as T,a as c}from"./MapExt-db8cd7f3.js";import{GloComm as C}from"./GloComm-7cfbdfd9.js";import{GPNReturnObj as a,GPNReturnType as A}from"./PageBaseGroupNew-ee20c033.js";import{MENoNameP1 as n}from"./MENoNameP1-b9165976.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./SFDBSrc-e641ea16.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";class Bu extends S{constructor(){super("GPE_TBFullCtrl");B(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u7981\u7528\uFF1A\u4E0D\u542F\u7528.
  - \u6587\u672C\u6846\u7684\u81EA\u52A8\u5B8C\u6210\uFF0C\u5C31\u662F\u5728\u6587\u672C\u6846\u8F93\u5165\u7684\u65F6\u5019\uFF0C\u8F93\u5165\u7279\u5B9A\u7684\u5173\u952E\u5B57\u5C31\u53EF\u4EE5\u641C\u7D22\u5230\u8981\u586B\u5199\u7684\u5185\u5BB9.
  - \u6BD4\u5982\uFF1A\u4EBA\u5458\u8F93\u5165\u3001\u836F\u54C1\u8F93\u5165.
  - \u5206\u4E3A\uFF1A\u7B80\u6D01\u6A21\u5F0F\u4E0E\u8868\u683C\u6A21\u5F0F\u4E24\u79CD.
  #### \u6548\u679C\u56FE
   - \u7B80\u6D01\u6A21\u5F0F 
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/Simple.png "\u5C4F\u5E55\u622A\u56FE.png")

  #### \u6548\u679C\u56FE
   -  \u8868\u683C\u6A21\u5F0F
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/Simple1.png "\u5C4F\u5E55\u622A\u56FE.png")

  `);B(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u586B\u5145SQL\u5E2E\u52A9
   1. \u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2\u7684SQL\u8BED\u53E5\uFF0C\u8BE5SQL\u5FC5\u987B\u5305\u542B No, Name \u5217, \u7528\u4E0E\u5C55\u793A\u5FEB\u901F\u8865\u5168\u7684\u90E8\u5206\u3002
   1. \u8BE5SQL\u5FC5\u987B\u5305\u542B @Key \u5173\u952E\u5B57\uFF0C@Key \u8F93\u5165\u6587\u672C\u6846\u7684\u503C.
   1. SQL\u8FD4\u56DE\u7684\u5217\u4E0E\u5176\u4ED6\u5B57\u6BB5\u540D\u79F0\u4FDD\u6301\u4E00\u81F4\uFF0C\u5C31\u53EF\u4EE5\u5B8C\u6210\u63A7\u4EF6\u6570\u636E\u7684\u81EA\u52A8\u586B\u5145\u3002
   1. \u6BD4\u5982: SELECT No,Name FROM WF_Emp WHERE No LIKE '@Key%'
   1. \u4E3A\u9632\u6B62URL\u7F16\u7801\u89C4\u5B9Alike\u7684\u7B2C\u4E00\u4E2A%\u5199\u6210[%],\u5982\u679Clike '%@Key%' \u5199\u6210'[%]@Key%'
   - \u586B\u5145Url\u5E2E\u52A9
   1. \u8BBE\u7F6EURL\uFF0C\u8FD4\u56DE\u7684\u5FC5\u987B\u662Fjson\u683C\u5F0F\u3002
   1. \u6BD4\u5982: /App/Handler.ashx?DoType=Emps&Key=@Key
   1. @Key \u662F\u8F93\u5165\u7684\u5173\u952E\u5B57

  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/SimplePeizhi.png "\u5C4F\u5E55\u622A\u56FE.png")

  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/Simple.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);B(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u586B\u5145SQL\u5E2E\u52A9
  1. \u8BBE\u7F6E\u4E00\u4E2A\u67E5\u8BE2\u7684SQL\u8BED\u53E5\uFF0C\u8BE5SQL\u5FC5\u987B\u5305\u542B No, Name \u5217, \u7528\u4E0E\u5C55\u793A\u5FEB\u901F\u8865\u5168\u7684\u90E8\u5206\u3002
  1. \u8BE5SQL\u5FC5\u987B\u5305\u542B @Key \u5173\u952E\u5B57\uFF0C@Key \u8F93\u5165\u6587\u672C\u6846\u7684\u503C.
  1. SQL\u8FD4\u56DE\u7684\u5217\u4E0E\u5176\u4ED6\u5B57\u6BB5\u540D\u79F0\u4FDD\u6301\u4E00\u81F4\uFF0C\u5C31\u53EF\u4EE5\u5B8C\u6210\u63A7\u4EF6\u6570\u636E\u7684\u81EA\u52A8\u586B\u5145\u3002
  1. \u6BD4\u5982:SELECT No,Name,Name as CaoZuoYuanMingCheng,Tel as DianHua,Email,FK_Dept FROM WF_Emp WHERE No LIKE '@Key%'
  1. \u4E3A\u9632\u6B62URL\u7F16\u7801\u89C4\u5B9Alike\u7684\u7B2C\u4E00\u4E2A%\u5199\u6210[%],\u5982\u679Clike '%@Key%' \u5199\u6210'[%]@Key%'

  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/SimplePeizhi1.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/TBFullCtrl/Img/Simple1.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);this.PageTitle="\u6587\u672C\u6846\u81EA\u52A8\u5B8C\u6210"}AfterSave(u,p){}BtnClick(u,p,e){var r;if(e=="\u5B57\u5178\u7EF4\u62A4"){const i=C.UrlSearch("TS.FrmUI.SFTable");return new a(A.OpenUrlByDrawer75,i)}if(e==="\u843D\u503C\u586B\u5145"||e==="\u586B\u5145"){const i=C.UrlEn("TS.MapExt.FullData",(r=this.entity)==null?void 0:r.MyPK);return new a(A.OpenUrlByDrawer75,i)}}Init(){return m(this,null,function*(){this.entity=new T,this.KeyOfEn=c.DoWay,yield this.entity.InitDataForMapAttr("TBFullCtrl",this.GetRequestVal("PKVal")),this.Btns=[{pageNo:"Simple",list:["\u586B\u5145"]},{pageNo:"Table",list:["\u586B\u5145"]},{pageNo:"SimpleSFTable",list:["\u586B\u5145","\u5B57\u5178\u7EF4\u62A4"]},{pageNo:"TableSFTable",list:["\u586B\u5145","\u5B57\u5178\u7EF4\u62A4"]}],yield this.entity.InitDataForMapAttr("TBFullCtrl",this.GetRequestVal("PKVal")),this.AddGroup("A","\u6587\u672C\u6846\u81EA\u52A8\u5B8C\u6210"),this.Blank("None","\u7981\u7528",this.Desc0),this.AddEntity("Simple","\u7B80\u6D01\u6A21\u5F0F",new g,this.Desc1),this.AddEntity("Table","\u8868\u683C\u6A21\u5F0F",new y,this.Desc2),this.AddEntity("SimpleSFTable","\u7B80\u6D01\u6A21\u5F0F(\u5B57\u5178\u8868)",new n,this.Desc1),this.AddEntity("TableSFTable","\u8868\u683C\u6A21\u5F0F(\u5B57\u5178\u8868)",new n,this.Desc2)})}}export{Bu as GPE_TBFullCtrl};
