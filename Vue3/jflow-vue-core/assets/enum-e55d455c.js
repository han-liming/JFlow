var t=(O=>(O.ZOOM_IN="zoomIn",O.ZOOM_OUT="zoomOut",O.RESET_ZOOM="resetZoom",O.UNDO="undo",O.REDO="redo",O.SNAPSHOT="snapshot",O.VIEW_DATA="viewData",O))(t||{});export{t as ToolbarTypeEnum};
