var e=Object.defineProperty;var m=(i,u,t)=>u in i?e(i,u,{enumerable:!0,configurable:!0,writable:!0,value:t}):i[u]=t;var o=(i,u,t)=>(m(i,typeof u!="symbol"?u+"":u,t),t);import{MapDtl as s,MapDtlAttr as r}from"./MapDtl-dc3f1bee.js";import{PageBaseGroupEdit as l}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class K extends l{constructor(){super("GPE_MobileShowCols");o(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u591A\u4E2A\u5B57\u6BB5\u7528\u9017\u53F7\u5206\u5F00
  - \u6BD4\u5982: File1,FIle2

  `);o(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u591A\u4E2A\u5B57\u6BB5\u7528\u9017\u53F7\u5206\u5F00
  - \u6BD4\u5982: File1,FIle2

  ...`);this.PageTitle="\u5B57\u6BB5\u5C55\u73B0\u6A21\u5F0F"}Init(){this.entity=new s,this.KeyOfEn=r.EditModel,this.AddGroup("A","\u5C55\u793A\u6A21\u5F0F"),this.Blank("0","\u65B0\u9875\u9762\u5C55\u793A",this.Desc0),this.SingleTextArea("1","\u5217\u8868\u5C55\u793A",r.ShowCols,"\u591A\u4E2A\u5B57\u6BB5\u7528\u9017\u53F7\u5206\u5F00",this.Desc1)}BtnClick(t,p,B){}AfterSave(t,p){}}export{K as GPE_MobileShowCols};
