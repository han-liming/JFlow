var e=Object.defineProperty;var E=(t,F,u)=>F in t?e(t,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[F]=u;var r=(t,F,u)=>(E(t,typeof F!="symbol"?F+"":F,u),u);import{EntityNoName as B}from"./EntityNoName-d08126ae.js";import{UAC as C}from"./UAC-8e255d47.js";import{Map as D}from"./Map-73575e6b.js";import"./DataType-33901a1c.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Entities-6a72b013.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";class y extends B{constructor(u){super("TS.WF.Template.FlowTestParaSetting");r(this,"DescTestSysPara",`
  #### \u5E2E\u52A9
  - \u6D41\u7A0B\u7CFB\u7EDF\u53C2\u6570\u662F\u6D41\u7A0B\u5B9E\u4F8B\u5728\u8FD0\u884C\u8FC7\u7A0B\u4E2D\uFF0C\u7531\u5916\u90E8\u63A5\u53E3\u4F20\u9012\u5199\u5165\u7684\u53C2\u6570\u3002
  - \u53C2\u6570\u5B58\u50A8\u5728WF_GenerWorkFlow\u8868\u7684 AtPara\u5B57\u6BB5\u91CC\u9762\uFF0C\u683C\u5F0F\u4E3A @Key1=Val1@Key2=Val2
  - \u8C03\u7528\u63A5\u53E3 Flow_SaveParas(workID, paras) \u53EF\u4EE5\u4FDD\u5B58\u6216\u8005\u66F4\u6539\u7CFB\u7EDF\u53C2\u6570. 
  - \u8BE5\u53C2\u6570\u53EF\u4EE5\u7528\u5230\uFF0C\u65B9\u5411\u6761\u4EF6\u3001\u63A5\u53D7\u4EBA\u89C4\u5219\u3001\u8868\u5355\u7684url\u53C2\u6570.
  #### \u586B\u5199\u793A\u4F8B
  @QingJiaTianShu=14
  #### \u5E38\u89C1\u7684\u56FA\u5B9A\u53C2\u6570\u7EA6\u5B9A.
  - \u53D1\u8D77\u6D41\u7A0B\u4F20\u5165\u7684\u5D4C\u5165\u6A21\u5F0F\u7684\u8868\u5355url\u53C2\u6570. \u53C2\u6570\u952E\u503C:  
  - \u793A\u4F8B: 

  #### \u5D4C\u5165\u5D4C\u5165\u5F0F\u8868\u5355url\u5730\u5740\u53C2\u6570
  - \u53C2\u6570\u952E\u503C: FrmUrl
  - \u793A\u4F8B:

  `);r(this,"TestFrmPara",`
  #### \u5E2E\u52A9
  - \u8868\u5355\u53C2\u6570\u662F\u6D4B\u8BD5\u7684\u65F6\u5019\uFF0C\u4E3A\u4E86\u907F\u514D\u91CD\u590D\u5F55\u5165\u5728\u53D1\u8D77\u4E4B\u524D\u8BBE\u7F6E\u7684\u9ED8\u8BA4\u5B57\u6BB5\u503C.
  - \u542F\u52A8\u6D41\u7A0B\u7684\u65F6\u5019\uFF0C\u7CFB\u7EDF\u81EA\u52A8\u6309\u7167\u8BBE\u7F6E\u7684\u6570\u636E\u586B\u5145\u5230\u8282\u70B9\u8868\u5355\uFF0C\u7136\u540E\u542F\u52A8\u6D41\u7A0B.
  #### \u53C2\u6570\u793A\u4F8B
  @XMMC=\u9A70\u9A8BBPM\u5DE5\u4F5C\u6D41\u5F15\u64CE\u9879\u76EE
  @XMJE=100
  @XMDZ=\u5C71\u4E1C\u6D4E\u5357\u9AD8\u65B0\u533A.\u78A7\u6842\u56ED\u51E4\u51F0\u4E2D\u5FC3.A\u5EA71903
  
  
  `);u&&this.setPKVal(u)}get HisUAC(){const u=new C;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new D("WF_Flow","\u6D4B\u8BD5\u53C2\u6570");return u.AddTBStringPK("No",null,"\u7F16\u53F7",!0,!0,1,3,50),u.AddTBString("Name",null,"\u540D\u79F0",!0,!0,0,50,500),u.AddTBStringDoc("TestSysPara",null,"\u6D41\u7A0B\u7CFB\u7EDF\u53C2\u6570",!0,!1,!0,this.DescTestSysPara),u.AddTBStringDoc("TestFrmPara",null,"\u8868\u5355\u586B\u5145\u6570\u636E",!0,!1,!0,this.TestFrmPara),this._enMap=u,this._enMap}}export{y as FlowTestParaSetting};
