import{a as Te,u as fe}from"./use-config-816d55a6.js";import{t as he,w as ot}from"./warn-77f3ea30.js";import{a8 as t,d as Q,dO as F,r as H,O as nt,b3 as je,k as oe,f as $,s as Ie,bi as be,F as xe,v as rt,a9 as it,D as lt,p as Le,P as Be,G as ye,ep as at,J as st}from"./index-f4658ae7.js";import{i as dt,u as ut,b as ct,a as ft}from"./Loading-fead3a83.js";import{a as ht,u as gt,c as De}from"./use-merged-state-66be05d7.js";import{N as _}from"./Icon-e3cbad7d.js";import{A as vt}from"./Add-4d1c6932.js";import{p as mt,N as pt}from"./Progress-892d6a15.js";import{N as Ee}from"./FadeInExpandTransition-fc975915.js";import{u as wt}from"./use-memo-f04d43e5.js";import{o as bt,i as Ct}from"./utils-b08bcee3.js";import{b as ge,j as $e,k as He,o as xt,c as U,e as w,h as yt,f as N,g as K}from"./light-0dfdc1ad.js";import{f as ze,a as Ce,o as se}from"./Scrollbar-35d51129.js";import{f as Rt}from"./fade-in-scale-up.cssr-0b26e361.js";import{t as Tt,N as Lt}from"./Tooltip-02d89ff2.js";import{u as Ne}from"./use-css-vars-class-3ae3b4b3.js";import{i as kt}from"./use-is-mounted-a34b74be.js";import{L as Pt,z as St}from"./Follower-3b5f0c65.js";import{r as ee}from"./replaceable-a957a029.js";import{c as Ot}from"./_createCompounder-ed41a610.js";import{b as It}from"./next-frame-once-7035a838.js";import{c as Re}from"./index-cad90cf4.js";import{b as Bt,N as de}from"./Button-53926a3b.js";import{E as Dt}from"./Eye-9d82dd1a.js";import{f as Fe}from"./fade-in-height-expand.cssr-390ab856.js";import{u as zt}from"./use-form-item-34ce685d.js";var Ft=Ot(function(e,n,o){return e+(o?"-":"")+n.toLowerCase()});const Mt=Ft,Ut=ee("attach",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M3.25735931,8.70710678 L7.85355339,4.1109127 C8.82986412,3.13460197 10.4127766,3.13460197 11.3890873,4.1109127 C12.365398,5.08722343 12.365398,6.67013588 11.3890873,7.64644661 L6.08578644,12.9497475 C5.69526215,13.3402718 5.06209717,13.3402718 4.67157288,12.9497475 C4.28104858,12.5592232 4.28104858,11.9260582 4.67157288,11.5355339 L9.97487373,6.23223305 C10.1701359,6.0369709 10.1701359,5.72038841 9.97487373,5.52512627 C9.77961159,5.32986412 9.4630291,5.32986412 9.26776695,5.52512627 L3.96446609,10.8284271 C3.18341751,11.6094757 3.18341751,12.8758057 3.96446609,13.6568542 C4.74551468,14.4379028 6.01184464,14.4379028 6.79289322,13.6568542 L12.0961941,8.35355339 C13.4630291,6.98671837 13.4630291,4.77064094 12.0961941,3.40380592 C10.7293591,2.0369709 8.51328163,2.0369709 7.14644661,3.40380592 L2.55025253,8 C2.35499039,8.19526215 2.35499039,8.51184464 2.55025253,8.70710678 C2.74551468,8.90236893 3.06209717,8.90236893 3.25735931,8.70710678 Z"}))))),_t=ee("trash",t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512"},t("path",{d:"M432,144,403.33,419.74A32,32,0,0,1,371.55,448H140.46a32,32,0,0,1-31.78-28.26L80,144",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("rect",{x:"32",y:"64",width:"448",height:"80",rx:"16",ry:"16",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("line",{x1:"312",y1:"240",x2:"200",y2:"352",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("line",{x1:"312",y1:"352",x2:"200",y2:"240",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}))),jt=ee("download",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M3.5,13 L12.5,13 C12.7761424,13 13,13.2238576 13,13.5 C13,13.7454599 12.8231248,13.9496084 12.5898756,13.9919443 L12.5,14 L3.5,14 C3.22385763,14 3,13.7761424 3,13.5 C3,13.2545401 3.17687516,13.0503916 3.41012437,13.0080557 L3.5,13 L12.5,13 L3.5,13 Z M7.91012437,1.00805567 L8,1 C8.24545989,1 8.44960837,1.17687516 8.49194433,1.41012437 L8.5,1.5 L8.5,10.292 L11.1819805,7.6109127 C11.3555469,7.43734635 11.6249713,7.4180612 11.8198394,7.55305725 L11.8890873,7.6109127 C12.0626536,7.78447906 12.0819388,8.05390346 11.9469427,8.2487716 L11.8890873,8.31801948 L8.35355339,11.8535534 C8.17998704,12.0271197 7.91056264,12.0464049 7.7156945,11.9114088 L7.64644661,11.8535534 L4.1109127,8.31801948 C3.91565056,8.12275734 3.91565056,7.80617485 4.1109127,7.6109127 C4.28447906,7.43734635 4.55390346,7.4180612 4.7487716,7.55305725 L4.81801948,7.6109127 L7.5,10.292 L7.5,1.5 C7.5,1.25454011 7.67687516,1.05039163 7.91012437,1.00805567 L8,1 L7.91012437,1.00805567 Z"}))))),Et=ee("cancel",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M2.58859116,2.7156945 L2.64644661,2.64644661 C2.82001296,2.47288026 3.08943736,2.45359511 3.2843055,2.58859116 L3.35355339,2.64644661 L8,7.293 L12.6464466,2.64644661 C12.8417088,2.45118446 13.1582912,2.45118446 13.3535534,2.64644661 C13.5488155,2.84170876 13.5488155,3.15829124 13.3535534,3.35355339 L8.707,8 L13.3535534,12.6464466 C13.5271197,12.820013 13.5464049,13.0894374 13.4114088,13.2843055 L13.3535534,13.3535534 C13.179987,13.5271197 12.9105626,13.5464049 12.7156945,13.4114088 L12.6464466,13.3535534 L8,8.707 L3.35355339,13.3535534 C3.15829124,13.5488155 2.84170876,13.5488155 2.64644661,13.3535534 C2.45118446,13.1582912 2.45118446,12.8417088 2.64644661,12.6464466 L7.293,8 L2.64644661,3.35355339 C2.47288026,3.17998704 2.45359511,2.91056264 2.58859116,2.7156945 L2.64644661,2.64644661 L2.58859116,2.7156945 Z"}))))),$t=ee("retry",t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512"},t("path",{d:"M320,146s24.36-12-64-12A160,160,0,1,0,416,294",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-miterlimit: 10; stroke-width: 32px;"}),t("polyline",{points:"256 58 336 138 256 218",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}))),Ht=ee("rotateClockwise",t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10C17 12.7916 15.3658 15.2026 13 16.3265V14.5C13 14.2239 12.7761 14 12.5 14C12.2239 14 12 14.2239 12 14.5V17.5C12 17.7761 12.2239 18 12.5 18H15.5C15.7761 18 16 17.7761 16 17.5C16 17.2239 15.7761 17 15.5 17H13.8758C16.3346 15.6357 18 13.0128 18 10C18 5.58172 14.4183 2 10 2C5.58172 2 2 5.58172 2 10C2 10.2761 2.22386 10.5 2.5 10.5C2.77614 10.5 3 10.2761 3 10Z",fill:"currentColor"}),t("path",{d:"M10 12C11.1046 12 12 11.1046 12 10C12 8.89543 11.1046 8 10 8C8.89543 8 8 8.89543 8 10C8 11.1046 8.89543 12 10 12ZM10 11C9.44772 11 9 10.5523 9 10C9 9.44772 9.44772 9 10 9C10.5523 9 11 9.44772 11 10C11 10.5523 10.5523 11 10 11Z",fill:"currentColor"}))),Nt=ee("rotateClockwise",t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M17 10C17 6.13401 13.866 3 10 3C6.13401 3 3 6.13401 3 10C3 12.7916 4.63419 15.2026 7 16.3265V14.5C7 14.2239 7.22386 14 7.5 14C7.77614 14 8 14.2239 8 14.5V17.5C8 17.7761 7.77614 18 7.5 18H4.5C4.22386 18 4 17.7761 4 17.5C4 17.2239 4.22386 17 4.5 17H6.12422C3.66539 15.6357 2 13.0128 2 10C2 5.58172 5.58172 2 10 2C14.4183 2 18 5.58172 18 10C18 10.2761 17.7761 10.5 17.5 10.5C17.2239 10.5 17 10.2761 17 10Z",fill:"currentColor"}),t("path",{d:"M10 12C8.89543 12 8 11.1046 8 10C8 8.89543 8.89543 8 10 8C11.1046 8 12 8.89543 12 10C12 11.1046 11.1046 12 10 12ZM10 11C10.5523 11 11 10.5523 11 10C11 9.44772 10.5523 9 10 9C9.44772 9 9 9.44772 9 10C9 10.5523 9.44772 11 10 11Z",fill:"currentColor"}))),At=ee("zoomIn",t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M11.5 8.5C11.5 8.22386 11.2761 8 11 8H9V6C9 5.72386 8.77614 5.5 8.5 5.5C8.22386 5.5 8 5.72386 8 6V8H6C5.72386 8 5.5 8.22386 5.5 8.5C5.5 8.77614 5.72386 9 6 9H8V11C8 11.2761 8.22386 11.5 8.5 11.5C8.77614 11.5 9 11.2761 9 11V9H11C11.2761 9 11.5 8.77614 11.5 8.5Z",fill:"currentColor"}),t("path",{d:"M8.5 3C11.5376 3 14 5.46243 14 8.5C14 9.83879 13.5217 11.0659 12.7266 12.0196L16.8536 16.1464C17.0488 16.3417 17.0488 16.6583 16.8536 16.8536C16.68 17.0271 16.4106 17.0464 16.2157 16.9114L16.1464 16.8536L12.0196 12.7266C11.0659 13.5217 9.83879 14 8.5 14C5.46243 14 3 11.5376 3 8.5C3 5.46243 5.46243 3 8.5 3ZM8.5 4C6.01472 4 4 6.01472 4 8.5C4 10.9853 6.01472 13 8.5 13C10.9853 13 13 10.9853 13 8.5C13 6.01472 10.9853 4 8.5 4Z",fill:"currentColor"}))),Vt=ee("zoomOut",t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M11 8C11.2761 8 11.5 8.22386 11.5 8.5C11.5 8.77614 11.2761 9 11 9H6C5.72386 9 5.5 8.77614 5.5 8.5C5.5 8.22386 5.72386 8 6 8H11Z",fill:"currentColor"}),t("path",{d:"M14 8.5C14 5.46243 11.5376 3 8.5 3C5.46243 3 3 5.46243 3 8.5C3 11.5376 5.46243 14 8.5 14C9.83879 14 11.0659 13.5217 12.0196 12.7266L16.1464 16.8536L16.2157 16.9114C16.4106 17.0464 16.68 17.0271 16.8536 16.8536C17.0488 16.6583 17.0488 16.3417 16.8536 16.1464L12.7266 12.0196C13.5217 11.0659 14 9.83879 14 8.5ZM4 8.5C4 6.01472 6.01472 4 8.5 4C10.9853 4 13 6.01472 13 8.5C13 10.9853 10.9853 13 8.5 13C6.01472 13 4 10.9853 4 8.5Z",fill:"currentColor"}))),Xt=Q({name:"ResizeSmall",render(){return t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20"},t("g",{fill:"none"},t("path",{d:"M5.5 4A1.5 1.5 0 0 0 4 5.5v1a.5.5 0 0 1-1 0v-1A2.5 2.5 0 0 1 5.5 3h1a.5.5 0 0 1 0 1h-1zM16 5.5A1.5 1.5 0 0 0 14.5 4h-1a.5.5 0 0 1 0-1h1A2.5 2.5 0 0 1 17 5.5v1a.5.5 0 0 1-1 0v-1zm0 9a1.5 1.5 0 0 1-1.5 1.5h-1a.5.5 0 0 0 0 1h1a2.5 2.5 0 0 0 2.5-2.5v-1a.5.5 0 0 0-1 0v1zm-12 0A1.5 1.5 0 0 0 5.5 16h1.25a.5.5 0 0 1 0 1H5.5A2.5 2.5 0 0 1 3 14.5v-1.25a.5.5 0 0 1 1 0v1.25zM8.5 7A1.5 1.5 0 0 0 7 8.5v3A1.5 1.5 0 0 0 8.5 13h3a1.5 1.5 0 0 0 1.5-1.5v-3A1.5 1.5 0 0 0 11.5 7h-3zM8 8.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3z",fill:"currentColor"})))}}),ke=Object.assign(Object.assign({},ge.props),{onPreviewPrev:Function,onPreviewNext:Function,showToolbar:{type:Boolean,default:!0},showToolbarTooltip:Boolean}),Ae=Te("n-image");var Ve=globalThis&&globalThis.__awaiter||function(e,n,o,r){function a(d){return d instanceof o?d:new o(function(u){u(d)})}return new(o||(o=Promise))(function(d,u){function f(l){try{i(r.next(l))}catch(h){u(h)}}function s(l){try{i(r.throw(l))}catch(h){u(h)}}function i(l){l.done?d(l.value):a(l.value).then(f,s)}i((r=r.apply(e,n||[])).next())})};const Xe=e=>e.includes("image/"),Me=(e="")=>{const n=e.split("/"),r=n[n.length-1].split(/#|\?/)[0];return(/\.[^./\\]*$/.exec(r)||[""])[0]},Ue=/(webp|svg|png|gif|jpg|jpeg|jfif|bmp|dpg|ico)$/i,Ze=e=>{if(e.type)return Xe(e.type);const n=Me(e.name||"");if(Ue.test(n))return!0;const o=e.thumbnailUrl||e.url||"",r=Me(o);return!!(/^data:image\//.test(o)||Ue.test(r))};function Zt(e){return Ve(this,void 0,void 0,function*(){return yield new Promise(n=>{if(!e.type||!Xe(e.type)){n("");return}n(window.URL.createObjectURL(e))})})}const Wt=dt&&window.FileReader&&window.File;function Yt(e){return e.isDirectory}function qt(e){return e.isFile}function Gt(e,n){return Ve(this,void 0,void 0,function*(){const o=[];let r,a=0;function d(){a++}function u(){a--,a||r(o)}function f(s){s.forEach(i=>{if(i){if(d(),n&&Yt(i)){const l=i.createReader();d(),l.readEntries(h=>{f(h),u()},()=>{u()})}else qt(i)&&(d(),i.file(l=>{o.push({file:l,entry:i,source:"dnd"}),u()},()=>{u()}));u()}})}return yield new Promise(s=>{r=s,f(e)}),o})}function le(e){const{id:n,name:o,percentage:r,status:a,url:d,file:u,thumbnailUrl:f,type:s,fullPath:i,batchId:l}=e;return{id:n,name:o,percentage:r!=null?r:null,status:a,url:d!=null?d:null,file:u!=null?u:null,thumbnailUrl:f!=null?f:null,type:s!=null?s:null,fullPath:i!=null?i:null,batchId:l!=null?l:null}}function Kt(e,n,o){return e=e.toLowerCase(),n=n.toLocaleLowerCase(),o=o.toLocaleLowerCase(),o.split(",").map(a=>a.trim()).filter(Boolean).some(a=>{if(a.startsWith(".")){if(e.endsWith(a))return!0}else if(a.includes("/")){const[d,u]=n.split("/"),[f,s]=a.split("/");if((f==="*"||d&&f&&f===d)&&(s==="*"||u&&s&&s===u))return!0}else return!0;return!1})}const We=(e,n)=>{if(!e)return;const o=document.createElement("a");o.href=e,n!==void 0&&(o.download=n),document.body.appendChild(o),o.click(),document.body.removeChild(o)};function Jt(){return{toolbarIconColor:"rgba(255, 255, 255, .9)",toolbarColor:"rgba(0, 0, 0, .35)",toolbarBoxShadow:"none",toolbarBorderRadius:"24px"}}const Qt=$e({name:"Image",common:He,peers:{Tooltip:Tt},self:Jt}),eo=e=>{const{iconColor:n,primaryColor:o,errorColor:r,textColor2:a,successColor:d,opacityDisabled:u,actionColor:f,borderColor:s,hoverColor:i,lineHeight:l,borderRadius:h,fontSize:k}=e;return{fontSize:k,lineHeight:l,borderRadius:h,draggerColor:f,draggerBorder:`1px dashed ${s}`,draggerBorderHover:`1px dashed ${o}`,itemColorHover:i,itemColorHoverError:xt(r,{alpha:.06}),itemTextColor:a,itemTextColorError:r,itemTextColorSuccess:d,itemIconColor:n,itemDisabledOpacity:u,itemBorderImageCardError:`1px solid ${r}`,itemBorderImageCard:`1px solid ${s}`}},to=$e({name:"Upload",common:He,peers:{Button:Bt,Progress:mt},self:eo}),oo=to,no=t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M6 5C5.75454 5 5.55039 5.17688 5.50806 5.41012L5.5 5.5V14.5C5.5 14.7761 5.72386 15 6 15C6.24546 15 6.44961 14.8231 6.49194 14.5899L6.5 14.5V5.5C6.5 5.22386 6.27614 5 6 5ZM13.8536 5.14645C13.68 4.97288 13.4106 4.9536 13.2157 5.08859L13.1464 5.14645L8.64645 9.64645C8.47288 9.82001 8.4536 10.0894 8.58859 10.2843L8.64645 10.3536L13.1464 14.8536C13.3417 15.0488 13.6583 15.0488 13.8536 14.8536C14.0271 14.68 14.0464 14.4106 13.9114 14.2157L13.8536 14.1464L9.70711 10L13.8536 5.85355C14.0488 5.65829 14.0488 5.34171 13.8536 5.14645Z",fill:"currentColor"})),ro=t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M13.5 5C13.7455 5 13.9496 5.17688 13.9919 5.41012L14 5.5V14.5C14 14.7761 13.7761 15 13.5 15C13.2545 15 13.0504 14.8231 13.0081 14.5899L13 14.5V5.5C13 5.22386 13.2239 5 13.5 5ZM5.64645 5.14645C5.82001 4.97288 6.08944 4.9536 6.28431 5.08859L6.35355 5.14645L10.8536 9.64645C11.0271 9.82001 11.0464 10.0894 10.9114 10.2843L10.8536 10.3536L6.35355 14.8536C6.15829 15.0488 5.84171 15.0488 5.64645 14.8536C5.47288 14.68 5.4536 14.4106 5.58859 14.2157L5.64645 14.1464L9.79289 10L5.64645 5.85355C5.45118 5.65829 5.45118 5.34171 5.64645 5.14645Z",fill:"currentColor"})),io=t("svg",{viewBox:"0 0 20 20",fill:"none",xmlns:"http://www.w3.org/2000/svg"},t("path",{d:"M4.089 4.216l.057-.07a.5.5 0 0 1 .638-.057l.07.057L10 9.293l5.146-5.147a.5.5 0 0 1 .638-.057l.07.057a.5.5 0 0 1 .057.638l-.057.07L10.707 10l5.147 5.146a.5.5 0 0 1 .057.638l-.057.07a.5.5 0 0 1-.638.057l-.07-.057L10 10.707l-5.146 5.147a.5.5 0 0 1-.638.057l-.07-.057a.5.5 0 0 1-.057-.638l.057-.07L9.293 10L4.146 4.854a.5.5 0 0 1-.057-.638l.057-.07l-.057.07z",fill:"currentColor"})),lo=t("svg",{xmlns:"http://www.w3.org/2000/svg",width:"32",height:"32",viewBox:"0 0 1024 1024"},t("path",{fill:"currentColor",d:"M505.7 661a8 8 0 0 0 12.6 0l112-141.7c4.1-5.2.4-12.9-6.3-12.9h-74.1V168c0-4.4-3.6-8-8-8h-60c-4.4 0-8 3.6-8 8v338.3H400c-6.7 0-10.4 7.7-6.3 12.9l112 141.8zM878 626h-60c-4.4 0-8 3.6-8 8v154H214V634c0-4.4-3.6-8-8-8h-60c-4.4 0-8 3.6-8 8v198c0 17.7 14.3 32 32 32h684c17.7 0 32-14.3 32-32V634c0-4.4-3.6-8-8-8z"})),ao=U([U("body >",[w("image-container","position: fixed;")]),w("image-preview-container",`
 position: fixed;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 display: flex;
 `),w("image-preview-overlay",`
 z-index: -1;
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 background: rgba(0, 0, 0, .3);
 `,[ze()]),w("image-preview-toolbar",`
 z-index: 1;
 position: absolute;
 left: 50%;
 transform: translateX(-50%);
 border-radius: var(--n-toolbar-border-radius);
 height: 48px;
 bottom: 40px;
 padding: 0 12px;
 background: var(--n-toolbar-color);
 box-shadow: var(--n-toolbar-box-shadow);
 color: var(--n-toolbar-icon-color);
 transition: color .3s var(--n-bezier);
 display: flex;
 align-items: center;
 `,[w("base-icon",`
 padding: 0 8px;
 font-size: 28px;
 cursor: pointer;
 `),ze()]),w("image-preview-wrapper",`
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 display: flex;
 pointer-events: none;
 `,[Rt()]),w("image-preview",`
 user-select: none;
 -webkit-user-select: none;
 pointer-events: all;
 margin: auto;
 max-height: calc(100vh - 32px);
 max-width: calc(100vw - 32px);
 transition: transform .3s var(--n-bezier);
 `),w("image",`
 display: inline-flex;
 max-height: 100%;
 max-width: 100%;
 `,[yt("preview-disabled",`
 cursor: pointer;
 `),U("img",`
 border-radius: inherit;
 `)])]),ue=32,Ye=Q({name:"ImagePreview",props:Object.assign(Object.assign({},ke),{onNext:Function,onPrev:Function,clsPrefix:{type:String,required:!0}}),setup(e){const n=ge("Image","-image",ao,Qt,e,F(e,"clsPrefix"));let o=null;const r=H(null),a=H(null),d=H(void 0),u=H(!1),f=H(!1),{localeRef:s}=ut("Image");function i(){const{value:c}=a;if(!o||!c)return;const{style:p}=c,g=o.getBoundingClientRect(),T=g.left+g.width/2,L=g.top+g.height/2;p.transformOrigin=`${T}px ${L}px`}function l(c){var p,g;switch(c.key){case" ":c.preventDefault();break;case"ArrowLeft":(p=e.onPrev)===null||p===void 0||p.call(e);break;case"ArrowRight":(g=e.onNext)===null||g===void 0||g.call(e);break;case"Escape":Pe();break}}nt(u,c=>{c?Ce("keydown",document,l):se("keydown",document,l)}),je(()=>{se("keydown",document,l)});let h=0,k=0,b=0,I=0,X=0,j=0,V=0,J=0,P=!1;function M(c){const{clientX:p,clientY:g}=c;b=p-h,I=g-k,It(q)}function m(c){const{mouseUpClientX:p,mouseUpClientY:g,mouseDownClientX:T,mouseDownClientY:L}=c,A=T-p,Y=L-g,G=`vertical${Y>0?"Top":"Bottom"}`,te=`horizontal${A>0?"Left":"Right"}`;return{moveVerticalDirection:G,moveHorizontalDirection:te,deltaHorizontal:A,deltaVertical:Y}}function C(c){const{value:p}=r;if(!p)return{offsetX:0,offsetY:0};const g=p.getBoundingClientRect(),{moveVerticalDirection:T,moveHorizontalDirection:L,deltaHorizontal:A,deltaVertical:Y}=c||{};let G=0,te=0;return g.width<=window.innerWidth?G=0:g.left>0?G=(g.width-window.innerWidth)/2:g.right<window.innerWidth?G=-(g.width-window.innerWidth)/2:L==="horizontalRight"?G=Math.min((g.width-window.innerWidth)/2,X-(A!=null?A:0)):G=Math.max(-((g.width-window.innerWidth)/2),X-(A!=null?A:0)),g.height<=window.innerHeight?te=0:g.top>0?te=(g.height-window.innerHeight)/2:g.bottom<window.innerHeight?te=-(g.height-window.innerHeight)/2:T==="verticalBottom"?te=Math.min((g.height-window.innerHeight)/2,j-(Y!=null?Y:0)):te=Math.max(-((g.height-window.innerHeight)/2),j-(Y!=null?Y:0)),{offsetX:G,offsetY:te}}function y(c){se("mousemove",document,M),se("mouseup",document,y);const{clientX:p,clientY:g}=c;P=!1;const T=m({mouseUpClientX:p,mouseUpClientY:g,mouseDownClientX:V,mouseDownClientY:J}),L=C(T);b=L.offsetX,I=L.offsetY,q()}const B=oe(Ae,null);function v(c){var p,g;if((g=(p=B==null?void 0:B.previewedImgPropsRef.value)===null||p===void 0?void 0:p.onMousedown)===null||g===void 0||g.call(p,c),c.button!==0)return;const{clientX:T,clientY:L}=c;P=!0,h=T-b,k=L-I,X=b,j=I,V=T,J=L,q(),Ce("mousemove",document,M),Ce("mouseup",document,y)}function S(c){var p,g;(g=(p=B==null?void 0:B.previewedImgPropsRef.value)===null||p===void 0?void 0:p.onDblclick)===null||g===void 0||g.call(p,c);const T=ae();x=x===T?1:T,q()}const R=1.5;let E=0,x=1,D=0;function O(){x=1,E=0}function z(){var c;O(),D=0,(c=e.onPrev)===null||c===void 0||c.call(e)}function Z(){var c;O(),D=0,(c=e.onNext)===null||c===void 0||c.call(e)}function W(){D-=90,q()}function ne(){D+=90,q()}function ve(){const{value:c}=r;if(!c)return 1;const{innerWidth:p,innerHeight:g}=window,T=Math.max(1,c.naturalHeight/(g-ue)),L=Math.max(1,c.naturalWidth/(p-ue));return Math.max(3,T*2,L*2)}function ae(){const{value:c}=r;if(!c)return 1;const{innerWidth:p,innerHeight:g}=window,T=c.naturalHeight/(g-ue),L=c.naturalWidth/(p-ue);return T<1&&L<1?1:Math.max(T,L)}function me(){const c=ve();x<c&&(E+=1,x=Math.min(c,Math.pow(R,E)),q())}function pe(){if(x>.5){const c=x;E-=1,x=Math.max(.5,Math.pow(R,E));const p=c-x;q(!1);const g=C();x+=p,q(!1),x-=p,b=g.offsetX,I=g.offsetY,q()}}function we(){const c=d.value;c&&We(c,void 0)}function q(c=!0){var p;const{value:g}=r;if(!g)return;const{style:T}=g,L=it((p=B==null?void 0:B.previewedImgPropsRef.value)===null||p===void 0?void 0:p.style);let A="";if(typeof L=="string")A=L+";";else for(const G in L)A+=`${Mt(G)}: ${L[G]};`;const Y=`transform-origin: center; transform: translateX(${b}px) translateY(${I}px) rotate(${D}deg) scale(${x});`;P?T.cssText=A+"cursor: grabbing; transition: none;"+Y:T.cssText=A+"cursor: grab;"+Y+(c?"":"transition: none;"),c||g.offsetHeight}function Pe(){u.value=!u.value,f.value=!0}function Qe(){x=ae(),E=Math.ceil(Math.log(x)/Math.log(R)),b=0,I=0,q()}const et={setPreviewSrc:c=>{d.value=c},setThumbnailEl:c=>{o=c},toggleShow:Pe};function tt(c,p){if(e.showToolbarTooltip){const{value:g}=n;return t(Lt,{to:!1,theme:g.peers.Tooltip,themeOverrides:g.peerOverrides.Tooltip,keepAliveOnHover:!1},{default:()=>s.value[p],trigger:()=>c})}else return c}const Se=$(()=>{const{common:{cubicBezierEaseInOut:c},self:{toolbarIconColor:p,toolbarBorderRadius:g,toolbarBoxShadow:T,toolbarColor:L}}=n.value;return{"--n-bezier":c,"--n-toolbar-icon-color":p,"--n-toolbar-color":L,"--n-toolbar-border-radius":g,"--n-toolbar-box-shadow":T}}),{inlineThemeDisabled:Oe}=fe(),re=Oe?Ne("image-preview",void 0,Se,e):void 0;return Object.assign({previewRef:r,previewWrapperRef:a,previewSrc:d,show:u,appear:kt(),displayed:f,previewedImgProps:B==null?void 0:B.previewedImgPropsRef,handleWheel(c){c.preventDefault()},handlePreviewMousedown:v,handlePreviewDblclick:S,syncTransformOrigin:i,handleAfterLeave:()=>{O(),D=0,f.value=!1},handleDragStart:c=>{var p,g;(g=(p=B==null?void 0:B.previewedImgPropsRef.value)===null||p===void 0?void 0:p.onDragstart)===null||g===void 0||g.call(p,c),c.preventDefault()},zoomIn:me,zoomOut:pe,handleDownloadClick:we,rotateCounterclockwise:W,rotateClockwise:ne,handleSwitchPrev:z,handleSwitchNext:Z,withTooltip:tt,resizeToOrignalImageSize:Qe,cssVars:Oe?void 0:Se,themeClass:re==null?void 0:re.themeClass,onRender:re==null?void 0:re.onRender},et)},render(){var e,n;const{clsPrefix:o}=this;return t(xe,null,(n=(e=this.$slots).default)===null||n===void 0?void 0:n.call(e),t(Pt,{show:this.show},{default:()=>{var r;return this.show||this.displayed?((r=this.onRender)===null||r===void 0||r.call(this),Ie(t("div",{class:[`${o}-image-preview-container`,this.themeClass],style:this.cssVars,onWheel:this.handleWheel},t(be,{name:"fade-in-transition",appear:this.appear},{default:()=>this.show?t("div",{class:`${o}-image-preview-overlay`,onClick:this.toggleShow}):null}),this.showToolbar?t(be,{name:"fade-in-transition",appear:this.appear},{default:()=>{if(!this.show)return null;const{withTooltip:a}=this;return t("div",{class:`${o}-image-preview-toolbar`},this.onPrev?t(xe,null,a(t(_,{clsPrefix:o,onClick:this.handleSwitchPrev},{default:()=>no}),"tipPrevious"),a(t(_,{clsPrefix:o,onClick:this.handleSwitchNext},{default:()=>ro}),"tipNext")):null,a(t(_,{clsPrefix:o,onClick:this.rotateCounterclockwise},{default:()=>t(Nt,null)}),"tipCounterclockwise"),a(t(_,{clsPrefix:o,onClick:this.rotateClockwise},{default:()=>t(Ht,null)}),"tipClockwise"),a(t(_,{clsPrefix:o,onClick:this.resizeToOrignalImageSize},{default:()=>t(Xt,null)}),"tipOriginalSize"),a(t(_,{clsPrefix:o,onClick:this.zoomOut},{default:()=>t(Vt,null)}),"tipZoomOut"),a(t(_,{clsPrefix:o,onClick:this.zoomIn},{default:()=>t(At,null)}),"tipZoomIn"),a(t(_,{clsPrefix:o,onClick:this.handleDownloadClick},{default:()=>lo}),"tipDownload"),a(t(_,{clsPrefix:o,onClick:this.toggleShow},{default:()=>io}),"tipClose"))}}):null,t(be,{name:"fade-in-scale-up-transition",onAfterLeave:this.handleAfterLeave,appear:this.appear,onEnter:this.syncTransformOrigin,onBeforeLeave:this.syncTransformOrigin},{default:()=>{const{previewedImgProps:a={}}=this;return Ie(t("div",{class:`${o}-image-preview-wrapper`,ref:"previewWrapperRef"},t("img",Object.assign({},a,{draggable:!1,onMousedown:this.handlePreviewMousedown,onDblclick:this.handlePreviewDblclick,class:[`${o}-image-preview`,a.class],key:this.previewSrc,src:this.previewSrc,ref:"previewRef",onDragstart:this.handleDragStart}))),[[rt,this.show]])}})),[[St,{enabled:this.show}]])):null}}))}}),qe=Te("n-image-group"),so=ke,uo=Q({name:"ImageGroup",props:so,setup(e){let n;const{mergedClsPrefixRef:o}=fe(e),r=`c${Re()}`,a=lt(),d=s=>{var i;n=s,(i=f.value)===null||i===void 0||i.setPreviewSrc(s)};function u(s){var i,l;if(!(a!=null&&a.proxy))return;const k=a.proxy.$el.parentElement.querySelectorAll(`[data-group-id=${r}]:not([data-error=true])`);if(!k.length)return;const b=Array.from(k).findIndex(I=>I.dataset.previewSrc===n);~b?d(k[(b+s+k.length)%k.length].dataset.previewSrc):d(k[0].dataset.previewSrc),s===1?(i=e.onPreviewNext)===null||i===void 0||i.call(e):(l=e.onPreviewPrev)===null||l===void 0||l.call(e)}Le(qe,{mergedClsPrefixRef:o,setPreviewSrc:d,setThumbnailEl:s=>{var i;(i=f.value)===null||i===void 0||i.setThumbnailEl(s)},toggleShow:()=>{var s;(s=f.value)===null||s===void 0||s.toggleShow()},groupId:r});const f=H(null);return{mergedClsPrefix:o,previewInstRef:f,next:()=>{u(1)},prev:()=>{u(-1)}}},render(){return t(Ye,{theme:this.theme,themeOverrides:this.themeOverrides,clsPrefix:this.mergedClsPrefix,ref:"previewInstRef",onPrev:this.prev,onNext:this.next,showToolbar:this.showToolbar,showToolbarTooltip:this.showToolbarTooltip},this.$slots)}}),co=Object.assign({alt:String,height:[String,Number],imgProps:Object,previewedImgProps:Object,lazy:Boolean,intersectionObserverOptions:Object,objectFit:{type:String,default:"fill"},previewSrc:String,fallbackSrc:String,width:[String,Number],src:String,previewDisabled:Boolean,loadDescription:String,onError:Function,onLoad:Function},ke),fo=Q({name:"Image",props:co,inheritAttrs:!1,setup(e){const n=H(null),o=H(!1),r=H(null),a=oe(qe,null),{mergedClsPrefixRef:d}=a||fe(e),u={click:()=>{if(e.previewDisabled||o.value)return;const i=e.previewSrc||e.src;if(a){a.setPreviewSrc(i),a.setThumbnailEl(n.value),a.toggleShow();return}const{value:l}=r;l&&(l.setPreviewSrc(i),l.setThumbnailEl(n.value),l.toggleShow())}},f=H(!e.lazy);Be(()=>{var i;(i=n.value)===null||i===void 0||i.setAttribute("data-group-id",(a==null?void 0:a.groupId)||"")}),Be(()=>{if(e.lazy&&e.intersectionObserverOptions){let i;const l=ye(()=>{i==null||i(),i=void 0,i=bt(n.value,e.intersectionObserverOptions,f)});je(()=>{l(),i==null||i()})}}),ye(()=>{var i;e.src,(i=e.imgProps)===null||i===void 0||i.src,o.value=!1});const s=H(!1);return Le(Ae,{previewedImgPropsRef:F(e,"previewedImgProps")}),Object.assign({mergedClsPrefix:d,groupId:a==null?void 0:a.groupId,previewInstRef:r,imageRef:n,showError:o,shouldStartLoading:f,loaded:s,mergedOnClick:i=>{var l,h;u.click(),(h=(l=e.imgProps)===null||l===void 0?void 0:l.onClick)===null||h===void 0||h.call(l,i)},mergedOnError:i=>{if(!f.value)return;o.value=!0;const{onError:l,imgProps:{onError:h}={}}=e;l==null||l(i),h==null||h(i)},mergedOnLoad:i=>{const{onLoad:l,imgProps:{onLoad:h}={}}=e;l==null||l(i),h==null||h(i),s.value=!0}},u)},render(){var e,n;const{mergedClsPrefix:o,imgProps:r={},loaded:a,$attrs:d,lazy:u}=this,f=(n=(e=this.$slots).placeholder)===null||n===void 0?void 0:n.call(e),s=this.src||r.src,i=t("img",Object.assign(Object.assign({},r),{ref:"imageRef",width:this.width||r.width,height:this.height||r.height,src:this.showError?this.fallbackSrc:u&&this.intersectionObserverOptions?this.shouldStartLoading?s:void 0:s,alt:this.alt||r.alt,"aria-label":this.alt||r.alt,onClick:this.mergedOnClick,onError:this.mergedOnError,onLoad:this.mergedOnLoad,loading:Ct&&u&&!this.intersectionObserverOptions?"lazy":"eager",style:[r.style||"",f&&!a?{height:"0",width:"0",visibility:"hidden"}:"",{objectFit:this.objectFit}],"data-error":this.showError,"data-preview-src":this.previewSrc||this.src}));return t("div",Object.assign({},d,{role:"none",class:[d.class,`${o}-image`,(this.previewDisabled||this.showError)&&`${o}-image--preview-disabled`]}),this.groupId?i:t(Ye,{theme:this.theme,themeOverrides:this.themeOverrides,clsPrefix:o,ref:"previewInstRef",showToolbar:this.showToolbar,showToolbarTooltip:this.showToolbarTooltip},{default:()=>i}),!a&&f)}}),ie=Te("n-upload"),Ge="__UPLOAD_DRAGGER__",ho=Q({name:"UploadDragger",[Ge]:!0,setup(e,{slots:n}){const o=oe(ie,null);return o||he("upload-dragger","`n-upload-dragger` must be placed inside `n-upload`."),()=>{const{mergedClsPrefixRef:{value:r},mergedDisabledRef:{value:a},maxReachedRef:{value:d}}=o;return t("div",{class:[`${r}-upload-dragger`,(a||d)&&`${r}-upload-dragger--disabled`]},n)}}}),Ke=Q({name:"UploadTrigger",props:{abstract:Boolean},setup(e,{slots:n}){const o=oe(ie,null);o||he("upload-trigger","`n-upload-trigger` must be placed inside `n-upload`.");const{mergedClsPrefixRef:r,mergedDisabledRef:a,maxReachedRef:d,listTypeRef:u,dragOverRef:f,openOpenFileDialog:s,draggerInsideRef:i,handleFileAddition:l,mergedDirectoryDndRef:h,triggerStyleRef:k}=o,b=$(()=>u.value==="image-card");function I(){a.value||d.value||s()}function X(P){P.preventDefault(),f.value=!0}function j(P){P.preventDefault(),f.value=!0}function V(P){P.preventDefault(),f.value=!1}function J(P){var M;if(P.preventDefault(),!i.value||a.value||d.value){f.value=!1;return}const m=(M=P.dataTransfer)===null||M===void 0?void 0:M.items;m!=null&&m.length?Gt(Array.from(m).map(C=>C.webkitGetAsEntry()),h.value).then(C=>{l(C)}).finally(()=>{f.value=!1}):f.value=!1}return()=>{var P;const{value:M}=r;return e.abstract?(P=n.default)===null||P===void 0?void 0:P.call(n,{handleClick:I,handleDrop:J,handleDragOver:X,handleDragEnter:j,handleDragLeave:V}):t("div",{class:[`${M}-upload-trigger`,(a.value||d.value)&&`${M}-upload-trigger--disabled`,b.value&&`${M}-upload-trigger--image-card`],style:k.value,onClick:I,onDrop:J,onDragover:X,onDragenter:j,onDragleave:V},b.value?t(ho,null,{default:()=>ht(n.default,()=>[t(_,{clsPrefix:M},{default:()=>t(vt,null)})])}):n)}}}),go=Q({name:"UploadProgress",props:{show:Boolean,percentage:{type:Number,required:!0},status:{type:String,required:!0}},setup(){return{mergedTheme:oe(ie).mergedThemeRef}},render(){return t(Ee,null,{default:()=>this.show?t(pt,{type:"line",showIndicator:!1,percentage:this.percentage,status:this.status,height:2,theme:this.mergedTheme.peers.Progress,themeOverrides:this.mergedTheme.peerOverrides.Progress}):null})}}),vo=t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 28 28"},t("g",{fill:"none"},t("path",{d:"M21.75 3A3.25 3.25 0 0 1 25 6.25v15.5A3.25 3.25 0 0 1 21.75 25H6.25A3.25 3.25 0 0 1 3 21.75V6.25A3.25 3.25 0 0 1 6.25 3h15.5zm.583 20.4l-7.807-7.68a.75.75 0 0 0-.968-.07l-.084.07l-7.808 7.68c.183.065.38.1.584.1h15.5c.204 0 .4-.035.583-.1l-7.807-7.68l7.807 7.68zM21.75 4.5H6.25A1.75 1.75 0 0 0 4.5 6.25v15.5c0 .208.036.408.103.593l7.82-7.692a2.25 2.25 0 0 1 3.026-.117l.129.117l7.82 7.692c.066-.185.102-.385.102-.593V6.25a1.75 1.75 0 0 0-1.75-1.75zm-3.25 3a2.5 2.5 0 1 1 0 5a2.5 2.5 0 0 1 0-5zm0 1.5a1 1 0 1 0 0 2a1 1 0 0 0 0-2z",fill:"currentColor"}))),mo=t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 28 28"},t("g",{fill:"none"},t("path",{d:"M6.4 2A2.4 2.4 0 0 0 4 4.4v19.2A2.4 2.4 0 0 0 6.4 26h15.2a2.4 2.4 0 0 0 2.4-2.4V11.578c0-.729-.29-1.428-.805-1.944l-6.931-6.931A2.4 2.4 0 0 0 14.567 2H6.4zm-.9 2.4a.9.9 0 0 1 .9-.9H14V10a2 2 0 0 0 2 2h6.5v11.6a.9.9 0 0 1-.9.9H6.4a.9.9 0 0 1-.9-.9V4.4zm16.44 6.1H16a.5.5 0 0 1-.5-.5V4.06l6.44 6.44z",fill:"currentColor"})));var po=globalThis&&globalThis.__awaiter||function(e,n,o,r){function a(d){return d instanceof o?d:new o(function(u){u(d)})}return new(o||(o=Promise))(function(d,u){function f(l){try{i(r.next(l))}catch(h){u(h)}}function s(l){try{i(r.throw(l))}catch(h){u(h)}}function i(l){l.done?d(l.value):a(l.value).then(f,s)}i((r=r.apply(e,n||[])).next())})};const ce={paddingMedium:"0 3px",heightMedium:"24px",iconSizeMedium:"18px"},wo=Q({name:"UploadFile",props:{clsPrefix:{type:String,required:!0},file:{type:Object,required:!0},listType:{type:String,required:!0}},setup(e){const n=oe(ie),o=H(null),r=H(""),a=$(()=>{const{file:m}=e;return m.status==="finished"?"success":m.status==="error"?"error":"info"}),d=$(()=>{const{file:m}=e;if(m.status==="error")return"error"}),u=$(()=>{const{file:m}=e;return m.status==="uploading"}),f=$(()=>{if(!n.showCancelButtonRef.value)return!1;const{file:m}=e;return["uploading","pending","error"].includes(m.status)}),s=$(()=>{if(!n.showRemoveButtonRef.value)return!1;const{file:m}=e;return["finished"].includes(m.status)}),i=$(()=>{if(!n.showDownloadButtonRef.value)return!1;const{file:m}=e;return["finished"].includes(m.status)}),l=$(()=>{if(!n.showRetryButtonRef.value)return!1;const{file:m}=e;return["error"].includes(m.status)}),h=wt(()=>r.value||e.file.thumbnailUrl||e.file.url),k=$(()=>{if(!n.showPreviewButtonRef.value)return!1;const{file:{status:m},listType:C}=e;return["finished"].includes(m)&&h.value&&C==="image-card"});function b(){n.submit(e.file.id)}function I(m){m.preventDefault();const{file:C}=e;["finished","pending","error"].includes(C.status)?j(C):["uploading"].includes(C.status)?J(C):ot("upload","The button clicked type is unknown.")}function X(m){m.preventDefault(),V(e.file)}function j(m){const{xhrMap:C,doChange:y,onRemoveRef:{value:B},mergedFileListRef:{value:v}}=n;Promise.resolve(B?B({file:Object.assign({},m),fileList:v}):!0).then(S=>{if(S===!1)return;const R=Object.assign({},m,{status:"removed"});C.delete(m.id),y(R,void 0,{remove:!0})})}function V(m){const{onDownloadRef:{value:C}}=n;Promise.resolve(C?C(Object.assign({},m)):!0).then(y=>{y!==!1&&We(m.url,m.name)})}function J(m){const{xhrMap:C}=n,y=C.get(m.id);y==null||y.abort(),j(Object.assign({},m))}function P(){const{onPreviewRef:{value:m}}=n;if(m)m(e.file);else if(e.listType==="image-card"){const{value:C}=o;if(!C)return;C.click()}}const M=()=>po(this,void 0,void 0,function*(){const{listType:m}=e;m!=="image"&&m!=="image-card"||n.shouldUseThumbnailUrlRef.value(e.file)&&(r.value=yield n.getFileThumbnailUrlResolver(e.file))});return ye(()=>{M()}),{mergedTheme:n.mergedThemeRef,progressStatus:a,buttonType:d,showProgress:u,disabled:n.mergedDisabledRef,showCancelButton:f,showRemoveButton:s,showDownloadButton:i,showRetryButton:l,showPreviewButton:k,mergedThumbnailUrl:h,shouldUseThumbnailUrl:n.shouldUseThumbnailUrlRef,renderIcon:n.renderIconRef,imageRef:o,handleRemoveOrCancelClick:I,handleDownloadClick:X,handleRetryClick:b,handlePreviewClick:P}},render(){const{clsPrefix:e,mergedTheme:n,listType:o,file:r,renderIcon:a}=this;let d;const u=o==="image";u||o==="image-card"?d=!this.shouldUseThumbnailUrl(r)||!this.mergedThumbnailUrl?t("span",{class:`${e}-upload-file-info__thumbnail`},a?a(r):Ze(r)?t(_,{clsPrefix:e},{default:()=>vo}):t(_,{clsPrefix:e},{default:()=>mo})):t("a",{rel:"noopener noreferer",target:"_blank",href:r.url||void 0,class:`${e}-upload-file-info__thumbnail`,onClick:this.handlePreviewClick},o==="image-card"?t(fo,{src:this.mergedThumbnailUrl||void 0,previewSrc:r.url||void 0,alt:r.name,ref:"imageRef"}):t("img",{src:this.mergedThumbnailUrl||void 0,alt:r.name})):d=t("span",{class:`${e}-upload-file-info__thumbnail`},a?a(r):t(_,{clsPrefix:e},{default:()=>t(Ut,null)}));const s=t(go,{show:this.showProgress,percentage:r.percentage||0,status:this.progressStatus}),i=o==="text"||o==="image";return t("div",{class:[`${e}-upload-file`,`${e}-upload-file--${this.progressStatus}-status`,r.url&&r.status!=="error"&&o!=="image-card"&&`${e}-upload-file--with-url`,`${e}-upload-file--${o}-type`]},t("div",{class:`${e}-upload-file-info`},d,t("div",{class:`${e}-upload-file-info__name`},i&&(r.url&&r.status!=="error"?t("a",{rel:"noopener noreferer",target:"_blank",href:r.url||void 0,onClick:this.handlePreviewClick},r.name):t("span",{onClick:this.handlePreviewClick},r.name)),u&&s),t("div",{class:[`${e}-upload-file-info__action`,`${e}-upload-file-info__action--${o}-type`]},this.showPreviewButton?t(de,{key:"preview",quaternary:!0,type:this.buttonType,onClick:this.handlePreviewClick,theme:n.peers.Button,themeOverrides:n.peerOverrides.Button,builtinThemeOverrides:ce},{icon:()=>t(_,{clsPrefix:e},{default:()=>t(Dt,null)})}):null,(this.showRemoveButton||this.showCancelButton)&&!this.disabled&&t(de,{key:"cancelOrTrash",theme:n.peers.Button,themeOverrides:n.peerOverrides.Button,quaternary:!0,builtinThemeOverrides:ce,type:this.buttonType,onClick:this.handleRemoveOrCancelClick},{icon:()=>t(ct,null,{default:()=>this.showRemoveButton?t(_,{clsPrefix:e,key:"trash"},{default:()=>t(_t,null)}):t(_,{clsPrefix:e,key:"cancel"},{default:()=>t(Et,null)})})}),this.showRetryButton&&!this.disabled&&t(de,{key:"retry",quaternary:!0,type:this.buttonType,onClick:this.handleRetryClick,theme:n.peers.Button,themeOverrides:n.peerOverrides.Button,builtinThemeOverrides:ce},{icon:()=>t(_,{clsPrefix:e},{default:()=>t($t,null)})}),this.showDownloadButton?t(de,{key:"download",quaternary:!0,type:this.buttonType,onClick:this.handleDownloadClick,theme:n.peers.Button,themeOverrides:n.peerOverrides.Button,builtinThemeOverrides:ce},{icon:()=>t(_,{clsPrefix:e},{default:()=>t(jt,null)})}):null)),!u&&s)}}),bo=Q({name:"UploadFileList",setup(e,{slots:n}){const o=oe(ie,null);o||he("upload-file-list","`n-upload-file-list` must be placed inside `n-upload`.");const{abstractRef:r,mergedClsPrefixRef:a,listTypeRef:d,mergedFileListRef:u,fileListStyleRef:f,cssVarsRef:s,themeClassRef:i,maxReachedRef:l,showTriggerRef:h,imageGroupPropsRef:k}=o,b=$(()=>d.value==="image-card"),I=()=>u.value.map(j=>t(wo,{clsPrefix:a.value,key:j.id,file:j,listType:d.value})),X=()=>b.value?t(uo,Object.assign({},k.value),{default:I}):t(Ee,{group:!0},{default:I});return()=>{const{value:j}=a,{value:V}=r;return t("div",{class:[`${j}-upload-file-list`,b.value&&`${j}-upload-file-list--grid`,V?i==null?void 0:i.value:void 0],style:[V&&s?s.value:"",f.value]},X(),h.value&&!l.value&&b.value&&t(Ke,null,n))}}}),Co=U([w("upload","width: 100%;",[N("dragger-inside",[w("upload-trigger",`
 display: block;
 `)]),N("drag-over",[w("upload-dragger",`
 border: var(--n-dragger-border-hover);
 `)])]),w("upload-dragger",`
 cursor: pointer;
 box-sizing: border-box;
 width: 100%;
 text-align: center;
 border-radius: var(--n-border-radius);
 padding: 24px;
 opacity: 1;
 transition:
 opacity .3s var(--n-bezier),
 border-color .3s var(--n-bezier),
 background-color .3s var(--n-bezier);
 background-color: var(--n-dragger-color);
 border: var(--n-dragger-border);
 `,[U("&:hover",`
 border: var(--n-dragger-border-hover);
 `),N("disabled",`
 cursor: not-allowed;
 `)]),w("upload-trigger",`
 display: inline-block;
 box-sizing: border-box;
 opacity: 1;
 transition: opacity .3s var(--n-bezier);
 `,[U("+",[w("upload-file-list","margin-top: 8px;")]),N("disabled",`
 opacity: var(--n-item-disabled-opacity);
 cursor: not-allowed;
 `),N("image-card",`
 width: 96px;
 height: 96px;
 `,[w("base-icon",`
 font-size: 24px;
 `),w("upload-dragger",`
 padding: 0;
 height: 100%;
 width: 100%;
 display: flex;
 align-items: center;
 justify-content: center;
 `)])]),w("upload-file-list",`
 line-height: var(--n-line-height);
 opacity: 1;
 transition: opacity .3s var(--n-bezier);
 `,[U("a, img","outline: none;"),N("disabled",`
 opacity: var(--n-item-disabled-opacity);
 cursor: not-allowed;
 `,[w("upload-file","cursor: not-allowed;")]),N("grid",`
 display: grid;
 grid-template-columns: repeat(auto-fill, 96px);
 grid-gap: 8px;
 margin-top: 0;
 `),w("upload-file",`
 display: block;
 box-sizing: border-box;
 cursor: default;
 padding: 0px 12px 0 6px;
 transition: background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 `,[Fe(),w("progress",[Fe({foldPadding:!0})]),U("&:hover",`
 background-color: var(--n-item-color-hover);
 `,[w("upload-file-info",[K("action",`
 opacity: 1;
 `)])]),N("image-type",`
 border-radius: var(--n-border-radius);
 text-decoration: underline;
 text-decoration-color: #0000;
 `,[w("upload-file-info",`
 padding-top: 0px;
 padding-bottom: 0px;
 width: 100%;
 height: 100%;
 display: flex;
 justify-content: space-between;
 align-items: center;
 padding: 6px 0;
 `,[w("progress",`
 padding: 2px 0;
 margin-bottom: 0;
 `),K("name",`
 padding: 0 8px;
 `),K("thumbnail",`
 width: 32px;
 height: 32px;
 font-size: 28px;
 display: flex;
 justify-content: center;
 align-items: center;
 `,[U("img",`
 width: 100%;
 `)])])]),N("text-type",[w("progress",`
 box-sizing: border-box;
 padding-bottom: 6px;
 margin-bottom: 6px;
 `)]),N("image-card-type",`
 position: relative;
 width: 96px;
 height: 96px;
 border: var(--n-item-border-image-card);
 border-radius: var(--n-border-radius);
 padding: 0;
 display: flex;
 align-items: center;
 justify-content: center;
 transition: border-color .3s var(--n-bezier), background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 overflow: hidden;
 `,[w("progress",`
 position: absolute;
 left: 8px;
 bottom: 8px;
 right: 8px;
 width: unset;
 `),w("upload-file-info",`
 padding: 0;
 width: 100%;
 height: 100%;
 `,[K("thumbnail",`
 width: 100%;
 height: 100%;
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
 font-size: 36px;
 `,[U("img",`
 width: 100%;
 `)])]),U("&::before",`
 position: absolute;
 z-index: 1;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 border-radius: inherit;
 opacity: 0;
 transition: opacity .2s var(--n-bezier);
 content: "";
 `),U("&:hover",[U("&::before","opacity: 1;"),w("upload-file-info",[K("thumbnail","opacity: .12;")])])]),N("error-status",[U("&:hover",`
 background-color: var(--n-item-color-hover-error);
 `),w("upload-file-info",[K("name","color: var(--n-item-text-color-error);"),K("thumbnail","color: var(--n-item-text-color-error);")]),N("image-card-type",`
 border: var(--n-item-border-image-card-error);
 `)]),N("with-url",`
 cursor: pointer;
 `,[w("upload-file-info",[K("name",`
 color: var(--n-item-text-color-success);
 text-decoration-color: var(--n-item-text-color-success);
 `,[U("a",`
 text-decoration: underline;
 `)])])]),w("upload-file-info",`
 position: relative;
 padding-top: 6px;
 padding-bottom: 6px;
 display: flex;
 flex-wrap: nowrap;
 `,[K("thumbnail",`
 font-size: 18px;
 opacity: 1;
 transition: opacity .2s var(--n-bezier);
 color: var(--n-item-icon-color);
 `,[w("base-icon",`
 margin-right: 2px;
 vertical-align: middle;
 transition: color .3s var(--n-bezier);
 `)]),K("action",`
 padding-top: inherit;
 padding-bottom: inherit;
 position: absolute;
 right: 0;
 top: 0;
 bottom: 0;
 width: 80px;
 display: flex;
 align-items: center;
 transition: opacity .2s var(--n-bezier);
 justify-content: flex-end;
 opacity: 0;
 `,[w("button",[U("&:not(:last-child)",{marginRight:"4px"}),w("base-icon",[U("svg",[ft()])])]),N("image-type",`
 position: relative;
 max-width: 80px;
 width: auto;
 `),N("image-card-type",`
 z-index: 2;
 position: absolute;
 width: 100%;
 height: 100%;
 left: 0;
 right: 0;
 bottom: 0;
 top: 0;
 display: flex;
 justify-content: center;
 align-items: center;
 `)]),K("name",`
 color: var(--n-item-text-color);
 flex: 1;
 display: flex;
 justify-content: center;
 text-overflow: ellipsis;
 overflow: hidden;
 flex-direction: column;
 text-decoration-color: #0000;
 font-size: var(--n-font-size);
 transition:
 color .3s var(--n-bezier),
 text-decoration-color .3s var(--n-bezier); 
 `,[U("a",`
 color: inherit;
 text-decoration: underline;
 `)])])])]),w("upload-file-input",`
 display: block;
 width: 0;
 height: 0;
 opacity: 0;
 `)]);var _e=globalThis&&globalThis.__awaiter||function(e,n,o,r){function a(d){return d instanceof o?d:new o(function(u){u(d)})}return new(o||(o=Promise))(function(d,u){function f(l){try{i(r.next(l))}catch(h){u(h)}}function s(l){try{i(r.throw(l))}catch(h){u(h)}}function i(l){l.done?d(l.value):a(l.value).then(f,s)}i((r=r.apply(e,n||[])).next())})};function xo(e,n,o){const{doChange:r,xhrMap:a}=e;let d=0;function u(s){var i;let l=Object.assign({},n,{status:"error",percentage:d});a.delete(n.id),l=le(((i=e.onError)===null||i===void 0?void 0:i.call(e,{file:l,event:s}))||l),r(l,s)}function f(s){var i;if(e.isErrorState){if(e.isErrorState(o)){u(s);return}}else if(o.status<200||o.status>=300){u(s);return}let l=Object.assign({},n,{status:"finished",percentage:d});a.delete(n.id),l=le(((i=e.onFinish)===null||i===void 0?void 0:i.call(e,{file:l,event:s}))||l),r(l,s)}return{handleXHRLoad:f,handleXHRError:u,handleXHRAbort(s){const i=Object.assign({},n,{status:"removed",file:null,percentage:d});a.delete(n.id),r(i,s)},handleXHRProgress(s){const i=Object.assign({},n,{status:"uploading"});if(s.lengthComputable){const l=Math.ceil(s.loaded/s.total*100);i.percentage=l,d=l}r(i,s)}}}function yo(e){const{inst:n,file:o,data:r,headers:a,withCredentials:d,action:u,customRequest:f}=e,{doChange:s}=e.inst;let i=0;f({file:o,data:r,headers:a,withCredentials:d,action:u,onProgress(l){const h=Object.assign({},o,{status:"uploading"}),k=l.percent;h.percentage=k,i=k,s(h)},onFinish(){var l;let h=Object.assign({},o,{status:"finished",percentage:i});h=le(((l=n.onFinish)===null||l===void 0?void 0:l.call(n,{file:h}))||h),s(h)},onError(){var l;let h=Object.assign({},o,{status:"error",percentage:i});h=le(((l=n.onError)===null||l===void 0?void 0:l.call(n,{file:h}))||h),s(h)}})}function Ro(e,n,o){const r=xo(e,n,o);o.onabort=r.handleXHRAbort,o.onerror=r.handleXHRError,o.onload=r.handleXHRLoad,o.upload&&(o.upload.onprogress=r.handleXHRProgress)}function Je(e,n){return typeof e=="function"?e({file:n}):e||{}}function To(e,n,o){const r=Je(n,o);r&&Object.keys(r).forEach(a=>{e.setRequestHeader(a,r[a])})}function Lo(e,n,o){const r=Je(n,o);r&&Object.keys(r).forEach(a=>{e.append(a,r[a])})}function ko(e,n,o,{method:r,action:a,withCredentials:d,responseType:u,headers:f,data:s}){const i=new XMLHttpRequest;i.responseType=u,e.xhrMap.set(o.id,i),i.withCredentials=d;const l=new FormData;if(Lo(l,s,o),l.append(n,o.file),Ro(e,o,i),a!==void 0){i.open(r.toUpperCase(),a),To(i,f,o),i.send(l);const h=Object.assign({},o,{status:"uploading"});e.doChange(h)}}const Po=Object.assign(Object.assign({},ge.props),{name:{type:String,default:"file"},accept:String,action:String,customRequest:Function,directory:Boolean,directoryDnd:{type:Boolean,default:void 0},method:{type:String,default:"POST"},multiple:Boolean,showFileList:{type:Boolean,default:!0},data:[Object,Function],headers:[Object,Function],withCredentials:Boolean,responseType:{type:String,default:""},disabled:{type:Boolean,default:void 0},onChange:Function,onRemove:Function,onFinish:Function,onError:Function,onBeforeUpload:Function,isErrorState:Function,onDownload:Function,defaultUpload:{type:Boolean,default:!0},fileList:Array,"onUpdate:fileList":[Function,Array],onUpdateFileList:[Function,Array],fileListStyle:[String,Object],defaultFileList:{type:Array,default:()=>[]},showCancelButton:{type:Boolean,default:!0},showRemoveButton:{type:Boolean,default:!0},showDownloadButton:Boolean,showRetryButton:{type:Boolean,default:!0},showPreviewButton:{type:Boolean,default:!0},listType:{type:String,default:"text"},onPreview:Function,shouldUseThumbnailUrl:{type:Function,default:e=>Wt?Ze(e):!1},createThumbnailUrl:Function,abstract:Boolean,max:Number,showTrigger:{type:Boolean,default:!0},imageGroupProps:Object,inputProps:Object,triggerStyle:[String,Object],renderIcon:Function}),en=Q({name:"Upload",props:Po,setup(e){e.abstract&&e.listType==="image-card"&&he("upload","when the list-type is image-card, abstract is not supported.");const{mergedClsPrefixRef:n,inlineThemeDisabled:o}=fe(e),r=ge("Upload","-upload",Co,oo,e,n),a=zt(e),d=$(()=>{const{max:v}=e;return v!==void 0?b.value.length>=v:!1}),u=H(e.defaultFileList),f=F(e,"fileList"),s=H(null),i={value:!1},l=H(!1),h=new Map,k=gt(f,u),b=$(()=>k.value.map(le));function I(){var v;(v=s.value)===null||v===void 0||v.click()}function X(v){const S=v.target;J(S.files?Array.from(S.files).map(R=>({file:R,entry:null,source:"input"})):null,v),S.value=""}function j(v){const{"onUpdate:fileList":S,onUpdateFileList:R}=e;S&&De(S,v),R&&De(R,v),u.value=v}const V=$(()=>e.multiple||e.directory);function J(v,S){if(!v||v.length===0)return;const{onBeforeUpload:R}=e;v=V.value?v:[v[0]];const{max:E,accept:x}=e;v=v.filter(({file:O,source:z})=>z==="dnd"&&(x!=null&&x.trim())?Kt(O.name,O.type,x):!0),E&&(v=v.slice(0,E-b.value.length));const D=Re();Promise.all(v.map(({file:O,entry:z})=>_e(this,void 0,void 0,function*(){var Z;const W={id:Re(),batchId:D,name:O.name,status:"pending",percentage:0,file:O,url:null,type:O.type,thumbnailUrl:null,fullPath:(Z=z==null?void 0:z.fullPath)!==null&&Z!==void 0?Z:`/${O.webkitRelativePath||O.name}`};return!R||(yield R({file:W,fileList:b.value}))!==!1?W:null}))).then(O=>_e(this,void 0,void 0,function*(){let z=Promise.resolve();O.forEach(Z=>{z=z.then(st).then(()=>{Z&&M(Z,S,{append:!0})})}),yield z})).then(()=>{e.defaultUpload&&P()})}function P(v){const{method:S,action:R,withCredentials:E,headers:x,data:D,name:O}=e,z=v!==void 0?b.value.filter(W=>W.id===v):b.value,Z=v!==void 0;z.forEach(W=>{const{status:ne}=W;(ne==="pending"||ne==="error"&&Z)&&(e.customRequest?yo({inst:{doChange:M,xhrMap:h,onFinish:e.onFinish,onError:e.onError},file:W,action:R,withCredentials:E,headers:x,data:D,customRequest:e.customRequest}):ko({doChange:M,xhrMap:h,onFinish:e.onFinish,onError:e.onError,isErrorState:e.isErrorState},O,W,{method:S,action:R,withCredentials:E,responseType:e.responseType,headers:x,data:D}))})}const M=(v,S,R={append:!1,remove:!1})=>{const{append:E,remove:x}=R,D=Array.from(b.value),O=D.findIndex(z=>z.id===v.id);if(E||x||~O){E?D.push(v):x?D.splice(O,1):D.splice(O,1,v);const{onChange:z}=e;z&&z({file:v,fileList:D,event:S}),j(D)}};function m(v){var S;if(v.thumbnailUrl)return v.thumbnailUrl;const{createThumbnailUrl:R}=e;return R?(S=R(v.file,v))!==null&&S!==void 0?S:v.url||"":v.url?v.url:v.file?Zt(v.file):""}const C=$(()=>{const{common:{cubicBezierEaseInOut:v},self:{draggerColor:S,draggerBorder:R,draggerBorderHover:E,itemColorHover:x,itemColorHoverError:D,itemTextColorError:O,itemTextColorSuccess:z,itemTextColor:Z,itemIconColor:W,itemDisabledOpacity:ne,lineHeight:ve,borderRadius:ae,fontSize:me,itemBorderImageCardError:pe,itemBorderImageCard:we}}=r.value;return{"--n-bezier":v,"--n-border-radius":ae,"--n-dragger-border":R,"--n-dragger-border-hover":E,"--n-dragger-color":S,"--n-font-size":me,"--n-item-color-hover":x,"--n-item-color-hover-error":D,"--n-item-disabled-opacity":ne,"--n-item-icon-color":W,"--n-item-text-color":Z,"--n-item-text-color-error":O,"--n-item-text-color-success":z,"--n-line-height":ve,"--n-item-border-image-card-error":pe,"--n-item-border-image-card":we}}),y=o?Ne("upload",void 0,C,e):void 0;Le(ie,{mergedClsPrefixRef:n,mergedThemeRef:r,showCancelButtonRef:F(e,"showCancelButton"),showDownloadButtonRef:F(e,"showDownloadButton"),showRemoveButtonRef:F(e,"showRemoveButton"),showRetryButtonRef:F(e,"showRetryButton"),onRemoveRef:F(e,"onRemove"),onDownloadRef:F(e,"onDownload"),mergedFileListRef:b,triggerStyleRef:F(e,"triggerStyle"),shouldUseThumbnailUrlRef:F(e,"shouldUseThumbnailUrl"),renderIconRef:F(e,"renderIcon"),xhrMap:h,submit:P,doChange:M,showPreviewButtonRef:F(e,"showPreviewButton"),onPreviewRef:F(e,"onPreview"),getFileThumbnailUrlResolver:m,listTypeRef:F(e,"listType"),dragOverRef:l,openOpenFileDialog:I,draggerInsideRef:i,handleFileAddition:J,mergedDisabledRef:a.mergedDisabledRef,maxReachedRef:d,fileListStyleRef:F(e,"fileListStyle"),abstractRef:F(e,"abstract"),acceptRef:F(e,"accept"),cssVarsRef:o?void 0:C,themeClassRef:y==null?void 0:y.themeClass,onRender:y==null?void 0:y.onRender,showTriggerRef:F(e,"showTrigger"),imageGroupPropsRef:F(e,"imageGroupProps"),mergedDirectoryDndRef:$(()=>{var v;return(v=e.directoryDnd)!==null&&v!==void 0?v:e.directory})});const B={clear:()=>{u.value=[]},submit:P,openOpenFileDialog:I};return Object.assign({mergedClsPrefix:n,draggerInsideRef:i,inputElRef:s,mergedTheme:r,dragOver:l,mergedMultiple:V,cssVars:o?void 0:C,themeClass:y==null?void 0:y.themeClass,onRender:y==null?void 0:y.onRender,handleFileInputChange:X},B)},render(){var e,n;const{draggerInsideRef:o,mergedClsPrefix:r,$slots:a,directory:d,onRender:u}=this;if(a.default&&!this.abstract){const s=a.default()[0];!((e=s==null?void 0:s.type)===null||e===void 0)&&e[Ge]&&(o.value=!0)}const f=t("input",Object.assign({},this.inputProps,{ref:"inputElRef",type:"file",class:`${r}-upload-file-input`,accept:this.accept,multiple:this.mergedMultiple,onChange:this.handleFileInputChange,webkitdirectory:d||void 0,directory:d||void 0}));return this.abstract?t(xe,null,(n=a.default)===null||n===void 0?void 0:n.call(a),t(at,{to:"body"},f)):(u==null||u(),t("div",{class:[`${r}-upload`,o.value&&`${r}-upload--dragger-inside`,this.dragOver&&`${r}-upload--drag-over`,this.themeClass],style:this.cssVars},f,this.showTrigger&&this.listType!=="image-card"&&t(Ke,null,a),this.showFileList&&t(bo,null,a)))}});export{en as N};
