var e=Object.defineProperty;var D=(E,F,u)=>F in E?e(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var r=(E,F,u)=>(D(E,typeof F!="symbol"?F+"":F,u),u);var p=(E,F,u)=>new Promise((i,o)=>{var m=t=>{try{B(u.next(t))}catch(C){o(C)}},A=t=>{try{B(u.throw(t))}catch(C){o(C)}},B=t=>t.done?i(t.value):Promise.resolve(t.value).then(m,A);B((u=u.apply(E,F)).next())});import{b as s,a}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as n}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class N extends n{constructor(){super("GPE_QRCode");r(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u7981\u7528\uFF1A\u4E0D\u4F7F\u7528\u626B\u7801\u5F55\u5165\u3002
   - \u4E8C\u7EF4\u7801\uFF1A\u9002\u7528\u4E8E\u79FB\u52A8\u7AEF\uFF0C\u626B\u4E8C\u7EF4\u7801\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C\u3002
   - \u6761\u7801\uFF1A\u9002\u7528\u4E8E\u79FB\u52A8\u7AEF\uFF0C\u626B\u6761\u7801\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C\u3002
  #### \u573A\u666F
   - \u5229\u7528\u626B\u63CF\u67AA\u6216\u624B\u673A\u626B\u63CF\u529F\u80FD\uFF0C\u628A\u5E26\u6709\u8BC6\u522B\u7801\u7684\u7269\u54C1\u4FE1\u606F\u5F55\u5165\u5230\u7CFB\u7EDF\u4E2D\u3002
   - \u6BD4\u5982\uFF1A\u7EF4\u4FEE\u5DE5\u5177\u5F52\u8FD8\u6D41\u7A0B\u4E2D\uFF0C\u5F53\u5DE5\u5177\u5F52\u8FD8\u65F6\uFF0C\u53EF\u4EE5\u5229\u7528\u5916\u7F6E\u626B\u63CF\u67AA\uFF0C\u626B\u63CF\u5DE5\u5177\u4E0A\u7684\u8BC6\u522B\u7801\uFF08\u4E8C\u7EF4\u7801\u6216\u6761\u7801\uFF09\u3002
   - \u628A\u5DE5\u5177\u7684\u7F16\u53F7\uFF0C\u540D\u79F0\u7B49\u4FE1\u606F\u81EA\u52A8\u5F55\u5165\u5230\u6D41\u7A0B\u8868\u5355\u4E2D\u3002
   
  

  `);r(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u7528\u6237\u5411\u901A\u8FC7\u626B\u63CF\u4E8C\u7EF4\u7801\u4E4B\u540E\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C\u3002
   - \u6211\u4EEC\u628A\u8FD9\u4E2A\u573A\u666F\u53EB\u505A\u4E8C\u7EF4\u7801\uFF0C\u626B\u63CF\u3002
  `);r(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u9002\u7528\u4E8E\u79FB\u52A8\u7AEF\uFF0C\u626B\u6761\u7801\u83B7\u5F97\u4E00\u4E9B\u4FE1\u606F\uFF0C\u8FDB\u884C\u4E00\u4E9B\u64CD\u4F5C
  `);this.PageTitle="\u626B\u7801\u5F55\u5165"}Init(){return p(this,null,function*(){this.entity=new s,this.KeyOfEn=a.DoWay,yield this.entity.InitDataForMapAttr("QRCode",this.GetRequestVal("PKVal")),this.AddGroup("A","\u626B\u7801\u5F55\u5165"),this.Blank("0","\u7981\u7528",this.Desc0),this.Blank("1","\u4E8C\u7EF4\u7801",this.Desc1),this.Blank("2","\u6761\u7801",this.Desc2)})}AfterSave(u,i){if(u==i)throw new Error("Method not implemented.")}BtnClick(u,i,o){if(u==i||u===o)throw new Error("Method not implemented.")}}export{N as GPE_QRCode};
