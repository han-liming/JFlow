var D=Object.defineProperty;var i=(F,E,u)=>E in F?D(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var r=(F,E,u)=>(i(F,typeof E!="symbol"?E+"":E,u),u);import{FrmTransferCustom as B}from"./FrmTransferCustom-5720752b.js";import{PageBaseGroupEdit as m}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";import"./EntityNoName-d08126ae.js";import"./Node-6b42ba5e.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class M extends m{constructor(){super("GPE_FrmTransfer");r(this,"Desc0",` 
  #### \u5E2E\u52A9
  - \u6D41\u8F6C\u81EA\u5B9A\u4E49 \u5B9A\u4E49: \u5728\u6D41\u7A0B\u8FD0\u884C\u8FC7\u7A0B\u4E2D\uFF0C\u53EF\u4EE5\u5BF9\u8282\u70B9\u8FDB\u884C\u52A8\u6001\u7F16\u6392, \u6211\u4EEC\u628A\u8FD9\u6837\u7684\u884C\u4E3A\uFF0C\u79F0\u4E3A\u6D41\u7A0B\u81EA\u5B9A\u4E49.
  - \u6BD4\u5982\uFF1A\u4E00\u4E2A\u9879\u76EE\u7533\u62A5\u6709n\u4E2A\u5DE5\u5E8F, \u6574\u4F53\u6D41\u7A0B\u4E2D,\u5934\u90E8\u4E0E\u5C3E\u90E8\u8282\u70B9\u56FA\u5B9A\uFF0C\u6839\u636E\u9879\u76EE\u7C7B\u578B\u4E0D\u540C\uFF0C\u9700\u8981\u9009\u62E9\u4E0D\u540C\u7684\u5DE5\u5E8F. 
  - \u6E38\u79BB\u6001\u8282\u70B9\u5B9A\u4E49: \u4E00\u4E2A\u8282\u70B9\u662F\u4E00\u9053\u5DE5\u5E8F\uFF0C\u8FD9\u9053\u5DE5\u5E8F\u5728\u4E00\u4E2A\u6D41\u7A0B\u4E2D.

  #### \u6D41\u7A0B\u56FE
  - \u8FD9\u662F\u4E00\u4E2A\u9879\u76EE\u7533\u62A5\u6D41\u7A0B\uFF0C\u4E2D\u95F4\u7684\u5DE5\u5E8F\u8282\u70B9\u662F\u53EF\u4EE5\u88AB\u9009\u62E9\u7684, \u53EF\u4EE5\u52A8\u6001\u7EC4\u6001.
  -![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmTransfer/Img/FrmTransferFlow.png "\u5C4F\u5E55\u622A\u56FE.png")  

  #### \u6D41\u8F6C\u81EA\u5B9A\u4E49-(\u5DE5\u5E8F\u63A7\u5236\u56FE)

  - \u53EF\u4EE5\u4E3A\u6BCF\u4E2A\u5DE5\u5E8F\u8BBE\u7F6E\u64CD\u4F5C\u4EBA\u5458. 
  - \u53EF\u4EE5\u8C03\u6574\u5DE5\u5E8F\u987A\u5E8F.
  - \u53EF\u4EE5\u589E\u52A0\u6216\u8005\u51CF\u5C11\u5DE5\u5E8F(\u8282\u70B9). 
  
 #### \u5176\u4ED6\u8BF4\u660E
  - \u5DE5\u5E8F\u7684\u8282\u70B9\uFF0C\u90FD\u662F\u6E38\u79BB\u6001\u8282\u70B9.
  - \u6D41\u7A0B\u6D4B\u8BD5\u6848\u4F8B 027. \u5982\u4E0B\u56FE.
  -![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmTransfer/Img/FrmTransferFlow2.png "\u5C4F\u5E55\u622A\u56FE.png")  
  `);r(this,"Desc1",` 
  #### \u5E2E\u52A9
  - \u53EA\u8BFB\u72B6\u6001\u4E0B\uFF0C\u4EC5\u4EC5\u53EF\u4EE5\u67E5\u770B\u5DE5\u5E8F\uFF0C\u4E0D\u80FD\u5BF9\u5DE5\u5E8F\u8FDB\u884C\u7F16\u6392.
  `);r(this,"Desc2",` 
  #### \u5E2E\u52A9
  - \u53EF\u4EE5\u52A8\u6001\u7F16\u6392\u5DE5\u5E8F.
  - \u53EF\u4EE5\u8C03\u6574\u987A\u5E8F.
  - \u53EF\u4EE5\u8BBE\u7F6E\u5DE5\u5E8F\u4E0A\u7684\u64CD\u4F5C\u5458.
  #### \u7EC4\u4EF6\u5DE5\u4F5C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmTransfer/Img/FrmTransfer.png "\u5C4F\u5E55\u622A\u56FE.png")  
 
....`);this.PageTitle="\u6D41\u8F6C\u81EA\u5B9A\u4E49"}Init(){this.entity=new B,this.KeyOfEn="FTCSta",this.AddGroup("A","\u7EC4\u4EF6\u72B6\u6001"),this.Blank("0","\u7981\u7528",this.Desc0),this.Blank("1","\u53EA\u8BFB",this.Desc1),this.AddEntity("2","\u53EF\u5B9A\u4E49",new B,this.Desc2)}BtnClick(u,A,t){if(u==A||u===t)throw new Error("Method not implemented.")}AfterSave(u,A){}}export{M as GPE_FrmTransfer};
