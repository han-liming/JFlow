var e=Object.defineProperty;var i=(A,E,u)=>E in A?e(A,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):A[E]=u;var F=(A,E,u)=>(i(A,typeof E!="symbol"?E+"":E,u),u);import{PageBaseGroupEdit as B}from"./PageBaseGroupEdit-202e8e85.js";import{Node as o}from"./Node-6b42ba5e.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class G extends B{constructor(){super("GPE_OvertimeRole");F(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u8D85\u65F6\u7684\u65F6\u5019\u4E00\u76F4\u5904\u7406\u8D85\u65F6\u7684\u72B6\u6001\u3002

 `);F(this,"Desc1",`
  #### \u5E2E\u52A9
   
   - \u8D85\u65F6\u4E86\u5F53\u524D\u8282\u70B9\u81EA\u52A8\u8FD0\u52A8\u5230\u4E0B\u4E00\u4E2A\u73AF\u8282\uFF0C\u5982\u679C\u8981\u63A7\u5236\u7279\u5B9A\u7684\u6761\u4EF6\u4E0B\u4E0D\u5411\u4E0B\u8FD0\u52A8\uFF0C\u5C31\u9700\u8981\u5728\u5F53\u524D\u8282\u70B9\u7684\u53D1\u9001\u524D\u4E8B\u4EF6\u91CC\u7F16\u5199\u76F8\u5173\u7684\u4E1A\u52A1\u903B\u8F91\u3002
   - \u81EA\u52A8\u5411\u4E0B\u8FD0\u52A8\u9700\u8981\u660E\u786E\u4E0B\u4E00\u4E2A\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\uFF0C\u4E0E\u5230\u8FBE\u7684\u8282\u70B9\uFF0C\u6240\u4EE5\u4E00\u4E0B\u4E24\u79CD\u884C\u4E3A\u4E0D\u80FD\u81EA\u52A8\u5411\u4E0B\u8FD0\u52A8\u3002
   - \u5F53\u524D\u8282\u70B9\u7684\u65B9\u5411\u6761\u4EF6\u63A7\u5236\u89C4\u5219\u662F\u9009\u62E9\u7684\u6A21\u5F0F.
   - \u5230\u8FBE\u7684\u8282\u70B9\u7684\u63A5\u53D7\u4EBA\u89C4\u5219\u662F\u4E0A\u4E00\u4E2A\u8282\u70B9\u9009\u62E9\u7684.
   
 `);F(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u8D85\u65F6\u81EA\u52A8\u8DF3\u8F6C\u5230\u6307\u5B9A\u7684\u8282\u70B9\u3002

 
 `);F(this,"Desc3",`
  #### \u5E2E\u52A9
   - \u63A5\u53D7\u8F93\u5165\u7684\u5FC5\u987B\u662F\u4EBA\u5458\u7684\u5DE5\u4F5C\u5E10\u53F7\u3002
   - \u5982\u679C\u6709\u591A\u4E2A\u4EBA\u5143\u7528\u534A\u89D2\u7684\u9017\u53F7\u5206\u5F00\uFF0C\u6BD4\u5982: zhangsan,lisi\u3002
   - \u8D85\u65F6\u540E\u5C31\u81EA\u52A8\u7684\u628A\u5DE5\u4F5C\u79FB\u4EA4\u7ED9\u6307\u5B9A\u7684\u4EBA\u5458\u3002
  
  `);F(this,"Desc4",`
  #### \u5E2E\u52A9
   - \u63A5\u53D7\u8F93\u5165\u7684\u5FC5\u987B\u662F\u4EBA\u5458\u7684\u5DE5\u4F5C\u5E10\u53F7\u3002
   - \u5982\u679C\u6709\u591A\u4E2A\u4EBA\u5143\u7528\u534A\u89D2\u7684\u9017\u53F7\u5206\u5F00\uFF0C\u6BD4\u5982: zhangsan,lisi\u3002
   - \u8D85\u65F6\u540E\uFF0C\u7CFB\u7EDF\u5C31\u4F1A\u5411\u8FD9\u4E9B\u4EBA\u5458\u53D1\u9001\u6D88\u606F\u63D0\u9192\u3002
  
  `);F(this,"Desc5",`
  #### \u5E2E\u52A9
   - \u8D85\u65F6\u540E\u5C31\u81EA\u52A8\u5220\u9664\u5F53\u524D\u7684\u6D41\u7A0B\u3002
     
  `);F(this,"ExecSQL",`
  #### \u5E2E\u52A9
   - \u5F53\u524D\u7684\u7684sql\u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F.\u6BD4\u5982:@WebUser.No,@WebUser.Name,@WebUser.DeptNo,@WebUser.OrgNo
   - \u6267\u884CSQL,\u5904\u7406\u4E1A\u52A1\u903B\u8F91.
  `);this.PageTitle="\u8D85\u65F6\u5904\u7406\u89C4\u5219"}Init(){this.entity=new o,this.KeyOfEn="OutTimeDeal",this.AddGroup("A","\u8D85\u65F6\u5904\u7406\u89C4\u5219"),this.Blank("0","\u4E0D\u5904\u7406",this.Desc0),this.Blank("1","\u81EA\u52A8\u5411\u4E0B\u8FD0\u52A8",this.Desc1);const u="SELECT NodeID as No,CONCAT(NodeID,'-',Name) AS Name FROM WF_Node WHERE FK_Flow='@FK_Flow'";this.SingleDDLSQL("2","\u8DF3\u8F6C\u5230\u6307\u5B9A\u7684\u8282\u70B9","DoOutTime",this.Desc2,u,!1),this.SingleTB("3","\u79FB\u4EA4\u7ED9\u6307\u5B9A\u7684\u4EBA\u5458","DoOutTime",this.Desc3,"\u8F93\u5165\u4EBA\u5458\u7F16\u53F7"),this.SingleTB("4","\u7ED9\u6307\u5B9A\u7684\u4EBA\u5458\u53D1\u9001\u6D88\u606F.","DoOutTime",this.Desc4,"\u8F93\u5165\u4EBA\u5458\u7F16\u53F7"),this.Blank("5","\u5220\u9664\u6D41\u7A0B",this.Desc5),this.SingleTBSQL("6","\u6309SQL","DoOutTime",this.ExecSQL)}AfterSave(u,D){if(u==D)throw new Error("Method not implemented.")}BtnClick(u,D,t){if(u==D||u===t)throw new Error("Method not implemented.")}}export{G as GPE_OvertimeRole};
