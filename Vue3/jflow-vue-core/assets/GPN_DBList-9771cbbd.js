var I=Object.defineProperty;var d=(e,E,u)=>E in e?I(e,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):e[E]=u;var r=(e,E,u)=>(d(e,typeof E!="symbol"?E+"":E,u),u);var n=(e,E,u)=>new Promise((A,p)=>{var s=t=>{try{F(u.next(t))}catch(i){p(i)}},D=t=>{try{F(u.throw(t))}catch(i){p(i)}},F=t=>t.done?A(t.value):Promise.resolve(t.value).then(s,D);F((u=u.apply(e,E)).next())});import{Y as a}from"./index-f4658ae7.js";import{PageBaseGroupNew as T,GPNReturnObj as C,GPNReturnType as l}from"./PageBaseGroupNew-ee20c033.js";import{G as o}from"./DataType-33901a1c.js";import{MapData as B}from"./MapData-4fa397be.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./EnumLab-4f91f91c.js";import"./GloComm-7cfbdfd9.js";import"./FrmTrack-10f0746d.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";class K extends T{constructor(){super("GPN_DBList");r(this,"Docs0",`
  #### \u5E2E\u52A9
  - \u6240\u6709\u4EBA\u90FD\u53EF\u4EE5\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Anyone.png "\u5C4F\u5E55\u622A\u56FE.png") 
`);r(this,"Docs1",`
  #### \u5E2E\u52A9
  - \u53EA\u6709\u7BA1\u7406\u5458\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Admin.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs2",`
  #### \u5E2E\u52A9
  - \u7BA1\u7406\u5458\u548C\u4E8C\u7EA7\u7BA1\u7406\u5458\u6709\u6743\u9650\u3002
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/AdminerAndAmin2.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs3",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Emp.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs4",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u89D2\u8272\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Stations.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs5",`
  #### \u5E2E\u52A9
  - \u6309\u9009\u62E9\u7684\u90E8\u95E8\u4EBA\u5458\u8D4B\u6743\u3002
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Dept.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);r(this,"Docs6",`
  #### \u5E2E\u52A9
  - \u81EA\u52A8\u6284\u9001\u7ED9\u8981\u7ED1\u5B9A\u7684\u4EBA\u5458.1. \u8F93\u5165\u7684SQL\u662F\u4E00\u4E2A\u67E5\u8BE2\u8BED\u53E5\uFF0C\u8FD4\u56DE\u7684\u4E00\u884C\u7684\u7B2C\u4E00\u5217\u6570\u636E\u3002
  - \u8BE5\u6570\u636E\u5927\u4E8E0 \uFF0C\u5C31\u662F\u771F(\u53EF\u4EE5\u62E5\u6709\u6B64\u6743\u9650)\uFF0C\u5426\u5219\u5C31\u662F\u5047\uFF08\u4E0D\u80FD\u64CD\u4F5C\u6B64\u6743\u9650\uFF09\u3002
  - SQL\u8BED\u53E5\u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F\uFF0C\u6BD4\u5982\uFF1ASELECT count(*) FROM Port_Dept WHERE No='@WebUser.DeptNo'\u3002
  #### \u8BF4\u660E
  - @WebUser.No \u5F53\u524D\u767B\u5F55\u7684\u4EBA\u5458\u7F16\u53F7
  - @WebUser.DeptNo \u5F53\u524D\u767B\u5F55\u7684\u90E8\u95E8\u7F16\u53F7
  - @RDT \u662F\u5F53\u524D\u65E5\u671F\uFF0C \u6BD4\u5982\uFF1A2020-01-01
  - @DateTime \u662F\u5F53\u524D\u65F6\u95F4\uFF0C \u6BD4\u5982\uFF1A2020-01-01 10:09
  #### \u914D\u7F6E\u56FE
   ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/PCenter/Img/Sql.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);this.PageTitle="\u6570\u636E\u6E90\u5B9E\u4F53"}Init(){this.AddGroup("A","\u6570\u636E\u6E90\u5B9E\u4F53"),this.TextBox3_NameNoNote("FrmID","\u6309\u8868\u5355ID\u521B\u5EFA",this.HelpTodo,"DB_","\u8BF7\u8F93\u5165\u6570\u636E\u6E90ID","\u8F93\u5165\u6570\u636E\u6E90\u540D\u79F0","\u8BF7\u8F93\u5165\u8868\u5355ID","\u6211\u7684\u6570\u636E\u6E90"),this.SelectItemsByGroupList("FrmID.Table","\u4E3B\u8868\u5B57\u6BB5",this.HelpTodo,!0,o.srcFrmGroups,o.srcFrmFields),this.SelectItemsByGroupList("FrmID.Table.SelectDtl","\u9009\u62E9\u4ECE\u8868",this.HelpTodo,!0,o.srcFrmGroups,o.srcFrmFields),this.SelectItemsByGroupList("FrmID.Table.SelectDtl.Dtl","\u4ECE\u8868\u5B57\u6BB5",this.HelpTodo,!0,o.srcFrmGroups,o.srcFrmFields),this.TextBox2_NameNo("View","\u6309\u5173\u7CFB\u6570\u636E\u6E90\u521B\u5EFA",this.HelpTodo,"DBList","\u8F93\u5165ID","\u8F93\u5165\u540D\u79F0","\u8F93\u5165\u8868\u5355ID"),this.TextSQL("View.SQL","\u8F93\u5165\u67E5\u8BE2SQL",this.HelpTodo,"DBList","SELECT No as OID, No as BillNo,  Name as Title, Tel,Email  FROM Port_Emp","\u8BF7\u8F93\u5165SQL")}GenerSorts(){return n(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,A,p,s,D){return n(this,null,function*(){if(u=="View"){const F=new B;if(F.No=s,(yield F.IsExits())==!1)return a.info("ID:"+s+"\u5DF2\u7ECF\u5B58\u5728,\u8BF7\u91CD\u547D\u540D."),new C(l.CloseAndReload)}if(u=="FrmID"){const F=new B;if(F.No=s,(yield F.IsExits())==!1)return a.info("ID:"+s+"\u5DF2\u7ECF\u5B58\u5728,\u8BF7\u91CD\u547D\u540D."),new C(l.CloseAndReload);if(F.No=D,(yield F.IsExits())==!1)return a.info("\u8F93\u5165\u8868\u5355ID"+D+"\u4E0D\u5B58\u5728."),new C(l.CloseAndReload)}if(u=="FrmID.TableField.Dtl"){const F=this.RequestVal("tb1","FrmID"),t=this.RequestVal("tb2","FrmID"),i=this.RequestVal("tb3","FrmID");this.RequestVal("tb1","FrmID.TableField"),this.RequestVal("tb1","FrmID.TableField.Dtl");const c=new B;c.No=i,yield c.RetrieveFromDBSources();const m=new B;m.No=F,m.Name=t,m.PTable=F,yield m.Insert()}})}}export{K as GPN_DBList};
