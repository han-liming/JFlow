var p=Object.defineProperty;var F=(r,t,u)=>t in r?p(r,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):r[t]=u;var i=(r,t,u)=>(F(r,typeof t!="symbol"?t+"":t,u),u);import{FrmTrack as o}from"./FrmTrack-10f0746d.js";import{PageBaseGroupEdit as e}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./DBAccess-d3bef90d.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class O extends e{constructor(){super("GPE_FrmTrack");i(this,"Desc0",` 
  #### \u5E2E\u52A9
  - 

  #### \u6D41\u7A0B\u56FE
  - 

  
  `);i(this,"Desc1",` 
  #### \u5E2E\u52A9
  - \u53EA\u8BFB\u72B6\u6001\u4E0B\uFF0C\u4EC5\u4EC5\u53EF\u4EE5\u67E5\u770B\u5DE5\u5E8F\uFF0C\u4E0D\u80FD\u5BF9\u5DE5\u5E8F\u8FDB\u884C\u7F16\u6392.
  `);i(this,"Desc2",` 
  #### \u5E2E\u52A9
  - 
....`);this.PageTitle="\u8F68\u8FF9\u7EC4\u4EF6"}Init(){this.entity=new o,this.KeyOfEn="FrmTrackSta",this.AddGroup("A","\u7EC4\u4EF6\u72B6\u6001"),this.Blank("0","\u7981\u7528",this.Desc0),this.AddEntity("1","\u663E\u793A\u8F68\u8FF9\u56FE",new o,this.Desc2),this.AddEntity("2","\u663E\u793A\u8F68\u8FF9\u8868",new o,this.Desc2)}BtnClick(u,E,m){if(u==E||u===m)throw new Error("Method not implemented.")}AfterSave(u,E){}}export{O as GPE_FrmTrack};
