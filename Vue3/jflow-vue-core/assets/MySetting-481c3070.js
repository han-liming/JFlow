var m=Object.defineProperty;var p=(t,u,F)=>u in t?m(t,u,{enumerable:!0,configurable:!0,writable:!0,value:F}):t[u]=F;var C=(t,u,F)=>(p(t,typeof u!="symbol"?u+"":u,F),F);var l=(t,u,F)=>new Promise((e,E)=>{var n=A=>{try{B(F.next(A))}catch(o){E(o)}},D=A=>{try{B(F.throw(A))}catch(o){E(o)}},B=A=>A.done?e(A.value):Promise.resolve(A.value).then(n,D);B((F=F.apply(t,u)).next())});import{EntityNoName as c}from"./EntityNoName-d08126ae.js";import{UAC as g}from"./UAC-8e255d47.js";import{Map as S}from"./Map-73575e6b.js";import{Dept as h}from"./Dept-342c50de.js";import{Auths as f,AuthAttr as M}from"./Auth-a55f8f1f.js";import{RefMethod as s}from"./RefMethod-33a71db4.js";import y from"./BSEntity-840a884b.js";import{a1 as r}from"./index-f4658ae7.js";import{GPN_WorkShift as N}from"./GPN_WorkShift-d60b8d32.js";import{CCBPMRunModel as i}from"./SystemConfig-b93c25b3.js";import{PageBaseGroupEdit as P}from"./PageBaseGroupEdit-202e8e85.js";class d extends P{constructor(){super("GPE_MyFrmStyle");C(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u8868\u5355\u98CE\u683C\u6709\u4E24\u79CD\u98CE\u683C\uFF1A\u7ECF\u5178\u98CE\u683C,\u7B80\u6D01\u98CE\u683C;
   - \u7ECF\u5178\u98CE\u683C\u6709\u4E09\u79CD\u6A21\u5F0F\uFF1A1.\u9ED8\u8BA4\u6A21\u5F0F\uFF0C2.\u65F6\u5C1A\u6A21\u5F0F\uFF0C3.\u9ED1\u8272\u6A21\u5F0F\u3002
   - \u7B80\u6D01\u98CE\u683C\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1.\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\uFF0C2.\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
   - \u73B0\u5728\u91C7\u7528\u7684\u662F\u7ECF\u5178\u98CE\u683C\u7684\u9ED8\u8BA4\u6A21\u5F0F\u3002
   #### \u8FD0\u884C\u6548\u679C\u56FE
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FrmStyle.png "\u5C4F\u5E55\u622A\u56FE.png")
 `);C(this,"Desc1",`
 #### \u5E2E\u52A9
  - \u8868\u5355\u98CE\u683C\u6709\u4E24\u79CD\u98CE\u683C;
  - \u7ECF\u5178\u98CE\u683C\u6709\u4E09\u79CD\u6A21\u5F0F\uFF1A1.\u9ED8\u8BA4\u6A21\u5F0F\uFF0C2.\u65F6\u5C1A\u6A21\u5F0F\uFF0C3.\u9ED1\u8272\u6A21\u5F0F\u3002
  - \u7B80\u6D01\u98CE\u683C\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1.\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\uFF0C2.\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
  - \u73B0\u5728\u91C7\u7528\u7684\u662F\u7ECF\u5178\u98CE\u683C\u7684\u65F6\u5C1A\u6A21\u5F0F\u3002
  #### \u8FD0\u884C\u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FrmStyle1.png "\u5C4F\u5E55\u622A\u56FE.png")
`);C(this,"Desc2",`
#### \u5E2E\u52A9
 - \u8868\u5355\u98CE\u683C\u6709\u4E24\u79CD\u98CE\u683C;
 - \u7ECF\u5178\u98CE\u683C\u6709\u4E09\u79CD\u6A21\u5F0F\uFF1A1.\u9ED8\u8BA4\u6A21\u5F0F\uFF0C2.\u65F6\u5C1A\u6A21\u5F0F\uFF0C3.\u9ED1\u8272\u6A21\u5F0F\u3002
 - \u7B80\u6D01\u98CE\u683C\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1.\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\uFF0C2.\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
 - \u73B0\u5728\u91C7\u7528\u7684\u662F\u7ECF\u5178\u98CE\u683C\u7684\u9ED1\u8272\u6A21\u5F0F\u3002
 #### \u8FD0\u884C\u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FrmStyle2.png "\u5C4F\u5E55\u622A\u56FE.png")
`);C(this,"Desc3",`
#### \u5E2E\u52A9
 - \u8868\u5355\u98CE\u683C\u6709\u4E24\u79CD\u98CE\u683C;
 - \u7ECF\u5178\u98CE\u683C\u6709\u4E09\u79CD\u6A21\u5F0F\uFF1A1.\u9ED8\u8BA4\u6A21\u5F0F\uFF0C2.\u65F6\u5C1A\u6A21\u5F0F\uFF0C3.\u9ED1\u8272\u6A21\u5F0F\u3002
 - \u7B80\u6D01\u98CE\u683C\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1.\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\uFF0C2.\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
 - \u73B0\u5728\u91C7\u7528\u7684\u662F\u65F6\u5C1A\u98CE\u683C\u7684\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\u3002
 #### \u8FD0\u884C\u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FrmStyle3.png "\u5C4F\u5E55\u622A\u56FE.png")
`);C(this,"Desc4",`
#### \u5E2E\u52A9
 - \u8868\u5355\u98CE\u683C\u6709\u4E24\u79CD\u98CE\u683C;
 - \u7ECF\u5178\u98CE\u683C\u6709\u4E09\u79CD\u6A21\u5F0F\uFF1A1.\u9ED8\u8BA4\u6A21\u5F0F\uFF0C2.\u65F6\u5C1A\u6A21\u5F0F\uFF0C3.\u9ED1\u8272\u6A21\u5F0F\u3002
 - \u7B80\u6D01\u98CE\u683C\u6709\u4E24\u79CD\u6A21\u5F0F\uFF1A1.\u7B80\u6D01\u6E05\u6670\u6A21\u5F0F\uFF0C2.\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
 - \u73B0\u5728\u91C7\u7528\u7684\u662F\u7ECF\u5178\u98CE\u683C\u7684\u7B80\u6D01\u7D27\u51D1\u6A21\u5F0F\u3002
 #### \u8FD0\u884C\u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/FrmSln/Img/FrmStyle4.png "\u5C4F\u5E55\u622A\u56FE.png")
`);C(this,"Desc11",`

  #### \u8BF4\u660E

   - \u4EC5\u4EC5\u5BF9\u5F53\u524D\u8282\u70B9\u542F\u7528\u4E86\u5BA1\u6279\u7EC4\u4EF6(\u6216\u8005\u7B7E\u6279\u7EC4\u4EF6)\u6709\u6548.
   - \u5BA1\u6838\u7EC4\u4EF6\u7684\u4FE1\u606F\u4F1A\u8BB0\u5F55\u5230\u5BA1\u6838\u4FE1\u606F\u8868\u91CC\u9762.
   - \u901A\u8FC7\u8BBE\u7F6E\u6279\u91CF\u5BA1\u6279\u5C5E\u6027\u53EF\u4EE5\u7075\u6D3B\u7684\u6EE1\u8DB3\u4E0D\u540C\u7684\u5BA2\u6237\u9700\u6C42.
   
  #### \u8FD0\u884C\u6548\u679C\u56FE
  
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRole.png "\u5C4F\u5E55\u622A\u56FE.png")

  #### \u6D41\u7A0B\u6848\u4F8B\u56FE
  - \u51CF\u5211\u5047\u91CA\u6D41\u7A0B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow.png "\u5C4F\u5E55\u622A\u56FE.png")
  - \u6279\u6B21\u51CF\u5211\u6D41\u7A0B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow1.png "\u5C4F\u5E55\u622A\u56FE.png")

  
  #### \u914D\u7F6E\u8BF4\u660E

  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/BatchRole/Img/NodeBatchRoleFlow2.png "\u5C4F\u5E55\u622A\u56FE.png")

 `);C(this,"Desc21",`
  #### \u5E2E\u52A9
  - \u5BF9\u4E8E\u8282\u70B9\u8868\u5355\u6709\u6548.
  - \u5EFA\u8BAE\u4F7F\u7528\u5BA1\u6838\u7EC4\u4EF6.
  #### \u5176\u5B83 
  - \u8BE5\u529F\u80FD\u57282022.10\u4EE5\u540E\u7684\u7248\u672C\u53D6\u6D88\u4E86.
  `);this.PageTitle="\u8868\u5355\u98CE\u683C"}Init(){this.entity=new a,this.KeyOfEn="FrmStyle",this.AddGroup("A","\u7ECF\u5178\u98CE\u683C"),this.Blank("0","\u9ED8\u8BA4\u6A21\u5F0F",this.Desc0),this.Blank("1","\u65F6\u5C1A\u6A21\u5F0F",this.Desc1),this.Blank("2","\u9ED1\u8272\u6A21\u5F0F",this.Desc2),this.AddGroup("B","\u7B80\u6D01\u98CE\u683C"),this.Blank("3","\u6E05\u6670\u6A21\u5F0F",this.Desc3),this.Blank("4","\u7D27\u51D1\u6A21\u5F0F",this.Desc4)}AfterSave(F,e){if(F==e)throw new Error("Method not implemented.")}BtnClick(F,e,E){if(F==e||F===E)throw new Error("Method not implemented.")}}const K=Object.freeze(Object.defineProperty({__proto__:null,GPE_MyFrmStyle:d},Symbol.toStringTag,{value:"Module"}));class a extends c{constructor(u){if(super("TS.Port.MySetting"),u){if(r.CCBPMRunModel==i.SAAS&&u!="admin"){this.setPKVal(r.OrgNo+"_"+u),this.SetValByKey("UserID",u);return}this.setPKVal(u)}}get HisUAC(){const u=new g;return u.IsDelete=!1,u.IsUpdate=!0,u.IsInsert=!1,u}get EnMap(){const u=new S("WF_Emp","\u6211\u7684\u8BBE\u7F6E");u.AddGroupAttr("\u57FA\u672C\u4FE1\u606F"),u.AddTBStringPK("No",null,"\u8D26\u53F7",!0,!0,1,3,50),u.AddTBString("UserID",null,"UserID",!0,!1,0,50,200),u.AddTBString("Name",null,"\u540D\u79F0",!0,!1,0,50,200),u.AddDDLEntities("FK_Dept",null,"\u90E8\u95E8",new h,!1),u.AddTBString("Tel",null,"\u7535\u8BDD",!0,!1,0,50,200),u.AddTBString("Email",null,"\u90AE\u4EF6",!0,!1,0,50,200),u.AddTBInt("FrmStyle",0,"\u8868\u5355\u98CE\u683C",!1,!1),u.AddDDLStringEnum("SysLang","CH","\u7CFB\u7EDF\u8BED\u8A00","@CH=\u4E2D\u6587@En=\u82F1\u6587@FT=\u7E41\u4F53@JP=\u65E5\u6587",!0),u.AddRM_DtlSearch("\u6388\u6743",new f,M.Auther,"","","AutherToEmpNo,AutherToEmpName,FlowNoT","icon-drop",!1,"");const F=new s;F.Title="\u4FEE\u6539\u5BC6\u7801",F.ClassMethod="CPass",F.HisAttrs.AddTBString("p1",null,"\u539F\u5BC6\u7801",!0,!1,0,100,1e3,!0),F.HisAttrs.AddTBString("p2",null,"\u65B0\u5BC6\u7801",!0,!1,0,100,1e3,!0),F.HisAttrs.AddTBString("p3",null,"\u786E\u8BA4\u5BC6\u7801",!0,!1,0,100,1e3,!0),u.AddRefMethod(F);const e=new s;e.Title="\u8BBE\u7F6E\u5934\u50CF",e.ClassMethod="SetIcon",e.HisAttrs.AddTBImageUpload("f1","","\u66F4\u6362\u5934\u50CF",!0,!1),u.AddRefMethod(e);const E=new s;return E.Title="\u8BBE\u7F6E\u7B7E\u540D\uFF08\u7535\u5B50\u7B7E\u540D\u56FE\u7247\uFF09",E.ClassMethod="SetSigine",E.HisAttrs.AddTBImageUpload("f1","","\u4E0A\u4F20\u7B7E\u540D",!0,!1),u.AddRefMethod(E),u.AddRM_GPE(new d,"icon-drop"),u.AddRM_GPN(new N,"icon-login"),this._enMap=u,this._enMap}SetIcon(){return"\u672A\u5B9E\u73B0."}SetSigine(){return"\u672A\u5B9E\u73B0."}CPass(u,F,e){return l(this,null,function*(){if(!u||!F||!e)return"\u65B0\u5BC6\u7801\u6216\u65E7\u5BC6\u7801\u4E0D\u80FD\u4E3A\u7A7A,\u8BF7\u586B\u5199\u5B8C\u6574.";{let E=r.No;r.CCBPMRunModel==i.SAAS&&(E=r.OrgNo+"_"+r.No);const n=new y("BP.Port.Emp",E);return yield n.Retrieve(),yield n.DoMethodReturnString("ChangePass",u,F,e)}})}}const j=Object.freeze(Object.defineProperty({__proto__:null,MySetting:a},Symbol.toStringTag,{value:"Module"}));export{d as G,a as M,K as a,j as b};
