var a=Object.defineProperty;var B=(F,t,u)=>t in F?a(F,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[t]=u;var m=(F,t,u)=>(B(F,typeof t!="symbol"?t+"":t,u),u);var e=(F,t,u)=>new Promise((r,p)=>{var n=i=>{try{o(u.next(i))}catch(E){p(E)}},s=i=>{try{o(u.throw(i))}catch(E){p(E)}},o=i=>i.done?r(i.value):Promise.resolve(i.value).then(n,s);o((u=u.apply(F,t)).next())});import{b as A,a as C}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as D}from"./PageBaseGroupEdit-202e8e85.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class Q extends D{constructor(){super("GPE_FastInput");m(this,"Desc0",` 

  #### \u5E2E\u52A9
  - \u5FEB\u901F\u5F55\u5165\u662F\u4E3A\u4E86\u89E3\u51B3\u91CD\u590D\u6570\u636E\u76F8\u540C\u5185\u5BB9\u7684\u586B\u5199\u3002
  - \u80FD\u591F\u51CF\u8F7B\u8F93\u5165\u4EBA\u5458\u7684\u52B3\u52A8\uFF0C\u5E76\u5927\u5E45\u5EA6\u63D0\u9AD8\u4F7F\u7528\u4F53\u9A8C\u3002
  - \u5FEB\u901F\u5F55\u5165\u662F\u5BF9\u7528\u6237\u53EF\u80FD\u8981\u8F93\u5165\u7684\u5185\u5BB9\uFF0C\u9884\u5148\u5B58\u50A8\u5230\u6570\u636E\u5E93\u4E2D\uFF0C\u5728\u7528\u6237\u5F55\u5165\u7684\u65F6\u5019\u8FDB\u884C\u9009\u62E9\u3002
  - \u6BD4\u5982\uFF1A\u5BA1\u6838\u610F\u89C1\u3001 \u6CD5\u5F8B\u6CD5\u89C4\u3001\u8BF7\u5047\u539F\u56E0
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/FastInput/Img/FastInput.png "\u5C4F\u5E55\u622A\u56FE.png")

   `);m(this,"Desc1",` 
  #### \u5E2E\u52A9
   - \u5FEB\u901F\u8F93\u5165\u5185\u5BB9\uFF0C\u591A\u884C\u7528@\u5206\u5F00\u3002
  #### \u914D\u7F6E\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/FastInput/Img/FastInputPeizhi.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/FastInput/Img/FastInput.png "\u5C4F\u5E55\u622A\u56FE.png")

  
   `);this.PageTitle="\u5FEB\u901F\u5F55\u5165"}Init(){return e(this,null,function*(){this.entity=new A,this.KeyOfEn="DoWay",yield this.entity.InitDataForMapAttr("FastInput",this.GetRequestVal("PKVal"),"0"),this.AddGroup("A","\u5FEB\u901F\u5F55\u5165"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc0),this.SingleTextArea("1","\u542F\u7528\u5FEB\u901F\u5F55\u5165",C.Doc,"@\u540C\u610F",this.Desc1)})}AfterSave(u,r){if(u==r)throw new Error("Method not implemented.")}BtnClick(u,r,p){if(u==r||u===p)throw new Error("Method not implemented.")}}export{Q as GPE_FastInput};
