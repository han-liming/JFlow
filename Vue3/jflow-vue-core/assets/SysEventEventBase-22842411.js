var n=(i,u,r)=>new Promise((m,p)=>{var E=e=>{try{o(r.next(e))}catch(s){p(s)}},a=e=>{try{o(r.throw(e))}catch(s){p(s)}},o=e=>e.done?m(e.value):Promise.resolve(e.value).then(E,a);o((r=r.apply(i,u)).next())});import{UAC as B}from"./UAC-8e255d47.js";import{Map as l}from"./Map-73575e6b.js";import{EntityMyPK as A}from"./EntityMyPK-e742fec8.js";import{SysEventAttr as t}from"./SysEvent-83a4fc0b.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";class N extends A{constructor(u){super("TS.Sys.SysEventEventBase"),u&&(this.MyPK=u)}get HisUAC(){const u=new B;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new l("Sys_FrmEvent","\u4E8B\u4EF6\u57FA\u7C7B");u.AddMyPK(),u.AddTBString(t.RefPKVal,null,"\u5173\u952E\u503C",!1,!1,0,100,10),u.AddTBString(t.EventDoType,null,"\u6267\u884C\u6807\u8BB0",!1,!1,0,100,100),u.AddTBString(t.EventDoTypeT,null,"\u6267\u884C\u6807\u8BB0",!0,!0,0,100,100);const r=`
    #### \u5E2E\u52A9
    - \u8BF7\u586B\u5199SQL\u6216\u8005\u5B58\u50A8\u8FC7\u7A0B
    - \u652F\u6301ccbpm\u8868\u8FBE\u5F0F.
    #### SQL\u7684demo.
    - \u652F\u6301ccbpm\u8868\u8FBE\u5F0F.
    - UPDATE MyTable SET XXX='@QingJiaTianshu', DoWorkerID='@WebUser.No', 
        DoWorkerName='@WebUser.Name', DoWorkerDept='@WebUser.DeptNo',
       WHERE xxxx=@WorkID
    #### \u5B58\u50A8\u8FC7\u7A0BDEMO
    - @yln \u5B8C\u5584.
    - \u6267\u884C\u7684\u7ED3\u679C\u8FD4\u56DEstring\u7C7B\u578B\u7684\u6570\u636E\uFF0C\u5982\u679C
    `;return u.AddTBStringDoc(t.DoDoc,null,"\u4E8B\u4EF6\u57FA\u7C7B",!0,!1,!0,r),u.AddTBAtParas(4e3),this._enMap=u,this._enMap}beforeInsert(){return n(this,null,function*(){return Promise.resolve(!0)})}beforeUpdateInsertAction(){return n(this,null,function*(){return Promise.resolve(!0)})}}export{N as SysEventEventBase};
