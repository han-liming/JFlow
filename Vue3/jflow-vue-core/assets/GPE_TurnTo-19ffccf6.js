var D=Object.defineProperty;var i=(E,F,u)=>F in E?D(E,F,{enumerable:!0,configurable:!0,writable:!0,value:u}):E[F]=u;var A=(E,F,u)=>(i(E,typeof F!="symbol"?F+"":F,u),u);import{PageBaseGroupEdit as e}from"./PageBaseGroupEdit-202e8e85.js";import{Node as B,NodeAttr as t}from"./Node-6b42ba5e.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class k extends e{constructor(){super("GPE_TurnTo");A(this,"Desc0",`
  #### \u5E2E\u52A9
   - \u9ED8\u8BA4\u4E3A\u4E0D\u8BBE\u7F6E\uFF0C\u6309\u7167\u673A\u5668\u81EA\u52A8\u751F\u6210\u7684\u8BED\u8A00\u63D0\u793A\uFF0C\u8FD9\u662F\u6807\u51C6\u7684\u4FE1\u606F\u63D0\u793A\u3002
   - \u6BD4\u5982\uFF1A\u60A8\u7684\u5F53\u524D\u7684\u5DE5\u4F5C\u5DF2\u7ECF\u5904\u7406\u5B8C\u6210\u3002\u4E0B\u4E00\u6B65\u5DE5\u4F5C\u81EA\u52A8\u542F\u52A8\uFF0C\u5DF2\u7ECF\u63D0\u4EA4\u7ED9xxx\u5904\u7406\u3002 
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/TurnTo/Img/Turn.png "\u5C4F\u5E55\u622A\u56FE.png")
 `);A(this,"Desc1",`
  #### \u5E2E\u52A9
   - \u6309\u7167\u60A8\u5B9A\u4E49\u7684\u4FE1\u606F\u683C\u5F0F\uFF0C\u63D0\u793A\u7ED9\u5DF2\u7ECF\u64CD\u4F5C\u5B8C\u6210\u7684\u7528\u6237\u3002
   - \u6BD4\u5982\uFF1A\u60A8\u7684\u7533\u8BF7\u5DF2\u7ECF\u53D1\u9001\u81F3XXX \u8FDB\u884C\u5BA1\u6279\u3002 
   - \u8BE5\u81EA\u5B9A\u4E49\u4FE1\u606F\u652F\u6301ccbpm\u7684\u8868\u8FBE\u5F0F\uFF0C\u5177\u4F53\u53EF\u53C2\u8003\u53F3\u4FA7\u5E2E\u52A9\u6587\u6863\u3002
   - \u53D1\u9001\u540E\u7CFB\u7EDF\u53D8\u91CF\u5982\u4E0B:
   - \u60A8\u53EF\u4EE5\u8BBE\u7F6E\u4E3A: \u5F53\u524D\u5DE5\u4F5C\u63D0\u4EA4\u7ED9:\u3010 @VarAcceptersName \u3011\u5904\u7406\u3002
   - \u4F8B\u5982\uFF1A\u60A8\u7684\u8BF7\u5047\u7533\u8BF7\u5355\uFF0C\u5DF2\u7ECF\u63D0\u4EA4\u7ED9 @VarAcceptersName \uFF0C\u63D0\u4EA4\u5230\uFF1A @VarToNodeName , \u8BF7\u5047\u4E86@QingJiaTianTianShu\u5929\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/TurnTo/Img/TurnTo.png "\u5C4F\u5E55\u622A\u56FE.png")
`);A(this,"Desc2",`
  #### \u5E2E\u52A9
  
   - \u6309\u7167\u60A8\u5B9A\u4E49\u7684url\u8F6C\u5411\uFF0C\u53EF\u5904\u7406\u8F83\u4E3A\u590D\u6742\u7684\u4E1A\u52A1\u903B\u8F91\u5904\u7406\u3002
   - \u6BD4\u5982\uFF1AURL\u4E3AMyFlow.htm\u9875\u9762\u6216www.baidu.com\u3002
   - \u8BE5URL\u652F\u6301ccbpm\u53C2\u6570\u5F62\u5F0F\uFF0C\u5177\u4F53\u4F20\u503C\u53C2\u8003\u53F3\u4FA7\u5E2E\u52A9\u3002
   - \u542F\u52A8\u5B50\u6D41\u7A0B\u5B9E\u4F8B: /WF/MyFlow.htm?FK_Flow=003&PFlowNo=002
  `);A(this,"Desc3",`
  #### \u5E2E\u52A9
  - \u53D1\u9001\u6210\u529F\u540E\u76F4\u63A5\u5173\u95ED.
  `);A(this,"Desc4",`
  #### \u5E2E\u52A9
   - \u6309\u7167\u8BBE\u7F6E\u7684\u6761\u4EF6\u8F6C\u5411\u3002
   - \u8BE5\u529F\u80FD\u5C06\u8981\u53D6\u6D88.
  `);this.PageTitle="\u53D1\u9001\u540E\u8F6C\u5411"}Init(){this.entity=new B,this.Icon="icon-directions",this.KeyOfEn=t.TurnToDeal,this.AddGroup("A","+\u8F6C\u5411\u89C4\u5219"),this.Blank("0","\u63D0\u793Accflow\u9ED8\u8BA4\u4FE1\u606F",this.Desc0),this.SingleTB("1","\u63D0\u793A\u6307\u5B9A\u4FE1\u606F",t.TurnToDealDoc,this.Desc1,"\u8BF7\u8F93\u5165\u63D0\u793A\u4FE1\u606F"),this.SingleTB("2","\u8F6C\u5411\u6307\u5B9A\u7684URL",t.TurnToDealDoc,this.Desc2,"\u8BF7\u8F93\u5165\u8F6C\u5411URL"),this.Blank("3","\u53D1\u9001\u5B8C\u6BD5\u5C31\u5173\u95ED\uFF0C\u4E0D\u63D0\u793A\u4EFB\u4F55\u4FE1\u606F.",this.Desc3)}AfterSave(u,o){if(u==o)throw new Error("Method not implemented.")}BtnClick(u,o,r){if(u==o||u===r)throw new Error("Method not implemented.")}}export{k as GPE_TurnTo};
