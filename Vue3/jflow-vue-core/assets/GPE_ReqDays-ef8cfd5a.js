var n=Object.defineProperty;var A=(t,E,u)=>E in t?n(t,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[E]=u;var p=(t,E,u)=>(A(t,typeof E!="symbol"?E+"":E,u),u);var m=(t,E,u)=>new Promise((i,r)=>{var s=F=>{try{o(u.next(F))}catch(e){r(e)}},a=F=>{try{o(u.throw(F))}catch(e){r(e)}},o=F=>F.done?i(F.value):Promise.resolve(F.value).then(s,a);o((u=u.apply(t,E)).next())});import{b as B,a as D}from"./MapExt-db8cd7f3.js";import{PageBaseGroupEdit as C}from"./PageBaseGroupEdit-202e8e85.js";import{GPEReqDays as y}from"./GPEReqDays-9eba4850.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class U extends C{constructor(){super("GPE_ReqDays");p(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u5E94\u7528\u573A\u666F\uFF1A\u5728\u8BF7\u5047\u5355\u4E2D\uFF0C\u6839\u636E\u8BF7\u5047\u65E5\u671F\u4ECE\uFF0C\u8BF7\u5047\u65E5\u671F\u5230\u7684\u5DEE\u503C\uFF0C\u81EA\u52A8\u8BA1\u7B97\u8BF7\u5047\u5929\u6570\u3002
  #### \u914D\u7F6E\u56FE\u4F8B1
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysBiaodan.png "\u5C4F\u5E55\u622A\u56FE.png") 
  - \u914D\u7F6E\u56FE\u4F8B2
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysBiaodan2.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u8FD0\u884C\u56FE\u4F8B
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysYanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  `);p(this,"Desc2",`
  #### \u5E2E\u52A9
   - \u5BF9\u4E24\u4E2A\u65E5\u671F\u6C42\u5DEE\uFF0C\u65E5\u671F1-\u65E5\u671F2= \u5929\u6570.
   - \u5F97\u51FA\u7684\u503C\u662F\u4E00\u4E2A\u6574\u5F62\u7684\u6570\u503C\u3002
  #### \u5E94\u7528\u573A\u666F
   - \u6839\u636E\u8BF7\u5047\u65E5\u671F\u4ECE\uFF0C\u8BF7\u5047\u65E5\u671F\u5230\u81EA\u52A8\u8BA1\u7B97\u8BF7\u5047\u5929\u6570.
  #### \u6548\u679C\u56FE
  - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/FrmLogic/MapExt/ReqDays/Img/ReqDaysYanshi.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
  `);this.PageTitle="\u6C42\u4E24\u4E2A\u65E5\u671F\u4E4B\u5DEE"}Init(){return m(this,null,function*(){this.entity=new B,this.KeyOfEn=D.DoWay,yield this.entity.InitDataForMapAttr("ReqDays",this.GetRequestVal("PKVal")),this.AddGroup("A","\u6C42\u4E24\u4E2A\u65E5\u671F\u4E4B\u5DEE"),this.Blank("0","\u4E0D\u542F\u7528",this.Desc2),this.AddEntity("1","\u9009\u62E9\u65E5\u671F",new y,this.Desc1)})}AfterSave(u,i){if(u==i)throw new Error("Method not implemented.")}BtnClick(u,i,r){if(u==i||u===r)throw new Error("Method not implemented.")}}export{U as GPE_ReqDays};
