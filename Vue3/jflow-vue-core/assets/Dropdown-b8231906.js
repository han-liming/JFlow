import{p as ye,r as Se,k as Pe,N as Ne,a as ae}from"./Popover-ab55c8ff.js";import{r as D,O as se,d as $,a8 as a,k as H,f as b,p as W,bi as Re,aP as ue,F as Ie,dO as K}from"./index-f4658ae7.js";import{a as te,u as Ce}from"./use-config-816d55a6.js";import{p as ce,b as ke,a as Ke,V as Oe,m as ze,d as Ae}from"./Follower-3b5f0c65.js";import{u as V}from"./use-memo-f04d43e5.js";import{r as G}from"./render-ee8eb435.js";import{N as Fe}from"./Icon-fc5d8913.js";import{C as Te}from"./ChevronRight-3f42dbba.js";import{h as ie}from"./happens-in-d88e25de.js";import{w as Be}from"./warn-77f3ea30.js";import{X as _e}from"./Scrollbar-35d51129.js";import{j as De,k as He,o as $e,e as C,c as j,h as de,f as O,g as F,b as pe}from"./light-0dfdc1ad.js";import{f as Le}from"./fade-in-scale-up.cssr-0b26e361.js";import{u as Me,c as oe}from"./use-merged-state-66be05d7.js";import{c as je}from"./create-b75cc1a9.js";import{u as We}from"./use-keyboard-ebac156c.js";import{c as _}from"./create-key-bf4384d6.js";import{u as Ee}from"./use-css-vars-class-3ae3b4b3.js";function Ue(e){return o=>{o?e.value=o.$el:e.value=null}}function qe(e,o,i){if(!o)return e;const r=D(e.value);let t=null;return se(e,n=>{t!==null&&window.clearTimeout(t),n===!0?i&&!i.value?r.value=!0:t=window.setTimeout(()=>{r.value=!0},o):r.value=!1}),r}const Ve={padding:"4px 0",optionIconSizeSmall:"14px",optionIconSizeMedium:"16px",optionIconSizeLarge:"16px",optionIconSizeHuge:"18px",optionSuffixWidthSmall:"14px",optionSuffixWidthMedium:"14px",optionSuffixWidthLarge:"16px",optionSuffixWidthHuge:"16px",optionIconSuffixWidthSmall:"32px",optionIconSuffixWidthMedium:"32px",optionIconSuffixWidthLarge:"36px",optionIconSuffixWidthHuge:"36px",optionPrefixWidthSmall:"14px",optionPrefixWidthMedium:"14px",optionPrefixWidthLarge:"16px",optionPrefixWidthHuge:"16px",optionIconPrefixWidthSmall:"36px",optionIconPrefixWidthMedium:"36px",optionIconPrefixWidthLarge:"40px",optionIconPrefixWidthHuge:"40px"},Ge=e=>{const{primaryColor:o,textColor2:i,dividerColor:r,hoverColor:t,popoverColor:n,invertedColor:p,borderRadius:f,fontSizeSmall:c,fontSizeMedium:y,fontSizeLarge:g,fontSizeHuge:S,heightSmall:N,heightMedium:P,heightLarge:R,heightHuge:z,textColor3:w,opacityDisabled:I}=e;return Object.assign(Object.assign({},Ve),{optionHeightSmall:N,optionHeightMedium:P,optionHeightLarge:R,optionHeightHuge:z,borderRadius:f,fontSizeSmall:c,fontSizeMedium:y,fontSizeLarge:g,fontSizeHuge:S,optionTextColor:i,optionTextColorHover:i,optionTextColorActive:o,optionTextColorChildActive:o,color:n,dividerColor:r,suffixColor:i,prefixColor:i,optionColorHover:t,optionColorActive:$e(o,{alpha:.1}),groupHeaderTextColor:w,optionTextColorInverted:"#BBB",optionTextColorHoverInverted:"#FFF",optionTextColorActiveInverted:"#FFF",optionTextColorChildActiveInverted:"#FFF",colorInverted:p,dividerColorInverted:"#BBB",suffixColorInverted:"#BBB",prefixColorInverted:"#BBB",optionColorHoverInverted:o,optionColorActiveInverted:o,groupHeaderTextColorInverted:"#AAA",optionOpacityDisabled:I})},Xe=De({name:"Dropdown",common:He,peers:{Popover:ye},self:Ge}),Je=Xe,fe=$({name:"DropdownDivider",props:{clsPrefix:{type:String,required:!0}},render(){return a("div",{class:`${this.clsPrefix}-dropdown-divider`})}}),re=te("n-dropdown-menu"),X=te("n-dropdown"),le=te("n-dropdown-option");function ne(e,o){return e.type==="submenu"||e.type===void 0&&e[o]!==void 0}function Qe(e){return e.type==="group"}function he(e){return e.type==="divider"}function Ye(e){return e.type==="render"}const ve=$({name:"DropdownOption",props:{clsPrefix:{type:String,required:!0},tmNode:{type:Object,required:!0},parentKey:{type:[String,Number],default:null},placement:{type:String,default:"right-start"},props:Object,scrollable:Boolean},setup(e){const o=H(X),{hoverKeyRef:i,keyboardKeyRef:r,lastToggledSubmenuKeyRef:t,pendingKeyPathRef:n,activeKeyPathRef:p,animatedRef:f,mergedShowRef:c,renderLabelRef:y,renderIconRef:g,labelFieldRef:S,childrenFieldRef:N,renderOptionRef:P,nodePropsRef:R,menuPropsRef:z}=o,w=H(le,null),I=H(re),E=H(ce),J=b(()=>e.tmNode.rawNode),U=b(()=>{const{value:l}=N;return ne(e.tmNode.rawNode,l)}),Q=b(()=>{const{disabled:l}=e.tmNode;return l}),Y=b(()=>{if(!U.value)return!1;const{key:l,disabled:m}=e.tmNode;if(m)return!1;const{value:k}=i,{value:T}=r,{value:ee}=t,{value:B}=n;return k!==null?B.includes(l):T!==null?B.includes(l)&&B[B.length-1]!==l:ee!==null?B.includes(l):!1}),Z=b(()=>r.value===null&&!f.value),q=qe(Y,300,Z),L=b(()=>!!(w!=null&&w.enteringSubmenuRef.value)),M=D(!1);W(le,{enteringSubmenuRef:M});function A(){M.value=!0}function d(){M.value=!1}function v(){const{parentKey:l,tmNode:m}=e;m.disabled||c.value&&(t.value=l,r.value=null,i.value=m.key)}function u(){const{tmNode:l}=e;l.disabled||c.value&&i.value!==l.key&&v()}function s(l){if(e.tmNode.disabled||!c.value)return;const{relatedTarget:m}=l;m&&!ie({target:m},"dropdownOption")&&!ie({target:m},"scrollbarRail")&&(i.value=null)}function x(){const{value:l}=U,{tmNode:m}=e;c.value&&!l&&!m.disabled&&(o.doSelect(m.key,m.rawNode),o.doUpdateShow(!1))}return{labelField:S,renderLabel:y,renderIcon:g,siblingHasIcon:I.showIconRef,siblingHasSubmenu:I.hasSubmenuRef,menuProps:z,popoverBody:E,animated:f,mergedShowSubmenu:b(()=>q.value&&!L.value),rawNode:J,hasSubmenu:U,pending:V(()=>{const{value:l}=n,{key:m}=e.tmNode;return l.includes(m)}),childActive:V(()=>{const{value:l}=p,{key:m}=e.tmNode,k=l.findIndex(T=>m===T);return k===-1?!1:k<l.length-1}),active:V(()=>{const{value:l}=p,{key:m}=e.tmNode,k=l.findIndex(T=>m===T);return k===-1?!1:k===l.length-1}),mergedDisabled:Q,renderOption:P,nodeProps:R,handleClick:x,handleMouseMove:u,handleMouseEnter:v,handleMouseLeave:s,handleSubmenuBeforeEnter:A,handleSubmenuAfterEnter:d}},render(){var e,o;const{animated:i,rawNode:r,mergedShowSubmenu:t,clsPrefix:n,siblingHasIcon:p,siblingHasSubmenu:f,renderLabel:c,renderIcon:y,renderOption:g,nodeProps:S,props:N,scrollable:P}=this;let R=null;if(t){const E=(e=this.menuProps)===null||e===void 0?void 0:e.call(this,r,r.children);R=a(me,Object.assign({},E,{clsPrefix:n,scrollable:this.scrollable,tmNodes:this.tmNode.children,parentKey:this.tmNode.key}))}const z={class:[`${n}-dropdown-option-body`,this.pending&&`${n}-dropdown-option-body--pending`,this.active&&`${n}-dropdown-option-body--active`,this.childActive&&`${n}-dropdown-option-body--child-active`,this.mergedDisabled&&`${n}-dropdown-option-body--disabled`],onMousemove:this.handleMouseMove,onMouseenter:this.handleMouseEnter,onMouseleave:this.handleMouseLeave,onClick:this.handleClick},w=S==null?void 0:S(r),I=a("div",Object.assign({class:[`${n}-dropdown-option`,w==null?void 0:w.class],"data-dropdown-option":!0},w),a("div",ue(z,N),[a("div",{class:[`${n}-dropdown-option-body__prefix`,p&&`${n}-dropdown-option-body__prefix--show-icon`]},[y?y(r):G(r.icon)]),a("div",{"data-dropdown-option":!0,class:`${n}-dropdown-option-body__label`},c?c(r):G((o=r[this.labelField])!==null&&o!==void 0?o:r.title)),a("div",{"data-dropdown-option":!0,class:[`${n}-dropdown-option-body__suffix`,f&&`${n}-dropdown-option-body__suffix--has-submenu`]},this.hasSubmenu?a(Fe,null,{default:()=>a(Te,null)}):null)]),this.hasSubmenu?a(ke,null,{default:()=>[a(Ke,null,{default:()=>a("div",{class:`${n}-dropdown-offset-container`},a(Oe,{show:this.mergedShowSubmenu,placement:this.placement,to:P&&this.popoverBody||void 0,teleportDisabled:!P},{default:()=>a("div",{class:`${n}-dropdown-menu-wrapper`},i?a(Re,{onBeforeEnter:this.handleSubmenuBeforeEnter,onAfterEnter:this.handleSubmenuAfterEnter,name:"fade-in-scale-up-transition",appear:!0},{default:()=>R}):R)}))})]}):null);return g?g({node:I,option:r}):I}}),Ze=$({name:"DropdownGroupHeader",props:{clsPrefix:{type:String,required:!0},tmNode:{type:Object,required:!0}},setup(){const{showIconRef:e,hasSubmenuRef:o}=H(re),{renderLabelRef:i,labelFieldRef:r,nodePropsRef:t,renderOptionRef:n}=H(X);return{labelField:r,showIcon:e,hasSubmenu:o,renderLabel:i,nodeProps:t,renderOption:n}},render(){var e;const{clsPrefix:o,hasSubmenu:i,showIcon:r,nodeProps:t,renderLabel:n,renderOption:p}=this,{rawNode:f}=this.tmNode,c=a("div",Object.assign({class:`${o}-dropdown-option`},t==null?void 0:t(f)),a("div",{class:`${o}-dropdown-option-body ${o}-dropdown-option-body--group`},a("div",{"data-dropdown-option":!0,class:[`${o}-dropdown-option-body__prefix`,r&&`${o}-dropdown-option-body__prefix--show-icon`]},G(f.icon)),a("div",{class:`${o}-dropdown-option-body__label`,"data-dropdown-option":!0},n?n(f):G((e=f.title)!==null&&e!==void 0?e:f[this.labelField])),a("div",{class:[`${o}-dropdown-option-body__suffix`,i&&`${o}-dropdown-option-body__suffix--has-submenu`],"data-dropdown-option":!0})));return p?p({node:c,option:f}):c}}),eo=$({name:"NDropdownGroup",props:{clsPrefix:{type:String,required:!0},tmNode:{type:Object,required:!0},parentKey:{type:[String,Number],default:null}},render(){const{tmNode:e,parentKey:o,clsPrefix:i}=this,{children:r}=e;return a(Ie,null,a(Ze,{clsPrefix:i,tmNode:e,key:e.key}),r==null?void 0:r.map(t=>{const{rawNode:n}=t;return n.show===!1?null:he(n)?a(fe,{clsPrefix:i,key:t.key}):t.isGroup?(Be("dropdown","`group` node is not allowed to be put in `group` node."),null):a(ve,{clsPrefix:i,tmNode:t,parentKey:o,key:t.key})}))}}),oo=$({name:"DropdownRenderOption",props:{tmNode:{type:Object,required:!0}},render(){const{rawNode:{render:e,props:o}}=this.tmNode;return a("div",o,[e==null?void 0:e()])}}),me=$({name:"DropdownMenu",props:{scrollable:Boolean,showArrow:Boolean,arrowStyle:[String,Object],clsPrefix:{type:String,required:!0},tmNodes:{type:Array,default:()=>[]},parentKey:{type:[String,Number],default:null}},setup(e){const{renderIconRef:o,childrenFieldRef:i}=H(X);W(re,{showIconRef:b(()=>{const t=o.value;return e.tmNodes.some(n=>{var p;if(n.isGroup)return(p=n.children)===null||p===void 0?void 0:p.some(({rawNode:c})=>t?t(c):c.icon);const{rawNode:f}=n;return t?t(f):f.icon})}),hasSubmenuRef:b(()=>{const{value:t}=i;return e.tmNodes.some(n=>{var p;if(n.isGroup)return(p=n.children)===null||p===void 0?void 0:p.some(({rawNode:c})=>ne(c,t));const{rawNode:f}=n;return ne(f,t)})})});const r=D(null);return W(ze,null),W(Ae,null),W(ce,r),{bodyRef:r}},render(){const{parentKey:e,clsPrefix:o,scrollable:i}=this,r=this.tmNodes.map(t=>{const{rawNode:n}=t;return n.show===!1?null:Ye(n)?a(oo,{tmNode:t,key:t.key}):he(n)?a(fe,{clsPrefix:o,key:t.key}):Qe(n)?a(eo,{clsPrefix:o,tmNode:t,parentKey:e,key:t.key}):a(ve,{clsPrefix:o,tmNode:t,parentKey:e,key:t.key,props:n.props,scrollable:i})});return a("div",{class:[`${o}-dropdown-menu`,i&&`${o}-dropdown-menu--scrollable`],ref:"bodyRef"},i?a(_e,{contentClass:`${o}-dropdown-menu__content`},{default:()=>r}):r,this.showArrow?Se({clsPrefix:o,arrowStyle:this.arrowStyle}):null)}}),no=C("dropdown-menu",`
 transform-origin: var(--v-transform-origin);
 background-color: var(--n-color);
 border-radius: var(--n-border-radius);
 box-shadow: var(--n-box-shadow);
 position: relative;
 transition:
 background-color .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier);
`,[Le(),C("dropdown-option",`
 position: relative;
 `,[j("a",`
 text-decoration: none;
 color: inherit;
 outline: none;
 `,[j("&::before",`
 content: "";
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 `)]),C("dropdown-option-body",`
 display: flex;
 cursor: pointer;
 position: relative;
 height: var(--n-option-height);
 line-height: var(--n-option-height);
 font-size: var(--n-font-size);
 color: var(--n-option-text-color);
 transition: color .3s var(--n-bezier);
 `,[j("&::before",`
 content: "";
 position: absolute;
 top: 0;
 bottom: 0;
 left: 4px;
 right: 4px;
 transition: background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 `),de("disabled",[O("pending",`
 color: var(--n-option-text-color-hover);
 `,[F("prefix, suffix",`
 color: var(--n-option-text-color-hover);
 `),j("&::before","background-color: var(--n-option-color-hover);")]),O("active",`
 color: var(--n-option-text-color-active);
 `,[F("prefix, suffix",`
 color: var(--n-option-text-color-active);
 `),j("&::before","background-color: var(--n-option-color-active);")]),O("child-active",`
 color: var(--n-option-text-color-child-active);
 `,[F("prefix, suffix",`
 color: var(--n-option-text-color-child-active);
 `)])]),O("disabled",`
 cursor: not-allowed;
 opacity: var(--n-option-opacity-disabled);
 `),O("group",`
 font-size: calc(var(--n-font-size) - 1px);
 color: var(--n-group-header-text-color);
 `,[F("prefix",`
 width: calc(var(--n-option-prefix-width) / 2);
 `,[O("show-icon",`
 width: calc(var(--n-option-icon-prefix-width) / 2);
 `)])]),F("prefix",`
 width: var(--n-option-prefix-width);
 display: flex;
 justify-content: center;
 align-items: center;
 color: var(--n-prefix-color);
 transition: color .3s var(--n-bezier);
 z-index: 1;
 `,[O("show-icon",`
 width: var(--n-option-icon-prefix-width);
 `),C("icon",`
 font-size: var(--n-option-icon-size);
 `)]),F("label",`
 white-space: nowrap;
 flex: 1;
 z-index: 1;
 `),F("suffix",`
 box-sizing: border-box;
 flex-grow: 0;
 flex-shrink: 0;
 display: flex;
 justify-content: flex-end;
 align-items: center;
 min-width: var(--n-option-suffix-width);
 padding: 0 8px;
 transition: color .3s var(--n-bezier);
 color: var(--n-suffix-color);
 z-index: 1;
 `,[O("has-submenu",`
 width: var(--n-option-icon-suffix-width);
 `),C("icon",`
 font-size: var(--n-option-icon-size);
 `)]),C("dropdown-menu","pointer-events: all;")]),C("dropdown-offset-container",`
 pointer-events: none;
 position: absolute;
 left: 0;
 right: 0;
 top: -4px;
 bottom: -4px;
 `)]),C("dropdown-divider",`
 transition: background-color .3s var(--n-bezier);
 background-color: var(--n-divider-color);
 height: 1px;
 margin: 4px 0;
 `),C("dropdown-menu-wrapper",`
 transform-origin: var(--v-transform-origin);
 width: fit-content;
 `),j(">",[C("scrollbar",`
 height: inherit;
 max-height: inherit;
 `)]),de("scrollable",`
 padding: var(--n-padding);
 `),O("scrollable",[F("content",`
 padding: var(--n-padding);
 `)])]),to={animated:{type:Boolean,default:!0},keyboard:{type:Boolean,default:!0},size:{type:String,default:"medium"},inverted:Boolean,placement:{type:String,default:"bottom"},onSelect:[Function,Array],options:{type:Array,default:()=>[]},menuProps:Function,showArrow:Boolean,renderLabel:Function,renderIcon:Function,renderOption:Function,nodeProps:Function,labelField:{type:String,default:"label"},keyField:{type:String,default:"key"},childrenField:{type:String,default:"children"},value:[String,Number]},ro=Object.keys(ae),io=Object.assign(Object.assign(Object.assign({},ae),to),pe.props),Ro=$({name:"Dropdown",inheritAttrs:!1,props:io,setup(e){const o=D(!1),i=Me(K(e,"show"),o),r=b(()=>{const{keyField:d,childrenField:v}=e;return je(e.options,{getKey(u){return u[d]},getDisabled(u){return u.disabled===!0},getIgnored(u){return u.type==="divider"||u.type==="render"},getChildren(u){return u[v]}})}),t=b(()=>r.value.treeNodes),n=D(null),p=D(null),f=D(null),c=b(()=>{var d,v,u;return(u=(v=(d=n.value)!==null&&d!==void 0?d:p.value)!==null&&v!==void 0?v:f.value)!==null&&u!==void 0?u:null}),y=b(()=>r.value.getPath(c.value).keyPath),g=b(()=>r.value.getPath(e.value).keyPath),S=V(()=>e.keyboard&&i.value);We({keydown:{ArrowUp:{prevent:!0,handler:Q},ArrowRight:{prevent:!0,handler:U},ArrowDown:{prevent:!0,handler:Y},ArrowLeft:{prevent:!0,handler:J},Enter:{prevent:!0,handler:Z},Escape:E}},S);const{mergedClsPrefixRef:N,inlineThemeDisabled:P}=Ce(e),R=pe("Dropdown","-dropdown",no,Je,e,N);W(X,{labelFieldRef:K(e,"labelField"),childrenFieldRef:K(e,"childrenField"),renderLabelRef:K(e,"renderLabel"),renderIconRef:K(e,"renderIcon"),hoverKeyRef:n,keyboardKeyRef:p,lastToggledSubmenuKeyRef:f,pendingKeyPathRef:y,activeKeyPathRef:g,animatedRef:K(e,"animated"),mergedShowRef:i,nodePropsRef:K(e,"nodeProps"),renderOptionRef:K(e,"renderOption"),menuPropsRef:K(e,"menuProps"),doSelect:z,doUpdateShow:w}),se(i,d=>{!e.animated&&!d&&I()});function z(d,v){const{onSelect:u}=e;u&&oe(u,d,v)}function w(d){const{"onUpdate:show":v,onUpdateShow:u}=e;v&&oe(v,d),u&&oe(u,d),o.value=d}function I(){n.value=null,p.value=null,f.value=null}function E(){w(!1)}function J(){L("left")}function U(){L("right")}function Q(){L("up")}function Y(){L("down")}function Z(){const d=q();d!=null&&d.isLeaf&&i.value&&(z(d.key,d.rawNode),w(!1))}function q(){var d;const{value:v}=r,{value:u}=c;return!v||u===null?null:(d=v.getNode(u))!==null&&d!==void 0?d:null}function L(d){const{value:v}=c,{value:{getFirstAvailableNode:u}}=r;let s=null;if(v===null){const x=u();x!==null&&(s=x.key)}else{const x=q();if(x){let l;switch(d){case"down":l=x.getNext();break;case"up":l=x.getPrev();break;case"right":l=x.getChild();break;case"left":l=x.getParent();break}l&&(s=l.key)}}s!==null&&(n.value=null,p.value=s)}const M=b(()=>{const{size:d,inverted:v}=e,{common:{cubicBezierEaseInOut:u},self:s}=R.value,{padding:x,dividerColor:l,borderRadius:m,optionOpacityDisabled:k,[_("optionIconSuffixWidth",d)]:T,[_("optionSuffixWidth",d)]:ee,[_("optionIconPrefixWidth",d)]:B,[_("optionPrefixWidth",d)]:be,[_("fontSize",d)]:we,[_("optionHeight",d)]:ge,[_("optionIconSize",d)]:xe}=s,h={"--n-bezier":u,"--n-font-size":we,"--n-padding":x,"--n-border-radius":m,"--n-option-height":ge,"--n-option-prefix-width":be,"--n-option-icon-prefix-width":B,"--n-option-suffix-width":ee,"--n-option-icon-suffix-width":T,"--n-option-icon-size":xe,"--n-divider-color":l,"--n-option-opacity-disabled":k};return v?(h["--n-color"]=s.colorInverted,h["--n-option-color-hover"]=s.optionColorHoverInverted,h["--n-option-color-active"]=s.optionColorActiveInverted,h["--n-option-text-color"]=s.optionTextColorInverted,h["--n-option-text-color-hover"]=s.optionTextColorHoverInverted,h["--n-option-text-color-active"]=s.optionTextColorActiveInverted,h["--n-option-text-color-child-active"]=s.optionTextColorChildActiveInverted,h["--n-prefix-color"]=s.prefixColorInverted,h["--n-suffix-color"]=s.suffixColorInverted,h["--n-group-header-text-color"]=s.groupHeaderTextColorInverted):(h["--n-color"]=s.color,h["--n-option-color-hover"]=s.optionColorHover,h["--n-option-color-active"]=s.optionColorActive,h["--n-option-text-color"]=s.optionTextColor,h["--n-option-text-color-hover"]=s.optionTextColorHover,h["--n-option-text-color-active"]=s.optionTextColorActive,h["--n-option-text-color-child-active"]=s.optionTextColorChildActive,h["--n-prefix-color"]=s.prefixColor,h["--n-suffix-color"]=s.suffixColor,h["--n-group-header-text-color"]=s.groupHeaderTextColor),h}),A=P?Ee("dropdown",b(()=>`${e.size[0]}${e.inverted?"i":""}`),M,e):void 0;return{mergedClsPrefix:N,mergedTheme:R,tmNodes:t,mergedShow:i,handleAfterLeave:()=>{e.animated&&I()},doUpdateShow:w,cssVars:P?void 0:M,themeClass:A==null?void 0:A.themeClass,onRender:A==null?void 0:A.onRender}},render(){const e=(r,t,n,p,f)=>{var c;const{mergedClsPrefix:y,menuProps:g}=this;(c=this.onRender)===null||c===void 0||c.call(this);const S=(g==null?void 0:g(void 0,this.tmNodes.map(P=>P.rawNode)))||{},N={ref:Ue(t),class:[r,`${y}-dropdown`,this.themeClass],clsPrefix:y,tmNodes:this.tmNodes,style:[n,this.cssVars],showArrow:this.showArrow,arrowStyle:this.arrowStyle,scrollable:this.scrollable,onMouseenter:p,onMouseleave:f};return a(me,ue(this.$attrs,N,S))},{mergedTheme:o}=this,i={show:this.mergedShow,theme:o.peers.Popover,themeOverrides:o.peerOverrides.Popover,internalOnAfterLeave:this.handleAfterLeave,internalRenderBody:e,onUpdateShow:this.doUpdateShow,"onUpdate:show":void 0};return a(Ne,Object.assign({},Pe(this.$props,ro),i),{trigger:()=>{var r,t;return(t=(r=this.$slots).default)===null||t===void 0?void 0:t.call(r)}})}});export{Ro as N,Ue as c,Je as d};
