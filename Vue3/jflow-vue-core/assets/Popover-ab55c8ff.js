import{ek as Be,dC as Ce,el as Fe,d as ie,r as k,P as ke,O as ve,b3 as pe,a8 as p,F as me,k as Me,G as ge,f as K,v as Oe,dO as J,p as Y,bi as Re,aP as Ie,s as be,f1 as Ae,fV as ze}from"./index-f4658ae7.js";import{c as le,p as _e,d as Pe,m as je,u as G,V as De,z as Ne,a as He,b as Le}from"./Follower-3b5f0c65.js";import{k as Ve,c as A,e as O,h as Z,g as ee,f as Q,p as We,b as se}from"./light-0dfdc1ad.js";import{b as Ke}from"./_baseMap-457e4262.js";import{u as Ue}from"./use-config-816d55a6.js";import{a as q,o as U,g as ae,X as Xe}from"./Scrollbar-35d51129.js";import{f as te}from"./format-length-c9d165c6.js";import{u as Je}from"./use-css-vars-class-3ae3b4b3.js";import{i as de,r as oe,u as Ye,c as X}from"./use-merged-state-66be05d7.js";import{c as qe}from"./index-cad90cf4.js";import{i as Ge}from"./use-is-mounted-a34b74be.js";import{u as ce}from"./use-memo-f04d43e5.js";import{u as Qe}from"./use-compitable-1a225331.js";import{f as Ze}from"./flatten-2bdfb3d3.js";import{w as ue}from"./warn-77f3ea30.js";function et(e,t){var o=Be(e)?Ce:Ke;return o(e,Fe(t))}function tt(e,t=[],o){const r={};return t.forEach(d=>{r[d]=e[d]}),Object.assign(r,o)}function fe(e,t="default",o=void 0){const r=e[t];if(!r)return ue("getFirstSlotVNode",`slot[${t}] is empty`),null;const d=Ze(r(o));return d.length===1?d[0]:(ue("getFirstSlotVNode",`slot[${t}] should have exactly one child`),null)}let ne;function ot(){return ne===void 0&&(ne=navigator.userAgent.includes("Node.js")||navigator.userAgent.includes("jsdom")),ne}const j="@@mmoContext",nt={mounted(e,{value:t}){e[j]={handler:void 0},typeof t=="function"&&(e[j].handler=t,q("mousemoveoutside",e,t))},updated(e,{value:t}){const o=e[j];typeof t=="function"?o.handler?o.handler!==t&&(U("mousemoveoutside",e,o.handler),o.handler=t,q("mousemoveoutside",e,t)):(e[j].handler=t,q("mousemoveoutside",e,t)):o.handler&&(U("mousemoveoutside",e,o.handler),o.handler=void 0)},unmounted(e){const{handler:t}=e[j];t&&U("mousemoveoutside",e,t),e[j].handler=void 0}},rt=nt;function he(e){return typeof e=="string"?document.querySelector(e):e()}function we(e){return e instanceof HTMLElement}function ye(e){for(let t=0;t<e.childNodes.length;t++){const o=e.childNodes[t];if(we(o)&&($e(o)||ye(o)))return!0}return!1}function Se(e){for(let t=e.childNodes.length-1;t>=0;t--){const o=e.childNodes[t];if(we(o)&&($e(o)||Se(o)))return!0}return!1}function $e(e){if(!at(e))return!1;try{e.focus({preventScroll:!0})}catch(t){}return document.activeElement===e}function at(e){if(e.tabIndex>0||e.tabIndex===0&&e.getAttribute("tabIndex")!==null)return!0;if(e.getAttribute("disabled"))return!1;switch(e.nodeName){case"A":return!!e.href&&e.rel!=="ignore";case"INPUT":return e.type!=="hidden"&&e.type!=="file";case"BUTTON":case"SELECT":case"TEXTAREA":return!0;default:return!1}}let W=[];const it=ie({name:"FocusTrap",props:{disabled:Boolean,active:Boolean,autoFocus:{type:Boolean,default:!0},onEsc:Function,initialFocusTo:String,finalFocusTo:String,returnFocusOnDeactivated:{type:Boolean,default:!0}},setup(e){const t=qe(),o=k(null),r=k(null);let d=!1,s=!1;const h=typeof document=="undefined"?null:document.activeElement;function a(){return W[W.length-1]===t}function c(l){var i;l.code==="Escape"&&a()&&((i=e.onEsc)===null||i===void 0||i.call(e,l))}ke(()=>{ve(()=>e.active,l=>{l?(C(),q("keydown",document,c)):(U("keydown",document,c),d&&F())},{immediate:!0})}),pe(()=>{U("keydown",document,c),d&&F()});function b(l){if(!s&&a()){const i=m();if(i===null||i.contains(ae(l)))return;S("first")}}function m(){const l=o.value;if(l===null)return null;let i=l;for(;i=i.nextSibling,!(i===null||i instanceof Element&&i.tagName==="DIV"););return i}function C(){var l;if(!e.disabled){if(W.push(t),e.autoFocus){const{initialFocusTo:i}=e;i===void 0?S("first"):(l=he(i))===null||l===void 0||l.focus({preventScroll:!0})}d=!0,document.addEventListener("focus",b,!0)}}function F(){var l;if(e.disabled||(document.removeEventListener("focus",b,!0),W=W.filter(x=>x!==t),a()))return;const{finalFocusTo:i}=e;i!==void 0?(l=he(i))===null||l===void 0||l.focus({preventScroll:!0}):e.returnFocusOnDeactivated&&h instanceof HTMLElement&&(s=!0,h.focus({preventScroll:!0}),s=!1)}function S(l){if(a()&&e.active){const i=o.value,x=r.value;if(i!==null&&x!==null){const R=m();if(R==null||R===x){s=!0,i.focus({preventScroll:!0}),s=!1;return}s=!0;const _=l==="first"?ye(R):Se(R);s=!1,_||(s=!0,i.focus({preventScroll:!0}),s=!1)}}}function z(l){if(s)return;const i=m();i!==null&&(l.relatedTarget!==null&&i.contains(l.relatedTarget)?S("last"):S("first"))}function v(l){s||(l.relatedTarget!==null&&l.relatedTarget===o.value?S("last"):S("first"))}return{focusableStartRef:o,focusableEndRef:r,focusableStyle:"position: absolute; height: 0; width: 0;",handleStartFocus:z,handleEndFocus:v}},render(){const{default:e}=this.$slots;if(e===void 0)return null;if(this.disabled)return e();const{active:t,focusableStyle:o}=this;return p(me,null,[p("div",{"aria-hidden":"true",tabindex:t?"0":"-1",ref:"focusableStartRef",style:o,onFocus:this.handleStartFocus}),e(),p("div",{"aria-hidden":"true",style:o,ref:"focusableEndRef",tabindex:t?"0":"-1",onFocus:this.handleEndFocus})])}}),st={space:"6px",spaceArrow:"10px",arrowOffset:"10px",arrowOffsetVertical:"10px",arrowHeight:"6px",padding:"8px 14px"},lt=e=>{const{boxShadow2:t,popoverColor:o,textColor2:r,borderRadius:d,fontSize:s,dividerColor:h}=e;return Object.assign(Object.assign({},st),{fontSize:s,borderRadius:d,color:o,dividerColor:h,textColor:r,boxShadow:t})},dt={name:"Popover",common:Ve,self:lt},ct=dt,re={top:"bottom",bottom:"top",left:"right",right:"left"},g="var(--n-arrow-height) * 1.414",ut=A([O("popover",`
 transition:
 box-shadow .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier);
 position: relative;
 font-size: var(--n-font-size);
 color: var(--n-text-color);
 box-shadow: var(--n-box-shadow);
 word-break: break-word;
 `,[A(">",[O("scrollbar",`
 height: inherit;
 max-height: inherit;
 `)]),Z("raw",`
 background-color: var(--n-color);
 border-radius: var(--n-border-radius);
 `,[Z("scrollable",[Z("show-header-or-footer","padding: var(--n-padding);")])]),ee("header",`
 padding: var(--n-padding);
 border-bottom: 1px solid var(--n-divider-color);
 transition: border-color .3s var(--n-bezier);
 `),ee("footer",`
 padding: var(--n-padding);
 border-top: 1px solid var(--n-divider-color);
 transition: border-color .3s var(--n-bezier);
 `),Q("scrollable, show-header-or-footer",[ee("content",`
 padding: var(--n-padding);
 `)])]),O("popover-shared",`
 transform-origin: inherit;
 `,[O("popover-arrow-wrapper",`
 position: absolute;
 overflow: hidden;
 pointer-events: none;
 `,[O("popover-arrow",`
 transition: background-color .3s var(--n-bezier);
 position: absolute;
 display: block;
 width: calc(${g});
 height: calc(${g});
 box-shadow: 0 0 8px 0 rgba(0, 0, 0, .12);
 transform: rotate(45deg);
 background-color: var(--n-color);
 pointer-events: all;
 `)]),A("&.popover-transition-enter-from, &.popover-transition-leave-to",`
 opacity: 0;
 transform: scale(.85);
 `),A("&.popover-transition-enter-to, &.popover-transition-leave-from",`
 transform: scale(1);
 opacity: 1;
 `),A("&.popover-transition-enter-active",`
 transition:
 box-shadow .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier),
 opacity .15s var(--n-bezier-ease-out),
 transform .15s var(--n-bezier-ease-out);
 `),A("&.popover-transition-leave-active",`
 transition:
 box-shadow .3s var(--n-bezier),
 background-color .3s var(--n-bezier),
 color .3s var(--n-bezier),
 opacity .15s var(--n-bezier-ease-in),
 transform .15s var(--n-bezier-ease-in);
 `)]),B("top-start",`
 top: calc(${g} / -2);
 left: calc(${M("top-start")} - var(--v-offset-left));
 `),B("top",`
 top: calc(${g} / -2);
 transform: translateX(calc(${g} / -2)) rotate(45deg);
 left: 50%;
 `),B("top-end",`
 top: calc(${g} / -2);
 right: calc(${M("top-end")} + var(--v-offset-left));
 `),B("bottom-start",`
 bottom: calc(${g} / -2);
 left: calc(${M("bottom-start")} - var(--v-offset-left));
 `),B("bottom",`
 bottom: calc(${g} / -2);
 transform: translateX(calc(${g} / -2)) rotate(45deg);
 left: 50%;
 `),B("bottom-end",`
 bottom: calc(${g} / -2);
 right: calc(${M("bottom-end")} + var(--v-offset-left));
 `),B("left-start",`
 left: calc(${g} / -2);
 top: calc(${M("left-start")} - var(--v-offset-top));
 `),B("left",`
 left: calc(${g} / -2);
 transform: translateY(calc(${g} / -2)) rotate(45deg);
 top: 50%;
 `),B("left-end",`
 left: calc(${g} / -2);
 bottom: calc(${M("left-end")} + var(--v-offset-top));
 `),B("right-start",`
 right: calc(${g} / -2);
 top: calc(${M("right-start")} - var(--v-offset-top));
 `),B("right",`
 right: calc(${g} / -2);
 transform: translateY(calc(${g} / -2)) rotate(45deg);
 top: 50%;
 `),B("right-end",`
 right: calc(${g} / -2);
 bottom: calc(${M("right-end")} + var(--v-offset-top));
 `),...et({top:["right-start","left-start"],right:["top-end","bottom-end"],bottom:["right-end","left-end"],left:["top-start","bottom-start"]},(e,t)=>{const o=["right","left"].includes(t),r=o?"width":"height";return e.map(d=>{const s=d.split("-")[1]==="end",a=`calc((${`var(--v-target-${r}, 0px)`} - ${g}) / 2)`,c=M(d);return A(`[v-placement="${d}"] >`,[O("popover-shared",[Q("center-arrow",[O("popover-arrow",`${t}: calc(max(${a}, ${c}) ${s?"+":"-"} var(--v-offset-${o?"left":"top"}));`)])])])})})]);function M(e){return["top","bottom"].includes(e.split("-")[0])?"var(--n-arrow-offset)":"var(--n-arrow-offset-vertical)"}function B(e,t){const o=e.split("-")[0],r=["top","bottom"].includes(o)?"height: var(--n-space-arrow);":"width: var(--n-space-arrow);";return A(`[v-placement="${e}"] >`,[O("popover-shared",`
 margin-${re[o]}: var(--n-space);
 `,[Q("show-arrow",`
 margin-${re[o]}: var(--n-space-arrow);
 `),Q("overlap",`
 margin: 0;
 `),We("popover-arrow-wrapper",`
 right: 0;
 left: 0;
 top: 0;
 bottom: 0;
 ${o}: 100%;
 ${re[o]}: auto;
 ${r}
 `,[O("popover-arrow",t)])])])}const xe=Object.assign(Object.assign({},se.props),{to:G.propTo,show:Boolean,trigger:String,showArrow:Boolean,delay:Number,duration:Number,raw:Boolean,arrowPointToCenter:Boolean,arrowStyle:[String,Object],displayDirective:String,x:Number,y:Number,flip:Boolean,overlap:Boolean,placement:String,width:[Number,String],keepAliveOnHover:Boolean,scrollable:Boolean,contentStyle:[Object,String],headerStyle:[Object,String],footerStyle:[Object,String],internalDeactivateImmediately:Boolean,animated:Boolean,onClickoutside:Function,internalTrapFocus:Boolean,internalOnAfterLeave:Function,minWidth:Number,maxWidth:Number}),ft=({arrowStyle:e,clsPrefix:t})=>p("div",{key:"__popover-arrow__",class:`${t}-popover-arrow-wrapper`},p("div",{class:`${t}-popover-arrow`,style:e})),ht=ie({name:"PopoverBody",inheritAttrs:!1,props:xe,setup(e,{slots:t,attrs:o}){const{namespaceRef:r,mergedClsPrefixRef:d,inlineThemeDisabled:s}=Ue(e),h=se("Popover","-popover",ut,ct,e,d),a=k(null),c=Me("NPopover"),b=k(null),m=k(e.show),C=k(!1);ge(()=>{const{show:u}=e;u&&!ot()&&!e.internalDeactivateImmediately&&(C.value=!0)});const F=K(()=>{const{trigger:u,onClickoutside:y}=e,$=[],{positionManuallyRef:{value:f}}=c;return f||(u==="click"&&!y&&$.push([le,_,void 0,{capture:!0}]),u==="hover"&&$.push([rt,R])),y&&$.push([le,_,void 0,{capture:!0}]),(e.displayDirective==="show"||e.animated&&C.value)&&$.push([Oe,e.show]),$}),S=K(()=>{const u=e.width==="trigger"?void 0:te(e.width),y=[];u&&y.push({width:u});const{maxWidth:$,minWidth:f}=e;return $&&y.push({maxWidth:te($)}),f&&y.push({maxWidth:te(f)}),s||y.push(z.value),y}),z=K(()=>{const{common:{cubicBezierEaseInOut:u,cubicBezierEaseIn:y,cubicBezierEaseOut:$},self:{space:f,spaceArrow:H,padding:L,fontSize:I,textColor:V,dividerColor:n,color:w,boxShadow:E,borderRadius:P,arrowHeight:T,arrowOffset:Ee,arrowOffsetVertical:Te}}=h.value;return{"--n-box-shadow":E,"--n-bezier":u,"--n-bezier-ease-in":y,"--n-bezier-ease-out":$,"--n-font-size":I,"--n-text-color":V,"--n-color":w,"--n-divider-color":n,"--n-border-radius":P,"--n-arrow-height":T,"--n-arrow-offset":Ee,"--n-arrow-offset-vertical":Te,"--n-padding":L,"--n-space":f,"--n-space-arrow":H}}),v=s?Je("popover",void 0,z,e):void 0;c.setBodyInstance({syncPosition:l}),pe(()=>{c.setBodyInstance(null)}),ve(J(e,"show"),u=>{e.animated||(u?m.value=!0:m.value=!1)});function l(){var u;(u=a.value)===null||u===void 0||u.syncPosition()}function i(u){e.trigger==="hover"&&e.keepAliveOnHover&&e.show&&c.handleMouseEnter(u)}function x(u){e.trigger==="hover"&&e.keepAliveOnHover&&c.handleMouseLeave(u)}function R(u){e.trigger==="hover"&&!D().contains(ae(u))&&c.handleMouseMoveOutside(u)}function _(u){(e.trigger==="click"&&!D().contains(ae(u))||e.onClickoutside)&&c.handleClickOutside(u)}function D(){return c.getTriggerElement()}Y(_e,b),Y(Pe,null),Y(je,null);function N(){if(v==null||v.onRender(),!(e.displayDirective==="show"||e.show||e.animated&&C.value))return null;let y;const $=c.internalRenderBodyRef.value,{value:f}=d;if($)y=$([`${f}-popover-shared`,v==null?void 0:v.themeClass.value,e.overlap&&`${f}-popover-shared--overlap`,e.showArrow&&`${f}-popover-shared--show-arrow`,e.arrowPointToCenter&&`${f}-popover-shared--center-arrow`],b,S.value,i,x);else{const{value:H}=c.extraClassRef,{internalTrapFocus:L}=e,I=!de(t.header)||!de(t.footer),V=()=>{var n;const w=I?p(me,null,oe(t.header,T=>T?p("div",{class:`${f}-popover__header`,style:e.headerStyle},T):null),oe(t.default,T=>T?p("div",{class:`${f}-popover__content`,style:e.contentStyle},t):null),oe(t.footer,T=>T?p("div",{class:`${f}-popover__footer`,style:e.footerStyle},T):null)):e.scrollable?(n=t.default)===null||n===void 0?void 0:n.call(t):p("div",{class:`${f}-popover__content`,style:e.contentStyle},t),E=e.scrollable?p(Xe,{contentClass:I?void 0:`${f}-popover__content`,contentStyle:I?void 0:e.contentStyle},{default:()=>w}):w,P=e.showArrow?ft({arrowStyle:e.arrowStyle,clsPrefix:f}):null;return[E,P]};y=p("div",Ie({class:[`${f}-popover`,`${f}-popover-shared`,v==null?void 0:v.themeClass.value,H.map(n=>`${f}-${n}`),{[`${f}-popover--scrollable`]:e.scrollable,[`${f}-popover--show-header-or-footer`]:I,[`${f}-popover--raw`]:e.raw,[`${f}-popover-shared--overlap`]:e.overlap,[`${f}-popover-shared--show-arrow`]:e.showArrow,[`${f}-popover-shared--center-arrow`]:e.arrowPointToCenter}],ref:b,style:S.value,onKeydown:c.handleKeydown,onMouseenter:i,onMouseleave:x},o),L?p(it,{active:e.show,autoFocus:!0},{default:V}):V())}return be(y,F.value)}return{displayed:C,namespace:r,isMounted:c.isMountedRef,zIndex:c.zIndexRef,followerRef:a,adjustedTo:G(e),followerEnabled:m,renderContentNode:N}},render(){return p(De,{ref:"followerRef",zIndex:this.zIndex,show:this.show,enabled:this.followerEnabled,to:this.adjustedTo,x:this.x,y:this.y,flip:this.flip,placement:this.placement,containerClass:this.namespace,overlap:this.overlap,width:this.width==="trigger"?"target":void 0,teleportDisabled:this.adjustedTo===G.tdkey},{default:()=>this.animated?p(Re,{name:"popover-transition",appear:this.isMounted,onEnter:()=>{this.followerEnabled=!0},onAfterLeave:()=>{var e;(e=this.internalOnAfterLeave)===null||e===void 0||e.call(this),this.followerEnabled=!1,this.displayed=!1}},{default:this.renderContentNode}):this.renderContentNode()})}}),vt=Object.keys(xe),pt={focus:["onFocus","onBlur"],click:["onClick"],hover:["onMouseenter","onMouseleave"],manual:[],nested:["onFocus","onBlur","onMouseenter","onMouseleave","onClick"]};function mt(e,t,o){pt[t].forEach(r=>{e.props?e.props=Object.assign({},e.props):e.props={};const d=e.props[r],s=o[r];d?e.props[r]=(...h)=>{d(...h),s(...h)}:e.props[r]=s})}const gt={show:{type:Boolean,default:void 0},defaultShow:Boolean,showArrow:{type:Boolean,default:!0},trigger:{type:String,default:"hover"},delay:{type:Number,default:100},duration:{type:Number,default:100},raw:Boolean,placement:{type:String,default:"top"},x:Number,y:Number,arrowPointToCenter:Boolean,disabled:Boolean,getDisabled:Function,displayDirective:{type:String,default:"if"},arrowStyle:[String,Object],flip:{type:Boolean,default:!0},animated:{type:Boolean,default:!0},width:{type:[Number,String],default:void 0},overlap:Boolean,keepAliveOnHover:{type:Boolean,default:!0},zIndex:Number,to:G.propTo,scrollable:Boolean,contentStyle:[Object,String],headerStyle:[Object,String],footerStyle:[Object,String],onClickoutside:Function,"onUpdate:show":[Function,Array],onUpdateShow:[Function,Array],internalDeactivateImmediately:Boolean,internalSyncTargetWithParent:Boolean,internalInheritedEventHandlers:{type:Array,default:()=>[]},internalTrapFocus:Boolean,internalExtraClass:{type:Array,default:()=>[]},onShow:[Function,Array],onHide:[Function,Array],arrow:{type:Boolean,default:void 0},minWidth:Number,maxWidth:Number},bt=Object.assign(Object.assign(Object.assign({},se.props),gt),{internalOnAfterLeave:Function,internalRenderBody:Function}),At=ie({name:"Popover",inheritAttrs:!1,props:bt,__popover__:!0,setup(e){const t=Ge(),o=k(null),r=K(()=>e.show),d=k(e.defaultShow),s=Ye(r,d),h=ce(()=>e.disabled?!1:s.value),a=()=>{if(e.disabled)return!0;const{getDisabled:n}=e;return!!(n!=null&&n())},c=()=>a()?!1:s.value,b=Qe(e,["arrow","showArrow"]),m=K(()=>e.overlap?!1:b.value);let C=null;const F=k(null),S=k(null),z=ce(()=>e.x!==void 0&&e.y!==void 0);function v(n){const{"onUpdate:show":w,onUpdateShow:E,onShow:P,onHide:T}=e;d.value=n,w&&X(w,n),E&&X(E,n),n&&P&&X(P,!0),n&&T&&X(T,!1)}function l(){C&&C.syncPosition()}function i(){const{value:n}=F;n&&(window.clearTimeout(n),F.value=null)}function x(){const{value:n}=S;n&&(window.clearTimeout(n),S.value=null)}function R(){const n=a();if(e.trigger==="focus"&&!n){if(c())return;v(!0)}}function _(){const n=a();if(e.trigger==="focus"&&!n){if(!c())return;v(!1)}}function D(){const n=a();if(e.trigger==="hover"&&!n){if(x(),F.value!==null||c())return;const w=()=>{v(!0),F.value=null},{delay:E}=e;E===0?w():F.value=window.setTimeout(w,E)}}function N(){const n=a();if(e.trigger==="hover"&&!n){if(i(),S.value!==null||!c())return;const w=()=>{v(!1),S.value=null},{duration:E}=e;E===0?w():S.value=window.setTimeout(w,E)}}function u(){N()}function y(n){var w;c()&&(e.trigger==="click"&&(i(),x(),v(!1)),(w=e.onClickoutside)===null||w===void 0||w.call(e,n))}function $(){if(e.trigger==="click"&&!a()){i(),x();const n=!c();v(n)}}function f(n){e.internalTrapFocus&&n.key==="Escape"&&(i(),x(),v(!1))}function H(n){d.value=n}function L(){var n;return(n=o.value)===null||n===void 0?void 0:n.targetRef}function I(n){C=n}return Y("NPopover",{getTriggerElement:L,handleKeydown:f,handleMouseEnter:D,handleMouseLeave:N,handleClickOutside:y,handleMouseMoveOutside:u,setBodyInstance:I,positionManuallyRef:z,isMountedRef:t,zIndexRef:J(e,"zIndex"),extraClassRef:J(e,"internalExtraClass"),internalRenderBodyRef:J(e,"internalRenderBody")}),ge(()=>{s.value&&a()&&v(!1)}),{binderInstRef:o,positionManually:z,mergedShowConsideringDisabledProp:h,uncontrolledShow:d,mergedShowArrow:m,getMergedShow:c,setShow:H,handleClick:$,handleMouseEnter:D,handleMouseLeave:N,handleFocus:R,handleBlur:_,syncPosition:l}},render(){var e;const{positionManually:t,$slots:o}=this;let r,d=!1;if(!t&&(o.activator?r=fe(o,"activator"):r=fe(o,"trigger"),r)){r=Ae(r),r=r.type===ze?p("span",[r]):r;const s={onClick:this.handleClick,onMouseenter:this.handleMouseEnter,onMouseleave:this.handleMouseLeave,onFocus:this.handleFocus,onBlur:this.handleBlur};if(!((e=r.type)===null||e===void 0)&&e.__popover__)d=!0,r.props||(r.props={internalSyncTargetWithParent:!0,internalInheritedEventHandlers:[]}),r.props.internalSyncTargetWithParent=!0,r.props.internalInheritedEventHandlers?r.props.internalInheritedEventHandlers=[s,...r.props.internalInheritedEventHandlers]:r.props.internalInheritedEventHandlers=[s];else{const{internalInheritedEventHandlers:h}=this,a=[s,...h],c={onBlur:b=>{a.forEach(m=>{m.onBlur(b)})},onFocus:b=>{a.forEach(m=>{m.onFocus(b)})},onClick:b=>{a.forEach(m=>{m.onClick(b)})},onMouseenter:b=>{a.forEach(m=>{m.onMouseenter(b)})},onMouseleave:b=>{a.forEach(m=>{m.onMouseleave(b)})}};mt(r,h?"nested":t?"manual":this.trigger,c)}}return p(Le,{ref:"binderInstRef",syncTarget:!d,syncTargetWithParent:this.internalSyncTargetWithParent},{default:()=>{this.mergedShowConsideringDisabledProp;const s=this.getMergedShow();return[this.internalTrapFocus&&s?be(p("div",{style:{position:"fixed",inset:0}}),[[Ne,{enabled:s,zIndex:this.zIndex}]]):null,t?null:p(He,null,{default:()=>r}),p(ht,tt(this.$props,vt,Object.assign(Object.assign({},this.$attrs),{showArrow:this.mergedShowArrow,show:s})),{default:()=>{var h,a;return(a=(h=this.$slots).default)===null||a===void 0?void 0:a.call(h)},header:()=>{var h,a;return(a=(h=this.$slots).header)===null||a===void 0?void 0:a.call(h)},footer:()=>{var h,a;return(a=(h=this.$slots).footer)===null||a===void 0?void 0:a.call(h)}})]}})}});export{it as F,At as N,gt as a,fe as g,tt as k,ct as p,ft as r};
