var D=Object.defineProperty;var m=(B,E,u)=>E in B?D(B,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):B[E]=u;var i=(B,E,u)=>(m(B,typeof E!="symbol"?E+"":E,u),u);var p=(B,E,u)=>new Promise((F,A)=>{var r=t=>{try{o(u.next(t))}catch(n){A(n)}},s=t=>{try{o(u.throw(t))}catch(n){A(n)}},o=t=>t.done?F(t.value):Promise.resolve(t.value).then(r,s);o((u=u.apply(B,E)).next())});import{G as a}from"./DataType-33901a1c.js";import{Cond as C,CondAttr as e}from"./Cond-7bb97535.js";import{PageBaseGroupEdit as l}from"./PageBaseGroupEdit-202e8e85.js";import"./index-f4658ae7.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./EntityNoName-d08126ae.js";import"./DBAccess-d3bef90d.js";import"./Node-6b42ba5e.js";import"./EntityNodeID-d5ae71b1.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./Help-be517e8f.js";class V extends l{constructor(){super("GPE_CondShenFenDept");i(this,"Desc0",`
  #### \u8BF4\u660E
   - \u9ED8\u8BA4\u4E3A\u8BE5\u6A21\u5F0F\u3002
   - \u63D0\u4EA4\u4EBA\u5458\u767B\u5F55\u90E8\u95E8\u5C31\u662F\uFF0C\u6761\u4EF6\u7684\u5224\u65AD\u53C2\u6570.
    `);i(this,"Desc1",`
  #### \u8BF4\u660E
   - \u6307\u5B9A\u8282\u70B9\u7684\u5904\u7406\u4EBA\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD\u3002
   - \u5982\u4E0B\u56FE\uFF0C\u8BBE\u5907\u7EF4\u4FEE\u7533\u8BF7\u4EBA\uFF0C\u4E3A\u516C\u53F8\u4E0D\u540C\u90E8\u95E8\uFF0C\u5F53\u8BBE\u5907\u90E8\u4EBA\u5458\u68C0\u67E5\u540E\uFF0C\u8BA4\u5B9A\u8BE5\u8BBE\u5907\u53EF\u4EE5\u7EF4\u62A4\u5E76\u7ED9\u51FA\u62A5\u4EF7\uFF0C\u8FD9\u65F6\u5BA1\u6279\u6743\u5C31\u4EA4\u8FD8\u7ED9\u7533\u8BF7\u90E8\u95E8\u7684\u9886\u5BFC\u3002
   - \u90A3\u4E48\u5728\u8F6C\u5411\u6761\u4EF6\u5C31\u8BBE\u7F6E\u4E3A\u6309\u89D2\u8272\u9009\u62E9\uFF0C\u4E5F\u5C31\u662F\u628A\u5404\u90E8\u95E8\u9886\u5BFC\u89D2\u8272\u7684\u4EBA\u9009\u62E9\u51FA\u6765\uFF0C\u518D\u786E\u8BA4\u7533\u8BF7\u4EBA\u7684\u90E8\u95E8\uFF0C\u8FD9\u6837\uFF0C\u5177\u6709\u5BA1\u6279\u6743\u7684\u63A5\u6536\u4EBA\u5C31\u662F\u7533\u8BF7\u4EBA\u7684\u90E8\u95E8\u9886\u5BFC\u3002
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModel.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  - \u9009\u62E9\u65B0\u589E\u65B9\u5411\u6761\u4EF6
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModelSetting.png "\u5C4F\u5E55\u622A\u56FE")
  - \u9009\u62E9\u4EBA\u5458\u8EAB\u4EFD
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/Cond2020/Img/CondShenFenModelSetting2.png "\u5C4F\u5E55\u622A\u56FE")
      `);i(this,"Desc2",`
  #### \u8BF4\u660E
  - \u9009\u62E9\u7684\u5B57\u6BB5\u5B58\u50A8\u7684\u662F\u4F5C\u4E3A\u4EBA\u5458\u8EAB\u4EFD(\u8BE5\u5B57\u6BB5\u91CC\u5B58\u50A8\u7684\u662F\u8D26\u53F7)
  - \u6307\u5B9A\u8282\u70B9\u8868\u5355\u7684\u5B57\u6BB5\u4F5C\u4E3A\u672C\u6B65\u9AA4\u7684\u672C\u6B65\u9AA4\u7684\u8EAB\u4EFD\u3002
  - \u9700\u8981\u9009\u62E9\u4E00\u4E2A\u8282\u70B9ID.
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingFlow2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u8868\u5355\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingBiaodan2.png "\u5C4F\u5E55\u622A\u56FE")
  #### \u914D\u7F6E\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/AccepterRole/Img/ShengqingPeizhi2.png "\u5C4F\u5E55\u622A\u56FE")
      `);i(this,"Desc3",`
  #### \u8BF4\u660E
   - \u83B7\u53D6\u5F53\u524D\u4EBA\u5458\u4FE1\u606F\u7684\u662F\u6309\u7167\u6307\u5B9A\u7684\u90E8\u95E8\u4E0E\u6307\u5B9A\u7684\u89D2\u8272\u7684\u4EA4\u96C6\u8BA1\u7B97.
   - \u6307\u5B9A\u7684\u90E8\u95E8\u662F\u4ECEccbpm\u7684\u7CFB\u7EDF\u53C2\u6570\u83B7\u53D6\u7684.
   - \u5728\u6D41\u7A0B\u8FD0\u884C\u7684\u8FC7\u7A0B\u4E2D\u7CFB\u7EDF\u53C2\u6570\uFF0C\u662F\u901A\u8FC7 Flow_SavePara() \u7684\u65B9\u6CD5\u4FDD\u5B58\u5230ccbpm\u4E2D\u7684.
   - \u6D41\u7A0B\u7684\u7CFB\u7EDF\u53C2\u6570\u5B58\u50A8\u5728 \u8868:WF_GenerWorkFlow \u5B57\u6BB5:AtPara \u4E2D.
   #### \u5176\u4ED6
   - \u8BF7\u9605\u8BFBccbpm\u7684\u63A5\u53E3\u65B9\u6CD5\uFF0C\u4FDD\u5B58\u53C2\u6570.

    `);this.PageTitle="\u4EBA\u5458\u8EAB\u4EFD"}Init(){return p(this,null,function*(){this.entity=new C,this.KeyOfEn=e.SpecOperWay;const u=new C(this.PKVal);yield u.Retrieve(),this.AddGroup("A","\u6309\u53D1\u9001\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97"),this.Blank("0"," \u53D1\u9001\u4EBA\u767B\u5F55\u7684\u90E8\u95E8",this.HelpUn),this.Blank("1"," \u53D1\u9001\u4EBA\u7684\u6240\u6709\u90E8\u95E8",this.HelpUn),this.Blank("2"," \u53D1\u9001\u4EBA\u4F7F\u7528\u7684\u90E8\u95E8",this.HelpUn),this.Blank("3"," \u53D1\u9001\u4EBA\u4F7F\u7528\u90E8\u95E8\u7684\u7236\u7EA7",this.HelpUn),this.AddGroup("B","\u6309\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u8BA1\u7B97");const F=`SELECT NodeID No,Name FROM WF_Node WHERE FK_Flow='${u.FK_Flow}'`;this.SelectItemsByList("10","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4F7F\u7528\u90E8\u95E8",this.HelpUn,!1,F,e.SpecOperPara,"Tag1"),this.SelectItemsByList("11","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u6240\u6709\u90E8\u95E8",this.HelpUn,!1,F,e.SpecOperPara,"Tag1"),this.SelectItemsByList("12","\u6307\u5B9A\u8282\u70B9\u63D0\u4EA4\u4EBA\u7684\u4E3B\u90E8\u95E8",this.HelpUn,!1,F,e.SpecOperPara,"Tag1"),this.AddGroup("C","\u6309\u8868\u5355\u5B57\u6BB5\u4EBA\u5458\u8BA1\u7B97");const A="ND"+parseInt(u.FK_Flow)+"Rpt",r=a.SQLOfMapAttrsGener(A);this.SelectItemsByList("20","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u4EBA\u5458\u7F16\u53F7-\u4E3B\u90E8\u95E8",this.HelpUn,!1,r,e.SpecOperPara,"Tag1"),this.SelectItemsByList("21","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u4EBA\u5458\u7F16\u53F7-\u6240\u6709\u90E8\u95E8",this.HelpUn,!1,r,e.SpecOperPara,"Tag1"),this.SelectItemsByList("22","\u5B57\u6BB5(\u53C2\u6570)\u503C\u662F\u90E8\u95E8\u7F16\u53F7",this.HelpUn,!1,r,e.SpecOperPara,"Tag1"),this.SingleTB("23","\u7CFB\u7EDF\u53C2\u6570\u503C\u662F\u90E8\u95E8\u7F16\u53F7",this.Desc3,e.SpecOperPara,"\u8BF7\u8F93\u5165\u7CFB\u7EDF\u53C2\u6570")})}AfterSave(u,F){if(u==F)throw new Error("Method not implemented.")}BtnClick(u,F,A){if(u==F||u===A)throw new Error("Method not implemented.")}}export{V as GPE_CondShenFenDept};
