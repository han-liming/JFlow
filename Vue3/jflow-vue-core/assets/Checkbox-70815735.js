import{a8 as u,d as _,r as U,f as B,p as se,dO as H,k as be}from"./index-f4658ae7.js";import{u as j,a as ue}from"./use-config-816d55a6.js";import{u as K}from"./use-form-item-34ce685d.js";import{u as L,c as d,r as he}from"./use-merged-state-66be05d7.js";import{k as fe,o as ke,c as x,e as c,f as D,g as w,m as me,n as ve,b as O}from"./light-0dfdc1ad.js";import{a as xe,b as ge}from"./Loading-fead3a83.js";import{u as pe}from"./use-rtl-889b67fe.js";import{u as Ce}from"./use-memo-f04d43e5.js";import{c as N}from"./create-key-bf4384d6.js";import{u as ye}from"./use-css-vars-class-3ae3b4b3.js";import{c as ze}from"./index-cad90cf4.js";import{a as we}from"./Scrollbar-35d51129.js";const Re={sizeSmall:"14px",sizeMedium:"16px",sizeLarge:"18px",labelPadding:"0 8px",labelFontWeight:"400"},Se=o=>{const{baseColor:g,inputColorDisabled:h,cardColor:z,modalColor:R,popoverColor:t,textColorDisabled:m,borderColor:l,primaryColor:n,textColor2:a,fontSizeSmall:S,fontSizeMedium:f,fontSizeLarge:r,borderRadiusSmall:v,lineHeight:k}=o;return Object.assign(Object.assign({},Re),{labelLineHeight:k,fontSizeSmall:S,fontSizeMedium:f,fontSizeLarge:r,borderRadius:v,color:g,colorChecked:n,colorDisabled:h,colorDisabledChecked:h,colorTableHeader:z,colorTableHeaderModal:R,colorTableHeaderPopover:t,checkMarkColor:g,checkMarkColorDisabled:m,checkMarkColorDisabledChecked:m,border:`1px solid ${l}`,borderDisabled:`1px solid ${l}`,borderDisabledChecked:`1px solid ${l}`,borderChecked:`1px solid ${n}`,borderFocus:`1px solid ${n}`,boxShadowFocus:`0 0 0 2px ${ke(n,{alpha:.3})}`,textColor:a,textColorDisabled:m})},Te={name:"Checkbox",common:fe,self:Se},De=Te,$e=u("svg",{viewBox:"0 0 64 64",class:"check-icon"},u("path",{d:"M50.42,16.76L22.34,39.45l-8.1-11.46c-1.12-1.58-3.3-1.96-4.88-0.84c-1.58,1.12-1.95,3.3-0.84,4.88l10.26,14.51  c0.56,0.79,1.42,1.31,2.38,1.45c0.16,0.02,0.32,0.03,0.48,0.03c0.8,0,1.57-0.27,2.2-0.78l30.99-25.03c1.5-1.21,1.74-3.42,0.52-4.92  C54.13,15.78,51.93,15.55,50.42,16.76z"})),Me=u("svg",{viewBox:"0 0 100 100",class:"line-icon"},u("path",{d:"M80.2,55.5H21.4c-2.8,0-5.1-2.5-5.1-5.5l0,0c0-3,2.3-5.5,5.1-5.5h58.7c2.8,0,5.1,2.5,5.1,5.5l0,0C85.2,53.1,82.9,55.5,80.2,55.5z"})),E=ue("n-checkbox-group"),Fe={min:Number,max:Number,size:String,value:Array,defaultValue:{type:Array,default:null},disabled:{type:Boolean,default:void 0},"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array],onChange:[Function,Array]},Ge=_({name:"CheckboxGroup",props:Fe,setup(o){const{mergedClsPrefixRef:g}=j(o),h=K(o),{mergedSizeRef:z,mergedDisabledRef:R}=h,t=U(o.defaultValue),m=B(()=>o.value),l=L(m,t),n=B(()=>{var f;return((f=l.value)===null||f===void 0?void 0:f.length)||0}),a=B(()=>Array.isArray(l.value)?new Set(l.value):new Set);function S(f,r){const{nTriggerFormInput:v,nTriggerFormChange:k}=h,{onChange:i,"onUpdate:value":p,onUpdateValue:C}=o;if(Array.isArray(l.value)){const s=Array.from(l.value),A=s.findIndex(I=>I===r);f?~A||(s.push(r),C&&d(C,s,{actionType:"check",value:r}),p&&d(p,s,{actionType:"check",value:r}),v(),k(),t.value=s,i&&d(i,s)):~A&&(s.splice(A,1),C&&d(C,s,{actionType:"uncheck",value:r}),p&&d(p,s,{actionType:"uncheck",value:r}),i&&d(i,s),t.value=s,v(),k())}else f?(C&&d(C,[r],{actionType:"check",value:r}),p&&d(p,[r],{actionType:"check",value:r}),i&&d(i,[r]),t.value=[r],v(),k()):(C&&d(C,[],{actionType:"uncheck",value:r}),p&&d(p,[],{actionType:"uncheck",value:r}),i&&d(i,[]),t.value=[],v(),k())}return se(E,{checkedCountRef:n,maxRef:H(o,"max"),minRef:H(o,"min"),valueSetRef:a,disabledRef:R,mergedSizeRef:z,toggleCheckbox:S}),{mergedClsPrefix:g}},render(){return u("div",{class:`${this.mergedClsPrefix}-checkbox-group`,role:"group"},this.$slots)}}),Ae=x([c("checkbox",`
 font-size: var(--n-font-size);
 outline: none;
 cursor: pointer;
 display: inline-flex;
 flex-wrap: nowrap;
 align-items: flex-start;
 word-break: break-word;
 line-height: var(--n-size);
 --n-merged-color-table: var(--n-color-table);
 `,[D("show-label","line-height: var(--n-label-line-height);"),x("&:hover",[c("checkbox-box",[w("border","border: var(--n-border-checked);")])]),x("&:focus:not(:active)",[c("checkbox-box",[w("border",`
 border: var(--n-border-focus);
 box-shadow: var(--n-box-shadow-focus);
 `)])]),D("inside-table",[c("checkbox-box",`
 background-color: var(--n-merged-color-table);
 `)]),D("checked",[c("checkbox-box",`
 background-color: var(--n-color-checked);
 `,[c("checkbox-icon",[x(".check-icon",`
 opacity: 1;
 transform: scale(1);
 `)])])]),D("indeterminate",[c("checkbox-box",[c("checkbox-icon",[x(".check-icon",`
 opacity: 0;
 transform: scale(.5);
 `),x(".line-icon",`
 opacity: 1;
 transform: scale(1);
 `)])])]),D("checked, indeterminate",[x("&:focus:not(:active)",[c("checkbox-box",[w("border",`
 border: var(--n-border-checked);
 box-shadow: var(--n-box-shadow-focus);
 `)])]),c("checkbox-box",`
 background-color: var(--n-color-checked);
 border-left: 0;
 border-top: 0;
 `,[w("border",{border:"var(--n-border-checked)"})])]),D("disabled",{cursor:"not-allowed"},[D("checked",[c("checkbox-box",`
 background-color: var(--n-color-disabled-checked);
 `,[w("border",{border:"var(--n-border-disabled-checked)"}),c("checkbox-icon",[x(".check-icon, .line-icon",{fill:"var(--n-check-mark-color-disabled-checked)"})])])]),c("checkbox-box",`
 background-color: var(--n-color-disabled);
 `,[w("border",`
 border: var(--n-border-disabled);
 `),c("checkbox-icon",[x(".check-icon, .line-icon",`
 fill: var(--n-check-mark-color-disabled);
 `)])]),w("label",`
 color: var(--n-text-color-disabled);
 `)]),c("checkbox-box-wrapper",`
 position: relative;
 width: var(--n-size);
 flex-shrink: 0;
 flex-grow: 0;
 user-select: none;
 -webkit-user-select: none;
 `),c("checkbox-box",`
 position: absolute;
 left: 0;
 top: 50%;
 transform: translateY(-50%);
 height: var(--n-size);
 width: var(--n-size);
 display: inline-block;
 box-sizing: border-box;
 border-radius: var(--n-border-radius);
 background-color: var(--n-color);
 transition: background-color 0.3s var(--n-bezier);
 `,[w("border",`
 transition:
 border-color .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier);
 border-radius: inherit;
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 border: var(--n-border);
 `),c("checkbox-icon",`
 display: flex;
 align-items: center;
 justify-content: center;
 position: absolute;
 left: 1px;
 right: 1px;
 top: 1px;
 bottom: 1px;
 `,[x(".check-icon, .line-icon",`
 width: 100%;
 fill: var(--n-check-mark-color);
 opacity: 0;
 transform: scale(0.5);
 transform-origin: center;
 transition:
 fill 0.3s var(--n-bezier),
 transform 0.3s var(--n-bezier),
 opacity 0.3s var(--n-bezier),
 border-color 0.3s var(--n-bezier);
 `),xe({left:"1px",top:"1px"})])]),w("label",`
 color: var(--n-text-color);
 transition: color .3s var(--n-bezier);
 user-select: none;
 -webkit-user-select: none;
 padding: var(--n-label-padding);
 font-weight: var(--n-label-font-weight);
 `,[x("&:empty",{display:"none"})])]),me(c("checkbox",`
 --n-merged-color-table: var(--n-color-table-modal);
 `)),ve(c("checkbox",`
 --n-merged-color-table: var(--n-color-table-popover);
 `))]),Be=Object.assign(Object.assign({},O.props),{size:String,checked:{type:[Boolean,String,Number],default:void 0},defaultChecked:{type:[Boolean,String,Number],default:!1},value:[String,Number],disabled:{type:Boolean,default:void 0},indeterminate:Boolean,label:String,focusable:{type:Boolean,default:!0},checkedValue:{type:[Boolean,String,Number],default:!0},uncheckedValue:{type:[Boolean,String,Number],default:!1},"onUpdate:checked":[Function,Array],onUpdateChecked:[Function,Array],privateInsideTable:Boolean,onChange:[Function,Array]}),We=_({name:"Checkbox",props:Be,setup(o){const g=U(null),{mergedClsPrefixRef:h,inlineThemeDisabled:z,mergedRtlRef:R}=j(o),t=K(o,{mergedSize(e){const{size:y}=o;if(y!==void 0)return y;if(n){const{value:b}=n.mergedSizeRef;if(b!==void 0)return b}if(e){const{mergedSize:b}=e;if(b!==void 0)return b.value}return"medium"},mergedDisabled(e){const{disabled:y}=o;if(y!==void 0)return y;if(n){if(n.disabledRef.value)return!0;const{maxRef:{value:b},checkedCountRef:T}=n;if(b!==void 0&&T.value>=b&&!r.value)return!0;const{minRef:{value:M}}=n;if(M!==void 0&&T.value<=M&&r.value)return!0}return e?e.disabled.value:!1}}),{mergedDisabledRef:m,mergedSizeRef:l}=t,n=be(E,null),a=U(o.defaultChecked),S=H(o,"checked"),f=L(S,a),r=Ce(()=>{if(n){const e=n.valueSetRef.value;return e&&o.value!==void 0?e.has(o.value):!1}else return f.value===o.checkedValue}),v=O("Checkbox","-checkbox",Ae,De,o,h);function k(e){if(n&&o.value!==void 0)n.toggleCheckbox(!r.value,o.value);else{const{onChange:y,"onUpdate:checked":b,onUpdateChecked:T}=o,{nTriggerFormInput:M,nTriggerFormChange:P}=t,F=r.value?o.uncheckedValue:o.checkedValue;b&&d(b,F,e),T&&d(T,F,e),y&&d(y,F,e),M(),P(),a.value=F}}function i(e){m.value||k(e)}function p(e){if(!m.value)switch(e.key){case" ":case"Enter":k(e)}}function C(e){switch(e.key){case" ":e.preventDefault()}}const s={focus:()=>{var e;(e=g.value)===null||e===void 0||e.focus()},blur:()=>{var e;(e=g.value)===null||e===void 0||e.blur()}},A=pe("Checkbox",R,h),I=B(()=>{const{value:e}=l,{common:{cubicBezierEaseInOut:y},self:{borderRadius:b,color:T,colorChecked:M,colorDisabled:P,colorTableHeader:F,colorTableHeaderModal:V,colorTableHeaderPopover:G,checkMarkColor:W,checkMarkColorDisabled:Y,border:q,borderFocus:J,borderDisabled:Q,borderChecked:X,boxShadowFocus:Z,textColor:ee,textColorDisabled:oe,checkMarkColorDisabledChecked:re,colorDisabledChecked:ne,borderDisabledChecked:ae,labelPadding:ce,labelLineHeight:le,labelFontWeight:ie,[N("fontSize",e)]:de,[N("size",e)]:te}}=v.value;return{"--n-label-line-height":le,"--n-label-font-weight":ie,"--n-size":te,"--n-bezier":y,"--n-border-radius":b,"--n-border":q,"--n-border-checked":X,"--n-border-focus":J,"--n-border-disabled":Q,"--n-border-disabled-checked":ae,"--n-box-shadow-focus":Z,"--n-color":T,"--n-color-checked":M,"--n-color-table":F,"--n-color-table-modal":V,"--n-color-table-popover":G,"--n-color-disabled":P,"--n-color-disabled-checked":ne,"--n-text-color":ee,"--n-text-color-disabled":oe,"--n-check-mark-color":W,"--n-check-mark-color-disabled":Y,"--n-check-mark-color-disabled-checked":re,"--n-font-size":de,"--n-label-padding":ce}}),$=z?ye("checkbox",B(()=>l.value[0]),I,o):void 0;return Object.assign(t,s,{rtlEnabled:A,selfRef:g,mergedClsPrefix:h,mergedDisabled:m,renderedChecked:r,mergedTheme:v,labelId:ze(),handleClick:i,handleKeyUp:p,handleKeyDown:C,cssVars:z?void 0:I,themeClass:$==null?void 0:$.themeClass,onRender:$==null?void 0:$.onRender})},render(){var o;const{$slots:g,renderedChecked:h,mergedDisabled:z,indeterminate:R,privateInsideTable:t,cssVars:m,labelId:l,label:n,mergedClsPrefix:a,focusable:S,handleKeyUp:f,handleKeyDown:r,handleClick:v}=this;(o=this.onRender)===null||o===void 0||o.call(this);const k=he(g.default,i=>n||i?u("span",{class:`${a}-checkbox__label`,id:l},n||i):null);return u("div",{ref:"selfRef",class:[`${a}-checkbox`,this.themeClass,this.rtlEnabled&&`${a}-checkbox--rtl`,h&&`${a}-checkbox--checked`,z&&`${a}-checkbox--disabled`,R&&`${a}-checkbox--indeterminate`,t&&`${a}-checkbox--inside-table`,k&&`${a}-checkbox--show-label`],tabindex:z||!S?void 0:0,role:"checkbox","aria-checked":R?"mixed":h,"aria-labelledby":l,style:m,onKeyup:f,onKeydown:r,onClick:v,onMousedown:()=>{we("selectstart",window,i=>{i.preventDefault()},{once:!0})}},u("div",{class:`${a}-checkbox-box-wrapper`},"\xA0",u("div",{class:`${a}-checkbox-box`},u(ge,null,{default:()=>this.indeterminate?u("div",{key:"indeterminate",class:`${a}-checkbox-icon`},Me):u("div",{key:"check",class:`${a}-checkbox-icon`},$e)}),u("div",{class:`${a}-checkbox-box__border`}))),k)}});export{Ge as N,We as a,De as c};
