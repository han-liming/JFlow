var a=Object.defineProperty;var C=(B,o,u)=>o in B?a(B,o,{enumerable:!0,configurable:!0,writable:!0,value:u}):B[o]=u;var s=(B,o,u)=>(C(B,typeof o!="symbol"?o+"":o,u),u);var l=(B,o,u)=>new Promise((t,D)=>{var m=E=>{try{A(u.next(E))}catch(e){D(e)}},n=E=>{try{A(u.throw(E))}catch(e){D(e)}},A=E=>E.done?t(E.value):Promise.resolve(E.value).then(m,n);A((u=u.apply(B,o)).next())});import{SubFlow as S}from"./SubFlow-ddccebaa.js";import{PageBaseGroupNew as p}from"./PageBaseGroupNew-ee20c033.js";import{Flow as c}from"./Flow-6121039a.js";import{Node as b}from"./Node-6b42ba5e.js";import"./UAC-8e255d47.js";import"./index-f4658ae7.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityMyPK-e742fec8.js";import"./Entities-6a72b013.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./EntityNoName-d08126ae.js";import"./BSEntity-840a884b.js";import"./EntityNodeID-d5ae71b1.js";class Z extends p{constructor(){super("GPN_NewSubFlow");s(this,"Docs0",`
  #### \u5E2E\u52A9
   - \u624B\u5DE5\u542F\u52A8\u7684\u5B50\u6D41\u7A0B\uFF0C\u4F9D\u9644\u4E8E\u5B50\u6D41\u7A0B\u7EC4\u4EF6\u3002
   - \u7ED1\u4F7F\u7528\u5B50\u6D41\u7A0B\u7EC4\u4EF6\uFF0C\u53EF\u4EE5\u542F\u7528\u4E00\u4E2A\u6216\u8005\u591A\u4E2A\u5B50\u6D41\u7A0B\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/SubFlow/Img/SubFlowHand1.png "\u5C4F\u5E55\u622A\u56FE.png")
   
`);s(this,"Docs1",`
  #### \u5E2E\u52A9
   - \u81EA\u52A8\u89E6\u53D1\u5B50\u6D41\u7A0B\uFF0C\u5728\u6307\u5B9A\u7684\u4E8B\u4EF6\uFF0C\u6307\u5B9A\u7684\u6761\u4EF6\u6EE1\u8DB3\u7684\u65F6\u5019\uFF0C\u5C31\u81EA\u52A8\u542F\u52A8\u5B50\u6D41\u3002
   - \u6307\u5B9A\u7684\u4E8B\u4EF6\uFF1A \u53D1\u9001\u65F6\uFF0C \u5DE5\u4F5C\u5230\u8FBE\u65F6\u3002
   - \u6307\u5B9A\u7684\u6761\u4EF6\uFF1A\u6309\u7167\u8868\u5355\u5B57\u6BB5\uFF0C\u6309\u7167\u53C2\u6570\uFF08\u7C7B\u4F3C\u4E8E\u914D\u7F6E\u65B9\u5411\u6761\u4EF6\uFF09\u3002
   - \u5176\u4ED6\u7684\u6761\u4EF6\u4E0E\u5C5E\u6027\u6982\u5FF5\u4E0E\u624B\u5DE5\u542F\u52A8\u5B50\u6D41\u7A0B\u76F8\u540C\u3002
  #### \u5E2E\u52A9\u6587\u6863
   - <a  href="https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3982374&doc_id=31094"> \u66F4\u591A\u5185\u5BB9\uFF0C\u8BE6\u89C1\u8BF4\u660E </a>
  #### \u56FE\u4F8B1
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/SubFlow/Img/SubFlow2.png "\u5C4F\u5E55\u622A\u56FE.png")

  #### \u56FE\u4F8B2
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/SubFlow/Img/SubFlow3.png "\u5C4F\u5E55\u622A\u56FE.png")
  
`);s(this,"Docs2",`
  #### \u5E2E\u52A9
   - \u5EF6\u7EED\u5B50\u6D41\u7A0B\u662F\u4E00\u4E2A\u7279\u6B8A\u7684\u4E0B\u7EA7\u5B50\u6D41\u7A0B.
   - \u4E3B\u6D41\u7A0B\u7684\u7279\u5B9A\u8282\u70B9\uFF0C\u53EA\u80FD\u53D1\u8D77\u4E00\u4E2A\u5EF6\u7EED\u5B50\u6D41\u7A0B\uFF0C\u542F\u52A8\u540E\u8BE5\u8282\u70B9\u7684\u6D3B\u52A8\u5C31\u88AB\u51BB\u7ED3\u4E86\uFF0C\u9700\u8981\u7B49\u5F85\u5EF6\u7EED\u6D41\u7A0B\u8FD0\u884C\u540E\uFF0C\u6839\u636E\u914D\u7F6E\u4E3B\u6D41\u7A0B\u81EA\u52A8\u8FD0\u884C\u6216\u8005\u81EA\u52A8\u7ED3\u675F\u3002
   - \u5B50\u6D41\u7A0B\u7684\u5F00\u59CB\u8282\u70B9\u4E5F\u53EF\u4EE5\u9000\u56DE\u5230\u7236\u6D41\u7A0B\u4E0A\u53BB\u3002
  #### \u5E2E\u52A9\u6587\u6863
  - <a  href="https://gitee.com/opencc/JFlow/wikis/pages/preview?sort_id=3982375&doc_id=31094"> \u66F4\u591A\u5185\u5BB9\uFF0C\u8BE6\u89C1\u8BF4\u660E </a>
  #### \u8FD0\u884C\u56FE\u4F8B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/SubFlow/Img/SubFlowYanXu1.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u6D41\u7A0B\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/SubFlow/Img/SubFlowYanXu2.png "\u5C4F\u5E55\u622A\u56FE.png")


`);s(this,"Docs3",`
  #### \u5E2E\u52A9
   - \u5728\u6D41\u7A0B\u5C5E\u6027\u7684\u524D\u7F6E\u5BFC\u822A\u4E2D\uFF0C\u6709\u51E0\u4E2A\u9009\u9879\u9700\u8981\u5728\u5F00\u59CB\u8282\u70B9\u9009\u62E9\u5B50\u6D41\u7A0B\u3002
   - \u8BF7\u53C2\u8003\u6D41\u7A0B\u5C5E\u6027\u7684\u524D\u7F6E\u5BFC\u822A\u529F\u80FD.
  #### \u5E94\u7528\u573A\u666F
   - \u6211\u4EEC\u505A\u4E00\u4E2A\u8D39\u7528\u62A5\u9500\u5355\uFF0C\u9700\u8981\u9009\u62E9\u4E00\u4E2A\u6216\u591A\u4E2A\u91C7\u8D2D\u7533\u8BF7\u5355.
   - \u5F53\u524D\u6D41\u7A0B\u5C31\u662F\u7236\u6D41\u7A0B\uFF0C\u8981\u9009\u62E9\u7684\u5355\u7B14\u6216\u8005\u591A\u7B14\u6570\u636E\u5C31\u662F\u5B50\u6D41\u7A0B.
   - \u8FD9\u4E2A\u73B0\u8C61\u662F\u5148\u51FA\u73B0\u5B50\u6D41\u7A0B\uFF0C\u7B49\u5176\u5B8C\u6210\u540E\u51FA\u73B0\u7236\u6D41\u7A0B.
 
 `);this.ForEntityClassID="TS.WF.SFlow.SubFlow",this.PageTitle="\u65B0\u5EFA\u5B50\u6D41\u7A0B"}Init(){this.AddGroup("A","\u8BF7\u9009\u62E9\u5B50\u6D41\u7A0B\u6A21\u5F0F");const u="SELECT No,Name FROM WF_FlowSort ORDER BY Idx",t="SELECT No,Name,FK_FlowSort as GroupNo FROM WF_Flow ORDER BY Idx";this.SelectItemsByGroupList("0","\u624B\u5DE5\u542F\u52A8\u5B50\u6D41\u7A0B",this.Docs0,!1,u,t),this.SelectItemsByGroupList("1","\u81EA\u52A8\u542F\u52A8\u5B50\u6D41\u7A0B",this.Docs1,!1,u,t),this.SelectItemsByGroupList("2","\u5EF6\u7EED\u5B50\u6D41\u7A0B",this.Docs2,!1,u,t),this.SelectItemsByGroupList("3","\u53D1\u8D77\u5BFC\u822A\u521B\u5EFA\u5B50\u6D41\u7A0B(\u5BF9\u5F00\u59CB\u8282\u70B9\u6709\u6548)",this.Docs3,!1,u,t)}GenerSorts(){return l(this,null,function*(){return Promise.resolve([])})}Save_TextBox_X(u,t,D,m,n){return l(this,null,function*(){const A=D;if(A===""){alert("\u8BF7\u8F93\u5165\u6D41\u7A0B\u7F16\u53F7");return}const E=this.RequestVal("RefPKVal"),e=new c(A);yield e.Retrieve();const r=new b(E);yield r.Retrieve();const F=new S;if(F.MyPK=E+"_"+A+"_"+u,(yield F.IsExits())===!0){alert("\u5B50\u6D41\u7A0B\u5DF2\u7ECF\u5B58\u5728");return}if(F.FK_Flow=r.FK_Flow,F.FK_Node=E,F.SubFlowNo=e.No,F.SubFlowName=e.Name,F.SubFlowType=u,u==="0"&&F.SetPara("EnName","TS.WF.SFlow.SubFlowHand"),u==="1"&&F.SetPara("EnName","TS.WF.SFlow.SubFlowAuto"),u==="2"&&F.SetPara("EnName","TS.WF.SFlow.SubFlowYanXu"),u==="3"&&F.SetPara("EnName","TS.WF.SFlow.SubFlowGuide"),yield F.Insert(),u==="1"){const w=r.GetParaInt("SubFlowAutoNum",0);r.SetPara("SubFlowAutoNum",w+1),yield r.Update()}let i="";return u==="0"&&(i="/src/WF/Comm/En.vue?EnName=TS.WF.SFlow.SubFlowHand&PKVal="+F.MyPK),u==="1"&&(i="/src/WF/Comm/En.vue?EnName=TS.WF.SFlow.SubFlowAuto&PKVal="+F.MyPK),u==="2"&&(i="/src/WF/Comm/En.vue?EnName=TS.WF.SFlow.SubFlowYanXu&PKVal="+F.MyPK),u==="3"&&(i="/src/WF/Comm/En.vue?EnName=TS.WF.SFlow.SubFlowGuide&PKVal="+F.MyPK),"url@"+i})}}export{Z as GPN_NewSubFlow};
