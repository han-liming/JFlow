var D=Object.defineProperty;var o=(F,E,u)=>E in F?D(F,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):F[E]=u;var B=(F,E,u)=>(o(F,typeof E!="symbol"?E+"":E,u),u);import{PageBaseGroupEdit as i}from"./PageBaseGroupEdit-202e8e85.js";import{Flow as A}from"./Flow-6121039a.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./EntityNoName-d08126ae.js";import"./Entities-6a72b013.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./BSEntity-840a884b.js";class b extends i{constructor(){super("GPE_SyncRole");B(this,"Desc0",`
  #### \u5E2E\u52A9
  - \u6570\u636E\u540C\u6B65\uFF0C\u5C31\u662F\u5728\u6D41\u7A0B\u8FD0\u52A8\u8FC7\u7A0B\u8FC7\u7A0B\u4E2D\u5411\u4ED6\u7CFB\u7EDF\u8BFB\u5199\u6570\u636E.
  - \u9ED8\u8BA4\u4E3A\u4E0D\u540C\u6B65.
  - \u6839\u636E\u5E94\u7528\u7CFB\u7EDF\u7684\u9700\u6C42\uFF0C\u8BBE\u7F6E\u540C\u6B65\u65F6\u95F4\u4E0E\u540C\u6B65\u65B9\u5F0F.
  - \u4F7F\u7528ccbpm\u7684\u6570\u636E\u540C\u6B65\u529F\u80FD\u53EF\u80FD\u5B9A\u671F\u7684\u5411\u6307\u5B9A\u7684\u7CFB\u7EDF\u63A8\u9001\u6570\u636E.
  - \u63A8\u9001\u4EC0\u4E48\u6570\u636E\uFF0C\u5728\u540C\u6B65\u5185\u5BB9\u91CC\u5B9A\u4E49\u3002
  ##### \u5E94\u7528\u573A\u666F
  - \u8BF7\u5047\u6D41\u7A0B\u4E2D\uFF0C\u8BF7\u5047\u4FE1\u606F\u9700\u8981\u5199\u5165\u5230HR\u7CFB\u7EDF\u4E2D\u53BB.
  - \u8BA2\u5355\u6D41\u7A0B\u9700\u8981\u5199\u5165\u5230ERP\uFF0C\u8FDB\u9500\u5B58\u7CFB\u7EDF\u4E2D\u53BB.
  ##### \u5176\u4ED6
  - \u6D41\u7A0B\u90FD\u6709\u4E00\u4E2A\u4E1A\u52A1\u6570\u636E\u8868,\u9ED8\u8BA4\u540D\u5B57NDxxxRpt,\u6B64\u8868\u7684\u540D\u5B57\u53EF\u4EE5\u81EA\u5B9A\u4E49.
  - ccbpm\u5728\u8FD0\u884C\u8FC7\u7A0B\u4E2D\uFF0C\u90FD\u628A\u4E1A\u52A1\u6570\u636E\u5199\u5165\u5230\u8FD9\u4E2A\u8868\u91CC\uFF0C\u8BE5\u8868\u7684\u6570\u636E\u662F\u6D41\u7A0B\u6570\u636E+\u4E1A\u52A1\u6570\u636E\u7EC4\u6210.
  - \u4E8C\u6B21\u5F00\u53D1\u4EBA\u5458\u53EF\u4EE5\u901A\u8FC7\u8BBF\u95EE\u8BE5\u8868\u5B9A\u671F\u7684\u83B7\u53D6\u6570\u636E,\u4E5F\u53EF\u4EE5\u4F7F\u7528\u89E6\u53D1\u5668\u6765\u5B8C\u6210\u6570\u636E\u540C\u6B65.
`);B(this,"Desc1",`
  #### \u5E2E\u52A9
  - \u5F53\u7528\u6237\u6267\u884C\u53D1\u9001\u7684\u65F6\u5019\u6267\u884C\u6570\u636E\u540C\u6B65
  - \u4EFB\u4F55\u8282\u70B9\u6267\u884C\u53D1\u9001,\u90FD\u6267\u884C\u540C\u6B65.
  #### \u5176\u5B83
  - \u5982\u679C\u6570\u636E\u91CF\u592A\u5927,\u5C31\u4F1A\u5BFC\u81F4\u53D1\u9001\u53D8\u6162.
`);B(this,"Desc2",`
  #### \u5E2E\u52A9
  - \u5728\u6D41\u7A0B\u7684\u7ED3\u675F\u4E8B\u4EF6\u91CC\u6267\u884C\u540C\u6B65.
`);B(this,"Desc3",`
  #### \u5E2E\u52A9
  - \u5728\u6307\u5B9A\u7684\u8282\u70B9\u53D1\u9001\u7684\u4E8B\u4EF6\u91CC\u6267\u884C\u540C\u6B65.
  - \u8BF7\u9009\u62E9\u8981\u6267\u884C\u7684\u8282\u70B9.
`);this.PageTitle="\u540C\u6B65\u6570\u636E\u89C4\u5219"}Init(){this.entity=new A,this.KeyOfEn="DTSWay",this.AddGroup("A","\u6570\u636E\u540C\u6B65\u89C4\u5219"),this.Blank("0","\u4E0D\u6267\u884C\u540C\u6B65",this.Desc0),this.Blank("1","\u4EFB\u4F55\u8282\u70B9\u53D1\u9001\u540E\u90FD\u6267\u884C\u540C\u6B65",this.Desc1);const u=`SELECT NodeID as No,Name FROM WF_Node WHERE FK_Flow='${this.PKVal}' `;this.SelectItemsByList("3","\u6307\u5B9A\u7684\u8282\u70B9\u53D1\u9001\u540E",this.Desc3,!0,u,"DTSSpecNodes",""),this.Blank("2","\u6D41\u7A0B\u7ED3\u675F\u65F6",this.Desc2)}AfterSave(u,t){if(u==t)throw new Error("Method not implemented.")}BtnClick(u,t,C){if(u==t||u===C)throw new Error("Method not implemented.")}}export{b as GPE_SyncRole};
