var e=Object.defineProperty;var F=(t,E,u)=>E in t?e(t,E,{enumerable:!0,configurable:!0,writable:!0,value:u}):t[E]=u;var o=(t,E,u)=>(F(t,typeof E!="symbol"?E+"":E,u),u);import{PageBaseGroupEdit as m}from"./PageBaseGroupEdit-202e8e85.js";import{Node as B,NodeAttr as p}from"./Node-6b42ba5e.js";import"./Group-ab70f402.js";import"./EnumLab-d43291c1.js";import"./Page-34b19d96.js";import"./index-f4658ae7.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Help-be517e8f.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./EntityNodeID-d5ae71b1.js";import"./Entities-6a72b013.js";class v extends m{constructor(){super("GPE_CCWriteRole");o(this,"Desc0",`
  #### \u8BF4\u660E
  - \u663E\u793A\u5728\u6284\u9001\u5217\u8868.
  - \u6284\u9001\u662F\u4E00\u4E2A\u5355\u72EC\u529F\u80FD\u9875\u9762.
  #### \u5173\u4E8E\u5F85\u529E\u7684\u5206\u7C7B
  - \u5F85\u529E\u6709\u5982\u4E0B\u7C7B\u522B: \u53D1\u9001\u7684\u3001\u9000\u56DE\u7684\u3001\u6284\u9001\u7684\u3001\u79FB\u4EA4\u7684\u3001\u52A0\u7B7E\u7684
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/CCRole/Img/cc.png "\u5C4F\u5E55\u622A\u56FE")
 `);o(this,"Desc1",`
  #### \u8BF4\u660E
  - \u51FA\u73B0\u4E86\u6284\u9001\u7684\u5DE5\u4F5C\u8BA9\u5176\u4E0E\u5F85\u529E\u653E\u5728\u4E00\u8D77,\u5F85\u529E\u7406\u8868\u91CC\u4E5F\u53EF\u4EE5\u67E5\u770B\u6284\u9001\u4FE1\u606F.
  #### \u5173\u4E8E\u5F85\u529E\u7684\u5206\u7C7B
  - \u5F85\u529E\u6709\u5982\u4E0B\u7C7B\u522B: \u53D1\u9001\u7684\u3001\u9000\u56DE\u7684\u3001\u6284\u9001\u7684\u3001\u79FB\u4EA4\u7684\u3001\u52A0\u7B7E\u7684
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/resource/WF/Admin/AttrNode/CCRole/Img/todolist.png "\u5C4F\u5E55\u622A\u56FE")
 `);o(this,"Desc2",`
  #### \u8BF4\u660E
  - \u6284\u9001\u7684\u4FE1\u606F\u663E\u793A\u5728\u5F85\u529E\u4E0E\u6284\u9001\u5217\u8868\u90FD\u5B58\u5728.
  #### \u5173\u4E8E\u5F85\u529E\u7684\u5206\u7C7B
  - \u5F85\u529E\u6709\u5982\u4E0B\u7C7B\u522B: \u53D1\u9001\u7684\u3001\u9000\u56DE\u7684\u3001\u6284\u9001\u7684\u3001\u79FB\u4EA4\u7684\u3001\u52A0\u7B7E\u7684
  `);this.PageTitle="\u6284\u9001\u5199\u5165\u89C4\u5219"}Init(){this.entity=new B,this.KeyOfEn=p.CCWriteTo,this.AddGroup("A","+\u6284\u9001\u5199\u5165\u89C4\u5219"),this.Blank("0","\u5199\u5165\u6284\u9001\u5217\u8868",this.Desc0),this.Blank("1","\u5199\u5165\u5F85\u529E",this.Desc1),this.Blank("2","\u5199\u5165\u5F85\u529E+\u6284\u9001\u5217\u8868",this.Desc2)}AfterSave(u,r){if(u==r)throw new Error("Method not implemented.")}BtnClick(u,r,i){if(u==r||u===i)throw new Error("Method not implemented.")}}export{v as GPE_CCWriteRole};
