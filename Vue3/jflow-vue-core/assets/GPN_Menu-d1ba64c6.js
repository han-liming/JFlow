var L=Object.defineProperty;var W=(N,m,E)=>m in N?L(N,m,{enumerable:!0,configurable:!0,writable:!0,value:E}):N[m]=E;var s=(N,m,E)=>(W(N,typeof m!="symbol"?m+"":m,E),E);var f=(N,m,E)=>new Promise((a,t)=>{var C=c=>{try{u(E.next(c))}catch(h){t(h)}},A=c=>{try{u(E.throw(c))}catch(h){t(h)}},u=c=>c.done?a(c.value):Promise.resolve(c.value).then(C,A);u((E=E.apply(N,m)).next())});import{PageBaseGroupNew as V,GPNReturnObj as r,GPNReturnType as i}from"./PageBaseGroupNew-ee20c033.js";import{Module as v,Modules as k,ModuleAttr as H}from"./Module-dc2f8ce5.js";import{Menu as T}from"./Menu-fac205b5.js";import{cR as n,Y as l,$ as p}from"./index-f4658ae7.js";import{Func as O}from"./Func-8e307a2f.js";import{SFTable as R}from"./SFTable-d63f9fb4.js";import{a as K}from"./Emp-e0a70077.js";import{useClassFactoryLoader as D}from"./useClassFactoryLoader-072dd6cb.js";import g from"./DBAccess-d3bef90d.js";import{GenerListEn as $}from"./GenerListEn-0e99be8a.js";import{GloComm as S}from"./GloComm-7cfbdfd9.js";import{MapData as I}from"./MapData-4fa397be.js";import{Flow as q}from"./Flow-6121039a.js";import{DictDtlView as x}from"./DictDtlView-b2f88213.js";import{FlowAdm as U}from"./FlowAdm-28590a36.js";import{FlowDtlView as J}from"./FlowDtlView-cb68c3e2.js";import"./Group-ab70f402.js";import"./Page-34b19d96.js";import"./EnumLab-d43291c1.js";import"./EntityNoName-d08126ae.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Entities-6a72b013.js";import"./UAC-8e255d47.js";import"./Map-73575e6b.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./PCenter-7f795966.js";import"./EntityMyPK-e742fec8.js";import"./PowerCenter-f8ebe7c0.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";import"./Dept-342c50de.js";import"./EntityTree-333c163f.js";import"./assign-481cba08.js";import"./_createAssigner-77c8874c.js";import"./DeptEmp-745bc1a9.js";import"./DeptEmpStation-6b7abbad.js";import"./FrmAdm-54c9d6a8.js";import"./EnumLab-4f91f91c.js";import"./FrmTrack-10f0746d.js";import"./GL_VSTOFrm-38c6b173.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./MapDtl-dc3f1bee.js";import"./PageBaseGenerList-b0d946a7.js";class w extends V{constructor(){super("GPN_Menu");s(this,"Components",`
  #### \u5E2E\u52A9.
  1. xxxx
  2. xxxx
  #### \u6548\u679C\u56FE.
  
  `);s(this,"Search",`
  #### \u5E2E\u52A9.
  1. \u67E5\u8BE2\u5B9E\u4F53:\u5C31\u662F\u628AEntity\u5B9E\u4F53,\u653E\u5165\u67E5\u8BE2\u7EC4\u4EF6/Comm/Search.vue \u4E0A\u5B9E\u73B0\u6570\u636E\u7684\u589E\u5220\u6539\u67E5.
  1. \u53EF\u4EE5\u5BF9\u5B9E\u4F53\u8FDB\u884C\uFF0C\u628A\u5173\u952E\u5B57\uFF0C\u65E5\u671F\u65F6\u95F4\u8303\u56F4\uFF0C\u679A\u4E3E\u5916\u952E\u8FDB\u884C\u67E5\u8BE2.
  #### \u6548\u679C\u56FE.
  1. xxxx
  1. xxxx
  #### \u6B65\u9AA41. \u521B\u5EFA\u5B9E\u4F53.
  1. \u521B\u5EFA\u4E00\u4E2A\u5B9E\u4F53\u5B50\u7C7B,\u6839\u636E\u5B9E\u4F53\u7279\u5F81\u786E\u5B9A\u7EE7\u627F\u8DEF\u5F84, \u8BF7\u53C2\u8003 BP.Demo.* /src/bp/demo/*.*
  1. \u6CE8\u610F\u547D\u540D\u7A7A\u95F4\u4E0D\u8981\u4E0E\u7CFB\u7EDF\u7684\u547D\u540D\u7A7A\u95F4\u91CD\u590D.
  1. \u67E5\u8BE2\u6761\u4EF6,\u9690\u85CF\u6761\u4EF6\u7684\u8BBE\u7F6E\u4E0Ebp\u67B6\u6784\u7684\u4E00\u6837.
  #### \u6B65\u9AA42. \u6CE8\u518C\u5230ClassFactory.
  1. \u628A\u6539\u5B9E\u4F53\u6CE8\u518C\u5230 /src/bp/da/ClassFactory  \u7C7B\u91CC\u9762.
  1. \u6CE8\u610FEns, En \u90FD\u8981\u6CE8\u518C.
  1. \u8BF7\u53C2\u8003:BP.Demo.Student \u7684\u5199\u6CD5.
  #### \u6B65\u9AA43. \u6309\u7167\u5411\u5BFC\u521B\u5EFA\u83DC\u5355.
  1. \u65B0\u5EFA\u83DC\u5355\uFF0C\u9009\u62E9\u6A21\u5757\uFF0C\u6309\u7167\u5411\u5BFC\u521B\u5EFA\u83DC\u5355.
  2. \u6D4B\u8BD5.
  `);s(this,"Search_Ens",`
    #### \u5E2E\u52A9.
    1. \u9009\u62E9\u4E00\u4E2A\u5B9E\u4F53\u7C7B.
    #### \u4E3A\u4F1A\u6CA1\u6709\u6709\u627E\u5230?
    1. \u6CA1\u6709\u6CE8\u518C\u5230 /src/bp/da/ClassFactory.ts\u91CC\u9762\u53BB.
    2. \u6CE8\u518C\u8FDB\u53BB\u4E86\uFF0C\u4F7F\u7528\u4E86\u7CFB\u7EDF\u547D\u540D\u7A7A\u95F4.
    `);s(this,"Search_Ens_Paras",`
  #### \u5E2E\u52A9.
  1. \u8F93\u5165\u53C2\u6570,\u53EF\u4EE5\u4F5C\u4E3A\u6761\u4EF6\u7684\u53C2\u6570,\u6539\u53C2\u6570\u53EF\u4EE5\u4E3A\u7A7A.
  1. \u6BD4\u5982: &FK_Dept=@WebUser.DeptNo&SortNo=xxxxx 
  1. \u66F4\u591A\u7684\u53C2\u6570\u8BF7\u53C2\u8003Search.vue\u7684\u8BBE\u8BA1\u8BF4\u660E.
  `);s(this,"GPN",`
  #### \u5E2E\u52A9.
  1. \u6682\u65E0
  #### \u6548\u679C\u56FE.
  1. \u6682\u65E0
  `);s(this,"GPN_Ens",`
    #### \u5E2E\u52A9.
    1. \u6682\u65E0
    #### \u6548\u679C\u56FE.
    1. \u6682\u65E0
    `);s(this,"GPN_Ens_Paras",`
  #### \u5E2E\u52A9.
  1. \u6682\u65E0
  #### \u6548\u679C\u56FE.
  1. \u6682\u65E0
  `);s(this,"TreeEns",`
  #### \u5E2E\u52A9.
  1. \u6682\u65E0
  #### \u6548\u679C\u56FE.
  1. \u6682\u65E0
  `);s(this,"TreeEns_Ens",`
  #### \u5E2E\u52A9.
  1. \u6682\u65E0
  #### \u6548\u679C\u56FE.
  1. \u6682\u65E0
    `);s(this,"Desc100","\u6682\u672A\u5F00\u653E");s(this,"DocSelfUrl",`
#### \u5E2E\u52A9
1. \u53EF\u4EE5\u4F7F\u7528\u76F8\u5BF9\u8DEF\u5F84\uFF0C\u4E5F\u53EF\u4EE5\u4F7F\u7528\u7EDD\u5BF9\u8DEF\u5F84\u3002
1. \u7528\u6237\u8F93\u5165\u7684Url:  http://ccbpm.cn/MyUrl.htm
1. \u6253\u5F00\u7684Url : http://ccbpm.cn/MyUrl.htm?UserNo=xxxx&Token=xxxx\u3002
1. SID\u5C31\u7C7B\u4F3C\u4E8Etoken, UserNo\u5C31\u662F\u5F53\u524D\u767B\u5F55\u7528\u6237\u7684\u7F16\u53F7\u3002


#### \u81EA\u5B9A\u4E49URL\u83DC\u5355 For H5
1. \u83DC\u5355\u8FDE\u63A5\uFF1A http://ccbpm.cn/MyUrl.htm   
1. \u83DC\u5355\u8FDE\u63A5\uFF1A http://ccbpm.cn/MyUrl.htm  
1. H5\u94FE\u63A5\uFF1A /WF/Comm/Search.htm?EnsName=TS.ZS.Projcets \u67E5\u8BE2
1. H5\u94FE\u63A5\uFF1A /WF/Comm/Group.htm?EnsName=TS.ZS.Projcets  \u5206\u6790
1. H5\u94FE\u63A5\uFF1A /WF/MyFlow.htm?FK_Flow=001 \u53D1\u8D77\u6307\u5B9A\u7684\u6D41\u7A0B. 

#### \u81EA\u5B9A\u4E49URL\u83DC\u5355 For Vue3.x

- <strong>vue3\u83DC\u5355\u652F\u6301<span style="color:red">\u4E24\u79CD</span>\u914D\u7F6E</strong>
- <span style="color:red">\u914D\u7F6E\u540E\u9700\u8981\u5237\u65B0\u9875\u9762</span>
- <strong>\u65B9\u5F0F1\uFF1A\u914D\u7F6E\u6253\u5F00\u7684vue\u6587\u4EF6\u548C\u53C2\u6570</strong>

  - \u6B64\u65B9\u5F0F\u672C\u8D28\u5C31\u662F\u6784\u5EFAvue\u7684\u8DEF\u7531\uFF0C \u9700\u8981\u586B\u5199\u6307\u5411\u6587\u4EF6\u548C\u94FE\u63A5
  - \u6307\u5411\u6587\u4EF6\u662F\u7CFB\u7EDF\u5B58\u5728\u7684vue\u6587\u4EF6\uFF0C \u8868\u793A\u4E0B\u65B9\u94FE\u63A5\u5B9E\u9645\u7531\u8FD9\u4E2A\u6587\u4EF6\u6765\u5904\u7406
    - /src/WF/Comm/Search.vue
  - URL\u8868\u793A\uFF1A \u901A\u8FC7\u54EA\u4E2A\u8DEF\u5F84\u8BBF\u95EE\u4E0A\u8FF0\u7684vue\u6587\u4EF6 \u53EF\u81EA\u5B9A\u4E49\uFF0C
    - \u4F8B\u5982\uFF1A\u9700\u8981\u901A\u8FC7\u4E0A\u9762\u7684Search.vue \u5B9E\u73B0\u4E00\u4E2A\u7CFB\u7EDF\u5B57\u5178\u7BA1\u7406\u7684\u94FE\u63A5 
    - \u9700\u8981\u81EA\u5B9A\u4E49\u4E2A\u8DEF\u5F84 /DictManage + \u53C2\u6570EnName=TS.FrmUI.SysEnumMain
    - \u5B8C\u6574\u8DEF\u5F84\u4E3A /DictManage?EnName=TS.FrmUI.SysEnumMain
    

- <strong>\u65B9\u5F0F2\uFF1A \u914D\u7F6Eurl\u94FE\u63A5</strong>
  - <span style="color:red">\u5982\u679C\u914D\u7F6Eurl\uFF0C\u6307\u5411\u6587\u4EF6\u586B\u5199\u4E00\u4E2A\u7A7A\u683C\u5373\u53EF</span>
  - url\u94FE\u63A5\u53EF\u4EE5\u914D\u7F6E\u5916\u94FE\uFF0C\u4EE5http:// \u6216\u8005 https:// \u5F00\u5934
    \u5916\u94FE\u53EF\u4EE5\u4E3A\u4EFB\u610F\u4E92\u8054\u7F51\u5730\u5740 - \u4F8B\u5982ccflow\u5B98\u7F51: http://ccflow.org
  - url\u94FE\u63A5\u4E5F\u53EF\u4EE5\u914D\u7F6E\u7CFB\u7EDF\u5185\u90E8\u7684\u94FE\u63A5\uFF0C \u4EE5self://\u5F00\u5934
    \u7CFB\u7EDF\u5185\u90E8\u94FE\u63A5\u53EF\u4EE5\u914D\u7F6E\u7CFB\u7EDF\u5DE5\u4F5C\u5730\u5740 - \u4F8B\u5982\u6253\u5F00\u7F16\u53F7001\u7684\u6D41\u7A0B: self://WF/Flow?FK_Flow=001 

- \u5E38\u7528vue\u6587\u4EF6\u5730\u5740\u5982\u4E0B\uFF1A
  - /src/WF/Comm/Search.vue \u67E5\u8BE2
  - /src/WF/Comm/Group.vue  \u5206\u7EC4

- \u5E38\u7528\u7684url\u5982\u4E0B\uFF1A
  - self://WF/MyFlow?FK_Flow=xxx \u53D1\u8D77id\u4E3Axxx\u7684\u6D41\u7A0B
        
#### \u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/SelfUrl.png "\u5C4F\u5E55\u622A\u56FE.png")
  `);s(this,"DocAloneflow",`
####  \u521B\u5EFA\u72EC\u7ACB\u8FD0\u884C\u7684\u6D41\u7A0B 
  - \u521B\u5EFA\u6D41\u7A0B\u540E\uFF0C\u7CFB\u7EDF\u81EA\u52A8\u5BF9\u8BE5\u6D41\u7A0B\u7684\u76F8\u5173\u64CD\u4F5C\u521B\u5EFA\u5230\u83DC\u5355\u4E0A\u53BB\u3002 
  - \u6BD4\u5982\uFF1A\u53D1\u8D77\u6D41\u7A0B\uFF0C\u6D41\u7A0B\u67E5\u8BE2\u3001\u5206\u6790\u3002 
 
  `);s(this,"DocRptwhite",`
####  \u4FE1\u606F\u7A97/\u5927\u5C4F(\u767D\u8272\u98CE\u683C) 
  - \u4FE1\u606F\u7A97\u652F\u6301\u6570\u636E\u7684\u56FE\u5F62\u5C55\u793A\uFF0C\u6BD4\u5982\u6298\u7EBF\u56FE\u3001\u67F1\u72B6\u56FE\u3001\u997C\u56FE\u3002
  - \u652F\u6301\u53D8\u91CF\u6587\u672C\u8F93\u51FA\uFF0C\u652F\u6301\u81EA\u5B9A\u4E49HTLM\u4EE3\u7801\u7684\u8F93\u51FA\u3002
  - \u662F\u591A\u79CD\u5F62\u5F0F\u7684\u7EDF\u8BA1\u5206\u6790\u5C55\u793A\u529F\u80FD\u3002
#### \u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Windows.png "\u5C4F\u5E55\u622A\u56FE.png")

  `);s(this,"DocRptblue",`
  
####  *\u4FE1\u606F\u7A97/\u5927\u5C4F(\u84DD\u8272\u98CE\u683C) 
  
  - \u4FE1\u606F\u7A97\u652F\u6301\u6570\u636E\u7684\u56FE\u5F62\u5C55\u793A\uFF0C\u6BD4\u5982\u6298\u7EBF\u56FE\u3001\u67F1\u72B6\u56FE\u3001\u997C\u56FE\u3002
  - \u652F\u6301\u53D8\u91CF\u6587\u672C\u8F93\u51FA\uFF0C\u652F\u6301\u81EA\u5B9A\u4E49HTLM\u4EE3\u7801\u7684\u8F93\u51FA\u3002
  - \u662F\u591A\u79CD\u5F62\u5F0F\u7684\u7EDF\u8BA1\u5206\u6790\u5C55\u793A\u529F\u80FD\u3002
  #### \u6548\u679C\u56FE
![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Windows.png "\u5C4F\u5E55\u622A\u56FE.png")
  
  `);s(this,"DocTab",`
  
  #### Tabs\u9875\u9762\u5BB9\u5668 
   -  \u5B9A\u4E49\uFF1A\u6BCF\u4E2Atab\u4E0B\u9762\u90FD\u6709\u4E00\u4E2A\u81EA\u5B9A\u4E49\u7684url\u3002
  #### \u6548\u679C\u56FE
   - ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Tabs.png "\u5C4F\u5E55\u622A\u56FE.png")
   `);s(this,"DocRpt3d",`
  
  #### \u4E09\u7EF4\u62A5\u8868 
     
   - \u5B9A\u4E49\uFF1A\u4E09\u7EF4\u62A5\u8868\u662F\u9700\u8981\u6307\u5B9A\u4E09\u4E2A\u6570\u636E\u6E90\uFF0C\u901A\u8FC7\u4E09\u7EF4\u5173\u7CFB\u663E\u793A\u6570\u636E\u3002
  #### \u4E8B\u4F8B
    
   - \u6570\u636E\u6E90\uFF1ASELECT FK_Flow,RunModel, FWCSta ,count(*) AS Num FROM wf_node GROUP BY FK_Flow,RunModel,FWCSta
   - \u7EF4\u5EA61\uFF1ASELECT No,Name FROM WF_Flow ;
   - \u7EF4\u5EA62\uFF1ASELECT IntKey AS No, Lab as Name FROM sys_enum WHERE EnumKey='RunModel';
   - \u7EF4\u5EA63\uFF1ASELECT IntKey AS No, Lab as Name FROM sys_enum WHERE EnumKey='FWCSta';
   
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Rpt3D.png "\u5C4F\u5E55\u622A\u56FE.png")
   
    `);s(this,"DocDict",`
  
  #### \u521B\u5EFA\u5B9E\u4F53 
      
   - \u5B9A\u4E49: \u5B9E\u4F53\u4E5F\u53EB\u53F0\u8D26\u6216\u8005\u5217\u8868\uFF0C\u5C31\u662F\u5B58\u50A8\u7BA1\u7406\u7684\u5BF9\u8C61\uFF0C\u5177\u6709\u7F16\u53F7\u4E0E\u540D\u79F0\u4E24\u4E2A\u5FC5\u8981\u7684\u5C5E\u6027
   - \u6BD4\u5982: \u5B66\u751F\u53F0\u8D26\u3001\u56FA\u5B9A\u8D44\u4EA7\u53F0\u8D26\u3001\u5458\u5DE5\u53F0\u5E10
   - \u8BE5\u529F\u80FD\uFF0C\u63D0\u4F9B\u4E86\u5BF9\u5B9E\u4F53\u7684\u589E\u5220\u6539\u67E5\uFF0C\u5BF9\u5217\u8868\u7684\u64CD\u4F5C\uFF0C\u5BF9\u5355\u884C\u8BB0\u5F55\u7684\u64CD\u4F5C\u3002

  #### \u6548\u679C\u56FE1
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/DictEn.png "\u5C4F\u5E55\u622A\u56FE.png")
  #### \u6548\u679C\u56FE2
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Dict.png "\u5C4F\u5E55\u622A\u56FE.png")
     `);s(this,"DBList",`
  
  #### \u6570\u636E\u6E90\u5B9E\u4F53 
   -  \u5B9A\u4E49\uFF1A\u6570\u636E\u6E90\u5B9E\u4F53\u5C31\u662F\u89C6\u56FE\uFF0C\u4E0D\u80FD\u5BF9\u6570\u636E\u6267\u884C\uFF0C\u589E\u52A0\uFF0C\u5220\u9664\uFF0C\u4FEE\u6539\u64CD\u4F5C\u3002
   -  \u5177\u5907\u5B9E\u4F53\u7684\u5176\u4ED6\u7684\u529F\u80FD\uFF0C\u53EF\u4EE5\u5F53\u4F5C\u67E5\u8BE2\u6240\u7528\u3002
   `);s(this,"DictCopy",`
   #### \u590D\u5236\u5B9E\u4F53
   - \u5B9A\u4E49\uFF1A\u81EA\u52A8\u542F\u52A8\u5DE5\u4F5C\u6D41\u7A0B\uFF0C\u4E00\u4E2A\u6D41\u7A0B\u7684\u5F00\u59CB\u8282\u70B9\u7684\u586B\u5199\u4E0E\u53D1\u8D77\u662F\u5728\u7279\u5B9A\u89C4\u5219\u7684\u8BBE\u7F6E\u4E0B\u81EA\u52A8\u53D1\u8D77\u7684\u6D41\u7A0B\u3002
   - \u89E3\u91CA\uFF1A\u901A\u5E38\u6A21\u5F0F\u4E0B\u7684\u6D41\u7A0B\u542F\u52A8\u662F\u624B\u5DE5\u7684\u542F\u52A8\uFF0C\u5C31\u662F\u7528\u6237\u4ECE\u4E00\u4E2A\u53D1\u8D77\u5217\u8868\uFF0C\u70B9\u51FB\u6D41\u7A0B\u540D\u5B57\uFF0C\u5C31\u542F\u52A8\u4E86\u8BE5\u6D41\u7A0B\u3002\u4F46\u662F\u6709\u7684\u65F6\u5019\uFF0C\u662F\u7CFB\u7EDF\u81EA\u52A8\u53D1\u8D77\u8BE5\u6D41\u7A0B\u3002
   - \u5E94\u7528\u573A\u666F\uFF1A
      1 \u5468\u4F8B\u4F1A\u6D41\u7A0B\uFF0C\u7528\u6237\u5E0C\u671B\u6BCF\u4E2A\u5468\u90FD\u8981\u542F\u52A8\u4F8B\u4F1A\u901A\u77E5\u6D41\u7A0B\u8FD9\u4E2A\u542F\u52A8\u662F\u8BA9\u7CFB\u7EDF\u81EA\u52A8\u53D1\u8D77\u800C\u975E\u4EBA\u5DE5\u53D1\u8D77\u3002
   `);s(this,"Bill",this.DictCopy);s(this,"DictRef","");s(this,"DictTable",`
  
  #### \u5B57\u5178\u8868 
   -  \u5B9A\u4E49\uFF1A\u53EA\u5177\u6709\u7F16\u53F7,\u540D\u79F0\u4E24\u4E2A\u5C5E\u6027\u7684\u5B57\u5178\u5B9E\u4F53\uFF0C\u6BD4\u5982\uFF1A\u89D2\u8272\u7C7B\u578B\u3001\u7CFB\u7EDF\u7C7B\u522B\u3001\u7A0E\u79CD\u3001\u7A0E\u76EE\u3001\u7701\u4EFD\u3001\u7247\u533A
 
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/DictTable.png "\u5C4F\u5E55\u622A\u56FE.png")
   
   `);s(this,"DictTableTree",`
  
  #### \u6811\u7ED3\u6784\u5B57\u5178\u8868 
   -  \u5B9A\u4E49\uFF1A\u53EA\u5177\u6709\u7F16\u53F7,\u540D\u79F0,\u7236\u8282\u70B9\uFF0C\u4E09\u4E2A\u5C5E\u6027\u7684\u5B57\u5178\u5B9E\u4F53\uFF0C\u6BD4\u5982\uFF1A\u90E8\u95E8
   `);s(this,"Task",`
  #### \u4EFB\u52A1 
   -  \u5B9A\u4E49\uFF1A\u8BB0\u5F55\u4EFB\u52A1\u53C2\u4E0E\u4EBA\uFF0C\u4EFB\u52A1\u65F6\u95F4\uFF0C\u7D27\u6025\u7A0B\u5EA6\u7B49\u4E8B\u4EF6\u7684\u8BB0\u4E8B\u672C\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Task.png "\u5C4F\u5E55\u622A\u56FE.png")
   `);s(this,"Info",`
  
  #### \u4FE1\u606F\u53D1\u5E03 
    
   -  \u5B9A\u4E49\uFF1A\u53EF\u4EE5\u7F16\u8F91\u53D1\u5E03\u4FE1\u606F\uFF0C\u4FE1\u606F\u4EE5\u5217\u8868\u7684\u5F62\u5F0F\u5C55\u793A\u5728\u9875\u9762\u4E0A\u3002 
   -  \u4E5F\u53EF\u4EE5\u5B9A\u4E49\u4FE1\u606F\u53D1\u5E03\u7C7B\u5F62\uFF0C\u6BD4\u5982\uFF1A\u4F1A\u8BAE\u8BB0\u8981\uFF0C\u5DE5\u4F5C\u8FDB\u5EA6\uFF0C\u6587\u4EF6\u4F20\u8FBE\u7B49 
  
  #### \u4FE1\u606F\u5217\u8868\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Info2.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u4FE1\u606F\u7F16\u8F91
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Info.png "\u5C4F\u5E55\u622A\u56FE.png") 
  #### \u4FE1\u606F\u7C7B\u578B
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Info3.png "\u5C4F\u5E55\u622A\u56FE.png") 
   
   `);s(this,"Calendar",`
   
     
    -  \u5B9A\u4E49\uFF1A\u53EF\u4EE5\u5728\u65E5\u5386\u4E0A\u7F16\u8F91\u8BB0\u4E8B\uFF0C\u5DE5\u4F5C\u63D0\u9192\u7B49\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Calendar.png "\u5C4F\u5E55\u622A\u56FE.png")
    
    `);s(this,"Notepad",`
    
  #### \u8BB0\u4E8B\u672C 
      
     -  \u5B9A\u4E49\uFF1A\u662F\u4E00\u6B3E\u5728\u7EBF\u8BB0\u4E8B\u672C\uFF0C\u53EF\u4EE5\u8BB0\u5F55\u751F\u6D3B\uFF0C\u5DE5\u4F5C\uFF0C\u4E8B\u4EF6\u3002
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/Notepad.png "\u5C4F\u5E55\u622A\u56FE.png") 
 
     `);s(this,"KnowledgeManagement",`
   
  #### \u77E5\u8BC6\u5E93 
     
   -  \u5B9A\u4E49\uFF1A\u5404\u79CD\u77E5\u8BC6\u7684\u96C6\u5408\uFF0C\u65B9\u4FBF\u4E86\u89E3\u548C\u67E5\u8BE2

  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/KnowledgeManagement.png "\u5C4F\u5E55\u622A\u56FE.png") 
  
 
    
    `);s(this,"WorkRec",`
   
  #### \u5DE5\u4F5C\u65E5\u5FD7 
     
   -  \u5B9A\u4E49\uFF1A\u8BB0\u5F55\u5DE5\u4F5C\u7684\u65E5\u5FD7
  #### \u6548\u679C\u56FE
  ![\u8F93\u5165\u56FE\u7247\u8BF4\u660E](/src/CCFast/GPM/CCMenu/Img/WorkRec.png "\u5C4F\u5E55\u622A\u56FE.png") 
   
    `);this.PageTitle="\u65B0\u5EFA\u83DC\u5355",this.ForEntityClassID="TS.GPM.Menu"}Init(){return f(this,null,function*(){this.AddGroup("B","\u5B9E\u4F53\u5355\u636E"),this.TextBox2_NameNo(n.Dict,"\u521B\u5EFA\u5B9E\u4F53",this.DocDict,"Dict_","\u7F16\u53F7","\u540D\u79F0","\u5B66\u751F\u53F0\u8D26"),this.TextBox1_Name("GenerListEn","GLEn\u901A\u7528\u5217\u8868\u7EC4\u4EF6",this.DBList,"\u8F93\u5165\u540D\u79F0","\u6211\u7684\u5217\u8868","\u8BF7\u8F93\u5165\u5217\u8868\u540D\u79F0."),this.AddIcon("GenerListEn","icon-wallet"),this.AddGroup("Z","\u89C6\u56FE"),this.TextBox2_NameNo("View","\u81EA\u5B9A\u4E49",this.HelpTodo,"View_","\u8F93\u5165ID","\u8F93\u5165\u540D\u79F0","\u6211\u7684\u89C6\u56FE"),this.TextBox3_NameNoNote("DictDtlView","\u5B9E\u4F53\u4ECE\u8868",this.HelpTodo,"ViewFrmDtl_","\u8BF7\u8F93\u5165\u89C6\u56FEID","\u8F93\u5165\u89C6\u56FE\u540D\u79F0","\u8BF7\u8F93\u5165\u8868\u5355ID","\u5B9E\u4F53\u4ECE\u8868\u89C6\u56FE"),this.SelectItemsByList("DictDtlView.SelectFrmDtl","\u9009\u62E9\u4ECE\u8868",this.HelpTodo,!1,()=>`SELECT No, Name FROM Sys_MapDtl WHERE FK_MapData='${this.RequestVal("tb3","DictDtlView")}' `),this.AddIcon("DictDtlView","icon-grid"),this.TextBox3_NameNoNote("FlowDtlView","\u6D41\u7A0B\u4ECE\u8868",this.HelpTodo,"ViewFrmDtl_","\u8BF7\u8F93\u5165\u89C6\u56FEID","\u8F93\u5165\u89C6\u56FE\u540D\u79F0","\u8F93\u5165\u6D41\u7A0B\u7F16\u53F7","\u6D41\u7A0B\u4ECE\u8868\u89C6\u56FE"),this.SelectItemsByList("FlowDtlView.SelectFlowDtl","\u9009\u62E9\u4ECE\u8868",this.HelpTodo,!1,()=>`SELECT No, Name FROM Sys_MapDtl WHERE FK_MapData='${"ND"+Number.parseInt(this.RequestVal("tb3","FlowDtlView"))+"01"}' `),this.AddIcon("FlowDtlView","icon-grid"),this.AddGroup("E","\u7CFB\u7EDF\u5B9E\u4F53"),this.AddBlank("Entity","Entity\u6570\u636E\u5B9E\u4F53",this.Search),this.SelectItemsByList("Entity.EnName","\u9009\u62E9\u5B9E\u4F53",this.Search_Ens,!1,yield w.GenerEnsList("Entity")),this.SelectItemsByList("Entity.EnName.componentName","\u7EC4\u4EF6\u7C7B\u578B",this.Search_Ens,!1,this.GenerEntitySort()),this.TextBox2_NameNo("Entity.EnName.componentName.Paras","\u8DEF\u5F84\u53CA\u53C2\u6570",this.Search_Ens_Paras,"","\u53C2\u6570","URL\u5B9A\u4E49",""),this.AddBlank("GPN","GPN\u65B0\u5EFA\u7EC4\u4EF6",this.GPN),this.SelectItemsByList("GPN.Ens","\u9009\u62E9\u5B9E\u4F53",this.GPN_Ens,!1,yield w.GenerEnsList("GPN")),this.TextBox1_Name("GPN.Ens.Paras","\u53EF\u9009\u53C2\u6570",this.GPN_Ens_Paras,"\u53C2\u6570","&1=1"),this.AddBlank("TreeEns","TreeEns\u6811\u5E72\u53F6\u5B50\u7EC4\u4EF6",this.TreeEns),this.SelectItemsByList("TreeEns.Ens","\u9009\u62E9\u5B9E\u4F53",this.TreeEns_Ens,!1,yield w.GenerEnsList("TreeEns")),this.TextBox1_Name("TreeEns.Ens.Paras","\u53EF\u9009\u53C2\u6570",this.Search_Ens,"\u53C2\u6570","&1=1"),this.AddBlank("GL","GL\u901A\u7528\u5217\u8868\u7EC4\u4EF6",this.TreeEns),this.SelectItemsByList("GL.Ens","\u9009\u62E9\u5B9E\u4F53",this.TreeEns_Ens,!1,yield w.GenerEnsList("GL")),this.TextBox1_Name("GL.Ens.Paras","\u53EF\u9009\u53C2\u6570",this.Search_Ens,"\u53C2\u6570","&1=1"),this.AddBlank("PG","PG\u5B9E\u4F53\u5206\u7EC4\u5C55\u793A\u7EC4\u4EF6",this.TreeEns),this.SelectItemsByList("PG.Ens","\u9009\u62E9\u5B9E\u4F53",this.TreeEns_Ens,!1,yield w.GenerEnsList("PG")),this.TextBox1_Name("PG.Ens.Paras","\u53EF\u9009\u53C2\u6570",this.Search_Ens,"\u53C2\u6570","&1=1"),this.AddGroup("A","\u901A\u7528\u529F\u80FD"),this.TextBox3_NameNoNote(n.SelfUrl,"\u81EA\u5B9A\u4E49URL\u83DC\u5355",this.DocSelfUrl,null,"\u6307\u5411\u6587\u4EF6","\u94FE\u63A5\u6807\u7B7E","URL\u5730\u5740","\u6211\u7684\u94FE\u63A5"),this.TextBox1_Name(n.RptWhite,"\u4FE1\u606F\u7A97/\u5927\u5C4F(\u767D\u8272\u98CE\u683C)",this.DocRptwhite,"\u9875\u9762\u540D\u79F0","\u7EDF\u8BA1\u5206\u6790"),this.TextBox1_Name(n.Tabs,"Tabs\u9875\u9762\u5BB9\u5668",this.DocTab,"\u540D\u79F0","\u6211\u7684tab\u5BB9\u5668\u9875"),this.TextBox1_Name(n.Rpt3D,"\u4E09\u7EF4\u62A5\u8868",this.DocRpt3d,"\u540D\u79F0","\u4E09\u7EF4\u62A5\u8868"),this.AddGroup("F","\u9875\u9762\u5F15\u7528"),this.SelectItemsByList("LinkFlow","\u6D41\u7A0B\u529F\u80FD",this.HelpTodo,!1,this.GenerFlowFunc()),this.TextBox1_Name("LinkFlow.FlowNo","\u8F93\u5165\u6D41\u7A0B\u7F16\u53F7",this.HelpTodo,"\u6D41\u7A0B\u7F16\u53F7","","\u5728\u6D41\u7A0B\u8BBE\u8BA1\u5668,\u6D41\u7A0B\u5C5E\u6027\u91CC\u67E5\u770B\u6D41\u7A0B\u7F16\u53F7."),this.SelectItemsByList("DictLink","\u5B9E\u4F53\u529F\u80FD",this.HelpTodo,!1,this.GenerDictFunc()),this.TextBox1_Name("DictLink.DictID","\u8F93\u5165\u5B9E\u4F53ID",this.HelpTodo,"\u5B9E\u4F53ID","","\u5728\u8868\u5355\u8BBE\u8BA1\u5668,\u5B9E\u4F53\u5C5E\u6027\u91CC\u67E5\u770B\u5B9E\u4F53ID."),this.AddGroup("D","OA\u5E94\u7528"),this.TextBox1_Name(n.Info,"\u4FE1\u606F\u53D1\u5E03",this.Info,"\u540D\u79F0","\u4FE1\u606F\u53D1\u5E03"),this.TextBox1_Name(n.Notepad,"\u8BB0\u4E8B\u672C",this.Notepad,"\u540D\u79F0","\u8BB0\u4E8B\u672C"),this.TextBox1_Name(n.KnowledgeManagement,"\u77E5\u8BC6\u5E93",this.KnowledgeManagement,"\u540D\u79F0","\u77E5\u8BC6\u5E93"),this.TextBox1_Name(n.WorkRec,"\u5DE5\u4F5C\u65E5\u5FD7",this.WorkRec,"\u540D\u79F0","\u5DE5\u4F5C\u65E5\u5FD7"),this.AddIcon("icon-link","book-open"),this.AddIcon("icon-heart","Entity"),this.AddIcon("icon-docs","GPN"),this.AddIcon("icon-control-pause","TreeEns"),this.AddIcon("icon-cup","GL"),this.AddIcon("icon-layers","PG"),this.AddIcon("icon-link","SelfUrl"),this.AddIcon("icon-doc","RptWhite"),this.AddIcon("icon-drop","Tabs"),this.AddIcon("icon-docs","Rpt3D"),this.AddIcon("icon-docs","Info"),this.AddIcon("icon-doc","Notepad"),this.AddIcon("icon-layers","KnowledgeManagement"),this.AddIcon("icon-film","WorkRec")})}GenerDictFunc(){return JSON.stringify([{No:"Home",Name:"\u5B9E\u4F53\u4E3B\u9875:\u901A\u8FC7En\u7EC4\u4EF6\u94FE\u63A5\u5230\u5217\u8868\u3001\u5206\u6790\u3001\u62A5\u8868\u3001\u5927\u5C4F\u94FE\u63A5."},{No:"Search",Name:"\u5217\u8868:\u94FE\u63A5\u5230\u6D41\u7A0B\u5B9E\u4F8B\u5206\u6790."},{No:"Group",Name:"\u5206\u6790:\u5206\u7EC4\u5206\u6790\u6570\u636E."},{No:"Rpt",Name:"\u62A5\u8868:2\u7EF4,3\u7EF4\u62A5\u8868"},{No:"BS",Name:"\u5927\u5C4F:BS\u53EF\u4EE5\u5B9A\u5236\u5316\u5206\u6790\u5185\u5BB9."}])}GenerFlowFunc(){return JSON.stringify([{No:"FlowSearch",Name:"\u67E5\u8BE2Search,\u94FE\u63A5\u5230\u6D41\u7A0B\u5B9E\u4F8B\u67E5\u8BE2"},{No:"FlowGroup",Name:"\u5206\u6790Group:\u94FE\u63A5\u5230\u6D41\u7A0B\u5B9E\u4F8B\u5206\u6790."},{No:"Start",Name:"\u53D1\u8D77Start:\u53D1\u8D77\u6307\u5B9A\u7684\u6D41\u7A0B."},{No:"Todolist",Name:"\u5F85\u529ETodolist:\u67E5\u770B\u6307\u5B9A\u6D41\u7A0B\u7684\u5F85\u529E."},{No:"Runing",Name:"\u5728\u9014Runing:\u67E5\u770B\u6307\u5B9A\u6D41\u7A0B\u7684\u5728\u9014."},{No:"Home",Name:"\u6D41\u7A0B\u4E3B\u9875Home:\u67E5\u770B\u6D41\u7A0B\u4E3B\u9875."}])}GenerEntitySort(){return JSON.stringify([{No:"Search",Name:"\u67E5\u8BE2\u7EC4\u4EF6Search"},{No:"Batch",Name:"\u6279\u5904\u7406\u7EC4\u4EF6Batch"},{No:"Ens",Name:"\u6279\u91CF\u4FEE\u6539\u7EC4\u4EF6Ens"}])}get SystemNo(){return this.params.SystemNo||this.RequestVal("SystemNo")||""}GenerSorts(E){return f(this,null,function*(){if(E==="xxxxx")throw new Error("");let a=this.SystemNo;if(a==""||a==null){const C=this.RequestVal("RefPKVal"),A=new v;A.No=C,(yield A.RetrieveFromDBSources())==0?a=this.params.RefPKVal:a=A.SystemNo}const t=new k;return yield t.Retrieve(H.SystemNo,a,"Idx"),t})}static GenerEnsList(E){return f(this,null,function*(){if(E==="GPN")return yield(yield D("ClassFactoryOfGroupPageNew")).toJSON([]);if(E==="TreeEns")return(yield D("ClassFactoryOfPageBaseTreeEns")).toJSON([]);if(E==="GL")return yield(yield D("ClassFactoryOfGenerList")).toJSON([]);if(E==="PG")return(yield D("ClassFactoryOfPanelGroup")).toJSON([]);if(E==="Entity")return yield(yield D("ClassFactory")).toJSON([]);const a=new K;return yield a.RetrieveAll(),JSON.stringify(a)})}Save_TextBox_X(E,a,t,C,A){return f(this,null,function*(){var G,_;const u=new T;if(u.Icon="icon-user",u.ModuleNo=a,u.ModuleNoT=this.GetSortName(a),u.SystemNo=this.SystemNo,u.SetPara("EnName","TS.GPM.MenuGenerPage"),E=="DBList"){const e=S.UrlGPN("GPN_DBList","");return new r(i.GoToUrl,e)}if(E=="DictDtlView"){const e=new x(t);if((yield e.IsExits())==!0)return new r(i.Message,"\u83DC\u5355ID:["+t+"]\u5DF2\u7ECF\u5B58\u5728");if(e.setPKVal(A),(yield e.RetrieveFromDBSources())==0)return new r(i.Message,"\u8868\u5355ID:["+A+"]\u4E0D\u5B58\u5728.")}if(E=="DictDtlView.SelectFrmDtl"){const e=new x;e.No=this.RequestVal("tb2","DictDtlView"),e.Name=this.RequestVal("tb1","DictDtlView");const o=this.RequestVal("tb3","DictDtlView"),F=new I(o);yield F.RetrieveFromDBSources(),e.DictID=F.No,e.DictName=F.Name,e.DictDtlID=t,e.DictDtlName=C,yield e.Insert(),u.MenuModel="DictDtlView",u.Name=e.Name,u.FrmID=e.No,u.No=e.No,u.Icon="icon-wallet",u.SetPara("EnName","TS.CCBill.DictDtlView"),u.Tag1=C,u.Alias="DictDtlView_"+e.No,u.UrlPath="/@/CCFast/CCBill/SearchDict.vue",u.UrlExt="/DictDtlView_"+e.No+"?EnName="+e.No,yield u.Insert();const B=S.UrlEn("TS.CCBill.DictDtlView",e.No);return new r(i.GoToUrl,B,"\u5C5E\u6027")}if(E=="FlowDtlView"){if((yield new x(t).IsExits())==!0)return new r(i.Message,"\u83DC\u5355ID:["+t+"]\u5DF2\u7ECF\u5B58\u5728");if((yield new U(A).IsExits())==!1)return new r(i.Message,"\u6D41\u7A0B\u7F16\u53F7\u9519\u8BEF:"+A)}if(E=="FlowDtlView.SelectFlowDtl"){const e=new J;e.No=this.RequestVal("tb2","FlowDtlView"),e.Name=this.RequestVal("tb1","FlowDtlView");const o=this.RequestVal("tb3","FlowDtlView"),F=new U(o);yield F.RetrieveFromDBSources(),e.FlowNo=F.No,e.DictID="ND"+Number.parseInt(F.No+"01"),e.DictName=F.Name,e.DictDtlID=t,e.DictDtlName=C,yield e.Insert(),u.MenuModel="FlowDtlView",u.Name=e.Name,u.FrmID=e.No,u.No=e.No,u.Icon="icon-wallet",u.SetPara("EnName","TS.CCBill.FlowDtlView"),u.Tag1=C,u.Alias="FlowDtlView_"+e.No,u.Name=e.Name,u.UrlExt="/"+e.No+"?displayMode=table&FrmID="+e.No,u.UrlPath="/src/CCFast/CCBill/SearchDict.vue",u.FrmID=e.No,u.ListModel=0,u.WorkType="0",yield u.Insert();const B=S.UrlEn("TS.CCBill.FlowDtlView",e.No);return new r(i.GoToUrl,B,"\u5C5E\u6027")}if(E=="View"){const e=new I;if(e.No=C,(yield e.IsExits())==!0)return new r(i.Message,"ID:"+C+"\u5DF2\u7ECF\u5B58\u5728,\u8BF7\u91CD\u547D\u540D.");const o=new p("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");o.AddPara("TB_Name",t),o.AddPara("TB_No",C),o.AddPara("DDL_DBSrc","local"),o.AddPara("FK_FrmSort",a),o.AddPara("EntityType",100);const F=yield o.DoMethodReturnString("NewFrmGuide_Create_DBList");if(F.includes("err@")==!0)return new r(i.Message,F);u.MenuModel=E,u.Name=t,u.FrmID=C,u.No=C,u.Icon="icon-wallet",u.SetPara("EnName","TS.CCBill.DBEntity"),u.Tag1=C,u.Alias="View_"+C,u.UrlPath="/@/WF/views/Search.vue",u.UrlExt="/WF/Comm/Search?EnName="+C,yield u.Insert();const B=S.UrlEn("TS.CCBill.DBEntity",C);return new r(i.GoToUrl,B,"\u5C5E\u6027")}if(E=="GenerListEn"){u.MenuModel=E,u.Name=t;const e=new $;e.No=g.GenerGUID(),e.Name=t,yield e.Insert(),u.No=e.No,u.Icon="icon-wallet",u.SetPara("EnName","TS.CCBill.GenerListEn"),u.Tag1=e.No,u.Alias=a+"_"+e.No,u.UrlPath="/@/WF/views/GenerList.vue",u.UrlExt="/WF/Comm/GenerList?EnName=GL_GLEn&EnID="+e.No,yield u.Insert();const o=S.UrlEn("TS.CCBill.GenerListEn",e.No);return new r(i.GoToUrl,o,"\u8BBE\u8BA1\u901A\u7528\u5217\u8868")}const c=["Entity.EnName.CompentType.Paras","Search.EnName.Paras","GPN.Ens.Paras","TreeEns.Ens.Paras","GL.Ens.Paras","PG.Ens.Paras"],h=new Map([["Search",{file:"/@/WF/Comm/Search.vue",urlPrefix:"/WF/Comm/Search",factory:yield D("ClassFactory")}],["GPN",{file:"/@/WF/Comm/UIEntity/GroupPageNew.vue",urlPrefix:"/WF/Comm/GroupPageNew",factory:yield D("ClassFactoryOfGroupPageNew")}],["TreeEns",{file:"/@/WF/Comm/TreeEns.vue",urlPrefix:"/WF/Comm/TreeEns",factory:yield D("ClassFactoryOfPageBaseTreeEns")}],["GL",{file:"/@/WF/views/GenerList.vue",urlPrefix:"/WF/Comm/GenerList",factory:yield D("ClassFactoryOfGenerList")}],["PG",{file:"/@/WF/Comm/PanelGroup.vue",urlPrefix:"/WF/Comm/PanelGroup",factory:yield D("ClassFactoryOfPanelGroup")}]]);if(c.includes(E)){const e=E.split(".")[0],o=E.split(".")[1],F=h.get(e);if(!F)return l.error("\u62B1\u6B49\uFF0C\u4F60\u8F93\u5165\u7684\u7C7B\u578B\u4E0D\u5B58\u5728"),new r(i.DoNothing,null);const{file:B,urlPrefix:d,factory:P}=F,y=this.RequestVal("tb1",e+"."+o),M=yield P.GetEn(y);return u.ModuleNo=a,u.Name=((G=M==null?void 0:M._enMap)==null?void 0:G.EnDesc)||M.PageTitle,u.Alias=a+"_"+y,u.UrlPath=B,u.UrlExt=d+"?EnName="+y+"&"+t,u.SystemNo=this.SystemNo,u.MenuModel=n.FixedUrl,u.IsEnable=1,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E==="Entity.EnName.componentName.Paras"){const e="/@/WF/Comm/",o=this.RequestVal("tb1","Entity.EnName"),F=this.RequestVal("tb1","Entity.EnName.componentName"),d=yield(yield D("ClassFactory")).GetEn(o);if(u.ModuleNo=a,u.Name=((_=d==null?void 0:d._enMap)==null?void 0:_.EnDesc)||d.PageTitle,u.Alias=a+"_"+o,u.UrlPath=e+F+".vue",t+="",t=t.trim(),t.length===0){const P=o.lastIndexOf(".");t=o.substring(P)+F}return u.UrlExt=t+"?EnName="+o,typeof C=="string"&&C.trim().length>0&&(u.UrlExt=t+"?EnName="+o+"&"+C.replace(/\?/g,"").replace(/^&+/g,"")),u.SystemNo=this.SystemNo,u.MenuModel=n.FixedUrl,u.IsEnable=1,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.SelfUrl){u.ModuleNo=a,u.Name=t,u.UrlPath=C,u.UrlExt=A,u.SystemNo=this.SystemNo,u.MenuModel=n.SelfUrl,u.IsEnable=1;const e=new p("BP.WF.HttpHandler.WF_Admin_FoolFormDesigner");return e.AddPara("name",t),e.AddPara("flag",!0),u.Alias=yield e.DoMethodReturnString("ParseStringToPinyin"),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E==="LinkFlow.FlowNo"){const e=this.RequestVal("tb1","LinkFlow"),o=t,F=new q(o);return yield F.Retrieve(),e=="Home"&&(u.Name="\u4E3B\u9875:"+F.Name,u.Alias=a+"_GL_CC"+F.No,u.UrlPath="/src/WF/Comm/En.vue",u.UrlExt=`/Flow_Home_${F.No}?EnName=TS.TSClass.FlowOneSetting&PKVal=${F.No}`),e=="FlowSearch"&&(u.Name=F.Name+"\u67E5\u8BE2",u.Alias="SearchFlow"+F.No,u.UrlPath="/src/WF/Rpt/SearchFlow.vue?FlowNo="+F.No,u.UrlExt=`/SearchFlow_${F.No}?FlowNo=${F.No}`),e=="FlowGroup"&&(u.Name=F.Name+"\u5206\u6790",u.Icon="icon-chart",u.Alias="FlowGroup"+F.No,u.UrlPath="/src/WF/Rpt/GroupFlow.vue?FlowNo="+F.No,u.UrlExt=`/GroupFlow_${F.No}?FlowNo=${F.No}`),e=="Start"&&(u.Alias=a+"_"+o,u.UrlPath="",u.UrlExt="self://WF/MyFlow?FlowNo="+o,u.Name="\u53D1\u8D77:"+F.Name),e=="Todolist"&&(u.Name="\u5F85\u529E:"+F.Name,u.Alias=a+"_GL_Todolist"+F.No,u.UrlPath="/@/WF/views/GenerList.vue",u.UrlExt=`/GL_Todolist_${o}?EnName=GL_Todolist&FlowNo=${F.No}`),e=="Runing"&&(u.Name="\u5728\u9014:"+F.Name,u.Alias=a+"_GL_Runing"+F.No,u.UrlPath="/@/WF/views/GenerList.vue",u.UrlExt=`/GL_Running_${o}?EnName=GL_Runing&FlowNo=${F.No}`),e=="CC"&&(u.Name="\u6284\u9001:"+F.Name,u.Alias=a+"GL_CC"+F.No,u.UrlPath="/@/WF/views/GenerList.vue",u.UrlExt=`/GL_CC_${o}?EnName=GL_CC&FlowNo=${F.No}`),u.ModuleNo=a,u.SystemNo=this.SystemNo,u.MenuModel=n.SelfUrl,u.IsEnable=1,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E==="DictLink.DictID"){const e=this.RequestVal("tb1","DictLink"),o=t,F=new I(o);return yield F.Retrieve(),e=="Home"&&(u.Name="\u4E3B\u9875:"+F.Name,u.Alias=a+"_Dict_Home_"+F.No,u.UrlPath="/@/WF/Comm/En.vue",u.UrlExt=`/Dict_Home_${F.No}?EnName=TS.CCBill.DictSettingOne&PKVal=${F.No}`),e=="Search"&&(u.Name="\u5217\u8868:"+F.Name,u.Alias=a+"_List_"+F.No,u.UrlPath="/@/CCFast/CCBill/SearchDict.vue",u.UrlExt=`/Dict_List_${F.No}?FrmID=${F.No}`),e=="Group"&&(u.Name="\u5206\u6790:"+F.Name,u.Alias=a+"_Analy_"+F.No,u.UrlPath="/@/CCFast/CCBill/SearchDict.vue",u.UrlExt=`/Dict_Analy_${F.No}?FrmID=${F.No}&displayMode=group`),e=="Rpt"&&(u.Name="\u62A5\u8868:"+F.Name,u.Alias=a+"_Rpt_"+F.No,u.UrlPath="/@/CCFast/CCBill/SearchDict.vue",u.UrlExt=`/Dict_Rpt_${F.No}?FrmID=${F.No}&displayMode=rpt`),e=="BS"&&(u.Name="\u5927\u5C4F:"+F.Name,u.Alias=a+"_BigScreen_"+F.No,u.UrlPath="/src/CCFast/Views/RptWhiteMain.vue",u.UrlExt=`/Dict_BigScreen_${F.No}?PageID=${F.No}`),u.ModuleNo=a,u.SystemNo=this.SystemNo,u.MenuModel=n.SelfUrl,u.IsEnable=1,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.StandAloneFlow){const e=new p("BP.WF.HttpHandler.WF_GPM_CreateMenu");e.AddPara("SortNo",a),e.AddPara("FlowName",t),e.AddPara("FlowDevModel",0),e.AddPara("ModuleNo",a);const o=yield e.DoMethodReturnString("StandAloneFlow_Save");if(typeof o=="string"&&o.startsWith("err@")){alert(o);return}return u.ModuleNo=a,u.Name=t,u.UrlExt=C,u.MenuModel=n.StandAloneFlow,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.RptWhite)return u.Name=t,u.MenuModel=n.RptWhite,u.IsEnable=1,u.Icon="icon-screen-desktop",u.SetPara("EnName","TS.CCFast.Rpt3D"),u.UrlPath="/src/CCFast/Views/RptWhiteMain.vue",yield u.Insert(),u.UrlExt="/RptWhite"+u.No.substring(0,6)+"?PageID="+u.No,yield u.Update(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.RptBlue)return u.Name=t,u.MenuModel=n.RptBlue,u.IsEnable=1,u.Icon="icon-screen-desktop",u.SetPara("EnName","TS.CCFast.Rpt3D"),yield u.Insert(),u.UrlExt=`${window.location.origin}/goview.html#/chart/home/${u.No}`,yield u.Update(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.Tabs)return u.Name=t,u.MenuModel=n.Tabs,u.IsEnable=1,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.Rpt3D)return u.Name=t,u.Icon="icon-screen-desktop",u.MenuModel=n.Rpt3D,u.SetPara("EnName","TS.CCFast.Rpt3D"),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.FlowUrl){alert("\u672A\u5B9E\u73B0");return}if(E===n.Func){const e=new O;return e.Name=t,e.FuncID=C,yield e.Insert(),u.Name=e.Name,u.MenuModel=n.Func,u.Icon="icon-energy",u.UrlExt=e.No,u.SetPara("EnName","TS.CCFast.Func"),u.SetPara("EnPKVal",e.No),u.No=e.No,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.Dict){const e=C,o=t,F=new p("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");return F.AddPara("TB_No",e),F.AddPara("TB_Name",o),F.AddPara("TB_PTable",e),F.AddPara("FK_FrmSort",this.SystemNo),F.AddPara("EntityType",2),yield F.DoMethodReturnString("NewFrmGuide_Create"),u.Name=o,u.UrlExt=e+"?displayMode=table&FrmID="+e,u.UrlPath="/src/CCFast/CCBill/SearchDict.vue",u.FrmID=e,u.MenuModel=n.Dict,u.ListModel=0,u.WorkType="0",u.SetPara("EnName","TS.CCBill.FrmDict"),u.SetPara("EnPKVal",e),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.DBList){const e=C,o=t,F=new p("BP.WF.HttpHandler.WF_Admin_CCFormDesigner");F.AddPara("TB_No",e),F.AddPara("TB_Name",o),F.AddPara("TB_PTable",e),F.AddPara("FK_FrmSort",this.SystemNo),F.AddPara("EntityType",2);const B=yield F.DoMethodReturnString("NewFrmGuide_Create_DBList");if(typeof B=="string"&&B.startsWith("err@")){l.error(B);return}return u.Name=o,u.UrlExt=e+"?FrmID="+e,u.UrlPath="/src/CCFast/CCBill/SearchDBList.vue",u.MenuModel=n.DBList,u.ListModel=0,u.WorkType="0",u.Icon="icon-book-open",yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.DictCopy){alert("\u672A\u7FFB\u8BD1..");return}if(E===n.Bill){alert("\u672A\u7FFB\u8BD1..");return}if(E===n.DictRef){alert("\u672A\u7FFB\u8BD1..");return}if(E===n.DictTable){const e=new R;if(e.No=C,e.Name=t,e.No==t||e.Name==""){alert("err@\u540D\u79F0\u4E0E\u7F16\u53F7\u4E0D\u80FD\u4E3A\u7A7A.");return}if(yield e.IsExits()){l.warning("\u7F16\u53F7\u5DF2\u7ECF\u5B58\u5728["+e.No+"]\u8BF7\u4F7F\u7528\u5176\u4ED6\u7684\u7F16\u53F7.");return}return e.DBSrcType="SysDict",e.CodeStruct=0,yield e.Insert(),u.Name=e.Name,u.UrlExt=e.No,u.MenuModel=n.DictTable,u.SystemNo=this.SystemNo,u.SetPara("CodeStruct",0),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.DictTableTree){const e=new R;if(e.No=C,e.Name=t,e.No==t||e.Name==""){alert("err@\u540D\u79F0\u4E0E\u7F16\u53F7\u4E0D\u80FD\u4E3A\u7A7A.");return}if(yield e.IsExits()){alert("\u7F16\u53F7\u5DF2\u7ECF\u5B58\u5728["+e.No+"]\u8BF7\u4F7F\u7528\u5176\u4ED6\u7684\u7F16\u53F7.");return}return e.SrcType="SysDict",e.CodeStruct=1,u.SetPara("CodeStruct",1),yield e.Insert(),u.Name=e.Name,u.UrlExt=e.No,u.MenuModel=n.DictTableTree,yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.Task)return u.Name=t,u.UrlExt="/src/CCFast/Task/Task.htm",u.MenuModel="Task",u.Mark="Task",u.Icon="icon-note",u.SetPara("EnName","TS.GPM.MenuExt"),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.Info){const e=new T;e.ModuleNo=a,e.SystemNo=this.SystemNo,e.Name=t,e.UrlExt="/src/WF/Comm/Search.vue?EnsName=TS.CCOA.CCInfo.Info",e.MenuModel="Info",e.Mark="Info",e.Icon="icon-note",e.Insert();const o=new T;return o.ModuleNo=a,o.SystemNo=this.SystemNo,o.Name=t+"\u7C7B\u578B",o.UrlExt="/src/WF/Comm/Ens.vue?EnsName=TS.CCOA.CCInfo.InfoType",o.MenuModel="Info",o.Mark="Info",o.Icon="icon-note",o.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)}if(E===n.Notepad)return u.Name=t,u.UrlExt="/src/CCFast/Notepad/Notepad.htm",u.MenuModel="Notepad",u.Mark="Notepad",u.Icon="icon-note",u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.KnowledgeManagement)return u.Name=t,u.UrlExt="/src/CCFast/KnowledgeManagement/Default.htm",u.MenuModel="KnowledgeManagement",u.Mark="KnowledgeManagement",u.Icon="icon-eyeglass",u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null);if(E===n.WorkRec)return u.ModuleNo=a,u.Name=t,u.UrlPath="/@/WF/Comm/Search.vue",u.UrlExt="/WF/Comm/Search?EnName=TS.CCOA.WorkLog.WorkRec&1=1",u.SystemNo=this.SystemNo,u.MenuModel="WorkRec",u.IsEnable=1,u.Alias=g.GenerGUID(),yield u.Insert(),l.info("\u521B\u5EFA\u6210\u529F\uFF0C\u60A8\u53EF\u4EE5\u5728\u5728\u83DC\u5355\u91CC\u6267\u884C\u9AD8\u7EA7\u7F16\u8F91\u4E0E\u6388\u6743."),new r(i.CloseAndReload,null)})}}export{w as GPN_Menu};
