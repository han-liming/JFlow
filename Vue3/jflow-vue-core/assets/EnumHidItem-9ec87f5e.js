var i=Object.defineProperty;var m=(r,t,u)=>t in r?i(r,t,{enumerable:!0,configurable:!0,writable:!0,value:u}):r[t]=u;var E=(r,t,u)=>(m(r,typeof t!="symbol"?t+"":t,u),u);import{UAC as o}from"./UAC-8e255d47.js";import{Map as s}from"./Map-73575e6b.js";import{EntityMyPK as a}from"./EntityMyPK-e742fec8.js";import{a as e}from"./MapExt-db8cd7f3.js";import"./index-f4658ae7.js";import"./Attrs-62391d6b.js";import"./EnumLab-3cbd0812.js";import"./DataType-33901a1c.js";import"./ParamUtils-cdc24dd6.js";import"./SystemConfig-b93c25b3.js";import"./bignumber-cf158d26.js";import"./ParamsUtils-3cbc5822.js";import"./Attr-d5feb8b8.js";import"./RefMethod-33a71db4.js";import"./SearchNormal-5c7fda42.js";import"./SearchFKEnum-31a3a609.js";import"./Glo-fda3f906.js";import"./EnMapExt-8e17025c.js";import"./Entities-6a72b013.js";import"./MapAttr-cb594d82.js";import"./Events-141c34ea.js";import"./SFTable-d63f9fb4.js";import"./EntityNoName-d08126ae.js";import"./SFDBSrc-e641ea16.js";import"./BSEntity-840a884b.js";class P extends a{constructor(u){super("TS.MapExt.EnumHidItem");E(this,"Desc1",`
  #### \u8BF4\u660E
   - \u8BBE\u7F6E\u683C\u5F0F
   - @0=1,2,3
   - @1=4,5
   - \u6807\u8BC6\u5F53\u9009\u9879\u662F0\u7684\u65F6\u5019,\u679A\u4E3E\u503C\u9690\u85CF1,2,3 \u5F53\u9009\u9879=1\u7684\u65F6\u5019\uFF0C\u9690\u85CF4,5.
   #### \u679A\u4E3E\u503C
   - \u8BF7\u6253\u5F00\u679A\u4E3E\u5E93\u67E5\u770B, \u5BF9\u5E94\u7684IntKey.
   - \u4E5F\u53EF\u4EE5\u901A\u8FC7,select * from sys_enum WHERE EnumKey='xxxxx' \u67E5\u770B \u679A\u4E3E\u7684\u952E\u503C.
  `);this.RefEnName="TS.Sys.MapExt",u&&(this.MyPK=u)}get HisUAC(){const u=new o;return u.IsDelete=!0,u.IsUpdate=!0,u.IsInsert=!0,u}get EnMap(){const u=new s("Sys_MapExt","\u70B9\u51FB\u4E8B\u4EF6\u9690\u85CF\u9009\u9879");u.AddMyPK(),u.AddTBString(e.FK_MapData,null,"\u8868\u5355ID",!0,!0,100);const p=`
    SELECT KeyOfEn as No, Name FROM Sys_MapAttr 
    WHERE LGType=1 AND UIVisible=1 AND UIIsEnable=1 AND FK_MapData='@FK_MapData' AND KeyOfEn!='@AttrKey'`;return u.AddDDLSQL(e.Tag,null,"\u8054\u52A8\u7684\u63A7\u4EF6",p,!0,null,!1,100),u.AddTBStringDoc(e.Tag4,null,"\u914D\u7F6E\u9879",!0,!1,!0,this.Desc1),this._enMap=u,this._enMap}}export{P as EnumHidItem};
