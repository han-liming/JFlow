#### 1.说明
1. JFlow 是一款经典的纯国产全开源的工作流引擎。
1. 数据库安装：自动安装数据库所需要的表及视图。
1. 程序需要修改 application-druid.yml 中的数据库连接。
1. 点starred记住序号,加群：578285417
1. 加微信联系商务: 18660153393

#### 2.相关连接
- JFlow官网： http://ccflow.org
#### 3.项目说明
- jflow-vue-core：  jflow前端vue3打包后代码
#### 4.后台项目启动
- 后台在H5文件夹中

![输入图片说明](doc/readme/jflow介绍.png)
- 数据库配置修改 application-druid.yml
- 先手动建立一个数据库 如下图：jflow_xzd（可自定义）

![输入图片说明](doc/readme/数据库参数.png)
- 此时数据库中没有任何表和视图
![输入图片说明](doc/readme/没有表.png)
  
- 启动项目 JFlowApplication.java  端口：8009 （可自定义）
![输入图片说明](doc/readme/启动后台项目.png)
#### 5.vue3项目启动
- 修改后台地址  
 _app.config.js文件中的VITE_GLOB_API_URL属性改为：http://localhost:8009
  ![输入图片说明](doc/readme/vue3后台.png)
- 进入jflow-vue-core目录   
cd jflow-vue-core
- 全局安装http-server   
npm install http-server -g
- 启动(-p:端口3000)  
http-server -p 3000
- 启动成功
![输入图片说明](doc/readme/vue启动成功.png)

#### 6.数据库表初始化
- 进入项目  htt://localhots:3000
- 项目检测到未创建数据库会自动跳到如下界面：
  ![输入图片说明](doc/readme/点击接受协议.png)
  ![输入图片说明](doc/readme/知道了.png)
- 自动安装数据库表和视图（需要多等待一会）
![输入图片说明](doc/readme/等待自动安装数据库表和视图.png)
- 安装完毕点击“确定”按钮
![输入图片说明](doc/readme/自动安装完成.png)
- 登录页面
![输入图片说明](doc/readme/正常登录页面.png)
- 选择中间件模式
![输入图片说明](doc/readme/中间件模式.png)
- 选择低代码模式
![输入图片说明](doc/readme/低代码模式.png)



 

